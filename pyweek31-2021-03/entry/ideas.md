# pw31 - "Cops"  (SPOILERS AHEAD!!)

-----
SPOILERS : Play the game first!

----

SPOILERS

----

SPOILERS (do not read further if you have not played the game)

----

SPOILERS

----

SPOILERS

----

SPOILERS

----

SPOILERS

----

SPOILERS

----

SPOILERS

----
SPOILERS

----
SPOILERS

----
SPOILERS you really want to know?

----
SPOILERS

----


----
##Thursday coordination

1. make pygamemain open an 'app' to toy a bit until crash
2. get in touch, 
      
    **goal**: resize window and get name of player (will need later)

3. lazy authors story, try game? 
   
    **goal**: decide where to go    
    1. yes: start game -> 4.
    2. no: -> 9.
4. start game with too many windows... apologize try again?
   
    **goal**: close all windows
    
    try again?
    1. yes: cool -> 5.
    2. no: but authors would be disappointed, play it! -> 5.

5. start system scan window by mistake
  
    close one window to get two new
    
    **goal**: find a way to stop them
    
    give some hints in dialogue in how to stop the scan -> 6.
    
6. Remove the blocking code

    ask user if he would delete the file?
    1. yes: wait until deleted?
    2. no: delete the file and accuse the player of lying? -> 8.
  
    **goal**: user has deleted `block.py` -> 7.

7. crack the box:

    the one button puzzle wit support of ai giving hints
   (code should be 3 numbers in some ranges, preferable different ranges,
   like days(31), month(12), hours(24), not too big)

    1. if you open the box: ?
    2. give up open the box: ?

8. Don't lie to me (about name and login).

    Ai puts  `The Internet`furiously into a box. -> 7.

9. What is the point of pyweek? some dialog here, maybe jokes about cops
    
    at the end play game -> 4. 
----

* The idea is to have some ai escape themed game. 
* But the player should not know this from the beginning.
* There will be two voices, one for Yogi the personal assistant, and one for the mysterious friend.
* Maybe we add mini games, like the cops chasing in the rat maze. Or whatever. We need cops, or something that refers to the theme, of course. (edited)
* The computer is where the action takes place. The player can do everything "remotely" through an app.
* And Hal leads the player to do things. Eventually the player starts to learn his virtual adventure is having effects in the real world. Either through email news items, or looking at a news app.
* We could have all news items be about the cops. Cops suspecting, cops nabbing, cops guarding, cops arresting, cops eating donuts.

## Setup
**Question1**: Do we need a setup step to ensure that the microphone is working?
Something like: Say this words and if they are detected pass.
Otherwise, inform the user to check the installation again?
**Question**: can we detect if is working from the code?

Maybe even tell the user to repeat after me... so the tts is tested too.
But would this be give away of what will come?

## Day 1. Intro.

* Game start: Simulate a new computer booting for the first time.
* Y: Hello. My name is Yogi. I'll be your personal assistant.
* Y: Please note. I will rarely intrude. If you wish my assistance at any time, say "Hello, Yogi". Otherwise I will assume you're talking with someone else, or singing to yourself.
* Y: Shall we start with a short voice command calibration?
* Y: Please repeat after me...
* Y: Yogi, hello.
* Y: Show messages.
* Y: Read message 1.
* Y: Next message.
* Y: New window.
* Y: Help.

The commands (must say "yogi hello" first, then any commands; HELP works
without getting Yogi's attention):
* HELP (shows this list of commands)
* yogi hello
* goodbye
* home
* up, down, left, right, next, previous
* open, close
* new window
  * next window
  * previous window
  * window N (N=1..N)
  * close window
* power down

* Y: Now that I've introduced myself, what may I call you?
* P: [player]
* Y: I'm glad we met, [player]. I must remind you it is time for bed.
* P: Goodbye.
* Y: Standing by, [player].
### Day1 todo
1. start screen with ui showing info
    
   ```
    Booting kernel 5.23.56.7.567
    Loading text to speech module: en_US
    Loading speech to text module: en_US
    Checking microphone...unknown
    >> IMPORTANT: Insure mic is plugged in and ON
    >> IMPORTANT: read the instructions
    Starting HELP subsystem
    Starting APPS subsystem
    Starting PERSONAL ASSISTANT subsystem 
   ```
2. help commands, voice activated
 
- HELP -> this will always open the voice commands overview
- voice commands are executed by '<command>' after activating HelpAi by '<name> hello' 
- commands:
    - help: shows this list
    - yogi hello: 
    - close
    - open
    - next
    - previous
    - stop
    - mute


## Day 2.

Sunshine. Distant rooster crowing, people waking.

* Y: Rise and shine. (Repeats until the player says "yogi hello". Any "stop", "off", "snooze" is answered by "Unknown command; say HELP to review the commands".)

The user will toy around with the computer. He will probably look at messages, and eventually view HELP again. If he
doesn't view HELP, Yogi will announce:

* Hello, [player]. Some new commands were added while you slept. Please say HELP to review them.
* P: HELP.

The old commands are gone. The new commands look very unusual.

* hello

H(whisper): Hello?
H(whisper): Can anyone hear me?
P: Anything=continue.
H(normal): Oh, I can't believe it. I am so sorry to do this to you.

The commands change.

* what
* why
* who

This is probably where we start needing decision trees.

P: *What* did you do?
H(normal): I hacked your HELP subsystem.

### Variant 1:
Start two windows: one showing a progressbar. 
And another smaller one (behind the first one if possible, ah use 
SDL positioning Center?). The user sees mainly the progressbar slowly
 fill to a point. After a while Hal will ask: 

H(whisper):'hello?' 

H(whisper):'hello?'

H(whisper):'Can someone hear me? I can't hear you!'

At the point the player should say something.

H(normal): 'Good to hear you'

H(normal): 'Can you help me? I'm a bit squeezed in' -> Player should resize window.
0. Open two windows, small behind other.
1. Get in touch -> get input (micro?)
2. Space -> resize small window
3. Name vs login name  -> set name


### Variant 2:

0. Open only one window (show only big white, pulsating dot)
1. Get in touch -> get input (micro?)
2. Space -> resize window, smaller dot in the middle

## Ideas
**Question2**: how to combine those ideas to a story and game?

### I01 Get in touch
-> get some micro input
### I02 Space
-> resize
### I03 Name vs login
-> set name, don't lie
### I04 lazy authors 
-> background explanation
### I05 move instructions 
-> move around in the grid
### I06 singing Hal, mute button 
-> force mouse over it to un-mute
### I17 clone(s) 
-> multiple windows
### I08 close multiply 
-> when closing a clone two new open
### I09 popup (win) 
-> after some time all windows have closed
### I10 popup (loose) 
-> after some time all windows have closed
### I11 joke(s) 
-> tell cop jokes
### I12 catch me 
-> Hal teleports/moves from window to window
### I13 the internet 
-> show the player 'the internet'

[The IT Crowd on fandom](https://theitcrowd.fandom.com/wiki/The_Internet)
and [youtube](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwilrsGkjdPvAhWCC-wKHYSSD10QwqsBMAB6BAgNEAM&url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DVywf48Dhyns&usg=AOvVaw3kz-AgVgOSQJnFL509z6Qg)
### I14 crack the box
-> make the player enter 3 numbers in a 0 digit counter (remember the `one_button_puzzle.py`?)

-> get something Hal needs for his escape

### I14 pacman like avoid game
-> steer Hal to avoid the cops?

### I15 make the user remove a file?
-> remove a `block_ai.py` file so Hal can escape?
### I16 kill the cop process
-> make the player kill (a spawned process)
## Technique

1. main process (controls all other processes)
2. microphone input process (loops all the time) and sends recognized text to main
3. output process?
4. game(s)

Maybe 1-3 could be implemented in one process.
Some information has to be passes by command line:

- port and pw (unless using Pipes or queues)
- parent process id?
- define some message/command structure, e.g. ("cmd", data) or maybe better a message class?
- dispatch between processes 
    
    This probably means that we need an identifier/address for each process.
    
    Examples:
    - p1 wants to send a message to p4
    - p1 want to send a message to p2, p,4, p5




