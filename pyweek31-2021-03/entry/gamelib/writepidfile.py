import logging
import os

logger = logging.getLogger(__name__)

logger.debug("importing writepidfile.py")

pid_file_name = 'game.pid'


def write_pid_file(pid, proc_name):
    """main and every subproc should call this early when starting"""
    logger.info(f'writing pid={str(pid)}')
    with open(pid_file_name, 'a') as f:
        f.write(f"{pid}: {proc_name}{os.linesep}")
        f.flush()
        f.close()


def remove_pid_file():
    """main should call this when exiting, after all the children exited"""
    try:
        os.remove(pid_file_name)
    except Exception as ex:
        logger.warning(ex)
