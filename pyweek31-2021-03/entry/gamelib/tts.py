# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'tts.py' is part of pw-31
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Text to speech module.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2021"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module


logger = logging.getLogger(__name__)
logger.debug("importing...")


def run(log_queue, log_level, publisher_queue, consumer_queue, proc_name, *args):
    import pyttsx3

    import gamelib.messages as m
    from gamelib.shared import get_messages, send_message

    def onStart(name):
        print('starting', name)

    def onWord(name, location, length, *args, **kwargs):
        print('word', name, location, length, args, kwargs)

    def onEnd(name, completed):
        logger.info('finishing %s %s', name, completed)
        msg = m.SpeechEnded(name, completed)
        send_message(publisher_queue, msg, proc_name)

    engine = pyttsx3.init(debug=False)
    # engine.connect('started-utterance', onStart)
    # engine.connect('started-word', onWord)
    engine.connect('finished-utterance', onEnd)
    logger.info("rate: %s, volume: %s, voice: %s", engine.getProperty("rate"), engine.getProperty("volume"),
                engine.getProperty("voice"))

    engine.startLoop(False)
    running = True
    while running:
        engine.iterate()
        messages = get_messages(consumer_queue, 0.2)
        for message in messages:
            if isinstance(message, m.Say):
                if message.interrupt:
                    engine.stop()
                engine.setProperty("rate", message.rate)
                engine.setProperty("volume", message.volume)
                if message.voice:
                    engine.setProperty("voice", message.voice)
                engine.say(message.text, message.speech_id)
            elif isinstance(message, m.Quit):
                logger.info("tts settings running to False")
                running = False

    engine.endLoop()
    logger.info("tts terminated")


logger.debug("imported")
