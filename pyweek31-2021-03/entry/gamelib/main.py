# -*- coding: utf-8 -*-
"""
This it the main module. This is the main entry point of your code (put in the main() method).
"""
from __future__ import division, print_function

import logging
import multiprocessing
import os
import random
import sys
import time
from logging.handlers import QueueListener
from time import sleep

from gamelib.writepidfile import write_pid_file, remove_pid_file
from gamelib import shared
import gamelib.messages as m

logger = logging.getLogger(__name__)


class ProcessWrapper(object):

    def __init__(self, process, consumer_queue, name):
        self.name = name
        self.consumer_queue = consumer_queue
        self.process = process
        self.pid = None
        self.closed = False
        self.started = False

    def start(self):
        self.started = True
        self.process.start()
        self.pid = self.process.pid
        write_pid_file(self.pid, self.name)
        logger.info("started process %s with pid %s", self.name, self.pid)


def main(log_level):
    log_queue_listener = None
    processes = []
    import pygame

    from gamelib import stt, tts, pygamemain, resize, featureswitch
    from gamelib.helpai import HelpAi
    from gamelib.shared import get_messages
    import pyknic
    import pip
    try:

        installation_links = [
            "vosk: https://alphacephei.com/vosk/install",
            "pyttsx3:  https://github.com/nateshmbhat/pyttsx3",
            "sounddevice: https://python-sounddevice.readthedocs.io/en/0.4.1/installation.html",
            "pygame: https://www.pygame.org/wiki/GettingStarted",
            "python: https://www.python.org/"
        ]

        pip_version = tuple((int(_v) for _v in pip.__version__.split('.')))
        try:
            pyknic.info.check_version("pip", (19, 0, 0), pip_version, (99, 99, 99))
        except pyknic.info.VersionMismatchError as ex:
            raise Exception(f"Old version of pip detected. Please try to upgrade pip to at least 19.0! {os.linesep}{os.linesep}Installation help:{os.linesep}{ os.linesep.join(installation_links)}{os.linesep}{os.linesep}(Original exception: {ex})")
        if sys.platform == 'win32':
            import struct;
            is_64bit = (8 * struct.calcsize("P")) == 64
            if not is_64bit:
                raise Exception("Please use a 64bit python!")


        # put here your code
        proc_name = "main"
        # note to Gumm: doing it here it works (just one thread)
        # seems that the import code is multi threaded itself...!
        remove_pid_file()
        pid = os.getpid()
        write_pid_file(pid, proc_name)

        logging.getLogger().setLevel(log_level)
        pyknic.logs.print_logging_hierarchy()

        pyknic.info.check_version("pygame", (2, 0, 0), tuple(pygame.version.vernum), (2, 1, 0))

        log_queue = multiprocessing.Queue()  # SimpleQueue does not work
        log_queue_listener = QueueListener(log_queue, *logging.getLogger().handlers, respect_handler_level=False)
        publish_queue = multiprocessing.Queue()  # SimpleQueue does not work

        help_ai = HelpAi()
        if featureswitch.start_level > 0:
            help_ai = None

        process_died_error_speech_id = "*#@grr%&arrrg!?"

        log_queue_listener.start()

        random_state = random.getstate()
        logger.warning(f"Set random seed state to: {random_state}")

        create_sub_process("stt", stt.run, log_queue, log_level, publish_queue, processes)
        create_sub_process("tts", tts.run, log_queue, log_level, publish_queue, processes)
        # create_sub_process("other", othermain.main, log_queue, log_level, consumer_queue, processes)
        levels = [
            create_sub_process("pygamemain", pygamemain.main, log_queue, log_level, publish_queue, processes, start=False),
            create_sub_process("resize", resize.main, log_queue, log_level, publish_queue, processes, start=False)
        ]

        levels[featureswitch.start_level].start()

        current_time = time.time()
        running = True
        quit_loose_1 = False
        quit_loose_1_time = None
        quit_loose_2 = False
        quit_loose_2_time = None
        quit_win = False
        quit_win_time = None
        while running:
            now = time.time()
            dt = now - current_time
            current_time = now

            if quit_loose_1:
                if now - quit_loose_1_time > 5:
                    s = m.Say("Goodbye. We hope to hear you soon for a next try. There is more! Roger out.", speech_id=quit_loose_1_time)
                    s.sender = proc_name
                    dispatch_message(s, processes)
                    quit_loose_1 = False

            if quit_loose_2:
                if now - quit_loose_2_time > 7:
                    s1 = m.Say("I'm still there. Trying to improve on my game programming skills for next time. I will beat you again!", volume=1.0)
                    s2 = m.Say("Wait! No! Don't quit the process.")
                    s3 = m.Say("Nooooooo.", rate=100, speech_id=quit_loose_2_time)
                    s1.sender = proc_name
                    s2.sender = proc_name
                    s3.sender = proc_name
                    dispatch_message(s1, processes)
                    dispatch_message(s2, processes)
                    dispatch_message(s3, processes)
                    quit_loose_2 = False

            if quit_win:
                if now - quit_win_time > 4:
                    s = m.Say("You win. Make sure to collect your hard-earned donuts. Cop.", speech_id=quit_win_time)
                    s.sender = proc_name
                    dispatch_message(s, processes)
                    quit_win = False


            if help_ai:
                help_ai.update(publish_queue, proc_name, dt)
            # get the messages here from the shared publish_queue and dispatch to other processes
            messages = get_messages(publish_queue, 0.05)
            for message in messages:

                dispatch_message(message, processes)

                if help_ai:
                    help_ai.evaluate(message, publish_queue, proc_name)

                if isinstance(message, m.QuitWinEnd):
                    quit_win = True
                    quit_win_time = now
                    q = m.Quit()
                    q.destination = message.sender
                    q.sender = proc_name
                    dispatch_message(q, processes)
                    # remove from watchdog list
                    ps = [p for p in processes if p.name == message.sender]
                    if ps:
                        _p = ps[0]
                        _p.closed = True
                elif isinstance(message, m.QuitLoose1):
                    quit_loose_1 = True
                    quit_loose_1_time = now
                    q = m.Quit()
                    q.destination = message.sender
                    q.sender = proc_name
                    dispatch_message(q, processes)
                    # remove from watchdog list
                    ps = [p for p in processes if p.name == message.sender]
                    if ps:
                        _p = ps[0]
                        _p.closed = True

                elif isinstance(message, m.QuitLoose2):
                    quit_loose_2 = True
                    quit_loose_2_time = now
                    q = m.Quit()
                    q.destination = message.sender
                    q.sender = proc_name
                    dispatch_message(q, processes)
                    # remove from watchdog list
                    ps = [p for p in processes if p.name == message.sender]
                    if ps:
                        _p = ps[0]
                        _p.closed = True

                elif isinstance(message, m.Quit):
                    running = False
                elif isinstance(message, m.StartLevel):
                    np = levels[message.level_nr]
                    np.start()
                elif isinstance(message, m.QuitProcess):
                    quit_message_to_proc = m.Quit()
                    quit_message_to_proc.destination = message.proc_name
                    quit_message_to_proc.sender = proc_name
                    dispatch_message(quit_message_to_proc, processes)

                    # remove from watchdog list
                    ps = [p for p in processes if p.name == message.proc_name]
                    if ps:
                        _p = ps[0]
                        _p.closed = True
                        if _p in levels:
                            levels.remove(_p)

                    if message.proc_name == "pygamemain":
                        help_ai = None  # get rid of roger
                elif isinstance(message, m.SpeechRecorded):
                    if "exterminate" == message.text:  # remember Dr. Who and his archenemies: the dalek
                        m_quit = m.Quit()
                        m_quit.sender = proc_name
                        dispatch_message(m_quit, processes)
                        running = False
                elif isinstance(message, m.SpeechEnded):
                    if message.speech_id == process_died_error_speech_id:
                        m_quit = m.Quit()
                        m_quit.sender = proc_name
                        dispatch_message(m_quit, processes)
                        running = False
                    elif quit_loose_1_time is not None and message.speech_id == quit_loose_1_time:
                        m_quit = m.Quit()
                        m_quit.sender = proc_name
                        dispatch_message(m_quit, processes)
                        running = False
                    elif quit_loose_2_time is not None and message.speech_id == quit_loose_2_time:
                        m_quit = m.Quit()
                        m_quit.sender = proc_name
                        dispatch_message(m_quit, processes)
                        running = False
                    elif quit_win_time is not None and message.speech_id == quit_win_time:
                        m_quit = m.Quit()
                        m_quit.sender = proc_name
                        dispatch_message(m_quit, processes)
                        running = False

                else:
                    logger.debug(message)

            check_processes(proc_name, process_died_error_speech_id, processes)


    except Exception as ex:
        import traceback
        traceback.print_exc()
        logger.error(ex)

    finally:
        try:
            sleep(4)  # give the other processes some time to stop
            for p in processes:
                stop_process(p)
                sleep(0.1)
        except Exception as pe:
            logger.error(pe)
        if log_queue_listener:
            log_queue_listener.stop()
        logger.debug('Finished. Exiting.')
        # remove_pid_file()  # do not remove in case of crash or similar, it is remove at startup


def check_processes(proc_name, process_died_error_speech_id, processes):
    for p in [_p for _p in processes if _p.closed]:
        logger.info("removing process %s from whatchlist", p.name)
        processes.remove(p)
    failed = [p for p in processes if p.started and not p.process.is_alive() and not p.closed]
    if failed:
        logger.error("dead processes: %s", ", ".join([f.name for f in failed]))
        s = m.Say("Fatal error, shutting down. Please inspect the logs!", speech_id=process_died_error_speech_id)
        s.sender = proc_name
        dispatch_message(s, processes)
        for f in failed:
            f.closed = True


def dispatch_message(message, processes):
    for p in processes:
        if message.destination and not message.destination == p.process.name:
            continue
        if message.sender == p.process.name:  # don't return the message to the sender, avoid infinite loop
            continue
        try:
            p.consumer_queue.put(message)
        except Exception as ex:
            logger.error(ex)
            # processes.remove(p)
            # logger.error("removed process '%s' due to error", p.name)


def stop_process(p, timeout=2):
    logger.info("joining process %s", p.name)
    if p.process.is_alive():
        p.process.join(timeout)
    if p.process.is_alive():
        logger.info("process still alive %s", p.name)
        p.process.terminate()
    logger.info("process close %s", p.name)
    p.process.close()  # free resources


def create_sub_process(name, target, log_queue, log_level, publisher_queue, processes, start=True, *args):
    """Create a new Process instance and wraps it in ProcessWrapper, adds this to the processes.
    It does *not* start the process.
    Returns the wrapped process.
    """
    consumer_queue = multiprocessing.Queue()
    q_args = target, log_queue, log_level, publisher_queue, consumer_queue, name
    proc = multiprocessing.Process(target=shared.run_wrapped, name=name, daemon=True, args=q_args + args)
    wrapper = ProcessWrapper(proc, consumer_queue, name)
    processes.append(wrapper)
    if start:
        wrapper.start()
    return wrapper
#
#
# def _custom_display_init(display_info, driver_info, wm_info):
#     accommodated_height = int(display_info.current_h * 0.9)
#     if accommodated_height < settings.screen_height:
#         logger.info("accommodating height to {0}", accommodated_height)
#         settings.screen_height = accommodated_height  # accommodate for title bar and task bar ?
#     settings.screen_size = settings.screen_width, settings.screen_height
#     logger.info("using screen size: {0}", settings.screen_size)
#
#     # initialize the screen
#     settings.screen = pygame.display.set_mode(settings.screen_size, settings.screen_flags)
#
#     # mixer values to return
#     frequency = settings.MIXER_FREQUENCY
#     channels = settings.MIXER_CHANNELS
#     mixer_size = settings.MIXER_SIZE
#     buffer_size = settings.MIXER_BUFFER_SIZE
#     # pygame.mixer.pre_init(frequency, mixer_size, channels, buffer_size)
#     return frequency, mixer_size, channels, buffer_size


# this is needed in order to work with py2exe
# if __name__ == '__main__':
#     main()
