# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'yogi.py' is part of pw-31
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Default help ai.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division

import logging
import time

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2021"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

import time

from gamelib import messages, featureswitch
from gamelib.shared import CommandEvaluationsDispatcher, SentenceEvaluator, send_message, InteractorNode, \
    NodeResponseEvaluator, RecordEvaluator, CommandEvaluator

logger = logging.getLogger(__name__)
logger.debug("importing...")


class HelpAi(object):
    ai_name = "Roger"

    def __init__(self):
        self._activated = False
        help_command = SentenceEvaluator("help", call_back=lambda t, q, n: send_message(q, messages.CmdHelp(), n))
        commands = [
            CommandEvaluator("up", call_back=lambda t, q, n: send_message(q, messages.CmdUp(t), n)),
            CommandEvaluator("down", call_back=lambda t, q, n: send_message(q, messages.CmdDown(t), n)),
            CommandEvaluator("left", call_back=lambda t, q, n: send_message(q, messages.CmdLeft(t), n)),
            CommandEvaluator("right", call_back=lambda t, q, n: send_message(q, messages.CmdRight(t), n)),
            CommandEvaluator("repair", call_back=lambda t, q, n: send_message(q, messages.CmdRepair(t), n)),
            CommandEvaluator("close", call_back=lambda t, q, n: send_message(q, messages.CmdClose(t), n)),
            CommandEvaluator("open", call_back=lambda t, q, n: send_message(q, messages.CmdOpen(t), n)),
            CommandEvaluator("power down", call_back=lambda t, q, n: send_message(q, messages.CmdPowerDown(), n)),
            CommandEvaluator("goodbye", call_back=self._deactivate),
            CommandEvaluator("good bye", call_back=self._deactivate),
            help_command,
        ]

        self.active_command_evaluator = CommandEvaluationsDispatcher(commands, messages.Say("Invalid command."))
        activate_command = SentenceEvaluator(self.ai_name + " hello", call_back=self._activate)
        self.inactive_command_evaluator = CommandEvaluationsDispatcher([activate_command, help_command])

        # pimp up the help command to send a list of available commands
        available_cms = [c.text for c in commands]
        available_cms.append(activate_command.text)
        help_command.call_back = lambda t, q, n: send_message(q, messages.CmdHelp(available_cms), n)

        # intro
        sentence = "I eat donuts."
        self._intro_speech_id = "help_ai_intro_id"

        intro_line = (
                f"Hello. My name is {self.ai_name}. I'll be your personal assistant. "
                f"Please note. I will rarely intrude, so if you wish my assistance at any time, say. "
                f"{self.ai_name} hello. to activate me. "
                "Otherwise I will assume you're talking with someone else, or singing to yourself. "
                f"By saying 'Goodbye' I will go into standby until you say. {self.ai_name} hello. again. "
                "A short voice command calibration is needed. Please, speak into your microphone and repeat: "
                + sentence
        )
        done_node = InteractorNode("done", messages.Say("Are we done?"), [])
        name_no_evaluator = NodeResponseEvaluator("no")
        correct_name_node = InteractorNode("correct_name", messages.Say("Is this your name?"),
                                           [NodeResponseEvaluator("yes", next_node=done_node), name_no_evaluator])
        name_node = InteractorNode("name", messages.Say("What is your name?"),
                                   [RecordEvaluator(next_node=correct_name_node)],
                                   forward_input=True)  # example how to forward input
        name_no_evaluator.next_node = name_node  # loop back to name input in case of no answer

        start_over_yes_evaluator = NodeResponseEvaluator("yes")
        n2 = InteractorNode("n2", messages.Say("Want to start over?"),
                            [start_over_yes_evaluator, NodeResponseEvaluator("no", next_node=name_node)])

        n1 = InteractorNode("n1", messages.Say("Another test: what is the result of 1+1"),
                            [NodeResponseEvaluator("a couple", next_node=name_node)],
                            input_fail_next=n2, endless=False, mumbling_repeat=[
                messages.Say("Think more out of the box."),
                messages.Say("Didn't understand", append_input=True),
                messages.Say("One last try.")
            ])
        self.i = InteractorNode("n0", messages.Say(intro_line), [NodeResponseEvaluator(sentence, next_node=n1)],
                                timeout_in_s=5)  # 5s time to input something before it asks again
        start_over_yes_evaluator.next_node = n1  # create the loop

        self.evaluate = self._evaluate_intro
        if featureswitch.skip_intro:
            self.evaluate = self._evaluate  # hack to avoid intro
        self._time = time.time()
        self.pause = 1.0

    def _deactivate(self, text, publish_queue, proc_name, *args):
        self._activated = False
        send_message(publish_queue, messages.Say("Standing by."), proc_name)
        send_message(publish_queue, messages.HelpAiDeactivated(), proc_name)

    def _activate(self, text, publish_queue, proc_name, *args):
        self._activated = True
        send_message(publish_queue, messages.Say("At your service."), proc_name)
        send_message(publish_queue, messages.HelpAiActivated(), proc_name)

    def _evaluate(self, msg, publish_queue, proc_name):
        # process commands if activated
        if isinstance(msg, messages.SpeechRecorded):
            if self._activated:
                self.active_command_evaluator.evaluate(msg, publish_queue, proc_name)
            else:
                self.inactive_command_evaluator.evaluate(msg, publish_queue, proc_name)

    def _evaluate_intro(self, msg, publish_queue, proc_name):
        if isinstance(msg, messages.HelpAiSayIntro):
            self.i.say(publish_queue, proc_name)
            send_message(publish_queue, messages.HelpAiActivated(), proc_name)
        else:
            result = self.i.evaluate(publish_queue, proc_name, msg)
            if result:
                if self.i.matched_evaluator:
                    next_node = self.i.matched_evaluator.next_node
                    if next_node:
                        if self.i.node_id == "name":
                            additional_input = self.i.matched_evaluator.text  # example how to extract data
                        self.i = next_node
                        self.i.say(publish_queue, proc_name)
                    else:
                        # finished
                        pass
                else:
                    # done
                    self.evaluate = self._evaluate
                    self._deactivate(None, publish_queue, proc_name)

    def update(self, publish_queue, proc_name, dt):
        # todo: this didn't do what I expected - Gumm
        # if self.pause:
        #     time.sleep(self.pause)
        #     self.pause = 0.0

        # self.interaction.update(publish_queue, proc_name, dt)
        self.i.update(publish_queue, proc_name, dt)


logger.debug("imported")
