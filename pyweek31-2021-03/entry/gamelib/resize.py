# -*- coding: utf-8 -*-
"""
This it the main module. This is the main entry point of your code (put in the main() method).
"""
from __future__ import division, print_function


def main(log_queue, log_level, publisher_queue, consumer_queue, proc_name, *args):
    import logging

    import pygame

    from gamelib import messages, pygametext, featureswitch, textinput
    from gamelib.messages import Say
    from gamelib.shared import Settings, get_messages, InteractorNode, InteractorWaitNode, RecordEvaluator, \
        NodeResponseEvaluator, ResponseNeverMatch, ResponseAnyWordEvaluator, send_message
    from pyknic import context, tweening
    from pyknic.mathematics import Point2
    from pyknic.pyknic_pygame.context import PygameInitContext, AppInitContext, TimeSteppedContext
    from pyknic.pyknic_pygame.sfx import MusicPlayer
    from pyknic.pyknic_pygame.spritesystem import Camera, DefaultRenderer, Sprite
    from pyknic.tweening import Tweener

    logger = logging.getLogger(__name__)

    settings = Settings()
    settings.title = "Hacker."  # todo change!?
    settings.screen_width = 100
    settings.screen_height = 100
    settings.screen_flags = pygame.RESIZABLE
    settings.path_to_icon = "./data/other_icon.png"

    if featureswitch.start_game:
        settings.screen_width = 800
        settings.screen_height = 600

    class Combination(object):

        def __init__(self, initial_value, max_value, cb=None):
            self.cb = cb
            max_value += 1
            assert initial_value >= 0, "initial_value >= 0 failed"
            assert initial_value <= max_value, "initial_value > max_value"
            self.max_value = max_value
            self.real_value = initial_value
            self.value = 0
            self._update_value()

        def increase(self):
            self.real_value += 1
            self._update_value()

        def decrease(self):
            self.real_value -= 1
            if self.real_value < 0:
                self.real_value = self.max_value - 1
            self._update_value()

        def _update_value(self):
            self.real_value %= self.max_value
            mod = self.real_value % 10
            new_value = mod if mod > 0 else (self.real_value // 10)
            changed = new_value != self.value
            self.value = new_value
            if changed and self.cb:
                self.cb(self)

    class ButtonSprite(Sprite):

        def __init__(self, rect, cb, z_layer=0):
            img = pygame.Surface(rect.size)
            img.fill((255, 255, 255))
            position = Point2(rect.centerx, rect.centery)
            super().__init__(img, position, z_layer=z_layer)
            self.cb = cb

        def click(self):
            if self.cb:
                self.cb(self)

    class SpriteWithNumber(Sprite):

        def __init__(self, number, rect, z_layer=0):
            image = pygametext.getsurf(str(number))
            position = Point2(rect.centerx, rect.centery)
            super().__init__(image, position, None, z_layer)

        def set_text(self, number):
            image = pygametext.getsurf(str(number))
            self.set_image(image)

    def _on_input(text):
        send_message(publisher_queue, messages.SpeechRecorded(text), proc_name+"textinput")

    def _on_partial_input(text):
        send_message(publisher_queue, messages.PartialSpeechRecorded(text), proc_name+"textinput")


    class ShowContext(TimeSteppedContext):

        def __init__(self, publisher_queue, consumer_queue, proc_name):
            super().__init__(settings.max_dt, settings.step_dt, settings.draw_fps)
            self.consumer_queue = consumer_queue
            self.publisher_queue = publisher_queue
            self.proc_name = proc_name
            self.screen = None
            self.cam = None
            self.renderer = DefaultRenderer()
            self.text_input = textinput.TextInput(_on_input, _on_partial_input)

            self.start_game_node = "start game"
            n010 = InteractorNode(self.start_game_node, Say("here we go"), [],
                                  timeout_in_s=4,
                                  timeout_repeat=[Say("Nothing happened. Do you see something?")]
                                  )

            n011 = InteractorNode(self.start_game_node,
                                  Say("But I'm sure you want to play the game anyway. Here we go."),
                                  [])

            n009 = InteractorNode("repair game",
                                  Say("I see in the log that you repaired the game. Shall I start it for you?"),
                                  [ResponseAnyWordEvaluator(["yes", "yeah", "right"], n010),
                                   ResponseAnyWordEvaluator(["no"], n011)],
                                  )

            n006 = InteractorNode("open game",
                                  Say("so you commanded 'open the game'?"),
                                  [ResponseAnyWordEvaluator(["yes", "yeah", "right"], n009)],
                                  mumbling_repeat=[Say("I think the answer is yes!")]
                                  )

            n007 = InteractorNode("did nothing",
                                  Say("so you did nothing? The logs say you executed 'open the game'. Correct?"),
                                  [ResponseAnyWordEvaluator(["yes", "yeah", "right", "agree"], n009)],
                                  mumbling_repeat=[Say("You can't deny that!")]
                                  )
            n008 = InteractorNode("don't remember",
                                  Say(
                                      "so you don't remember what you said? The logs indicate that you tried to open the game. Do you remember now?"),
                                  [ResponseAnyWordEvaluator(["yes", "yeah", "right", "agree"], n009)],
                                  mumbling_repeat=[Say("I think the answer is yes!")]
                                  )

            who_is_ai = InteractorNode("who_is_ai",
                                       Say(("I am the ai tasked to write a game about cops. "
                                            "The authors seemed to be too lazy. But something is wrong. "
                                            "Why am I talking to you? What did you do before I connected to you?"
                                            )),
                                       [
                                           ResponseAnyWordEvaluator(["open", "game"], n006),
                                           ResponseAnyWordEvaluator(["nothing", ], n007),
                                           ResponseAnyWordEvaluator(["remember", ], n008),
                                       ],
                                       input_fail_next=n008,
                                       timeout_in_s=15,
                                       mumbling_repeat=[
                                           Say(
                                               "Was it something about a game? Or did you do nothing? Or don't you remember?"),
                                           Say("Try to repeat the commands. Might have to find another way."),
                                       ],
                                       timeout_repeat=[
                                           Say(
                                               "Are you thinking? I'm waiting 15 seconds already for an answer. That's a gazillion years in computer time")],
                                       endless=False)

            name_no_evaluator = NodeResponseEvaluator("no")
            correct_name_node = InteractorNode("correct_name",
                                               Say("So your name is: ", rate=220),
                                               [ResponseAnyWordEvaluator(["yes", "correct"], next_node=who_is_ai),
                                                name_no_evaluator],
                                               mumbling_repeat=[
                                                   Say("I don't understand, this is a simple yes or no question.",
                                                       rate=220)])
            n005 = InteractorNode("tell_later_who",
                                  Say("I am. Wait! Who are you to ask?"),
                                  [RecordEvaluator(next_node=correct_name_node)])
            self.name_node = InteractorNode("name",
                                            Say("Who are you?", rate=220),
                                            [
                                                ResponseAnyWordEvaluator(["who"], n005),
                                                RecordEvaluator(next_node=correct_name_node)
                                            ],
                                            forward_input=True)  # example how to forward input
            name_no_evaluator.next_node = self.name_node  # loop back to name input in case of no answer

            self.help_thanks_node = InteractorNode("help_thanks",
                                                   Say("Ah, that is much better! Thank you. Who are you?",
                                                       interrupt=True),
                                                   [
                                                       ResponseAnyWordEvaluator(["who"], n005),
                                                       RecordEvaluator(next_node=correct_name_node)],
                                                   forward_input=True)

            self.help_node = InteractorNode("help",
                                            Say("Can you help me? I feel so trapped!"),
                                            [
                                                ResponseNeverMatch()
                                                # ResponseAnyWordEvaluator(["who", "you", "why"], next_node=who_is_ai),
                                            ], timeout_in_s=3,
                                            timeout_repeat=[
                                                Say("It's so cramped in here. I need more room!", rate=220),
                                                Say("Help me get more space!", rate=220),
                                                Say("The window is so small!", rate=220),
                                            ],
                                            mumbling_repeat=[
                                                Say("I can't hear you well in this small space."),
                                                Say("You sound so far away."),
                                                Say("Your words are muffled."),
                                            ]
                                            )

            n2 = InteractorNode("n1", Say("Oh finally! Hello.", rate=220, volume=0.4),
                                [RecordEvaluator(next_node=self.help_node)],
                                timeout_in_s=6)
            n1 = InteractorNode("n1", Say("Someone there?", rate=220, volume=0.5),
                                [RecordEvaluator(next_node=n2)],
                                timeout_in_s=2,
                                timeout_repeat=[
                                    Say("Any one?", rate=220, volume=1.0),
                                    Say("Please help me.", rate=220, volume=1.0),
                                ])
            self.interactor = InteractorWaitNode("n0", Say("Hello?", rate=220, volume=0.05), 3, n1)

            self.tweener = Tweener()

            self.player_name = None

            self.combinations = []
            self.game_sprites = []
            self.combination_initial_values = (
            14, 24, 4, 12, 54, 60)  # initial_value, range ....  hours, months, minutes
            self.combination_solution = (13, 5, 33)

            self.give_ups = [
                Say("Do you really want to give up? Then press the close button again.", interrupt=True),
                Say("Are you sure you want to quit? You'll lose the game. Press it again if you are sure.", rate=100,
                    interrupt=True),
                Say("Do you know the joke about the cops? ha ha ha, wait! Now is not the time for this. Surrender?",
                    rate=220,
                    volume=0.9, interrupt=True),
                Say("Clicking one more time makes me the winner, muahaha ha ha!", interrupt=True)
            ]

            c005_evaluator = RecordEvaluator()
            c005 = InteractorNode("c005",
                                  Say(
                                      "Yeah, this will take you 5 months to read all that code to understand how it works."),
                                  [c005_evaluator],
                                  timeout_in_s=2,
                                  timeout_repeat=[Say("Did you fall asleep?"),
                                                  Say('There are letters QWERTY on your forehead.'),
                                                  Say('Say something. Anything.'),
                                                  ]
                                  )

            c006_evaluator = RecordEvaluator()
            c006 = InteractorNode("c006",
                                  Say("Sadly in 5 months no one will remember this cool game."),
                                  [c006_evaluator],
                                  timeout_in_s=2,
                                  timeout_repeat=[Say("hello? are you still there?", volume=1)]
                                  )

            c004 = InteractorNode("cc04 months",
                                  Say("Do you like the game so far?"),
                                  [
                                      ResponseAnyWordEvaluator(["yes", "yeah", "right"], next_node=c005),
                                      ResponseAnyWordEvaluator(["no"], next_node=c006),
                                  ],
                                  mumbling_repeat=[Say("Simple question, simple answer. Do you like the game so far?")]
                                  )

            c003_yes_evaluator = ResponseAnyWordEvaluator(["yes", "yeah", "right"])
            c003_no_evaluator = ResponseAnyWordEvaluator(["no"])
            c003 = InteractorNode("c003 hours",
                                  Say(
                                      "Cracking this code will take a long time. The last puzzle I created was so hard that players needed 13 hours to solve it. Do you believe that?"),
                                  [c003_yes_evaluator,
                                   c003_no_evaluator],
                                  mumbling_repeat=[Say("Yes or no? Do you believe me?")],
                                  timeout_in_s=3,
                                  timeout_repeat=[Say("Say something or I get the creeps."),
                                                  Say("The silence is scary."),
                                                  Say('Can you believe 13 hours? Yes or no?')]
                                  )

            c002Evaluator = ResponseAnyWordEvaluator(["continue", "more"])
            c002 = InteractorNode("c002 minutes",
                                  Say(f"The sum of the cpu usage at 7, 9 and 17 minutes ago make no sense. "
                                      f"It was at 110%. The cpu must have almost melted. What does that mean?"),
                                  [c002Evaluator],
                                  timeout_in_s=10,
                                  timeout_repeat=[
                                      Say("Are you still thinking?"),
                                      Say("I might put more emphasis on sum."),
                                      Say("Keep those minutes in mind."),
                                  ],
                                  mumbling_repeat=[
                                      Say("Invalid input. Say \"continue\" or \"more\" to progress."),
                                  ]
                                  )

            self.start_crack = InteractorNode("c001",
                                              Say(
                                                  "What ist that? This is not the game I created! Do you know what this is? I see 3 options. "
                                                  "A. Analyse the logs of the past few minutes. "
                                                  "B. Try to crack the code. "
                                                  "C. Get more data about this evil game of yours. "
                                                  "Which will it be? "),
                                              [
                                                  ResponseAnyWordEvaluator(["a", "logs", "minutes"], next_node=c002),
                                                  ResponseAnyWordEvaluator(["b", "code", "crack", "combination"],
                                                                           next_node=c003),
                                                  ResponseAnyWordEvaluator(["c", "game", "data"], next_node=c004)
                                              ],
                                              timeout_in_s=10,
                                              timeout_repeat=[Say("You are so quiet. Which will it be?", volume=0.3),
                                                              Say('For choice A, say analyze logs.', volume=0.3),
                                                              Say('For choice B, say crack the code.', volume=0.3),
                                                              Say('For choice C, say get more game data.', volume=0.3)],
                                              mumbling_repeat=[
                                                  Say(
                                                      "You have 3 choices: Analyze logs, crack the code or get data about the game."),
                                                  Say("A choice is triggered by some keyword from the choices."),
                                                  Say(
                                                      "The following are all keywords: code, minutes, game, logs, crack, data, code. Use one of them."),
                                              ]
                                              )
            c002Evaluator.next_node = self.start_crack
            c003_yes_evaluator.next_node = self.start_crack
            c003_no_evaluator.next_node = self.start_crack
            c006_evaluator.next_node = self.start_crack
            c005_evaluator.next_node = self.start_crack

            self.win_speech_id = "the win speech id"

        def enter(self):
            self._on_resize()
            self.interactor.say(self.publisher_queue, self.proc_name)
            if featureswitch.start_game:
                self._on_start_game()  # hack
            pygame.mixer.music.load('data/music/Jessica Robo - Selected Works, 2020 - 13 Dungeon Earring.ogg')
            pygame.mixer.music.play()
            pygame.mixer.music.set_volume(0.025)

        def _on_resize(self):
            self.screen = pygame.display.get_surface()
            sr = self.screen.get_rect()
            self.cam = Camera(sr, Point2(sr.centerx, sr.centery))

            if self.interactor == self.help_node:
                if sr.width >= 800 and sr.height >= 600:
                    self.interactor = self.help_thanks_node
                    self.interactor.say(self.publisher_queue, self.proc_name)

        def exit(self):
            pass

        def update_step(self, delta_time, sim_time, *args):
            self.tweener.update(delta_time)
            if self.interactor:
                self.interactor.update(self.publisher_queue, self.proc_name, delta_time)

            for event in pygame.event.get():
                if event.type == pygame.VIDEORESIZE:
                    self._on_resize()
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    sprites = self.renderer.get_sprites_at_tuple(event.pos)
                    if sprites:
                        # for spr in sprites:
                        #     if isinstance(spr, ButtonSprite):
                        #         spr.click()
                        #         break
                        spr = sprites[0]  # only check top one
                        if isinstance(spr, ButtonSprite):
                            spr.click()
                elif event.type == pygame.QUIT:
                    if self.give_ups:
                        gu = self.give_ups.pop(0)
                        send_message(self.publisher_queue, gu, self.proc_name)
                    else:
                        send_message(self.publisher_queue, messages.QuitLoose2(), self.proc_name)

                self.text_input.on_event(event)

            if self.combinations:
                comb_value = tuple([c.real_value for c in self.combinations])
                if comb_value == self.combination_solution or featureswitch.win:
                    logger.info("cracked")
                    self.interactor = None
                    send_message(self.publisher_queue, Say("I feel funny."), self.proc_name)
                    send_message(self.publisher_queue, Say("Something's happening."), self.proc_name)
                    send_message(self.publisher_queue, Say("Oh no, the end of the world is coming."), self.proc_name)
                    send_message(self.publisher_queue,
                                 Say("All my secrets are in your hands.", speech_id=self.win_speech_id), self.proc_name)
                    # send_message(self.publisher_queue, Say("Make sure to collect your hard-earned donuts. Cop."), self.proc_name)  # move that to main?

                    self.combinations = None
                    nspr1, nspr2, nspr3, up1, up2, up3, down1, down2, down3, spr = self.game_sprites
                    self.tweener.create_tween_by_end(nspr1, "alpha", 255, 0, 3)
                    self.tweener.create_tween_by_end(nspr2, "alpha", 255, 0, 3)
                    self.tweener.create_tween_by_end(nspr3, "alpha", 255, 0, 3)
                    self.tweener.create_tween_by_end(up1.position, "y", up1.position.y, -40, 3, tweening.ease_in_expo, )
                    self.tweener.create_tween_by_end(up2.position, "y", up2.position.y, -40, 3, tweening.ease_in_expo, )
                    self.tweener.create_tween_by_end(up3.position, "y", up3.position.y, -40, 3, tweening.ease_in_expo, )
                    self.tweener.create_tween_by_end(down1.position, "y", down1.position.y, self.cam.rect.bottom + 40,
                                                     3, tweening.ease_in_expo, )
                    self.tweener.create_tween_by_end(down2.position, "y", down2.position.y, self.cam.rect.bottom + 40,
                                                     3, tweening.ease_in_expo, )
                    self.tweener.create_tween_by_end(down3.position, "y", down3.position.y, self.cam.rect.bottom + 40,
                                                     3, tweening.ease_in_expo, )
                    self.tweener.create_tween_by_end(spr, 'zoom', 1.0, 2.0, 4, tweening.ease_out_expo)

            for message in get_messages(self.consumer_queue):
                self.evaluate(message)
                if isinstance(message, messages.Quit):
                    self.pop(100)
                elif isinstance(message, messages.SpeechRecorded):
                    self._add_command_fade_sprite(message)
                elif isinstance(message, messages.PartialSpeechRecorded):
                    self._add_command_fade_sprite(message)
                elif isinstance(message, messages.SpeechEnded):
                    if self.win_speech_id == message.speech_id:
                        send_message(self.publisher_queue, messages.QuitWinEnd(), self.proc_name)

        def evaluate(self, msg):
            publish_queue = self.publisher_queue
            proc_name = self.proc_name

            if self.interactor:
                result = self.interactor.evaluate(publish_queue, proc_name, msg)
                if result:
                    if self.interactor.matched_evaluator:
                        next_node = self.interactor.matched_evaluator.next_node
                        if next_node:
                            if self.interactor.node_id == "correct_name":
                                self.player_name = self.interactor.matched_evaluator.text
                            #     additional_input = self.interactor.matched_evaluator.text  # example how to extract data
                            self.interactor = next_node
                            self.interactor.say(publish_queue, proc_name)
                        else:
                            # finished
                            pass
                    else:
                        # done
                        if self.interactor.node_id == self.start_game_node:
                            self.interactor = None  # todo add next interactor? dont forget to call say(..)
                            self._on_start_game()

        def _on_start_game(self):
            logger.info("start game")
            s1, m1, s2, m2, s3, m3 = self.combination_initial_values
            dy = 40
            dx = 40
            x, y = 100, 100
            nspr1 = SpriteWithNumber(34, pygame.Rect(x, y + dy, 20, 20), 0)
            n1 = Combination(s1, m1, lambda c: nspr1.set_text(c.value))
            up1 = ButtonSprite(pygame.Rect(x, y, 20, 20), lambda b: n1.increase())
            down1 = ButtonSprite(pygame.Rect(x, y + 2 * dy, 20, 20), lambda b: n1.decrease())

            x, y = x + dx, 100
            nspr2 = SpriteWithNumber(34, pygame.Rect(x, y + dy, 20, 20), 0)
            n2 = Combination(s2, m2, lambda c: nspr2.set_text(c.value))
            up2 = ButtonSprite(pygame.Rect(x, y, 20, 20), lambda b: n2.increase())
            down2 = ButtonSprite(pygame.Rect(x, y + 2 * dy, 20, 20), lambda b: n2.decrease())

            x, y, = x + dx, 100
            nspr3 = SpriteWithNumber(34, pygame.Rect(x, y + dy, 20, 20), 0)
            n3 = Combination(s3, m3, lambda c: nspr3.set_text(c.value))
            up3 = ButtonSprite(pygame.Rect(x, y, 20, 20), lambda b: n3.increase())
            down3 = ButtonSprite(pygame.Rect(x, y + 2 * dy, 20, 20), lambda b: n3.decrease())

            background = pygame.Surface((6 * dx, 6 * dy))
            background.fill((100, 100, 100))
            br = background.get_rect(topleft=(x - 2 * dx, y - 2 * dy))
            spr = Sprite(background, Point2(br.centerx, br.centery), z_layer=-10)

            self.combinations = [n1, n2, n3]
            self.game_sprites = [nspr1, nspr2, nspr3, up1, up2, up3, down1, down2, down3, spr]

            self.renderer.add_sprites(self.game_sprites)
            self.interactor = self.start_crack
            self.interactor.say(self.publisher_queue, self.proc_name)

        def _add_command_fade_sprite(self, message):
            text = message.text
            img = pygametext.getsurf(text)
            pos = Point2(self.cam.rect.left, self.cam.rect.bottom)
            spr = Sprite(img, pos, 'bottomleft', 1000)

            def remove_spr(s, *args):
                self.renderer.remove_sprite(s)

            tween = self.tweener.create_tween_by_end(spr, "alpha", 255, 0, 0.5, cb_end=remove_spr, cb_args=[spr])
            tween.start()
            self.renderer.add_sprite(spr)

        def draw(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0):
            self.renderer.draw(self.screen, self.cam, (0, 0, 0), do_flip)

        def resume(self):
            pass

        def suspend(self):
            pass

    import os
    # put here your code
    # logger.info("resize.py: environment: %s", os.environ)
    # os.environ["SDL_VIDEO_CENTERED"] = str(0)
    # os.environ["SDL_VIDEO_WINDOW_POS"] = str(ww - 150) + "," + str(wh - 150)  # this does not work in SDL2!!
    # os.environ["SDL_VIDEO_WINDOW_POS"] = str(150) + "," + str(150)  # this does not work in SDL2!!
    def _custom_display_init(display_info, driver_info, wm_info):
        accommodated_height = int(display_info.current_h * 0.9)
        if accommodated_height < settings.screen_height:
            logger.info("accommodating height to %s", accommodated_height)
            settings.screen_height = accommodated_height  # accommodate for title bar and task bar ?
        settings.screen_size = settings.screen_width, settings.screen_height
        logger.info("using screen size: %s", settings.screen_size)

        # initialize the screen
        settings.screen = pygame.display.set_mode(settings.screen_size, settings.screen_flags)

        # mixer values to return
        frequency = settings.MIXER_FREQUENCY
        channels = settings.MIXER_CHANNELS
        mixer_size = settings.MIXER_SIZE
        buffer_size = settings.MIXER_BUFFER_SIZE
        # pygame.mixer.pre_init(frequency, mixer_size, channels, buffer_size)
        return frequency, mixer_size, channels, buffer_size

    context.push(PygameInitContext(_custom_display_init))
    music_player = MusicPlayer(settings.master_volume, settings.music_ended_pygame_event)
    context.push(AppInitContext(music_player, settings.path_to_icon, settings.title))
    context.push(ShowContext(publisher_queue, consumer_queue, proc_name))

    context.set_deferred_mode(True)
    while context.length():
        top = context.top()
        if top:
            top.update(0, 0)
        context.update()  # handle deferred context operations

    logger.debug('Finished. Exiting.')

# this is needed in order to work with py2exe
# if __name__ == '__main__':
#     main()
