Workin' For The Man
===================

CONTACT: See homepage for more information.

Homepage: https://pyweek.org/e/GD/
Name: Workin' For The Man
Team: GD
Members:  DR0ID, Gummbum


DEPENDENCIES
============

-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

    IMPORTANT: This game uses a speech-to-text library. A microphone is required to play. The speech is translated
    locally; it is NOT uploaded to any services on the Internet, and is not stored in files; speech is processed in
    buffers and local game code only. Please read HOW TO PLAY THE GAME for tips on playing and speaking. An English
    dictionary is used. Non-English speakers, sorry; we did not have the resources to support additional languages.
    Feel free to mark this game DNW (Does Not Work) if you're unable to play due to any of these requirements.

-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

You might need to install some of these before running the game:

  Python (3.7, 3.8):  http://www.python.org/
  PyGame (2.0.0):     http://www.pygame.org/
  vosk:               https://alphacephei.com/vosk/install
  pyttsx3:            https://github.com/nateshmbhat/pyttsx3
  sounddevice:        https://python-sounddevice.readthedocs.io/en/0.4.1/installation.html

  Note: other versions not tested. Still, it may work.

pip install -r requirements.txt

or

python -m pip install -r requirements.txt

or

py -x -m pip install -r requirements.txt

(But ya doesn't hasta call me Johnson. :))

Linux (tested with Ubuntu) following might be needed too:

sudo apt update && sudo apt install espeak ffmpeg libespeak1
sudo apt install libportaudio2

MICRO NOT WORKING or you can't install vosk:

There is a fallback mode where you can type in the answers (just type into the window and press enter to submit).
Just wait until the question has finished. And if it seems to do nothing type something.

RUNNING THE GAME
================

On Windows or Mac OS X, locate the "run.py" file and double-click it.

Otherwise open a terminal / console and "cd" to the game directory and run:

  python run.py

  -or in some cases-

  py3 run.py

Note: You may need to: pip install pygame.


HOW TO PLAY THE GAME
====================

Story:

Workin' For The Man is a futuristic interactive computer simulation. The game puts you in the role of a crack Computer
Forensics specialist in the local police bureau. Day in and day out it is your lot to pillage an endless train of
broken and booby-trapped electronic devices and scour them for admissible evidence against the purveyors of vice and
graft. Today is no different. The smashed heap unceremoniously dumped on your table by a mirror-eyed, burly COP clone
is an inert hulk of metal, plastic, and rare earth for you to reassemble, jolt back to life, and torment for its
secrets. "Do yer thing, doc," he jokes without humor. Some days it's a rewarding job, most others a curse to scratch
your eyes out for. What'll it be today?

Details:

When you start the game you will see a boot screen. This puts you right into the egghead cop role, trying to bust in to
a perp's computer. The yellow messages remind you to set up a working microphone. If you need to set up a mic at this
point, you'll have to kill the game processes to take care of that. See below, "Tip 5: Killing the game."

After the simulated computer boots, you'll be greeted by the governing AI named Roger. It looks like this job will be
easier than most. Roger seems to think the computer is at factory default. As a result, Roger welcomes you and gives
the lay of the land and even offers to help you ransack his domain. Listen carefully to Roger's spiel.

Roger explains the basics of how to interact with him, how to get help, and how to operate the machine. Then you're on
your own to ferret the hidden secrets out of the friendly user interface in time for a tall cold one.

Tip 1: A dedicated condenser mic is ideal, but not everyone has one. If you are using a headset or a builtin mic with
speakers, it is possible, even likely, that the game audio will malfunction: game sounds will feed back into the mic.
This will result in the game looping, as it mistakes Roger's voice for yours. If it isn't possible to adjust the audio
devices to defeat this problem (as was the case for Gumm), you can perhaps work around this in a couple ways:
1) if you're using a builtin mic, playback through headphones instead of speakers;
2) if you're using a headset, mute the mic when the computer is not prompting you to speak.
3) if all fails, mute the microphone and just type in the answer and press enter to submit it (it will highlight
    as it would have come from the speech recognition).

Tip 2: When speaking a reply or command, speak slowly and enunciate. You can watch the speech-to-text work out your
words. The words are displayed in the lower left corner. This may help you figure out what sounds you're making that
confuse the translator.

Tip 3: If you get lost in the space-age user interface, don't despair. It happens to the best of us. Here are the must-
have commands to get more out of your brainy computer.
1) Roger, Hello: this command alerts Roger to start taking your commands. The idiot light in the bottom right corner
   displays Roger's status, either "Roger: Active" or "Roger: Inactive". When inactive, Roger will ignore you;
   because you're probably singing in the shower or something else that doesn't interest him. Freebie: turn Roger off
   when you don't need him; it's really creepy to hear coming from another room saying, "Hello? Is someone there?"
2) Help: this command opens the document on command language. Help is usable anywhere, whether Roger is active or not.
3) Open [thing]: This is a versatile command. You can open a spinach can with it; I mean, if you can't squeeze it open.
   Say "open home" to get to home screen; "open messages" to view the list of messages; "open message 1" to view
   message #1. The message commands are examples of commands that function only in the proper context, of course.
4) Exterminate: If things get too rough for you, say "exterminate" to escape Roger's death grip on your fragile mind.
   It is most important to say this just like a Dalek.

Tip 4: When you blunder into the surprise second part of the game (you'll know what I'm talking about), have fun with
it. Give him the ol' Computer Forensics' homegrown genius Turing test.

Tip 5: Killing the game. The game is purposely hard to quit, to prevent the player from accidentally quitting. However,
if something goes wrong, you may need to kill the game. There are many ways to kill processes. This game uses Python's
subprocess module, so you need to get all of the processes. The following commands should get all the processes.
WARNING: These commands will kill ALL python processes that your account has permission to kill.
1) Windows: taskkill /IM "python.exe" /F.
2) Linux: killproc /usr/bin/python; or ps | awk 'BEGIN{IGNORECASE=1} /python/{print $1}' | xargs kill.
3) All platforms: there is a game.pid file in the game root that contains all the pids. Get those pids and use the
   tool of your choice to kill them. Make sure each is still a python process, as some kernels reuse pids.

Tip 6: Troubleshooting. Look around for random kids, dogs, parrots.

Tip 7: Leave a comment "I eat donuts" on the project page for a chance to be a winner.


LICENSE
=======

This game's skellington and other code have the same license as pyknic.

Sound and graphics assets are freely available, some with restrictions.
Attributions, where known, are in the various data directories.


WARNING - SPOILERS BELOW - WARNING - SPOILERS BELOW - WARNING - SPOILERS BELOW - WARNING - SPOILERS BELOW
WARNING - SPOILERS BELOW - WARNING - SPOILERS BELOW - WARNING - SPOILERS BELOW - WARNING - SPOILERS BELOW
WARNING - SPOILERS BELOW - WARNING - SPOILERS BELOW - WARNING - SPOILERS BELOW - WARNING - SPOILERS BELOW

WARNING - SPOILERS BELOW vvv

WARNING - SPOILERS BELOW vvv

WARNING - SPOILERS BELOW vvv

WARNING - SPOILERS BELOW vvv

WARNING - SPOILERS BELOW vvv

WARNING - SPOILERS BELOW vvv

WARNING - SPOILERS BELOW vvv

WARNING - SPOILERS BELOW vvv

WARNING - SPOILERS BELOW vvv

WARNING - SPOILERS BELOW vvv

WARNING - SPOILERS BELOW vvv

WARNING - SPOILERS BELOW vvv

WARNING - SPOILERS BELOW vvv



Spoiler: Hint 1 ----------------------------------------------------------------------------------------------------


- what is 1 + 1: a couple



Spoiler: Hint 2 ----------------------------------------------------------------------------------------------------


Commands to access the damaged game:
- open games
- repair the game
- open the game



Spoiler: Hint 3 ----------------------------------------------------------------------------------------------------


To free the cramped AI:
- resize the screen to 800 x 600 px



Spoiler: Hint 4 ----------------------------------------------------------------------------------------------------


- for the evil combo lock, the clue for each number is in each of the three options:
  A) analyze logs; B) crack the code; C) more game data.



Spoiler: Hint 5 ----------------------------------------------------------------------------------------------------


- A) analyze logs, sum the hours, minutes, and seconds; the other options should be more obvious



Spoiler: Hint 6 ----------------------------------------------------------------------------------------------------


- this is the really devious part; when you set each tumbler to 9 and then click up, what comes next? there is a
  pattern. you'll have to experiment a little with each one.



Spoiler: Hint 7 ----------------------------------------------------------------------------------------------------


- the first tumbler only accommodates digits between 0 and 24.
- the second tumbler only accommodates digits between 0 and 12.
- the third tumbler only accommodates digits between 0 and 60.



Spoiler: Hint 8 and full solution -----------------------------------------------------------------------------------


- answer 13; increment the first tumbler until you see the sequence 9, 1(this means 10), 1, 2, to 3 (13). this clue was
  "13 hours" under option B/crack the code.
- answer 5; increment the second tumbler to 5. this clue was "5 months" under option C/get more game data.
- answer 33; increment the third tumbler until you see the sequence 9, 3(this means 30), 1, 2, 3 (33). this clue was "7,
  9, and 17 minutes ago" under option A/analyze logs; sum the numbers 7, 9, and 17 to get 33.






WARNING - SPOILERS ABOVE ^^^

WARNING - SPOILERS ABOVE ^^^

WARNING - SPOILERS ABOVE ^^^

WARNING - SPOILERS ABOVE ^^^

WARNING - SPOILERS ABOVE ^^^

WARNING - SPOILERS ABOVE ^^^

WARNING - SPOILERS ABOVE ^^^

WARNING - SPOILERS ABOVE ^^^

WARNING - SPOILERS ABOVE ^^^

WARNING - SPOILERS ABOVE ^^^

WARNING - SPOILERS ABOVE ^^^

WARNING - SPOILERS ABOVE ^^^

WARNING - SPOILERS ABOVE ^^^

WARNING - SPOILERS ABOVE - WARNING - SPOILERS ABOVE - WARNING - SPOILERS ABOVE - WARNING - SPOILERS ABOVE
WARNING - SPOILERS ABOVE - WARNING - SPOILERS ABOVE - WARNING - SPOILERS ABOVE - WARNING - SPOILERS ABOVE
WARNING - SPOILERS ABOVE - WARNING - SPOILERS ABOVE - WARNING - SPOILERS ABOVE - WARNING - SPOILERS ABOVE
