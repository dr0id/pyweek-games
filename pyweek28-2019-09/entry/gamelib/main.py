# -*- coding: utf-8 -*-
"""
This it the main module. This is the main entry point of your code (put in the main() method).
"""
from __future__ import division, print_function

import logging

import pygame

from gamelib.mission_manager import MissionManager
from pyknic.pyknic_pygame.context import PygameInitContext, AppInitContext

from pyknic.pyknic_pygame.sfx import MusicPlayer

from gamelib import settings
from gamelib.client.client import Client
# from gamelib.game_context import StartInfo
from gamelib.splash_context import SplashContext
from gamelib.intro_context import IntroContext
from gamelib.victory_context import VictoryContext
from gamelib.settings import cell_size
from pyknic import context
from pyknic.events import EventDispatcher
from pyknic.mathematics.geometry import SphereBinSpace2

logger = logging.getLogger(__name__)


def main():
    # put here your code
    import pyknic

    logging.getLogger().setLevel(settings.log_level)
    pyknic.logs.print_logging_hierarchy()

    context.push(PygameInitContext(_custom_display_init))
    music_player = MusicPlayer(settings.master_volume, settings.music_ended_pygame_event)
    settings.music_player = music_player
    context.push(AppInitContext(music_player, settings.path_to_icon, settings.title))

    # todo: implement intro and such?
    # todo: initialize info
    if settings.debug_context_shim:
        # noinspection PyUnresolvedReferences,PyProtectedMember
        from gamelib._custom_context import CustomContext
        space = SphereBinSpace2(cell_size)
        event_dispatcher = EventDispatcher()
        client = Client(music_player, space, event_dispatcher)
        mission_manager = CustomContext(client, space, event_dispatcher)
    else:
        mission_manager = MissionManager()
    
    splashscreen = SplashContext()
    introscreen = IntroContext()
    victoryscreen = VictoryContext()
    
    context.push(victoryscreen)
    context.push(mission_manager)
    if not settings.debug_bypass_intro:
        context.push(introscreen)
    if not settings.debug_bypass_splash:
        context.push(splashscreen)

    context.set_deferred_mode(True)
    while context.length():
        top = context.top()
        if top:
            top.update(0, 0)
        context.update()  # handle deferred context operations

    logger.debug('Finished. Exiting.')


def _custom_display_init(display_info, driver_info, wm_info):
    accommodated_height = int(display_info.current_h * 0.9)
    if accommodated_height < settings.screen_height:
        logger.info("accommodating height to {0}", accommodated_height)
        settings.screen_height = accommodated_height  # accommodate for title bar and task bar ?
    settings.screen_size = settings.screen_width, settings.screen_height
    logger.info("using screen size: {0}", settings.screen_size)

    # initialize the screen
    settings.screen = pygame.display.set_mode(settings.screen_size, settings.screen_flags)

    # mixer values to return
    frequency = settings.MIXER_FREQUENCY
    channels = settings.MIXER_CHANNELS
    mixer_size = settings.MIXER_SIZE
    buffer_size = settings.MIXER_BUFFER_SIZE
    # pygame.mixer.pre_init(frequency, mixer_size, channels, buffer_size)
    return frequency, mixer_size, channels, buffer_size


# this is needed in order to work with py2exe
if __name__ == '__main__':
    main()
