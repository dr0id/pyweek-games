# -*- coding: utf-8 -*-

"""
The pygame specific code related to the context module of pyknic.
"""

# Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage
#
#   +-- api change, probably incompatible with older versions
#   |     +-- enhancements but no api change
#   |     |
# major.minor[.build[.revision]]
#                |
#                +-|* 0 for alpha (status)
#                  |* 1 for beta (status)
#                  |* 2 for release candidate
#                  |* 3 for (public) release
#
# For instance:
#     * 1.2.0.1 instead of 1.2-a
#     * 1.2.1.2 instead of 1.2-b2 (beta with some bug fixes)
#     * 1.2.2.3 instead of 1.2-rc (release candidate)
#     * 1.2.3.0 instead of 1.2-r (commercial distribution)
#     * 1.2.3.5 instead of 1.2-r5 (commercial distribution with many bug fixes)

import logging

from pyknic.pyknic_pygame.context import effects

logger = logging.getLogger(__name__)
logger.debug("importing...")


__version__ = "1.0.1.0"
__author__ = "DR0ID (C) 2011"

__copyright__ = "Copyright 2011, Multiverse Factory team for Pyweek 12"
__credits__ = ["Cosmologicon", "Gummbum", "DR0ID"]
__license__ = "New BSD license"
__maintainer__ = "DR0ID"

__all__ = ["effects", "AppInitContext", "PygameInitContext", "TimeSteppedContext"]

# TODO: introduce a 'GameBaseContext' prepared with a scheduler, renderer, tweener...
from context_game_init import AppInitContext
from context_init_pygame import PygameInitContext
from context_timestep import TimeSteppedContext


logger.debug("imported")
