# README - PyWeek 34 #

See [pyweek 34](https://pyweek.org/34/) for more information. 

## Theme for Pyweek 34, September 2022 ##

*The Red Planet*

## Project RandomDice ##

PyWeek project: [RandomDice](https://pyweek.org/e/3420220904/)

Game title: The red planet: The planet of the King

RandomDice is a collaboration between DR0ID and Gummbum for pyweek 34.

### Setup ###

1. Install Python and make sure the python command is in your system or user path.
2. Install the requirements: pip install -r requirements.txt

Note: The game has been tested with Python 3.10.0 and pygame 2.1.2. Earlier versions may work. Before reporting bugs
please make sure your environment meets these minimum requirements.

### Playing the Game ###

*Flyer: DO YOU HAVE WHAT IT TAKES TO BE KING? Currently accepting applications on the Red Planet. Prove your mettle by
journeying to the Red Planet. No experience necessary. Qualified applicants must be able to read, write, speak, serve
and protect the people, smile at least 900 Watts, and travel by spaceship and teleport.*

Interacting with the game should be fairly intuitive.
* Select language options using the mouse.
* Select a planet on the solar system map using the mouse.
* For landing steer your spaceship using the cursor keys or asdf.
* Solve puzzles using the mouse.

### Contact and Requesting Help ###

Come visit [our PyWeek 34 Discord channel](https://discord.com/channels/622703017405317130/1018573675399745576) for
help with the game. This channel will be available during the judging period.
