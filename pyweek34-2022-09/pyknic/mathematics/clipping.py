# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file '__init__.py.py' is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Fast clipping algorithm for axis aligned bounding boxes (AABB).
"""
from __future__ import print_function

import logging
import math

from pyknic.mathematics import EPSILON

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2017"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["clip_segment_against_aabb", "is_segment_intersecting_aabb"]  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


def clip_segment_against_aabb(px, py, ex, ey, x_min, y_min, x_max, y_max):
    r"""
    Clips a line segment on an AABB (axis aligned bounding box).

    :param px:
        x-coordinate of the starting point of the segment
    :param py:
        y-coordinate of the starting point of the segment
    :param ex:
        x-coordinate of the end point of the segment
    :param ey:
        y-coordinate of the end point of the segment
    :param x_min:
        the minimal x coordinate of the AABB
    :param y_min:
        the minimal y coordinate of the AABB
    :param x_max:
        the maximal x coordinate of the AABB
    :param y_max:
        the maximal y coordinate of the AABB

    :Returns:
        A tuple like (bool, ux, uy, wx, wy)
        Start point and end point of the clipped line segment if there
        is a line segment within the axis aligned bounding box. Otherwise 
        returns none.
        

    Explanation::

        +------->       \|            |                                   ¦            ¦
        |       x        \            |                                   ¦            ¦
        |                |\           |                                   ¦            ¦
        |                | \          |                                   ¦         ^  ¦
        V y              |  \         |                                   ¦      N1 |  ¦
                         |   \(px,py) |                                   ¦         |  ¦
            (x_min,y_min)|    x       |                      (x_min,y_min)¦         |  ¦
            -------------+-----\------+-------------         -------------+======>-----+----------
                         |      \     |                                   |   U1       |
                         |       \    |                                   |            |
                         |        \   |                                   | U2      U4 |
                         |         \  |                                   V            V
                         |          \ |                                   ¦            ¦
                         |           \|                                   ¦            ¦
                         |            \                            <======¦            ¦=======>
                         |            |\                             N2   ¦            ¦   N4
                         |            | \                                 ¦   U3       ¦
            -------------+------------+--\----------         -------------+======>-----+----------
                         |   (x_max,  |   \                               ¦         |  ¦(x_max,y_max)
                         |     y_max) |    \                              ¦         |  ¦
                         |            |     \ (ex,ey)                     ¦      N3 |  ¦
                         |            |      x                            ¦         V  ¦
                         |            |       \                           ¦            ¦

                        segment intersecting the AABB                     normal vectors Ni and Ui vectors


        using a line in parametric form:

            P + t * V      where    P start point (px, py)
                                    t parameter in range [0, 1]
                                    V as direction vector (vx, vy) = (ex-px,ey-py)


        intersection of two parametric lines:

            P + t * V = Q + s * U

                =>   s = (V cross (P - Q)) / (V cross U) = (vx * (py - qy) - vy * (px - qx)) / (vx * uy - vy * ux)

                =>   t = (qy - py + s * uy) / vy


        the formulas for the 4 intersection points Xi with their ti parameters for the 4 sides of the AABB
        (because of the 0 in the U vector some terms cancel out):

            case 1: Q1 = (x_min, y_min)    U1 = (1, 0)    N1 = (0, -1)

                    t1 = (y_min - py) / vy
                    X1 = (px + vx/vy * (y_min - py), y_min)

            case 2: Q2 = (x_min, y_min)    U2 = (0, 1)    N2 = (-1, 0)

                    t2 = (x_min - px) / vx
                    X2 = (x_min, py + vy/vx * (x_min - px))

            case 3: Q3 = (x_min, y_max)    U3 = (1, 0)    N3 = (0, 1)

                    t3 = (y_max - py) / vy
                    X3 = (px + vx/vy * (y_max - py), y_max)

            case 4: Q4 = (x_max, y_max)    U4 = (0, 1)    N4 = (1, 0)

                    t2 = (x_max - px) / vx
                    X4 = (x_max, py + vy/vx * (x_max - px))

        there are two special cases:

            case 5: V parallel to x axis (vy = 0)

            case 6: V parallel to y axis (vx = 0)


        +------->       \|            |
        |       x        * X2         |
        |                |\           |
        |                | \ (px,py)  |
        V y              |  x         |
                         |   \        |
                         |    \ X1    |
            -------------+-----*------+-------------
            (x_min,y_min)|      \     |
                         |       \    |
                         |        \   |
                         |         \  |
                         |          \ |
                         |           \|
                         |            *  X4
                         |            |\
                         |            | \  X3
            -------------+------------+--*----------
                         |    (x_max, |   \
                         |     y_max) |    \
                         |            |     \ (ex,ey)
                         |            |      x
                         |            |       \

                    the 4 intersection points X1, X2, X3 and X4


            the line always has 4 intersection points, but only two are of interest:


                        (px,py)                                      (ex,ey)
            ------*--------x=======*================*=========*=========x--------------
                  X2               X1               X4        X3



                                    V = (vx,vy) = (ex-px, ey-py)
            ---------------x============================================>--------------
               t < 0       |            t in [0,1]                      |    t > 1



            an entry point is defined by: N dot V < 0
            en leave point is defined by: N dot V > 0

            the points of interest are:

                entry point: E = Xi where Xi = P + te * V where te = max(ti) where Ni dot Vi < 0 for i=1,2,3,4

                leave point: L = Xi where Xi = P + tl * V where tl = min(ti) where Ni dot Vi > 0 for i=1,2,3,4

            (in the example above the entry point is E = X1 and the leaving point is L = X4)


            finally decide if P and P + V are clipped or not:

                check special cases of parallel V first:

                if vy == 0: # V parallel x-axis:
                    if y_min <= py <= y_max:
                        if px > x_max or px + vx < x_min:
                            completely outside
                        if px < x_min:
                            Xe = (x_min, py)
                        elif px <= x_max:
                            Xe = (px, py)
                        if px + vx > x_max:
                            Xl = (x_max, py)
                        elif px + vx > x_min:
                            Xl = (px + vx, py)
                    else:
                        completely outside

                check analog for y-axis

                check entry points
                if te > 1 : segment totally outside
                if 0 <= te <= 1: P outside so use Xe as segment start point
                if te < 0: P might be inside (leaving point will check), use P as segment start point

                check if the intersection point Xe is actually outside


                if tl < 0 : segment totally outside
                if 0 <= tl <= 1: end point outside, use Xl as end point of segment
                if tl > 1 : end point might be inside (entry point will check), use P + V as segment end point

    """

    # can't handle inf values
    assert not math.isinf(px), "can't handle 'inf' for px"
    assert not math.isinf(py), "can't handle 'inf' for py"
    assert not math.isinf(ex), "can't handle 'inf' for ex"
    assert not math.isinf(ey), "can't handle 'inf' for ey"
    assert not math.isinf(x_min), "can't handle 'inf' for x_min"
    assert not math.isinf(y_min), "can't handle 'inf' for y_min"
    assert not math.isinf(x_max), "can't handle 'inf' for x_max"
    assert not math.isinf(y_max), "can't handle 'inf' for y_max"

    assert not math.isnan(px), "can't handle 'nan' for px"
    assert not math.isnan(py), "can't handle 'nan' for py"
    assert not math.isnan(ex), "can't handle 'nan' for ex"
    assert not math.isnan(ey), "can't handle 'nan' for ey"
    assert not math.isnan(x_min), "can't handle 'nan' for x_min"
    assert not math.isnan(y_min), "can't handle 'nan' for y_min"
    assert not math.isnan(x_max), "can't handle 'nan' for x_max"
    assert not math.isnan(y_max), "can't handle 'nan' for y_max"

    # this calculation could be saved if passing in the vector instead of the endpoint (??)
    vx = float(ex - px)
    vy = float(ey - py)

    # resulting points
    xx = px
    xy = py
    yx = ex
    yy = ey

    if EPSILON > vy > -EPSILON:
        # case 5, parallel to x axis
        if y_min <= py <= y_max:
            if vx < 0:
                if px < x_min:
                    return False, px, py, ex, ey
                elif px > x_max:
                    xx = x_max
                if ex > x_max:
                    return False, px, py, ex, ey
                elif ex < x_min:
                    yx = x_min
            else:
                if px > x_max:
                    return False, px, py, ex, ey
                elif px < x_min:
                    xx = x_min
                if ex < x_min:
                    return False, px, py, ex, ey
                elif ex > x_max:
                    yx = x_max
            return True, xx, xy, yx, yy
        else:
            # completely outside
            return False, px, py, ex, ey

    # case 6, parallel to y axis
    if EPSILON > vx > -EPSILON:
        if x_min <= px <= x_max:
            if vy < 0:
                if py < y_min:
                    return False, px, py, ex, ey
                elif py > y_max:
                    xy = y_max
                if ey > y_max:
                    return False, px, py, ex, ey
                elif ey < y_min:
                    yy = y_min
            else:
                if py > y_max:
                    return False, px, py, ex, ey
                elif py < y_min:
                    xy = y_min
                if ey < y_min:
                    return False, px, py, ex, ey
                elif ey > y_max:
                    yy = y_max
            return True, xx, xy, yx, yy
        else:
            # completely outside
            return False, px, py, ex, ey

    # N1 dot V -> entry point on U1 and leave point on U3
    if -vy < 0:
        # N2 dot V -> entry point on U2 and leave point on U4
        if -vx < 0:

            # entry points X1 and X2
            t1 = (y_min - py) / vy
            t2 = (x_min - px) / vx
            if t1 > t2:
                # X1 is entry point of interest
                # segment checks
                if t1 > 1:
                    # outside, entry point is 'after' the end point
                    return False, px, py, ex, ey
                elif 0.0 <= t1 <= 1.0:
                    # start point is outside, but it is intersecting
                    xx = px + t1 * vx
                    if xx < x_min or xx > x_max:
                        return False, px, py, ex, ey
                    xy = y_min
            else:
                # X2 is entry point of interest
                if t2 > 1:
                    # outside, entry point is 'after' the end point
                    return False, px, py, ex, ey
                elif 0.0 <= t2 <= 1.0:
                    # start point is outside, but it is intersecting
                    xx = x_min
                    xy = py + t2 * vy
                    if xy < y_min or xy > y_max:
                        return False, px, py, ex, ey

            # leave points X3 and X4
            t3 = (y_max - py) / vy
            t4 = (x_max - px) / vx
            if t3 < t4:
                # X3 is leave point of interest
                if t3 < 0:
                    # outside, leave point is 'before' start point
                    return False, px, py, ex, ey
                elif 0.0 <= t3 <= 1.0:
                    # end point is outside, start point intersecting
                    yx = px + t3 * vx
                    # if yx < x_min or yx > x_max:
                    # return (False, px, py, ex, ey)
                    yy = y_max
            else:
                # X4 is leave point of interest
                if t4 < 0:
                    return False, px, py, ex, ey
                elif 0.0 <= t4 <= 1.0:
                    yx = x_max
                    yy = py + t4 * vy
                    # if yy < y_min or yy > y_max:
                    # return (False, px, py, ex, ey)

        else:
            # N2 dot V -> entry point on U4 and leave point on U2

            # entry points X1 and X4
            t1 = (y_min - py) / vy
            t4 = (x_max - px) / vx
            if t1 > t4:
                # X1 is entry point of interest
                # segment checks
                if t1 > 1:
                    # outside, entry point is 'after' the end point
                    return False, px, py, ex, ey
                elif 0.0 <= t1 <= 1.0:
                    # start point is outside, but it is intersecting
                    xx = px + t1 * vx
                    if xx < x_min or xx > x_max:
                        return False, px, py, ex, ey
                    xy = y_min
            else:
                # X4 is entry point of interest
                if t4 > 1:
                    # outside, entry point is 'after' the end point
                    return False, px, py, ex, ey
                elif 0.0 <= t4 <= 1.0:
                    # start point is outside, but it is intersecting
                    xx = x_max
                    xy = py + t4 * vy
                    if xy < y_min or xy > y_max:
                        return False, px, py, ex, ey

            # leave points X3 and X2
            t2 = (x_min - px) / vx
            t3 = (y_max - py) / vy
            if t3 < t2:
                # X3 is leave point of interest
                if t3 < 0:
                    # outside, leave point is 'before' start point
                    return False, px, py, ex, ey
                elif 0.0 <= t3 <= 1.0:
                    # end point is outside, start point intersecting
                    yx = px + t3 * vx
                    # if yx < x_min or yx > x_max:
                    # return (False, px, py, ex, ey)
                    yy = y_max
            else:
                # X2 is leave point of interest
                if t2 < 0:
                    return False, px, py, ex, ey
                elif 0.0 <= t2 <= 1.0:
                    yx = x_min
                    yy = py + t2 * vy
                    # if yy < y_min or yy > y_max:
                    # return (False, px, py, ex, ey)

    elif -vy > 0:
        # N1 dot V -> entry point on U3 and leave point on U1
        if -vx < 0:
            # N2 dot V -> entry point on U2 and leave point on U4

            # entry points X2 and X3
            t2 = (x_min - px) / vx
            t3 = (y_max - py) / vy
            if t3 > t2:
                # X3 is entry point of interest
                # segment checks
                if t3 > 1:
                    # outside, entry point is 'after' the end point
                    return False, px, py, ex, ey
                elif 0.0 <= t3 <= 1.0:
                    # start point is outside, but it is intersecting
                    xx = px + t3 * vx
                    if xx < x_min or xx > x_max:
                        return False, px, py, ex, ey
                    xy = y_max
            else:
                # X2 is entry point of interest
                if t2 > 1:
                    # outside, entry point is 'after' the end point
                    return False, px, py, ex, ey
                elif 0.0 <= t2 <= 1.0:
                    # start point is outside, but it is intersecting
                    xx = x_min
                    xy = py + t2 * vy
                    if xy < y_min or xy > y_max:
                        return False, px, py, ex, ey

            # leave points X1 and X4
            t1 = (y_min - py) / vy
            t4 = (x_max - px) / vx
            if t1 < t4:
                # X1 is leave point of interest
                if t1 < 0:
                    # outside, leave point is 'before' start point
                    return False, px, py, ex, ey
                elif 0.0 <= t1 <= 1.0:
                    # end point is outside, start point intersecting
                    yx = px + t1 * vx
                    # if yx < x_min or yx > x_max:
                    # return (False, px, py, ex, ey)
                    yy = y_min
            else:
                # X4 is leave point of interest
                if t4 < 0:
                    return False, px, py, ex, ey
                elif 0.0 <= t4 <= 1.0:
                    yx = x_max
                    yy = py + t4 * vy
                    # if yy < y_min or yy > y_max:
                    # return (False, px, py, ex, ey)

        else:
            # N2 dot V -> entry point on U4 and leave point on U2

            # entry points X1 and X4
            t3 = (y_max - py) / vy
            t4 = (x_max - px) / vx
            if t3 > t4:
                # X1 is entry point of interest
                # segment checks
                if t3 > 1:
                    # outside, entry point is 'after' the end point
                    return False, px, py, ex, ey
                elif 0.0 <= t3 <= 1.0:
                    # start point is outside, but it is intersecting
                    xx = px + t3 * vx
                    if xx < x_min or xx > x_max:
                        return False, px, py, ex, ey
                    xy = y_max
            else:
                # X4 is entry point of interest
                if t4 > 1:
                    # outside, entry point is 'after' the end point
                    return False, px, py, ex, ey
                elif 0.0 <= t4 <= 1.0:
                    # start point is outside, but it is intersecting
                    xx = x_max
                    xy = py + t4 * vy
                    if xy < y_min or xy > y_max:
                        return False, px, py, ex, ey

            # leave points X3 and X2
            t1 = (y_min - py) / vy
            t2 = (x_min - px) / vx
            if t1 < t2:
                # X3 is leave point of interest
                if t1 < 0:
                    # outside, leave point is 'before' start point
                    return False, px, py, ex, ey
                elif 0.0 <= t1 <= 1.0:
                    # end point is outside, start point intersecting
                    yx = px + t1 * vx
                    # if yx < x_min or yx > x_max:
                    # return (False, px, py, ex, ey)
                    yy = y_min
            else:
                # X2 is leave point of interest
                if t2 < 0:
                    return False, px, py, ex, ey
                elif 0.0 <= t2 <= 1.0:
                    yx = x_min
                    yy = py + t2 * vy
                    # if yy < y_min or yy > y_max:
                    # return (False, px, py, ex, ey)

    return True, xx, xy, yx, yy

    # (bool, sx, sy, ex, ey)


def is_segment_intersecting_aabb(px, py, ex, ey, x_min, y_min, x_max, y_max):
    r"""

    Clips a line segment on an AABB (axis aligned bounding box).

    :param px:
        x-coordinate of the starting point of the segment
    :param py:
        y-coordinate of the starting point of the segment
    :param ex:
        x-coordinate of the end point of the segment
    :param ey:
        y-coordinate of the end point of the segment
    :param x_min:
        the minimal x coordinate of the AABB
    :param y_min:
        the minimal y coordinate of the AABB
    :param x_max:
        the maximal x coordinate of the AABB
    :param y_max:
        the maximal y coordinate of the AABB

    :Returns:
        A tuple like (bool, ux, uy, wx, wy)
        Start point and end point of the clipped line segment if there
        is a line segment within the axis aligned bounding box. Otherwise 
        returns none.
        

    Explanation::
    
    
        This method uses the separating axis theorem (SAT).
    
    
                                                    X
            +---.----.--.---------------.------------>
            |   '    '  '               '
            -. .'. . x  '               '
            |   '   /   '               '
            |   '  /    '               '
            -. .'./. . .+---------------+
            |   '/     ´|              ´|
            -. .x     ´ |             ´ |
            |  ´     ´  |            ´  |
          \ -.´. . .´. .+-----------´---+
           \|´     ´   ´           ´   ´
            ´     ´   ´           ´   ´
            |\   ´   ´           ´   ´
            | \ ´   ´           ´   ´
            |  ´   ´           ´   ´
            |   \ ´           ´   ´
            |    ´           ´   ´
            |     \         ´   ´
         Y  v      \       ´   ´
                    \     ´   ´
                     \   ´   ´
                      \ ´   ´
                       ´   ´
                        \ ´
                         ´
                          \
                  
    SAT works as follows::
        
        The corners are projected onto the axis given by the normals.
        If any axis has no intersection, then they do not intersect.
        
    
    
    """
    # can't handle inf values
    assert not math.isinf(px), "can't handle 'inf' for px"
    assert not math.isinf(py), "can't handle 'inf' for py"
    assert not math.isinf(ex), "can't handle 'inf' for ex"
    assert not math.isinf(ey), "can't handle 'inf' for ey"
    assert not math.isinf(x_min), "can't handle 'inf' for x_min"
    assert not math.isinf(y_min), "can't handle 'inf' for y_min"
    assert not math.isinf(x_max), "can't handle 'inf' for x_max"
    assert not math.isinf(y_max), "can't handle 'inf' for y_max"

    assert not math.isnan(px), "can't handle 'nan' for px"
    assert not math.isnan(py), "can't handle 'nan' for py"
    assert not math.isnan(ex), "can't handle 'nan' for ex"
    assert not math.isnan(ey), "can't handle 'nan' for ey"
    assert not math.isnan(x_min), "can't handle 'nan' for x_min"
    assert not math.isnan(y_min), "can't handle 'nan' for y_min"
    assert not math.isnan(x_max), "can't handle 'nan' for x_max"
    assert not math.isnan(y_max), "can't handle 'nan' for y_max"

    # x axis
    if ex > px:
        if ex < x_min:
            return False
        elif px > x_max:
            return False
    else:
        if px < x_min:
            return False
        elif ex > x_max:
            return False

    # y axis
    if ey > py:
        if ey < y_min:
            return False
        elif py > y_max:
            return False
    else:
        if py < y_min:
            return False
        elif ey > y_max:
            return False

    # normal axis
    nx = float(ey - py)
    ny = float(px - ex)

    l_sq = nx * nx + ny * ny

    if l_sq == 0:
        # point is inside aabb
        return True

    # project aabb onto normal axis
    # return self.dot(other) / other.length_sq * other
    # p1 = (nx * vx + ny + vy) / l_sq
    x_min_px_nx = (x_min - px) * nx
    x_max_px_nx = (x_max - px) * nx
    y_min_py_ny = (y_min - py) * ny
    y_max_py_ny = (y_max - py) * ny
    # actually the projections would need a division by l_sq
    # but this is only a scaling and does not change the 
    # sign nor the order so we can save these cpu cycles
    # also determine the max and min points
    p1 = x_min_px_nx + y_min_py_ny
    p4 = x_max_px_nx + y_max_py_ny
    if p1 > p4:
        p1, p4 = p4, p1

    p = x_min_px_nx + y_max_py_ny
    if p > p4:
        p4 = p
    elif p < p1:
        p1 = p

    p = x_max_px_nx + y_min_py_ny
    if p > p4:
        p4 = p
    elif p < p1:
        p1 = p

    if p1 > 0:
        return False
    if p4 < 0:
        return False

    # here we have intersection!!
    # either it is within the aabb 
    # or it intersects one side of the aabb
    # or it intersects both sides
    return True


logger.debug("imported")
