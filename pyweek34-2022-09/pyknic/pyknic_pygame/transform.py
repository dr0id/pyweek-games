# -*- coding: utf-8 -*-

"""
This module contains transformations involving pygame.Surface.
"""
from __future__ import print_function, division

import base64
import logging
import time
import timeit
import zlib
from typing import NamedTuple

import pygame

logger = logging.getLogger(__name__)
logger.debug("importing...")


# __all__ = [""]

class ColorFactors(NamedTuple):
    red_factor: float
    green_factor: float
    blue_factor: float

    @property
    def _sum(self):
        return self.red_factor + self.green_factor + self.blue_factor

    def check(self):
        assert self._sum < 1.0 + 0.005, f"color factors {self} summed should be 1.0 but are {self._sum}"
        assert self._sum > 1.0 - 0.005, f"color factors {self} summed should be 1.0 but are {self._sum}"

        assert sum(self.to_color_tuple()) <= 255, f"color tuple {self.to_color_tuple()} summed should be 255"

    def to_color_tuple(self):
        return int(round(self.red_factor * 255)), int(round(self.green_factor * 255)), int(
            round(self.blue_factor * 255))


hdtv_factors = ColorFactors(0.2126, 0.7152, 0.0722)
pal_factors = ColorFactors(0.299, 0.587, 0.114)
hdr_factors = ColorFactors(0.2627, 0.6780, 0.0593)

hdtv_factors.check()
pal_factors.check()
hdr_factors.check()

GREY_PALETTE = [(i, i, i) for i in range(255)]


def to_grey_scale_accurate(color_surf, color_factors=hdtv_factors,
                           dest=None):
    if dest is None:
        dest = color_surf.copy()

    assert dest is not color_surf, "dest and surf are the same! they should not!"
    assert dest.get_size() == color_surf.get_size(), "surfaces have not same dimensions!"

    r_factor = color_factors.red_factor
    g_factor = color_factors.green_factor
    b_factor = color_factors.blue_factor

    sw, sh = color_surf.get_size()
    color_surf_get_at = color_surf.get_at
    dest_set_at = dest.set_at
    color_surf.lock()
    dest.lock()
    for y in range(sh):
        for x in range(sw):
            c = color_surf_get_at((x, y))
            grey = int(round(c[0] * r_factor + c[1] * g_factor + c[2] * b_factor))
            grey = grey if grey <= 255 else 255
            dest_set_at((x, y), (grey, grey, grey, c[3] if len(c) == 4 else 255))
    dest.unlock()
    color_surf.unlock()
    return dest


def to_grey_scale(color_surf, color_factors=hdtv_factors, dest=None, palette=GREY_PALETTE, intensity=3):
    temp1 = color_surf.copy()
    # this adjusts the color values
    temp1.fill(color_factors.to_color_tuple(), None, pygame.BLEND_RGB_MULT)

    if dest is None:
        dest = pygame.Surface(color_surf.get_size(), 0, 8)
        dest.set_palette(palette)

    assert dest is not color_surf, "dest and surf are the same! they should not!"
    assert dest.get_size() == color_surf.get_size(), "surfaces have not same dimensions!"

    # preserve alpha
    alpha = color_surf.copy()
    alpha.fill((0, 0, 0, 255), None, pygame.BLEND_RGBA_MULT)

    # blit 3 times because a color blit to a 8 bit surface does (r+g+b) / 3
    # this is not that accurate, but almost accurate
    dest.blit(temp1, (0, 0), None, pygame.BLEND_RGB_ADD)

    for n in range(intensity):
        alpha.blit(dest, (0, 0), None, pygame.BLEND_RGB_ADD)
    # alpha.blit(dest, (0, 0), None, pygame.BLEND_RGB_ADD)
    # alpha.blit(dest, (0, 0), None, pygame.BLEND_RGB_ADD)
    return alpha


def image_to_string(image, img_format='RGBA', level=9):
    """
    Converts the image into a compressed, base64 encoded string.
    """
    str_img = pygame.image.tostring(image, img_format)
    img_w, img_h = image.get_size()
    str_img = '|'.join((img_format, str(img_w), str(img_h), str_img))
    if level:
        str_img = zlib.compress(str_img, level)
    return base64.b64encode(str_img)


def string_to_image(input_str, flipped=False):
    """
    Converts a string encoded previously with :py:func:`image_to_string()` back to an image.
    """
    input_str = base64.b64decode(input_str)
    input_str = zlib.decompress(input_str)
    img_format, img_w, img_h, str_img = input_str.split('|', 3)
    size = (int(img_w), int(img_h))
    return pygame.image.fromstring(str_img, size, img_format, flipped)


#   http://www.akeric.com/blog/?p=720

def blur_surf(surface, amount_x, amount_y=None, border=None, background=(0, 0, 0, 0)):
    """
    From: http://www.akeric.com/blog/?p=720

    Blur the given surface by the given 'amount'.  Only values 1 and greater
    are valid.  Value 1 = no blur.

    """

    amount_y = amount_y or amount_x  # use x value if y is not set

    if amount_x < 1.0:
        raise ValueError("Arg 'amount_x' must be greater than 1.0, passed in value is %s" % amount_x)
    if amount_y < 1.0:
        raise ValueError("Arg 'amount_y' must be greater than 1.0, passed in value is %s" % amount_y)

    width, height = surface.get_size()
    if border:
        width += 2 * border
        height += 2 * border
        border_surface = pygame.Surface((width, height), pygame.SRCALPHA)
        border_surface.fill(background)
        border_surface.blit(surface, (border, border))
        surface = border_surface

    scale_x = 1.0 / float(amount_x)
    scale_y = 1.0 / float(amount_y)
    min_pixel = 1
    scale_size = (int(round(max(min_pixel, width * scale_x))), int(round(max(min_pixel, height * scale_y))))
    # print '? scale', scale_x, scale_y, 'scale size:', scale_size

    # surf = pygame.transform.scale(surface, scale_size)
    surf = pygame.transform.smoothscale(surface, scale_size)
    return pygame.transform.smoothscale(surf, (width, height))

    # surf = pygame.transform.rotozoom(surface, 0, scale_x)
    # w, h = surf.get_size()
    # print 'new', width / float(w), 'amount', amount_x
    # return pygame.transform.rotozoom(surf, 0, width / float(w))
    # # return pygame.transform.rotozoom(surf, 0, amount_x)


def box_blur(surface, box_size, iterations=1, border=None, background=(0, 0, 0, 0)):
    """
    This is a box blur function.
    :param surface: The surface to blur.
    :param box_size: The size of the box to use to blur.
    :param border: The border to add.
    :param background: The background color to use for the border.
    :param iterations: Number of iterations.
    :return: Blurred image.
    """
    bw, bh = box_size
    box = pygame.Rect(0, 0, bw, bh)
    surf_rect = surface.get_rect()
    dest = surface.copy()
    other = surface.copy()
    for i in range(iterations):
        for x in range(surf_rect.width):
            for y in range(surf_rect.height):
                box.center = (x, y)
                c = pygame.transform.average_color(other, box)
                dest.set_at(box.center, c)
        other = dest
    return dest


logger.debug("imported")

if __name__ == '__main__':

    def draw_blurs(blurs, w, h, radius, screen, original, count, show_rings):
        screen.fill((0, 0, 0))
        screen.blit(original, (0, 0))
        info = f"size: {w}, {h} radius: {radius}"
        label = font.render(info, True, (255, 255, 255))
        screen.blit(label, (0, 0))
        for idx, bt in enumerate(blurs):
            b, l = bt
            x = idx % count * w
            y = idx // count * h + h
            screen.blit(b, (x, y))
            screen.blit(l, (x, y))
            if show_rings:
                pygame.draw.circle(screen, (255, 255, 255), (x + w // 2, y + h // 2), radius, 1)
        pygame.display.flip()


    def box_blur_demo(screen, font):
        pygame.display.set_caption("box blur demo - be patient - 'r' to toggle rings")
        w = 100
        h = 100
        radius = 20
        original = pygame.Surface((w, h))
        pygame.draw.circle(original, (255, 255, 255), original.get_rect().center, radius)

        box_sizes = [(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (11, 11), (21, 21), (42, 42), (50, 50), (31, 5), (3, 31)]
        iterations_to_test = [1, 2, 3, 4, 5, 6]

        blurs = []
        for it in iterations_to_test:
            for bw, bh in box_sizes:
                start = time.time()
                blurred = box_blur(original, (bw, bh), iterations=it)
                diff = time.time() - start
                # pygame.draw.circle(blurred, (255, 255, 255), original.get_rect().center, radius, 1)
                info = f"box:{bw}x{bh} itr:{it}"
                label = font.render(info, True, (255, 255, 255))
                print(f"{info} t:{diff:.3}s")
                blurs.append((blurred, label))
                draw_blurs(blurs, w, h, radius, screen, original, len(box_sizes), False)
        show_rings = True
        running = True
        while running:
            draw_blurs(blurs, w, h, radius, screen, original, len(box_sizes), show_rings)
            for e in [pygame.event.wait()]:
                if e.type == pygame.QUIT:
                    running = False
                elif e.type == pygame.KEYDOWN:
                    if e.key == pygame.K_r:
                        show_rings = not show_rings
                    elif e.key == pygame.K_ESCAPE:
                        running = False


    def blur_demo(screen, font):
        pygame.display.set_caption("blur surf demo - be patient - 'r' to toggle rings")
        w = 100
        h = 100
        radius = 20
        original = pygame.Surface((w, h))
        pygame.draw.circle(original, (255, 255, 255), original.get_rect().center, radius)

        amounts = [
            # (1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (11, 11), (21, 21), (42, 42), (50, 50), (31, 5), (3, 31),
            (1, 1), (1.2, 1.2), (1.4, 1.4), (1.6, 1.6), (1.8, 1.8),
            (2, 2), (2.2, 2.2), (2.4, 2.4), (2.6, 2.6), (2.8, 2.8),
            (3, 3), (3.2, 3.2), (3.4, 3.4), (3.6, 3.6), (3.8, 3.8),
            (4, 4), (4.2, 4.2), (4.4, 4.4), (4.6, 4.6), (4.8, 4.8),
            (5, 5), (5.2, 5.2), (5.4, 5.4), (5.6, 5.6), (5.8, 5.8),
            (11, 11), (11.2, 11.2), (11.4, 11.4), (11.6, 11.6), (11.8, 11.8),
            (21, 21), (21.2, 21.2), (21.4, 21.4), (21.6, 21.6), (21.8, 21.8),
            (42, 42), (42.2, 42.2), (42.4, 42.4), (42.6, 42.6), (42.8, 42.8),
            (50, 50), (50.2, 50.2), (50.4, 50.4), (50.6, 50.6), (50.8, 50.8),
        ]

        blurs = []
        for bw, bh in amounts:
            start = time.time()
            blurred = blur_surf(original, bw, bh)
            diff = time.time() - start
            # pygame.draw.circle(blurred, (255, 255, 255), original.get_rect().center, radius, 1)
            info = f"blur: ax:{bw}  ay:{bh}"
            label = font.render(info, True, (255, 255, 255))
            print(f"{info} t:{diff:.3}s")
            blurs.append((blurred, label))
            draw_blurs(blurs, w, h, radius, screen, original, 11, False)
        show_rings = True
        running = True
        while running:
            draw_blurs(blurs, w, h, radius, screen, original, 11, show_rings)
            for e in [pygame.event.wait()]:
                if e.type == pygame.QUIT:
                    running = False
                elif e.type == pygame.KEYDOWN:
                    if e.key == pygame.K_r:
                        show_rings = not show_rings
                    elif e.key == pygame.K_ESCAPE:
                        running = False


    def grey_scale_demo(screen, font):
        pygame.display.set_caption("demo - gray scale")
        # create colorful image
        height = 50
        width = 360
        color_image = pygame.Surface((width, height))
        color = pygame.Color(255, 255, 255, 255)
        step = 360 // 360
        for i in range(0, 360, step):
            color.hsva = i, 100, 100, 100
            color_image.fill(color, pygame.Rect(i, 0, step, height))

        number_of_samples = 10
        gray_scale_accurate = to_grey_scale_accurate(color_image)
        time_gray_scale_accurate = timeit.timeit(lambda: to_grey_scale_accurate(color_image), number=number_of_samples)
        gray_scale = to_grey_scale(color_image)
        time_gray_scale = timeit.timeit(lambda: to_grey_scale(color_image), number=number_of_samples)

        images = [
            (color_image, (font.render("colors", 1, (255, 255, 255)))),
            (gray_scale_accurate,
             (font.render(f"{time_gray_scale_accurate:.3}s to_grey_scale_accurate", 1, (255, 255, 255)))),
            (gray_scale, (font.render(f"{time_gray_scale:.3}s to_grey_scale", 1, (255, 255, 255))))
        ]

        running = True
        while running:
            for e in [pygame.event.wait()]:
                if e.type == pygame.QUIT:
                    running = False
                elif e.type == pygame.KEYDOWN:
                    if e.key == pygame.K_ESCAPE:
                        running = False

            screen.fill((0, 0, 0))
            # draw
            for idx, t in enumerate(images):
                img, label = t
                screen.blit(img, (0, idx * height + 1))
                screen.blit(label, (width, idx * height + 1))
                pygame.draw.line(screen, (255, 255, 255), (0, idx * height), (2 * width, idx * height))

            pygame.display.flip()


    pygame.init()
    pygame.display.set_caption("demo - press numbers on keyboard")
    screen = pygame.display.set_mode((1280, 1024))
    font = pygame.font.Font(None, 15)

    labels = [
        (font.render("1 - box blur demo", 1, (255, 255, 255))),
        (font.render("2 - fast blur demo", 1, (255, 255, 255))),
        (font.render("3 - gray color demo", 1, (255, 255, 255)))
    ]
    demos = [
        box_blur_demo,
        blur_demo,
        grey_scale_demo
    ]

    running = True
    while running:
        for e in [pygame.event.wait()]:
            if e.type == pygame.QUIT:
                running = False
            elif e.type == pygame.KEYDOWN:
                if e.key == pygame.K_ESCAPE:
                    running = False
                elif e.key == pygame.K_1 or \
                        e.key == pygame.K_2 or \
                        e.key == pygame.K_3:
                    index = e.key - pygame.K_1
                    demos[index](screen, font)

        pygame.display.set_caption("demo - press numbers on keyboard")
        screen.fill((0, 0, 0))
        for idx, label in enumerate(labels):
            screen.blit(label, (100, 100 + idx * 30))
        pygame.display.flip()
