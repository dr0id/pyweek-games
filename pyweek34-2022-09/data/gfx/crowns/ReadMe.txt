Crowns
by Pavel Kutejnikov

CC0 / Public domain

If you want to support me, you can add to your wishlist or buy my rogue-like game "Tzakol in Exile":
https://store.steampowered.com/app/1764840/Tzakol_in_Exile/

Good luck!