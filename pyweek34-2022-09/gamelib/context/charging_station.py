# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'charging_station.py' is part of pw-34
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The charging station level.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2022"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

import math

import pygame
from pygame import Vector2, Rect

import pyknic.pyknic_pygame.context.effects
from gamelib import settings, resources
from pyknic.mathematics.clipping import clip_segment_against_aabb
from pyknic.pyknic_pygame.context import TimeSteppedContext
from pyknic.timing import Scheduler

logger = logging.getLogger(__name__)
logger.debug("importing...")


class Beam:

    def __init__(self, start, direction, color, source):
        self.start: Vector2 = start
        self.direction: Vector2 = direction.normalize()
        self.end = start.copy()
        self.next = None
        self.color = color
        self.source = source
        self.index = 0

    # @property
    # def strength(self):
    #     return self.limit - self.index  # just an idea. Maybe base it on the total length!


class Source:
    limit = 500

    def __init__(self, position, direction, color, beam_offset=5, limit=None):
        self.position = position
        self.rect = Rect(0, 0, 20, 20)
        self.rect.center = position
        self.color = color
        self._beam_offset = beam_offset
        self.beam = Beam(position + direction * beam_offset, direction.copy(), color, self)
        self.limit = limit or Source.limit  # this could be used to limit the number of bounces!

    @property
    def orientation(self):
        return self.beam.direction.copy()

    @orientation.setter
    def orientation(self, value):
        self.beam.direction = value.normalize().copy()
        self.beam.start = self.position + self.beam.direction * self._beam_offset

    def move_to(self, pos):
        self.position.update(pos)
        self.rect.center = self.position  # needed for fast collision detection
        self.beam.start = self.position + self.beam.direction * self._beam_offset


class Target:

    def __init__(self, position, color):
        self.position = position
        self.rect = Rect(0, 0, 20, 20)
        self.rect.center = position
        self.color = color
        self.is_hit = False
        self.old_is_hit = False

    def update(self):
        self.old_is_hit = self.is_hit

    def move_to(self, pos):
        self.position.update(pos)
        self.rect.center = self.position  # needed for fast collision detection


class Mirror:

    def __init__(self, position, rotation_degrees: float, size):
        self._orientation: Vector2 = Vector2(1, 0).rotate(rotation_degrees)
        self.size = size
        self.position = position
        w, h = size
        radius = math.hypot(w / 2, h / 2) + 1
        self.rect = Rect(0, 0, 2 * radius, 2 * radius)
        self.rect.center = position  # needed for fast collision detection

    def move_to(self, pos):
        self.position.update(pos)
        self.rect.center = self.position  # needed for fast collision detection

    @property
    def rotation(self):
        return self._orientation.angle_to(Vector2(1, 0))

    @property
    def orientation(self):
        return self._orientation.copy()

    @orientation.setter
    def orientation(self, value: Vector2):
        self._orientation.update(value.normalize())

    @property
    def half_x_axis(self):
        return self._orientation.copy() * self.size[0] / 2

    @property
    def half_y_axis(self):
        return self._orientation.rotate(90) * self.size[1] / 2

    @property
    def x_axis(self):
        return self._orientation.copy()

    @property
    def y_axis(self):
        return self._orientation.rotate(90)


class Wall:

    def __init__(self, rect):
        self.rect = rect  # needed for fast collision detection
        self.position = Vector2(rect.center)

    def move_to(self, pos):
        self.rect.center = pos  # needed for fast collision detection
        self.position.update(self.rect.center)


class ChargingStation(TimeSteppedContext):

    def __init__(self, max_dt, step_dt, draw_fps, music_player, resource_manager, level_progress, sound_player):
        TimeSteppedContext.__init__(self, max_dt, step_dt, draw_fps)
        self.level_progress = level_progress
        self.walls = []
        self.sources = []
        self.targets = []
        self.mirrors: list[Mirror] = []
        self.screen = None
        self.focus = None
        self.mouse_pos = None
        self.focus_orientation = None
        self.dragging = None
        self.scheduler = Scheduler()

        self.sound_player = sound_player
        self.music_player = music_player
        self.resource_manager = resource_manager
        self.mouse_hover = None

    def enter(self):
        self.screen = pygame.display.get_surface()
        self.resume()

    def resume(self):
        pygame.mouse.set_visible(True)
        self.screen = pygame.display.get_surface()
        screen_width = settings.screen_width
        screen_height = settings.screen_height
        #         """
        #         ##### SAVED LEVEL ######
        #         Mirror(Vector2(500.0, 30.0), rotation_degrees=30, size=(30,30)),
        #         Mirror(Vector2(300.0, 500.0), rotation_degrees=30, size=(30,30)),
        #         Mirror(Vector2(599.0, 54.0), rotation_degrees=30, size=(30,30)),
        #         Mirror(Vector2(500.0, 500.0), rotation_degrees=30, size=(50,50)),
        #         Mirror(Vector2(100.0, 400.0), rotation_degrees=30, size=(50,50)),
        #         Mirror(Vector2(344.0, 367.0), rotation_degrees=30, size=(50,50)),
        #         Mirror(Vector2(797.0, 25.0), rotation_degrees=30, size=(10,10)),
        #         Mirror(Vector2(812.0, 124.0), rotation_degrees=30, size=(10,10)),
        #         Mirror(Vector2(779.0, 158.0), rotation_degrees=30, size=(10,10)),
        #         Mirror(Vector2(817.0, 193.0), rotation_degrees=30, size=(20,20)),
        #         Mirror(Vector2(785.0, 246.0), rotation_degrees=30, size=(20,20)),
        #         Mirror(Vector2(823.0, 292.0), rotation_degrees=30, size=(20,20)),
        #         Mirror(Vector2(800.0, 400.0), rotation_degrees=30, size=(100,100)),
        #         Source(Vector2(30.0, 30.0), direction=Vector2(0.9838763464746746, 0.17885003451955514), color=(0, 255, 0, 255)),
        #         Source(Vector2(30.0, 718.0), direction=Vector2(0.9781237744684966, -0.20802375301753775), color=(255, 0, 0, 255)),
        #         Source(Vector2(150.0, 400.0), direction=Vector2(0.8692080629404937, 0.4944465019789652), color=(0, 0, 255, 255)),
        #         Target(Vector2(800.0, 600.0), color=(0, 255, 0, 255)),
        #         Target(Vector2(600.0, 600.0), color=(255, 0, 0, 255)),
        #         Target(Vector2(500.0, 600.0), color=(0, 0, 255, 255)),
        #         Wall(Rect((0, 0), (1024, 10))),
        #         Wall(Rect((0, 0), (10, 768))),
        #         Wall(Rect((1014, 0), (10, 768))),
        #         Wall(Rect((0, 758), (1024, 10))),
        #         Wall(Rect((22, 85), (300, 10))),
        #         Wall(Rect((100, 400), (10, 100))),
        #         Wall(Rect((120, 100), (10, 50))),
        #         Wall(Rect((140, 100), (10, 50))),
        #         Wall(Rect((160, 100), (10, 80))),
        #         Wall(Rect((180, 100), (10, 80))),
        #         Wall(Rect((200, 100), (10, 100))),
        #         Wall(Rect((220, 100), (10, 100))),
        #         Wall(Rect((750, 40), (10, 300))),
        #         Wall(Rect((844, 17), (10, 300))),
        #         Wall(Rect((502, 223), (50, 10))),
        #         Wall(Rect((500, 120), (50, 10))),
        #         Wall(Rect((500, 140), (70, 10))),
        #         Wall(Rect((500, 160), (70, 10))),
        #         Wall(Rect((500, 180), (100, 10))),
        #         Wall(Rect((500, 200), (120, 10))),
        #         Wall(Rect((635, 692), (300, 10))),
        #         Wall(Rect((363, 85), (300, 10))),
        #         ##### SAVED LEVEL END ######
        #         """
        #
        #         """
        #
        #         1226620 [MainProcess 22288 MainThread 19712]: INFO     ##### SAVED LEVEL ###### [gamelib.context.charging_station: _handle_events in charging_station.py(503)]
        # Mirror(Vector2(689.0, 585.0), rotation_degrees=30, size=(30,30)),
        # Mirror(Vector2(657.0, 712.0), rotation_degrees=30, size=(30,30)),
        # Mirror(Vector2(426.0, 276.0), rotation_degrees=30, size=(30,30)),
        # Mirror(Vector2(173.0, 170.0), rotation_degrees=30, size=(50,50)),
        # Mirror(Vector2(100.0, 400.0), rotation_degrees=30, size=(50,50)),
        # Mirror(Vector2(121.0, 245.0), rotation_degrees=30, size=(50,50)),
        # Mirror(Vector2(807.0, 140.0), rotation_degrees=30, size=(10,10)),
        # Mirror(Vector2(506.0, 125.0), rotation_degrees=30, size=(10,10)),
        # Mirror(Vector2(459.0, 557.0), rotation_degrees=30, size=(10,10)),
        # Mirror(Vector2(404.0, 411.0), rotation_degrees=30, size=(20,20)),
        # Mirror(Vector2(781.0, 229.0), rotation_degrees=30, size=(20,20)),
        # Mirror(Vector2(301.0, 664.0), rotation_degrees=30, size=(20,20)),
        # Mirror(Vector2(595.0, 181.0), rotation_degrees=30, size=(100,100)),
        # Source(Vector2(161.0, 692.0), direction=Vector2(0.9995767297329896, 0.02909229066783597), color=(0, 255, 0, 255)),
        # Source(Vector2(167.0, 546.0), direction=Vector2(0.9994403700407545, 0.03345066117134085), color=(255, 0, 0, 255)),
        # Source(Vector2(126.0, 629.0), direction=Vector2(0.8050123228848883, -0.5932580888647675), color=(0, 0, 255, 255)),
        # Target(Vector2(769.0, 361.0), color=(0, 255, 0, 255)),
        # Target(Vector2(853.0, 403.0), color=(255, 0, 0, 255)),
        # Target(Vector2(937.0, 454.0), color=(0, 0, 255, 255)),
        # Wall(Rect((0, 0), (1024, 10))),
        # Wall(Rect((0, 0), (10, 768))),
        # Wall(Rect((1014, 0), (10, 768))),
        # Wall(Rect((0, 758), (1024, 10))),
        # Wall(Rect((22, 85), (300, 10))),
        # Wall(Rect((100, 400), (10, 100))),
        # Wall(Rect((248, 555), (10, 50))),
        # Wall(Rect((251, 641), (10, 50))),
        # Wall(Rect((160, 100), (10, 80))),
        # Wall(Rect((251, 727), (10, 80))),
        # Wall(Rect((200, 100), (10, 100))),
        # Wall(Rect((708, 594), (10, 100))),
        # Wall(Rect((249, 219), (10, 300))),
        # Wall(Rect((699, 151), (10, 300))),
        # Wall(Rect((267, 117), (50, 10))),
        # Wall(Rect((896, 159), (50, 10))),
        # Wall(Rect((891, 118), (70, 10))),
        # Wall(Rect((844, 78), (70, 10))),
        # Wall(Rect((101, 213), (100, 10))),
        # Wall(Rect((131, 508), (120, 10))),
        # Wall(Rect((106, 63), (300, 10))),
        # Wall(Rect((363, 85), (300, 10))),
        # ##### SAVED LEVEL END ###### [gamelib.context.charging_station: _handle_events in charging_station.py(514)]
        #
        #         """
        #
        #         """
        # Mirror(Vector2(688.0, 577.0), rotation_degrees=30, size=(30,30)),
        # Mirror(Vector2(657.0, 712.0), rotation_degrees=30, size=(30,30)),
        # Mirror(Vector2(426.0, 276.0), rotation_degrees=30, size=(30,30)),
        # Mirror(Vector2(317.0, 70.0), rotation_degrees=30, size=(30,30)),
        # Mirror(Vector2(506.0, 125.0), rotation_degrees=30, size=(30,30)),
        # Mirror(Vector2(460.0, 554.0), rotation_degrees=30, size=(30,30)),
        # Mirror(Vector2(415.0, 423.0), rotation_degrees=30, size=(30,30)),
        # Mirror(Vector2(301.0, 664.0), rotation_degrees=30, size=(30,30)),
        # Mirror(Vector2(595.0, 181.0), rotation_degrees=30, size=(30,30)),
        # Source(Vector2(158.0, 696.0), direction=Vector2(0.99984737503105, 0.017470736776650963), color=(0, 255, 0, 255)),
        # Source(Vector2(169.0, 540.0), direction=Vector2(0.9962648443158436, -0.086350217025368), color=(255, 0, 0, 255)),
        # Source(Vector2(105.0, 618.0), direction=Vector2(0.9912931122241663, 0.13167370905737638), color=(0, 0, 255, 255)),
        # Target(Vector2(768.0, 358.0), color=(0, 255, 0, 255)),
        # Target(Vector2(889.0, 409.0), color=(255, 0, 0, 255)),
        # Target(Vector2(960.0, 493.0), color=(0, 0, 255, 255)),
        # Wall(Rect((0, 0), (1024, 10))),
        # Wall(Rect((0, 0), (10, 768))),
        # Wall(Rect((0, 758), (1024, 10))),
        # Wall(Rect((962, 404), (10, 100))),
        # Wall(Rect((248, 555), (10, 50))),
        # Wall(Rect((251, 641), (10, 50))),
        # Wall(Rect((816, 326), (10, 80))),
        # Wall(Rect((251, 727), (10, 80))),
        # Wall(Rect((708, 594), (10, 100))),
        # Wall(Rect((260, 205), (10, 300))),
        # Wall(Rect((683, 191), (10, 300))),
        # Wall(Rect((119, 174), (50, 10))),
        # Wall(Rect((896, 159), (50, 10))),
        # Wall(Rect((817, 395), (70, 10))),
        # Wall(Rect((689, 347), (100, 10))),
        # Wall(Rect((150, 495), (120, 10))),
        # ##### SAVED LEVEL END ######
        #         """
        #
        self.sources = [
            Source(Vector2(158.0, 696.0), direction=Vector2(0.9996010256236113, 0.02824516900683203),
                   color=(0, 255, 0, 255)),
            Source(Vector2(169.0, 540.0), direction=Vector2(0.99885024204123, -0.04793948241456614),
                   color=(255, 0, 0, 255)),
            Source(Vector2(105.0, 618.0), direction=Vector2(0.99819506805806, 0.060055025639534465),
                   color=(0, 0, 255, 255)),
        ]

        self.mirrors = [
            Mirror(Vector2(688.0, 577.0), rotation_degrees=30, size=(30, 30)),
            Mirror(Vector2(657.0, 712.0), rotation_degrees=30, size=(30, 30)),
            Mirror(Vector2(426.0, 276.0), rotation_degrees=30, size=(30, 30)),
            Mirror(Vector2(317.0, 70.0), rotation_degrees=30, size=(30, 30)),
            Mirror(Vector2(506.0, 125.0), rotation_degrees=30, size=(30, 30)),
            Mirror(Vector2(460.0, 554.0), rotation_degrees=30, size=(30, 30)),
            Mirror(Vector2(415.0, 423.0), rotation_degrees=30, size=(30, 30)),
            Mirror(Vector2(301.0, 664.0), rotation_degrees=30, size=(30, 30)),
            Mirror(Vector2(595.0, 181.0), rotation_degrees=30, size=(30, 30)),
        ]
        self.targets = [
            Target(Vector2(768.0, 358.0), color=(0, 255, 0, 255)),
            Target(Vector2(889.0, 409.0), color=(255, 0, 0, 255)),
            Target(Vector2(960.0, 493.0), color=(0, 0, 255, 255)),
        ]
        self.walls = [
            Wall(Rect((260, 0), (433, 10))),
            Wall(Rect((0, 496), (269, 10))),
            Wall(Rect((0, 496), (10, 272))),
            Wall(Rect((0, 758), (1024, 10))),
            Wall(Rect((963, 396), (10, 372))),
            Wall(Rect((248, 555), (10, 50))),
            Wall(Rect((251, 641), (10, 50))),
            Wall(Rect((816, 346), (10, 60))),
            Wall(Rect((251, 727), (10, 80))),
            Wall(Rect((708, 594), (10, 100))),
            Wall(Rect((260, 0), (10, 505))),
            Wall(Rect((683, 0), (10, 491))),
            Wall(Rect((896, 159), (50, 10))),
            Wall(Rect((817, 395), (208, 10))),
            Wall(Rect((689, 347), (142, 10))),
        ]

        # self.walls = [
        #     # outer walls
        #     Wall(Rect(0, 0, screen_width, 10)),
        #     Wall(Rect(0, 0, 10, screen_height)),
        #     Wall(Rect(screen_width - 10, 0, 10, screen_height)),
        #     Wall(Rect(0, screen_height - 10, screen_width, 10)),
        #     # inner walls
        #     Wall(Rect((22, 85), (300, 10))),
        #     Wall(Rect((100, 400), (10, 100))),
        #     Wall(Rect((120, 100), (10, 50))),
        #     Wall(Rect((140, 100), (10, 50))),
        #     Wall(Rect((160, 100), (10, 80))),
        #     Wall(Rect((180, 100), (10, 80))),
        #     Wall(Rect((200, 100), (10, 100))),
        #     Wall(Rect((220, 100), (10, 100))),
        #     Wall(Rect((750, 40), (10, 300))),
        #     Wall(Rect((844, 17), (10, 300))),
        #     Wall(Rect((502, 223), (50, 10))),
        #     Wall(Rect((500, 120), (50, 10))),
        #     Wall(Rect((500, 140), (70, 10))),
        #     Wall(Rect((500, 160), (70, 10))),
        #     Wall(Rect((500, 180), (100, 10))),
        #     Wall(Rect((500, 200), (120, 10))),
        #     Wall(Rect((635, 692), (300, 10))),
        #     Wall(Rect((363, 85), (300, 10))),
        # ]
        #
        # self.sources = [
        #     Source(Vector2(30, 30), direction=Vector2(1, 0), color=pygame.Color("green")),
        #     Source(Vector2(30, settings.screen_height - 50), direction=Vector2(1, -0.25), color=pygame.Color("red")),
        #     Source(Vector2(150, 400), direction=Vector2(1, 1), color=pygame.Color("blue")),
        # ]
        #
        # self.mirrors = [
        #     Mirror(Vector2(500.0, 30.0), rotation_degrees=0, size=(30, 30)),
        #     Mirror(Vector2(300.0, 500.0), rotation_degrees=0, size=(30, 30)),
        #     Mirror(Vector2(599.0, 54.0), rotation_degrees=0, size=(30, 30)),
        #     Mirror(Vector2(500.0, 500.0), rotation_degrees=0, size=(50, 50)),
        #     Mirror(Vector2(100.0, 400.0), rotation_degrees=0, size=(50, 50)),
        #     Mirror(Vector2(344.0, 367.0), rotation_degrees=0, size=(50, 50)),
        #     Mirror(Vector2(797.0, 25.0), rotation_degrees=0, size=(10, 10)),
        #     Mirror(Vector2(812.0, 124.0), rotation_degrees=0, size=(10, 10)),
        #     Mirror(Vector2(779.0, 158.0), rotation_degrees=0, size=(10, 10)),
        #     Mirror(Vector2(817.0, 193.0), rotation_degrees=0, size=(20, 20)),
        #     Mirror(Vector2(785.0, 246.0), rotation_degrees=0, size=(20, 20)),
        #     Mirror(Vector2(823.0, 292.0), rotation_degrees=0, size=(20, 20)),
        #     Mirror(Vector2(800.0, 400.0), rotation_degrees=0, size=(100, 100)),
        # ]
        #
        # self.targets = [
        #     Target(Vector2(700, 650), color=pygame.Color("green")),
        #     Target(Vector2(600, 600), color=pygame.Color("red")),
        #     Target(Vector2(500, 600), color=pygame.Color("blue")),
        # ]

        self.focus = self.mirrors[0]

    def draw_step(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0, screen=None):
        self.screen = screen or pygame.display.get_surface()  # hack to get transitions working!!! AARRGGHHH!
        self.screen.fill((0, 0, 0))


        color_wall = pygame.Color("grey")
        for w in self.walls:
            pygame.draw.rect(self.screen, color_wall, w.rect)

        for t in self.targets:
            if t.is_hit:
                pygame.draw.circle(self.screen, t.color, t.position, 20)
            else:
                pygame.draw.circle(self.screen, t.color, t.position, 10, 2)

        for s in self.sources:
            pygame.draw.circle(self.screen, s.color, s.position, 10)

        img = self.resource_manager.resources[resources.res_img_charging_station_background].image
        self.screen.blit(img, (0, 0))

        color_mirror_aabb = pygame.Color("grey90")
        color_mirror = (255, 255, 25)
        img_mirror = self.resource_manager.resources[resources.res_img_mirror].image
        for mirror in self.mirrors:
            m: Mirror = mirror
            rotated = pygame.transform.rotate(img_mirror, mirror.rotation)
            r = rotated.get_rect(center=m.position)
            self.screen.blit(rotated, r)
            # x_axis = m.half_x_axis
            # y_axis = m.half_y_axis
            # p1 = m.position + x_axis + y_axis
            # p2 = m.position - x_axis + y_axis
            # p3 = m.position - x_axis - y_axis
            # p4 = m.position + x_axis - y_axis

            # pygame.draw.polygon(self.screen, color_mirror, [p1, p2, p3, p4])
            # pygame.draw.rect(self.screen, color_mirror_aabb, m.rect, 1)
            # pygame.draw.line(self.screen, pygame.Color("red"), mirror.position,
            #                  mirror.position + mirror.x_axis * mirror.rect.w)
            # pygame.draw.line(self.screen, pygame.Color("green"), mirror.position,
            #                  mirror.position + mirror.y_axis * mirror.rect.w)

        for s in self.sources:
            beam = s.beam
            while beam:
                # logger.info("beam end: {0}", beam.end)
                # pygame.draw.line(self.screen, beam.color, beam.start, beam.end)
                pygame.draw.aaline(self.screen, beam.color, beam.start, beam.end)
                # label = pygametext.getsurf(f"s:{beam.start} e:{beam.end} d:{beam.direction}   {beam.source}")
                # self.screen.blit(label, (beam.start + beam.end) / 2)
                beam = beam.next

        pygame.draw.rect(self.screen, pygame.Color("orange"), self.focus.rect.inflate(6, 6), 1)
        if self.mouse_hover:
            pygame.draw.rect(self.screen, pygame.Color("yellow"), self.mouse_hover.rect.inflate(6, 6), 1)

        if do_flip:
            pygame.display.flip()

    def update_step(self, delta, sim_time, *args):
        self._handle_events()
        self.scheduler.update(delta, sim_time)

        for t in self.targets:
            t.update()

        for t in self.targets:
            t.is_hit = False

        for s in self.sources:
            limit = s.limit
            beam: Beam = s.beam
            beam_count = 0
            while beam and beam_count < limit:
                beam.index = beam_count
                beam_count += 1
                hit, hit_point, normal = self.get_beam_hit(beam)
                beam.end = hit_point
                if isinstance(hit, Mirror):
                    # find accurate intersection point
                    new_end = hit_point
                    if normal is None:
                        logger.info("what?")
                    else:
                        beam.end = new_end
                        new_dir = beam.direction.reflect(normal)
                        if beam.next and False:
                            beam.next.direction = new_dir
                            beam.next.start = beam.end
                            beam.next.color = beam.color
                            beam.source = hit
                        else:
                            new_beam = Beam(beam.end, new_dir, beam.color, hit)
                            beam.next = new_beam
                elif isinstance(hit, Target):
                    # update target
                    if hit.color == beam.color:
                        hit.is_hit = True
                    beam.next = None
                else:
                    # wall
                    beam.next = None

                beam = beam.next

        for t in (t for t in self.targets if t.is_hit and not t.old_is_hit):
            logger.info(f"target {t.color} fired up")
            self.sound_player.play_res(resources.res_sound_laser_up)
            self.scheduler.schedule(self.move_to_next, settings.move_next_timeout)

        for t in (t for t in self.targets if not t.is_hit and t.old_is_hit):
            logger.info(f"target {t.color} canceled up")
            self.sound_player.play_res(resources.res_sound_laser_down)

        if all((t.is_hit for t in self.targets)):
            if not self.level_progress.is_current_done():
                self.level_progress.set_current_to_done()
                logger.info(">>>>>>>>> WIN <<<<<<<<<<<<<<")
                logger.info(">>>>>>>>> WIN <<<<<<<<<<<<<<")
                logger.info(">>>>>>>>> WIN <<<<<<<<<<<<<<")
                self.sound_player.play_res(resources.res_sound_solve_puzzle)

    def move_to_next(self, *args):
        if all((t.is_hit for t in self.targets)):
            self.move_to_next_wihtout_check()
        return 0

    def move_to_next_wihtout_check(self):
        self.pop(effect=pyknic.pyknic_pygame.context.effects.FadeOutEffect(settings.effect_duration))

    def get_beam_hit(self, beam: Beam):
        hit = None
        hit_point = None
        normal = None
        n = None
        max_length = settings.screen_width + settings.screen_height
        beam_end = beam.start + beam.direction * max_length
        nearest_hit_point = beam.start.distance_squared_to(beam_end)
        for w in self.walls + self.mirrors + self.targets:
            _hit, p1, p2 = self._clip(beam.start, beam_end, w.rect)
            if _hit:
                if w != beam.source:

                    if isinstance(w, Mirror):
                        p1, n = self._get_mirror_hit_point(beam, w)
                        if p1 is None:
                            continue

                    dist = beam.start.distance_squared_to(p1)
                    if dist < nearest_hit_point:
                        nearest_hit_point = dist
                        hit = w
                        hit_point = p1
                        normal = n

        if __debug__ and hit is None:
            logger.error("No hit")
            hit_point = beam_end
        return hit, hit_point, normal

    @staticmethod
    def _clip(start: Vector2, end: Vector2, rect: Rect):
        hit, ex, ey, ox, oy = clip_segment_against_aabb(start.x, start.y, end.x, end.y, rect.left, rect.top, rect.right,
                                                        rect.bottom)
        return hit, Vector2(ex, ey), Vector2(ox, oy)

    def _handle_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.move_to_next_wihtout_check()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                edit_mode = pygame.key.get_mods() & pygame.KMOD_CTRL
                mirrors = self.mirrors + self.sources
                if edit_mode:
                    mirrors += self.targets + self.walls
                mouse_rect = Rect(event.pos, (1, 1))
                idx = mouse_rect.collidelist(mirrors)
                if idx > -1:
                    mir = mirrors[idx]
                    if edit_mode:
                        self.dragging = mir
                    else:
                        self.focus = mir
                else:
                    self.mouse_pos = Vector2(event.pos)
                    self.focus_orientation = self.focus.orientation
            elif event.type == pygame.MOUSEBUTTONUP:
                self.dragging = None
                self.mouse_pos = None
                self.focus_orientation = None
            elif event.type == pygame.MOUSEMOTION:
                if self.dragging:
                    self.dragging.move_to(event.pos)
                elif any(event.buttons) and self.mouse_pos:
                    da = (self.mouse_pos - self.focus.position).angle_to(Vector2(event.pos) - self.focus.position)
                    new_orientation = self.focus_orientation.rotate(da)
                    self.focus.orientation = new_orientation
                else:
                    mirrors = self.mirrors + self.sources
                    mouse_rect = Rect(event.pos, (1, 1))
                    idx = mouse_rect.collidelist(mirrors)
                    if idx > -1:
                        mir = mirrors[idx]
                        self.mouse_hover = mir
                    else:
                        self.mouse_hover = None

            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_TAB:
                    mirrors = self.mirrors + self.sources
                    idx = mirrors.index(self.focus)
                    idx += 1
                    idx %= len(mirrors)
                    self.focus = mirrors[idx]
                elif event.key == pygame.K_p:
                    logger.info("##### SAVED LEVEL ######")
                    for m in self.mirrors:
                        logger.info(
                            f"Mirror(Vector2({m.position.x}, {m.position.y}), rotation_degrees=30, size=({m.size[0]},{m.size[1]})),\t\t")
                    for s in self.sources:
                        logger.info(
                            f"Source(Vector2({s.position.x}, {s.position.y}), direction=Vector2({s.beam.direction.x}, {s.beam.direction.y}), color={s.color}),\t\t")
                    for t in self.targets:
                        logger.info(f"Target(Vector2({t.position.x}, {t.position.y}), color={t.color}),\t\t")
                    for w in self.walls:
                        logger.info(f"Wall(Rect({w.rect.topleft}, {w.rect.size})),\t\t")
                    logger.info("##### SAVED LEVEL END ######")
            elif event.type == settings.music_ended_pygame_event:
                self.music_player.on_music_ended()

    @staticmethod
    def _get_mirror_hit_point(beam: Beam, mirror: Mirror):
        # # >>>>> THIS IS WRONG <<<<<
        #
        #                  top
        #           +---------------+
        #           |            d2 |                                  ---->   <---- => negative
        #           |           /   |     <--------  beam              ---->   ----> => positive
        #           |         /     |
        #      left |       o--->x  | right
        #           |       | \     |
        #           |       v   \   |
        #           |       y    d1 |
        #           +---------------+
        #                 bottom

        # beam_dir = beam.direction

        # # this is wrong!!!! # fixme analyse after pyweek
        # d1 = mirror.x_axis + mirror.y_axis
        # # d1.normalize_ip()
        # d2 = mirror.x_axis - mirror.y_axis
        # # d2.normalize_ip()
        # logger.info(f"axis: x {mirror.x_axis}, y {mirror.y_axis}, d1 {d1}, d2 {d2}  beam dir: {beam.direction}")
        # logger.info(f"beam_dir.dot(d1): {beam_dir.dot(d1)}, beam_dir.dot(d2): {beam_dir.dot(d2)}")
        # logger.info(
        #     f"beam_dir.dot(x-axis):{beam_dir.dot(mirror.x_axis)}, beam_dir.dot(yaxis): {beam_dir.dot(mirror.y_axis)}")

        # if beam_dir.dot(d1) < 0:
        #     # bottom right
        #     if beam_dir.dot(d2) < 0:
        #         # right
        #         segment = (mirror.position + mirror.half_x_axis + mirror.half_y_axis,
        #                    mirror.position + mirror.half_x_axis - mirror.half_y_axis)
        #         normal = mirror.x_axis
        #     else:
        #         # bottom
        #         segment = (mirror.position - mirror.half_x_axis + mirror.half_y_axis,
        #                    mirror.position + mirror.half_x_axis + mirror.half_y_axis)
        #         normal = mirror.y_axis
        # else:
        #     # top left
        #     if beam_dir.dot(d2) < 0:
        #         # top
        #         segment = (mirror.position - mirror.half_x_axis - mirror.half_y_axis,
        #                    mirror.position + mirror.half_x_axis - mirror.half_y_axis)
        #         normal = -mirror.y_axis
        #     else:
        #         # left
        #         segment = (mirror.position - mirror.half_x_axis - mirror.half_y_axis,
        #                    mirror.position - mirror.half_x_axis + mirror.half_y_axis)
        #         normal = -mirror.x_axis

        segment0 = (mirror.position + mirror.half_x_axis + mirror.half_y_axis,
                    mirror.position + mirror.half_x_axis - mirror.half_y_axis)
        normal0 = mirror.x_axis
        segment1 = (mirror.position - mirror.half_x_axis + mirror.half_y_axis,
                    mirror.position + mirror.half_x_axis + mirror.half_y_axis)
        normal1 = mirror.y_axis
        segment2 = (mirror.position - mirror.half_x_axis - mirror.half_y_axis,
                    mirror.position + mirror.half_x_axis - mirror.half_y_axis)
        normal2 = -mirror.y_axis
        segment3 = (mirror.position - mirror.half_x_axis - mirror.half_y_axis,
                    mirror.position - mirror.half_x_axis + mirror.half_y_axis)
        normal3 = -mirror.x_axis

        d = (settings.screen_width + settings.screen_height) ** 2
        point = None
        normal = None
        for s, n in [(segment0, normal0), (segment1, normal1), (segment2, normal2), (segment3, normal3)]:
            intersected, p = line_ray_intersection_point(beam.start, beam.direction, s[0], s[1])
            if intersected:
                dist = beam.start.distance_squared_to(p)
                if dist < d:
                    d = dist
                    point = p
                    normal = n

        return point, normal


def line_ray_intersection_point(ray_origin, ray_direction, point1, point2):
    """
    Based on : https://gist.github.com/danieljfarrell/faf7c4cafd683db13cbc
    see link below for explanation:     # http://bit.ly/1CoxdrG

    """
    # Ray-Line Segment Intersection Test in 2D
    # http://bit.ly/1CoxdrG
    v1 = ray_origin - point1
    v2 = point2 - point1
    v3 = Vector2(-ray_direction.y, ray_direction.x)
    if v2.dot(v3) == 0:
        # logger.error("huh!")
        # collinear
        return False, ray_direction
    t1 = v2.cross(v1) / v2.dot(v3)
    t2 = v1.dot(v3) / v2.dot(v3)
    if t1 >= 0.0 and 0.0 <= t2 <= 1.0:
        return True, ray_origin + t1 * ray_direction
    return False, ray_direction


# def lineLineIntersection(A, B, C, D):
#     """
#     Calculates the intersection point of lines AB and CD:
#
#     Base on: https://www.geeksforgeeks.org/program-for-point-of-intersection-of-two-lines/
#     Modified to work with pygame.Vector2!
#
#     :param A:
#     :param B:
#     :param C:
#     :param D:
#     :return:
#     """
#     # Line AB represented as a1x + b1y = c1
#     a1 = B.y - A.y
#     b1 = A.x - B.x
#     c1 = a1 * A.x + b1 * A.y
#
#     # Line CD represented as a2x + b2y = c2
#     a2 = D.y - C.y
#     b2 = C.x - D.x
#     c2 = a2 * C.x + b2 * C.y
#
#     determinant = a1 * b2 - a2 * b1
#
#     if determinant == 0:
#         # The lines are parallel.
#         return False, Vector2(0, 0)
#     else:
#         x = (b2 * c1 - b1 * c2) / determinant
#         y = (a1 * c2 - a2 * c1) / determinant
#         return True, Vector2(x, y)


logger.debug("imported")
