# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'Coronation.py' is part of pw-34
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2022"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

import pygame.display
from pygame import Vector2

import pyknic
from gamelib import settings, resources
from pyknic.pyknic_pygame.context import TimeSteppedContext

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class Crown:

    kind = 3.1416

    def __init__(self, position, surf):
        self.surf = surf
        self.position = position


class Coronation(TimeSteppedContext):

    def __init__(self, max_dt, step_dt, draw_fps, music_player, resource_manager, level_progress, sound_player):
        TimeSteppedContext.__init__(self, max_dt, step_dt, draw_fps)
        self.resource_manager = resource_manager
        surf = resource_manager.resources[resources.res_img_crowns].data[1]
        self.crown = Crown(Vector2(settings.screen_width // 2, settings.screen_height // 2), surf)

    def enter(self):
        pygame.event.clear()

    def resume(self):
        pygame.event.clear()

    def draw_step(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0, screen=None):

        screen.blit(self.crown.surf, self.crown.surf.get_rect(center=self.crown.position))

        if do_flip:
            pygame.display.flip()

    def update_step(self, delta, sim_time, *args):
        self._handle_events()

    def _handle_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.move_to_next()
            elif event.type == pygame.KEYDOWN:
                self.move_to_next()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                self.move_to_next()
            elif event.type == settings.music_ended_pygame_event:
                self.music_player.on_music_ended()

    def move_to_next(self):
        self.pop(effect=pyknic.pyknic_pygame.context.effects.FadeOutEffect(settings.effect_duration))


logger.debug("imported")
