# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'setup.py' is part of pw-34
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Initial setup of the game. Choose language, maybe sound.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2022"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

import pygame

from gamelib import strings, settings
from pyknic.pyknic_pygame import pygametext
# __all__ = []  # list of public visible parts of this module

from pyknic.pyknic_pygame.context import TimeSteppedContext

logger = logging.getLogger(__name__)
logger.debug("importing...")


class Label:

    def __init__(self, rect, text):
        self.rect = rect
        self.text = text


class Button:

    def __init__(self, rect, text, callback, data=None):
        self.data = data
        self.position = pygame.Vector2(rect.center)
        self.rect = rect.copy()
        self.has_focus = False
        self.is_active = False
        self.text = text
        self.callback = callback

    def on_click(self):
        if self.callback:
            self.callback(self)


class Credits(TimeSteppedContext):

    def __init__(self, max_dt, step_dt, draw_fps, music_player, resource_manager, sound_player):
        TimeSteppedContext.__init__(self, max_dt, step_dt, draw_fps)
        self.buttons = []
        self.labels = []
        self.screen = None
        self.font: pygame.font.Font = None
        self.focus = None
        self.sound_player = sound_player

    def enter(self):
        pass

    def resume(self):
        # self.buttons = []
        self.screen = pygame.display.get_surface()
        self.font = pygame.font.Font(None, 30)
        rect = pygame.Rect(10, 10, 100, 35)
        distance = rect.h * 2
        rect.center = settings.screen_width // 2, distance

        rect.center = settings.screen_width // 2, settings.screen_height - distance
        start_button = Button(rect, strings.messages.setup_start, self._quit, "start")
        self.buttons.append(start_button)

        label_rect = pygame.Rect(0, 0, 1, 1)
        label_rect.midbottom = (settings.screen_width // 2, settings.screen_height - 2 * distance)
        self.labels.append(Label(label_rect, strings.messages.credits_info))

        start_button.text = strings.messages.credits_quit

    def _quit(self, *args):
        self.pop(20)

    def draw_step(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0, screen=None):
        self.screen = screen  # hack to get transitions working!!! AARRGGHHH!
        color_background = pygame.Color("white")
        self.screen.fill(color_background)

        color_button = pygame.Color("black")
        color_button_active = pygame.Color("red")
        color_text = pygame.Color("white")
        color_label_text = pygame.Color("black")
        color_focus = pygame.Color("orange")
        for label in self.labels:
            label_surf = pygametext.getsurf(label.text, antialias=True, color=color_label_text)
            r = label_surf.get_rect(midbottom=label.rect.midbottom)
            pygame.draw.rect(self.screen, pygame.Color("grey80"), r.inflate(20, 20))
            self.screen.blit(label_surf, r)

        for button in self.buttons:
            if button.has_focus:
                pygame.draw.rect(self.screen, color_focus, button.rect.inflate(8, 8))
            pygame.draw.rect(self.screen, color_button_active if button.is_active else color_button, button.rect)
            label = self.font.render(button.text, True, color_text)

            r = label.get_rect(center=button.rect.center)
            self.screen.blit(label, r)

        if do_flip:
            pygame.display.flip()

    def update_step(self, delta, sim_time, *args):
        self._handle_events()

    def _handle_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self._quit()
            elif event.type == pygame.MOUSEMOTION:
                mouse_pos = pygame.Vector2(event.pos)
                new_focus = self.get_entity_at_position(mouse_pos)
                if self.focus != new_focus:
                    if self.focus:
                        self.focus.has_focus = False
                    self.focus = new_focus
                    if new_focus:
                        new_focus.has_focus = True

                if new_focus:
                    pygame.mouse.set_cursor(pygame.SYSTEM_CURSOR_HAND)
                else:
                    pygame.mouse.set_cursor(pygame.SYSTEM_CURSOR_ARROW)

            elif event.type == pygame.MOUSEBUTTONUP:
                entity = self.get_entity_at_position(pygame.Vector2(event.pos))
                if entity:
                    entity.on_click()
            elif event.type == pygame.KEYDOWN:
                if event.key in (pygame.K_RETURN, pygame.K_SPACE):
                    if self.focus:
                        self.focus.on_click()
            elif event.type == pygame.QUIT:
                self.pop(20)
            elif event.type == settings.music_ended_pygame_event:
                self.music_player.on_music_ended()

    def get_entity_at_position(self, mouse_pos):
        for button in self.buttons:
            if button.rect.collidepoint(mouse_pos):
                return button


logger.debug("imported")
