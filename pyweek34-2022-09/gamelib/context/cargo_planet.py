# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'cargo_planet.py' is part of pw-34
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2022"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

import random

import pygame
from pygame import Vector2, Rect

import pyknic
from gamelib import resources, settings
# __all__ = []  # list of public visible parts of this module

from pyknic.pyknic_pygame.context import TimeSteppedContext
from pyknic.timing import Scheduler

logger = logging.getLogger(__name__)
logger.debug("importing...")


class Piece:

    def __init__(self, position, surf):
        self.surf = surf
        self.mask = pygame.mask.from_surface(self.surf)
        self.position = position
        self.rect = self.surf.get_rect(center=position)

    def rotate(self):
        self.surf = pygame.transform.rotate(self.surf, 90)
        self.mask = pygame.mask.from_surface(self.surf)

    def update(self):
        self.rect = self.surf.get_rect(center=self.position)


class Trunk(Piece):

    def __init__(self, position, surf):
        Piece.__init__(self, position, surf)

    def rotate(self):
        pass  # no rotation of the trunk allowed


# todo sounds
# todo hints ( robot?)
# todo place the cargo around the ship
# todo instructions?
class CargoPlanet(TimeSteppedContext):

    def __init__(self, max_dt, step_dt, draw_fps, music_player, resource_manager, level_progress, sound_player):
        TimeSteppedContext.__init__(self, max_dt, step_dt, draw_fps)
        self.level_progress = level_progress
        self.resource_manager = resource_manager
        self.sound_player = sound_player
        self.music_player = music_player
        self.screen = None
        self.pieces = []
        trunk_surf = resource_manager.resources[resources.res_img_trunk].image
        self.trunk = Piece(Vector2(344 + 160, 496 + 90), trunk_surf)
        self.mouse_hover = None

        resource_ids = [resources.res_img_piece0] * 5 + [resources.res_img_piece1] * 3 + [
            resources.res_img_piece2] * 2 + [resources.res_img_piece3] * 4 + [resources.res_img_piece4] * 2

        right_rect = pygame.Rect(737, 488, 232, 223)
        left_rect = pygame.Rect(56, 488, 232, 223)
        regions = [left_rect, right_rect]
        for idx, res_id in enumerate(resource_ids):
            region = random.choice(regions)
            x = random.randint(region.left, region.left + region.w)
            y = random.randint(region.top, region.top + region.h)
            piece_surf = resource_manager.resources[res_id].image
            piece = Piece(Vector2(x, y), piece_surf)
            for i in range(random.randint(0, 3)):
                piece.rotate()
            self.pieces.append(piece)

        self.solution_bit_count = sum((p.mask.count() for p in self.pieces))

        self.dragging = None
        self.kludge_progress = False
        self.scheduler = Scheduler()

    def enter(self):
        pygame.mouse.set_visible(True)
        self.screen = pygame.display.get_surface()
        self.dragging = None

    def resume(self):
        self.screen = pygame.display.get_surface()
        pygame.mouse.set_visible(True)
        self.dragging = None

    def draw_step(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0, screen=None):
        self.screen = screen or pygame.display.get_surface()  # hack to get transitions working!!! AARRGGHHH!
        self.screen.fill((200, 200, 200))

        self.screen.blit(self.resource_manager.resources[resources.res_img_spaceship_trunk].image, (0, 0))

        trunk = self.trunk
        # self.screen.blit(trunk.surf, trunk.rect)
        # self.screen.blit(trunk.mask.to_surface(setcolor=(0, 255, 0, 128), unsetcolor=(0, 0, 0, 0)), trunk.rect)
        # pygame.draw.rect(self.screen, pygame.Color("white"), trunk.rect, 1)

        for p in self.pieces:
            self.screen.blit(p.surf, p.rect)
            # pygame.draw.rect(self.screen, pygame.Color("white"), p.rect, 1)
            # self.screen.blit(p.mask.to_surface(setcolor=(0, 0, 255, 128), unsetcolor=(0, 0, 0, 0)), p.rect)

        accumulated = trunk.mask.copy()
        accumulated.clear()
        inner_overlaps = trunk.mask.copy()
        inner_overlaps.clear()
        for p in reversed(self.pieces):
            offset = Vector2(p.rect.topleft) - Vector2(self.trunk.rect.topleft)
            overlap = accumulated.overlap_mask(p.mask, offset)
            inner_overlaps.draw(overlap, (0, 0))
            accumulated.draw(p.mask, offset)
        outer_overlaps = accumulated.overlap_mask(trunk.mask, (0, 0))

        overlap_surf = self.resource_manager.resources[resources.res_img_overlap].image

        surf = outer_overlaps.to_surface(setsurface=overlap_surf, setcolor=(255, 0, 0, 128),
                                         unsetcolor=(0, 0, 0, 0))  # setcolor=(255, 0, 0, 128), unsetcolor=(0, 0, 0, 0))
        self.screen.blit(surf, trunk.rect)  # self.trunk.position)

        surf = inner_overlaps.to_surface(setsurface=overlap_surf, setcolor=(255, 0, 0, 128),
                                         unsetcolor=(0, 0, 0, 0))  # setcolor=(255, 0, 0, 128), unsetcolor=(0, 0, 0, 0))
        self.screen.blit(surf, trunk.rect)  # self.trunk.position)

        if self.dragging:
            self.screen.blit(self.dragging.surf,
                             self.dragging.rect)  # just draw over again the pice to it look like its on the top

        if self.mouse_hover:
            pygame.draw.rect(self.screen, pygame.Color("white"), self.mouse_hover.rect.inflate(6, 6), 1)

        if inner_overlaps.count() == 0 and outer_overlaps.count() == 0 and accumulated.count() == self.solution_bit_count:
            if not self.level_progress.is_current_done():
                # if not self.kludge_progress:
                #     self.kludge_progress = True
                logger.info(">>>> WIN <<<<<")
                logger.info(">>>> WIN <<<<<")
                logger.info(">>>> WIN <<<<<")
                self.level_progress.set_current_to_done()
                self.sound_player.play_res(resources.res_sound_solve_puzzle)
                self.scheduler.schedule(self.move_next, settings.move_next_timeout)

        if do_flip:
            pygame.display.flip()

    def move_next(self, *args):
        self.pop(effect=pyknic.pyknic_pygame.context.effects.FadeOutEffect(settings.effect_duration))
        return 0

    def update_step(self, delta, sim_time, *args):
        self.scheduler.update(delta, sim_time)
        self._handle_events()
        for p in self.pieces:
            p.update()  # keep rect in sync with position
        self.trunk.update()

    def _handle_events(self):
        left_mouse_button = 1
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.move_next()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == left_mouse_button:
                    self.dragging = self.get_piece_at(Vector2(event.pos))
                    if self.dragging:
                        self.pieces.remove(self.dragging)
                        self.pieces.append(self.dragging)  # so get_piece_at get the dragged first
                else:
                    if self.dragging:
                        self.dragging.rotate()
                    else:
                        piece = self.get_piece_at(Vector2(event.pos))
                        if piece:
                            piece.rotate()
            elif event.type == pygame.MOUSEBUTTONUP:
                if event.button == left_mouse_button:
                    if self.dragging:
                        self.dragging = None
            elif event.type == pygame.MOUSEMOTION:
                if self.dragging:
                    pos = Vector2(event.pos)
                    if self.trunk.rect.collidepoint(pos):
                        trunk_topleft = Vector2(self.trunk.rect.topleft)
                        to_center = Vector2(self.dragging.rect.w // 2, self.dragging.rect.h // 2)
                        offset = (pos - trunk_topleft) // 20 * 20 + to_center - to_center // 20 * 20
                        # offset.update(offset.x // 20 , offset.y //20 )*20
                        pos = trunk_topleft + offset
                    self.dragging.position.update(pos)
                else:
                    piece = self.get_piece_at(Vector2(event.pos))
                    self.mouse_hover = piece
            elif event.type == settings.music_ended_pygame_event:
                self.music_player.on_music_ended()

    def get_piece_at(self, at):
        r = Rect(at, (1, 1))
        indices = r.collidelistall(self.pieces)
        if indices:
            return self.pieces[indices[-1]]
        return None


logger.debug("imported")
