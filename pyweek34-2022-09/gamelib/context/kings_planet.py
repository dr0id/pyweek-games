# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'kings_planet.py' is part of pw-34
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Kings planet context
"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2022"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

import pygame

import pyknic
from gamelib import settings, resources
from gamelib.entity_renderer import EntityRenderer, _ScreenEntity
from pyknic.collider import Collider

from pyknic.pyknic_pygame.context import TimeSteppedContext
from pyknic.pyknic_pygame.spritesystem import Sprite
from pyknic.resource2 import ResourceManager
from pyknic.timing import Scheduler

logger = logging.getLogger(__name__)
logger.debug("importing...")

KIND_SHIP = settings.global_id_generator.next("KIND_SHIP")
KIND_LANDING_PLATFORM = settings.global_id_generator.next("KIND_LANDING_PLATFORM")
KIND_CITY = settings.global_id_generator.next("KIND_CITY")
KIND_WIND = settings.global_id_generator.next("KIND_WIND")
KIND_SWIRL = settings.global_id_generator.next("KIND_SWIRL")
KIND_RED_BACKGROUND = settings.global_id_generator.next("KIND_SKY")

screen_w, screen_h = settings.screen_size


class Sky:
    kind = KIND_RED_BACKGROUND
    layer = -100

    def __init__(self, rect):
        self.rect = rect
        self.position = pygame.Vector2(self.rect.center)

    def update(self, dt):
        pass


class Ship:

    kind = KIND_SHIP
    layer = 5

    def __init__(self, position, rect, max_speed, max_acceleration, k, max_rot_vel):
        self.position = position
        self.old_position = position.copy()
        self.velocity = pygame.Vector2(0, 0)
        self.acceleration = pygame.Vector2(0, 0)
        self.max_speed = max_speed
        self.max_acceleration = max_acceleration
        self.rect: pygame.Rect = rect
        self.rot_acc = 0  # α(t) = dω / dt → dω = α(t) * dt
        self.rot_vel = 0  # ω = dφ/dt → dφ = ω * dt
        self.rotation = 90
        self.orientation = pygame.Vector2(0, 0)
        self.is_landed = False
        self.k = k
        self.wind = pygame.Vector2(0, 0)
        self.rot_wind = 0
        self.max_rot_vel = max_rot_vel
        self.bump_acceleration = pygame.Vector2(0, 0)

    def reset(self):
        self.position.update(50, 50)
        self.rot_vel = 0
        self.rot_acc = 0
        self.acceleration.update(0, 0)
        self.velocity.update(0, 0)
        self.wind.update(0, 0)
        self.rot_wind = 0
        self.is_landed = False

    def move(self, move_x_dir, move_y_dir):
        if move_x_dir:
            self.acceleration += self.max_acceleration * move_x_dir * self.orientation
            self.rot_acc += 125 * move_x_dir

        if move_y_dir:
            self.acceleration += self.max_acceleration * move_y_dir * self.orientation.rotate(90)

        if self.acceleration.length_squared() > self.max_acceleration * self.max_acceleration:
            self.acceleration.scale_to_length(self.max_acceleration)  # ensure max acceleration
            # logger.info("ACC: {0}, {1}", self.acceleration, self.rot_acc)

    def bump(self, direction):
        self.bump_acceleration += direction * self.max_acceleration
        self.velocity.update(direction * self.velocity.length())

    def update(self, dt):
        if self.is_landed:
            return

        air_vel = self.velocity - self.wind
        if air_vel.x or air_vel.y:
            drag = self.k * air_vel.length_squared() * -air_vel.normalize()
            self.acceleration += drag

        # self leveling of ship
        if 1 < self.rotation < 359:
            leveling_acceleration = 5
            if self.rotation < 180:
                self.rot_acc += leveling_acceleration * -self.rotation * 0.99
            else:
                self.rot_acc += leveling_acceleration * (360 - self.rotation) * 0.99

        # rotational drag
        rot_wind_vel = self.rot_vel - self.rot_wind
        rot_drag = self.k * (rot_wind_vel ** 2) * (-1 if rot_wind_vel > 0 else 1)
        # logger.warning("rot_wind: {0}, rot_vel: {1}, rot_wind: {2}, rot_acc: {3}, drag: {4}", rot_wind_vel,
        #                self.rot_vel, self.rot_wind, self.rot_acc, rot_drag)
        self.rot_acc += rot_drag

        self.rot_vel += self.rot_acc * dt
        if abs(self.rot_vel) > self.max_rot_vel:
            self.rot_vel = self.max_rot_vel if self.rot_vel > 0 else -self.max_rot_vel

        self.rotation += self.rot_vel * dt
        self.rotation %= 360
        self.orientation.update(1, 0)
        self.orientation.rotate_ip(self.rotation)

        self.old_position.update(self.position)

        if self.position.y < 0:  # overrule any acceleration if it is outside on top
            self.acceleration.y += self.max_acceleration

        self.velocity += self.acceleration * dt
        if self.velocity.length_squared() > self.max_speed * self.max_speed:
            self.velocity.scale_to_length(self.max_speed)  # ensure max speed
        self.velocity += self.bump_acceleration * dt  # careful!
        self.position += self.velocity * dt
        self.rect.center = self.position

        if self.position.x > screen_w:
            self.position.x = 0
        if self.position.x < 0:
            self.position.x = screen_w
        if self.position.y < -20:
            self.position.y = -5
            self.velocity.y = 0

        self.wind.x = 0
        self.wind.y = 0
        self.acceleration.update(0, 0)
        self.bump_acceleration.update(0, 0)
        self.rot_acc = 0
        self.rot_wind = 0


class LandingPlatform:

    kind = KIND_LANDING_PLATFORM
    layer = 4

    def __init__(self, rect):
        self.rect = rect
        self.position = pygame.Vector2(self.rect.center)

    def update(self, dt):
        pass


class City:

    kind = KIND_CITY
    layer = 3

    def __init__(self, rect):
        self.rect = rect
        self.position = pygame.Vector2(self.rect.center)

    def update(self, dt):
        pass


class Wind:

    kind = KIND_WIND
    layer = 9

    def __init__(self, position, velocity, rect, layer=None):
        self.rect = rect
        self.position = position
        self.old_position = position.copy()
        self.velocity = velocity
        self.layer = layer or Wind.layer

    def update(self, dt):
        self.position += self.velocity * dt
        self.rect.center = self.position

        if self.position.x > screen_w:
            self.position.x = 0
        if self.position.x < 0:
            self.position.x = screen_w
        if self.position.y < 0 or self.position.y > screen_h:
            self.velocity.y *= -1
        # todo maybe update the wind according to the position (not sure, looked easy in the documentation: channel.set_volume(left, right)
        # todo or just some random wind noises


class Swirl:

    kind = KIND_SWIRL
    layer = 10

    def __init__(self, position, velocity, rect, wind_rot_vel, rot_vel, layer=None):
        self.rect = rect
        self.position = position
        self.old_position = position.copy()
        self.velocity = velocity
        self.wind_rot_vel = wind_rot_vel
        self.rot_vel = rot_vel
        self.rotation = 0
        self.layer = layer or Swirl.layer

    def update(self, dt):
        self.position += self.velocity * dt
        self.rect.center = self.position

        if self.position.x > screen_w:
            self.position.x = 0
        if self.position.x < 0:
            self.position.x = screen_w
        if self.position.y < 0 or self.position.y > screen_h:
            self.velocity.y *= -1

        rot_delta = self.rot_vel * dt
        self.rotation += rot_delta
        self.velocity.rotate_ip(rot_delta)


class LevelSetup:

    def __init__(self):
        self.ship = Ship(position=pygame.Vector2(0, 0), rect=pygame.Rect(0, 0, 60, 40), max_speed=300,
                         max_acceleration=20000, k=0.15, max_rot_vel=20000)
        self.entities = [
            LandingPlatform(pygame.Rect(400, 700, 107, 123)),
            City(pygame.Rect(0, settings.screen_height - 50, settings.screen_width, 50)),
            Sky(pygame.Rect(0, 0, settings.screen_width, settings.screen_height)),
        ]

        self.winds = [
            Wind(position=pygame.Vector2(40, 500), velocity=pygame.Vector2(-300, -100),
                 rect=pygame.Rect(0, 0, 200, 300), layer=Ship.layer + 1),
            Wind(position=pygame.Vector2(40, 100), velocity=pygame.Vector2(300, 0),
                 rect=pygame.Rect(0, 0, 200, 200), layer=Ship.layer - 1),
            Wind(position=pygame.Vector2(40, 500), velocity=pygame.Vector2(-500, 0),
                 rect=pygame.Rect(0, 0, 300, 300), layer=Ship.layer - 1),

        ]

        self.swirls = [
            Swirl(position=pygame.Vector2(240, 300), velocity=pygame.Vector2(300, 0),
                  rect=pygame.Rect(0, 0, 200, 200), wind_rot_vel=500, rot_vel=35, layer=Ship.layer - 2),
            Swirl(position=pygame.Vector2(240, 300), velocity=pygame.Vector2(100, 300),
                  rect=pygame.Rect(0, 0, 200, 200), wind_rot_vel=-400, rot_vel=-40, layer=Ship.layer + 1),

        ]

        # change those if the appearance should be different
        self.resource_map = {
            Ship.kind: [resources.res_img_red_p_ship],
            Wind.kind: [resources.res_img_red_p_wind],
            Swirl.kind: [resources.res_img_red_p_swirl],
            KIND_LANDING_PLATFORM: [resources.res_img_landing_platform],
            KIND_CITY: [resources.res_img_red_p_city],
            KIND_RED_BACKGROUND: [resources.res_img_red_p_background],
        }


# todo  MUST: there can be more variation, need to tune them better

planet1_level = LevelSetup()
planet1_level.resource_map[KIND_RED_BACKGROUND] = [resources.res_img_green_p_background]
planet1_level.swirls = []

planet2_level = LevelSetup()
planet2_level.resource_map[KIND_RED_BACKGROUND] = [resources.res_img_blue_p_background]
planet2_level.winds = []

planet3_level = LevelSetup()  # red planet, hardest and last level

planet4_level = LevelSetup()
planet4_level.resource_map[KIND_RED_BACKGROUND] = [resources.res_img_violette_p_background]
planet4_level.winds = planet4_level.winds[:2]
planet4_level.swirls = planet4_level.swirls[:1]

planet5_level = LevelSetup()
planet5_level.resource_map[KIND_RED_BACKGROUND] = [resources.res_img_ice_p_background]

# medium level
medium_level = LevelSetup()
medium_level.winds = medium_level.winds[:2]
medium_level.swirls = []

# advanced level
advanced_level = LevelSetup()
advanced_level.winds = []  # no winds, all swirls

# hard level
hard_level = LevelSetup()


class KingsPlanet(TimeSteppedContext):

    def __init__(self, max_dt, step_dt, draw_fps, music_player, resource_manager, level_progress, sound_player,
                 level_setup, followup_context=None):
        TimeSteppedContext.__init__(self, max_dt, step_dt, draw_fps)
        self.followup_context = followup_context
        self.music_player = music_player
        self.sound_player = sound_player
        self.level_progress = level_progress
        self.ship = None
        self.collider = Collider()
        self.entities = []
        self.collider.register(Ship.kind, LandingPlatform.kind, self._coll_ship_landing_platform)
        self.collider.register(Ship.kind, Wind.kind, self._coll_ship_wind)
        self.collider.register(Ship.kind, Swirl.kind, self._coll_ship_swirl)
        self.collider.register(Ship.kind, City.kind, self._coll_ship_city)
        self.screen = None
        self.resource_manager: ResourceManager = resource_manager
        self.renderer: EntityRenderer = EntityRenderer(resource_manager)

        self.renderer.register_update_callbacks({
            Ship.kind: self._updated_ship,
            Wind.kind: self._update_wind,
            Swirl.kind: self._update_swirl,
        })

        self.level_setup = level_setup
        self.scheduler = Scheduler()

    def _coll_ship_city(self, ships, cities):
        for ship in ships:
            for city in cities:
                if ship.rect.colliderect(city.rect):
                    if ship.velocity.dot(pygame.Vector2(0, -1)) > 0:
                        return  # ship is flying away from the city
                    # ship.bump(pygame.Vector2(0, -50))
                    ship.bump(pygame.Vector2(ship.acceleration.x / ship.max_acceleration,
                                             50 * -abs(ship.acceleration.y / ship.max_acceleration)))
                    self.sound_player.play_res(resources.res_sound_crash_city)
                    # todo add particles? smoke?

    def _coll_ship_swirl(self, ships, swirls):
        ship: Ship = next(iter(ships))
        for swirl in swirls:
            if ship.rect.colliderect(swirl.rect):
                ship.rot_wind += swirl.wind_rot_vel
                ship.wind += swirl.velocity
                ship.bump(ship.orientation)
                self.sound_player.play_res(resources.res_sound_wind0, extra='queue')

    def _coll_ship_wind(self, ships, winds):
        ship: Ship = next(iter(ships))
        for wind in winds:
            if ship.rect.colliderect(wind.rect):
                ship.wind += wind.velocity
                self.sound_player.play_res(resources.res_sound_wind1, extra='queue')

                # todo sound: strong winds and maybe exclamations like 'Aaaaah!'
                # res = random.choice([resources.res_sound_wind0,resources.res_sound_wind1, resources.res_sound_wind2])
                # self.sound_player.play_res(res)

    def _coll_ship_landing_platform(self, ships, platforms):
        ship: Ship = next(iter(ships))
        if ship.is_landed:
            return  # prevent spam
        platform = next(iter(platforms))
        if ship.rect.colliderect(platform.rect):
            if abs(ship.orientation.angle_to(pygame.Vector2(1, 0))) < 10 \
                    and abs(ship.position.x - platform.rect.centerx) < 10 \
                    and ship.velocity.y > 0:
                ship.is_landed = True
                # self.sound_player.play_res(resources.res_sound_solve_puzzle)

            else:
                # crashing

                if ship.velocity.dot(pygame.Vector2(0, -1)) > 0 \
                        and ship.velocity.dot(pygame.Vector2(-1, 0)) > 0 \
                        and ship.velocity.dot(pygame.Vector2(1, 0)) > 0:
                    return  # ship is flying away from the platform

                self.sound_player.play_res(resources.res_sound_crash_platform, extra='mutex')
                # todo add particles, animations?

                if ship.position.y >= platform.rect.top:
                    # if ship.old_position.x < platform.rect.left <= ship.position.x:
                    if ship.rect.left < platform.rect.left < ship.rect.right:
                        ship.bump(pygame.Vector2(-50, -50))
                    # elif ship.position.x < platform.rect.right <= ship.old_position.x:
                    elif ship.rect.left < platform.rect.right <= ship.rect.right:
                        ship.bump(pygame.Vector2(50, -50))

                else:
                    ship.bump(pygame.Vector2(0, -abs(ship.acceleration.y / ship.max_acceleration)))

    def enter(self):
        pygame.mouse.set_visible(False)

        # self.ship = Ship(position=pygame.Vector2(0, 0), rect=pygame.Rect(0, 0, 60, 40), max_speed=300,
        #                  max_acceleration=20000, k=0.15, max_rot_vel=20000)
        # self.entities.append(LandingPlatform(pygame.Rect(400, 600, 100, 300)))
        # self.entities.append(City(pygame.Rect(0, settings.screen_height - 50, settings.screen_width, 50)))
        # wind0 = Wind(position=pygame.Vector2(40, 500), velocity=pygame.Vector2(-300, -100),
        #              rect=pygame.Rect(0, 0, 200, 300), layer=Ship.layer + 1)
        # wind1 = Wind(position=pygame.Vector2(40, 100), velocity=pygame.Vector2(300, 0),
        #              rect=pygame.Rect(0, 0, 200, 200),
        #              layer=Ship.layer - 1)
        # wind2 = Wind(position=pygame.Vector2(40, 500), velocity=pygame.Vector2(-500, 0),
        #              rect=pygame.Rect(0, 0, 300, 300), layer=Ship.layer - 1)
        # self.entities.append(wind0)
        # self.entities.append(wind1)
        # self.entities.append(wind2)
        #
        # swirl0 = Swirl(position=pygame.Vector2(240, 300), velocity=pygame.Vector2(300, 0),
        #                rect=pygame.Rect(0, 0, 200, 200), wind_rot_vel=500, rot_vel=35, layer=Ship.layer - 2)
        # swirl1 = Swirl(position=pygame.Vector2(240, 300), velocity=pygame.Vector2(100, 300),
        #                rect=pygame.Rect(0, 0, 200, 200), wind_rot_vel=-400, rot_vel=-40, layer=Ship.layer + 1)
        # self.entities.append(swirl0)
        # self.entities.append(swirl1)
        #
        # self.entities.append(self.ship)
        # self.entities.append(Sky(pygame.Rect(0, 0, settings.screen_width, settings.screen_height)))
        #
        # # alternate way to add a static sprite that does not move anything (could be an animation too I guess)
        # # sky_position = pygame.Vector2(settings.screen_width // 2, settings.screen_height // 2)
        # # self.renderer.add_static(resources.resource_red_background, sky_position, -100)
        # resource_map = {
        #     Ship.kind: [resources.res_img_red_p_ship],
        #     Wind.kind: [resources.res_img_red_p_wind],
        #     Swirl.kind: [resources.res_img_red_p_swirl],
        #     KIND_LANDING_PLATFORM: [resources.res_img_landing_platform],
        #     KIND_CITY: [resources.res_img_red_p_city],
        #     KIND_RED_BACKGROUND: [resources.res_img_red_p_background],
        # }

        self.entities = list(self.level_setup.entities)  # do not modify level_setup_entities!
        self.entities += self.level_setup.winds
        self.entities += self.level_setup.swirls
        self.entities.append(self.level_setup.ship)
        self.ship = self.level_setup.ship
        self.ship.reset()

        self.renderer.add_resource_map(self.level_setup.resource_map)

        for e in self.entities:
            self.renderer.add_entity(e)

        self.screen = pygame.display.get_surface()

    def exit(self):
        pass

    def resume(self):
        pygame.mouse.set_visible(False)

    def suspend(self):
        pass

    def update_step(self, delta, sim_time, *args):
        self.scheduler.update(delta, sim_time)
        self._handle_events()
        self.collider.check_all_collisions(self.entities)
        for ent in self.entities:
            ent.update(delta)

        if self.ship.is_landed and not self.level_progress.is_current_done():
            logger.info(">>>>>>>>>>> WIN <<<<<<<<<<<")
            self.level_progress.set_current_to_done()
            self.sound_player.play_res(resources.res_sound_solve_puzzle)
            self.scheduler.schedule(self.move_next, 3)

    def move_next(self, *args):
        if self.followup_context:
            self.exchange(self.followup_context,
                          effect=pyknic.pyknic_pygame.context.effects.FadeOutEffect(settings.effect_duration))
        else:
            self.pop(effect=pyknic.pyknic_pygame.context.effects.FadeOutEffect(settings.effect_duration))
        return 0

    @staticmethod
    def _updated_ship(entities):
        for screen_ship in entities:
            sprite: Sprite = screen_ship.sprites[resources.res_img_red_p_ship]
            sprite.rotation = -screen_ship.entity.rotation

    @staticmethod
    def _update_wind(entities):
        for screen_wind in entities:
            sprite: Sprite = screen_wind.sprites[resources.res_img_red_p_wind]
            sprite.rotation = screen_wind.entity.velocity.angle_to(pygame.Vector2(1, 0))

    @staticmethod
    def _update_swirl(entities: [_ScreenEntity]):
        for screen_swirl in entities:
            sprite: Sprite = screen_swirl.sprites[resources.res_img_red_p_swirl]
            sprite.rotation = -screen_swirl.entity.rotation

    def draw_step(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0, screen=None):
        self.screen = screen or pygame.display.get_surface()  # hack to get transitions working!!! AARRGGHHH!
        self.renderer.draw(self.screen, fill_color=(0, 0, 0), do_flip=False)

        # self.screen.fill((0, 0, 0))
        # for ent in self.entities:
        #     pygame.draw.rect(self.screen, pygame.Color('white'), ent.rect, 1)
        #     if hasattr(ent, 'rotation'):
        #         pygame.draw.line(self.screen, pygame.Color('red'), ent.position,
        #                          ent.position + pygame.Vector2(40, 0).rotate(ent.rotation), 3)
        #         pygame.draw.line(self.screen, pygame.Color('green'), ent.position,
        #                          ent.position + pygame.Vector2(40, 0).rotate(ent.rotation + 90), 3)
        #     if hasattr(ent, 'velocity'):
        #         pygame.draw.line(self.screen, pygame.Color('blue'), ent.position,
        #                          ent.position + ent.velocity * 1, 1)

        if do_flip:
            pygame.display.flip()

    def _handle_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.move_next()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    if __debug__:
                        self.ship.is_landed = False
                        self.ship.reset()
            elif event.type == settings.music_ended_pygame_event:
                self.music_player.on_music_ended()
        pressed_keys = pygame.key.get_pressed()
        pressed_right = pressed_keys[pygame.K_RIGHT] or pressed_keys[pygame.K_d]
        pressed_left = pressed_keys[pygame.K_LEFT] or pressed_keys[pygame.K_a]
        pressed_down = pressed_keys[pygame.K_DOWN] or pressed_keys[pygame.K_s]
        pressed_up = pressed_keys[pygame.K_UP] or pressed_keys[pygame.K_w]
        move_x_dir = pressed_right - pressed_left
        move_y_dir = pressed_down - pressed_up
        self.ship.move(move_x_dir, move_y_dir)


logger.debug("imported")
