# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'navigation.py' is part of pw-34
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Navigation context.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2022"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

import pygame

import pyknic
from gamelib import settings, strings, resources
from gamelib.context.cargo_planet import CargoPlanet
from gamelib.context.charging_station import ChargingStation
from gamelib.context.coronation import Coronation
from gamelib.entity_renderer import EntityRenderer
from gamelib.context.kings_planet import KingsPlanet, planet1_level, planet2_level, planet4_level, planet5_level, \
    planet3_level
from pyknic.pyknic_pygame import pygametext
# __all__ = []  # list of public visible parts of this module

from pyknic.pyknic_pygame.context import TimeSteppedContext

logger = logging.getLogger(__name__)
logger.debug("importing...")

KIND_PLANET0 = settings.global_id_generator.next("KIND_PLANET0")
KIND_PLANET1 = settings.global_id_generator.next()
KIND_PLANET2 = settings.global_id_generator.next()
KIND_PLANET3 = settings.global_id_generator.next()
KIND_PLANET4 = settings.global_id_generator.next()
KIND_PLANET5 = settings.global_id_generator.next()
KIND_SUN = settings.global_id_generator.next()
KIND_SPACE_BACKGROUND = settings.global_id_generator.next()

screen_w, screen_h = settings.screen_size


class Background:

    kind = KIND_SPACE_BACKGROUND


class Planet:

    layer = 100

    def __init__(self, position, kind, radius, message, callback=None):
        self.radius = radius
        self.kind = kind
        self.position = position
        self.has_focus = False
        self.progress_state = LevelProgress.Done
        self.message = message
        self.callback = callback

    def on_click(self):
        if self.callback:
            self.callback(self)


class DummyProgress:

    def __init__(self):
        self.done = False

    def set_current_to_done(self):
        self.done = True

    def is_current_done(self):
        return self.done


class LevelProgress:

    Done = 1
    Locked = 2
    Open = 3

    def __init__(self):
        self.progress = {
            KIND_PLANET0: self.Open,
            KIND_PLANET1: self.Locked,
            KIND_PLANET2: self.Locked,
            KIND_PLANET3: self.Locked,
            KIND_PLANET4: self.Locked,
            KIND_PLANET5: self.Locked,
        }
        self.current = None

    def set_current_to_done(self):
        if self.current:
            self.progress[self.current] = self.Done
            if self.current == KIND_PLANET0:
                self.progress[KIND_PLANET1] = self.Open
            elif self.current == KIND_PLANET1:
                self.progress[KIND_PLANET3] = self.Open
            # those others are never unlocked
            elif self.current == KIND_PLANET2:
                self.progress[KIND_PLANET4] = self.Open
            elif self.current == KIND_PLANET4:
                self.progress[KIND_PLANET5] = self.Open
            elif self.current == KIND_PLANET5:
                self.progress[KIND_PLANET3] = self.Open
        else:
            logger.error("current level is None, cannot set it to Done!")

    def is_current_done(self):
        if self.current:
            return self.progress[self.current] == self.Done

        logger.error("current level is None, cannot check if it is Done!")
        return False

    def is_game_ended(self):
        return self.progress[KIND_PLANET0] == self.Done and self.progress[KIND_PLANET1] == self.Done and self.progress[
            KIND_PLANET3] == self.Done

    # todo: if there is time a ship flying from one planet to another


# todo: animations?
class Navigation(TimeSteppedContext):

    def __init__(self, max_dt, step_dt, draw_fps, music_player, resource_manager, sound_player):
        TimeSteppedContext.__init__(self, max_dt, step_dt, draw_fps)
        self.sound_player = sound_player
        self.resource_manager = resource_manager
        self.music_player = music_player
        self.entities = []
        self.focus = None
        self.screen = None
        self.font: pygame.font.Font = None
        self.level_progress = LevelProgress()
        self.planets = []
        self.renderer: EntityRenderer = EntityRenderer(resource_manager)

        self.renderer.add_resource_map({
            KIND_PLANET0: [resources.res_img_planet0, resources.res_img_planet0_highlight],
            KIND_PLANET1: [resources.res_img_planet1, resources.res_img_planet1_highlight],
            KIND_PLANET2: [resources.res_img_planet2, resources.res_img_planet2_highlight],
            KIND_PLANET3: [resources.res_img_planet3, resources.res_img_planet3_highlight],
            KIND_PLANET4: [resources.res_img_planet4, resources.res_img_planet4_highlight],
            KIND_PLANET5: [resources.res_img_planet5, resources.res_img_planet5_highlight],
            KIND_SUN: [resources.res_img_sun],
            KIND_SPACE_BACKGROUND: [resources.res_img_space],
        })

        self.renderer.register_update_callbacks({
            KIND_PLANET0: self._update_planet,
            KIND_PLANET1: self._update_planet,
            KIND_PLANET2: self._update_planet,
            KIND_PLANET3: self._update_planet,
            KIND_PLANET4: self._update_planet,
            KIND_PLANET5: self._update_planet,
        })

    def _update_planet(self, screen_planets):
        for screen_planet in screen_planets:
            planet = screen_planet.entity
            resource_ids = self.renderer._kind_resource_map[planet.kind]  # hack
            highlight_sprite = screen_planet.sprites[resource_ids[1]]
            highlight_sprite.visible = planet.has_focus
            highlight_sprite.z_layer = screen_planet.sprites[resource_ids[0]].z_layer - 1

    def exit(self):
        pygame.mouse.set_cursor(pygame.SYSTEM_CURSOR_ARROW)

    def suspend(self):
        pygame.mouse.set_cursor(pygame.SYSTEM_CURSOR_ARROW)

    def enter(self):
        self.focus = None
        pygame.mouse.set_visible(True)
        if settings.progress_override:
            self.level_progress.progress[KIND_PLANET0] = settings.progress_override[0]
            self.level_progress.progress[KIND_PLANET1] = settings.progress_override[1]
            self.level_progress.progress[KIND_PLANET2] = settings.progress_override[2]
            self.level_progress.progress[KIND_PLANET3] = settings.progress_override[3]
            self.level_progress.progress[KIND_PLANET4] = settings.progress_override[4]
            self.level_progress.progress[KIND_PLANET5] = settings.progress_override[5]

        # self.resume()

    def resume(self):
        pygame.mouse.set_visible(True)
        self.focus = None
        self.screen = pygame.display.get_surface()
        self.font = pygame.font.Font(None, 30)
        pygame.event.clear()
        self.renderer.clear()
        planet0 = Planet(pygame.Vector2(620, 331), KIND_PLANET0, radius=36 / 2,
                         message=strings.messages.msg_planet0_message,
                         callback=self._callback_planet)
        planet1 = Planet(pygame.Vector2(320, 360), KIND_PLANET1, radius=56 / 2,
                         message=strings.messages.msg_planet1_message,
                         callback=self._callback_planet)
        planet2 = Planet(pygame.Vector2(413, 483), KIND_PLANET2, radius=40 / 2,
                         message=strings.messages.msg_planet2_message,
                         callback=self._callback_planet)
        the_red_planet3 = Planet(pygame.Vector2(919, 413), KIND_PLANET3, radius=66 / 2,
                                 message=strings.messages.msg_planet3_message,
                                 callback=self._callback_planet)
        planet4 = Planet(pygame.Vector2(138 + 0, 262 - 10), KIND_PLANET4, radius=82 / 2,
                         message=strings.messages.msg_planet4_message,
                         callback=self._callback_planet)
        planet5 = Planet(pygame.Vector2(360, 144), KIND_PLANET5, radius=54 / 2,
                         message=strings.messages.msg_planet5_message,
                         callback=self._callback_planet)

        sun = Planet(pygame.Vector2(screen_w // 2, screen_h // 2), KIND_SUN, radius=60 / 2, message="Sun")
        sun.progress_state = LevelProgress.Locked

        # background = Background()

        self.renderer.add_static(resources.res_img_space,
                                 pygame.Vector2(settings.screen_width // 2, settings.screen_height // 2), -100)

        planets = [planet0, planet1, planet2, planet4, planet5, the_red_planet3]
        for p in planets:
            p.progress_state = self.level_progress.progress[p.kind]

        # keep the red planet locked until all others are done
        if the_red_planet3.progress_state == LevelProgress.Locked:
            if all(p.progress_state == LevelProgress.Done for p in planets if p != the_red_planet3):
                the_red_planet3.progress_state = LevelProgress.Open

        self.entities = [sun] + planets
        self.planets = planets + [sun]

        for p in self.planets:
            self.renderer.add_entity(p)

    def _callback_planet(self, planet):
        logger.info(f"planet {planet.kind} has been clicked!")
        if planet.progress_state == LevelProgress.Locked:
            logger.info(f"planet is locked!")
            self.sound_player.play_res(resources.res_sound_planet_locked)
            return

        if planet.progress_state == LevelProgress.Done:
            logger.info(f"planet is done")
            self.sound_player.play_res(resources.res_sound_planet_done)
            return

        self.sound_player.play_res(resources.res_sound_planet_click)
        self.level_progress.current = planet.kind
        logger.info(f"travel to planet {planet.kind}")
        if planet.kind == KIND_PLANET0:
            cont = ChargingStation(settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS, self.music_player,
                                   self.resource_manager, self.level_progress, self.sound_player)
            self.push(cont, effect=pyknic.pyknic_pygame.context.effects.FadeOutEffect(settings.effect_duration))
        elif planet.kind == KIND_PLANET1:
            cont = CargoPlanet(settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS, self.music_player,
                               self.resource_manager, self.level_progress, self.sound_player)
            cont = KingsPlanet(settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS, self.music_player,
                               self.resource_manager, DummyProgress(), self.sound_player, planet1_level, cont)
            self.push(cont, effect=pyknic.pyknic_pygame.context.effects.FadeOutEffect(settings.effect_duration))
        elif planet.kind == KIND_PLANET2:
            # todo replace with corresponding context!
            cont = KingsPlanet(settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS, self.music_player,
                               self.resource_manager, self.level_progress, self.sound_player, planet2_level)
            self.push(cont, effect=pyknic.pyknic_pygame.context.effects.FadeOutEffect(settings.effect_duration))
        elif planet.kind == KIND_PLANET3:
            ## this is the RED planet!
            cont = KingsPlanet(settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS, self.music_player,
                               self.resource_manager, self.level_progress, self.sound_player, planet3_level)
            self.push(cont, effect=pyknic.pyknic_pygame.context.effects.FadeOutEffect(settings.effect_duration))
        elif planet.kind == KIND_PLANET4:
            cont = KingsPlanet(settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS, self.music_player,
                               self.resource_manager, self.level_progress, self.sound_player, planet4_level)
            self.push(cont, effect=pyknic.pyknic_pygame.context.effects.FadeOutEffect(settings.effect_duration))
        elif planet.kind == KIND_PLANET5:
            cont = KingsPlanet(settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS, self.music_player,
                               self.resource_manager, self.level_progress, self.sound_player, planet5_level)
            self.push(cont, effect=pyknic.pyknic_pygame.context.effects.FadeOutEffect(settings.effect_duration))

    def draw_step(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0, screen=None):
        self.screen = screen  # hack to get transitions working!!! AARRGGHHH!
        self.screen.fill((0, 0, 0))

        self.renderer.draw(self.screen)

        for ent in self.entities:
            if ent.kind == KIND_SUN:
                continue
            if ent.has_focus:
                if ent.progress_state == LevelProgress.Done:
                    message = f"{strings.messages.msg_done}\n{ent.message}"
                elif ent.progress_state == LevelProgress.Locked:
                    message = f"{strings.messages.msg_locked}\n{ent.message}"
                elif ent.progress_state == LevelProgress.Open:
                    message = f"{strings.messages.msg_open}\n{ent.message}"
                label = pygametext.getsurf(message)  # todo maybe use some other cool font and style...
                r = label.get_rect(midtop=ent.position + pygame.Vector2(0, 1) * 2 * ent.radius)
                rb = r.inflate(6, 6)
                rb.clamp_ip(self.screen.get_rect())
                r.center = rb.center
                self.screen.fill((0, 0, 0), rb)
                pygame.draw.rect(self.screen, pygame.Color("chartreuse2"), rb, 1)
                self.screen.blit(label, r)

        if do_flip:
            pygame.display.flip()

    def update_step(self, delta, sim_time, *args):
        self._handle_events()
        self.check_win()

    def check_win(self):
        if self.level_progress.is_game_ended():
            coronation = Coronation(settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS, self.music_player,
                                    self.resource_manager, self.level_progress, self.sound_player)
            self.exchange(coronation,
                          effect=pyknic.pyknic_pygame.context.effects.FadeOutEffect(settings.effect_duration))

    def _handle_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.pop(effect=pyknic.pyknic_pygame.context.effects.FadeOutEffect(settings.effect_duration))
            elif event.type == pygame.MOUSEMOTION:
                mouse_pos = pygame.Vector2(event.pos)
                new_focus = self.get_entity_at_position(mouse_pos)
                old_focus = self.focus
                if self.focus != new_focus:
                    if self.focus:
                        self.focus.has_focus = False
                    self.focus = new_focus
                    if new_focus:
                        new_focus.has_focus = True
                        if new_focus.progress_state == LevelProgress.Locked:
                            pygame.mouse.set_cursor(pygame.SYSTEM_CURSOR_NO)
                        elif new_focus.progress_state == LevelProgress.Open:
                            pygame.mouse.set_cursor(pygame.SYSTEM_CURSOR_HAND)
                        elif new_focus.progress_state == LevelProgress.Done:
                            pygame.mouse.set_cursor(pygame.SYSTEM_CURSOR_ARROW)
                    else:
                        pygame.mouse.set_cursor(pygame.SYSTEM_CURSOR_CROSSHAIR)
                    self._focus_changed(old_focus, new_focus)
            elif event.type == pygame.MOUSEBUTTONUP:
                entity = self.get_entity_at_position(pygame.Vector2(event.pos))
                if entity:
                    entity.on_click()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_e:
                    strings.activate_language("en")
                elif event.key == pygame.K_d:
                    strings.activate_language("de")
                elif event.key in (pygame.K_RETURN, pygame.K_SPACE):
                    pass
            elif event.type == settings.music_ended_pygame_event:
                self.music_player.on_music_ended()

    def get_entity_at_position(self, position):
        new_focus = None
        for ent in self.planets:  # (e for e in self.entities if e.is_eligible):
            if position.distance_to(ent.position) < ent.radius:
                new_focus = ent
                break
        return new_focus

    def _focus_changed(self, old_focus, new_focus):
        if old_focus is None and new_focus is not None:
            sound_map = {
                KIND_PLANET0: resources.res_sound_planet_0,
                KIND_PLANET1: resources.res_sound_planet_1,
                KIND_PLANET2: resources.res_sound_planet_2,
                KIND_PLANET3: resources.res_sound_planet_3,
                KIND_PLANET4: resources.res_sound_planet_4,
                KIND_PLANET5: resources.res_sound_planet_5,
                KIND_SUN: resources.res_sound_planet_star,
            }
            res_id = sound_map.get(new_focus.kind, None)
            if res_id:
                self.sound_player.play_res(res_id)


logger.debug("imported")
