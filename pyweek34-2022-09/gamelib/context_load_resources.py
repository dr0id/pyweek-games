# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'context_load_resources.py' is part of pw-30
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The resource loading context.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2020"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

import pygame

import pyknic
from pyknic.pyknic_pygame.resource2 import resource_manager, SpriteSheetLoader, FakeImageLoader, ImageLoader, \
    DirectoryLoader, MusicLoader, SoundDataLoader, FontLoader

from gamelib import settings
from pyknic.pyknic_pygame.context import TimeSteppedContext

logger = logging.getLogger(__name__)
logger.debug("importing...")


class ResourceLoaderContext(TimeSteppedContext):

    def __init__(self, resource_def):
        """
        Loads the resources in an own context...
        :param resource_def: the resources to load
        """
        TimeSteppedContext.__init__(self, settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS)
        self.resource_def = resource_def
        resource_manager.register_loader(SpriteSheetLoader(), True)
        resource_manager.register_loader(FakeImageLoader(), True)
        # resource_manager.register_loader(MapLoader(), True)
        resource_manager.register_loader(ImageLoader(), True)
        resource_manager.register_loader(DirectoryLoader(), True)
        resource_manager.register_loader(MusicLoader(), True)
        resource_manager.register_loader(SoundDataLoader(settings.MIXER_RESERVED_CHANNELS, settings.MIXER_NUM_CHANNELS),
                                         True)
        resource_manager.register_loader(FontLoader(), True)
        self.screen = None
        self._loaded = False

    def enter(self):
        logger.info("clearing resources")
        resource_manager.resources.clear()
        self.screen = pygame.display.get_surface()

    def draw_step(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0, screen=None):
        pass

    def update_step(self, delta, sim_time, *args):
        if not self._loaded:  # execute only once, in rare occasions this is called multiple times!
            logger.info("loading resources")
            resource_manager.load_resources(self.resource_def, self.progress_cb)
            self.pop(effect=pyknic.pyknic_pygame.context.effects.FadeOutEffect(settings.effect_duration))
            self._loaded = True

    def progress_cb(self, *args):
        idx, total, percent, res_id, res = args
        logger.info(f"RESOURCE PROGRESS: {idx}/{total}, {percent * 100}%, res_id {res_id}, res {res}")
        if self.screen:
            self.screen.fill((0, 0, 0))
            r_w = settings.screen_width
            r_h = 50
            r = pygame.Rect(0, 0, int(r_w * percent), r_h)
            r.bottomleft = (0, settings.screen_height - 100)
            a = pygame.Color("darkorange")
            b = pygame.Color("red2")
            color = a.lerp(b, percent)
            pygame.draw.rect(self.screen, color, r)
            r.width = r_w
            # r.midbottom = (settings.screen_width // 2, settings.screen_height - 100)
            pygame.draw.rect(self.screen, (180, 180, 180), r, 1)
            pygame.display.flip()


logger.debug("imported")
