# -*- coding: utf-8 -*-
"""
This it the main module. This is the main entry point of your code (put in the main() method).
"""
from __future__ import division, print_function

import logging
import random

import pygame

from gamelib import settings, resources
from gamelib.context.credits import Credits
from gamelib.context.game_setup import GameSetup
from gamelib.context.navigation import Navigation
from gamelib.context_load_resources import ResourceLoaderContext
from gamelib.resources import resource_def
from pyknic import context
from pyknic.pyknic_pygame.context import PygameInitContext, AppInitContext
from pyknic.pyknic_pygame.resource2 import resource_manager
from pyknic.pyknic_pygame.sfx import MusicPlayer, SoundPlayer

logger = logging.getLogger(__name__)


def main(log_level):
    # put here your code
    import pyknic
    logging.getLogger().setLevel(log_level)
    pyknic.logs.print_logging_hierarchy()

    random_state = random.getstate()
    logger.info(f"Set random seed state to: {random_state}")

    context.push(PygameInitContext(_custom_display_init))
    music_player = MusicPlayer(settings.master_volume, settings.music_ended_pygame_event)
    sound_player = SoundPlayer(0.5, resource_manager)
    context.push(AppInitContext(music_player, settings.path_to_icon, settings.title))
    #
    # # order of context
    # context.push(OutroContext(settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS, music_player))
    # context.push(GameContext(settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS, music_player))
    # if not settings.GOTO_LEVELS:
    #     context.push(IntroContext(music_player))
    credits_context = Credits(settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS, music_player, resource_manager,
                              sound_player)
    context.push(credits_context)
    navigation = Navigation(settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS, music_player, resource_manager,
                            sound_player)
    context.push(navigation)
    if not settings.skip_game_setup:
        game_setup = GameSetup(settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS, music_player, resource_manager,
                               sound_player)
        context.push(game_setup)
    context.push(ResourceLoaderContext(resource_def))

    music_player.fill_music_carousel([
        # this works because the music resource is actually only a filepath and
        # nothing has to be loaded because the music is streamed
        resources.resource_def[resources.resource_music_1],
        resources.resource_def[resources.resource_music_2],
        resources.resource_def[resources.resource_music_3],
    ])
    music_player.start_music_carousel()

    screen = pygame.display.get_surface()  # hack!
    clock = pygame.time.Clock()
    sim_time = 0.0
    context.set_deferred_mode(True)
    while context.length():
        top = context.top()
        if top:
            dt = clock.tick()
            dt /= 1000.0  # convert to seconds
            sim_time += dt
            top.update(dt, sim_time)
            top.draw(screen)
        context.update()  # handle deferred context operations

    logger.debug('Finished. Exiting.')


def _custom_display_init(display_info, driver_info, wm_info):
    logger.info("driver info:  {0}", driver_info)
    logger.info("wm_info:  {0}", wm_info)
    accommodated_height = int(display_info.current_h * 0.9)
    if accommodated_height < settings.screen_height:
        logger.info("accommodating height to {0}", accommodated_height)
        settings.screen_height = accommodated_height  # accommodate for title bar and task bar ?
    settings.screen_size = settings.screen_width, settings.screen_height
    logger.info("using screen size: {0}", settings.screen_size)

    # initialize the screen
    settings.screen = pygame.display.set_mode(settings.screen_size, settings.screen_flags)

    # mixer values to return
    frequency = settings.MIXER_FREQUENCY
    channels = settings.MIXER_CHANNELS
    mixer_size = settings.MIXER_SIZE
    buffer_size = settings.MIXER_BUFFER_SIZE
    # pygame.mixer.pre_init(frequency, mixer_size, channels, buffer_size)
    return frequency, mixer_size, channels, buffer_size


# this is needed in order to work with py2exe
if __name__ == '__main__':
    main(settings.log_level)
