# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'resources.py' is part of pw-33
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Just holds the list of resources to load.

"""
from __future__ import print_function, division

import logging

import pygame

from gamelib import settings
from pyknic.pyknic_pygame.resource2 import AlphaTransform, TransparentTransform, resource_manager, RotateTransform, \
    ResizeTransform, TintTransform
from pyknic.pyknic_pygame.sfx import SoundData, MusicData
from pyknic.resource2 import FakeImage, Image, SpriteSheetConfig, AbstractResourceConfig, AbstractResourceLoader, \
    AbstractResource

logger = logging.getLogger(__name__)
logger.debug("importing...")

_global_id_generator = settings.global_id_generator


def get_generator_name(gen):
    return _global_id_generator.names.get(gen, "?")


class FakeSoundData(AbstractResourceConfig):
    def __init__(self, *args, **kwargs):
        AbstractResourceConfig.__init__(self, None)
        pass


class FakeSoundResource(AbstractResource):

    def __init__(self, resource_config):
        AbstractResource.__init__(self, resource_config)


class FakeSoundLoader(AbstractResourceLoader):

    def __init__(self):
        AbstractResourceLoader.__init__(self, FakeSoundData)

    def load(self, resource_config, file_cache):
        return FakeSoundResource(resource_config)

    def unload(self, resource_config):
        pass


resource_manager.register_loader(FakeSoundLoader(), True)

# ------------------------------------------------------------------------------
# ResourcesIds
# ------------------------------------------------------------------------------
_first_resource_id = _global_id_generator.next('_first_resource_id')

# item resources'
res_img_red_p_ship = _global_id_generator.next('res_img_red_p_ship')
res_img_landing_platform = _global_id_generator.next()
res_img_red_p_city = _global_id_generator.next()
res_img_red_p_wind = _global_id_generator.next()
res_img_red_p_swirl = _global_id_generator.next()
res_img_red_p_background = _global_id_generator.next()
res_img_green_p_background = _global_id_generator.next()
res_img_blue_p_background = _global_id_generator.next()
res_img_violette_p_background = _global_id_generator.next()
res_img_ice_p_background = _global_id_generator.next()

res_img_planet0 = _global_id_generator.next('res_img_planet0')
res_img_planet1 = _global_id_generator.next()
res_img_planet2 = _global_id_generator.next()
res_img_planet3 = _global_id_generator.next()
res_img_planet4 = _global_id_generator.next()
res_img_planet5 = _global_id_generator.next()
res_img_planet0_highlight = _global_id_generator.next('res_img_planet0_highlight')
res_img_planet1_highlight = _global_id_generator.next()
res_img_planet2_highlight = _global_id_generator.next()
res_img_planet3_highlight = _global_id_generator.next()
res_img_planet4_highlight = _global_id_generator.next()
res_img_planet5_highlight = _global_id_generator.next()
res_img_sun = _global_id_generator.next('res_img_sun')
res_img_star = _global_id_generator.next()
res_img_space = _global_id_generator.next()
res_img_nebula = _global_id_generator.next()
res_img_charging_station_background = _global_id_generator.next()
res_img_mirror = _global_id_generator.next()

res_img_trunk = _global_id_generator.next()
res_img_overlap = _global_id_generator.next()
res_img_spaceship_trunk = _global_id_generator.next()
res_img_piece0 = _global_id_generator.next('res_img_piece0')
res_img_piece1 = _global_id_generator.next()
res_img_piece2 = _global_id_generator.next()
res_img_piece3 = _global_id_generator.next()
res_img_piece4 = _global_id_generator.next()

res_img_crowns = _global_id_generator.next('crowns')

# sfx resources
res_sound_crash_city = _global_id_generator.next('resource_sound_crash_city')
res_sound_crash_platform = _global_id_generator.next('crash platform')
res_sound_wind0 = _global_id_generator.next()
res_sound_wind1 = _global_id_generator.next()
res_sound_wind2 = _global_id_generator.next()
res_sound_swirl0 = _global_id_generator.next('swirl0')
res_sound_swirl1 = _global_id_generator.next()
res_sound_swirl2 = _global_id_generator.next()
res_sound_landed = _global_id_generator.next('res_sound_landed')

res_sound_solve_puzzle = _global_id_generator.next('res_sound_solve_puzzle')
res_sound_planet_locked = _global_id_generator.next('res_sound_planet_locked')
res_sound_planet_done = _global_id_generator.next('res_sound_planet_done')
res_sound_planet_click = _global_id_generator.next('res_sound_planet_click')
res_sound_planet_0 = _global_id_generator.next('res_sound_planet_0')
res_sound_planet_1 = _global_id_generator.next('res_sound_planet_1')
res_sound_planet_2 = _global_id_generator.next('res_sound_planet_2')
res_sound_planet_3 = _global_id_generator.next('res_sound_planet_3')
res_sound_planet_4 = _global_id_generator.next('res_sound_planet_4')
res_sound_planet_5 = _global_id_generator.next('res_sound_planet_5')
res_sound_planet_star = _global_id_generator.next()
res_sound_laser_up = _global_id_generator.next()
res_sound_laser_down = _global_id_generator.next()
res_sound_cargo_good = _global_id_generator.next()
res_sound_cargo_bad = _global_id_generator.next()

resource_music_1 = _global_id_generator.next('resource_music_1')
resource_music_2 = _global_id_generator.next('resource_music_2')
resource_music_3 = _global_id_generator.next('resource_music_3')

# ------------------------------------------------------------------------------
# Resources
# ------------------------------------------------------------------------------

alpha_transform = AlphaTransform()
alpha_transparent_transform = AlphaTransform().then(TransparentTransform(128))
resource_def_image = {
    # 523, 484 -> 60,
    res_img_red_p_ship: Image("data/gfx/spaceship/spaceship.png",
                              AlphaTransform().then(ResizeTransform(70, 65))),
    # FakeImage(100, 60, tuple(pygame.Color("blue")), "ship", alpha_transform),
    res_img_landing_platform: Image("data/gfx/platform.png", AlphaTransform()),
    # FakeImage(100, 300, tuple(pygame.Color("green")), "landing platform", alpha_transform),
    res_img_red_p_city: Image("data/gfx/city.png", alpha_transform),
    # FakeImage(1000, 50, tuple(pygame.Color("yellow")), "city", alpha_transform),
    res_img_red_p_wind: Image("data/gfx/wind.png", AlphaTransform()),
    # FakeImage(300, 300, tuple(pygame.Color("lightblue")), "wind", alpha_transparent_transform),
    res_img_red_p_swirl: Image("data/gfx/swirl.png", AlphaTransform()),
    # FakeImage(200, 200, tuple(pygame.Color("gray60")), "swirl", alpha_transparent_transform),
    res_img_red_p_background: Image("data/gfx/sky.png", AlphaTransform().then(TintTransform((255, 128, 128)))),
    # FakeImage(1024, 768, tuple(pygame.Color("darkorange2")), "red sky", alpha_transform),
    res_img_green_p_background: Image("data/gfx/sky.png", AlphaTransform().then(TintTransform((128, 255, 128)))),
    # FakeImage(1024, 768, tuple(pygame.Color("darkorange2")), "red sky", alpha_transform),
    res_img_blue_p_background: Image("data/gfx/sky.png", AlphaTransform().then(TintTransform((128, 128, 255)))),
    # FakeImage(1024, 768, tuple(pygame.Color("darkorange2")), "red sky", alpha_transform),
    res_img_violette_p_background: Image("data/gfx/sky.png", AlphaTransform().then(TintTransform((72, 0, 255)))),
    # FakeImage(1024, 768, tuple(pygame.Color("darkorange2")), "red sky", alpha_transform),
    res_img_ice_p_background: Image("data/gfx/sky.png", AlphaTransform().then(TintTransform((128, 128, 128)))),
    # FakeImage(1024, 768, tuple(pygame.Color("darkorange2")), "red sky", alpha_transform),

    res_img_planet0: Image("data/gfx/planets/planet_12.png",
                           AlphaTransform().then(RotateTransform(180)).then(ResizeTransform(40, 40))),
    res_img_planet1: Image("data/gfx/planets/planet_10.png",
                           AlphaTransform().then(RotateTransform(-145)).then(ResizeTransform(80, 80))),
    res_img_planet2: Image("data/gfx/planets/planet_01.png",
                           AlphaTransform().then(RotateTransform(-180)).then(ResizeTransform(50, 50))),
    res_img_planet3: Image("data/gfx/planets/planet_02.png",
                           AlphaTransform().then(RotateTransform(45)).then(ResizeTransform(80, 80))),
    res_img_planet4: Image("data/gfx/planets/planet_17.png",
                           AlphaTransform().then(RotateTransform(150)).then(ResizeTransform(180, 180))),
    res_img_planet5: Image("data/gfx/planets/planet_07.png",
                           AlphaTransform().then(RotateTransform(170)).then(ResizeTransform(50, 50))),
    res_img_planet0_highlight: Image("data/gfx/sun/sun.png", AlphaTransform().then(ResizeTransform(160, 160))),
    res_img_planet1_highlight: Image("data/gfx/sun/sun.png", AlphaTransform().then(ResizeTransform(230, 230))),
    res_img_planet2_highlight: Image("data/gfx/sun/sun.png", AlphaTransform().then(ResizeTransform(200, 200))),
    res_img_planet3_highlight: Image("data/gfx/sun/sun.png", AlphaTransform().then(ResizeTransform(230, 230))),
    res_img_planet4_highlight: Image("data/gfx/sun/sun.png", AlphaTransform().then(ResizeTransform(300, 300))),
    res_img_planet5_highlight: Image("data/gfx/sun/sun.png", AlphaTransform().then(ResizeTransform(160, 160))),
    res_img_sun: Image("data/gfx/sun/sun.png", AlphaTransform().then(ResizeTransform(250, 250))),
    res_img_star: FakeImage(5, 5, tuple(pygame.Color("white")), "star", alpha_transform),
    # res_img_space: FakeImage(1024, 768, tuple(pygame.Color("darkblue")), "dark blue sky", alpha_transform),
    res_img_space: Image("data/gfx/starsystem.png", AlphaTransform()),
    # res_img_nebula: FakeImage(512, 480, tuple(pygame.Color("blueviolet")), "nebula", alpha_transform),
    res_img_charging_station_background: Image("data/gfx/chargingstationbackground.png", AlphaTransform()),
    res_img_mirror: Image("data/gfx/mirror.png", AlphaTransform()),

    res_img_trunk: Image("data/gfx/trunk.png", AlphaTransform()),
    res_img_overlap: Image("data/gfx/piecesoverlap.png", AlphaTransform()),
    res_img_spaceship_trunk: Image("data/gfx/spaceshiptrunk.png", AlphaTransform()),
    res_img_piece0: Image("data/gfx/piece0.png", AlphaTransform()),
    res_img_piece1: Image("data/gfx/piece1.png", AlphaTransform()),
    res_img_piece2: Image("data/gfx/piece2.png", AlphaTransform()),
    res_img_piece3: Image("data/gfx/piece3.png", AlphaTransform()),
    res_img_piece4: Image("data/gfx/piece4.png", AlphaTransform()),

    res_img_crowns: SpriteSheetConfig("data/gfx/crowns/crowns.png", (256, 186), 1, 2, 0,
                                      transformation=AlphaTransform()),

}

resource_def_sound = {
    res_sound_crash_city: SoundData('data/sfx/crash_city.ogg', 0.4, reserved_channel_id=settings.channel_crash),
    res_sound_planet_done: SoundData('data/sfx/planet_done.ogg', 0.4),
    res_sound_planet_locked: SoundData('data/sfx/planet_locked.ogg', 0.4),
    res_sound_planet_click: SoundData('data/sfx/planet_click.ogg', 0.4),
    res_sound_planet_0: SoundData('data/sfx/planet_0.ogg', 0.4),
    res_sound_planet_1: SoundData('data/sfx/planet_2.ogg', 0.4),
    res_sound_planet_2: SoundData('data/sfx/planet_0.ogg', 0.4),
    res_sound_planet_3: SoundData('data/sfx/planet_2.ogg', 0.4),
    res_sound_planet_4: SoundData('data/sfx/planet_0.ogg', 0.4),
    res_sound_planet_5: SoundData('data/sfx/planet_2.ogg', 0.4),
    res_sound_planet_star: SoundData('data/sfx/planet_1.ogg', 0.4),

    res_sound_solve_puzzle: SoundData('data/sfx/solve_puzzle.ogg', 0.4),

    res_sound_crash_platform: SoundData('data/sfx/crash_platform.ogg', 0.4,
                                        reserved_channel_id=settings.channel_other1),
    res_sound_wind0: SoundData('data/sfx/wind/Wind.ogg', 1.0, reserved_channel_id=settings.channel_wind0),
    # FakeSoundData('', 0.4),
    res_sound_wind1: SoundData('data/sfx/wind/Wind2.ogg', 1.0, reserved_channel_id=settings.channel_wind1),
    # FakeSoundData('', 0.4),
    res_sound_wind2: SoundData('data/sfx/wind/Wind3.ogg', 1.0, reserved_channel_id=settings.channel_wind2),
    # FakeSoundData('', 0.4),
    res_sound_swirl0: FakeSoundData('', 0.4),
    res_sound_swirl1: FakeSoundData('', 0.4),
    res_sound_swirl2: FakeSoundData('', 0.4),
    res_sound_landed: FakeSoundData('', 0.4),
    res_sound_laser_up: FakeSoundData('', 0.4),
    res_sound_laser_down: FakeSoundData('', 0.4),
    res_sound_cargo_good: FakeSoundData('data/sfx/cargo_good.ogg', 0.4),
    res_sound_cargo_bad: FakeSoundData('data/sfx/cargo_bad.ogg', 0.4),

}

resource_def_music = {
    resource_music_1: MusicData('data/music/Bumba Crossing.ogg', 0.4),
    resource_music_2: MusicData('data/music/Anamalie.ogg', 0.4),
    resource_music_3: MusicData('data/music/Anguish.ogg', 0.4),
}

resource_def = resource_def_image | resource_def_sound | resource_def_music

logger.debug("imported")

if __name__ == '__main__':
    logger.setLevel(logging.INFO)
    # print all missing resources
    logger.error("")
    logger.error("Fake resources:")
    for res_id, res in resource_def.items():
        if isinstance(res, FakeImage):
            logger.error(f" FakeImage Id: {res_id}  name: {res.name}  size: {res.width}x{res.height} ")

    logger.error("")
    logger.error("Used image files:")
    images = []
    for res_id, res in resource_def.items():
        if isinstance(res, Image) or isinstance(res, SpriteSheetConfig):
            images.append((res_id, res))
    for res_id, res in sorted(images, key=lambda i: i[1].path_to_file.lower()):
        logger.error(f"  Image: id: {res_id} filename: {res.path_to_file}")

    logger.error("")
    logger.error("Used sound files:")
    for res_id, res in resource_def.items():
        if isinstance(res, SoundData):
            logger.error(f"  Sound: id {res_id} filename: {res.filename}")

    logger.error("")
    logger.error("Used music files:")
    for res_id, res in resource_def.items():
        if isinstance(res, MusicData):
            logger.error(f"  Music: id {res_id} filename: {res.filename}")

    logger.error("")
    logger.error("Unused resource ids:")
    for key in _global_id_generator.names.keys():
        if key <= _first_resource_id:
            continue
        if key not in resource_def.keys():
            logger.error("Missing resource id in resource def: {0} ({1})", key, get_generator_name(key))

    logger.error("")
    logger.error("compare")
    logger.error("id generator  <->  resource_def")
    for key, name in _global_id_generator.names.items():
        if key <= _first_resource_id:
            continue
        # logger.error(f"{(key, name)}  <->  {(key, resource_def.get(key, 'not found'))}")
        if key in resource_def.keys():
            logger.error(f"{(key, name)}  <->  {(key, resource_def.get(key, 'not found'))}")
        else:
            logger.error(f"{(key, name)}  <->  ???")
