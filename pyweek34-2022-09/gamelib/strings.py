# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'strings.py' is part of pw-34
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
All the string resources.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import gettext
import logging
import os
import pathlib

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(__d) for __d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2022"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")

domain = "strings"
locale_dir = os.path.join('.', 'data', 'locales')


class __Messages:

    def __init__(self):
        self._t = gettext.gettext  # can't rename '_t' without adapting it in the main script!!
        self._update()

    def _update(self):
        # !!!! IMPORTANT !!!!
        # Do not change this strings! (otherwise the id change and the merge is trickier)
        #
        # Change a string
        #   If you want to change a text, please do in the corresponding file strings.po in
        #   the data/locales/**/ directory!
        #   After changing the *.po file please run strings.py (this script) within this directory and choose option 3.
        #
        # Add a string
        #   Adding a string at the bottom of this list the same way as the others.
        #   After adding a new string, run this script (strings.py) and run options in that order:
        #      1, 2, then merge/update all *.po files manually, 3
        #
        # !!!! IMPORTANT !!!!
        self.msg_planet0_message = self._t("'I want to be king!' \n"
                                           "You have to prove yourself that you are worth to be a king.\n"
                                           "If you get to the 'The red planet' then you will be the king.\n\n"
                                           "The first challenge will be to get of this planet!")
        self.msg_planet1_message = self._t("Pick up the cargo for the next task.")
        self.msg_planet2_message = self._t("Feed the animals.")
        self.msg_planet3_message = self._t("'The red planet'\n"
                                           "Land on it and you will be king.")
        self.msg_planet4_message = self._t("Inhabited planet. There is nothing here.")
        self.msg_planet5_message = self._t("Inhabited planet. Nothing but desert.")
        self.msg_done = self._t("Done.")
        self.msg_locked = self._t("Unreachable.")
        self.msg_open = self._t("Reachable.")
        self.setup_start = self._t("Go")
        self.setup_instructions = self._t(
            "Welcome!\n\nInstructions:\n\nIf you see a mouse pointer, use the mouse (left and right click, drag and drop).\n\nOtherwise arrow keys or asdf keys to move around.\n")
        self.credits_quit = self._t("Quit")
        self.credits_info = self._t("Thanks for playing.\n\nThis game was made for pyweek 34\n\nSeptember 2022\n")
        # !!!! IMPORTANT !!!! => read note above

    def set_translation_method(self, func):
        self._t = func
        self._update()


messages = __Messages()


def activate_language(language):
    logger.info("activating language: {0}", language)
    os.environ['LANGUAGE'] = language
    global messages
    # if language == "en":
    #     _t = gettext.gettext
    #     messages.set_translation_method(_t)
    # else:
    locale_dir_path = os.path.normpath(os.path.join("data", "locales"))
    translation = gettext.translation(domain, localedir=locale_dir_path, languages=[language], fallback=False)
    # translation.install()
    _t = translation.gettext
    # global messages
    messages.set_translation_method(_t)


def get_available_languages():
    import glob
    messages_dirs = glob.glob(os.path.join("**", "LC_MESSAGES"), root_dir=locale_dir, recursive=True)
    found_lang_codes = [pathlib.Path(_p).parts[0] for _p in messages_dirs]
    # found_lang_codes.append('en')  # this is the source language
    return found_lang_codes


logger.info("Available languages: {0}", get_available_languages())

logger.debug("imported")

if __name__ == '__main__':
    logging.basicConfig()
    import sys
    import os
    import subprocess
    import shutil
    import glob

    locale_dir = os.path.join('..', 'data', 'locales')

    print("1 - extract pot")
    print("2 - copy pot to languages")
    print("3 - compile po file to mo files")
    print("4 - load present languages")
    print("q - quit")
    choice = input("choice:")
    while choice != 'q':
        print(choice)

        source_fpath = os.path.join(locale_dir, domain + '.pot')
        p, exe = os.path.split(sys.executable)
        script_dir = os.path.join(p, 'Tools', 'i18n')

        if choice == '1':
            # step 1: fun pygettext.py on the modules/package with domain
            script = os.path.join(script_dir, 'pygettext.py')
            args = [sys.executable, script, '-d', domain, '-o', source_fpath, '-k', '_t',
                    os.path.join('.', 'strings.py')]

            print("cwd", os.getcwd())
            print(script, args)
            result = subprocess.check_call(args)
            print("result", result)

        elif choice == '2':
            # step 2: copy template *.pot file to a *.po file in the desired languages once (not overwrite of the files)
            languages = ['de', 'en']
            for lang in languages:
                destination_path = os.path.join(locale_dir, lang, "LC_MESSAGES")
                destination_fpath = os.path.join(destination_path, domain + '.po')
                if not os.path.exists(destination_path):
                    os.makedirs(destination_path, exist_ok=True)
                if not os.path.exists(destination_fpath):
                    shutil.copy(source_fpath, destination_fpath)
                else:
                    shutil.copy(source_fpath, destination_fpath + ".new")

        elif choice == '3':
            # step 3: translate/update the *.po files! (merge with existing translations!?)

            # step 4: convert the *.po file into *.mo files
            script_msgfmt_py = os.path.join(script_dir, 'msgfmt.py')
            import glob

            po_file_name = os.path.join("**", domain + '.po')
            print("glob", po_file_name, locale_dir)
            po_files = glob.glob(po_file_name, root_dir=locale_dir, recursive=True)
            print("po files: ", po_files)
            for po_file in po_files:
                po_file = os.path.join(locale_dir, po_file)
                out_mo_file = os.path.join(os.path.split(po_file)[0], domain + ".mo")
                in_po_file = po_file
                args = [sys.executable, script_msgfmt_py, '-o', out_mo_file, in_po_file]
                print("args", args)
                print("msgfm.py", subprocess.check_call(args))

        elif choice == '4':
            # load test: find the *.mo files
            lc_messages_dirs = glob.glob(os.path.join("**", "LC_MESSAGES"), root_dir=locale_dir, recursive=True)
            lang_codes = [pathlib.Path(p).parts[0] for p in lc_messages_dirs]
            print("lang_codes:", lang_codes)
            os.environ['LANGUAGE'] = ":".join(lang_codes)
            result = gettext.find(domain, localedir=locale_dir, all=True)
            # result = gettext.find(domain, localedir=locale_dir, languages=None, all=True)
            print("gettest find all: ", result)

        choice = input("choice:")

    print(choice)
    print("quit")
