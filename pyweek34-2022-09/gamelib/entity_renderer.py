# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'entity_renderer.py' is part of pw-34
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
EntityRenderer, a new way to draw the entities.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import itertools
import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2022"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

logger = logging.getLogger(__name__)
logger.debug("importing...")

# __all__ = []  # list of public visible parts of this module

import uuid
import pygame
from gamelib import settings
from pyknic.pyknic_pygame.resource2 import FakeImageResource, ImageResource
from pyknic.pyknic_pygame.spritesystem import Sprite, DefaultRenderer, Camera


class _ScreenEntity:

    def __init__(self, entity, layer, kind, identifier):
        self.id = identifier or uuid.uuid4()
        self.entity = entity
        self.sprites = {}  # {resource_id: sprite}
        self.layer = layer
        self.kind = kind


class EntityRenderer:

    def __init__(self, resource_manager):
        self._resource_manager = resource_manager
        self._update_callbacks = {}
        self._kind_resource_map = {}  # {kind:{resource_id}}
        self._screen_entities = set()
        self._screen_entities_dirty = True
        self._sprite_system = DefaultRenderer()
        self._cam = Camera(pygame.Rect(0, 0, settings.screen_width, settings.screen_height))
        self._grouped_callbacks = []  # [(callback, group),]

    def clear(self):
        self._screen_entities.clear()
        self._screen_entities_dirty = True
        self._sprite_system.clear()

    def add_resource_map_for_kind(self, kind, resource_ids):
        self._kind_resource_map[kind] = list(resource_ids)

    def add_resource_map(self, resource_map):
        for kind, resource_ids in resource_map.items():
            self.add_resource_map_for_kind(kind, resource_ids)

    def register_update_callback(self, kind, callback):
        if kind in self._update_callbacks.keys():
            raise Exception(f"kind is already registered {kind}")
        self._update_callbacks[kind] = callback

    def register_update_callbacks(self, callbacks):
        for kind, callback in callbacks.items():
            self.register_update_callback(kind, callback)

    def add_static(self, resource_id, position, layer, identifier=None):
        screen_entity = _ScreenEntity(None, layer, None, identifier)
        self._screen_entities.add(screen_entity)
        self._screen_entities_dirty = True

        sprite = self._get_sprite_from_resource(resource_id, position, layer)
        sprite.layer = layer
        sprite.visible = True
        screen_entity.sprites[resource_id] = sprite

        self._sprite_system.add_sprites(screen_entity.sprites.values())
        return screen_entity.id

    def remove_static_by_id(self, identifier):
        self.remove_entity_by_id(identifier)

    def remove_entity(self, entity):
        for se in [e for e in self._screen_entities if e.entity == entity]:
            self._screen_entities.remove(se)
            self._screen_entities_dirty = True

    def remove_entity_by_id(self, identifier):
        for se in [e for e in self._screen_entities if e.id == identifier]:
            self._screen_entities.remove(se)
            self._screen_entities_dirty = True

    def add_entity(self, entity, identifier=None, visible_resource_id=None):
        if __debug__:
            if entity in (e.entity for e in self._screen_entities if e.entity == entity):
                raise Exception(f"entity of kind '{entity.kind}' already added")

        screen_entity = _ScreenEntity(entity, entity.layer, entity.kind, identifier)
        self._screen_entities.add(screen_entity)
        self._screen_entities_dirty = True

        resource_ids = self._kind_resource_map[entity.kind]
        if not visible_resource_id:
            visible_resource_id = resource_ids[0]

        for resource_id in resource_ids:
            sprite = self._get_sprite_from_resource(resource_id, entity.position, entity.layer)
            sprite.layer = entity.layer
            sprite.visible = True if visible_resource_id == resource_id else False
            screen_entity.sprites[resource_id] = sprite

        self._sprite_system.add_sprites(screen_entity.sprites.values())
        return screen_entity.id

    def _get_sprite_from_resource(self, resource_id, position, layer):  # todo this should be provided externally?
        res = self._resource_manager.resources.get(resource_id, None)
        if isinstance(res, FakeImageResource) or isinstance(res, ImageResource):
            return Sprite(res.image, position, z_layer=layer)

        logger.warning("Could not convert resource {0}", res.__class__.__name__)
        if __debug__:
            raise NotImplementedError(f"Could not convert resource {res.__class__.__name__}")

    def draw(self, screen, fill_color=None, do_flip=False, fill_flags=0):

        if self._screen_entities_dirty:
            self._screen_entities_dirty = False
            grouped_screen_entities = itertools.groupby(self._screen_entities, key=self._entity_group_key)
            grouped_callbacks = ((self._update_callbacks.get(k, None), g) for k, g in grouped_screen_entities)
            self._grouped_callbacks = [(cb, list(g)) for cb, g in grouped_callbacks if cb is not None]

        for cb, g in self._grouped_callbacks:
            cb(g)
        # for kind, grouped_entities in self._grouped_screen_entities:
        #     update_callback = self._update_callbacks.get(kind, None)
        #     if update_callback is not None:
        #         update_callback(grouped_entities)
        #     if __debug__:
        #         if update_callback is None:
        #             for entity in grouped_entities:
        #                 logger.error("no update_callback found for: {0} (kind: {1})", entity, entity.kind)

        self._sprite_system.draw(screen, self._cam, fill_color=fill_color, do_flip=do_flip, fill_flags=fill_flags)

    @staticmethod
    def _entity_group_key(entity):
        return entity.kind


logger.debug("imported")
