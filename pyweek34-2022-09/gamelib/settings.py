# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'settings.py' is part of pw-33
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The settings of the game.
"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2022"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module... not practical in settings.py!
import pygame
# from gamelib import imagecache
from pyknic.generators import GlobalIdGenerator

from pyknic.pyknic_pygame import pygametext

logger = logging.getLogger(__name__)
logger.debug("importing...")

# --------------------------------------------------------------------------------
# IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT
# --------------------------------------------------------------------------------
#
# This file contains the distributed default settings.
#
# Don't override settings by editing this file, else they will end up in the repo.
# Instead, see the DEBUGS section at the end of this file for instructions on
# keeping your custom settings out of the repo.
#
# --------------------------------------------------------------------------------
# IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT
# --------------------------------------------------------------------------------


#
#
# # ------------------------------------------------------------------------------
# # Tunables
# # ------------------------------------------------------------------------------
#
# print_fps = False
max_fps = 60
master_volume = 0.5  # min: 0.0; max: 1.0
music_volume = 1.0  # resulting level = master_volume * music_volume
sfx_volume = 1.0  # resulting level = master_volume * sfx_volume
log_level = [logging.WARNING, logging.DEBUG, logging.INFO][1]

#
#
# # ------------------------------------------------------------------------------
# # Display
# # ------------------------------------------------------------------------------
#
# os.environ['SDL_VIDEO_CENTERED'] = '1'
#
screen_width = 1024
screen_height = 768
screen_size = screen_width, screen_height
screen_flags_presets = (
    0,
    pygame.FULLSCREEN | pygame.DOUBLEBUF,
)
screen_flags = screen_flags_presets[0]
default_fill_color = 100, 100, 100

title = "The red planet: The planet of the King"
path_to_icon = "./data/icon.png"

# ------------------------------------------------------------------------------
# Game
# ------------------------------------------------------------------------------

# set this to [1, 1, 1, 1, 1, 1] where     Done = 1    Locked = 2    Open = 3
#                    ^ idx 3 is the red planet
progress_override = None
skip_game_setup = False
ppu = 15
move_next_timeout = 3
effect_duration = 0.5

# ------------------------------------------------------------------------------
# Sound
# ------------------------------------------------------------------------------

# Mixer
MIXER_FREQUENCY = 0  # default:22050
MIXER_SIZE = -16  # default: -16
MIXER_CHANNELS = 2  # default: 2 stereo or 1 mono
MIXER_BUFFER_SIZE = 16  # default: 4096

MIXER_NUM_CHANNELS = 48
MIXER_RESERVED_CHANNELS = 6
(
    channel_wind0,
    channel_wind1,
    channel_wind2,
    channel_crash,
    channel_other1,
    channel_other2,
) = range(MIXER_RESERVED_CHANNELS)

music_ended_pygame_event = pygame.USEREVENT

global_id_names = {}
GLOBAL_ID_ACTIONS = 1000
global_id_generator = GlobalIdGenerator(GLOBAL_ID_ACTIONS)  # avoid collision with event ids

# ------------------------------------------------------------------------------
# Events
# ------------------------------------------------------------------------------
# EVT_SFX_ITEM_HIT_WINDOW = global_id_generator.next('EVT_ITEM_HIT_WINDOW')


# actions


# ------------------------------------------------------------------------------
# Stepper
# ------------------------------------------------------------------------------
STEP_DT = 1.0 / 120.0  # [1.0 / fps] = [s]
MAX_DT = 20 * STEP_DT  # [s]
DRAW_FPS = 60  # [fps]

# ------------------------------------------------------------------------------
# Graphics
# ------------------------------------------------------------------------------

draw_debug = False

# cached surfaces (required by spritefx)
# Note: pygametext does its own caching. No need to cache surfaces from pygametext.getsurf() unless you transform them.
# Note: these settings are not tunable on the fly. If you want to change the image cache settings during runtime, you'll
# have to call the image_cache instance's methods.
# Note: If you rely on aging or memory cap, remember to call image_cache.update(), or purge_memory() or purge_old().

# image_cache_expire = 1 * 60  # expire unused textures after N seconds; recommend 1 min (1 * 60)
# image_cache_memory = 5 * 2 ** 20  # force expiration if memory is exceeded; recommend 5 MB (5 * 2**20)
# image_cache = imagecache.ImageCache(image_cache_expire, image_cache_memory)
# image_cache.enable_age_cap(True)

pygametext.FONT_NAME_TEMPLATE = 'data/fonts/%s'
pygametext.MEMORY_LIMIT_MB = 32  # it takes about 5 min to hit the default 64 MB, so 32 is not too aggressive

# # cached surfaces (required by spritefx)
# # Note: pygametext does its own caching,no need to cache surfaces from pygametext.getsurf() unless you transform them.
# # Note: these settings are not tunable on the fly. If you want to change the image cache settings during runtime,
# # you'll have to call the image_cache instance's methods.
# # Note: If you rely on aging or memory cap, remember to call image_cache.update(), or purge_memory() or purge_old().
# image_cache_expire = 1 * 60  # expire unused textures after N seconds; recommend 1 min (1 * 60)
# image_cache_memory = 5 * 2 ** 20  # force expiration if memory is exceeded; recommend 5 MB (5 * 2**20)
# image_cache = imagecache.ImageCache(image_cache_expire, image_cache_memory)
# image_cache.enable_age_cap(True)

# font_themes is used with the functions in module pygametext
# Example call to render some text using a theme:
#   image = pygametext.getsurf('some text', **settings.font_themes['intro'])
# font_themes can be accessed by nested dict keys:
#   font_theme['intro']['fontname']
font_themes = dict(
    # Game title
    game_message=dict(
        fontname='Vera.ttf',
        fontsize=25,
        color='yellow',
        gcolor='orange',
        ocolor='black',
        # owidth=0.5,
        scolor='grey20',
        shadow=(1, 1),
    ),
    title=dict(
        fontname='Bubblegum_Sans.ttf',
        fontsize=50,
        color='red2',
        gcolor='orange1',
        ocolor='yellow',
        owidth=0.5,
        scolor=None,
        shadow=None,
    ),
    loose=dict(
        fontname='Bubblegum_Sans.ttf',
        fontsize=50,
        color='red2',
        gcolor='orange1',
        ocolor='yellow',
        owidth=0.5,
        scolor=None,
        shadow=None,
    ),
    win=dict(
        fontname='Bubblegum_Sans.ttf',
        fontsize=50,
        color='red2',
        gcolor='orange1',
        ocolor='yellow',
        owidth=0.5,
        scolor=None,
        shadow=None,
    ),
    intro2=dict(
        fontname='VeraBd.ttf',
        fontsize=25,
        color='white',
        gcolor='grey80',
        # ocolor=None,
        # owidth=0.5,
        scolor='grey20',
        shadow=(1, 1),
    ),
    speechbubble=dict(
        fontname='ComicNoteSmooth.ttf',
        fontsize=20,
        color='black',
        width=300,
        # gcolor='orange',
        # ocolor='black',
        # owidth=0.1,
        # scolor=None,
        # shadow=None,
        background=(255, 240, 142, 255)
    ),
    navigation=dict(
        fontname='ComicNoteSmooth.ttf',
        fontsize=20,
        color='black',
        width=300,
        # gcolor='orange',
        # ocolor='black',
        # owidth=0.1,
        # scolor=None,
        # shadow=None,
        background=(117, 255, 147, 200)
    ),
    gamehud=dict(
        fontname='digital-7.mono.ttf',  # fontname='digital-7.regular.ttf',
        fontsize=30,
        color='black',
        gcolor=None,  # 'orange2',
        ocolor=None,
        owidth=0.5,
        scolor='grey20',
        shadow=(1, 1),
    ),
    floatingtext=dict(
        fontname='Vera.ttf',
        fontsize=10,
        color='white',
        # gcolor='orange',
        ocolor='black',
        owidth=0.1,
        scolor=None,
        shadow=None,
        background=(0, 0, 0, 128)
    ),
    debug=dict(
        fontname='Vera.ttf',
        fontsize=16,
        color='yellow',
        # gcolor='orange',
        ocolor='black',
        owidth=0.5,
        scolor='black',
        shadow=(1, 1),
    ),
)

# DEV DEBUG CHEATS

# time step multiplier; <1.0 to slow down time; NOTE change in _custom.py, not here!
# to use it, place "dt *= settings.DEBUG_TIME" anywhere you want to modify the time step
DEBUG_TIME = 1.0

# noinspection PyBroadException
try:
    # define a _custom.py file to override some settings (useful while developing)
    # noinspection PyUnresolvedReferences,PyProtectedMember
    from gamelib._custom import *
except ImportError:
    pass

logger.debug("imported")
