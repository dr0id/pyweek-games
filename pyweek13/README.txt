Requirements:

Python 2.6
Pygame 1.9.1


To play the game:

For Windows double-click run_game.pyw.

For Linux and Mac run: python run_game.py.


Playing the game:

Left click to move the avatar. Read the signs, they provide a helpful introduction to the world.
