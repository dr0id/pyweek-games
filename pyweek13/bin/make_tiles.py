import os, sys
import pygame

write_labels = False

if len(sys.argv) > 1:
    save_file = sys.argv[1]
else:
    save_file = os.path.join('..','data','map','color_palette_1.png')

color_data = [
    # name     func     start end
    ('grey',   (1,1,1),   32, 255),
    ('red',    (1,0,0),   32, 255),
    ('orange', (1,.65,0), 32, 255),
    ('yellow', (1,1,0),   32, 255),
    ('green',  (0,1,0),   32, 255),
    ('blue',   (0,0,1),   32, 255),
    ('indigo', (.5,0,1),  32, 255),
    ('violet', (1,.54,1), 32, 255),
]

pygame.init()
tile_size = 32
tileset_size = len(color_data)
screen_size = tile_size*tileset_size, tile_size*tileset_size
outline_color = pygame.Color('black')

screen = pygame.display.set_mode(screen_size)
screen_rect = screen.get_rect()
rect = pygame.Rect((0,0), (tile_size,tile_size))

font = pygame.font.Font(os.path.join('..','data','font','Vera.ttf'), 10)

for y in xrange(tileset_size):
    name,(fr,fg,fb),start,end = color_data[y]
    start = 16
    print '## PALETTE',name
    for x in xrange(tileset_size):
        fx = (float(x+1)/tileset_size)
        r = int(255 * fx * fr)
        g = int(255 * fx * fg)
        b = int(255 * fx * fb)
        print 'Fx {0:0.1f} RGB {1:s}'.format(fx,str((r,g,b)))
        fill_color = pygame.Color(r,g,b)
        rect.topleft = x*tile_size, y*tile_size
        pygame.draw.rect(screen, fill_color, rect)
        pygame.draw.rect(screen, outline_color, rect, 1)
        if write_labels:
            txt = '{0:d},{1:d}'.format(x,y)
            btxt = font.render(txt, True, (0,0,0))
            wtxt = font.render(txt, True, (255,255,255))
            txt_rect = wtxt.get_rect(center=rect.center)
            for xo in (-1,0,1):
                for yo in (-1,0,1):
                    screen.blit(btxt, txt_rect.move(xo,yo))
            screen.blit(wtxt, txt_rect)

pygame.display.flip()

pygame.image.save(screen, save_file)

while 1:
    for e in pygame.event.get():
        if e.type == pygame.KEYDOWN:
            quit()
    pygame.time.wait(100)
