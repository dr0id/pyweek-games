import sys,os,random
import pygame
from pygame.locals import *

if not sys.argv[0]:
    appdir = os.path.abspath(os.path.dirname(__file__))
else:
    appdir = os.path.abspath(os.path.dirname(sys.argv[0]))
os.chdir(appdir)
# make sure that sub modules in gamelib are imported corectly
appdir = os.path.join(appdir, '..', 'gamelib')
if not appdir in sys.path:
    sys.path.insert(0,appdir)

from gummworld2 import Vec2d
from gummworld2.geometry import distance, point_on_circumference


save_name = 'hit.png'
if len(sys.argv) > 1:
    save_name = sys.argv[1]


class Particle(object):
    def __init__(self, pos, angle, speed, color):
        self._position = Vec2d(pos)
        self.position = pos
        self.start_pos = Vec2d(pos)
        self.angle = angle
        self.speed = speed
        self.color = color
        self.distance = 0
    @property
    def position(self):
        return self._position
    @position.setter
    def position(self, val):
        p = self._position
        p.x,p.y = val
    def update(self):
        self.position = point_on_circumference(self.position, self.speed, self.angle)
        d = distance(self.start_pos, p.position)
        self.distance = d
    def draw(self, surf):
        r,g,b = self.color
        pygame.draw.line(surf, (r,g,b), self.position, self.position)


def make_particles(n):
    particles = []
    for i in xrange(n):
        a = random.randrange(0,359)
        s = random.randrange(10,40) * .1
        r,g,b = random.randrange(128,255),0,0
        p = Particle(rect.center, a, s, (r,g,b))
        particles.append(p)
    return particles


def save_images(images):
    r = images[0].get_rect()
    full_width = r.w * len(images)
    surf = pygame.Surface((full_width,r.h))
    for i,im in enumerate(images):
        r.x = i * r.w
        surf.blit(im, r)
    i = 0
    while 1:
        for e in pygame.event.get():
            if e.type == KEYDOWN:
                pygame.image.save(surf, save_name)
                print 'image saved:',save_name
                quit()
        clock.tick(10)
        screen.fill((0,0,0))
        if i >= len(images):
            i = 0
        screen.blit(images[i], (0,0))
        i += 1
        screen.blit(surf, (0,r.h))
        pygame.display.flip()


pygame.init()
screen = pygame.display.set_mode((800,600))
screen_rect = screen.get_rect()
clock = pygame.time.Clock()
image = pygame.Surface((48,48))
rect = image.get_rect()
secs = 0

n = 250
particles = make_particles(n)

images = []

while 1:
    clock.tick(10)
    t = pygame.time.get_ticks() / 1000
    if t > secs:
        secs = t
        pygame.display.set_caption('{0:.0f}'.format(clock.get_fps()))
    pygame.event.clear()
    
    screen.fill((0,0,0))
    image.fill((0,0,0))
    
    if not len(particles):
        print len(images),'images'
        save_images(images)
        quit()
    
    image.lock()
    for p in particles[:]:
        p.update()
        if p.distance > rect.centerx:
            particles.remove(p)
        p.draw(image)
    image.unlock()
    images.append(image.copy())
    
    screen.blit(image, (0,0))
    pygame.display.flip()
