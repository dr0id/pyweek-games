import pygame

from gummworld2 import State
from gummworld2 import data

import food
import foodspawn
import abilitydash
import door
import critter
import critterspawn
import player
import level


normal_font = None
big_font = None


class Tooltip(object):
    
    def __init__(self, obj):
        global normal_font, big_font
        
        self.parent = obj
        obj.tooltip = self
        self.lines = []
        
        if not normal_font:
            font_name = data.filepath('font','Vera.ttf')
            normal_font = pygame.font.Font(font_name, 14)
            font_name = data.filepath('font','VeraBd.ttf')
            big_font = pygame.font.Font(font_name, 28)
        
        self.refresh()
    
    @property
    def position(self):
        return self.rect.bottomleft
    @position.setter
    def position(self, val):
        self.rect.bottomleft = val
        self.rect.clamp_ip(State.camera.view.rect)
    
    def refresh(self):
        """Call this to update the image with new stats. Only useful for things
        whose stats change, like Player or Critter.
        """
        del self.lines[:]
        self._get_message()
        self._paint()
    
    def draw(self):
        State.camera.view.blit(self.image, self.rect)
    
    def _get_message(self):
        parent = self.parent
        player_obj = State.game.player
        if parent.name:
            self.lines.append(parent.name)
        else:
            self.lines.append('Misc Object')
        if isinstance(parent, food.Food):
            for name,val in parent.effects.items():
                name = str(name)
                val = int(val)
                if name in food.CALCS:
                    self.lines.append('{0} {1:+d}'.format(
                        player_obj.abilities[name].proper_name, val))
        elif isinstance(parent, foodspawn.FoodSpawn):
            self.lines.append('Spawn rate ' + str(parent.spawn_rate))
            self.lines.append('Max out ' + str(parent.max_out))
            self.lines.append('Num out ' + str(len(parent.treats)))
        elif isinstance(parent, critterspawn.CritterGroup):
            n = 0
            for child in parent.spawners:
                n += 1
                self.lines.append('Spawner'+str(n))
                self.lines.append(' Spawn rate ' + str(child.spawn_rate))
                self.lines.append(' Max out ' + str(child.max_out))
                self.lines.append(' Num out ' + str(len(child.critters)))
                if 'health' in child.class_kwargs:
                    self.lines.append(' Health ' + str(child.class_kwargs['health']))
        elif isinstance(parent, critterspawn.CritterSpawn):
            self.lines.append('Spawn rate ' + str(parent.spawn_rate))
            self.lines.append('Max out ' + str(parent.max_out))
            self.lines.append('Num out ' + str(len(parent.treats)))
            self.lines.append('Health ' + str(parent.health))
        elif isinstance(parent, abilitydash.AbilityGauge):
            self.lines.extend(parent.desc)
        elif isinstance(parent, door.Fire):
            self.lines.append('Requires abilities: ' + parent.lock_value)
            print 'FIRE'
        elif isinstance(parent, door.Ice):
            self.lines.append('Requires abilities: ' + parent.lock_value)
            print 'ICE'
        elif isinstance(parent, door.Door):
            self.lines.append('Requires abilities: ' + parent.lock_value)
        elif isinstance(parent, critter.Critter):
            self.lines.append('Health ' + str(parent.health))
        elif isinstance(parent, player.Player):
            self.lines.append('Health ' + str(parent.health))
            self.lines.append('Arms ' + str(parent.mutations.arms.value))
            self.lines.append('Legs ' + str(parent.mutations.legs.value))
            self.lines.append('Skin ' + str(parent.mutations.skin.value))
        elif isinstance(parent, level.Level):
            del self.lines[:]
        self._get_msg()
    
    def _get_msg(self):
        parent = self.parent
        line = ''
        if hasattr(parent, 'msg'):
            for word in parent.msg.split():
                if len(line+word) > 35:
                    self.lines.append(line)
                    line = word
                else:
                    line = line + ' ' + word
        if len(line):
            self.lines.append(line)
    
    def _paint(self):
        if isinstance(self.parent, level.Level):
            font = big_font
        else:
            font = normal_font
        surfw,surfh = 0,0
        for w,h in [font.size(t) for t in self.lines]:
            if w > surfw: surfw = w
            surfh += h
        self.image = pygame.Surface((surfw,surfh))
        self.image.fill((33,33,33))
        self.rect = self.image.get_rect()
        for x in xrange(0,surfw,2):
            for y in xrange(0,surfh,3):
                pygame.draw.line(self.image, (0,0,0), (x,y), (x,y), 1)
        for i,text in enumerate(self.lines):
            txt_img = font.render(text, True, (255,255,255))
            self.image.blit(txt_img, (0,i*h))
        self.image.set_alpha(255-48)
        if isinstance(self.parent, level.Level):
            screen_rect = State.screen.rect
            self.rect.center = screen_rect.centerx,screen_rect.centery/2
