import pygame

import lock


class Wall(lock.Lockable):
    
    def __init__(self, map_obj):
        lock.Lockable.__init__(self, lock_value=map_obj.properties.get('lock','*'))
        
        self.map_obj = map_obj
        self.name = map_obj.name if map_obj.name else 'Wall'
        
        self.rect = pygame.Rect(
            map_obj.x, map_obj.y, map_obj.width, map_obj.height)
        
#        self.image = pygame.Surface(self.rect.size)
#        self.image.fill(map_obj.color)
