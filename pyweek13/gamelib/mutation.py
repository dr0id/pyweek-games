class Mutations(dict):
    
    def add(self, mutation):
        """add a mutation to self[mutation_name] and to self.mutation_name
        
        muts = Mutations()
        mut = Mutation('legs')
        muts.add(mut)
        print muts.legs.value
        print muts['legs'].value
        """
        self[mutation.name] = mutation
        self.__dict__[mutation.name] = mutation
    
    def print_all(self):
        for key in self:
            print self[key]


class Mutation(object):
    
    def __init__(self, name, minval=0, maxval=10, initval=5):
        self.name = name
        self._init = initval
        self._min = minval
        self._max = maxval
        self._value = initval
    
    def reset_state(self):
        self.value = self._init
    
    @property
    def percent(self):
        mn = self._min
        mx = self._max
        val = self._value
        if mn < 0:
            mx += abs(mn)
            val += abs(mn)
            mn = 0
        return float(val) / (mx-mn)
    
    @property
    def value(self):
        return self._value
    @value.setter
    def value(self, val):
        if val > self._max:
            self._value = self._max
        elif val < self._min:
            self._value = self._min
        else:
            self._value = val
    
    def __str__(self):
        return 'Mutation({obj.name},{obj._min},{obj._max},{obj.value})'.format(
            obj=self
        )


if __name__ == '__main__':
    def print_mut(m):
        print '{0}: val={1:0.1f} pct={2:0.2f}'.format(m.name, m.value, m.percent)
    m = Mutation('rarr', -10, 10, -10)
    print_mut(m)
    m.value = -11
    assert m.value == -10
    for i in range(22):
        m.value += 1
        print_mut(m)
