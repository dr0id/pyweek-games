import pygame

import gummworld2
from gummworld2 import State

import lock
import animation


class Door(lock.Lockable):
    
    def __init__(self, map_obj=None, lock_value='None'):
        lock.Lockable.__init__(self, map_obj, lock_value)
        self.map_obj = map_obj
## set by Lockable
##        self.name = map_obj.name
        self.rect = pygame.Rect(
            map_obj.x, map_obj.y, map_obj.width, map_obj.height)
            
        self._image = pygame.Surface(self.rect.size)
        self._image.fill(map_obj.color)

    @property
    def image(self):
        return self._image

class InOutDoor(Door):
    
    def __init__(self, map_obj=None, lock_value='None'):
        Door.__init__(self, map_obj, lock_value)
        print '??????????????????', self.name, map_obj
        if self.name == 'Start':
            self.anim = animation.start_point()
        else:
            self.anim = animation.end_point()
        self.anim.start()
            
    def draw(self):
        """Draw a sprite on the camera's surface using world-to-screen conversion.
        """
        camera = State.camera
        cx,cy = camera.rect.topleft
        sx,sy = self.rect.topleft
        camera.surface.blit(self.anim.get_image(), (sx-cx, sy-cy-32))


class Fire(lock.Lockable):
    
    def __init__(self, map_obj=None, lock_value='None'):
        lock.Lockable.__init__(self, map_obj, lock_value)
        self.map_obj = map_obj
## set by Lockable
##        self.name = map_obj.name
        self.rect = pygame.Rect(
            map_obj.x, map_obj.y, map_obj.width, map_obj.height)
        
        # add fire animation
        self.anim = animation.fire_animation
        if "fire_prot" not in self.lock_value:
            print '## Fire: bad lock "{0}"'.format(self.lock_value)
        
        self.msg = 'Fire burns! Maybe fireproof skin would help.'
    
    @property
    def image(self):
        return self.anim.get_image()

    def draw(self):
        """Draw a sprite on the camera's surface using world-to-screen conversion.
        """
        camera = State.camera
        cx,cy = camera.rect.topleft
        sx,sy = self.rect.topleft
        camera.surface.blit(self.image, (sx-cx, sy-cy-32))

        
class Ice(lock.Lockable):
    
    def __init__(self, map_obj=None, lock_value='None'):
        lock.Lockable.__init__(self, map_obj, lock_value)
        self.map_obj = map_obj
## set by Lockable
##        self.name = map_obj.name
        self.rect = pygame.Rect(
            map_obj.x, map_obj.y, map_obj.width, map_obj.height)
        
        # add fire animation
        self.anim = animation.ice_animation
        if "cold_prot" not in self.lock_value:
            print '## Ice: bad lock "{0}"'.format(self.lock_value)
        
        self.msg = 'Ice freezes! Maybe cold-proof skin would help.'
    
    @property
    def image(self):
        return self.anim.get_image()

    def draw(self):
        """Draw a sprite on the camera's surface using world-to-screen conversion.
        """
        camera = State.camera
        cx,cy = camera.rect.topleft
        sx,sy = self.rect.topleft
        camera.surface.blit(self.image, (sx-cx, sy-cy-32))
