import os, glob

import pygame

import gummworld2

pygame.init()
pygame.mixer.quit()
pygame.mixer.pre_init(0, -16, 2, 256)
pygame.mixer.init()


## SLOW COMPUTERS MAY NEED TO LOWER game_ticks_per_second TO 25
##
## The game's title bar displays your UPS (game ticks per second).
##
## If you can't get 25 UPS, you probably can't run this game very well. But
## you can try maybe as low as 20.
##
game_ticks_per_second = 30


## Display ##

resolution = 800,600        # screen size in pixels: width,height
fullscreen = False          # open fullscreen if True
os.environ['SDL_VIDEO_CENTERED'] = '1'  # place window: '1'=center, ''=auto

## Game ##

maps = [
    '00.tmx',
    '02.tmx',
    '01.tmx',
]
#maps = glob.glob(os.path.join(gummworld2.data.paths['map'], '*.tmx'))

# AI distances measured in pixels ...
max_visibility = 250
max_range_attack = 175
max_melee_attack = 50

## GUI ##

systemcursor = False      # True: use system cursor; False: use custom cursor

game_layout = dict(
    # name       x,y                  w,h
    # -------   -------------------   --------------------
    playfield = (0,0,                   resolution[0],resolution[1]*11/12),
    dashboard = (0,resolution[1]*11/12, resolution[0],resolution[1]*1/12),
)

## Graphics ##

# Defaults already set for graphics asset paths. You can add new subdirs like so.
#gummworld2.data.set_subdir('myname', 'subdir')

## Sound ##

# Defaults already set for sound asset paths. You can add new subdirs like so.
gummworld2.data.set_subdir('music', 'music')

sfx_volumes = {
    'firedoor' : 0.2,
    'footstep1' : 0.1,
    'footstep2' : 0.1,
    'footstep3' : 0.1,
    'footstep4' : 0.1,
    'grunt1' : 0.2,
    'grunt2' : 0.2,
    'grunt3' : 0.2,
    'grunt4' : 0.2,
}
background_music_volume = 0.3
background_music = gummworld2.data.filepath('music', '0pything2.ogg')
pygame.mixer.music.load(background_music)
pygame.mixer.music.set_volume(background_music_volume)
pygame.mixer.music.play(-1)

## Debug ##

quickstart = False          # jump right into Game
quickquit = False           # QUIT or ESCAPE in Game quits immediately
profile = False             # run cProfile if True
errormapobjects = False      # unhandled map objects raise exception (see gameobjects.make_object)
errormissingexits = False    # missing exits raise exception (see level.Level.__init__)
# conditions where spammy debugs are printed:
spam = {
    'Lockable' : __debug__ and False,
    'Player'   : __debug__ and False,
    'Critter'   : __debug__ and False,
}

## Custom ##

# Code to be run only if the game is running in the detected environment, e.g.
# on Gummbum's PC.

if os.environ.get('COMPUTERNAME','*bad*') == 'BW-PC' or os.environ.get('HOME','*bad*') == '/home/gumm':
    print '## CUSTOM SETTINGS FOR Gummbum (see settings.py)'
    quickstart = True
    quickquit = True
    maps = [
#        'concept2.tmx',
        '02.tmx',
        '01.tmx',
        '00.tmx',
    ]
