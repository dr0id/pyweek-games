import random

import pygame

from gummworld2 import State
from gummworld2 import data

import lock, foodtext, combattext, sfx


__doc__ = """
Food effects are looked up in the EFFECTS dict. The key is either a
Mutation.name (e.g. 'legs') or an Ability.name (e.g. 'run'). The corresponding
value is a list of Mutation names which will be adjusted by the food. Some foods
affect only one Mutation, others may affect a combination.

The details of Food effects can be specified in Tiled or programatically. In
both cases the object properties format is: key:value. For example: 'run':1; or 'run':-2.

The value will be applied to all Mutations in the corresponding list.

A food object can have more than one effect. All effects will be applied.
For example:
    'run':1
    'strong':-1
Note: Tiled does not support duplicate property keys, so you cannot specify a
property more than once. You *can* however:
    'run':1
    'jump':-1
"""
EFFECTS = {
    ## These are NAMES taken from Player. See player.Player.__init__.
    ##
    ## mutation or ability      applies to mutation(s)
    ## -------------------      ----------------------
    'legs'                  :   ['legs'],
    'run'                   :   ['legs'],
    'jump'                  :   ['legs'],
    'sturdy'                :   ['legs'],
    
    'arms'                  :   ['arms'],
    'reach_out'             :   ['arms'],
    'snatch'                :   ['arms'],
    'strong'                :   ['arms'],
    'attack'                :   ['arms'],
    
    'fire_prot'             :   ['skin'],
    'cold_prot'             :   ['skin'],
    
    'reach_high'            :   ['legs','arms'],
}
def inc(m,v): m.value += v          # increment
def dec(m,v): m.value -= v          # decrement
CALCS = {
    ## These are NAMES taken from Player. See player.Player.__init__.
    ##
    ## mutation or ability      applies to mutation(s)
    ## -------------------      ----------------------
    'legs'                  :   {'legs' : inc},
    'run'                   :   {'legs' : inc},
    'jump'                  :   {'legs' : inc},
    'sturdy'                :   {'legs' : dec},
    
    'arms'                  :   {'arms' : inc},
    'reach_out'             :   {'arms' : inc},
    'snatch'                :   {'arms' : inc},
    'strong'                :   {'arms' : dec},
    'attack'                :   {'arms' : inc},
    
    'fire_prot'             :   {'skin' : inc},
    'cold_prot'             :   {'skin' : dec},
    
    'reach_high'            :   {'legs' : inc, 'arms' : inc},
}


class Food(lock.Lockable):
    
    def __init__(self, map_obj=None, name=None, position=None, size=None, spawner=None, effects={}, lock_value='None'):
        
        self.visible = True
        self.effects = {}
        
        # Initialize me as lockable. Default is Not Locked.
        lock.Lockable.__init__(self, map_obj, lock_value)
        
        # Initialize my effects.
        if map_obj:
            self.map_obj = map_obj
            self.name = map_obj.name
            self._update_effects(map_obj.properties)
            self.rect = pygame.Rect(
                map_obj.x, map_obj.y, map_obj.width, map_obj.height)
        else:
            self.map_obj = None
            self.name = name
            self._update_effects(effects)
            self.rect = pygame.Rect(position, size)
        
#        self.image = pygame.Surface(self.rect.size)
#        if map_obj:
#            self.image.fill(map_obj.color)
#        else:
#            self.image.fill(pygame.Color('yellow'))
        self.image = pygame.image.load(data.filepath('image','food.png'))
        self.rect = self.image.get_rect(center=self.rect.center)
        
        self.spawner = spawner
        
    def _update_effects(self, src_dict):
        """initialize dict of effects from src_dict
        """
        for key,val in src_dict.items():
            if key in EFFECTS:
                self.effects[key] = int(val)
    
    def eaten_by(self, obj):
        """apply one-time effects to obj
        
        This food is removed from the world afterwards.
        """
        
        # Example breakdown in comments:
        #
        # key,val = ('run',-1)
        eat_sound = random.choice([s for s in sfx.sfx.keys() if s.startswith('eat')])
        sfx.play(eat_sound)
        for ability_name,val in self.effects.items():
            if __debug__:
                print 'Food: {0} eating {1} {2}'.format(obj.name,ability_name,val)
            
# doesn't help game accessibility
#            combattext.CombatText(obj, ability_name.capitalize(), 'yellow')
            foodtext.FoodText(self, obj.abilities[ability_name], val)
            
            # mutation = EFFECTS['run'] = 'legs'
            for mutation_name,calculate in CALCS[ability_name].items():
                if __debug__:
                    print 'Food: applying',mutation_name,val
                ## Note: val is subject to the calculator. Look up the
                ## calculator in CALCS[ability_name][mutation_name]. Also note
                ## that food objects *can* speficy CALCS[ability_name] or
                ## CALCS[mutation_name] in their properties.
                mutation = obj.mutations[mutation_name]
                if __debug__: print 'BEFORE',mutation_name,mutation.value
                calculate(mutation, val)
                if __debug__: print 'AFTER',mutation_name,mutation.value
        
        if self.spawner:
            self.spawner.kill(self)
        
        if getattr(State, 'game', None):
            if getattr(State.game, 'level', None):
                ## TO DO: eaten presentation
                State.game.level.objects.remove(self)
    
    def __str__(self):
        return 'Food({0},{1})'.format(self.name,str(self.effects))
