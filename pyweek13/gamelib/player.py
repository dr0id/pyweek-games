import random
import math
from math import atan2

import animation

import pygame

import gummworld2
from gummworld2 import data, geometry, toolkit
from gummworld2 import State, Vec2d

import settings, animation, sfx, combattext
import mutation, ability, lock, food, critter


class Player(object):
    
    def __init__(self, position=(0,0)):
        if __debug__: print 'Player: INIT'
        self.visible = True
        self.name = 'Player'
        self.rect = pygame.Rect(0,0,20,20)
        self._position = Vec2d(position)
        self.tooltip = None
        
        self.image = pygame.Surface((20,20))
        self.image.fill(pygame.Color('white'))
        
        self.position = position
        self.rect.center = position
        
        self.speed = 3
        self.health = 10.
        
        # Movement helpers.
        self.start = Vec2d(self.rect.center)
        self.end = Vec2d(self.rect.center)
        self.test_dummy = gummworld2.model.Object()
        
        ## Mutations and Abilities are defined here, and processed by:
        ##  food.Food.EFFECTS
        ##  lock.Lockable.can_pass
        ##  (maybe more to come)
        self.mutations = mutation.Mutations()
        self.abilities = ability.Abilities()
        
        # Arm mutations and abilities.
        self.mutations.add(mutation.Mutation('arms'))
        self.abilities.add(ability.Ability('reach_out', self.mutations.arms))
        self.abilities.add(ability.Ability('snatch', self.mutations.arms))
        self.abilities.add(ability.Ability('strong',
            self.mutations.arms,
            lambda m:10-m.value))
        self.abilities.add(ability.Ability('attack', self.mutations.arms))
        
        # Leg mutations and abilities.
        self.mutations.add(mutation.Mutation('legs'))
        self.abilities.add(ability.Ability('run', self.mutations.legs))
        self.abilities.add(ability.Ability('jump', self.mutations.legs))
        self.abilities.add(ability.Ability('sturdy',
            self.mutations.legs,
            lambda m:10-m.value))
        
        # Skin mutations and abilities.
        self.mutations.add(mutation.Mutation('skin'))
        self.abilities.add(ability.Ability('fire_prot', self.mutations.skin))
        self.abilities.add(ability.Ability('cold_prot', self.mutations.skin,
            lambda m:10-m.value))
        
        # Compound abilities.
        self.abilities.add(ability.Ability('reach_high',
            (self.mutations.legs,self.mutations.arms),
            lambda m:(m[0].value+m[1].value)/2,
        ))
        
        # Which critter is targeted for attacking.
        self.target = None
        
        # 0 or 1: no critters alerted
        # 2: critter chasing
        # 3: critter in range attack distance
        # 4: critter in melee attack distance
        self.alert = 0
        
        # Highest state of any critter
        self.attack_alert = None
        
        self.footstep_period = .45
        self.footstep_timer = .75
        
        self.current_dir = 0
        self.anim = animation.player_E()
        
        if __debug__:
            self.mutations.print_all()
            self.abilities.print_all()
    
    @property
    def position(self):
        return self._position
    @position.setter
    def position(self, val):
        p = self._position
        p.x,p.y = val
    
    def reset_stats(self):
        self.health = 10.
        self.target = None
        for m in self.mutations.values():
            m.reset_state()
        self.refresh_tooltip()
    
    def update(self, dt):
        self.alert = 0
        State.game.mouse_target.peace()
        self.start[:] = self.position
        self.end[:] = self.position
    
    def draw(self):
        self.rect.center = self.position
        camera_pos = State.camera.rect.topleft
        sprite_pos = Vec2d(self.rect.topleft)
#        screen_pos = sprite_pos - camera_pos - Vec2d(32, 64)
        screen_pos = sprite_pos - camera_pos - Vec2d(16, 64)
        step = self.end - self.start
        
        if (-.1,-.1) <= (round(step[0],1),round(step[1],1)) <= (.1,.1):
            # Tiny distance to go. Move to end.
            State.camera.view.blit(self.anim.get_image(), screen_pos)
        else:
            # Still a long way to go. Interp to end.
            interp = State.camera.interp
            interp_pos = toolkit.interpolated_step(screen_pos, step, interp)
            State.camera.view.blit(self.anim.get_image(), interp_pos)
    
    def move(self, dt, dx, dy):
        if dx or dy:
            if self.footstep_timer <= 0:
                step_sound = random.choice([s for s in sfx.sfx.keys() if s.startswith('footstep')])
                sfx.play(step_sound)
                self.footstep_timer = self.footstep_period
            else:
                self.footstep_timer -= dt
        r,collisions = self.test_collide(self.rect, dx, dy)
        if collisions:
            r = self.handle_collisions(r, collisions, dx, dy)
        if r:
            self.position = r.center
            self.end[:] = r.center
            
            if dx == 0 and dy == 0:
                self.anim.stop()
            else:
                self.anim.start()
                
                # find angle and sector to know wich direction to show
                alpha = math.degrees(atan2(dy, dx)) # (-pi, pi]
                dir = int(((alpha + 22.5) / 45.5) % 8)
                
                if dir != self.current_dir:
                    self.current_dir = dir
                    if dir == 0: 
                        self.anim.stop()
                        self.anim = animation.player_E()
                        self.anim.start()
                    elif dir == 1:
                        self.anim.stop()
                        self.anim = animation.player_SE()
                        self.anim.start()
                    elif dir == 2:
                        self.anim.stop()
                        self.anim = animation.player_S()
                        self.anim.start()
                    elif dir == 3:
                        self.anim.stop()
                        self.anim = animation.player_SW()
                        self.anim.start()
                    elif dir == 4:
                        self.anim.stop()
                        self.anim = animation.player_W()
                        self.anim.start()
                    elif dir == 5:
                        self.anim.stop()
                        self.anim = animation.player_NW()
                        self.anim.start()
                    elif dir == 6:
                        self.anim.stop()
                        self.anim = animation.player_N()
                        self.anim.start()
                    elif dir == 7:
                        self.anim.stop()
                        self.anim = animation.player_NE()
                        self.anim.start()
                    else:
                        if __debug__:
                            print'!!!!!!!!!! unknown direction', dir
            
        return r
    
    def test_collide(self, rect, dx, dy):
        r = rect.move(round(dx), round(dy))
        r.clamp_ip(State.map.rect)
        self.test_dummy.rect = r
        exclude = State.game.collision_excludes
        collisions = [c for c in
            State.game.level.objects.collide(self.test_dummy)
                if c not in exclude
        ]
        return r, collisions
    
    def handle_collisions(self, to_rect, collisions, dx, dy):
        """handle collisions and return new rect
        
        If player can move, a new rect is returned indicating its new position.
        If player cannot move, None is returned.
        """
        blocked = False
        for c in collisions:
            if c is self:
                continue
            if self.spam: print 'Player: collision:',c.name
            if isinstance(c, lock.Lockable):
                if c.can_pass(self):
                    if self.spam: print 'Player: can pass Lockable'
                else:
                    if self.spam: print 'Player: cannot pass Lockable'
                    blocked = True
                    if not self.bumping_something and c.lock_value != '*':
                        sfx.play('bump')
                        self.bumping_something = True
##                    to_rect = None
            if to_rect and isinstance(c, food.Food):
                if self.spam:
                    print 'Player: abilities before food:'
                    self.abilities.print_all()
                self.eat_food(c)
                if self.spam:
                    print 'Player: abilities after food:'
                    self.abilities.print_all()
        if blocked:
            # try X axis
            if self.spam: print 'BLOCKED: try X axis'
## TO DO: some kind of smart cornering: player drags around corner
##            r1,c1 = self.test_collide(self.rect, self.speed*sign(dx), 0)
            r1,c1 = self.test_collide(self.rect, dx, 0)
            if c1:
                # blocked, try Y axis
                if self.spam: print 'BLOCKED: try Y axis'
## TO DO: some kind of smart cornering: player drags around corner
##                r2,c2 = self.test_collide(self.rect, 0, self.speed*sign(dy))
                r2,c2 = self.test_collide(self.rect, 0, dy)
                if c2:
                    # X and Y are blocked, set to_rect=None for no-pass
                    if self.spam: print 'BLOCKED: X and Y are blocked'
                    to_rect = None
                else:
                    # Y axis not blocked, use it
                    if self.spam: print 'BLOCKED: Y axis not blocked, use it'
                    to_rect = r2
            else:
                # X axis not blocked, use it
                if self.spam: print 'BLOCKED: X axis not blocked, use it'
                to_rect = r1
        if self.spam and not to_rect: print 'BLOCKED: --- cannot move ---'
        return to_rect
    
    def new_move(self):
        self.bumping_something = False
    
    def attack(self):
        attack_sound = random.choice([s for s in sfx.sfx.keys() if s.startswith('hit')])
        sfx.play(attack_sound)
        if self.target:
            
            # Only attack critters
            if not isinstance(self.target, critter.Critter):
                return
            
            if __debug__: print 'Player: attacking',self.target
            
            # Only attack critters in range
            dist = geometry.distance(self.rect.center, self.target.rect.center)
            if dist <= settings.max_melee_attack:
                if __debug__: print 'Player: melee attack distance', dist
                self.target.hit((10-self.mutations.arms.value)/10. * 10)
            elif dist <= settings.max_range_attack:
                if __debug__: print 'Player: range attack distance', dist
                self.target.hit(self.mutations.arms.value/10. * 10)
            else:
                return
            
#            # Do the animation
#            hit_anim = animation.hit_animation()
#            hit_anim.rect = hit_anim.get_image().get_rect()
#            hit_anim.rect.center = State.camera.world_to_screen(self.target.position)
#            State.game.hit_anims.append(hit_anim)
            
            # Update the tooltip
            tt = State.game.tooltip
            if tt and tt.parent is self.target:
                tt.refresh()
            
            # Drop target if dead
            if not self.target.is_alive:
                self.target = None
    
    def eat_food(self, morsel):
        morsel.eaten_by(self)
    
    def tell_critter_state(self, state_id):
        if state_id > self.alert:
            self.alert = state_id
            mouse = State.game.mouse_target
            if state_id == 4:
                mouse.melee()
            elif state_id == 3:
                mouse.range()
            elif state_id == 2:
                mouse.chase()
            else:
                mouse.peace()
    
    def refresh_tooltip(self):
        if self.tooltip:
            self.tooltip.refresh()
    
    def hit(self, damage):
        hit_sound = random.choice([s for s in sfx.sfx.keys() if s.startswith('ouch')])
        combattext.CombatText(self, str(-damage), 'red')
        self.health -= damage
        self.refresh_tooltip()
        if self.health <= 0:
            sfx.play('die')
            self.kill()
        else:
            sfx.play(hit_sound)
        # Do the animation
        hit_anim = animation.hit_animation()
        hit_anim.rect = hit_anim.get_image().get_rect()
        hit_anim.rect.center = State.camera.world_to_screen(self.position)
        State.game.hit_anims.append(hit_anim)
    
    def kill(self):
        if __debug__: print "Player: I'm dead, Jim!"
        State.game.lost_level()
    
    @property
    def spam(self):
        return settings.spam.get('Player', False)


def sign(n):
    """return the sign of number n"""
    if n >= 0:
        return 1
    else:
        return -1
