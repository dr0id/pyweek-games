import math, random
from math import atan2

import pygame

import gummworld2
from gummworld2 import State, Vec2d
from gummworld2.geometry import distance, rect_collided_other, angle_of
from gummworld2.geometry import point_on_circumference

import settings, lock, food, combattext, sfx, animation



class _BaseState:
    @staticmethod
    def enter(self):
        pass
    @staticmethod
    def exit(self):
        pass
    @staticmethod
    def think(self, dt):
        return self.current_state
    @staticmethod
    def rest(self):
        pass
    @staticmethod
    def chase(self):
        pass
    @staticmethod
    def melee_attack(self):
        pass

class InitState(_BaseState):
    pass

class RestState(_BaseState):
    @staticmethod
    def enter(self):
        self.attack_reload = 1.5
        self.anim.stop()
    @staticmethod
    def think(self, dt):
        if self.chase():
            return ChaseState
        return self.current_state

class ChaseState(_BaseState):

    @staticmethod
    def enter(self):
        self.anim.start()
        chase_sound = random.choice(['grunt3','grunt4'])
        sfx.play(chase_sound)
    @staticmethod
    def exit(self):
        sfx.play('grunt2')
    @staticmethod
    def think(self, dt):
        angle = angle_of(self.position, State.game.player.position)
        pos = point_on_circumference(self.position, self.speed, angle)
        self.move(pos-self.position)
        if self.melee():
            return MeleeAttackState
        if self.rest():
            return RestState
        return self.current_state

class MeleeAttackState(_BaseState):

    @staticmethod
    def enter(self):
        self.anim.stop()

    @staticmethod
    def think(self, dt):
        self.attack_reload += dt
        if self.attack_reload >= self.attack_speed:
            self.attack(State.game.player)
            self.attack_reload %= self.attack_speed
        if self.melee():
            return self.current_state
        return ChaseState


STATE_IDS = {
    InitState : 0,
    RestState : 1,
    ChaseState : 2,
    MeleeAttackState : 4,
}



class Critter(object):
    
    default_health = 1.
    
    def __init__(self, map_obj=None, position=None, health=None, color='red', spawner=None):
        self.visible = True
        self._position = Vec2d(0,0)
        if health is None:
            health = self.default_health
        
        # AI...
        self.test_dummy = gummworld2.model.Object()
        size = (settings.max_visibility*2,)*2
        self.look_area = pygame.Rect(self.position, size)
        size = (settings.max_melee_attack*2,)*2
        self.melee_area = pygame.Rect(self.position, size)
        size = (settings.max_range_attack*2,)*2
        self.range_area = pygame.Rect(self.position, size)
        self.current_state = InitState
        
        if map_obj:
            self.name = map_obj.name
            self.color = map_obj.color
            self.rect = pygame.Rect(map_obj.x,map_obj.y,map_obj.width,map_obj.height)
            if position:
                self.position = position
            else:
                self.position = self.rect.center
            self.health = float(map_obj.properties.get('health', health))
        else:
            self.name = 'Critter'
            self.color = pygame.Color(color) if isinstance(color, str) else color
            self.rect = pygame.Rect(0,0,32,32)
            if position:
                self.position = position
            else:
                self.position = 0,0
            self.health = health
        
        # self.image = pygame.Surface(self.rect.size)
        # self.image.fill(self.color)
        
        self.anim = animation.critter_E()
        self.spawner = spawner
        self._alive = True
        self.speed = 2
        self.attack_speed = 3.
        self.attack_reload = 3.
        
        self.tooltip = None
        
        self.set_state(RestState)
        self.current_dir = 0

    @property
    def image(self):
        return self.anim.get_image()
    
    def draw(self):
        """Draw a sprite on the camera's surface using world-to-screen conversion.
        """
        camera = State.camera
        cx,cy = camera.rect.topleft
        sx,sy = self.rect.topleft
        camera.surface.blit(self.image, (sx-cx, sy-cy-64))

    @property
    def position(self):
        return self._position
    @position.setter
    def position(self, val):
        p = self._position
        p.x,p.y = val
        self.look_area.center = self.melee_area.center = self.range_area.center= self.rect.center = round(val[0]),round(val[1])
##        State.game.level.objects.add(self)
    
    @property
    def is_alive(self):
        return self._alive
    
    # support extended collide for SpatialHash
    collided = staticmethod(rect_collided_other)
    
    def update(self, dt):
        new_state = self.current_state.think(self, dt)
        if new_state != self.current_state:
            self.set_state(new_state)
        
        player = State.game.player
        state_id = STATE_IDS[self.current_state]
        if state_id == 2 and self.range():
            state_id += 1
        player.tell_critter_state(state_id)
    
    def set_state(self, new_state):
        if __debug__:
            print 'Critter: {0} -> {1}'.format(
                self.current_state.__name__, new_state.__name__)
        self.current_state.exit(self)
        self.current_state = new_state
        new_state.enter(self)
    
    def rest(self):
        return not self.in_range(self.look_area, State.game.player.rect, settings.max_visibility)
    
    def chase(self):
        return self.in_range(self.look_area, State.game.player.rect, settings.max_visibility)
    
    def melee(self):
        return self.in_range(self.melee_area, State.game.player.rect, settings.max_melee_attack)
    
    def range(self):
        return (not self.melee()) and self.in_range(self.range_area, State.game.player.rect, settings.max_range_attack)
    
    def attack(self, other):
        print 'Critter: Attacking',other
        other.hit(1)
    
    def in_range(self, my_rect, other_rect, dist):
        return my_rect.colliderect(other_rect) and \
            distance(my_rect.center, other_rect.center) <= dist
    
#    def move(self, (dx,dy)):
#        self.position += (dx,dy)
#        State.game.level.objects.add(self)
    
    def move(self, (dx,dy)):
        r,collisions = self.test_collide(self.rect, dx, dy)
        if collisions:
            r = self.handle_collisions(r, collisions, dx, dy)
        if r:
            self.position = r.center
            State.game.level.objects.add(self)
            
            # find angle and sector to know wich direction to show
            alpha = math.degrees(atan2(dy, dx)) # (-pi, pi]
            dir = int(((alpha + 22.5) / 45.5) % 8)
            
            if dir != self.current_dir:
                self.current_dir = dir
                if dir == 0: 
                    self.anim.stop()
                    self.anim = animation.critter_E()
                    self.anim.start()
                elif dir == 1:
                    self.anim.stop()
                    self.anim = animation.critter_SE()
                    self.anim.start()
                elif dir == 2:
                    self.anim.stop()
                    self.anim = animation.critter_S()
                    self.anim.start()
                elif dir == 3:
                    self.anim.stop()
                    self.anim = animation.critter_SW()
                    self.anim.start()
                elif dir == 4:
                    self.anim.stop()
                    self.anim = animation.critter_W()
                    self.anim.start()
                elif dir == 5:
                    self.anim.stop()
                    self.anim = animation.critter_NW()
                    self.anim.start()
                elif dir == 6:
                    self.anim.stop()
                    self.anim = animation.critter_N()
                    self.anim.start()
                elif dir == 7:
                    self.anim.stop()
                    self.anim = animation.critter_NE()
                    self.anim.start()
                else:
                    if __debug__:
                        print'!!!!!!!!!! unknown direction', dir

                
    
    def test_collide(self, rect, dx, dy):
        r = rect.move(round(dx), round(dy))
        r.clamp_ip(State.map.rect)
        self.test_dummy.rect = r
        exclude = State.game.collision_excludes
        collisions = [c for c in
            State.game.level.objects.collide(self.test_dummy)
                if c not in exclude
        ]
        return r, collisions
    
    def handle_collisions(self, to_rect, collisions, dx, dy):
        """handle collisions and return new rect
        
        If critter can move, a new rect is returned indicating its new position.
        If critter cannot move, None is returned.
        
        Note that critter uses PLAYER's abilities to pass doors.
        """
        blocked = False
        CAN_PASS = None
        for c in collisions:
            if c is self:
                continue
            if self.spam: print 'Critter: collision:',c.name
            if isinstance(c, lock.Lockable) and not isinstance(c, food.Food):
                if c.can_pass(State.game.player):
                    if self.spam: print 'Critter: can pass Lockable'
                    CAN_PASS = c
                else:
                    if self.spam: print 'Critter: cannot pass Lockable'
                    blocked = True
        if blocked:
            # try X axis
            if self.spam: print 'BLOCKED: try X axis'
            r1,c1 = self.test_collide(self.rect, dx, 0)
            if any([o is not CAN_PASS and not isinstance(o, (food.Food,Critter)) for o in c1]):
                # blocked, try Y axis
                if self.spam: print 'BLOCKED: try Y axis',c1
                r2,c2 = self.test_collide(self.rect, 0, dy)
                if any([o is not CAN_PASS and not isinstance(o, (food.Food,Critter)) for o in c2]):
                    # X and Y are blocked, set to_rect=None for no-pass
                    if self.spam: print 'BLOCKED: X and Y are blocked', c2
                    to_rect = None
                else:
                    # Y axis not blocked, use it
                    if self.spam: print 'BLOCKED: Y axis not blocked, use it'
                    to_rect = r2
            else:
                # X axis not blocked, use it
                if self.spam: print 'BLOCKED: X axis not blocked, use it'
                to_rect = r1
        if self.spam and not to_rect: print 'BLOCKED: --- cannot move ---'
        return to_rect

## If a game object has no draw() method, Game.draw() will draw objects in a
## batch, which is more effecient. If the critter needs special rendering, the
## inheriting class can simply implement a draw(self, dt) method and it will
## automatically be invoked. See toolkit.draw_sprites().
#
#    def draw(self):
#        surf = State.screen.surface
#        surf.blit(self.image, State.camera.world_to_screen(self.rect))
#        x,y = State.camera.rect.topleft
#        pygame.draw.circle(surf, (255,255,255), self.position-(x,y), self.look_area.w/2, 1)
#        pygame.draw.circle(surf, (255,0,0), self.position-(x,y), self.melee_area.w/2, 1)
#        pygame.draw.circle(surf, (255,255,0), self.position-(x,y), settings.max_range_attack, 1)
    
    def hit(self, damage=1):
        print 'Critter: OUCH!'
        self.health -= damage
        State.game.floating_texts.append(
            combattext.CombatText(self, -damage, pygame.Color('yellow')))
        if self.tooltip:
            self.tooltip.refresh()
        if self.health <= 0:
            sfx.play('grunt1')
            self.kill()
        # Do the animation
        hit_anim = animation.hit_animation()
        hit_anim.rect = hit_anim.get_image().get_rect()
        hit_anim.rect.center = State.camera.world_to_screen(self.position)
        State.game.hit_anims.append(hit_anim)
    
    def kill(self):
        ## TO DO: drop food
        ## TO DO: death throe
        if self.is_alive:
            print "Critter: He's dead, Jim..."
            self._alive = False
            State.game.level.objects.remove(self)
            if self.spawner:
                self.spawner.kill(self)
    
    @property
    def spam(self):
        return settings.spam.get('Critter', False)
