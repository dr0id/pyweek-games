import pygame
from pygame.locals import *

import gummworld2
from gummworld2 import State

import settings


class Text(pygame.sprite.Sprite):
    
    def __init__(self, text_img, y):
        self.image = text_img
        self.rect = text_img.get_rect(y=y, centerx=State.screen.rect.centerx)


class Credits(gummworld2.Engine):
    
    def __init__(self):
        gummworld2.Engine.__init__(self,
            resolution=settings.resolution,
            update_speed=30, frame_speed=30,
        )
    
    def enter(self):
        gummworld2.Engine.enter(self)
        font_file = gummworld2.data.filepath('font', 'VeraBd.ttf')
        font = pygame.font.Font(font_file, 32)
        screen_centery = State.screen.rect.centery
        text_height = font.get_height()
        
        self.buttons = []
        
        n = -5
        text_img = font.render('Coding: Gummbum', True, pygame.Color('white'))
        self.buttons.append(Text(text_img, screen_centery + n * text_height))
        
        n += 2
        text_img = font.render('3D Art, Animations, and Coding: DR0ID', True, pygame.Color('white'))
        self.buttons.append(Text(text_img, screen_centery + n * text_height))
        
        n += 2
        text_img = font.render('Concept: DR0ID and Gummbum', True, pygame.Color('white'))
        self.buttons.append(Text(text_img, screen_centery + n * text_height))
        
        n += 2
        text_img = font.render('Level Design and Maps: Gummbum', True, pygame.Color('white'))
        self.buttons.append(Text(text_img, screen_centery + n * text_height))
        
        n += 2
        text_img = font.render('Sound and SFX: JDruid', True, pygame.Color('white'))
        self.buttons.append(Text(text_img, screen_centery + n * text_height))
    
    def draw(self, dt):
        State.screen.clear()
        for b in self.buttons:
            State.screen.blit(b.image, b.rect)
        State.screen.flip()
    
    def on_mouse_button_down(self, pos, button):
        self.pop()
    
    def on_key_down(self, unicode, key, mod):
        self.pop()
    
    def on_quit(self):
        self.pop()
