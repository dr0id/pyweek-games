import random

import pygame

from gummworld2 import State, Vec2d
from gummworld2 import geometry

import food


class FoodSpawn(object):
    """spawns a single kind of food
    """
    
    def __init__(self, map_obj=None, food_class=food.Food, position=None, init_out=1, max_out=1, spawn_rate=8., capacity=0, class_args=None, class_kwargs=None):
        """construct
        
        map_obj is an object from the Tiled map. It can contain any of the
        constructor arguments as properties. If map_obj is None, then
        the other arguments will be used. If a property is missing, the default
        argument will be used.
        
        food_class is the class that constructs a treat.
        
        init_out is the number of treats to spawn immediately.
        
        max_out is the maximum number of treats that are alive at any time. If
        the treat count goes below this, the spawn rate timer begins after which
        another treat spawns to maintain treat count==max_out.
        
        spawn_rate is the number of seconds to elapse before spawning another
        treat to maintain treat count==max_out.
        
        capacity is the total number of treats this spawner will create. The
        value 0 means unlimited.
        """
        self.visible = 0
        self.food_class = food_class
        self.class_args = []
        self.class_kwargs = {}
        if map_obj:
            self.name = map_obj.name
            props = map_obj.properties
            self.position = Vec2d(map_obj.x,map_obj.y)
            self.init_out = int(props.get('init_out', init_out))
            self.max_out = int(props.get('max_out', max_out))
            self.spawn_rate = float(props.get('spawn_rate', spawn_rate))
            self.capacity = int(props.get('capacity', capacity))
            self.placement = tuple([int(s) for s in props.get('placement', '32 64').split()])
            self.class_kwargs['effects'] = {}
            for p in props:
                if p in food.EFFECTS:
                    self.class_kwargs['effects'][p] = int(props[p])
            self.rect = pygame.Rect(self.position, (map_obj.width, map_obj.height))
        else:
            self.name = 'Food Spawn'
            self.position = Vec2d(position)
            self.init_out = init_out
            self.max_out = max_out
            self.spawn_rate = spawn_rate
            self.capacity = capacity
            self.rect = pygame.Rect(self.position, (32,32))
        
        self.image = pygame.Surface(self.rect.size)
        self.image.fill(map_obj.color)
        
        self.spawn_timer = 0.
        self.treats = []
    
    def update(self, dt):
        while self.init_out:
            self.spawn_food()
            self.init_out -= 1
        if len(self.treats) < self.max_out:
            # not enough treats about; run timer up
            self.spawn_timer += dt
        if self.spawn_timer >= self.spawn_rate:
            # timer dinged; make a treat
            self.spawn_food()
            self.spawn_timer %= self.spawn_rate
    
    def spawn_food(self):
        if __debug__: print 'FoodSpawn: spawning a',self.food_class.__name__
        radius = random.randrange(*self.placement)
        angle = random.randrange(0,359)
        spawn_pos = geometry.point_on_circumference(self.position, radius, angle)
        kwargs = dict(self.class_kwargs)
        kwargs['name'] = self.name
        kwargs['position'] = spawn_pos
        kwargs['size'] = 32,32
        kwargs['spawner'] = self
        food = self.food_class(*self.class_args, **kwargs)
        State.game.level.objects.add(food)
        self.treats.append(food)
    
    def kill(self, food):
        if food in self.treats:
# Not needed at this time
#            food.kill()
            self.treats.remove(food)
