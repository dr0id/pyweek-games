import pygame
from pygame.locals import *

import gummworld2
from gummworld2 import data, geometry, toolkit
from gummworld2 import State, Vec2d

import settings, level, player, mousetarget
import door, sign, food, foodspawn, critter, critterspawn, wall
import ability, abilitydash, tooltip, pausemenu, sfx


class Game(gummworld2.Engine):
    
    def __init__(self):
        if __debug__: print 'Game: INIT'
        gummworld2.Engine.__init__(self,
            resolution=settings.resolution,
            camera_target=gummworld2.model.Object(),
            camera_view_rect=settings.game_layout['playfield'],
            update_speed=settings.game_ticks_per_second, frame_speed=0,
            set_state=False,
        )
        
        State.game = self
        
        self.player = player.Player()
        self.mouse_target = mousetarget.MouseTarget()
        self.collision_excludes = [
            self.player,
            self.mouse_target,
        ]
        
        if __debug__: print 'Game: initializing ability dashboard'
        self.ability_dash = abilitydash.AbilityDash()
        for name in ability.GAUGE_ORDER:
            player_ability = self.player.abilities[name]
            ability_gauge = abilitydash.AbilityGauge(player_ability)
            self.ability_dash.add(ability_gauge)
        
        self.clock.schedule_interval(self.show_stats, 2.)
        
    def enter(self):
        gummworld2.Engine.enter(self)
        if not settings.systemcursor:
            pygame.mouse.set_visible(False)
        sfx.play('magicrock')
    
    def resume(self):
        gummworld2.Engine.resume(self)
        if not settings.systemcursor:
            pygame.mouse.set_visible(False)
        sfx.play('magicrock')
    
    def exit(self):
        gummworld2.Engine.exit(self)
        if not settings.systemcursor:
            pygame.mouse.set_visible(True)
    
    def suspend(self):
        gummworld2.Engine.suspend(self)
        if not settings.systemcursor:
            pygame.mouse.set_visible(True)
    
    def load_level(self, map_file):
        if __debug__: print 'Game: loading map file',map_file
        
        self.movex = 0
        self.movey = 0
        self.move_to = None
        self.mouse_down = False
        self.tooltip = None
        
        # self.level = level.Level('concept2.tmx')
        # self.level = level.Level('001-1.tmx')
        self.map_file = map_file
        self.level = level.Level(map_file)
        self.map = self.level.map
        self.layers = [[] for l in self.map.layers]
#        self.ground_objects = []
        self.ground_objects_on_screen = []
        self.floating_texts = []
        self.hit_anims = []
        
        # This IF is so we can run the mana world map.
        if self.level.in_door:
            spawn_position = self.level.in_door.rect.center
        else:
            spawn_position = self.map.rect.center
        self.camera.init_position(spawn_position)
        self.camera.rect.clamp_ip(self.map.rect)
        self.camera.init_position(self.camera.rect.center)
        
        self.player.reset_stats()
        self.player.rect.center = spawn_position
        self.player.rect.clamp_ip(self.map.rect)
        self.player.position = self.player.rect.center
        self.level.objects.add(self.player)
        
        self.mouse_target.peace()
        
        State.map = self.map
    
    def reload_level(self):
        self.pop()
        self.load_level(self.map_file)
        self.push(self)
    
    def won_level(self):
        sfx.play('magicdoor')
        self.player.update(0)
        self.level.won_level()
    
    def lost_level(self):
        self.player.update(0)
        self.level.lost_level()
    
    def update(self, dt):
        if State.game is not self:
            return
        
        if self.level.won or self.level.lost:
            self.update_end_level(dt)
            mouse_pos = pygame.mouse.get_pos()
            self.update_mouse_movement(mouse_pos)
            self.update_mouse_over(mouse_pos)
            self.get_tiles()
        else:
            self.update_spawners(dt)
            mouse_pos = pygame.mouse.get_pos()
            self.update_mouse_movement(mouse_pos)
            self.update_mouse_over(mouse_pos)
            self.update_player(dt)
            self.update_critters(dt)
            self.update_camera()
            self.get_tiles()
    
    def update_spawners(self, dt):
        level = self.level
        for collection in (level.critter_spawns, level.critter_groups, level.food_spawns):
            for spawner in collection:
                spawner.update(dt)
    
    def get_tiles(self):
        ## cull tiles and objects
        above_ground_classes = critter.Critter,food.Food,sign.Sign
        on_ground_classes = door.Door,door.Fire,door.Ice
        objects_on_screen = self.level.objects.intersect_objects(self.camera.rect)
        fringe_objects = [o for o in objects_on_screen if isinstance(o,above_ground_classes)]
        fringe_objects.append(self.player)
        self.ground_objects_on_screen[:] = [o for o in objects_on_screen if o.visible and isinstance(o,on_ground_classes)]
        self.ground_objects_on_screen.sort(key=lambda o:o.rect.bottom)
        #
        self_layers = self.layers
        cam_rect = self.camera.rect
        for i,self_layer in enumerate(self_layers):
            del self_layer[:]
            map_layer = self.level.tile_layers[i]
#            for t in map_layer.intersect_objects(cam_rect):
#                self_layer.append(t)
            self_layer.extend(map_layer.intersect_objects(cam_rect))
            ## adding fringe_objects to fringe layer
            if i == 1:
                self_layer.extend(fringe_objects)
                self_layer.append(self.player)
            self_layer.sort(key=lambda s:s.rect.bottom)
#        ## culling ground objects (exclude fringe_objects)
#        visible_objs = self.level.objects.intersect_objects(self.camera.rect)
#        exclude = self.collision_excludes + fringe_objects
#        self.ground_objects = [s for s in visible_objs
#                if hasattr(s, 'image') and s not in exclude]
    
    def update_mouse_movement(self, pos):
        speed = self.player.speed
        if self.mouse_down:
            # Mouse held down. Update move_to if it is a minimum distance from
            # player (avoids stop-start scrolling).
            move_to = State.camera.screen_to_world(pos)
            if geometry.distance(self.player.position, move_to) > speed*3:
                self.move_to = move_to
        if self.move_to:
            # move_to is set. Step towards move_to.
            if geometry.distance(self.player.position, self.move_to) < speed:
                # Less than a frame of distance left. Just go there.
                self.movex,self.movey = self.move_to - self.player.position
                self.move_to = None
                self.movex = 0
                self.movey = 0
            else:
                # More than a frame of distance left. Step one PLAYER SPEED unit
                # in the direction of move_to.
                angle = geometry.angle_of(self.player.position, self.move_to)
                new_pos = Vec2d(geometry.point_on_circumference(
                    self.player.position, speed, angle))
                self.movex,self.movey = new_pos - self.player.position
    
    def update_mouse_over(self, mouse_pos):
        # get objects colliding with mouse
        # if collide() then take first in list as target; maybe sort on distance?
        player_obj = self.player
        mouse_target = self.mouse_target
        targetable_objs = self.level.objects.collide(mouse_target)
        if not targetable_objs:
            # No targetable game objects. Try tooltip for an ability gauge.
            self.tooltip = None
#            self.player.target = None
            test_pos = Vec2d(mouse_pos) - self.ability_dash.view.abs_offset
            if self.ability_dash.view.parent_rect.collidepoint(mouse_pos):
                for g in self.ability_dash.gauges:
                    if g.rect.collidepoint(test_pos):
                        self.tooltip = getattr(g, 'tooltip', None)
                        if not self.tooltip:
                            self.tooltip = tooltip.Tooltip(g)
                        break
            else:
                return
        
        # Precedence of selection:
        #   critter
        #   self
        #   door
        #   food
        #   exit
        #   sign
        #   foodspawn
        #   critterspawn
        cclass = critter.Critter
        critters = [o for o in targetable_objs if isinstance(o, cclass)]
        if critters:
            if player_obj.target not in critters:
                player_obj.target = critters[0]
            # else, keep the previous target
        elif self.player in targetable_objs:
            player_obj.target = player_obj
        else:
            # Low-priority targetables
            dclass = door.Door,door.Fire,door.Ice
            fclass = food.Food
            sclass = sign.Sign
            fsclass = foodspawn.FoodSpawn
            wclass = wall.Wall
            csclass = critterspawn.CritterSpawn,critterspawn.CritterGroup
            doors = [o for o in targetable_objs if isinstance(o, dclass)]
            foods = [o for o in targetable_objs if isinstance(o, fclass)]
            signs = [o for o in targetable_objs if isinstance(o, sclass)]
            walls = [o for o in targetable_objs if isinstance(o, wclass)]
            foodspawns = [o for o in targetable_objs if isinstance(o, fsclass)]
            critterspawns = [o for o in targetable_objs if isinstance(o, csclass)]
            player_obj.target = None
            for group in (doors,foods,signs,foodspawns,critterspawns,walls):
                if group:
                    player_obj.target = group[0]
                    break
        
        if player_obj.target:
            # Get tooltip for object
            self.tooltip = getattr(player_obj.target, 'tooltip', None)
            if not self.tooltip:
                self.tooltip = tooltip.Tooltip(player_obj.target)
    
    def update_player(self, dt):
        player = self.player
        game_objects = self.level.objects
        player.update(dt)
#        if not player.move(dt, self.movex, self.movey):
        pos = player.position[:]
        player.move(dt, self.movex, self.movey)
        if player.position == pos:
            self.movex = self.movey = 0
            self.move_to = None
        game_objects.add(player)
        
        out_door = self.level.out_door
        for c in game_objects.collide(player):
            if c is out_door:
                self.won_level()
    
    def update_critters(self, dt):
        for c in self.level.objects:
            if isinstance(c, critter.Critter):
                c.update(dt)
    
    def update_camera(self):
        r = Rect(State.camera.rect)
        r.center = self.player.rect.center
        r.clamp_ip(State.map.rect)
        State.camera.position = r.center
    
    def update_end_level(self, dt):
        self.level.countdown -= dt
        if self.level.countdown <= 0:
            if self.level.lost:
                self.pop()
                self.reload_level()
                self.push(self)
            else:
                self.pop()
    
    def draw(self, dt):
        if State.game is not self:
            return
        State.screen.clear()
        self.draw_tiles_bg()
        self.draw_ground_objects()
        self.draw_tiles_fringe()
        self.draw_tiles_over()
        self.draw_hits()
        self.ability_dash.draw()
        self.mouse_target.position = pygame.mouse.get_pos()
        self.mouse_target.draw()
        self.draw_food_text(dt)
        self.draw_tooltip()
        State.screen.flip()
    
    def draw_tiles_bg(self):
        toolkit.draw_sprites(self.layers[0])
    
    def draw_tiles_fringe(self):
        toolkit.draw_sprites(self.layers[1])
    
    def draw_tiles_over(self):
        # mana world layer
        if len(self.map.layers) >= 3:
            toolkit.draw_sprites(self.layers[2])
    
    def draw_hits(self):
        for h in self.hit_anims[:]:
            if h.stopped:
                self.hit_anims.remove(h)
            else:
                State.screen.blit(h.get_image(), h.rect)
    
    def draw_ground_objects(self):
        # food,foodspawn,critterspawn,door
        # critter,sign,wall
#        food_class = food.Food
#        classes1 = critter.Critter,sign.Sign,wall.Wall,food.Food
#        classes2 = critter.Critter,sign.Sign,wall.Wall
#        toolkit.draw_sprites([o for o in self.ground_objects if o.visible and not isinstance(o, classes1)])
#        toolkit.draw_sprites([o for o in self.ground_objects if o.visible and isinstance(o, food_class)])
#        toolkit.draw_sprites([o for o in self.ground_objects if o.visible and isinstance(o, classes2)])
        toolkit.draw_sprites(self.ground_objects_on_screen)

    def draw_food_text(self, dt):
        for f in self.floating_texts[:]:
            if f.draw(dt): self.floating_texts.remove(f)
    
    def draw_tooltip(self):
        tt = self.tooltip
        if tt:
            tt.position = self.camera.world_to_screen(
                self.mouse_target.position + (24,-16))
            tt.draw()
        tt = self.level.tooltip
        if tt:
            tt.draw()
    
    def show_stats(self, dt):
        pygame.display.set_caption(
            '{clock.ups:d} ups - {clock.fps:d} fps'.format(clock=self.clock))
    
    def action_quit(self):
        n = 1
        if settings.quickquit:
            n = 3
        self.pop(n)
    
    def action_pause(self):
        self.push(pausemenu.PauseMenu())
    
    def on_mouse_button_down(self, pos, button):
        if button == 1:
            self.mouse_down = True
            self.player.new_move()
        elif button == 3:
            self.player.attack()
    
    def on_mouse_button_up(self, pos, button):
        if button == 1:
            self.mouse_down = False
    
    def on_mouse_motion(self, pos, rel, buttons):
        mouse_target = self.mouse_target
        if not mouse_target.visible:
            mouse_target.visible = True
#        mouse_target.position = State.camera.screen_to_world(pos)
#        self.level.objects.add(mouse_target)
    
    def on_key_down(self, unicode, key, mod):
#        if key == K_LEFT:
#            self.movex = -1
#        elif key == K_RIGHT:
#            self.movex = 1
#        elif key == K_UP:
#            self.movey = -1
#        elif key == K_DOWN:
#            self.movey = 1
#        elif key == K_ESCAPE:
#            self.action_quit()
        if key == K_ESCAPE:
            self.action_pause()
#        elif key == K_UP:
#            self.player.mutations.arms.value += 1
#            print self.player.mutations.arms.value
#        elif key == K_DOWN:
#            self.player.mutations.arms.value -= 1
#            print self.player.mutations.arms.value
    
#    def on_key_up(self, key, mod):
#        if key in (K_LEFT,K_RIGHT):
#            self.movex = 0
#        elif key in (K_UP,K_DOWN):
#            self.movey = 0
    
    def on_active_event(self, gain, state):
        if gain:
            self.mouse_target.visible = True
        else:
            self.mouse_target.visible = False
        self.movex = 0
        self.movey = 0
        pygame.key.set_mods(0)
    
    def on_quit(self):
        self.action_pause()
