__doc__ = """
Map object properties:
    name :  If name is set on the map object it is used; else, the class
            constructor should give the object a default name.
    
    type :  If type is set on the map object it passes map_obj to the
            appropriate class; else, group.name is used.
    
    lock :  See class constructors. Most objects should honor the lock value on
            map objects, and fall back on a class-specific default.
"""


import settings
import wall
import door
import food
import foodspawn
import critter
import critterspawn
import moldprotector
import sign


def make_object(group, map_obj):
    obj_type = str(map_obj.type).lower()
    group_name = str(group.name).lower()
    
    if 'ice' in  (obj_type,group_name):
        return door.Ice(map_obj)
    
    if 'fire' in (obj_type,group_name):
        return door.Fire(map_obj)
    
    if 'door' in (obj_type,group_name):
        return door.Door(map_obj)
    
    elif 'food' in (obj_type,group_name):
        return food.Food(map_obj)
    
    elif 'foodspawn' in (obj_type,group_name):
        return foodspawn.FoodSpawn(map_obj)
    
    elif 'wall' in (obj_type,group_name):
        return wall.Wall(map_obj)
    
    elif obj_type == 'moldprotector':
        return moldprotector.MoldProtector(map_obj)
    
    elif 'critter' in (obj_type,group_name):
        return critter.Critter(map_obj)
    
    elif 'start' in (obj_type,group_name):
        return door.InOutDoor(map_obj)
    elif 'exit' in (obj_type,group_name):
        return door.InOutDoor(map_obj)
        # return door.Door(map_obj)
    
    elif 'critterspawn' in (obj_type,group_name):
        return critterspawn.CritterGroup(map_obj)

    elif 'sign' in (obj_type,group_name):
        return sign.Sign(map_obj)
    
    else:
        msg = 'unhandled type "{0}" or group name "{1}"'.format(
            obj_type, group_name,
        )
        if settings.errormapobjects:
            raise ValueError, msg
        else:
            print '## gameobjects.make_object: ' + msg
