import random

import pygame

from gummworld2 import State, Vec2d
from gummworld2 import data
from gummworld2.geometry import distance, angle_of, point_on_circumference


font = None

class CombatText(object):
    
    def __init__(self, target, value, color):
        global font
        
        if __debug__: print 'CombatText:',target,value,color
        
        if not font:
            font_name = data.filepath('font','VeraBd.ttf')
            font = pygame.font.Font(font_name, 13)
        
        if isinstance(value, (str,unicode)):
            text = value
        else:
            text = '{0:+.0f}'.format(value)
        if isinstance(color, pygame.Color):
            pass
        elif isinstance(color, str):
            color = pygame.Color(color)
        else:
            color = pygame.Color(*color)
        self.image = font.render(text, True, color)
        self.rect = self.image.get_rect()
        self._position = Vec2d(0,0)
#        pos = State.camera.world_to_screen(target.position)
        pos = State.camera.world_to_screen((target.rect.centerx,target.rect.y))
        self.position = pos
        
        self.move_to = pos + (random.randrange(0,32)-32, -100)
        
        self.speed = 1
        self.accel = lambda s:9.8 / (1000000*s*s)
        
        State.game.floating_texts.append(self)
    
    @property
    def position(self):
        return self._position
    @position.setter
    def position(self, val):
        p = self._position
        p.x,p.y = val
        self.rect.center = round(p.x),round(p.y)
    
    def draw(self, dt):
        """step and draw a food text
        
        If the food text has arrived at its destination, the object is returned
        for handling. Else, None is returned.
        """
        if distance(self.position, self.move_to) < 1:
            return self
        angle = angle_of(self.position, self.move_to)
        speed = self.speed * dt
        self.speed += self.accel(dt)
        self.position = point_on_circumference(self.position, speed, angle)
        State.screen.blit(self.image, self.rect)
