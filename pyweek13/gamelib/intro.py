import random

import pygame

import gummworld2
from gummworld2 import State

import settings, mainmenu

class Intro(gummworld2.Engine):
    
    def __init__(self):
        gummworld2.Engine.__init__(self,
            resolution=settings.resolution,
            update_speed=60, frame_speed=60,
            set_state=False,
        )
    
    def enter(self):
        gummworld2.Engine.enter(self)
        if settings.quickstart:
            self.quit_intro()
        self.screen.clear_color = pygame.Color('darkgreen')
        font_file = gummworld2.data.filepath('font', 'VeraBd.ttf')
        font = pygame.font.Font(font_file, 32)
        self.text_img = font.render(
            'Intro (Wait 10 or Click)', True, pygame.Color('white'))
        self.text_rect = self.text_img.get_rect()
        self.dx = random.choice((-3,-2,-1,1,2,3))
        self.dy = random.choice((-3,-2,-1,1,2,3))
        self.countdown = 10  ## seconds
    
    def update(self, dt):
        self.countdown -= dt
        if self.countdown <= 0.:
            self.quit_intro()
        
        text_rect = self.text_rect
        text_rect.x += self.dx
        text_rect.y += self.dy
        screen_rect = State.screen.rect
        if not screen_rect.contains(text_rect):
            if text_rect.x < 0 or text_rect.right >= screen_rect.right:
                self.dx *= -1
            if text_rect.y < 0 or text_rect.bottom >= screen_rect.bottom:
                self.dy *= -1
            self.text_rect.clamp_ip(screen_rect)
    
    def draw(self, dt):
        State.screen.clear()
        State.screen.blit(self.text_img, self.text_rect)
        State.screen.flip()
    
    def quit_intro(self):
        self.pop()
        self.push(mainmenu.MainMenu())
    
    def on_mouse_button_down(self, pos, button):
        self.quit_intro()
