__doc__ = """
USAGE
    # sfx is the module
    sfx.play('name1')
    sfx.sfx.name1.sound.play()
    sfx.sfx['name1'].sound.play()

DETAILS
    sfx = {
        'name1' : {
            'name' : 'attr name',
            'path' : 'directory',
            'filename' : 'short filename',
            'fullname' : 'fully qualified path to file',
            'sound' : 'pygame.mixer.Sound',
            'volume' : '0.0 to 1.0',
        },
    }

    OR

    sfx.name1           # an sfx data structure
    sfx.name1.name      # sfx name
    sfx.name1.path      # directory
    sfx.name1.filename  # short filename
    sfx.name1.fullname  # fully qualified path to file
    sfx.name1.sound     # pygame.mixer.Sound
    sfx.name1.volume    # default volume for this sound

EXTERNAL INFLUENCES
    If a sound 'name' is mentioned in the settings.sfx_volumes dict, its value
    will be used as the default volume for the sound.

BUGS
    Changing a sound's volume is persistent.
"""

import os, glob

import pygame

from gummworld2 import data

import settings

def play(name):
    sfx[name].sound.play()


class _Struct(dict):
    def __init__(self, **kw):
        self.update(**kw)
        self.__dict__.update(**kw)
    def add(self, key, val):
        self[key] = val
        self.__dict__[key] = val

sfx = _Struct()

def load_sound(name, filename):
    if os.path.sep not in filename:
        path = data.paths['sound']
    else:
        path,filename = os.path.split(filename)
        filename = data.filepath('sound', filename)
    volume = settings.sfx_volumes.get(name, 1.0)
    fullname = os.path.join(path,filename)
    sound = pygame.mixer.Sound(filename)
    sound.set_volume(volume)
    sfx.add(name, _Struct(
        name=name,
        path=path,
        filename=filename,
        fullname=fullname,
        sound=sound,
        volume=volume,
    ))

def load_all(path):
    if __debug__: print 'SFX: path',path
    for fp in glob.glob(path):
        f = os.path.basename(fp)
        name,ext = os.path.splitext(f)
        if __debug__: print 'SFX: loading',name,':',f
        load_sound(name, fp)

load_all(os.path.join(data.paths['sound'], '*.ogg'))


if __name__ == '__main__':
    screen = pygame.display.set_mode((400,400))
#    for name in sorted(sfx.keys()):
#    for name in ['ouch1','ouch2','ouch3','ouch4',]:
    for name in ['magicdoor','magicrock','win',]:
        print 'Playing', name, ':', file
        chan = sfx[name].sound.play()
        while chan.get_busy():
            pygame.time.wait(500)
