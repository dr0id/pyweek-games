import pygame
from pygame.locals import *

import gummworld2
from gummworld2 import State
from gummworld2 import context

import settings, loader, credits


class ClickButton(pygame.sprite.Sprite):
    
    def __init__(self, text_img, y, action):
        self.image = text_img
        self.rect = text_img.get_rect(y=y, centerx=State.screen.rect.centerx)
        self.action = action
    
    def click(self, mouse_pos):
        if self.rect.collidepoint(mouse_pos):
            self.action()


class PauseMenu(gummworld2.Engine):
    
    def __init__(self):
        gummworld2.Engine.__init__(self,
            resolution=settings.resolution,
            update_speed=60, frame_speed=60,
            set_state=False,
        )
    
    def enter(self):
        gummworld2.Engine.enter(self)
        font_file = gummworld2.data.filepath('font', 'VeraBd.ttf')
        font = pygame.font.Font(font_file, 32)
        screen_centery = State.screen.rect.centery
        text_height = font.get_height()
        
        self.buttons = []
        
        n = -1
        text_img = font.render('RESUME GAME', True, pygame.Color('white'))
        self.buttons.append(ClickButton(
            text_img, screen_centery + n * text_height, self.action_resume_game))
        
        n += 1
        text_img = font.render('RESTART LEVEL', True, pygame.Color('white'))
        self.buttons.append(ClickButton(
            text_img, screen_centery + n * text_height, self.action_restart_level))
        
        n += 1
        text_img = font.render('CREDITS', True, pygame.Color('white'))
        self.buttons.append(ClickButton(
            text_img, screen_centery + n * text_height, self.action_show_credits))
        
        n += 1
        text_img = font.render('QUIT GAME', True, pygame.Color('white'))
        self.buttons.append(ClickButton(
            text_img, screen_centery + n * text_height, self.action_quit_game))
    
    def exit(self):
        ## Outro?
        gummworld2.Engine.exit(self)
    
    def action_resume_game(self):
        self.pop()
    
    def action_restart_level(self):
        context.cstack[-2].reload_level()
        self.pop()
    
    def action_new_game(self):
        self.pop(4)
        self.push(self)
        self.push(loader.Loader())
    
    def action_show_credits(self):
        self.push(credits.Credits())
    
    def action_quit_game(self):
        self.pop(4)
    
    def update(self, dt):
        pass
    
    def draw(self, dt):
        State.screen.clear()
        for b in self.buttons:
            State.screen.blit(b.image, b.rect)
        State.screen.flip()
    
    def on_mouse_button_down(self, pos, button):
        for b in self.buttons:
            b.click(pos)
    
    def on_key_down(self, unicode, key, mod):
        if key == K_ESCAPE:
            self.action_resume_game()
        else:
            print(context.cstack)
    
    def on_quit(self):
        self.action_quit_game()
