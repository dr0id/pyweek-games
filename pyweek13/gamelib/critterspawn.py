__doc__ = """
CritterGroup objects can be loaded from Tiled map objects. Properties must be
specified as space-delimited strings. The strings will be split into arrays so
that element 0 of each property is an argument for CritterSpawn 0 in the
CritterGroup's constructor.

Required properties:
    
    type            "critterspawn"; can be blank if layer name is "critterspawn"
                    (see gameobjects.make_objects)
    classes         Keys in critterspawn.SPAWN_TYPES for all the critters this
                    group will make.
    position        NO NEED TO SPECIFY: x,y position; this is taken from the map
                    object's builtin properties
    
Optional properties (see default args in CritterSpawn's constructor):
    
    class_args      NOT YET IMPLEMENTED
    class_kwargs    NOT YET IMPLEMENTED
    init_out        int; default 0
    max_out         int; default 2
    spawn_rate      float or int; default 8.
    capacity        int; capacity 0
"""

import random

import pygame

from gummworld2 import State, Vec2d
from gummworld2 import geometry

import critter, moldprotector

## Add new types here for map objects to use.
SPAWN_TYPES = {
    '0' : critter.Critter,
    '1' : moldprotector.MoldProtector,
}


class CritterSpawn(object):
    """spawns a single kind of critter
    """
    
    name = 'CritterSpawn'
    
    def __init__(self, critter_class, position, init_out=1, max_out=1, spawn_rate=4., capacity=0, health=None, class_args=[], class_kwargs={}):
        """construct
        
        critter_class is the class that constructs a critter
        
        init_out is the number of critters to spawn immediately.
        
        max_out is the maximum number of critters that are alive at any time. If
        the critter count goes below this, the spawn rate timer begins after which
        another critter spawns to maintain critter count==max_out.
        
        spawn_rate is the number of seconds to elapse before spawning another
        critter to maintain critter count==max_out.
        
        capacity is the total number of critters this spawner will create. The
        value 0 means unlimited.
        """
        self.visible = False
        self.position = Vec2d(position)
        self.critter_class = critter_class
        self.class_args = class_args
        self.class_kwargs = class_kwargs
        self.init_out = init_out
        self.max_out = max_out
        self.spawn_rate = spawn_rate
        self.capacity = capacity
        
        if health:
            self.class_kwargs['health'] = health
        
        self.spawn_timer = 0.
        self.critters = []
    
    def update(self, dt):
        while len(self.critters) < self.init_out:
            self.spawn_critter()
            self.init_out -= 1
        if len(self.critters) < self.max_out:
            # not enough critters running about; run timer up
            self.spawn_timer += dt
        if self.spawn_timer >= self.spawn_rate:
            # timer dinged; make a critter
            self.spawn_critter()
            self.spawn_timer %= self.spawn_rate
    
    def spawn_critter(self):
        if __debug__: print 'CritterSpawn: spawning a',self.critter_class.__name__
#        dx = random.randrange(100)
#        dy = random.randrange(100)
        radius = random.randrange(32,32+32)
        angle = random.randrange(0,359)
        spawn_pos = geometry.point_on_circumference(self.position, radius, angle)
        kwargs = dict(self.class_kwargs)
#        kwargs['position'] = self.position + (dx-50,dy-50)
        kwargs['position'] = spawn_pos
        kwargs['spawner'] = self
        critter = self.critter_class(*self.class_args, **kwargs)
        State.game.level.objects.add(critter)
        self.critters.append(critter)
    
    def kill(self, critter):
        if critter in self.critters:
            critter.kill()
            self.critters.remove(critter)


class CritterGroup(object):
    """collection of CritterSpawn for a single location
    """
    
    def __init__(self, map_obj=None, spawners=[], group_name=None):
        self.visible = False
        self.spawners = []
        if map_obj:
            self._parse_spawners(map_obj)
            if not group_name:
                group_name = map_obj.name
            self.rect = pygame.Rect(map_obj.x,map_obj.y,map_obj.width,map_obj.height)
            self.image = pygame.Surface(self.rect.size)
            self.image.fill(map_obj.color)
        elif spawners:
            self.rect.center = pygame.Rect(spawners[0].rect)
        else:
            self.rect = pygame.Rect(0,0,32,32)
        self.name = group_name if group_name else 'Generic Spawner'
        self.spawners.extend(spawners)
    
    def _parse_spawners(self, obj):
        props = obj.properties
        n = len(props.get('classes', '').split())
        kw = None
        def getprop(i, conv, name):
            value = props.get(name, '')
            if value:
                parts = value.split()
                if len(parts) > i:
                    part = parts[i]
                    data = conv(part)
                    return data
                elif __debug__:
                    print 'CritterGroup: map object missing data:',obj.name,name
            else:
                return None
        def kwadd(i, conv, name):
            data = getprop(i, conv, name)
            if data is not None:
                kw[name] = data
        for i in range(n):
            kw = {}
            cls = getprop(i, str, 'classes')
            kwadd(i, int, 'init_out')
            kwadd(i, int, 'max_out')
            kwadd(i, float, 'spawn_rate')
            kwadd(i, int, 'capacity')
            kwadd(i, int, 'health')
            self.rect = pygame.Rect(obj.x,obj.y,obj.width,obj.height)
            pos = self.rect.center
            self.spawners.append(CritterSpawn(SPAWN_TYPES[cls], pos, **kw))
        if __debug__: print 'CritterGroup: making CritterSpawn:',kw
    
    def update(self, dt):
        for s in self.spawners:
            s.update(dt)
