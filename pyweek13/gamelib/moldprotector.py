import pygame

import critter


class MoldProtector(critter.Critter):
    
    def __init__(self, map_obj=None, position=None, health=None, color='red', spawner=None):
        critter.Critter.__init__(self, map_obj, position, health, color, spawner)
