import pygame

import gummworld2
from gummworld2 import data, spatialhash, toolkit
from gummworld2 import State

import settings
import gameobjects
import critterspawn
import foodspawn
import door
import sign
import tooltip


class Level(object):
    
    def __init__(self, map_file):
        if __debug__:
            print 'Level: loading map file', map_file
        self.name = map_file
        self.map_file = data.filepath('map',map_file)
#        self.map = toolkit.collapse_map(
#            toolkit.load_tiled_tmx_map(self.map_file), (8,8))
        self.map = toolkit.load_tiled_tmx_map(self.map_file)
        self.map.layers[0] = toolkit.collapse_map_layer(self.map, 0, (8,8))
        
        self.tile_layers = []
        for layer in self.map.layers:
            new_layer = spatialhash.SpatialHash(self.map.rect, 64)
            for tile in layer:
                if tile: new_layer.add(tile)
            self.tile_layers.append(new_layer)
        
        # cell size is 128 for 64-pixel objects
        self.objects = spatialhash.SpatialHash(self.map.rect, 128)
        self.critter_groups = []
        self.critter_spawns = []
        self.food_spawns = []
        self.fire = []
        self.ice = []
        self.signs = []
        
        # load the map objects.
        self.in_door = None
        self.out_door = None
        self.load_objects()
        if settings.errormissingexits:
            if None in (self.in_door,self.out_door):
                raise pygame.error,'map {0} missing exit(s)'.format(map_file)
        
        self.won = False
        self.lost = False
        self.tooltip = None
        self.msg = None
        self.countdown = 0
        self.win_level_msg = 'Great job!'
        self.lose_level_msg = "Let's pretend that didn't happen..."
        self.win_game_msg = "You have attained enlightenment. See you next life!"
    
    def won_level(self):
        self.won = True
        self.msg = self.win_level_msg
        tooltip.Tooltip(self)
        self.countdown = 4
    
    def lost_level(self):
        self.lost = True
        self.msg = self.lose_level_msg
        tooltip.Tooltip(self)
        self.countdown = 4
    
    def add_object(self, obj):
        self.objects.add(obj)
    
    def load_objects(self):
        """load Tiled map objects into a container
        """
        if __debug__:
            print 'Level: loading map objects'
        groups = self.map.tiled_map.object_groups
        for group in groups:
            color = getattr(group, 'color', '#ffffff')
            ## The following blows up when unicode comes from Tiled.
            ##color = pygame.Color(color)
            color = pygame.Color(str(color))
            for obj in group.objects:
                
                ## Allow object to override group.color.
                if 'color' in obj.properties:
                    color = pygame.Color(str(obj.properties['color']))
                obj.color = color
                if not obj.name:
                    ## object name should never be None
                    obj.name = group.name
                ## See gameobjects.py for tips on map object properties.
                
                game_obj = gameobjects.make_object(group, obj)
                if game_obj is None:
                    continue
                if not hasattr(game_obj, 'color'):
                    game_obj.color = color
                if 'msg' in obj.properties:
                    game_obj.msg = obj.properties['msg']
                if not hasattr(game_obj, 'visible'):
                    game_obj.visible = int(obj.properties.get('visible', 1))
                game_obj.tiled_properties = obj.properties
                self.add_object(game_obj)
                
#                if isinstance(game_obj, door.Door):
#                    print '## VISIBLE',game_obj.visible
                
                if isinstance(game_obj, critterspawn.CritterGroup):
                    self.critter_groups.append(game_obj)
                elif isinstance(game_obj, critterspawn.CritterSpawn):
                    self.critter_spawns.append(game_obj)
                elif isinstance(game_obj, foodspawn.FoodSpawn):
                    self.food_spawns.append(game_obj)
                elif isinstance(game_obj, door.Fire):
                    self.fire.append(game_obj)
                elif isinstance(game_obj, door.Ice):
                    self.ice.append(game_obj)
                elif isinstance(game_obj, sign.Sign):
                    self.signs.append(game_obj)
                elif str(game_obj.name).lower() == 'start':
                    self.in_door = game_obj
                elif str(game_obj.name).lower() == 'end':
                    self.out_door = game_obj
