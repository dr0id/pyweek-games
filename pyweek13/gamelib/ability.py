import mutation


COLORS = {
    'reach_out'  : (0,       0,       255*5/8),
    'snatch'     : (0,       0,       255*6/8),
    'strong'     : (0,       0,       255*7/8),
    'attack'     : (0,       0,       255*8/8),
    'reach_high' : (0,       255*5/8, 0),
    'run'        : (255*6/8, 255*6/8, 0),
    'jump'       : (255*7/8, 255*7/8, 0),
    'sturdy'     : (255*8/8, 255*8/8, 0),
    'cold_prot'  : (255*7/8, 0,       0),
    'fire_prot'  : (255*8/8, 0,       0),
}
GAUGE_ORDER = (
    'reach_out',
    'snatch',
    'strong',
    'attack',
    'reach_high',
    'run',
    'jump',
    'sturdy',
    'cold_prot',
    'fire_prot',
)
DESCS = {
    'reach_out'     : ['Mutation: Arms','High values reach farther'],
    'snatch'        : ['Mutation: Arms','High values snatch faster'],
    'strong'        : ['Mutation: Arms','High values greater manual strength'],
    'attack'        : ['Mutation: Arms','High values greater range damage', 'Low values greater melee damage'],
    'run'           : ['Mutation: Legs','High values run faster'],
    'jump'          : ['Mutation: Legs','High values jump higher/farther'],
    'sturdy'        : ['Mutation: Legs','High values strengthen legs'],
    'reach_high'    : ['Mutation: Arms and Legs','High values reach higher','Longer legs and arms contribute'],
    'cold_prot'     : ['Mutation: Skin','High values protect against cold'],
    'fire_prot'     : ['Mutation: Skin','High values protect against fire'],
}


class Abilities(dict):
    
    def add(self, ability):
        """add an ability to self[ability_name] and to self.ability_name
        
        abils = Abilities()
        abil = Ability('run', Mutation('legs'))
        abils.add(abil)
        print abils.run.value
        print abils['run'].value
        """
        self[ability.name] = ability
        self.__dict__[ability.name] = ability
    
    def print_all(self):
        for key in self:
            print self[key]


class Ability(object):
    
    def __init__(self, name, mutations, func=None):
        """Define an ability based on mutations
        
        name is an arbitrary identifier string.
        
        mutations can be a sequence of mutations or a discrete mutation.
        
        func is the function to apply on mutations to produce the return value of
        Ability.value. If func is None then Ability.value will return
        mutations.value. Otherwise, func must be a callable type that accepts one
        argument: mutations. func needs to be able to handle the data type of its
        argument appropriately. Thus, if mutations is a sequence, func must deal
        with the sequence.
        
        Example:
            Ability('Run', one_mut)
            Ability('Reach Up', one_mut, lambda mut:(mut+mut)/2)
            Ability('Nuke', many_muts, lambda muts:sum([m.value for m in muts])
        """
        self.name = name
        self.proper_name = self._proper_name(name)
        self._mutations = mutations
        self._func = func
    
    @property
    def value(self):
        if self._func is None:
            return self._mutations.value
        else:
            return self._func(self._mutations)
    
    @staticmethod
    def _proper_name(name):
        parts = name.split('_')
        cap_parts = [s.capitalize() for s in parts]
        return ' '.join(cap_parts)
    
    def __str__(self):
        if isinstance(self._mutations, mutation.Mutation):
            return 'Ability({obj.name},{obj._mutations}) -> {obj.value:0.1f}'.format(
                obj=self,
            )
        else:
            return 'Ability({obj.name},{mutations}) -> {obj.value:0.1f}'.format(
                obj=self,
                mutations=[str(m) for m in self._mutations],
            )
