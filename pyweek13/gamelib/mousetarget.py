import pygame

from gummworld2 import State, Vec2d
from gummworld2 import data, geometry

import settings


class MouseTarget(object):
    
    def __init__(self):
        self.visible = False
        self.color = pygame.Color('green')
        
        # collisions only
        self.rect = pygame.Rect(0,0,21,21)
        
        # rendering
        self.images = dict(
            peace=pygame.image.load(data.filepath('image','mouse_peace.png')).convert(),
            chase=pygame.image.load(data.filepath('image','mouse_chase.png')).convert(),
            range=pygame.image.load(data.filepath('image','mouse_range.png')).convert(),
            melee=pygame.image.load(data.filepath('image','mouse_melee.png')).convert(),
        )
        for image in self.images.values():
            image.set_colorkey((0,0,0))
            image.set_alpha(255-96)
        self.image = self.images['peace']
        self.draw_rect = self.image.get_rect()
#        x,y,w,h = self.draw_rect
#        cx,cy = self.draw_rect.center
#        pygame.draw.circle(self.image, (1,1,1), (cx,cy), cx)
#        pygame.draw.circle(self.image, self.color, (cx,cy), cx, 1)
#        pygame.draw.line(self.image, self.color, (cx,y), (cx,h))
#        pygame.draw.line(self.image, self.color, (x,cy), (w,cy))
#        pygame.image.save(self.image, 'crosshairs.png')
#        self.image.set_colorkey((0,0,0))
#        self.image.set_alpha(255-96)
        
        self._position = Vec2d(0,0)
    
    @property
    def position(self):
        return self._position
    @position.setter
    def position(self, val):
        p = self._position
        p[:] = State.camera.screen_to_world(val)
        self.rect.center = round(p[0]),round(p[1])
        self.draw_rect.center = round(val[0]),round(val[1])
    
    def peace(self):
        self.image = self.images['peace']
    
    def chase(self):
        self.image = self.images['chase']
    
    def melee(self):
        self.image = self.images['melee']
    
    def range(self):
        self.image = self.images['range']
    
    def draw(self):
        if self.visible:
            if not settings.systemcursor:
                State.screen.blit(self.image, self.draw_rect)
