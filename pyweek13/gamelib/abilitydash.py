import pygame
from pygame.locals import *

from gummworld2 import State, View
from gummworld2 import data

import settings
import ability


class AbilityGauge(object):
    
    grades_image = None
    
    def __init__(self, ability_obj):
        self.ability = ability_obj
        self.name = ability_obj.proper_name
        self.desc = ability.DESCS[ability_obj.name]
        
        num_gauges = len(ability.GAUGE_ORDER)
        full_width = settings.game_layout['dashboard'][2]
        full_height = settings.game_layout['dashboard'][3]
        width = full_width / num_gauges
        left = ability.GAUGE_ORDER.index(ability_obj.name) * width
        
        # stretch the last gauge horizontally if there will be a blank area
        if ability_obj.name == ability.GAUGE_ORDER[-1]:
            if width * num_gauges < full_width:
                width += full_width - width * num_gauges
        
        # create the gauge
        self.rect = Rect(0,0,width,full_height)
        c = ability.COLORS.get(ability_obj.name, (0,0,255))
        self.color = Color(*ability.COLORS.get(ability_obj.name, (0,0,255)))
        
        if not self.grades_image:
            gradesw,gradesh = self.rect.w, 15*11
            self.grades_image = pygame.Surface((gradesw,gradesh))
            fg_color = Color('white')
            bg_color = Color('black')
            font_file = data.filepath('font','Vera.ttf')
            font = pygame.font.Font(font_file, 14)
            for i in range(11):
                txt_img = font.render(str(i), True, fg_color, bg_color)
                self.text_height = txt_img.get_height()
                r = txt_img.get_rect(top=i*gradesh*1/11)
                r.centerx = self.rect.centerx
                pygame.draw.line(
                    self.grades_image, fg_color, (0,r.centery), (gradesw,r.centery))
                self.grades_image.blit(txt_img, r)
        
        self.frame_image = pygame.Surface(self.rect.size)
        self.frame_image.fill(self.color)
        r = self.rect.inflate(-self.rect.w*1/4, 0)
        cy = r.h*3/4
        r.h = self.text_height + 2
        r.centery = cy
        self.gauge_align = cy
        self.frame_image.fill(Color('black'), r)
        self.frame_image.set_colorkey(Color('black'))
        fg_color = Color(1,1,1)  # avoid colorkey
        bg_color = Color('white')
        font_file = data.filepath('font','VeraBd.ttf')
        font = pygame.font.Font(font_file, 9)
#        txt_img = font.render(self.ability.proper_name, True, bg_color)
#        r = txt_img.get_rect(centerx=self.rect.centerx,y=self.rect.h*2/11)
#        for dx in (-1,0,1):
#            for dy in (-1,0,1):
#                self.frame_image.blit(txt_img, r.move(dx,dy))
        words = self.ability.proper_name.split()
        for i,word in enumerate(words):
            txt_img = font.render(word, True, fg_color)
            r = txt_img.get_rect(centerx=self.rect.centerx,y=self.rect.h*i*2/11+5)
            self.frame_image.blit(txt_img, r)
        
        self.rect.left = left


class AbilityDash(object):
    
    def __init__(self):
        self.view = View(State.screen.surface, Rect(settings.game_layout['dashboard']))
        self.gauges = []
    
    def add(self, ability_gauge):
        self.gauges.append(ability_gauge)
#        for i,g in enumerate(self.gauges):
#            x = i*g.rect.w
#            y = 0
#            g.rect.topleft = x,y
    
    def draw(self, *args):
        blit = self.view.blit
        for i,g in enumerate(self.gauges):
#            x = i*g.rect.w
            cy = g.rect.centery
            r = g.grades_image.get_rect()
#            r.x = x
            r.centerx = g.rect.centerx
            r.y = g.gauge_align - g.ability.value*r.h/11 - g.text_height/2
            blit(g.grades_image, r)
            blit(g.frame_image, g.rect)
