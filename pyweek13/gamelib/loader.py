import os, glob

import gummworld2
from gummworld2 import data

import settings, game


class Loader(gummworld2.Engine):
    """just a sequential loader
    
    Loads map from the list and pushes the new game. When Game pops, the next one
    is loaded and pushed, and so on.
    """
    
    def __init__(self):
        gummworld2.Engine.__init__(self, set_state=False)
        self.level = 0
        self.map_files = settings.maps
    
    def enter(self):
        gummworld2.Engine.enter(self)
        self.game = game.Game()
    
    def update(self, dt):
        if self.level < len(self.map_files):
            map_file = self.map_files[self.level]
            self.game.load_level(map_file)
            self.push(self.game)
            self.level += 1
        else:
            self.pop()
