import pygame

from gummworld2 import Vec2d
from gummworld2 import data


class Sign(object):
    
    def __init__(self, map_obj):
        self.name = map_obj.name
        self.position = Vec2d(map_obj.x,map_obj.y)
        self.image = pygame.image.load(data.filepath('image', 'sign.png')).convert()
        self.image.set_colorkey((0,0,0))
        self.msg = map_obj.properties['msg']
        self.rect = self.image.get_rect(topleft=self.position)
