import glob
import os

import pygame

from gummworld2 import data


class Animation(object):
    
    def __init__(self, images, duration, frames=None, loop=0):
        """a cyclic animation
        
        images is a list of images.
        
        duration is the length of the entire animation in seconds.
        
        frames is an optional list of indices to images. If None, indices will
        be calculated one frame per image in ascending order. So if len(images)
        is 3, frames will be calculated as [0,1,2]. If frames is supplied it
        can be any length, in any order, and frames can be skipped or repeated
        for a variety of effects.
        
        loop is the number of times to loop. If loop==0 the anim loops
        infinitely.
        
        While you can change frames and duration on the fly, it will not look
        smooth in most cases. This is due to the way the frame index is
        calculated. It is best to:
            
            1. stop the anim
            2. display frame 0, or whichever you consider the "at rest" image;
               if frame 0 is your rest pose, then anim.start() will cause
               anim.get_image() to serve up that image.
            3. modify frames and/or duration
            4. call anim.start()
            5. resume your normal anim cycle
        """
        self.images = images
        self.duration = duration
        self.loop = loop
        if frames:
            self.frames = frames
        else:
            self.frames = range(len(images))
    
        self.time = lambda:pygame.time.get_ticks() / 1000.
        self.started = None
        self.stopped = True
        self.nloops = 0
        self.prev_frame = 0
        self.imagei = 0
    
    @property
    def frames(self):
        return self._frames
    @frames.setter
    def frames(self, val):
        self._frames = val
        self._nframes = len(val)
    
    @property
    def nframes(self):
        return self._nframes
    
    @property
    def framei(self):
        ## the frame index calculator...
        elapsed = self.time() - self.started
        duration = self.duration
        return int((elapsed % duration) * self.nframes / duration)
    
    def start(self):
        if self.stopped:
            # if __debug__: print '???? ------- anim started'
            self.stopped = False
            self.started = self.time()
            
    def stop(self):
        if not self.stopped:
            # if __debug__: print '???? ------- anim stopped'
            self.stopped = True
    
    def get_image(self):
        if self.stopped:
            # if __debug__:
                # print '??? stopped anim returns:', self.imagei, self.images[self.imagei]
            return self.images[self.imagei]
        if self.started is None:
            self.start()
        framei = self.framei
        if self.loop > 0:
            if framei > self.prev_frame:
                self.prev_frame = framei
            elif framei < self.prev_frame:
                self.prev_frame = 0
                self.nloops += 1
            if self.nloops >= self.loop:
                self.stopped = True
                return self.images[self.imagei]
        self.imagei = self.frames[self.framei]
        return self.images[self.imagei]

_image_cache = {}
def load_images(source_path, ext="*.png", reverse=False):
    source_path = os.path.join(source_path, ext)
    img_files = glob.glob(source_path)
    img_files.sort()
    global _image_cache
    if reverse:
        img_files.reverse()
    images = []
    for img_file in img_files:
        # images.append(pygame.image.load(img_file).convert())
        if img_file not in _image_cache:
            _image_cache[img_file] = pygame.image.load(img_file)
            if __debug__:
                print 'loaded:', img_file
        images.append(_image_cache[img_file])
    return images
    
def load_images_range(source_path, start, end, ext="*.png", reverse=False):
    source_path = os.path.join(source_path, ext)
    img_files = glob.glob(source_path)
    img_files.sort()
    global _image_cache
    if reverse:
        img_files.reverse()
    images = []
    in_range = False
    for img_file in img_files:
        if not in_range and start in img_file:
            in_range = True
        # images.append(pygame.image.load(img_file).convert())
        if in_range:
            if img_file not in _image_cache:
                _image_cache[img_file] = pygame.image.load(img_file)
                if __debug__:
                    print 'loaded:', img_file
            images.append(_image_cache[img_file])
            # if img_file in _image_cache:
                # images.append(_image_cache[img_file])
            # else:
                # images.append(pygame.image.load(img_file))
                # if __debug__:
                    # print 'loaded:', img_file
        if in_range and end in img_file:
            in_range = False
    return images



def load_strip(file_name, width, height, colorkey=None):
    images = []
    if file_name in _strip_cache:
        image = _strip_cache[file_name]
    else:
        image = pygame.image.load(file_name)
        _strip_cache[file_name] = image
    r = pygame.Rect(0,0,width,height)
    for x in xrange(0,image.get_width(),r.w):
        r.x = x
        img = image.subsurface(r).copy()
        if colorkey: img.set_colorkey(colorkey)
        images.append(img)
    return images
_strip_cache = {}


fire_animation = Animation(load_images("data/image/fire"), 2)
fire_animation.start()
ice_animation = Animation(load_images("data/image/ice"), 2)
ice_animation.start()

critter_E = lambda:Animation(load_images_range("data/image/critter", "0001", "0020"), 1)
critter_NE = lambda:Animation(load_images_range("data/image/critter", "0021", "0040"), 1)
critter_N = lambda:Animation(load_images_range("data/image/critter", "0042", "0061"), 1)
critter_NW = lambda:Animation(load_images_range("data/image/critter", "0063", "0083"), 1)
critter_W = lambda:Animation(load_images_range("data/image/critter", "0084", "0104"), 1)
critter_SW = lambda:Animation(load_images_range("data/image/critter", "0105", "0125"), 1)
critter_S = lambda:Animation(load_images_range("data/image/critter", "0126", "0146"), 1)
critter_SE = lambda:Animation(load_images_range("data/image/critter", "0147", "0168"), 1)

player_SW = lambda:Animation(load_images_range("data/image/avatar", "0001", "0021"), 1)
player_W = lambda:Animation(load_images_range("data/image/avatar", "0022", "0042"), 1)
player_NW = lambda:Animation(load_images_range("data/image/avatar", "0043", "0063"), 1)
player_N = lambda:Animation(load_images_range("data/image/avatar", "0064", "0084"), 1)
player_NE = lambda:Animation(load_images_range("data/image/avatar", "0085", "0105"), 1)
player_E = lambda:Animation(load_images_range("data/image/avatar", "0106", "0126"), 1)
player_SE = lambda:Animation(load_images_range("data/image/avatar", "0127", "0147"), 1)
player_S = lambda:Animation(load_images_range("data/image/avatar", "0148", "0168"), 1)


start_point = lambda:Animation(load_images("data/image/startfield"), 1)
end_point = lambda:Animation(load_images("data/image/startfield", reverse=True), 1)



def hit_animation():
    anim = Animation(
        load_strip('data/image/hit.png', 48, 48, colorkey=(0,0,0)),
        1.5, loop=1)
    anim.start()
    return anim


if __name__ == '__main__':
    from pygame.locals import *
    pygame.init()
    screen = pygame.display.set_mode((400,400))
    screen_rect = screen.get_rect()
    clock = pygame.time.Clock()
    font = pygame.font.SysFont(None, 16)
    fill_color = Color('black')
    images = []
    n = 20
    for i in range(n):
        s = pygame.Surface((n*3,n*3))
        sr = s.get_rect()
        sr.width = i*3
        s.fill(Color('white'), sr)
        images.append(s)
    anim = Animation(images, 1)
    anim.start()  # reset the sequence so we are sure to start at frame 0
    while 1:
        dt = clock.tick(10) / 1000.
        e = None
        for e in pygame.event.get():
            if e.type == KEYDOWN:
                if e.key == K_ESCAPE: quit()
                elif e.key == K_SPACE:
                    ## test anim.start
                    anim.start()
                elif e.key == K_BACKSPACE:
                    ## test changing anim frames in real time
                    anim.frames = sorted(anim.frames, reverse=True)
                elif e.key == K_RETURN:
                    frames = []
                    for i in range(n):
                        frames.extend([i]*int(i/5+1))
                    anim.frames = frames
                elif e.key == K_UP:
                    ## faster
                    if anim.duration > .1:
                        anim.duration -= .1
                elif e.key == K_DOWN:
                    ## slower
                    anim.duration += .1
        if e:
            pygame.display.set_caption('{0:0.1f} duration {1:d} frames {2:d} images'.format(
                anim.duration, len(anim.frames), len(anim.images),
            ))
        image = anim.get_image()
        screen.fill(fill_color)
        screen.blit(image, (0,0))
        pygame.display.flip()
