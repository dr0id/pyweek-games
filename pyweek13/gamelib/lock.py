import operator
import re

import settings


_ops = {
    '==':operator.eq,
    '!=':operator.ne,
    '<':operator.lt,
    '>':operator.gt,
    '<=':operator.le,
    '>=':operator.ge,
}


class Lockable(object):
    
    def __init__(self, map_obj=None, lock_value='None'):
        """create a lock from a Tiled map object or a lock value string
        
        If map_obj is None, or 'lock' is not found in map_obj.properties the
        lock value is taken from lock_value.
        
        Lock value string format is:
            "[not] ability_name cmp num [(and|or) ...]"
        
        not is an optional negator. It negates the following expression.
        
        The value of ability_name is looked up in the abilities attribute of the
        object seeking to pass (can_pass(obj) -> value=obj.abilities[name]).
        
        cmp is one of the relational operators: == != < > <= >= . There is no
        operator precedence, and grouping with parentheses is not supported.
        Expressions are evaluated as they are encountered. This also means
        care must be taken in statements because "bool1 or bool2 and bool3" is
        not equivalent to "bool1 or (bool2 and bool3)". (Parens are not allowed.
        This example just emphasizes what "no operator precedence" means.)
        
        num is either an int- or a float-compatible value.
        
        and | or logically join multiple expressions. Short circuit evalutation
        is not performed.
        
        Example valid lock values:
            run >= 6
            attack < 5
            reach_high >= 8 or jump >= 8
        """
        if map_obj:
            self.lock_value = map_obj.properties.get('lock', lock_value)
            self.name = map_obj.name
        else:
            self.lock_value = lock_value
            self.name = 'Generic Lockable'
        self.lock_tokens = self.lock_value.split()
        if __debug__: print self
    
    def can_pass(self, obj):
        if self.spam: print 'LOCK: needs',self.lock_value
        can = [True, 'and'] ## the evaluation stack; default is True
        op = None
        neg = False
        vals = []
        for tok in self.lock_tokens:
            ## pick group of "ability cmp num" expressions. Push their values
            ## onto the stack, as well as the "and"|"or" conjunction.
            if tok == '*':
                if self.spam: print 'LOCK: * -> False'
                return False
            elif re.match(tok, r'none', re.I):
                if self.spam: print 'LOCK: None -> True'
                return True
            elif tok in ('==','!=','<','>','<=','>='):
                if self.spam: print 'LOCK: op:',tok
                op = tok
            elif tok == 'not':
                if self.spam: print 'LOCK: not'
                neg = True
            elif tok in ('and','or'):
                ## Push conjunction on the stack.
                if self.spam: print 'LOCK: conjunction:',tok
                can.append(tok)
            elif tok in obj.abilities:
                if self.spam: print 'LOCK: ability:',tok
                vals.append(obj.abilities[tok].value)
            else:
                if tok.isdigit():
                    if self.spam: print 'LOCK: int:', tok
                    vals.append(int(tok))
                elif re.match(tok, r'[\d\.]+'):
                    if self.spam: print 'LOCK: float:', tok
                    vals.append(float(tok))
                else:
                    if self.spam: print 'LOCK: UNEXPECTED:', tok
                    raise ValueError,'unexpected token {0}'.format(repr(tok))
            ## Evaluate an "ability cmp num" expression. Push the result onto
            ## the stack.
            if len(vals) == 2:
                if self.spam: print 'LOCK: cmp:', op, vals
                left,right = vals
                bool = _ops[op](left, right)
                if neg:
                    if self.spam: print 'LOCK: negating', bool, '->', not bool
                    bool = not bool
                can.append(bool)
                # reset for next expression
                op = None
                neg = False
                del vals[:]
        ## Empty the stack, reducing the conjunctions to a single return value.
        ## At minimum, can = [True, 'and']. This alone will return a True value.
        ## If there were more expressions in the lock value, they will have been
        ## pushed onto the stack, along with any conjunctions.
        if self.spam: print 'LOCK: wrap it up (note: final True is the default)...'
        val = can.pop()
        while can:
            next = can.pop()
            if next in ('and','or'):
                if self.spam: print 'LOCK: op:',next
                op = next
            elif op == 'and':
                if self.spam: print 'LOCK: and:',val,next,'->',val and next
                val = val and next
            else:
                if self.spam: print 'LOCK: or:',val,next,'->',val or next
                val = val or next
        if self.spam: print 'LOCK: return ->',val
        if self.spam: print 'LOCK: -----'
        return val
    
    @property
    def spam(self):
        return settings.spam.get('Lockable', False)
    
    def __str__(self):
        return 'Lockable({obj.lock_value!r},{obj.lock_tokens})'.format(
            obj=self,
        )


if __name__ == '__main__':
    class Struct(dict):
        def __init__(self, **kw):
            self.update(kw)
    lock = Lockable('key == 10')
    obj = Struct(ability=Struct(key=10))
