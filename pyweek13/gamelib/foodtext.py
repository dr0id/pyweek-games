import pygame

from gummworld2 import State, Vec2d
from gummworld2 import data
from gummworld2.geometry import distance, angle_of, point_on_circumference


font = None

class FoodText(object):
    
    def __init__(self, food, ability, value):
        global font
        
        if __debug__: print 'FoodText:',ability,value
        
        if not font:
            font_name = data.filepath('font','VeraBd.ttf')
            font = pygame.font.Font(font_name, 13)
        
        text = '{0} {1:+.0f}'.format(ability.proper_name, value)
        self.image = font.render(text, True, pygame.Color('yellow'))
        self.rect = self.image.get_rect()
        self._position = Vec2d(0,0)
        self.position = State.camera.world_to_screen(food.rect.center)
        
        self.move_to = list(self._find_gauge(ability).rect.center)
        self.move_to[1] += State.game.ability_dash.view.abs_offset[1]
        
        self.speed = 50
        self.accel = lambda s:9.8 / (100000*s*s)
        
        State.game.floating_texts.append(self)
    
    @property
    def position(self):
        return self._position
    @position.setter
    def position(self, val):
        p = self._position
        p.x,p.y = val
        self.rect.center = round(p.x),round(p.y)
    
    def draw(self, dt):
        """step and draw a food text
        
        If the food text has arrived at its destination, the object is returned
        for handling. Else, None is returned.
        """
        if distance(self.position, self.move_to) < 10:
            return self
        angle = angle_of(self.position, self.move_to)
        speed = self.speed * dt
        self.speed += self.accel(dt)
        self.position = point_on_circumference(self.position, speed, angle)
        State.screen.blit(self.image, self.rect)
    
    def _find_gauge(self, ability):
        for g in State.game.ability_dash.gauges:
            if g.ability is ability:
                return g
