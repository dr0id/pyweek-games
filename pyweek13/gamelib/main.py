import cProfile, pstats

import gummworld2

import intro, mainmenu, settings


def start_context():
    # run() is imported into gummworld2.__init__ from Engine
#    gummworld2.run(intro.Intro())
    gummworld2.run(mainmenu.MainMenu())


def main():
    global start_context
    if settings.profile:
        cProfile.run('start_context()', 'prof.dat')
        p = pstats.Stats('prof.dat')
        p.sort_stats('time').print_stats()
    else:
        start_context()
