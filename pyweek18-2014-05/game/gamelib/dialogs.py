# -*- coding: utf-8 -*-
# 
# New BSD license
#  
# Copyright (c) DR0ID
# This file is part of pyweek18-2014-05
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import pygame

from gamesprites import ClickTextSprite, ClickSprite

import pyknicpygame
import pyknicpygame.pyknic.events
import pyknicpygame.pyknic.context
import pyknicpygame.pyknic.timing
from pyknicpygame.pyknic.mathematics import Point2
import resources
import settings
import tweening


__version_number__ = (0, 0, 0, 0)
__version__ = ".".join([str(num) for num in __version_number__])

__author__ = 'dr0iddr0id {at} gmail [dot] com'
__copyright__ = "DR0ID @ 2014"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

import sound



class DialogBase(pyknicpygame.pyknic.context.Context):
    def __init__(self):
        self.tween = tweening.Tweener()
        self.renderer = pyknicpygame.spritesystem.DefaultRenderer()
        sw, sh = settings.SCREEN_SIZE
        self.cam = pyknicpygame.spritesystem.Camera(pygame.Rect(0, 0, sw, sh), position=Point2(sw // 2, sh // 2))
        self.scheduler = pyknicpygame.pyknic.timing.Scheduler()
        self.sprite = None

    def think(self, delta_time):
        self._handle_events()

    def draw(self, screen):
        self.renderer.draw(screen, self.cam, fill_color=(0, 0, 0, 128), do_flip=True, interpolation_factor=1.0)

    def _handle_events(self):
        unhandled = []
        for event in pygame.event.get():
            # if event.type == pygame.KEYDOWN:
            #     if event.key == pygame.K_ESCAPE:
            #         pyknicpygame.pyknic.context.pop()
            # elif event.type == pygame.QUIT:
            #     pyknicpygame.pyknic.context.pop()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    sprites = self.renderer.get_sprites_in_rect(pygame.Rect(event.pos, (1, 1)))
                    if sprites:
                        self.sprite = sprites[0]
            elif event.type == pygame.MOUSEBUTTONUP:
                if event.button == 1:
                    sprites = self.renderer.get_sprites_in_rect(pygame.Rect(event.pos, (1, 1)))
                    if sprites:
                        spr = sprites[0]
                        if self.sprite is not None and spr == self.sprite:
                            self.sprite.on_click()
            else:
                unhandled.append(event)
        return unhandled


class MenuContext(DialogBase):
    def _handle_events(self):
        unhandled = DialogBase._handle_events(self)
        for event in unhandled:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pyknicpygame.pyknic.context.pop()
                elif event.key == pygame.K_F8:
                    self._cheat_index += 1
                    self._cheat_index %= len(context.level_order)
                    print("cheat:", self._cheat_index)
                elif event.key == pygame.K_F9:
                    self._push_level(self._cheat_index)
            if event.type == pygame.QUIT:
                pyknicpygame.pyknic.context.pop()

    def enter(self):
        resources.load_resources()
        self._cheat_index = 0
        distance = settings.SCREEN_HEIGHT // 5
        middle = settings.SCREEN_WIDTH // 2
        play = ClickTextSprite("Play", Point2(middle, 1 * distance))
        play.clicked_event += self._on_play
        # resume = ClickTextSprite("Resume", Point2(middle, 2 * distance))
        # resume.clicked_event += self._on_resume
        instructions = ClickTextSprite("Instructions", Point2(middle, 2 * distance))
        instructions.clicked_event += self._on_instructions
        credits = ClickTextSprite("Credits", Point2(middle, 3 * distance))
        credits.clicked_event += self._on_credits
        exit = ClickTextSprite("Exit", Point2(middle, 4 * distance))
        exit.clicked_event += self._on_exit
        sound.songs['infados'].play()

        self.renderer.add_sprites([play, instructions, credits, exit])

    def _on_exit(self, sender):
        sound.Song.fadeout()
        while sound.Song.get_busy():
            pygame.time.wait(100)
        pyknicpygame.pyknic.context.pop()

    def _on_play(self, sender):
        self._push_level(0)

    def _on_resume(self, sender):
        pass

    def _on_instructions(self, sender):
        pyknicpygame.pyknic.context.push(InstructionsDialog())

    def _on_credits(self, sender):
        pyknicpygame.pyknic.context.push(CreditsDialog())

    def _push_level(self, index):
        pyknicpygame.pyknic.context.push(context.level_order[index])


class PauseMenu(DialogBase):
    def _handle_events(self):
        unhandled = DialogBase._handle_events(self)
        for event in unhandled:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self._on_play(None)
            if event.type == pygame.QUIT:
                self._on_exit(None)

    def enter(self):
        distance = settings.SCREEN_HEIGHT // 6
        middle = settings.SCREEN_WIDTH // 2
        play = ClickTextSprite("Resume play", Point2(middle, 1 * distance))
        play.clicked_event += self._on_play
        exit = ClickTextSprite("Return to main menu", Point2(middle, 2 * distance))
        exit.clicked_event += self._on_exit

        self.renderer.add_sprites([play, exit])

    def _on_play(self, sender):
        pyknicpygame.pyknic.context.pop()

    def _on_exit(self, sender):
        pyknicpygame.pyknic.context.pop(2)


class StoryDialogBase(DialogBase):

    def __init__(self, index):
        super(StoryDialogBase, self).__init__()
        self.level_index = index

    def enter(self):
        next = ClickTextSprite("Next", Point2(settings.SCREEN_WIDTH - 60, settings.SCREEN_HEIGHT - 60))
        next.clicked_event += self._on_next_clicked

        self.renderer.add_sprites([next])
        
        if not sound.Song.get_busy():
            sound.songs['infados'].play()

    def _on_next_clicked(self, sender):
        if self.board_num == 5:
            sound.Song.fadeout(1000)
            while sound.Song.get_busy():
                pygame.time.wait(100)
            sound.songs['tafi'].play()
        
        pyknicpygame.pyknic.context.pop()
        index = self.level_index + 1
        if index < len(context.level_order):
            next_level = context.level_order[index]
            pyknicpygame.pyknic.context.push(next_level)
        #else:
        #    raise NotImplementedError("should not happen, means there is no context after story!")


class StoryDialog(StoryDialogBase):
    
    def __init__(self, index, board_num):
        super(StoryDialog, self).__init__(index)
        self.board_num = board_num
        self.file_name = 'data/chapter{0}.png'.format(board_num)
        img = pygame.image.load(self.file_name)
        sprite = ClickSprite(img, Point2(256+128, 256+64))
        self.renderer.add_sprite(sprite)


class InstructionsDialog(DialogBase):
    
    def __init__(self):
        super(InstructionsDialog, self).__init__()
        self.file_name = 'data/instructions.png'
        img = pygame.image.load(self.file_name)
        sprite = ClickSprite(img, Point2(256+128, 256+64))
        self.renderer.add_sprite(sprite)

    def enter(self):
        next = ClickTextSprite("Next", Point2(settings.SCREEN_WIDTH - 60, settings.SCREEN_HEIGHT - 60))
        next.clicked_event += lambda spr: pyknicpygame.pyknic.context.pop()

        self.renderer.add_sprites([next])


    
    def _handle_events(self):
        unhandled = DialogBase._handle_events(self)
        for event in unhandled:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pyknicpygame.pyknic.context.pop()
            if event.type == pygame.QUIT:
                pyknicpygame.pyknic.context.pop()


class CreditsDialog(DialogBase):
    
    def __init__(self):
        super(CreditsDialog, self).__init__()
        self.file_name = 'data/credits.png'
        img = pygame.image.load(self.file_name)
        sprite = ClickSprite(img, Point2(256+128, 256+64))
        self.renderer.add_sprite(sprite)

    def enter(self):
        next = ClickTextSprite("Next", Point2(settings.SCREEN_WIDTH - 60, settings.SCREEN_HEIGHT - 60))
        next.clicked_event += lambda spr: pyknicpygame.pyknic.context.pop()

        self.renderer.add_sprites([next])
    
    def _handle_events(self):
        unhandled = DialogBase._handle_events(self)
        for event in unhandled:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pyknicpygame.pyknic.context.pop()
            if event.type == pygame.QUIT:
                pyknicpygame.pyknic.context.pop()



# import has to be here because of cyclic import (I know, it is bad)
import context
