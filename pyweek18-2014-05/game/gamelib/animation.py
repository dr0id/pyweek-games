# -*- coding: utf-8 -*-
# 
# New BSD license
#  
# Copyright (c) DR0ID
# This file is part of pyweek18-2014-05
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function
import pyknicpygame
import pyknicpygame.pyknic.events
import pyknicpygame.spritesystem


__version_number__ = (0, 0, 0, 0)
__version__ = ".".join([str(num) for num in __version_number__])

__author__ = 'dr0iddr0id {at} gmail [dot] com'
__copyright__ = "DR0ID @ 2014"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"


class Animation(pyknicpygame.spritesystem.Sprite):

    def __init__(self, position, images, scheduler, interval, instant_play=True):
        super(Animation, self).__init__(images[0], position)
        self._images = images
        self._index = 0
        self._anim_length = len(images)
        self._schedule_id = None
        self.scheduler = scheduler
        self.interval = interval
        self.finished_event = pyknicpygame.pyknic.events.Signal()
        if instant_play:
            self.start()

    def start(self):
        if self._schedule_id is None:
            self._schedule_id = self.scheduler.schedule(self._on_image_change, self.interval)

    def stop(self):
        self.scheduler.remove(self._schedule_id)
        self._schedule_id = None

    def reset(self):
        self._index = 0
        self.set_image(self._images[0])

    # def _on_image_change(self):
    #     self._index += 1
    #     if self._index >= len(self.images):
    #         self.finished_event.fire(self)
    #         return 0  # stop scheduling
    #     # self._index %= len(self.images)
    #     self.set_image(self.images[self._index])
    #     return self.interval

# def loop(anim):
#     anim.reset()
#     anim.start()

    def _on_image_change(self):
        self._index += 1
        if self._index >= self._anim_length:
            self.finished_event.fire(self)
        self._index %= self._anim_length
        self.set_image(self._images[self._index])
        return self.interval

def stop(anim):
    anim.stop()

__all__ = [Animation]  # list of public visible parts of this module
