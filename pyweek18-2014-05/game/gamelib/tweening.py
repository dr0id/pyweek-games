# -*- coding: utf-8 -*-

"""
TODO: docstring

equations based on:

http://wiki.python-ogre.org/index.php?title=CodeSnippits_pyTweener
http://hosted.zeh.com.br/tweener/docs/en-us/misc/transitions.html
http://code.google.com/p/tweener/source/browse/trunk/as3/caurina/transitions/Equations.as

tweens based on:

http://www.robertpenner.com/easing/penner_chapter7_tweening.pdf

"""

import math


def LINEAR(t, b, c, d, p):
    return c * t / d + b


def IN_QUAD(t, b, c, d, p):
    t /= d
    return c * (t) * t + b


def OUT_QUAD(t, b, c, d, p):
    t /= d
    return -c * (t) * (t - 2) + b


def IN_OUT_QUAD(t, b, c, d, p):
    t /= d / 2.0
    if ((t) < 1):
        return c / 2.0 * t * t + b
    t -= 1
    return -c / 2.0 * ((t) * (t - 2) - 1) + b


def OUT_IN_QUAD(t, b, c, d, p):
    if (t < d / 2):
        return OUT_QUAD(t * 2, b, c / 2, d, p)
    return IN_QUAD((t * 2) - d, b + c / 2, c / 2, d, p)


def IN_CUBIC(t, b, c, d, p):
    t /= d
    return c * (t) * t * t + b


def OUT_CUBIC(t, b, c, d, p):
    t = t / d - 1
    return c * ((t) * t * t + 1) + b


def IN_OUT_CUBIC(t, b, c, d, p):
    t /= d / 2
    if ((t) < 1):
        return c / 2 * t * t * t + b
    t -= 2
    return c / 2 * ((t) * t * t + 2) + b


def OUT_IN_CUBIC(t, b, c, d, p):
    if t < d / 2:
        return OUT_CUBIC(t * 2, b, c / 2, d, p)
    return IN_CUBIC((t * 2) - d, b + c / 2, c / 2, d, p)


def IN_QUART(t, b, c, d, p):
    t /= d
    return c * (t) * t * t * t + b


def OUT_QUART(t, b, c, d, p):
    t = t / d - 1
    return -c * ((t) * t * t * t - 1) + b


def IN_OUT_QUART(t, b, c, d, p):
    t /= d / 2
    if (t < 1):
        return c / 2 * t * t * t * t + b
    t -= 2
    return -c / 2 * ((t) * t * t * t - 2) + b


def OUT_IN_QUART(t, b, c, d, p):
    raise NotImplementedError()


def IN_QUINT(t, b, c, d, p):
    raise NotImplementedError()


def OUT_QUINT(t, b, c, d, p):
    raise NotImplementedError()


def IN_OUT_QUINT(t, b, c, d, p):
    raise NotImplementedError()


def OUT_IN_QUINT(t, b, c, d, p):
    raise NotImplementedError()


def IN_SINE(t, b, c, d, p):
    raise NotImplementedError()


def OUT_SINE(t, b, c, d, p):
    raise NotImplementedError()


def IN_OUT_SINE(t, b, c, d, p):
    raise NotImplementedError()


def OUT_IN_SINE(t, b, c, d, p):
    raise NotImplementedError()


def IN_CIRC(t, b, c, d, p):
    raise NotImplementedError()


def OUT_CIRC(t, b, c, d, p):
    raise NotImplementedError()


def IN_OUT_CIRC(t, b, c, d, p):
    raise NotImplementedError()


def OUT_IN_CIRC(t, b, c, d, p):
    raise NotImplementedError()


def IN_EXPO(t, b, c, d, p):
    # return (t==0) ? b : c * Math.pow(2, 10 * (t/d - 1)) + b - c * 0.001;
    return b if t == 0 else c * (2 ** (10 * (t / d - 1))) + b - c * 0.001


def OUT_EXPO(t, b, c, d, p):
    return b + c if (t == d) else c * (-2 ** (-10 * t / d) + 1) + b


def IN_OUT_EXPO(t, b, c, d, p):
    raise NotImplementedError()


def OUT_IN_EXPO(t, b, c, d, p):
    raise NotImplementedError()


def IN_ELASTIC(t, b, c, d, p):
    raise NotImplementedError()


def OUT_ELASTIC(t, b, c, d, p):  # Not working :(
    if (t == 0):
        return b
    t /= d
    if t == 1:
        return b + c
    p = period = d * 0.3
    a = amplitude = 1.0
    if a < abs(c):
        a = c
        s = p / 4
    else:
        s = p / (2 * math.pi) * math.asin(c / a)

    return (a * math.pow(2, -10 * t) * math.sin((t * d - s) * (2 * math.pi) / p) + c + b)


def IN_OUT_ELASTIC(t, b, c, d, p):
    raise NotImplementedError()


def OUT_IN_ELASTIC(t, b, c, d, p):
    raise NotImplementedError()


def IN_BACK(t, b, c, d, p):
    raise NotImplementedError()


def OUT_BACK(t, b, c, d, p):
    raise NotImplementedError()


def IN_OUT_BACK(t, b, c, d, p):
    raise NotImplementedError()


def OUT_IN_BACK(t, b, c, d, p):
    raise NotImplementedError()


def IN_BOUNCE(t, b, c, d, p):
    raise NotImplementedError()


def OUT_BOUNCE(t, b, c, d, p):
    raise NotImplementedError()


def IN_OUT_BOUNCE(t, b, c, d, p):
    raise NotImplementedError()


def OUT_IN_BOUNCE(t, b, c, d, p):
    raise NotImplementedError()


ease_functions = [
    LINEAR,
                  IN_QUAD,
                  OUT_QUAD,
                  IN_OUT_QUAD,
                  OUT_IN_QUAD,
                  IN_CUBIC,
                  OUT_CUBIC,
                  IN_OUT_CUBIC,
                  OUT_IN_CUBIC,
                  IN_QUART,
                  OUT_QUART,
                  IN_OUT_QUART,
                  OUT_ELASTIC,
                  OUT_EXPO,
]


class _Tween(object):
    def __init__(self, ctrl, obj, attr_name, begin, change, duration, tween_function, params, cb_end, *cb_args):
        self.t = 0
        self.ctrl = ctrl  # the controlling Tweener instance
        self.o = obj
        self.a = attr_name
        self.f = tween_function
        self.cb = cb_end  # TODO: use a signal here to support multiple listeners
        self.cb_args = cb_args
        # tween vars
        self.b = begin
        self.c = change
        self.d = duration
        self.p = params


class Tweener(object):
    def __init__(self):
        self.active_tweens = []  # ? maybe a dict? {entity: [tween]}

    def update(self, delta_time):
        ended_tweens = []
        for tween in self.active_tweens:
            tween.t += delta_time
            setattr(tween.o, tween.a, tween.f(tween.t, tween.b, tween.c, tween.d, tween.p))
            if tween.t > tween.d:
                ended_tweens.append(tween)
        for ended in ended_tweens:
            self.active_tweens.remove(ended)
            if ended.cb is not None:
                # TODO: this cb call seems strange with that many args
                ended.cb(self, ended.o, ended.a, ended.b, ended.c, ended.d, ended.f, ended.p, ended.cb, ended.cb_args)

    def create_tween(self, obj, attr_name, begin, change, duration, tween_function=LINEAR, params=None, cb_end=None,
                     cb_args=[]):
        tween = _Tween(self, obj, attr_name, begin, change, duration, tween_function, params, cb_end, cb_args)
        self.active_tweens.append(tween)

    def remove_tween(self, object, attr_name):
        to_remove = []
        for tween in self.active_tweens:
            if tween.o == object and tween.a == attr_name:
                to_remove.append(tween)
        for tween in to_remove:
            self.active_tweens.remove(tween)


if __name__ == '__main__':

    def restart(tweener, *args):
        args[0].left = 10
        tweener.create_tween(*args)

    import pygame

    pygame.init()

    screen = pygame.display.set_mode((800, 600))

    tweener = Tweener()

    rects = []
    x = 10
    d = 10
    w = h = 3
    for i in range(len(ease_functions)):
        rect = pygame.Rect(x, i * d + d, w, h)
        rects.append(rect)
        tweener.create_tween(rect, 'left', x, 100, 20, ease_functions[i], cb_end=restart)

    clock = pygame.time.Clock()
    running = True
    while running:
        clock.tick(10)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False

        for rect in rects:
            pygame.draw.rect(screen, (255, 255, 255), rect, 0)

        tweener.update(1)
        pygame.display.flip()
        screen.fill((0, 0, 0))
