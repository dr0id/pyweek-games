# -*- coding: utf-8 -*-
# 
# New BSD license
#  
# Copyright (c) DR0ID
# This file is part of pyweek18-2014-05
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function
import animation
from nodes import BitState
import settings
import sound


__version_number__ = (0, 0, 0, 0)
__version__ = ".".join([str(num) for num in __version_number__])

__author__ = 'dr0iddr0id {at} gmail [dot] com'
__copyright__ = "DR0ID @ 2014"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

import pyknicpygame
import pyknicpygame.spritesystem
import pyknicpygame.pyknic.events


class ClickSpriteMixin(object):
    def __init__(self):
        self.clicked_event = pyknicpygame.pyknic.events.Signal()

    def on_click(self):
        self.clicked_event.fire(self)

class ClickSprite(pyknicpygame.spritesystem.Sprite, ClickSpriteMixin):
    def __init__(self, image, position, anchor=None, z_layer=None, parallax_factors=None, do_init=True):
        pyknicpygame.spritesystem.Sprite.__init__(self, image, position, anchor, z_layer, parallax_factors, do_init)
        ClickSpriteMixin.__init__(self)


class ClickTextSprite(pyknicpygame.spritesystem.TextSprite, ClickSpriteMixin):
    def __init__(self, text, position, font=None, antialias=None, color=None, backcolor=None, \
                 anchor=None, z_layer=1000, parallax_factors=None):
        pyknicpygame.spritesystem.TextSprite.__init__(self, text, position, font, antialias, color, backcolor, anchor,
                                                      z_layer, parallax_factors)
        ClickSpriteMixin.__init__(self)

class NodeSprite(pyknicpygame.spritesystem.Sprite):
    def __init__(self, node, renderer, image, position, anchor=None, z_layer=None, parallax_factors=None, do_init=True, rotation=0):
        super(NodeSprite, self).__init__(image, position, anchor, z_layer, parallax_factors, do_init)
        self._renderer = renderer
        self._visible = False
        self.changed_visibility_event = pyknicpygame.pyknic.events.Signal()
        self.visible = True
        self.rotation = rotation
        self.node = node
        node.position = self.position

    @property
    def visible(self):
        return self._visible

    @visible.setter
    def visible(self, value):
        if self._visible != value:
            if value is True:
                self._renderer.add_sprite(self)
            elif value is False:
                self._renderer.remove_sprite(self)
            self._visible = value
            self.changed_visibility_event.fire(self)

    # def on_toggle_visibility(self, other):
    #     self.visible = not other.visible

    def on_click(self):
        pass

    # def on_bit_state_changed(self, node):
    #     pass

class PowerSprite(NodeSprite):

    def __init__(self, node, renderer, image_power, image_powerless, position, anchor=None,
                 z_layer=None,
                 parallax_factors=None,
                 do_init=True):
        super(PowerSprite, self).__init__(node, renderer, image_powerless, position, anchor, z_layer, parallax_factors, do_init)
        self.image_power = image_power
        self.image_powerless = image_powerless

    def on_bit_state_changed(self, node):
        if node.is_powered:
            self.set_image(self.image_power)
        else:
            self.set_image(self.image_powerless)

class WirePowerSprite(NodeSprite):
    def __init__(self, node, side, bit_pos, renderer, image_power, image_powerless, position, anchor=None,
                 z_layer=None,
                 parallax_factors=None,
                 do_init=True):
        super(WirePowerSprite, self).__init__(node, renderer, image_powerless, position, anchor, z_layer, parallax_factors, do_init)
        self.image_power = image_power
        self.image_powerless = image_powerless
        self.bit_pos = bit_pos


    def on_bit_changed(self, node):
        # return
        current = node.get_state_of_bit(self.bit_pos, 0)
        if current == BitState.On:
            self.set_image(self.image_power)
        elif current == BitState.Off or current == BitState.PowerLess:
            self.set_image(self.image_powerless)
        else:
            raise Exception("unknown state")

class BitSprite(NodeSprite):
    def __init__(self, node, side, bit_pos, renderer, image_on, image_off, image_powerless, position, anchor=None,
                 z_layer=None,
                 parallax_factors=None,
                 do_init=True):
        super(BitSprite, self).__init__(node, renderer, image_powerless, position, anchor, z_layer, parallax_factors, do_init)
        self.side = side
        self.bit_pos = bit_pos
        self.image_on = image_on
        self.image_off = image_off
        self.image_powerless = image_powerless

    def _get_bit_state(self, node):
        return node.get_state_of_bit(self.bit_pos, self.side)

    def on_bit_state_changed(self, node):
        current = self._get_bit_state(node)
        if current == BitState.On:
            self.set_image(self.image_on)
        elif current == BitState.Off:
            self.set_image(self.image_off)
        elif current == BitState.PowerLess:
            self.set_image(self.image_powerless)
        else:
            raise Exception("unknown state")

class ExpectedBitSprite(BitSprite):
    def _get_bit_state(self, node):
        return node.get_expected_state_of_bit(self.bit_pos, self.side)

class WireSprite(animation.Animation):
    def __init__(self, node, bit_pos, images, renderer, scheduler, img, position, anchor, rotation):
        super(WireSprite, self).__init__(position, images, scheduler, settings.ANIM_POWER_INTERVAL, False)
        self.bit_pos = bit_pos
        self.node = node
        self.renderer = renderer
        self.renderer.add_sprite(self)
        self.anchor = anchor
        self.rotation = rotation
        self.wire_img = img
        self.set_image(self.wire_img)

    def on_bit_changed(self, node):
        # return
        current = node.get_state_of_bit(self.bit_pos, 0)
        if current == BitState.On:
            self.start()
        elif current == BitState.Off or current == BitState.PowerLess:
            self.stop()
            self.set_image(self.wire_img)
        else:
            raise Exception("unknown state")

class SwitchSprite(NodeSprite):
    def __init__(self, node, side, bit_pos, renderer, image_on, image_off, position, anchor=None, z_layer=None,
                 parallax_factors=None,
                 do_init=True):
        super(SwitchSprite, self).__init__(node, renderer, image_on, position, anchor, z_layer, parallax_factors, do_init)
        self.side = side
        self.bit_pos = bit_pos
        self.image_on = image_on
        self.image_off = image_off

    def on_click(self):
        sound.sfx['click'].play()
        self.node.flip_bit(self.bit_pos)

    def on_bit_state_changed(self, node):
        current = node.get_state_of_bit(self.bit_pos, self.side, False)  # don't check power for switches
        if current == BitState.Off:
            self.set_image(self.image_off)
        elif current == BitState.On:
            self.set_image(self.image_on)
        elif current == BitState.PowerLess:
            pass
        else:
            raise Exception("unknown state")