import glob
import os

import pygame

import settings


if __name__ == '__main__':
    os.chdir('..')
    os.environ['SDL_VIDEODRIVER'] = 'dummy'
    pygame.init()
    pygame.mixer.pre_init(frequency=0, size=-16, channels=2, buffer=128)
    pygame.mixer.init()
    pygame.display.set_mode((1, 1))


class Song(object):
    
    def __init__(self, name, file_name, volume=1.0):
        self.name = name
        self.file_name = file_name
        self.volume = volume
    
    def play(self):
        pygame.mixer.music.load(self.file_name)
        self.set_volume(self.volume)
        pygame.mixer.music.play()
    
    @staticmethod
    def fadeout(time=1000):
        pygame.mixer.music.fadeout(time)
    
    @staticmethod
    def stop():
        pygame.mixer.music.stop()
    
    @staticmethod
    def pause():
        pygame.mixer.music.pause()
    
    @staticmethod
    def unpause():
        pygame.mixer.music.unpause()
    
    @staticmethod
    def stop():
        pygame.mixer.music.stop()
    
    @staticmethod
    def get_volume():
        return pygame.mixer.music.get_volume()
    
    @staticmethod
    def set_volume(value):
        pygame.mixer.music.set_volume(value)
    
    @staticmethod
    def get_busy():
        return pygame.mixer.music.get_busy()


sfx = {}
songs = {}

def init():
    """call me *after* pygame is initialized"""
    
    ## SFX
    for name, file_name, volume in (
        ('click', 'data/switch-10.ogg', 1.0),
        ('hacked', 'data/31882__HardPCM__Chip045.ogg', 0.2),
        ('level up', 'data/down.ogg', 1.0),
        ('s1', 'data/31882__HardPCM__Chip045.ogg', 1.0),
        ('s2', 'data/46485__PhreaKsAccount__comms3.ogg', 1.0),
        ('s3', 'data/58919__mattwasser__coin_up.ogg', 1.0),
        ('s4', 'data/A-6734-Mach_New-7460_hifi.ogg', 1.0),
        ('s5', 'data/Bigbeat_-Mach_New-7662_hifi.ogg', 1.0),
        ('s6', 'data/Bumble_B-Mach_New-7657_hifi.ogg', 1.0),
        ('s7', 'data/Domp_Ble-Mach_New-7664_hifi.ogg', 1.0),
        ('s8', 'data/Kommunis-Mach_New-7484_hifi.ogg', 1.0),
        ('s9', 'data/laser_26667.ogg', 1.0),
        ('s10', 'data/power_up_16893.ogg', 1.0),
        ('s11', 'data/ReBirth_-Mach_New-7665_hifi.ogg', 1.0)):
        assert os.access(file_name, os.F_OK)
        sfx[name] = pygame.mixer.Sound(file_name)
        sfx[name].set_volume(volume)
    
    ## Music
    for name, file_name, volume in (
        ('confused', 'data/Confused State.ogg',       1.0),
        ('infados',  'data/Infados.ogg',              1.0),
        ('tafi',     'data/Tafi Maradi no voice.ogg', 1.0),
        ('dreamy',   'data/Dreamy Flashback.ogg',     1.0),
    ):
        assert os.access(file_name, os.F_OK)
        songs[name] = Song(name, file_name, volume)


if __name__ == '__main__':
    init()
    case = ''
    if case == 'songs':
        def play_song(s):
            # play the song for 10 seconds...
            songs[s].play()
            n = 0
            while Song.get_busy() and n < 10:
                pygame.time.wait(1000)
                n += 1
            # then fade out
            Song.fadeout()
            pygame.time.wait(1000)
        for s in sorted(songs):
            play_song(s)
    else:
        def play_sfx(s):
            chan = sfx[s].play()
            while chan.get_busy():
                pygame.time.wait(1000)
        play_sfx('hacked')
        quit()
        for s in sorted(sfx):
            play_sfx(s)
