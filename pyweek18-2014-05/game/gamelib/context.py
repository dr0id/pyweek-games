# -*- coding: utf-8 -*-
# 
# New BSD license
#  
# Copyright (c) DR0ID
# This file is part of pyweek18-2014-05
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function
import gamesprites
import nodes
import resources
from resources import Resources
import tweening


__version_number__ = (0, 0, 0, 0)
__version__ = ".".join([str(num) for num in __version_number__])

__author__ = 'dr0iddr0id {at} gmail [dot] com'
__copyright__ = "DR0ID @ 2014"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 

import math

import pygame

import pyknicpygame.pyknic.context
import pyknicpygame.spritesystem
import pyknicpygame.pyknic.timing
from pyknicpygame.pyknic.mathematics import Point2, Vec2

import settings
import dialogs
import sound


class ContextBase(pyknicpygame.pyknic.context.Context):
    def __init__(self):
        self.tween = tweening.Tweener()
        self.renderer = pyknicpygame.spritesystem.DefaultRenderer()
        sw, sh = settings.SCREEN_SIZE
        self.cam = pyknicpygame.spritesystem.Camera(pygame.Rect(0, 0, sw, sh), position=Point2(sw // 2, sh // 2))
        self.scheduler = pyknicpygame.pyknic.timing.Scheduler()
        self.sprite = None


class LevelUnlocked(ContextBase):
    def __init__(self, next):
        super(LevelUnlocked, self).__init__()
        self.next = next

    def enter(self):
        self.scheduler.schedule(self._on_done, settings.SHOW_LOCKED_STATE_DURATION)
        sw, sh = settings.SCREEN_SIZE
        pos = self.cam.screen_to_world(Point2(sw // 2, sh // 2))
        text = pyknicpygame.spritesystem.TextSprite(settings.TEXT_LEVEL_UNLOCK, pos)
        self.tween.create_tween(text, 'zoom', 1.0, 2.0, settings.SHOW_LOCKED_STATE_DURATION)
        self.renderer.add_sprite(text)

    def _on_done(self):
        pyknicpygame.pyknic.context.pop(2, False)
        if self.next is not None:
            pyknicpygame.pyknic.context.push(self.next)
        return 0

    def think(self, delta_time):
        self.tween.update(delta_time)
        self.scheduler.update(delta_time)

    def draw(self, screen):
        sub = pyknicpygame.pyknic.context.top(1)
        sub.draw(screen, False)
        self.renderer.draw(screen, self.cam)
        pygame.display.flip()


class LevelBase(pyknicpygame.pyknic.context.Context):
    def __init__(self, level_index):
        self.level_index = level_index
        self.tween = tweening.Tweener()
        self.sprite = None
        self.renderer = pyknicpygame.spritesystem.DefaultRenderer()
        self.cams = []
        sw, sh = settings.SCREEN_SIZE
        cam = pyknicpygame.spritesystem.Camera(pygame.Rect(0, 0, sw, sh), position=Point2(0, 0))
        self.cams.append(cam)
        self.scheduler = pyknicpygame.pyknic.timing.Scheduler()
        self.cam_vel = Vec2(0, 0)
        self.tree = None

    def exit(self):
        self.renderer.clear()

    def _show_locked_state(self, node):
        print(node.is_locked)
        position = node.position
        spr = pyknicpygame.spritesystem.TextSprite("locked" if node.is_locked else "unlocked", position, antialias=5)
        # self, text, position, font=None, antialias=None, color=None, backcolor=None, anchor=None, z_layer=1000, parallax_factors=None):
        self.tween.create_tween(spr, 'zoom', 1.0, 1.5, settings.SHOW_LOCKED_STATE_DURATION,
                                cb_end=lambda *args: self.renderer.remove_sprite(args[-1][0]), cb_args=spr)
        self.renderer.add_sprite(spr)
        if not node.is_locked:
            sound.sfx['hacked'].play()


    def _create_switch_sprites(self, sprite, side, surf_on, surf_off, offset=-30):
        node = sprite.node
        sprites = []
        # distance = settings.TILE_WIDTH // 9
        # start = - settings.TILE_WIDTH // 2 + distance
        distance = surf_on.get_size()[0]
        start = -4 * distance + distance // 2
        for i in range(8):
            pos = Point2(0, 0)
            pos.copy_values(sprite.position)
            spr = gamesprites.SwitchSprite(node, side, i, self.renderer, surf_on, surf_off, pos,
                                           Vec2(start + i * distance, offset), 1)
            node.bit_state_changed_event += spr.on_bit_state_changed
            spr.rotation = sprite.rotation
            sprites.append(spr)
        # initialize images according to nodes bit state
        node.bit_state_changed_event.fire(node)
        return sprites

    def _create_bit_sprites(self, sprite, side, surf_on, surf_off, surf_powerless, offset=30):
        node = sprite.node
        sprites = []
        # distance = settings.TILE_WIDTH * 3 // 4 // 9
        # start = - settings.TILE_WIDTH * 3 // 8 + distance
        distance = surf_on.get_size()[0]
        start = -4 * distance + distance // 2
        for i in range(8):
            pos = Point2(0, 0)
            pos.copy_values(sprite.position)
            spr = gamesprites.BitSprite(node, side, i, self.renderer, surf_on, surf_off, surf_powerless, pos,
                                        Vec2(start + i * distance, offset), 1)
            node.bit_state_changed_event += spr.on_bit_state_changed

            spr.rotation = sprite.rotation
            sprites.append(spr)
        # initialize images according to nodes bit state
        node.bit_state_changed_event.fire(node)
        return sprites

    def _create_expected_bit_sprites(self, sprite, side, surf_on, surf_off, surf_powerless, offset=30):
        node = sprite.node
        sprites = []
        # distance = settings.TILE_WIDTH * 3 // 4 // 9
        # start = - settings.TILE_WIDTH * 3 // 8 + distance
        distance = surf_on.get_size()[0]
        start = -4 * distance + distance // 2
        for i in range(8):
            pos = Point2(0, 0)
            pos.copy_values(sprite.position)
            spr = gamesprites.ExpectedBitSprite(node, side, i, self.renderer, surf_on, surf_off, surf_powerless, pos,
                                                Vec2(start + i * distance, offset), 1)
            node.bit_state_changed_event += spr.on_bit_state_changed

            spr.rotation = sprite.rotation
            sprites.append(spr)
        # initialize images according to nodes bit state
        node.bit_state_changed_event.fire(node)
        return sprites

    def _create_wire(self, node, img_wire, position, rotation):
        sprites = []
        distance = img_wire.get_size()[0]
        start = -4 * distance + distance // 2 - 1
        for i in range(4):
            pos = Point2(0, 0)
            pos.copy_values(position)
            images = [Resources.img_wire_powered1, Resources.img_wire_powered2, Resources.img_wire_powered3,
                      Resources.img_wire_powered4]

            spr = gamesprites.WireSprite(node, i, images, self.renderer, self.scheduler, img_wire, pos,
                                         Vec2(start + i * distance, 0), rotation)
            # node.bit_state_changed_event += spr.on_bit_changed
            spr.rotation = rotation
            self.renderer.update_z_layer(spr, -5)
            sprites.append(spr)
            pwr_spr = self._create_wire_power_led(spr)
            sprites.append(pwr_spr)

            spr = gamesprites.WireSprite(node, i + 4, images, self.renderer, self.scheduler, img_wire, pos,
                                         Vec2(start + (i + 4) * distance + 1, 0), rotation)
            # node.bit_state_changed_event += spr.on_bit_changed
            spr.rotation = rotation
            self.renderer.update_z_layer(spr, -5)
            sprites.append(spr)
            pwr_spr = self._create_wire_power_led(spr)
            sprites.append(pwr_spr)


        # initialize images according to nodes bit state
        node.bit_state_changed_event.fire(node)
        return sprites

    def _create_wire_power_led(self, wire_spr):
        spr = gamesprites.WirePowerSprite(wire_spr.node, 0, wire_spr.bit_pos, self.renderer, Resources.img_led_blue,
                                          Resources.img_led_powerless, wire_spr.position, wire_spr.anchor, 5)
        spr.rotation = wire_spr.rotation
        wire_spr.node.bit_state_changed_event += spr.on_bit_changed
        return spr


    def _create_power_led(self, sprite):
        node = sprite.node
        pos = Point2()
        pos.copy_values(sprite.position)
        spr = gamesprites.PowerSprite(node, self.renderer, Resources.img_led_blue, Resources.img_led_powerless, pos,
                                      sprite.anchor, z_layer=2)
        spr.rotation = sprite.rotation
        node.bit_state_changed_event += spr.on_bit_state_changed
        node.bit_state_changed_event.fire(node)
        return spr

    def _create_name(self, spr):
        self.renderer.add_sprite(pyknicpygame.spritesystem.Sprite(Resources.names[spr.node.op_str], spr.position, z_layer=2))


    def _handle_events(self):
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pyknicpygame.pyknic.context.push(dialogs.PauseMenu())
            elif event.type == pygame.QUIT:
                pyknicpygame.pyknic.context.push(dialogs.PauseMenu())
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    sprites = self.renderer.get_sprites_in_rect(pygame.Rect(event.pos, (1, 1)))
                    if sprites:
                        self.sprite = sprites[0]
                elif event.button == 2:
                    cam = self.cams[0]
                    world_mouse = cam.screen_to_world(Point2(event.pos[0], event.pos[1]))
                    p = Point2(0, 0)
                    p.copy_values(cam.position)
                    self.tween.create_tween(cam, 'position', p, world_mouse - cam.position, settings.PAN_DURATION,
                                            tweening.OUT_EXPO)
            elif event.type == pygame.MOUSEBUTTONUP:
                if event.button == 1:
                    sprites = self.renderer.get_sprites_in_rect(pygame.Rect(event.pos, (1, 1)))
                    if sprites:
                        spr = sprites[0]
                        if spr == self.sprite and self.sprite is not None and hasattr(self.sprite, "on_click"):
                            self.sprite.on_click()
                self.sprite = None
            elif event.type == pygame.MOUSEMOTION:
                if event.buttons[0] == 1:
                    relative_x, relative_y = event.rel
                    if math.hypot(relative_x, relative_y) < 3:
                        return
                    cam = self.cams[0]
                    cam.position = Point2(cam.position.x - relative_x, cam.position.y - relative_y)
                self.sprite = None

    def think(self, delta_time):
        self._handle_events()
        self.tween.update(delta_time)
        self.scheduler.update(delta_time)
        self._check_end_condition()

    def draw(self, screen, do_flip=True):
        screen.fill((0, 0, 0, 0))
        for cam in self.cams:
            self.renderer.draw(screen, cam, fill_color=None, do_flip=False, interpolation_factor=1.0)
        if do_flip:
            pygame.display.flip()

    def _check_end_condition(self):
        if not self.tree.root.is_locked:
            # level is done
            index = self.level_index + 1
            sound.sfx['level up'].play()
            if index < len(level_order):
                next = level_order[index]
                pyknicpygame.pyknic.context.push(LevelUnlocked(next))
            else:
                pyknicpygame.pyknic.context.push(LevelUnlocked(None))


class Level01(LevelBase):
    def enter(self):
        # logic
        tree = nodes.Tree()
        in_node = nodes.InputNode(0xae, tree)
        wire1 = nodes.Wire(in_node)
        generator = nodes.Generator(int('0b11110110', 2))
        wire2 = nodes.Wire(generator)
        and_node = nodes.AndNode(wire1, wire2)
        wire = nodes.Wire(and_node)
        lock = nodes.LockNode(wire, 0xf0)
        tree.root = lock

        lock.locked_changed_event += self._show_locked_state

        # visualization
        self.cams[0].position = Point2(0, 2 * settings.TILE_WIDTH)

        for x in range(-1, 4):
            for y in range(-1, 6):
                self.renderer.add_sprite(pyknicpygame.spritesystem.Sprite(Resources.img_background,
                                                                          Point2(x * settings.TILE_WIDTH,
                                                                                 y * settings.TILE_HEIGHT),
                                                                          z_layer=-100))

        ## generator
        generator_spr = gamesprites.NodeSprite(generator, self.renderer, Resources.img_chip, Point2(0, 0), rotation=0)
        offset = settings.TILE_HEIGHT // 4
        self._create_power_led(generator_spr)

        ## wire
        self._create_wire(wire2, Resources.img_wire, Point2(0, settings.TILE_HEIGHT), 180)

        ## chip
        and_spr = gamesprites.NodeSprite(and_node, self.renderer, Resources.img_chip,
                                         Point2(0, 2 * settings.TILE_HEIGHT), rotation=0)
        self._create_name(and_spr)
        self._create_power_led(and_spr)

        ## wire
        self._create_wire(wire1, Resources.img_wire, Point2(0, 3 * settings.TILE_HEIGHT), 0)

        ## input
        in_spr = gamesprites.NodeSprite(in_node, self.renderer, Resources.img_switch_body,
                                        Point2(0, 4 * settings.TILE_HEIGHT), rotation=0)
        self._create_switch_sprites(in_spr, 0, Resources.img_switch_on, Resources.img_switch_off, offset - 10)

        ## wire
        self._create_wire(wire, Resources.img_wire, Point2(settings.TILE_WIDTH, 2 * settings.TILE_HEIGHT), -90)

        ## lock
        lock_spr = gamesprites.NodeSprite(lock, self.renderer, Resources.img_lock,
                                          Point2(2 * settings.TILE_WIDTH, 2 * settings.TILE_HEIGHT), rotation=-90)
        self._create_expected_bit_sprites(lock_spr, 0, Resources.img_led_green, Resources.img_led_red,
                                          Resources.img_led_powerless,
                                          -offset)
        self._create_power_led(lock_spr)


        # init

        tree.root.calculate()
        self.tree = tree


class Level02(LevelBase):
    def enter(self):
        # logic
        tree1 = nodes.Tree()
        in_node = nodes.InputNode(0xae, tree1)
        wire1 = nodes.Wire(in_node)
        generator = nodes.Generator(0xf0)
        wire2 = nodes.Wire(generator)
        and_node = nodes.AndNode(wire1, wire2)
        wire = nodes.Wire(and_node)
        lock = nodes.LockNode(wire, 0xf0)
        tree1.root = lock

        lock.locked_changed_event += self._show_locked_state
        lock.locked_changed_event += self._on_power

        # visualization

        for x in range(-1, 8):
            for y in range(-1, 6):
                self.renderer.add_sprite(pyknicpygame.spritesystem.Sprite(Resources.img_background,
                                                                          Point2(x * settings.TILE_WIDTH,
                                                                                 y * settings.TILE_HEIGHT),
                                                                          z_layer=-100))

        self.cams[0].position = Point2(3 * settings.TILE_WIDTH, 2 * settings.TILE_HEIGHT)

        generator_spr = gamesprites.NodeSprite(generator, self.renderer, Resources.img_chip, Point2(0, 0), rotation=0)
        gen_power_spr = self._create_power_led(generator_spr)
        offset = settings.TILE_HEIGHT // 4
        # self._create_bit_sprites(generator_spr, 0, Resources.img_led_green, Resources.img_led_red,
        #                          Resources.img_led_powerless, -offset)

        # wire2_spr = gamesprites.NodeSprite(wire2, self.renderer, Resources.img_wire, Point2(0, settings.TILE_HEIGHT),
        #                                    rotation=0)
        self._create_wire(wire2, Resources.img_wire, Point2(0, settings.TILE_HEIGHT), 0)
        and_spr = gamesprites.NodeSprite(and_node, self.renderer, Resources.img_chip,
                                         Point2(0, 2 * settings.TILE_HEIGHT), rotation=0)
        self._create_power_led(and_spr)
        self._create_name(and_spr)
        # wire1_spr = gamesprites.NodeSprite(wire1, self.renderer, Resources.img_wire,
        #                                    Point2(0, 3 * settings.TILE_HEIGHT), rotation=0)
        self._create_wire(wire1, Resources.img_wire, Point2(0, 3 * settings.TILE_HEIGHT), 0)


        in_spr = gamesprites.NodeSprite(in_node, self.renderer, Resources.img_switch_body,
                                        Point2(0, 4 * settings.TILE_HEIGHT), rotation=0)
        self._create_switch_sprites(in_spr, 0, Resources.img_switch_on, Resources.img_switch_off, offset - 10)
        # self._create_bit_sprites(in_spr, 0, Resources.img_led_green, Resources.img_led_red, Resources.img_led_powerless,
        #                          offset)

        # wire_spr = gamesprites.NodeSprite(wire, self.renderer, Resources.img_wire,
        #                                   Point2(settings.TILE_WIDTH, 2 * settings.TILE_HEIGHT), rotation=90)
        self._create_wire(wire, Resources.img_wire, Point2(settings.TILE_WIDTH, 2 * settings.TILE_HEIGHT), 90)


        lock_spr = gamesprites.NodeSprite(lock, self.renderer, Resources.img_lock,
                                          Point2(2 * settings.TILE_WIDTH, 2 * settings.TILE_HEIGHT), rotation=-90)
        # self._create_bit_sprites(lock_spr, 0, Resources.img_led_green, Resources.img_led_red,
        #                          Resources.img_led_powerless,
        #                          -offset)
        self._create_expected_bit_sprites(lock_spr, 0, Resources.img_led_green, Resources.img_led_red,
                                 Resources.img_led_powerless,
                                 -offset)



        # logic
        tree = nodes.Tree()
        in_node = nodes.InputNode(0xae, tree)
        wire1 = nodes.Wire(in_node)
        generator = nodes.Generator(0xf0)
        wire2 = nodes.Wire(generator)
        or_node = nodes.OrNode(wire1, wire2)
        wire = nodes.Wire(or_node)
        lock = nodes.LockNode(wire, 0xf0)
        tree.root = lock

        lock.locked_changed_event += self._show_locked_state
        tree.set_power_on_all_nodes(False)

        # visualization

        generator_spr = gamesprites.NodeSprite(generator, self.renderer, Resources.img_chip,
                                               Point2(4 * settings.TILE_WIDTH, 0), rotation=0)
        offset = settings.TILE_HEIGHT // 4
        # self._create_bit_sprites(generator_spr, 0, Resources.img_led_green, Resources.img_led_red,
        #                          Resources.img_led_powerless, -offset)
        self._create_power_led(generator_spr)

        # wire2_spr = gamesprites.NodeSprite(wire2, self.renderer, Resources.img_wire,
        #                                    Point2(4 * settings.TILE_WIDTH, settings.TILE_HEIGHT),
        #                                    rotation=0)
        self._create_wire(wire2, Resources.img_wire, Point2(4 * settings.TILE_WIDTH, settings.TILE_HEIGHT), 0)
        and_spr = gamesprites.NodeSprite(or_node, self.renderer, Resources.img_chip,
                                         Point2(4 * settings.TILE_WIDTH, 2 * settings.TILE_HEIGHT), rotation=0)
        self._create_power_led(and_spr)
        self._create_name(and_spr)
        # wire1_spr = gamesprites.NodeSprite(wire1, self.renderer, Resources.img_wire,
        #                                    Point2(4 * settings.TILE_WIDTH, 3 * settings.TILE_HEIGHT), rotation=0)
        self._create_wire(wire1, Resources.img_wire, Point2(4 * settings.TILE_WIDTH, 3 * settings.TILE_HEIGHT), 0)

        in_spr = gamesprites.NodeSprite(in_node, self.renderer, Resources.img_switch_body,
                                        Point2(4 * settings.TILE_WIDTH, 4 * settings.TILE_HEIGHT), rotation=0)
        self._create_switch_sprites(in_spr, 0, Resources.img_switch_on, Resources.img_switch_off,
                                    offset - 10)
        # self._create_bit_sprites(in_spr, 0, Resources.img_led_green, Resources.img_led_red, Resources.img_led_powerless,
        #                          offset)

        # wire_spr = gamesprites.NodeSprite(wire, self.renderer, Resources.img_wire,
        #                                   Point2(5 * settings.TILE_WIDTH, 2 * settings.TILE_HEIGHT), rotation=90)
        self._create_wire(wire, Resources.img_wire, Point2(5 * settings.TILE_WIDTH, 2 * settings.TILE_HEIGHT), -90)

        lock_spr = gamesprites.NodeSprite(lock, self.renderer, Resources.img_lock,
                                          Point2(6 * settings.TILE_WIDTH, 2 * settings.TILE_HEIGHT), rotation=-90)
        # self._create_bit_sprites(lock_spr, 0, Resources.img_led_green, Resources.img_led_red,
        #                          Resources.img_led_powerless,
        #                          -offset)
        self._create_expected_bit_sprites(lock_spr, 0, Resources.img_led_green, Resources.img_led_red, Resources.img_led_powerless, -offset)
        self._create_power_led(lock_spr)

        # wire_spr = gamesprites.NodeSprite(wire, self.renderer, Resources.img_wire,
        #                                   Point2(3 * settings.TILE_WIDTH, 2 * settings.TILE_HEIGHT), rotation=90)
        self._create_wire(wire, Resources.img_wire, Point2(3 * settings.TILE_WIDTH, 2 * settings.TILE_HEIGHT), 90)

        # init

        tree1.root.calculate()
        tree.root.calculate()
        self.tree = tree

    def _on_power(self, node):
        self.tree.set_power_on_all_nodes(not node.is_locked)

class Level03(LevelBase):
    def enter(self):
        # logic
        tree = nodes.Tree()

        generator = nodes.Generator(int('0b10011001', 2))
        wire6 = nodes.Wire(generator)
        input1 = nodes.InputNode(0xd5, tree)
        wire5 = nodes.Wire(input1)
        and_node = nodes.AndNode(wire5, wire6)
        wire4 = nodes.Wire(and_node)
        input2 = nodes.InputNode(0x5d, tree)
        wire3 = nodes.Wire(input2)
        xor_node = nodes.XorNode(wire3, wire4)
        wire2 = nodes.Wire(xor_node)
        not_node = nodes.Not(wire2)
        wire1 = nodes.Wire(not_node)
        lock = nodes.LockNode(wire1, int('0b10110110', 2))

        tree.root = lock

        lock.locked_changed_event += self._show_locked_state
        tree.set_power_on_all_nodes(True)

        # visualization
        tw, th = settings.TILE_SIZE

        generator_spr = gamesprites.NodeSprite(generator, self.renderer, Resources.img_chip, Point2(0, 0), rotation=0)
        self._create_power_led(generator_spr)

        wire6_spr = self._create_wire(wire6, Resources.img_wire, Point2(0, th), 180)

        input1_spr = gamesprites.NodeSprite(input1, self.renderer, Resources.img_switch_body, Point2(0, 4 * th), rotation=0)
        self._create_switch_sprites(input1_spr, 0, Resources.img_switch_on, Resources.img_switch_off, th // 4 - 10)

        input2_spr = gamesprites.NodeSprite(input2, self.renderer, Resources.img_switch_body, Point2(2 * tw, 0 * th), rotation=180)
        self._create_switch_sprites(input2_spr, 0, Resources.img_switch_on, Resources.img_switch_off, th // 4 - 10)

        wire5_spr = self._create_wire(wire5, Resources.img_wire, Point2(0, 3 * th), 0)

        and_spr = gamesprites.NodeSprite(and_node, self.renderer, Resources.img_chip, Point2(0, 2 * th), rotation=0)
        self._create_name(and_spr)
        self._create_power_led(and_spr)

        xor_spr = gamesprites.NodeSprite(xor_node, self.renderer, Resources.img_chip, Point2(2 * tw, 2 * th), rotation=0)
        self._create_name(xor_spr)
        self._create_power_led(xor_spr)


        not_spr = gamesprites.NodeSprite(not_node, self.renderer, Resources.img_chip, Point2(2 * tw, 4 * th), rotation=0)
        self._create_name(not_spr)
        self._create_power_led(not_spr)

        lock_spr = gamesprites.NodeSprite(lock, self.renderer, Resources.img_chip, Point2(4 * tw, 4 * th), rotation=-90)
        self._create_expected_bit_sprites(lock_spr, 0, Resources.img_led_green, Resources.img_led_red, Resources.img_led_powerless, -th // 4)
        self._create_power_led(lock_spr)

        wire4_spr = self._create_wire(wire4, Resources.img_wire, Point2(1 * tw, 2 * th), 90)
        wire3_spr = self._create_wire(wire3, Resources.img_wire, Point2(2 * tw, 1 * th), 180)
        wire2_spr = self._create_wire(wire2, Resources.img_wire, Point2(2 * tw, 3 * th), 180)
        wire1_spr = self._create_wire(wire1, Resources.img_wire, Point2(3 * tw, 4 * th), -90)

        for x in range(-1, 8):
            for y in range(-1, 8):
                self.renderer.add_sprite(pyknicpygame.spritesystem.Sprite(Resources.img_background,
                                                                          Point2(x * settings.TILE_WIDTH,
                                                                                 y * settings.TILE_HEIGHT),
                                                                          z_layer=-100))

        self.cams[0].position = Point2(0, 2 * settings.TILE_WIDTH)




        tree.root.calculate()
        self.tree = tree

class Level04(LevelBase):
    def enter(self):
        # logic




        tree = nodes.Tree()

        generator = nodes.Generator(int('0b10011001', 2))
        wire6 = nodes.Wire(generator)
        input1 = nodes.InputNode(0xd5, tree)
        wire5 = nodes.Wire(input1)
        and_node = nodes.AndNode(wire5, wire6)
        wire4 = nodes.Wire(and_node)
        input2 = nodes.InputNode(0x5d, tree)
        wire3 = nodes.Wire(input2)
        xor_node = nodes.XorNode(wire3, wire4)
        wire2 = nodes.Wire(xor_node)
        not_node = nodes.Not(wire2)
        wire1 = nodes.Wire(not_node)
        lock = nodes.LockNode(wire1, int('0b10110110', 2))

        tree.root = lock

        lock.locked_changed_event += self._show_locked_state
        tree.set_power_on_all_nodes(True)

        and_node.is_powered = False
        xor_node.is_powered = False
        lock.is_powered = False

        tree1 = nodes.Tree()
        in1 = nodes.InputNode(0x4e, tree1)
        xwire1 = nodes.Wire(in1)
        lock1 = nodes.LockNode(xwire1, 0xb6)
        lock1.locked_changed_event += lambda node: tree.set_power_on_all_nodes(True)
        tree1.root = lock1


        # visualization
        tw, th = settings.TILE_SIZE

        xlock_spr = gamesprites.NodeSprite(lock1, self.renderer, Resources.img_chip, Point2(-2 * tw, 2 * th), rotation=180)
        self._create_expected_bit_sprites(xlock_spr, 0, Resources.img_led_green, Resources.img_led_red, Resources.img_led_powerless, -th // 4)
        self._create_power_led(xlock_spr)
        xwire_spr = self._create_wire(xwire1, Resources.img_wire, Point2(-2 * tw, 1 * th), 180)
        xinput1_spr = gamesprites.NodeSprite(in1, self.renderer, Resources.img_switch_body, Point2(-2 * tw, 0 * th), rotation=180)
        self._create_switch_sprites(xinput1_spr, 0, Resources.img_switch_on, Resources.img_switch_off, th // 4 - 10)
        xwire_spr2 = self._create_wire(xwire1, Resources.img_wire, Point2(-1 * tw, 2 * th), 90)



        generator_spr = gamesprites.NodeSprite(generator, self.renderer, Resources.img_chip, Point2(0, 0), rotation=0)
        self._create_power_led(generator_spr)

        wire6_spr = self._create_wire(wire6, Resources.img_wire, Point2(0, th), 180)

        input1_spr = gamesprites.NodeSprite(input1, self.renderer, Resources.img_switch_body, Point2(0, 4 * th), rotation=0)
        self._create_switch_sprites(input1_spr, 0, Resources.img_switch_on, Resources.img_switch_off, th // 4 - 10)

        input2_spr = gamesprites.NodeSprite(input2, self.renderer, Resources.img_switch_body, Point2(2 * tw, 0 * th), rotation=180)
        self._create_switch_sprites(input2_spr, 0, Resources.img_switch_on, Resources.img_switch_off, th // 4 - 10)

        wire5_spr = self._create_wire(wire5, Resources.img_wire, Point2(0, 3 * th), 0)

        and_spr = gamesprites.NodeSprite(and_node, self.renderer, Resources.img_chip, Point2(0, 2 * th), rotation=0)
        self._create_name(and_spr)
        self._create_power_led(and_spr)

        xor_spr = gamesprites.NodeSprite(xor_node, self.renderer, Resources.img_chip, Point2(2 * tw, 2 * th), rotation=0)
        self._create_name(xor_spr)
        self._create_power_led(xor_spr)


        not_spr = gamesprites.NodeSprite(not_node, self.renderer, Resources.img_chip, Point2(2 * tw, 4 * th), rotation=0)
        self._create_name(not_spr)
        self._create_power_led(not_spr)

        lock_spr = gamesprites.NodeSprite(lock, self.renderer, Resources.img_chip, Point2(4 * tw, 4 * th), rotation=-90)
        self._create_expected_bit_sprites(lock_spr, 0, Resources.img_led_green, Resources.img_led_red, Resources.img_led_powerless, -th // 4)
        self._create_power_led(lock_spr)

        wire4_spr = self._create_wire(wire4, Resources.img_wire, Point2(1 * tw, 2 * th), 90)
        wire3_spr = self._create_wire(wire3, Resources.img_wire, Point2(2 * tw, 1 * th), 180)
        wire2_spr = self._create_wire(wire2, Resources.img_wire, Point2(2 * tw, 3 * th), 180)
        wire1_spr = self._create_wire(wire1, Resources.img_wire, Point2(3 * tw, 4 * th), -90)

        for x in range(-3, 6):
            for y in range(-1, 6):
                self.renderer.add_sprite(pyknicpygame.spritesystem.Sprite(Resources.img_background,
                                                                          Point2(x * settings.TILE_WIDTH,
                                                                                 y * settings.TILE_HEIGHT),
                                                                          z_layer=-100))

        self.cams[0].position = Point2(0, 2 * settings.TILE_WIDTH)




        tree.root.calculate()
        self.tree = tree

class Level05(LevelBase):
    def enter(self):
        # logic
        tree = nodes.Tree()

        generator = nodes.Generator(int('0b10011001', 2))
        wire6 = nodes.Wire(generator)
        input1 = nodes.InputNode(0xd5, tree)
        wire5 = nodes.Wire(input1)
        and_node = nodes.AndNode(wire5, wire6)
        wire4 = nodes.Wire(and_node)
        input2 = nodes.InputNode(0x5d, tree)
        wire3 = nodes.Wire(input2)
        xor_node = nodes.XorNode(wire3, wire4)
        wire2 = nodes.Wire(xor_node)
        not_node = nodes.Not(wire2)
        wire1 = nodes.Wire(not_node)
        lock = nodes.LockNode(wire1, int('0b10110110', 2))

        tree.root = lock

        lock.locked_changed_event += self._show_locked_state
        tree.set_power_on_all_nodes(True)

        # visualization
        tw, th = settings.TILE_SIZE

        generator_spr = gamesprites.NodeSprite(generator, self.renderer, Resources.img_chip, Point2(0, 0), rotation=0)
        self._create_power_led(generator_spr)

        wire6_spr = self._create_wire(wire6, Resources.img_wire, Point2(0, th), 180)

        input1_spr = gamesprites.NodeSprite(input1, self.renderer, Resources.img_switch_body, Point2(0, 4 * th), rotation=0)
        self._create_switch_sprites(input1_spr, 0, Resources.img_switch_on, Resources.img_switch_off, th // 4 - 10)

        input2_spr = gamesprites.NodeSprite(input2, self.renderer, Resources.img_switch_body, Point2(2 * tw, 0 * th), rotation=180)
        self._create_switch_sprites(input2_spr, 0, Resources.img_switch_on, Resources.img_switch_off, th // 4 - 10)

        wire5_spr = self._create_wire(wire5, Resources.img_wire, Point2(0, 3 * th), 0)

        and_spr = gamesprites.NodeSprite(and_node, self.renderer, Resources.img_chip, Point2(0, 2 * th), rotation=0)
        self._create_name(and_spr)
        self._create_power_led(and_spr)

        xor_spr = gamesprites.NodeSprite(xor_node, self.renderer, Resources.img_chip, Point2(2 * tw, 2 * th), rotation=0)
        self._create_name(xor_spr)
        self._create_power_led(xor_spr)


        not_spr = gamesprites.NodeSprite(not_node, self.renderer, Resources.img_chip, Point2(2 * tw, 4 * th), rotation=0)
        self._create_name(not_spr)
        self._create_power_led(not_spr)

        lock_spr = gamesprites.NodeSprite(lock, self.renderer, Resources.img_chip, Point2(4 * tw, 4 * th), rotation=0)
        self._create_expected_bit_sprites(lock_spr, 0, Resources.img_led_green, Resources.img_led_red, Resources.img_led_powerless, -th // 4)
        self._create_power_led(lock_spr)

        wire4_spr = self._create_wire(wire4, Resources.img_wire, Point2(1 * tw, 2 * th), 90)
        wire3_spr = self._create_wire(wire3, Resources.img_wire, Point2(2 * tw, 1 * th), 180)
        wire2_spr = self._create_wire(wire2, Resources.img_wire, Point2(2 * tw, 3 * th), 180)
        wire1_spr = self._create_wire(wire1, Resources.img_wire, Point2(3 * tw, 4 * th), -90)

        for x in range(-1, 8):
            for y in range(-1, 8):
                self.renderer.add_sprite(pyknicpygame.spritesystem.Sprite(Resources.img_background,
                                                                          Point2(x * settings.TILE_WIDTH,
                                                                                 y * settings.TILE_HEIGHT),
                                                                          z_layer=-100))

        self.cams[0].position = Point2(0, 2 * settings.TILE_WIDTH)




        tree.root.calculate()
        self.tree = tree

class Level06(LevelBase):
    def enter(self):
        # logic
        tree = nodes.Tree()

        generator = nodes.Generator(int('0b10011001', 2))
        wire6 = nodes.Wire(generator)
        input1 = nodes.InputNode(0xd5, tree)
        wire5 = nodes.Wire(input1)
        and_node = nodes.AndNode(wire5, wire6)
        wire4 = nodes.Wire(and_node)
        input2 = nodes.InputNode(0x5d, tree)
        wire3 = nodes.Wire(input2)
        xor_node = nodes.XorNode(wire3, wire4)
        wire2 = nodes.Wire(xor_node)
        not_node = nodes.Not(wire2)
        wire1 = nodes.Wire(not_node)
        lock = nodes.LockNode(wire1, int('0b10110110', 2))

        tree.root = lock

        lock.locked_changed_event += self._show_locked_state
        tree.set_power_on_all_nodes(True)

        # visualization
        tw, th = settings.TILE_SIZE

        generator_spr = gamesprites.NodeSprite(generator, self.renderer, Resources.img_chip, Point2(0, 0), rotation=0)
        self._create_power_led(generator_spr)

        wire6_spr = self._create_wire(wire6, Resources.img_wire, Point2(0, th), 180)

        input1_spr = gamesprites.NodeSprite(input1, self.renderer, Resources.img_switch_body, Point2(0, 4 * th), rotation=0)
        self._create_switch_sprites(input1_spr, 0, Resources.img_switch_on, Resources.img_switch_off, th // 4 - 10)

        input2_spr = gamesprites.NodeSprite(input2, self.renderer, Resources.img_switch_body, Point2(2 * tw, 0 * th), rotation=180)
        self._create_switch_sprites(input2_spr, 0, Resources.img_switch_on, Resources.img_switch_off, th // 4 - 10)

        wire5_spr = self._create_wire(wire5, Resources.img_wire, Point2(0, 3 * th), 0)

        and_spr = gamesprites.NodeSprite(and_node, self.renderer, Resources.img_chip, Point2(0, 2 * th), rotation=0)
        self._create_name(and_spr)
        self._create_power_led(and_spr)

        xor_spr = gamesprites.NodeSprite(xor_node, self.renderer, Resources.img_chip, Point2(2 * tw, 2 * th), rotation=0)
        self._create_name(xor_spr)
        self._create_power_led(xor_spr)


        not_spr = gamesprites.NodeSprite(not_node, self.renderer, Resources.img_chip, Point2(2 * tw, 4 * th), rotation=0)
        self._create_name(not_spr)
        self._create_power_led(not_spr)

        lock_spr = gamesprites.NodeSprite(lock, self.renderer, Resources.img_chip, Point2(4 * tw, 4 * th), rotation=0)
        self._create_expected_bit_sprites(lock_spr, 0, Resources.img_led_green, Resources.img_led_red, Resources.img_led_powerless, -th // 4)
        self._create_power_led(lock_spr)

        wire4_spr = self._create_wire(wire4, Resources.img_wire, Point2(1 * tw, 2 * th), 90)
        wire3_spr = self._create_wire(wire3, Resources.img_wire, Point2(2 * tw, 1 * th), 180)
        wire2_spr = self._create_wire(wire2, Resources.img_wire, Point2(2 * tw, 3 * th), 180)
        wire1_spr = self._create_wire(wire1, Resources.img_wire, Point2(3 * tw, 4 * th), -90)

        for x in range(-1, 8):
            for y in range(-1, 8):
                self.renderer.add_sprite(pyknicpygame.spritesystem.Sprite(Resources.img_background,
                                                                          Point2(x * settings.TILE_WIDTH,
                                                                                 y * settings.TILE_HEIGHT),
                                                                          z_layer=-100))

        self.cams[0].position = Point2(0, 2 * settings.TILE_WIDTH)




        tree.root.calculate()
        self.tree = tree

    def exit(self, *args):
        if self.level_index == 11:
            sound.Song.fadeout(1000)
            while sound.Song.get_busy():
                pygame.time.wait(100)
            sound.songs['dreamy'].play()

class Level_Unfinished(LevelBase):
    def enter(self):
        # logic
        tree = nodes.Tree()

        in1 = nodes.InputNode(0xf0, tree)
        in2 = nodes.InputNode(0x0f, tree)

        # wire1 = nodes.Wire()  #
        wire3 = nodes.Wire(in2)
        wire4 = nodes.Wire(in1)
        and_node = nodes.AndNode(wire3, wire4)
        wire2 = nodes.Wire(and_node)
        # wire5 = nodes.Wire()  #
        in3 = nodes.InputNode(0xff, tree)
        in4 = nodes.InputNode(0xff, tree)
        wire7 = nodes.Wire(in3)
        wire8 = nodes.Wire(in4)
        nor_node = nodes.NorNode(wire7, wire8)
        wire6 = nodes.Wire(nor_node)
        # wire9 = nodes.Wire()  #
        # wire10 = nodes.Wire()
        # wire11 = nodes.Wire()
        # wire12 = nodes.Wire()
        # wire13 = nodes.Wire()
        # wire14 = nodes.Wire()  #
        in7 = nodes.InputNode(0xff, tree)
        in6 = nodes.InputNode(0xff, tree)

        wire16 = nodes.Wire(in7)
        wire17 = nodes.Wire(in6)
        xnor_node = nodes.XnorNode(wire16, wire17)
        wire15 = nodes.Wire(xnor_node)

        lock1 = nodes.LockNode(wire2, 0xf0)
        lock2 = nodes.LockNode(wire6, 0xf0)
        gen = nodes.Generator(0xff)
        lock3 = nodes.LockNode(gen, 0xff)
        lock4 = nodes.LockNode(wire15, 0xf0)

        lock5 = nodes.LockNodeSpecial(lock1, lock2, lock3, lock4)
        tree.root = lock5

        lock5.locked_changed_event += self._show_locked_state
        tree.set_power_on_all_nodes(True)

        tw, th = settings.TILE_SIZE

        wire2_spr = self._create_wire(wire2, Resources.img_wire, Point2(1 * tw, 4 * th), 90)
        wire3_spr = self._create_wire(wire3, Resources.img_wire, Point2(0 * tw, 3 * th), 180)
        wire4_spr = self._create_wire(wire4, Resources.img_wire, Point2(0 * tw, 5 * th), 0)
        wire6_spr = self._create_wire(wire6, Resources.img_wire, Point2(4 * tw, 1 * th), 180)
        wire7_spr = self._create_wire(wire7, Resources.img_wire, Point2(3 * tw, 0 * th), 90)
        wire8_spr = self._create_wire(wire8, Resources.img_wire, Point2(5 * tw, 0 * th), -90)
        wire15_spr = self._create_wire(wire15, Resources.img_wire, Point2(4 * tw, 7 * th), 0)
        wire16_spr = self._create_wire(wire16, Resources.img_wire, Point2(3 * tw, 8 * th), 90)
        wire17_spr = self._create_wire(wire17, Resources.img_wire, Point2(5 * tw, 8 * th), -90)



        for x in range(-1, 6):
            for y in range(-1, 8):
                self.renderer.add_sprite(pyknicpygame.spritesystem.Sprite(Resources.img_background,
                                                                          Point2(x * settings.TILE_WIDTH,
                                                                                 y * settings.TILE_HEIGHT),
                                                                          z_layer=-100))

        self.cams[0].position = Point2(0, 2 * settings.TILE_WIDTH)

        tree.root.calculate()
        self.tree = tree


level_order = [
    dialogs.StoryDialog(0, 0),
    Level01(1),
    dialogs.StoryDialog(2, 1),
    Level02(3),
    dialogs.StoryDialog(4, 2),
    Level03(5),
    dialogs.StoryDialog(6, 3),
    Level04(7),
    dialogs.StoryDialog(8, 4),
    Level05(9),
    dialogs.StoryDialog(10, 5),
    Level06(11),
    dialogs.StoryDialog(12, 6),
]

