# -*- coding: utf-8 -*-
# 
# New BSD license
#  
# Copyright (c) DR0ID
# This file is part of pyweek18-2014-05
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function


__version_number__ = (0, 0, 0, 0)
__version__ = ".".join([str(num) for num in __version_number__])

__author__ = 'dr0iddr0id {at} gmail [dot] com'
__copyright__ = "DR0ID @ 2014"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module


import pygame

import settings


class Resources(object):
    img_background = pygame.Surface(settings.TILE_SIZE)
    img_background.fill((68, 144, 65))

    img_chip = pygame.Surface(settings.TILE_SIZE)
    img_chip.fill((255, 0, 255))
    img_chip.fill((100, 100, 100), (
        settings.TILE_WIDTH // 8, settings.TILE_HEIGHT // 8, settings.TILE_WIDTH * 3 // 4,
        settings.TILE_HEIGHT * 3 // 4))

    img_lock = pygame.Surface(settings.TILE_SIZE)
    img_lock.fill((255, 255, 0))
    img_lock.fill((100, 100, 100, 128), (
        settings.TILE_WIDTH // 8, settings.TILE_HEIGHT // 8, settings.TILE_WIDTH * 3 // 4,
        settings.TILE_HEIGHT * 3 // 4),
                  pygame.BLEND_MULT)

    img_switch_body = pygame.Surface(settings.TILE_SIZE)
    img_switch_body.fill((0, 0, 150))

    img_wire = pygame.Surface((settings.TILE_WIDTH * 3 // 4, settings.TILE_HEIGHT))
    img_wire.fill((0, 200, 0))

    led_size = (8, 8)
    led_area = (3, 3, 3, 3)
    img_led_green = pygame.Surface(led_size)
    img_led_green.fill((0, 255, 0), led_area)

    img_led_blue = pygame.Surface(led_size)
    img_led_blue.fill((0, 0, 255), led_area)

    img_led_red = pygame.Surface(led_size)
    img_led_red.fill((255, 0, 0), led_area)

    img_led_powerless = pygame.Surface(led_size)
    img_led_powerless.fill((50, 50, 50), led_area)

    img_switch_on = pygame.Surface((12, 20))
    img_switch_on.fill((255, 255, 255), (1, 0, 10, 10))

    img_switch_off = pygame.Surface((12, 20))
    img_switch_off.fill((255, 255, 255), (1, 10, 10, 10))

    img_wire_powered1 = pygame.Surface((4, 100))
    img_wire_powered2 = pygame.Surface((4, 100))
    img_wire_powered3 = pygame.Surface((4, 100))
    img_wire_powered4 = pygame.Surface((4, 100))

    keys = ['&',
            '|',
            '!',
            '^',
            '!&',
            '!|',
            '!^', ]

    names = {
        '&': pygame.Surface((10, 10)),
        '|': pygame.Surface((10, 10)),
        '!': pygame.Surface((10, 10)),
        '^': pygame.Surface((10, 10)),
        '!&': pygame.Surface((10, 10)),
        '!|': pygame.Surface((10, 10)),
        '!^': pygame.Surface((10, 10)),
    }


def load_resources():
    Resources.img_background = pygame.image.load("data/background.png").convert()
    Resources.img_chip = pygame.image.load("data/chip.png").convert_alpha()
    Resources.img_wire = pygame.image.load("data/wire.png").convert_alpha()
    Resources.img_wire_powered1 = pygame.image.load("data/wirepowered1.png").convert_alpha()
    Resources.img_wire_powered2 = pygame.image.load("data/wirepowered2.png").convert_alpha()
    Resources.img_wire_powered3 = pygame.image.load("data/wirepowered3.png").convert_alpha()
    Resources.img_wire_powered4 = pygame.image.load("data/wirepowered4.png").convert_alpha()
    Resources.img_led_green = pygame.image.load("data/ledgreen.png").convert_alpha()
    Resources.img_led_red = pygame.image.load("data/ledred.png").convert_alpha()
    Resources.img_led_blue = pygame.image.load("data/ledblue.png").convert_alpha()
    Resources.img_led_yellow = pygame.image.load("data/ledyellow.png").convert_alpha()
    Resources.img_led_powerless = pygame.image.load("data/ledpowerless.png").convert_alpha()
    Resources.img_switch_body = pygame.image.load("data/switchbody.png").convert_alpha()
    Resources.img_switch_off = pygame.image.load("data/dipswitchoff.png").convert_alpha()
    Resources.img_switch_on = pygame.image.load("data/dipswitchon.png").convert_alpha()
    Resources.img_lock = pygame.image.load("data/lock.png").convert_alpha()
    img_name = pygame.image.load("data/names.png").convert_alpha()
    h = img_name.get_size()[1]
    for i, name in enumerate(Resources.keys):
        img = pygame.Surface((h, h), pygame.SRCALPHA, img_name)
        img.blit(img_name, (0, 0), (i * h, 0, h, h))
        Resources.names[name] = img

