# -*- coding: utf-8 -*-
# 
# New BSD license
#  
# Copyright (c) DR0ID
# This file is part of pyweek18-2014-05
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
from __future__ import print_function
import pyknicpygame


__version_number__ = (0, 0, 0, 0)
__version__ = ".".join([str(num) for num in __version_number__])

__author__ = 'dr0iddr0id {at} gmail [dot] com'
__copyright__ = "DR0ID @ 2014"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

import unittest
import nodes as mut  # module under test

if mut.__version_number__ != __version_number__:
    raise Exception("version numbers do not match!")


class NodeTests(unittest.TestCase):
    def test_get_un_powered_bit_state(self):
        # arrange
        node = mut.Node()
        node.is_powered = False

        # act
        state = node.get_state_of_bit(0)

        # verify
        self.assertEqual(mut.BitState.PowerLess, state)

    def test_get_powered_bit_state_off(self):
        # arrange
        node = mut.Node()
        node.is_powered = True

        # act
        state = node.get_state_of_bit(0)

        # verify
        self.assertEqual(mut.BitState.Off, state)

    def test_get_powered_bit_state_on(self):
        # arrange
        node = mut.Node()
        node.is_powered = True

        # act
        state = node.get_state_of_bit(0)

        # verify
        self.assertEqual(mut.BitState.Off, state)


class PoweredNodesTest(unittest.TestCase):
    def test_LockedNode_calculation_should_not_work_if_sub_node_is_not_powered(self):
        # arrange
        in_node = mut.InputNode(0xff)
        in_node.is_powered = False
        wire = mut.Wire(in_node)
        wire.is_powered = False
        node = mut.LockNode(wire, 0)
        node.is_powered = True

        # act
        node.calculate()

        # verify
        self.assertEqual(0, node.output)

    def test_LockedNode_calculation_should_update_if_sub_node_is_powered(self):
        # arrange
        in_node = mut.InputNode(0xff)
        in_node.is_powered = True
        wire = mut.Wire(in_node)
        wire.is_powered = False
        node = mut.LockNode(wire, 0)
        node._output = 0xf0  # set the output to a random value, will be override by the calculation if powered normally
        node.is_powered = True

        # act
        node.calculate()

        # verify
        self.assertEqual(0xff, node.output)

    def test_OperatorNode_calculation_should_not_work_if_sub_node_is_not_powered(self):
        # arrange
        in_node = mut.InputNode(0xff)
        in_node.is_powered = False
        leaf = mut.Generator(0xff)
        node = mut.AndNode(in_node, leaf)
        node.is_powered = True

        # act
        node.calculate()

        # verify
        self.assertEqual(0, node.output)

    def test_LeafNode_calculation(self):
        # arrange
        in_node = mut.InputNode(0xf0)
        in_node.is_powered = False
        wire1 = mut.Wire(in_node)
        wire1.is_powered = True
        wire2 = mut.Wire(wire1)
        wire2.is_powered = True
        op_node = mut.AndNode(wire2, mut.Generator(0xff))
        op_node.is_powered = True

        # act
        op_node.calculate()

        # verify
        self.assertFalse(wire1.is_powered)
        self.assertFalse(wire2.is_powered)
        self.assertEqual(0, op_node.output)


class AndNodesTests(unittest.TestCase):
    def create_and_tree(self, value1, value2):
        n1 = mut.Generator(value1)
        n2 = mut.Generator(value2)
        an = mut.AndNode(n1, n2)
        return an

    def test_and_node_zeros(self):
        # arrange
        value1 = 0xff
        value2 = 0x00
        an = self.create_and_tree(value1, value2)

        # act
        actual = an.calculate()

        # verify
        self.assertEqual(0, actual)


    def test_and_node_ones(self):
        # arrange
        value1 = 0xff
        value2 = 0xff
        an = self.create_and_tree(value1, value2)

        # act
        actual = an.calculate()

        # verify
        self.assertEqual(0xff, actual)


class OrNodesTests(unittest.TestCase):
    def create_or_tree(self, value1, value2):
        n1 = mut.Generator(value1)
        n2 = mut.Generator(value2)
        an = mut.OrNode(n1, n2)
        return an

    def test_or_node_zeros(self):
        # arrange
        value1 = 0x00
        value2 = 0x00
        an = self.create_or_tree(value1, value2)

        # act
        actual = an.calculate()

        # verify
        self.assertEqual(0x0, actual)


    def test_or_node_ones(self):
        # arrange
        value1 = 0xff
        value2 = 0x0
        an = self.create_or_tree(value1, value2)

        # act
        actual = an.calculate()

        # verify
        self.assertEqual(0xff, actual)


class WireTests(unittest.TestCase):
    def test_normal_wire_transmission(self):
        # arrange
        leaf = mut.Generator(0xf0)
        wire = mut.Wire(leaf)

        # act
        actual = wire.calculate()

        # verify
        self.assertEqual(0xf0, actual)

    def test_cross_over_wire(self):
        # arrange
        leaf = mut.Generator(0xf0)
        wire = mut.CrossOverWire(leaf)

        # act
        actual = wire.calculate()

        # verify
        self.assertEqual(0x0f, actual)

    def test_cross_over_wire2(self):
        # arrange
        leaf = mut.Generator(int('0b10100000', 2))
        wire = mut.CrossOverWire(leaf)

        # act
        actual = wire.calculate()

        # verify
        self.assertEqual(int('0b00001010', 2), actual)

    def test_power_update_wire(self):
        # arrange
        leaf = mut.Generator(0xff)
        leaf.is_powered = False
        wire = mut.Wire(leaf)
        wire.is_powered = True

        # act
        wire.calculate()

        # assert
        self.assertFalse(wire.is_powered)


class InputTests(unittest.TestCase):
    def test_input_flip(self):
        # arrange
        initial_value = int('0b01000000', 2)
        dip = mut.InputNode(initial_value)

        # act
        dip.flip_bit(6)
        dip.calculate()

        # verify
        self.assertEqual(0, dip.output)

    def test_input_flip_notification(self):
        # arrange
        initial_value = int('0b10000000', 2)
        tree = mut.Tree()
        dip = mut.InputNode(initial_value, tree)
        wire = mut.Wire(dip)
        lock = mut.LockNode(wire, 0x00)
        tree.root = lock

        lock.calculate()

        self.assertEqual(initial_value, lock.output)
        self.assertEqual(initial_value, wire.output)
        self.assertEqual(initial_value, dip.output)

        # act
        dip.flip_bit(7)

        # verify
        self.assertEqual(0, lock.output)


class NandTests(unittest.TestCase):
    def test_nand(self):
        # arrange
        in1 = mut.Generator(int('0b01010101', 2))
        in2 = mut.Generator(int('0b00110011', 2))
        nand = mut.NandNode(in1, in2)

        # act
        nand.calculate()

        # verify
        self.assertEqual(int('0b11101110', 2), nand.output)


class NorTests(unittest.TestCase):
    def test_nor(self):
        # arrange
        in1 = mut.Generator(int('0b01010101', 2))
        in2 = mut.Generator(int('0b00110011', 2))
        nor = mut.NorNode(in1, in2)

        # act
        nor.calculate()

        # verify
        self.assertEqual(int('0b10001000', 2), nor.output)


class XorTests(unittest.TestCase):
    def test_xor(self):
        # arrange
        in1 = mut.Generator(int('0b01010101', 2))
        in2 = mut.Generator(int('0b00110011', 2))
        nor = mut.XorNode(in1, in2)

        # act
        nor.calculate()

        # verify
        self.assertEqual(int('0b01100110', 2), nor.output)


class XnorTests(unittest.TestCase):
    def test_nand(self):
        # arrange
        in1 = mut.Generator(int('0b01010101', 2))
        in2 = mut.Generator(int('0b00110011', 2))
        nor = mut.XnorNode(in1, in2)

        # act
        nor.calculate()

        # verify
        self.assertEqual(int('0b10011001', 2), nor.output)


class NotTests(unittest.TestCase):
    def test_not(self):
        # arrange
        in1 = mut.Generator(0xf0)
        sut = mut.Not(in1)

        # act
        sut.calculate()

        # verify
        self.assertEqual(0x0f, sut.output)

if __name__ == '__main__':
    unittest.main()
