# -*- coding: utf-8 -*-
# 
# New BSD license
#  
# Copyright (c) DR0ID
# This file is part of pyweek18-2014-05
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function


__version_number__ = (0, 0, 0, 0)
__version__ = ".".join([str(num) for num in __version_number__])

__author__ = 'dr0iddr0id {at} gmail [dot] com'
__copyright__ = "DR0ID @ 2014"
__credits__ = ["DR0ID", "Gummbum"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

import pygame

domain_name = "DR0IDs-pyweek18-Hackers-8-bit"  # used for translation files

SCREEN_WIDTH = 1024
SCREEN_HEIGHT = 768
SCREEN_SIZE = (SCREEN_WIDTH, SCREEN_HEIGHT)
caption = "Hackers 8-bit"
flags = pygame.DOUBLEBUF
bit_depth = 32

mixer_frequency = 0
mixer_buffersize = 128

SIM_TIME_STEP = 0.025

PAN_DURATION = 1.0

TILE_WIDTH = 100
TILE_HEIGHT = 100
TILE_SIZE = (TILE_WIDTH, TILE_HEIGHT)

SHOW_LOCKED_STATE_DURATION = 1.0

TEXT_LEVEL_UNLOCK = "Main chip has been hacked!"

ANIM_POWER_INTERVAL = 0.05
