#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
TODO: docstring
"""

__version__ = '$Id: main.py 239 2009-07-27 20:41:19Z dr0iddr0id $'

# do not use __file__ because it is not set if using py2exe

# put your imports here

import gettext
import logging
import os
import sys

import pygame

import pyknicpygame

import settings



# call init before using any of the pyknicpygame/pyknic modules!!
pyknicpygame.init()

# import first context
import dialogs
import sound

logger = logging.getLogger()
logger.setLevel(pyknicpygame.pyknic.settings['log_level'])
# if __debug__:
#     if 'log_console_handler' in pyknicpygame.pyknic.settings:
#         logger.addHandler(pyknicpygame.pyknic.settings['log_console_handler'])

pyknicpygame.pyknic.settings['appdir'] = sys.path[0]
gettext.install(settings.domain_name, pyknicpygame.pyknic.settings['appdir'], 1)


def main():
    # put here your code

    os.environ['SDL_VIDEO_CENTERED'] = '1'
    pygame.mixer.pre_init(
        frequency=settings.mixer_frequency, buffer=settings.mixer_buffersize)
    pygame.init()
    pygame.display.set_caption(settings.caption)
    icon = pygame.image.load("icon.png")
    pygame.display.set_icon(icon)

    screen = pygame.display.set_mode(settings.SCREEN_SIZE, settings.flags, settings.bit_depth)
    sound.init()

    clock = pygame.time.Clock()
    settings.clock = clock

    context = dialogs.MenuContext()
    pyknicpygame.pyknic.context.push(context)

    lock_stepper = pyknicpygame.pyknic.timing.LockStepper()

    context_len = pyknicpygame.pyknic.context.length
    context_top = pyknicpygame.pyknic.context.top

    lock_stepper.event_integrate.add(lambda ls, dt, simt: context_top().think(dt) if context_top() else None)

    if __debug__:
        scheduler = pyknicpygame.pyknic.timing.Scheduler()
        scheduler.schedule(_print_fps, 2, 0, clock)
        lock_stepper.event_integrate.add(lambda ls, dt, simt: scheduler.update(dt))

    while context_len():
        # limit the fps
        dt = clock.tick() / 1000.0  # convert to seconds
        context_top().draw(screen)
        alpha = lock_stepper.update(dt, timestep_seconds=settings.SIM_TIME_STEP)
        if __debug__:
            fps = clock.get_fps()
            # if fps < 60:
            logger.debug('fps: %s, %s', str(fps), alpha)

    pygame.quit()


def _print_fps(clock):
    fps = clock.get_fps()
    logger.debug('fps: %s', str(fps))
    return 1  # return next interval


# this is needed in order to work with py2exe
if __name__ == '__main__':
    main()
