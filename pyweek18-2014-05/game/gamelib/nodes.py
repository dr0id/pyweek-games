# -*- coding: utf-8 -*-
# 
# New BSD license
#  
# Copyright (c) DR0ID
# This file is part of pyweek18-2014-05
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function
import pyknicpygame
from pyknicpygame.pyknic import events

__version_number__ = (0, 0, 0, 0)
__version__ = ".".join([str(num) for num in __version_number__])

__author__ = 'dr0iddr0id {at} gmail [dot] com'
__copyright__ = "DR0ID @ 2014"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

import operator


class Tree(object):
    def __init__(self):
        self.root = None

    def on_bit_state_changed(self, node):
        self.root.calculate()

    def set_power_on_all_nodes(self, value):
        nodes = [self.root]
        while nodes:
            node = nodes.pop(0)
            node.is_powered = value
            nodes.extend(node._children)
        self.root.calculate()


class BitState(object):
    PowerLess = -1
    Off = 0
    On = 1


class Node(object):
    def __init__(self, initial_value=0, powered=True):
        self._is_powered = powered
        self._children = []
        self._output = initial_value
        self.bit_state_changed_event = pyknicpygame.pyknic.events.Signal()

    @property
    def output(self):
        return self._output

    def calculate(self):
        raise NotImplementedError

    def get_state_of_bit(self, bit_position, side=0, check_power=True):
        """
        This returns whether a wire has power or not (green means set, red means not set, grey means without power)
        :param bit_position:
        :param side:
        :return:
        """
        assert bit_position < 8
        assert bit_position >= 0
        if check_power and not self._is_powered:
            return BitState.PowerLess
        pos = 1 << bit_position
        value = self._output & pos
        if value != 0:
            return BitState.On
        return BitState.Off


    @property
    def is_powered(self):
        return self._is_powered

    @is_powered.setter
    def is_powered(self, value):
        if value != self._is_powered:
            self._is_powered = value
            self.bit_state_changed_event.fire(self)


class LockNode(Node):
    def __init__(self, input, expected):
        super(LockNode, self).__init__()
        self.locked_changed_event = pyknicpygame.pyknic.events.Signal()
        self._children = [input]
        self._expected = expected

    def calculate(self):
        old_locked = self.is_locked
        self._children[0].calculate()
        if not self._children[0].is_powered:
            return self._output
        self._output = self._children[0].output
        self.bit_state_changed_event.fire(self)
        if old_locked != self.is_locked:
            self.locked_changed_event.fire(self)
        return self._output

    @property
    def is_locked(self):
        return self._expected != self._output

    def get_expected_state_of_bit(self, bit_position, side=0, check_power=True):
        """
        This returns if the current input is the expected one (green for a match, red for a mismatch, grey means without power)
        :param bit_position:
        :param side:
        :return:
        """
        assert bit_position < 8
        assert bit_position >= 0
        if check_power and not self._is_powered:
            return BitState.PowerLess
        pos = 1 << bit_position
        actual = self._output & pos
        expected = self._expected & pos
        if actual == expected:
            return BitState.On
        return BitState.Off

class LockNodeSpecial(Node):
    def __init__(self, in1, in2, in3, in4):
        super(LockNodeSpecial, self).__init__()
        self._children = [in1, in2, in3, in4]
        self.locked_changed_event = pyknicpygame.pyknic.events.Signal()
        self._is_locked = True

    def calculate(self):
        for child in self._children:
            if child.is_locked:
                return self._output

        self._is_locked = False
        self.locked_changed_event.fire(self)

    @property
    def is_locked(self):
        return self._is_locked

    def get_expected_state_of_bit(self, bit_position, side=0, check_power=True):
        pos = bit_position // 2
        if self._children[pos].is_locked:
            return BitState.Off
        return BitState.On


class OperatorNode(Node):
    def __init__(self, op, input1, input2):
        super(OperatorNode, self).__init__()
        self._op = op
        self._children = [input1, input2]

    def calculate(self):
        for child in self._children:
            child.calculate()
        if not (self._children[0].is_powered and  self._children[0].is_powered):
            return self._output
        self._output = self._op(self._children[0].output, self._children[1].output)
        self.bit_state_changed_event.fire(self)
        return self._output


class AndNode(OperatorNode):
    def __init__(self, input1, input2):
        super(AndNode, self).__init__(operator.and_, input1, input2)
        self.op_str = '&'


class OrNode(OperatorNode):
    def __init__(self, input1, input2):
        super(OrNode, self).__init__(operator.or_, input1, input2)
        self.op_str = '|'

class XorNode(OperatorNode):
    def __init__(self, input1, input2):
        super(XorNode, self).__init__(operator.xor, input1, input2)
        self.op_str = '^'

class NandNode(OperatorNode):
    def __init__(self, input1, input2):
        super(NandNode, self).__init__(lambda a,b: invert(operator.and_(a, b)), input1, input2)
        self.op_str = '!&'

class NorNode(OperatorNode):
    def __init__(self, input1, input2):
        super(NorNode, self).__init__(lambda a,b: invert(operator.or_(a, b)), input1, input2)
        self.op_str = '!|'

class XnorNode(OperatorNode):
    def __init__(self, input1, input2):
        super(XnorNode, self).__init__(lambda a,b: invert(operator.xor(a, b)), input1, input2)
        self.op_str = '!^'

class Generator(Node):
    def __init__(self, value):
        super(Generator, self).__init__()
        self._output = value

    def calculate(self):
        if not self.is_powered:
            return 0
        return self._output


class InputNode(Generator):
    def __init__(self, initial, tree=None):
        super(InputNode, self).__init__(initial)
        if tree:
            self.bit_state_changed_event += tree.on_bit_state_changed

    def flip_bit(self, bit_position):
        assert bit_position < 8
        assert bit_position >= 0
        pos = 1 << bit_position
        self._output ^= pos
        self.bit_state_changed_event.fire(self)


class Wire(Node):
    def __init__(self, input):
        super(Wire, self).__init__()
        self._children = [input]

    def calculate(self):
        self._output = self._children[0].calculate()
        self.is_powered = self._children[0].is_powered
        self.bit_state_changed_event.fire(self)
        return self._do_switch()


    def _do_switch(self):
        # override this method to implement switch logic
        return self._output

class Not(Wire):

    def __init__(self, input):
        super(Not, self).__init__(input)
        self.op_str = '!'

    def _do_switch(self):
        self._output = invert(self._output)
        return self._output

class CrossOverWire(Wire):
    def _do_switch(self):
        # getting the upper bits
        upper = self._output & 0xf0
        # clearing the upper bits
        self._output &= 0x0f
        # shift left
        self._output <<= 4
        # shift right
        upper >>= 4
        # set lower bits
        self._output |= upper
        return self._output

def invert(value):
    return ~value & 0xff