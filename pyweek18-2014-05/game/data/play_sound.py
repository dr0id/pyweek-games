#!/usr/bin/env python

import sys
import pygame

pygame.mixer.init(0, 0, 0, buffer=1024)
if len(sys.argv) > 1:
    sound_file = sys.argv[1]
else:
    sound_file = '../music/Loveshadow_-_WE_R_1.ogg'
sound = pygame.mixer.Sound(sound_file)

channel = sound.play()
while channel.get_busy() and channel.get_sound() is sound:
    pygame.time.wait(100)
