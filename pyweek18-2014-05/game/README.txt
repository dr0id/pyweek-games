﻿Hackers 8-bit
=============

A game written for pyweek 18 (www.pyweek.org)

The theme was '8-bit'.



Members: DR0ID, Gummbum


DEPENDENCIES: 
python (tested with: 3.4, 3.2, 2.7, 2.6)
pygame (tested with: 1.9.2a0)

You might need to install some of these before running the game:

  Python:     http://www.python.org/
  PyGame:     http://www.pygame.org/
              pygame for python 3.4 can be found here: http://www.lfd.uci.edu/~gohlke/pythonlibs/#pygame


RUNNING THE GAME:

On Windows or Mac OS X, locate the "run_game.pyw" file and double-click it.

Otherwise open a terminal / console and "cd" to the game directory and run:

  python run_game.py



HOW TO PLAY THE GAME:

Hackers 8-bit is an adventure in which you will use your leet hacking skills to
escape a fate worse than death.

You will encounter puzzles of various difficulty, and you will be under pressure
to solve each one to get one step closer to freedom.

The challenges involve hacking alien security systems. Unlocking the security
systems' array of chips will open up new areas to conquer.

Each control chip can be manipulated by its associated binary switches. Flipping
a switch turns a circuit on and off. The associated LED indicates the state of
the circuit.

You may have to move the circuit board around to see all the chips.

Chips have different functions, and combine in complementary bit-logic ways.
You will have to keep your wits to keep your life!

NOTE:

Unfortunately the level design was not that fast and easy as thought. Therefore some levels
may repeat and are very simple to solve.