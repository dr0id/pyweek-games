# -*- coding: utf-8 -*-

import string
import os
import sys

import glob

import operator

import pygame

prefix = "./converted/"

def process_image(img_name, size):
    img = pygame.image.load(img_name)
    new_img = pygame.transform.rotozoom(img, 0.0, size)
    new_name = prefix+img_name
    if os.path.exists(new_name):
        os.remove(new_name)
        print("deleted: " + new_name)
    pygame.image.save(new_img, new_name)
    print("created: "+new_name)

def process_map_file(img_name, size):
    map_file_name = string.rsplit(img_name, ".", 1)[0] + ".txt"

    if os.path.exists(map_file_name):
        new_name = prefix+map_file_name
        with open(map_file_name, 'rb') as original:
            print("reading file: "+map_file_name)
            with open(new_name, 'wb') as converted:
                for line in original:
                    name, rect_data = string.split(line, " = ")
                    rect_data = string.split(rect_data, " ")
                    rect_data = map(int, rect_data)
                    rect_data = map(operator.mul, rect_data, [size] * 4)
                    rect_data = map(int, rect_data)
                    rect_data = map(str, rect_data)
                    new_line = name+" = "+" ".join(rect_data)+"\n"
                    converted.write(new_line)
            print("written file: "+new_name)

if __name__ == '__main__':

    not_used, size = sys.argv
    size = float(size)
    import pygame
    from pygame.locals import *
    pygame.init()
    
    for img_name in glob.glob("*.png"):
        print img_name
        process_image(img_name, size)
        process_map_file(img_name, size)

    
    
    
