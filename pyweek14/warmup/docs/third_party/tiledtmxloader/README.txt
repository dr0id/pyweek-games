pytmxloader 

a python map loader for the map format used by Tiled (see www.mapeditor.org)

Homepage and download:
    https://code.google.com/p/pytmxloader/
    https://code.google.com/p/pytmxloader/downloads/list



Version 3.0.0 changelog:

    
    - using 'New BSD license' now
    - improved loading speed of maps
    - improved pygame loader and renderer
    - pyglet part is pretty broken
    






