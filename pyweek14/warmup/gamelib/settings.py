"""gummworld2.py - startup and runtime settings file

This file is part of Gummworld2.

Gummworld2 is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gummworld2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Gummworld2.  If not, see <http://www.gnu.org/licenses/>.

Usage
=====
Settings in this file should always be accessed like so:
    
    import settings
    if settings.whatever: ...
    settings.whatever = value

This insures that runtime changes will be noticed by other modules that import
the settings module. Of course, one can always make local copies of the values
if static behavior is desired.
"""

import gummworld2

## Display
resolution = 800,600        # screen size in pixels: width,height
fullscreen = False          # open fullscreen if True

## Graphics
# Defaults already set for graphics asset paths. You can add new subdirs like so.
#gummworld2.data.set_subdir('myname', 'subdir')

## Sound
# Defaults already set for sound asset paths. You can add new subdirs like so.
gummworld2.data.set_subdir('music', 'music')

## Debug
profile = False             # run cProfile if True

## Custom
import os

# Gummbum's development preferences... remove or add a case for your own.
if os.environ.get('COMPUTERNAME',None) == 'BW-PC':
    print '## CUSTOM SETTINGS FOR BW (see settings.py)'
    os.environ['SDL_VIDEO_CENTERED'] = '1'

del os
