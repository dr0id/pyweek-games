"""intro.py - a context for displaying a splash screen, slideshow,
movie, etc.

This file is part of Gummworld2.

Gummworld2 is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gummworld2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Gummworld2.  If not, see <http://www.gnu.org/licenses/>.
"""

import random

import pygame

import gummworld2
from gummworld2 import State

import settings, mainmenu

class Intro(gummworld2.Engine):
    
    def __init__(self):
        gummworld2.Engine.__init__(self,
            resolution=settings.resolution,
            update_speed=60, frame_speed=60,
        )
    
    def enter(self):
        self.screen.clear_color = pygame.Color('darkgreen')
        font_file = gummworld2.data.filepath('font', 'VeraBd.ttf')
        font = pygame.font.Font(font_file, 32)
        self.text_img = font.render(
            'Intro (Wait 10 or Click)', True, pygame.Color('white'))
        self.text_rect = self.text_img.get_rect()
        self.dx = random.choice((-3,-2,-1,1,2,3))
        self.dy = random.choice((-3,-2,-1,1,2,3))
        self.countdown = 10  ## seconds
    
    def update(self, dt):
        self.countdown -= dt
        if self.countdown <= 0.:
            self.action_main_menu()
        
        text_rect = self.text_rect
        text_rect.x += self.dx
        text_rect.y += self.dy
        screen_rect = State.screen.rect
        if not screen_rect.contains(text_rect):
            if text_rect.x < 0 or text_rect.right >= screen_rect.right:
                self.dx *= -1
            if text_rect.y < 0 or text_rect.bottom >= screen_rect.bottom:
                self.dy *= -1
            self.text_rect.clamp_ip(screen_rect)
    
    def draw(self, dt):
        State.screen.clear()
        State.screen.blit(self.text_img, self.text_rect)
        State.screen.flip()
    
    def action_main_menu(self):
        self.pop()
        self.push(mainmenu.MainMenu())
    
    def on_mouse_button_down(self, pos, button):
        self.action_main_menu()
