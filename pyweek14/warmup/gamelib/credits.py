"""credits.py - a context for displaying a game's credits

This file is part of Gummworld2.

Gummworld2 is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gummworld2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Gummworld2.  If not, see <http://www.gnu.org/licenses/>.
"""

import gummworld2

import settings


class Credits(gummworld2.Engine):
    
    def __init__(self):
        gummworld2.Engine.__init__(self,
            resolution=settings.resolution,
            update_speed=60, frame_speed=60,
        )
        
        ## Auto Exit - remove me...
        self.clock.schedule_interval(self.auto_exit, 2.0)
    
    def auto_exit(self, dt):
        self.pop()
    
    def update(self, dt):
        pass
    
    def draw(self, dt):
        self.screen.fill((0,0,0))
        self.screen.flip()
