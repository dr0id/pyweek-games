"""mainmenu.py - a context for running the application's main menu

This file is part of Gummworld2.

Gummworld2 is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gummworld2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Gummworld2.  If not, see <http://www.gnu.org/licenses/>.
"""

import pygame

import gummworld2
from gummworld2 import State

import settings, game, credits


class ClickButton(pygame.sprite.Sprite):
    
    def __init__(self, text_img, y, action):
        self.image = text_img
        self.rect = text_img.get_rect(y=y, centerx=State.screen.rect.centerx)
        self.action = action
    
    def click(self, mouse_pos):
        if self.rect.collidepoint(mouse_pos):
            self.action()


class MainMenu(gummworld2.Engine):
    
    def __init__(self):
        gummworld2.Engine.__init__(self,
            resolution=settings.resolution,
            update_speed=60, frame_speed=60,
        )
    
    def enter(self):
        font_file = gummworld2.data.filepath('font', 'VeraBd.ttf')
        font = pygame.font.Font(font_file, 32)
        screen_centery = State.screen.rect.centery
        text_height = font.get_height()
        
        self.buttons = []
        
        n = -1
        text_img = font.render('PLAY GAME', True, pygame.Color('white'))
        self.buttons.append(ClickButton(
            text_img, screen_centery + n * text_height, self.action_play_game))
        
        n += 1
        text_img = font.render('CREDITS', True, pygame.Color('white'))
        self.buttons.append(ClickButton(
            text_img, screen_centery + n * text_height, self.action_show_credits))
        
        n += 1
        text_img = font.render('QUIT GAME', True, pygame.Color('white'))
        self.buttons.append(ClickButton(
            text_img, screen_centery + n * text_height, self.action_quit_game))
    
    def exit(self):
        ## Outro?
        pass
    
    def action_play_game(self):
        self.push(game.Game())
    
    def action_show_credits(self):
        self.push(credits.Credits())
    
    def action_quit_game(self):
        self.pop()
    
    def update(self, dt):
        pass
    
    def draw(self, dt):
        State.screen.clear()
        for b in self.buttons:
            State.screen.blit(b.image, b.rect)
        State.screen.flip()
    
    def on_mouse_button_down(self, pos, button):
        for b in self.buttons:
            b.click(pos)
