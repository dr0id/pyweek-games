# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------------

import sys

import pygame
from pygame.locals import (
    QUIT,
    KEYDOWN, KEYUP,
    MOUSEMOTION, MOUSEBUTTONUP, MOUSEBUTTONDOWN,
    JOYAXISMOTION, JOYBALLMOTION, JOYHATMOTION, JOYBUTTONUP, JOYBUTTONDOWN,
    ACTIVEEVENT, VIDEORESIZE, VIDEOEXPOSE,
    USEREVENT,
)


import paths
import gummworld2
from gummworld2 import context, Engine, Vec2d, State
from gummworld2 import events


WINDOW_SIZE = (800, 600)
DURATION = 1.0

EVENT_TYPES_MAP = {
    ACTIVEEVENT: "on_active_event",        #(gain, state)
    JOYAXISMOTION: "on_joy_axis_motion",   #(joy, axis, value)
    JOYBALLMOTION: "on_joy_ball_motion",   #(joy, ball, rel)
    JOYBUTTONDOWN: "on_joy_button_down",   #(joy, button)
    JOYBUTTONUP: "on_joy_button_up",       #(joy, button)
    JOYHATMOTION: "on_joy_hat_motion",     #(joy, hat, value)
    KEYDOWN: "on_key_down",                #(unicode, key, mod)
    KEYUP: "on_key_up",                    #(key, mod)
    MOUSEBUTTONDOWN: "on_mouse_button_down", #(pos, button)
    MOUSEBUTTONUP: "on_mouse_button_up",   #(pos, button)
    MOUSEMOTION: "on_mouse_motion",        #(pos, rel, buttons)
    QUIT: "on_quit",                       #()
    USEREVENT: "on_user_event",            #(e)
    VIDEOEXPOSE: "on_video_expose",        #()
    VIDEORESIZE: "on_video_resize",        #(size, w, h)
    }
    
EVENT_TYPES = ["on_special_1", "on_special_2"]
EVENT_TYPES.extend(EVENT_TYPES_MAP.values())


# register the event types
for event_type in EVENT_TYPES:
    events.EventQueueDispatcher.register_event_type(event_type)

# mapping for pygame
def get_pygame_events(): # -> [(event_type, args)]
    return [(EVENT_TYPES_MAP[e.type], [e]) for e in pygame.event.get()]

# event queue using the pygame mapper function
event_queue = events.EventQueueDispatcher(get_pygame_events)


class EventPrinter(object):

    def on_special_1(self, *args):
        print str(self), "on_special_1", args
    
    def on_special_2(self, a, b, c):
        print str(self), "on_special_2", a, b, c

    ## Override an event handler to get specific events.
    def on_active_event(self, event):
        print str(self), event

    def on_joy_axis_motion(self, event):
        print str(self), event

    def on_joy_ball_motion(self, event):
        print str(self), event
    
    def on_joy_button_down(self, event):
        print str(self), event

    def on_joy_button_up(self, event):
        print str(self), event

    def on_joy_hat_motion(self, event):
        print str(self), event

    def on_key_down(self, event):
        print str(self), event
        if event.key == pygame.K_SPACE:
            switch_printer(self)
        elif event.key == pygame.K_1:
            event_queue.enqueue_event("on_special_1", [])
        elif event.key == pygame.K_2:
            event_queue.enqueue_event("on_special_2", 11, 22, 33)
        elif event.key == pygame.K_ESCAPE:
            sys.exit() # unelegant


    def on_key_up(self, event):
        print str(self), event

    def on_mouse_button_down(self, event):
        print str(self), event

    def on_mouse_button_up(self, event):
        print str(self), event

    def on_mouse_motion(self, event):
        print str(self), event

    def on_quit(self, event):
        print str(self), event
        sys.exit() # unelegant

    def on_user_event(self, event):
        print str(self), event

    def on_video_expose(self, event):
        print str(self), event

    def on_video_resize(self, size, w, h):
        print str(self), event

def switch_printer(event_printer):
    print 'removing printer', event_printer
    event_queue.pop_listeners(event_printer)
    new_printer = EventPrinter()
    print 'registering new printer', new_printer
    event_queue.push_listeners(new_printer)

def main():
    event_queue.push_listeners(EventPrinter())
    
    pygame.init()
    pygame.display.set_caption("keys: space | 1 | 2")
    pygame.display.set_mode((400, 60))
    pygame.display.flip()
    
    while True:
        event_queue.update_all()
    
    

if __name__ == '__main__':

    main()



