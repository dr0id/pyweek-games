# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------------
# _path is only needed if run from the examples directory
try:
    import _path # has to be first import to make sure it finds the 'context' package
except:
    pass
# -----------------------------------------------------------------------------

import random

import pygame

# import context
# import pyknicpygame
# from pyknicpygame import pyknic

import paths
import gummworld2
from gummworld2 import context, Engine, Vec2d, State
from gummworld2.context import effects, transitions



WINDOW_SIZE = (800, 600)
DURATION = 1.0

class EffectShowContext(Engine):

    effects = [
                effects.Slide(DURATION, WINDOW_SIZE[0], effects.Slide.LEFT),
                effects.Slide(DURATION, WINDOW_SIZE[0], effects.Slide.RIGHT),
                effects.Slide(DURATION, WINDOW_SIZE[1], effects.Slide.UP),
                effects.Slide(DURATION, WINDOW_SIZE[1], effects.Slide.DOWN),
                effects.Slide(DURATION, WINDOW_SIZE[0], effects.Slide.LEFT, False),
                effects.Slide(DURATION, WINDOW_SIZE[0], effects.Slide.RIGHT, False),
                effects.Slide(DURATION, WINDOW_SIZE[1], effects.Slide.UP, False),
                effects.Slide(DURATION, WINDOW_SIZE[1], effects.Slide.DOWN, False),
                effects.Slide(DURATION, WINDOW_SIZE[0], effects.Slide.LEFT, True, False),
                effects.Slide(DURATION, WINDOW_SIZE[0], effects.Slide.RIGHT, True, False),
                effects.Slide(DURATION, WINDOW_SIZE[1], effects.Slide.UP, True, False),
                effects.Slide(DURATION, WINDOW_SIZE[1], effects.Slide.DOWN, True, False),
              ]
    current_effect = -1
    old_think = False
    new_think = False
    font = None
    font_small = None

    def __init__(self):
        
        context.Context.__init__(self)
        self.color = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
        self.text = ""
        self.trans_description = ""

    def exit(self):
        # clear events after transition
        pygame.event.clear()

    def enter(self):
        EffectShowContext.current_effect += 1
        if EffectShowContext.current_effect >= len(EffectShowContext.effects):
            EffectShowContext.current_effect = 0
        self.text = "Context B" if self.current_effect % 2 else "Context A"
        self.trans_description = str(EffectShowContext.effects[EffectShowContext.current_effect])

    def draw(self, screen):
        screen.fill(self.color)
        text_image = self.font.render(self.text, 0, (255, 255, 255))
        rect = text_image.get_rect(center=screen.get_rect().center)
        screen.blit(text_image, rect)
        
        text_image = self.font_small.render(self.trans_description, 0, (255, 255, 255))
        rect = text_image.get_rect(center=screen.get_rect().center)
        screen.blit(text_image, rect.move(0, 60))
    
    def update(self, dt):
        context.Context.update(self, dt)

class Application(Engine):

    def __init__(self):
        screen_size = Vec2d(800,600)
        Engine.__init__(self,
        resolution=screen_size, caption='EffectShowContext',
        # tile_size=(128,128), map_size=(10,10),
        # camera_target=CameraTarget(screen_size//2),
        frame_speed=60)
        context.push(EffectShowContext())
        context.print_stack()

    def on_quit(self):
        context.pop()
            
    def on_key_down(self, unicode, key, mod):
        if key == pygame.K_ESCAPE:
            context.pop()
        elif key == pygame.K_SPACE:
            transEffect = EffectShowContext.effects[EffectShowContext.current_effect]
            context.push( \
                transitions.Transition( EffectShowContext(), 
                                                transEffect, 
                                                EffectShowContext.old_think, 
                                                EffectShowContext.new_think)
                                                )

    def update(self, dt):
        if context.top():
            context.top().update(dt)
            
    def draw(self, interp):
        self.screen.clear()
        if context.top():
            context.top().draw(self.screen.surface)
        State.screen.flip()

        
    def run(self):
        self.enter()
        while context.top():
            State.clock.tick()



def main():

    # init pygame
    pygame.init()
    screen = pygame.display.set_mode(WINDOW_SIZE)
    pygame.display.set_caption("effects demo, press 'SPACE' for next transition")
    clock = pygame.time.Clock()
    
    # init context
    con = EffectShowContext()
    context.push(con)
    EffectShowContext.font = pygame.font.Font(None, 60)
    EffectShowContext.font_small = pygame.font.Font(None, 20)
    
    # loop
    while context.top():
        dt = clock.tick(60.0) / 1000.0 # convert to seconds
        context.top().draw(screen)
        context.top().update(dt)
        pygame.display.flip()



if __name__ == '__main__':

    EffectShowContext.font = pygame.font.Font(None, 60)
    EffectShowContext.font_small = pygame.font.Font(None, 20)

    app = Application()
    app.run()
    
    # main()



