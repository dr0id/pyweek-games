Requirements:

    Python 2.6
    Pygame 1.9.1

    Mouse


To play the game:

    For Windows double-click run_game.py.

    For Linux and Mac run: python run_game.py.


Playing the game:

    Use the mouse to click and eventually the top number keys. Using space the most things can be skipped if wanted.




Art | Spritesheets | 3rd party:

    As listed below, this resource is being provided under an "Attribution" license. 
    This means you are free to use it for any purpose as long as "Pow Studios" is given credit.



