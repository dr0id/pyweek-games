#!/usr/bin/env python

"""
Gather the system fonts and display sample alphabet and punctuation characters.

Keyboard controls:
    RIGHT, LEFT     Next, previous font.
    UP, DOWN        Increase, decrease size.
    a               Antialias rendering.
"""


import glob
import os
from os.path import join as join_path
import sys
import pygame
from pygame.locals import *


# Default font dir. Command line argument overrides this.
font_dir = join_path('..','font')


# This program will avoid the fonts in this list.
bad_font_list = ()


##                         ##
##                         ##
##  No more configurables  ##
##                         ##
##                         ##


def is_bad_font(name):
    return name not in bad_font_list


def do_key(key):
    global n, name, size, antialias, font
    dirty = True
    if key == K_a:
        antialias = antialias == False
    elif key == K_LEFT:
        if n > 0:
            n -= 1
            name = font_list[n]
    elif key == K_RIGHT:
        if n < len(font_list)-1:
            n += 1
            name = font_list[n]
    elif key == K_UP:
        size += 1
    elif key == K_DOWN:
        size -= 1
    else:
        dirty = False
    if dirty:
        set_caption()
        make_font()
        draw(texts)


def make_font():
    global font
    print 'loading font',n,name,'(if this hangs, manually add the name to bad_font_list)'
    font = pygame.font.Font(name, size)


def draw(lines):
    screen.fill((0,0,0))
    for i,line in enumerate(lines):
        im = font.render(line, antialias, Color('white'))
        screen.blit(im, (5,i*font.get_height()))
    pygame.display.flip()


def set_caption():
    pygame.display.set_caption('%d/%d: %d %s %s' % (
        n+1, nr_fonts, size,
        aa_display[antialias],
        name))


##                ##
##                ##
##  Main program  ##
##                ##
##                ##

os.environ['SDL_VIDEO_CENTERED'] = '1'
pygame.init()
screen = pygame.display.set_mode((600,600))

texts = (
    'ABCDEFGHIJKLMNOPQRTUVWXYZ',
    'abcdefghijklmnopqrtuvwxyz',
    '1234567890`~!@#$%^&*()-_=+[]{}\\|;:\'",.<>/?',
)

aa_display = {
    True : 'AA',
    False : 'No_AA',
}


if len(sys.argv) > 1:
    font_dir = sys.argv[1]
font_list = []
if os.name == 'nt':
    truetype_pats = ('*.ttf',)
else:
    truetype_pats = ('*.ttf','*.TTF')
for pat in truetype_pats:
    font_list.extend(filter(is_bad_font, glob.glob(join_path(font_dir,pat))))
font_list.sort()
n = 0
nr_fonts = len(font_list)
name = font_list[n]
size = 13
antialias = True
font = None

set_caption()
make_font()
draw(texts)

while 1:
    for e in pygame.event.get():
        if e.type == KEYDOWN:
            if e.key == K_ESCAPE:
                quit()
            else:
                do_key(e.key)
    pygame.time.wait(100)
