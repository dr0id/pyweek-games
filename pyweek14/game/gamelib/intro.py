"""intro.py - a context for displaying a splash screen, slideshow,
movie, etc.

This file is part of Gummworld2.

Gummworld2 is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gummworld2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Gummworld2.  If not, see <http://www.gnu.org/licenses/>.
"""

import random
import time
import os
import math

import pygame

import gummworld2
from gummworld2 import State
from gummworld2 import context, Engine, Vec2d, events
from gummworld2.context import effects, transitions
import animation
from animation import Animation

import settings, mainmenu, sound
import entity
import scenemanager
import settings
import fonts


class BlackScreen(context.Context):

    def __init__(self, intro, end=False):
        context.Context.__init__(self)
        self.intro = intro
        if end:
            State.clock.schedule_interval(self.done, 1, 1)

    def draw(self, surf):
        surf.fill((0,0,0))

    def done(self, args):
        self.intro.goto_mainmenu()


class FadeOutEffect(transitions.TransitionEffect):

    def __init__(self, duration, resolution):
        self.duration = duration
        self.current_time = duration
        self._time = time.time()
        self._start_time = self._time
        self.surf = pygame.Surface(resolution)
        self.alpha_value = 255

    def update(self, delta_time):
        self.alpha_value = int(255.0 * self.current_time / self.duration)
        self.alpha_value = max(0, self.alpha_value)
        self.alpha_value = min(255, self.alpha_value)

        ## Note from Gumm.
        ## As I reported, this was very slow even on my Intel i3 CPU. In
        ## Intro.__init__ I reduced ups and fps from 60 to 25 and the timing is
        ## much closer now on my system.
        ## It looks like this is using a lot of CPU. After looking closer I
        ## can see that creating a new large surface (1000x600) every frame,
        ## and blitting with surface-alpha would be very expensive.
        t = time.time()
        self.current_time -= delta_time ##* 5
        if __debug__: print '???', self.current_time, t-self._time, delta_time, self.duration, pygame.time.get_ticks()
        self._time = t
        if self.current_time > 0:
            return False
        if __debug__: print 'FadeOutEffect: elapsed {0:0.2f}'.format(
            time.time() - self._start_time)
        return True # done


    def draw(self, screen, old_context, new_context):
        new_context.draw(screen)
        old_context.draw(self.surf)
        self.surf.set_alpha(self.alpha_value)
        screen.blit(self.surf, (0,0))

class Ent(object):
    def __init__(self, image, pos, z, vis=True):
        self.image = image
        self.pos = pos
        self.z = z
        self.visible = vis
    def draw(self, surf):
        if self.visible:
            surf.blit(self.image, self.pos)
    def update(self, dt):
        pass
    def set_visible(self, vis):
        self.visible = vis

class AnimatedEnt(Ent):
    def __init__(self, anim, pos, z, vis=True):
        Ent.__init__(self, anim.image(), pos, z, vis)
        self.anim = anim
        self.event_anim_end = events.Signal()
        anim.event_loop_end.add(self._anim_end)
    def update(self, dt):
        self.anim.think(dt)
    def draw(self, surf):
        if self.visible:
            self.image = self.anim.image()
            Ent.draw(self, surf)
    def _anim_end(self, anim):
        self.event_anim_end.fire(self)

class MonsterEnt(AnimatedEnt):
    def __init__(self, anim, pos, z, vis):
        AnimatedEnt.__init__(self, anim, pos, z, vis)
        anim.play()
        self.x = pos[0]
        self.y = pos[1]
        self.tx = 840
        self.ty = 350
        self.duration = 10
        self.current_time = self.duration
        self.event_done = events.Signal()
    def update(self, dt):
        AnimatedEnt.update(self, dt)
        self.current_time -= dt
        percent = 1.0 - self.current_time / self.duration
        x, y = self.pos
        x = self.x + self.tx * percent
        y = self.y + self.ty * percent
        self.pos = x, y
        if __debug__: print 'Monsta', self.pos
        if x > settings.resolution[0] - 50:
            self.event_done.fire()

class FadingText(Ent):
    def __init__(self, txt, duration, pos, z, vis=True):
        font_file = gummworld2.data.filepath('font', settings.fonts.intro)
        font = pygame.font.Font(font_file, 32)

        font = pygame.font.Font(gummworld2.data.filepath('font', settings.fonts.credits), 40)

        color = pygame.Color('darkolivegreen1')
        img = font.render(txt, True, color)
        self.text_rect = img.get_rect()
        self.image = pygame.Surface(self.text_rect.size)
        self.image.blit(img, (0, 0))
        self.image.set_colorkey(self.image.get_at((0,0)))

        Ent.__init__(self, self.image, (pos[0], pos[1]-self.text_rect.h), z, vis)
        self.text = txt
        self.duration = duration * 0.25
        self.current_time = duration * 0.25
        self.alpha = 0
        self.update = self.update_inc
    def update_inc(self, dt):
        self.current_time -= dt
        self.alpha = 255.0 - (255.0 * self.current_time / self.duration)
        if self.current_time <= 0:
            self.current_time = self.duration
            self.update = self.update_steady
    def update_steady(self, dt):
        self.alpha = 255
        self.current_time -= dt
        if self.current_time <= 0:
            self.current_time = self.duration
            self.update = self.update_dec
    def update_dec(self, dt):
        self.current_time -= dt
        self.alpha = 255.0 * self.current_time / self.duration
        if self.current_time <= 0:
            self.visible = False
    def draw(self, surf):
        self.image.set_alpha(int(self.alpha))
        Ent.draw(self, surf)

class Flash(Ent):
    def __init__(self, image, pos, z):
        Ent.__init__(self, image, pos, z)
        self.is_on = False
        self.schedules = [
        (self.set_on, 0.3, 1),
        (self.set_off, 0.1, 1),
        (self.set_on, 0.2, 1),
        (self.set_off, 0.2, 1),
        (self.set_on, 0.1, 1),
        (self.set_off, 0.1, 1),
        ]
        # self.schedule_next()
        self.bomb_channel = None
    def draw(self, surf):
        if self.is_on:
            Ent.draw(self, surf)
    def set_on(self, *args):
        if self.bomb_channel is None or not self.bomb_channel.get_busy():
            self.bomb_channel = sound.bomb(0)
        self.is_on = True
        self.schedule_next()
    def set_off(self, *args):
        self.is_on = False
        self.schedule_next()
    def schedule_next(self, *args):
        if self.schedules:
            args = self.schedules.pop(0)
            if __debug__: print 'flash scheduling next:', args
            State.clock.schedule_interval(*args)

# Tips from the scientist

import pygame, gummworld2
import settings
import animation


class Shaker(object):
    def __init__(self, anchor_x, anchor_y, tracker):
        self.tracker = tracker
        self.anchor = gummworld2.Vec2d(anchor_x, anchor_y)
        self.position = gummworld2.Vec2d(tracker.position)
        self.vel = gummworld2.Vec2d(0.0, 0.0)
        self.acc = gummworld2.Vec2d(0.0, 0.0)
        self.spring_constant = 10 # value??
        self.damping = 0.9 # value??
        self.z = 0

    def shake(self):
        if __debug__: print '!?!? shaker!'
        self.position += gummworld2.Vec2d(10, 10)

    def update(self, dt):
        # stiff spring simulation
        rel_pos = self.position - self.anchor
        gamma = 0.5 * math.sqrt(4 * self.spring_constant - self.damping ** 2)
        print '>>>> shaker up', rel_pos, gamma
        if gamma == 0:
            return
        c = rel_pos * (self.damping / ( 2 * gamma)) + self.vel * (1.0 / gamma)
        target = rel_pos * math.cos(gamma * dt) + c * math.sin(gamma * dt)
        target *= math.e ** (-0.5 * self.damping * self.damping)
        accel = (target - rel_pos) * 1.0 / (dt * dt) - self.vel * dt

        self.acc += accel
        self.vel += self.acc * dt
        self.position += self.vel * dt

        self.tracker.position = self.position
        print '>>>> tracker pos', self.tracker.position

    def draw(self, surf):
        pass

class HeadQuarter(context.Context):

    def __init__(self, intro):
        self.intro = intro
        # self.shaker = Shaker(settings.resolution[0]//2, settings.resolution[1]//2, self.intro.tracker)
        image = pygame.image.load(gummworld2.data.filepath('intro', "facility.png"))
        facility = Ent(image, (0,0), 10)
        # door
        image = pygame.image.load(gummworld2.data.filepath('intro', "door.png"))
        self.door = Ent(image, (320, 165), 17)
        image = pygame.image.load(gummworld2.data.filepath('intro', "door-damaged.png"))
        self.door_damaged = Ent(image, (320, 165), 14, False)
        # entry
        image = pygame.image.load(gummworld2.data.filepath('intro', "entry-front.png"))
        self.entry_front = Ent(image, (150, 60), 16)
        image = pygame.image.load(gummworld2.data.filepath('intro', "entry-back.png"))
        self.entry_back = Ent(image, (150, 60), 14)
        # damaged entry
        image = pygame.image.load(gummworld2.data.filepath('intro', "entry-damaged-front.png"))
        self.entry_damaged_front = Ent(image, (150, 60), 16, False)
        image = pygame.image.load(gummworld2.data.filepath('intro', "entry-damaged-back.png"))
        self.entry_damaged_back = Ent(image, (150, 60), 14, False)

        image = pygame.image.load(gummworld2.data.filepath('intro', "front-wall.png"))
        self.front_wall = Ent(image, (615, 108), 20, True)
        # flashes
        image = pygame.image.load(gummworld2.data.filepath('intro', "flash.png"))
        image.set_alpha(None)
        image.set_colorkey((0,0,0))
        self.flash = Flash(image, (0, 10), 9)
        self.flash_up = Flash(image, (0, -120), 9)
        # smoke
        smoke_plume = Animation(os.path.join("spritesheets_powstudio", "smoke_plume_spritesheet.png"), nframes=10, fps=10, zoom=2.0)
        smoke_puff_right = Animation(os.path.join("spritesheets_powstudio", "smoke_puff_right_spritesheet.png"), nframes=10, fps=10, zoom=2.0, rotation=-25)
        self.smoke_big = AnimatedEnt(smoke_plume, (200, 50), 21, True)
        self.smoke_puff = AnimatedEnt(smoke_puff_right, (250, 100), 20, True)
        self.smoke_big.event_anim_end.add(self.remove_entity)
        self.smoke_puff.event_anim_end.add(self.remove_entity)
        # fading text
        self.text = FadingText("Secret military research facility 143...", 10, (50, settings.resolution[1] - 50), 100, vis=True)
        # monster
        monsterwalkright = Animation("monster_x5.png", frames=(2,0,1,2,4,3), fps=3, size=(80,100), mirror=True)
        self.monster = MonsterEnt(monsterwalkright, (230, 130), 15, True)
        self.monster.event_done.add(self.done)


        # TODO: add conversation using tipper, timit it right with the other things going on
        sentences = [

                ]
        # TODO: add comic strings!
        sequence = [
            # text
            (self.schedule_add, 3, 1, [self.text, True]),
            # (self.shake, 0, 1, [self.shake, True]),
            # flash
            (self.flash_up.schedule_next, 6, 1, []),
            (self.comicstring, 2, 1, [self, 100, 50, 0, 0, 4, entity.grrr_image, 0, 0]),

            (self.comicstring, 1, 1, [self, 200, 20, 0, 0, 1, entity.pam_image, 0, -4]),
            (self.comicstring, 0.3, 1, [self, 210, 40, 0, 0, 1, entity.pam_image, 0, -4]),
            (self.comicstring, 0.4, 1, [self, 205, 45, 0, 0, 1, entity.pam_image, 0, -4]),
            (self.comicstring, 0.7, 1, [self, 190, 50, 0, 0, 1, entity.pam_image, 0, -4]),

            (self.comicstring, 0.3, 1, [self, 190, 55, 0, 0, 2, entity.click_image, 0, -4]),
            (self.comicstring, 0.4, 1, [self, 210, 45, 0, 0, 2, entity.click_image, 0, -4]),

            (self.comicstring, 1.5, 1, [self, 230, 25, 0, 0, 3, entity.aaah_image, 0, -4]),

            (self.flash.schedule_next, 3, 1, []),
            (self.comicstring, 2, 1, [self, 100, 150, 0, 0, 4, entity.grrr_image, 0, -1]),
            # explosion
            (self.set_visible, 4, 1, [self.door, False]),
            # (self.shake, 0, 1, [self.shake, True]),
            (self.set_visible, 0, 1, [self.door_damaged, True]),
            (self.set_visible, 0, 1, [self.entry_back, False]),
            (self.set_visible, 0, 1, [self.entry_front, False]),
            (self.set_visible, 0, 1, [self.entry_damaged_back, True]),
            (self.set_visible, 0, 1, [self.entry_damaged_front, True]),
            (self.schedule_add, 0, 1, [self.smoke_big, True]),
            (self.schedule_add, 0, 1, [self.smoke_puff, True]),
            # MONSTA!
            (self.schedule_add, 2, 1, [self.monster, True]),
            ]

        t = 0
        for s in sequence:
            func, delta, life, args = s
            t += delta
            id = State.clock.schedule_interval(func, t, life, args)
            if __debug__: print 'headquarter, scheduling: ', t, s, id

        self.ents = [facility,
                    self.door,
                    self.door_damaged,
                    self.entry_front,
                    self.entry_back,
                    self.flash,
                    self.flash_up,
                    self.entry_damaged_front,
                    self.entry_damaged_back,
                    self.front_wall,
                    # self.shaker,
        ]

    # def shake(self, *args):
        # self.shaker.shake()

    def comicstring(self, *args):
        if __debug__: print 'C-string:', args
        unused, game, x, y, relx, rely, duration, image, dirx, diry = args
        class E(object):
            def __init__(self, x, y):
                self.position = gummworld2.Vec2d(x, y)
        e = entity.ComicString(game, E(x, y), relx, rely, duration, image, dirx, diry)
        e.position.x = x
        e.position.y = y


    def done(self, *args):
        self.intro.next()

    def schedule_add(self, *args):
        u, ent, arg = args
        if hasattr(ent, 'anim'):
            ent.anim.play()
        self.add_entity(ent)

    def set_visible(self, *args):
        print '????', pygame.time.get_ticks(), args
        unused, ent, arg = args
        ent.set_visible(arg)
        if ent is self.entry_damaged_front:
            sound.bomb(3)

    def add_entity(self, ent):
        self.ents.append(ent)
#        if ent is self.monster:
#            sound.monster_laugh(1)

    def remove_entity(self, ent):
        if ent in self.ents:
            self.ents.remove(ent)

    def update(self, dt):
        for e in self.ents:
            e.update(dt)

    def draw(self, surf):
        surf.fill((0,0,0))
        for e in sorted(self.ents, key = lambda e: e.z):
            if isinstance(e, entity.ComicString):
                surf.blit(e.image, e.position)
            else:
                e.draw(surf)

class TextContext(context.Context):

    def __init__(self, intro, text, font):
        self.intro = intro
        # render_with_border(text, font, fg_color, bg_color, border_color, border_width=1, colorkey = -1):
#        self.image = fonts.render_with_border(text, font, (0, 230, 230), (0, 200, 255), (255, 255, 0), 2)
        fg_color,bg_color,border_color = settings.title_color
        self.image = fonts.render_with_border(
            text, font, fg_color, bg_color, border_color, 25)
        # self.image = font.render(self.text, 0, (255, 255, 255))
        self.rect = self.image.get_rect(center=(settings.resolution[0] // 2, settings.resolution[1] // 2))

    def enter(self):
        State.clock.schedule_interval(self.done, 4, 1)

    def done(self, *args):
        # use a slide effect if you dont like the fade out
        context.push(transitions.Transition(BlackScreen(self.intro, True), FadeOutEffect(1, settings.resolution)))

    def draw(self, surf):
        surf.blit(self.image, self.rect)



class Intro(gummworld2.Engine):

    def __init__(self):
        self.tracker = gummworld2.model.Object()  # for camera tracking
        self.tracker.position.x = settings.resolution[0] // 2
        self.tracker.position.y = settings.resolution[1] // 2
        gummworld2.Engine.__init__(self,
            resolution=settings.resolution,
            update_speed=25,
            frame_speed=25,
            camera_target=self.tracker,
        )
        self.is_in_title_screen = False
        pygame.display.set_caption('{0} - {1} Mode'.format(
            settings.game_title,
            'Story' if settings.game_mode==settings.STORY_MODE else 'Arcade'))

    def enter(self):
        context.push(BlackScreen(self))
        new_cont = HeadQuarter(self)
        context.push( transitions.Transition(new_cont, FadeOutEffect(1, settings.resolution)))

    def update(self, dt):
        sound.update()
        top_cont = context.top()
        if top_cont != self:
            top_cont.update(dt)

    def draw(self, dt):
        State.screen.clear()
        top_cont = context.top()
        if top_cont != self:
            top_cont.draw(self.screen.surface)

        # State.screen.blit(self.text_img, self.text_rect)
        State.screen.flip()

    def action_main_menu(self):
        self.next()

    def on_key_down(self, unicode, key, mod):
        if key in (pygame.K_SPACE, pygame.K_RETURN, pygame.K_ESCAPE) and not self.is_in_title_screen:
            self.next()

    def on_mouse_button_down(self, pos, button):
        pass
        # self.next()

    def on_quit(self):
        self.next()

    def next(self):
        font = pygame.font.Font(None, 80)
        new_cont = TextContext(self, settings.game_title, font)

        WINDOW_SIZE = settings.resolution
        DURATION = 1.0
        # effects_list = [
                    # effects.Slide(DURATION, WINDOW_SIZE[0], effects.Slide.LEFT),
                    # effects.Slide(DURATION, WINDOW_SIZE[0], effects.Slide.RIGHT),
                    # effects.Slide(DURATION, WINDOW_SIZE[1], effects.Slide.UP),
                    # effects.Slide(DURATION, WINDOW_SIZE[1], effects.Slide.DOWN),
                    # effects.Slide(DURATION, WINDOW_SIZE[0], effects.Slide.LEFT, False),
                    # effects.Slide(DURATION, WINDOW_SIZE[0], effects.Slide.RIGHT, False),
                    # effects.Slide(DURATION, WINDOW_SIZE[1], effects.Slide.UP, False),
                    # effects.Slide(DURATION, WINDOW_SIZE[1], effects.Slide.DOWN, False),
                    # effects.Slide(DURATION, WINDOW_SIZE[0], effects.Slide.LEFT, True, False),
                    # effects.Slide(DURATION, WINDOW_SIZE[0], effects.Slide.RIGHT, True, False),
                    # effects.Slide(DURATION, WINDOW_SIZE[1], effects.Slide.UP, True, False),
                    # effects.Slide(DURATION, WINDOW_SIZE[1], effects.Slide.DOWN, True, False),
                  # ]

        self.push(transitions.Transition(new_cont, effects.Slide(DURATION, WINDOW_SIZE[0], effects.Slide.LEFT, False)))
        self.is_in_title_screen = True

    def goto_mainmenu(self):
        self.pop(10, False)
        pygame.display.set_caption('{0} - {1} Mode'.format(
            settings.game_title,
            'Story' if settings.game_mode==settings.STORY_MODE else 'Arcade'))
        if settings.game_mode == settings.STORY_MODE:
            ## story mode
            m = scenemanager.SceneManager()
            for i,s in enumerate(State.scores):
                if s[0] == 0:
                    m.set_level(i+1)
                    break
            self.push(m)
        else:
            ## arcade mode
            self.push(mainmenu.MainMenu())

