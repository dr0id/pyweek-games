"""credits.py - a context for displaying a game's credits

This file is part of Gummworld2.

Gummworld2 is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gummworld2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Gummworld2.  If not, see <http://www.gnu.org/licenses/>.
"""

import math

import pygame

import gummworld2

import settings, sound
import animation
import savegame


def sign(num):
    if num < 0: return -1.0
    return 1.0

font = pygame.font.Font(gummworld2.data.filepath('font', settings.fonts.credits), 84)


class Wheel(object):

    def __init__(self, texts, center, rotation_speed, radius, start_angle=0):
        self.texts = texts
        self.rotation_speed = rotation_speed
        self.delta_angle = 360 / len(self.texts) * sign(self.rotation_speed)
        self.start_angle = start_angle
        self.angle = 90 + self.start_angle * self.delta_angle
        self.center = center
        self.radius = radius
        
    def update(self, dt):
        self.angle += self.rotation_speed * dt
        
    def draw(self, dt, surf):
        color = (200, 200, 200)
        back_color = (60, 0, 60)
        for idx, text in enumerate(self.texts):
            if text:
                angle = self.angle - idx * self.delta_angle
                fsurf = font.render(text, True, color, back_color)
                tsurf = pygame.transform.rotozoom(fsurf, angle - 90 * sign(self.rotation_speed), 1)
                r = self.radius + fsurf.get_rect().w // 2
                pos = ( r * math.sin(math.radians(angle)) + self.center[0], 
                        r * math.cos(math.radians(angle)) + self.center[1])
                surf.blit(tsurf, tsurf.get_rect(center = pos))

class Credits(gummworld2.Engine):
    
    
    def __init__(self):
        self.tracker = gummworld2.model.Object()  # for camera tracking
        self.tracker.position.x = settings.resolution[0] // 2
        self.tracker.position.y = settings.resolution[1] // 2
        gummworld2.Engine.__init__(self,
            resolution=settings.resolution,
            update_speed=25, 
            frame_speed=25,
            camera_target=self.tracker,
        )
        self.radius = int(settings.resolution[1] * 0.75 * 0.5)
        authors =   ["Cosmologicon",  "DR0ID", "Gummbum",   "Gumm, DR0ID", "" ,  "pyweek 14", "Team multifac"]
        jobs =      ["mad science", "Coding:",  "Coding:",     "Coding:", "Graphics", "copyright 2012", ""]
        rotation_speed = 10
        assert len(authors) == len(jobs)
        self.authors_center = (settings.resolution[0], settings.resolution[1] // 2)
        self.jobs_center = (0, settings.resolution[1] // 2)
        self.wheels = [ Wheel(authors, self.authors_center, -rotation_speed, self.radius, 1.5), 
                        Wheel(jobs, self.jobs_center, rotation_speed, self.radius, -0.5),]

        self.anims = [
                animation.woman_head,
                animation.general_head,
                animation.monsterhit,
                animation.scientist_head,
                animation.soldier_head
                ]
        self.current_anim = 0
        self.clock.schedule_interval(self.switch_anims, 3, life=0)
#        gummworld2.State.scores[gummworld2.State.current_level-1] = [1,1]
#        savegame.save()
        settings.game_mode = settings.ARCADE_MODE

    def on_key_down(self, unicode, key, mod):
        if key in (pygame.K_ESCAPE, pygame.K_SPACE, pygame.K_RETURN):
            self.on_quit()

    def on_mouse_button_down(self, pos, button):
        self.on_quit()

    def on_quit(self):
        self.clock.unschedule(self.switch_anims)
        self.pop()

    def update(self, dt):
        sound.update()
        for wheel in self.wheels:
            wheel.update(dt)
    
    def draw(self, dt):
        gummworld2.State.camera.interpolate(dt)
        self.screen.fill((0,0,0))
        for wheel in self.wheels:
            wheel.draw(dt, self.screen.surface)
        pygame.draw.circle(self.screen.surface, (60, 0, 60), self.jobs_center, self.radius + 2)
        pygame.draw.circle(self.screen.surface, (60, 0, 60), self.authors_center, self.radius + 2)
        
        self.anims[self.current_anim % len(self.anims)].draw(self.screen.surface, (0, settings.resolution[1] // 2 - 50))
        self.anims[(self.current_anim + 1) % len(self.anims)].draw(self.screen.surface, (settings.resolution[0]-100, settings.resolution[1] // 2 - 50))
            
        self.screen.flip()

    def switch_anims(self, *args):
        self.current_anim += 1


