import random
import math

import pygame

import gummworld2
import settings, animation, spritesheet, sound, util


class Entity(object):
    vx = 0  # horizontal speed
    y = 0   # vertical position
    w = 50    # width
    h = 100   # height
    color0 = 255, 255, 255  # outline color
    color1 = 128, 128, 128  # fill color
    anchor = "midbottom"    # can be bottom or center
    anim = None             # anim takes precedence in update and draw
    images = None           # list of images from a spritesheet (no animation)
    image = None            # current image
    imagename = None        # for non-animated sprites
    animfactor = 1          # relative speed of the animation
    clickable = True
    def __init__(self, game, x, vx=None, y=None, anchor=None):
        self.game = game
        self.position = gummworld2.Vec2d(x, self.y)
        if y is not None:
            self.position.y = y
        if anchor is not None:
            self.anchor = anchor
        self.z = self.y
        if vx is not None:
            self.vx = vx
        if self.image is None and self.imagename is not None:
            filename = gummworld2.data.filepath('image', self.imagename)
            self.image = pygame.image.load(filename)
            self.image = pygame.transform.smoothscale(self.image, (self.w, self.h))

    def update(self, dt):
        self.position.x += self.vx * dt
        if self.anim:
            self.anim.think(dt * self.animfactor)

    def screenpos(self):
#        if self.__class__.__name__ == 'Monster':
#            print '###',self.__class__.__name__,self.position,gummworld2.State.camera.world_to_screen(self.position),gummworld2.State.camera.rect
        return gummworld2.State.camera.world_to_screen(self.position)
    
    def getrect(self):
        rect = pygame.Rect(0, 0, self.w, self.h)
        setattr(rect, self.anchor, self.screenpos())
        return rect

    def draw(self, surf, highlight = False):
        if __debug__:  # draw hit rectangle
            r = self.getrect()
            surf.fill(self.color1, r)
            pygame.draw.rect(surf, self.color0, r, 1)
        if self.anim:
            rect = self.anim.image().get_rect()
            setattr(rect, self.anchor, self.screenpos())
            self.anim.draw(surf, rect)
        elif self.image:
            rect = self.image.get_rect()
            setattr(rect, self.anchor, self.screenpos())
            img = util.highlight(self.image) if highlight else self.image
            surf.blit(img, rect)

    def collidemonster(self, monster):
        pass
    
    def destroy(self):
        pass


class Mailbox(Entity):
    y = -40
    w = 30
    h = 60
    color1 = 100, 50, 0

class Lamppost(Entity):
    y = -40
    w = 60
    h = 300
    color1 = 144, 144, 0
    
    def __init__(self, game, x):
        Entity.__init__(self, game, x)
        # load image from spritesheet
        filename = gummworld2.data.filepath('image', 'lamppost.png')
        ss = spritesheet.SpriteSheet(filename)
        size = 20,self.h
        self.images = ss.load_strip(size, 2, colorkey=(0,0,0))
        self.image = self.images[0]
        self.has_bulb = True
    def loose_bulb(self):
        self.image = self.images[1]
        self.has_bulb = False


class Hydrant(Entity):
    y = -30
    w = 50
    h = 80
    color1 = 200, 0, 0
    
    def __init__(self, game, x):
        Entity.__init__(self, game, x)
        # load image from spritesheet
        filename = gummworld2.data.filepath('image', 'fire_hydrant.png')
        self.image = pygame.image.load(filename)
        self.image.set_colorkey(self.image.get_at((0,0)))

class Crate(Entity):
    y = 50
    w = 120
    h = 120
    color1 = 120, 90, 0
    
    def __init__(self, game, x, contents=None):
        Entity.__init__(self, game, x)
        # load image from spritesheet
        filename = gummworld2.data.filepath('image', 'crate.png')
        ss = spritesheet.SpriteSheet(filename)
        size = self.w,self.h
        self.images = ss.load_strip(size, 2, colorkey=-1)
        self.image = self.images[0]
        self.contents = contents
    
    def destroy(self):
        if self.contents:
            if __debug__: print 'Crate: drops', self.contents
            if self.contents is Civilian:
                self.game.add_entity(Civilian(self.game, self.position.x))
                ## bump up total number of civilians
                self.game.civilians += 1
                self.game.civilians_survived += 1
            else:
                cont = self.contents()
                self.game.add_tool(cont)
                self.game.tipper.showtip("get" + self.contents.name)
                ComicString(self.game, self, 0, 0, 2, cont.get_image().copy())

# hi_image = pygame.transform.rotozoom(pygame.image.load(gummworld2.data.filepath('image', 'hi.png')), 0, 0.6)
haha_image = pygame.transform.rotozoom(pygame.image.load(gummworld2.data.filepath('image', 'hahaha.png')), 0, 0.6)
hi_image = haha_image
aaah_image = pygame.transform.rotozoom(pygame.image.load(gummworld2.data.filepath('image', 'aaah.png')), 0, 0.6)
bzzz_image = pygame.transform.rotozoom(pygame.image.load(gummworld2.data.filepath('image', 'bzzz.png')), 0, 0.6)
click_image = pygame.transform.rotozoom(pygame.image.load(gummworld2.data.filepath('image', 'click.png')), 0, 0.6)
grrr_image = pygame.transform.rotozoom(pygame.image.load(gummworld2.data.filepath('image', 'grrr.png')), 0, 0.6)
pam_image = pygame.transform.rotozoom(pygame.image.load(gummworld2.data.filepath('image', 'pam.png')), 0, 0.6)
censor_image = pygame.transform.rotozoom(pygame.image.load(gummworld2.data.filepath('image', 'censor-strip.png')), 0, 1.0)

class ComicString(Entity):
    w = 100
    h = 100
    clickable = False

    def __init__(self, game, ent, rel_x, rel_y, duration, image, dirx=None, diry=None):
        Entity.__init__(self, game, ent.position.x)
        self.image = image
        self.ent = ent
        self.relative = gummworld2.Vec2d(rel_x, rel_y)
        dirx = 0 if dirx is None else dirx
        diry = -50 if diry is None else diry
        self.direction = gummworld2.Vec2d(dirx, diry)
        self.duration = duration
        self.current_time = duration
        gummworld2.State.clock.schedule_interval(self.time_up, duration, 1)
        if __debug__: print 'ComicString direction:', self.direction
        if __debug__: print 'ComicString schedule:', self
        self.z = 100
        self.game.add_entity(self)
        
    def update(self, dt):
        factor = self.current_time / self.duration
        self.current_time -= dt
        if self.current_time >= 0:
            self.position = self.ent.position + self.relative + self.direction * (1.0 - factor * factor)
            self.image.set_alpha(int(255.0 * factor))
        
    def time_up(self, *args):
        if __debug__: print 'ComicString remove:', self
        self.game.remove_entity(self)

            
class Civilian(Entity):
    vx = 50
    y = 0
    w = 100
    h = 60
    clickable = False
    def __init__(self, game, x, vx=None, y=None, anchor=None):
        Entity.__init__(self, game, x, vx, y, anchor)
        self.anim = random.choice((
            animation.civilian11walkright,
            animation.civilian12walkright,
            animation.civilian21walkright,
            animation.civilian22walkright,
        ))
        self.y += random.choice((-2,-1,0,1,2))
        self.position.y = self.y
    def collidemonster(self, monster):
        if __debug__: print 'Civilian colliding monster', self.position.x - monster.position.x
        monster.collide_civilian(self)
        dist = self.position.x - monster.position.x
        # add random aaahh (dpending on dist??)
        if dist < 55:
            if random.random() < 0.1:
                ComicString(self.game, self, 0, -70, 2, aaah_image)
        if dist < 38:
            if __debug__: print 'Civilian caught by Monster!'
            sound.civilian()
            self.game.tipper.showtip("killciv")
            self.game.kill_civilian()
            self.game.remove_entity(self)
            ComicString(self.game, monster, 30, -3, 0.8, censor_image, 0, 0)
            ent = Blood(self.position.x, self.position.y + 10, animation.blood_d, self.game)
            self.game.add_entity(ent)
            

class Blood(Entity):
    clickable = False
    def __init__(self, x, y, anim, game):
        Entity.__init__(self, game, x)
        self.position.x = x
        self.position.y = y
        self.z = 50
        self.anim = anim
        self.anim.play()
        self.anim.event_loop_end.add(self.end)
        self.game = game
    def end(self, *args):
        self.game.remove_entity(self)
            
class Balloon(Entity):
    name = "balloon"
    vx = -30
    w = 60
    h = 100
    anchor = "midtop"
    color1 = 255, 0, 0
    y = -280
    t = 0
    imagename = "carlitosBalloons.png"
    def __init__(self, game, x, vx=None, y=None, anchor=None):
        Entity.__init__(self, game, x, vx, y, anchor)
        self.z = 1
    def update(self, dt):
        self.t += dt
        self.position.y = -280 + 30 * math.sin(self.t)
        Entity.update(self, dt)


# Something that when the monster touches it the level's over
class FinishLine(Entity):
    y = 30
    w = 300
    h = 300
    anchor = "bottomleft"
    clickable = False
    
    def __init__(self, game, x):
        Entity.__init__(self, game, x)
        # load image from spritesheet
        filename = gummworld2.data.filepath('image', 'lrg_Signs_Hazard_Warning.png')
        image = pygame.image.load(filename).convert()
        w = image.get_width() / 2
        h = image.get_height() / 2
        image = pygame.transform.smoothscale(image, (w,h))
        image.set_colorkey(image.get_at((0,0)))
        self.image = image
        self.w = w
        self.h = h
    
    def collidemonster(self, monster):
        self.game.finish()
