"""gummworld2.py - startup and runtime settings file

This file is part of Gummworld2.

Gummworld2 is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gummworld2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Gummworld2.  If not, see <http://www.gnu.org/licenses/>.

Usage
=====
Settings in this file should always be accessed like so:
    
    import settings
    if settings.whatever: ...
    settings.whatever = value

This insures that runtime changes will be noticed by other modules that import
the settings module. Of course, one can always make local copies of the values
if static behavior is desired.
"""

import os

import pygame

import gummworld2

## Game
game_title = "Lunatic's Arsenal"
title_color = (
    pygame.Color('white'),
    pygame.Color('purple'),
    pygame.Color('black'),
)
STORY_MODE = 0
ARCADE_MODE = 1
game_mode = STORY_MODE

gummworld2.State.scene_manager = None
gummworld2.State.scores = [
#   [survived,total]
    [0,0],  # level 1
    [0,0],  # level 2
    [0,0],  # level 3
    [0,0],  # level 4
    [0,0],  # level 5
]
# Seconds to wait before starting action in game. This gives player a moment to
# get oriented after a mission failure, or when the dialogue ends.
start_action_delay = 4.0

## Display
resolution = 1000,600        # screen size in pixels: width,height
fullscreen = False          # open fullscreen if True
ground = 0                # y-coordinate of ground

## Graphics
# Defaults already set for graphics asset paths. You can add new subdirs like so.
#gummworld2.data.set_subdir('myname', 'subdir')
gummworld2.data.set_subdir('toolimages', os.path.join('image','tools'))
gummworld2.data.set_subdir('intro', os.path.join('image','intro'))
gummworld2.data.set_subdir('scenery', os.path.join('image','scenery'))
gummworld2.data.set_subdir('save', 'save')
cacheframes = True      # True=cache animation frames; False=load from disk

class fonts:
    credits = "Bangers.ttf"
    toolnumbers = "YOUTTX__.ttf"
    dialogue = ["YOUTTX__.ttf", "BlackOpsOne.ttf", "IrishGrover.ttf", "Wallpoet-Regular.ttf"]
    cutscene = "SpecialElite.ttf"
    intro = "SpecialElite.ttf"
    pause = "Bangers.ttf"
gummworld2.data.set_subdir('dialogue', os.path.join('dialogue'))

## Sound
# Defaults already set for sound asset paths. You can add new subdirs like so.
gummworld2.data.set_subdir('music', 'music')

## Debug
profile = False                 # run cProfile if True
debug_monster_slowdown = True   # debug messages when monster is slowed

## Custom
# Gummbum's development preferences... remove or add a case for your own.
if os.environ.get('COMPUTERNAME',None) == 'BW-PC':
    if __debug__: print '## CUSTOM SETTINGS FOR BW (see settings.py)'

os.environ['SDL_VIDEO_CENTERED'] = '1'
