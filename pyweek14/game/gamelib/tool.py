# -*- coding: utf-8 -*-
import pygame
import gummworld2
import settings

import entity
from entity import *
import monster


image_files = dict(
    feather    = 'feather.png', 
    glue       = 'glue.png', 
    mallet     = 'mallet.gif', 
    wrench     = 'wrench-725.gif', 
    crowbar    = 'Crowbar.png', 
    toaster    = 'toaster.png', 
    toothpick  = 'johnnyautomaticgreenoliveonatoothpick.png', 
    rock       = 'toaster.png', 
    balloon    = 'toaster.png', 
    gun        = '', 
    glueblob   = 'white-splash-th.png', 
    lightbulb  = 'lightbulb.png', 
    puddle     = 'blob-th.png', 
    gluepuddle = '', 
)
for k,v in image_files.items():
    image_files[k] = gummworld2.data.filepath('toolimages',v)


class ToolEffect(entity.Entity):
    ## see tool.Crowbar for an example
    
    def __init__(self, game, worldpos, anim, anchor='center'):
        entity.Entity.__init__(self, game, worldpos.x, y=worldpos.y, anchor=anchor)
        self.anim = anim
        self.anim.event_loop_end.add(self.on_anim_end)
        game.add_entity(self)
    
    def update(self, dt):
        self.anim.think(dt)
    
    def on_anim_end(self, animation):
        self.game.remove_entity(self)


class FeatherEffect(entity.Entity):
    clickable = False
    def __init__(self, monster):
        entity.Entity.__init__(self, monster.game, 0, anchor='center')
        self.monster = monster
        img = pygame.image.load(
            gummworld2.data.filepath('toolimages', 'feather.png'))
        img.set_colorkey(img.get_at((0,0)))
        self.frame_length = 0.5
        self.elapsed = 0.0
        self.image_nr = 0
        self.images = [
            self.rotate(img, 0),
            self.rotate(img, 90),
        ]
        monster.game.add_entity(self)
        sound.tickle()
    
    @property
    def rect(self):
        i,r = self.images[self.image_nr]
        return r
    
    @staticmethod
    def rotate(image, angle):
        """rotate an image while keeping its center and size"""
        rot_image = pygame.transform.rotozoom(image, angle, 0.3)
        rot_rect = rot_image.get_rect()
        rot_image.set_colorkey(image.get_at((0,0)))
        return rot_image,rot_rect
    
    def update(self, dt):
        self.elapsed += dt
        if self.elapsed > Tickler.duration:
            self.monster.game.remove_entity(self)
        self.image_nr = int(round(self.elapsed % self.frame_length * 2))
    
    def draw(self, surf, highlight=False):
        img,rect = self.images[self.image_nr]
        rect = self.monster.getrect().midbottom
        surf.blit(img, rect)
    
    def screenpos(self):
        return gummworld2.State.camera.world_to_screen(self.rect.center)


# Cursor logic (what happens when you click on stuff)
class Tool(object):
    name = "tool"
    image = None
    cooldown = 0.0
    @classmethod
    def click(cls, game, worldpos, entity):
        if entity:
            if __debug__: print "Clicked on %s with %s" % (entity.__class__.__name__, cls.name)
        else:
            if __debug__: print "Clicked at %s with %s" % (worldpos, cls.name)
    
    @classmethod
    def get_image(cls):
        if cls.image is None and cls.name in image_files:
            image = pygame.image.load(image_files[cls.name]).convert(32)
            w = image.get_width()
            h = image.get_height()
            fx = 48
            fy = 48
            image = pygame.transform.smoothscale(image, (fx,fy))
            image.set_colorkey(image.get_at((0,0)))
            cls.image = image
        return cls.image
    

# tickles the monster to slow him down
class Feather(Tool):
    name = "feather"
    image = None
    cooldown = 5.0
    @classmethod
    def click(cls, game, worldpos, entity):
        if isinstance(entity, monster.Monster):
            if __debug__: print "Clicked on Monster with {0}".format(cls.name)
            game.add_entity(Tickler(game, worldpos))
            return True ## worked: game should start cooldown
        return False    ## failed: game should not start cooldown

# drops to the floor making a glue puddle
class Glue(Tool):
    name = "glue"
    image = None
    cooldown = 30.0
    @classmethod
    def click(cls, game, worldpos, entity):
        if not entity and worldpos.y < settings.ground:
            if __debug__: print "Clicked on Monster with {0}".format(cls.name)
            game.add_entity(GlueBlob(game, worldpos))
            return True ## worked: game should start cooldown
        return False    ## failed: game should not start cooldown

# can knock light bulbs from a lamppost
class Mallet(Tool):
    name = "mallet"
    image = None
    @classmethod
    def click(cls, game, worldpos, entity):
        if isinstance(entity, Lamppost):
            if entity.has_bulb:
                entity.loose_bulb()
                if __debug__: print "Clicked on Lamppost with {0}".format(cls.name)
                game.add_entity(Lightbulb(game, (entity.position.x, entity.position.y - 250)))
                ToolEffect(game, worldpos, animation.cut_b)
                sound.lightbulb()


# can be used to open a hydrant creating a water puddle
class Wrench(Tool):
    name = "wrench"
    image = None
    @classmethod
    def click(cls, game, worldpos, entity):
        if isinstance(entity, Hydrant):
            if __debug__: print "Clicked on hydrant with {0}".format(cls.name)
            game.remove_entity(entity)

            pos = (entity.position.x - Puddle.w//2, settings.ground - Puddle.h//2)
            r = pygame.Rect(pos[0], pos[1], Puddle.w, Puddle.h)
            toasters = [e for e in game.entities if isinstance(e, Toaster)]# and pygame.Rect(e.position.x, e.position.y, e.w, e.h).colliderect(r)]
            if __debug__: print 'found toasters:', len(toasters)
            if __debug__: print 'pos', pos, Puddle.w, Puddle.h, r
            for t in toasters:
                if __debug__: print '??', t.position.x, t.position.y, t.w, t.h
            if toasters:
                if __debug__: print '???? creating SHOCKING puddle'
                p = ShockingPuddle(game, pos)
            else:
                if __debug__: print '???? creating puddle'
                p = Puddle(game, pos)
            game.add_entity(p)
            ToolEffect(game, game.world_click, animation.cut_d)
            sound.wrench()

# destorys crates
class Crowbar(Tool):
    name = "crowbar"
    image = None
    @classmethod
    def click(cls, game, worldpos, entity):
        if isinstance(entity, Crate):
            game.remove_entity(entity)
            ToolEffect(game, game.world_click, animation.cut_a)
            sound.crate()

# causes a shock when it lands in a puddle
class Toaster(Tool):
    name = "toaster"
    image = None
    cooldown = 20.0
    @classmethod
    def click(cls, game, worldpos, entity):
        if worldpos.y < settings.ground:
            game.add_entity(FallingToaster(game, worldpos))
            return True ## worked: game should start cooldown
        return False    ## failed: game should not start cooldown

# destroys everything you click on
class Gun(Tool):
    name = "gun"
    image = None
    @classmethod
    def click(cls, game, worldpos, entity):
        game.remove_entity(entity)

# pops balloons
class Toothpick(Tool):
    name = "toothpick"
    image = None
    @classmethod
    def click(cls, game, worldpos, e):
        if isinstance(e, entity.Balloon):
            game.remove_entity(e)
            game.add_entity(Rock(game, (e.position.x, e.position.y + 50)))


# The manifestation of a tool that appears as an entity in the game
class FloatingTool(entity.Entity):
    vx = 0
    w = 30
    h = 30
    color0 = 255,   0,   0  # outline color
    color1 = 200, 200,   0
    name = "floatingtool"
    image_file = None
    image = None
    duration = 0  # set to some positive value if it's supposed to expire

    def __init__(self, game, pos):
        entity.Entity.__init__(self, game, pos[0])
        self.position = gummworld2.Vec2d(*pos)
        self.game = game
        self.z = game.monster.z - 1
        if self.duration:
            self.game.clock.schedule_interval(self.time_up, self.duration, life=1)

    def getrect(self):
        r = Entity.getrect(self)
        r.centery -= int(abs(self.position.x % 30 - 15) * 0.5)
        return r

    def update(self, dt):
        pass

    def time_up(self, *args):
        self.game.remove_entity(self)
        ## this erases cumulative effects
        self.game.monster.reset()
        
    def __str__(self):
        return "<tool.Tool '%s' object at %s>" % (self.name, hex(id(self)))
        
    __repr__ = __str__

class FallingTool(FloatingTool):
    name = "fallingtool"
    image = None
    g = 9.81 * 2500

    def __init__(self, game, pos):
        FloatingTool.__init__(self, game, pos)
        self.vy = 0
        self.play_sound()

    def play_sound(self):
        pass
    
    def land(self):
        self.vy = 0
        self.position.y = settings.ground

    def update(self, dt):
        if self.position.y < settings.ground:
            self.vy += 0.5 * self.g * dt * dt
            self.position.y += self.vy * dt

        if self.position.y >= settings.ground and self.vy > 0:
            self.land()

class GlueBlob(FallingTool):
    color1 = 200, 200, 200
    name = "glueblob"
    image = None
    def __init__(self, game, pos):
        file_name = gummworld2.data.filepath('toolimages', image_files[self.name])
        self.image = pygame.image.load(file_name)
        FallingTool.__init__(self, game, pos)
    def play_sound(self):
        sound.squish(0)
    def land(self):
        t0 = 2 + (self.vy / 220.) ** 2 
        FallingTool.land(self)
        self.game.remove_entity(self)
        self.game.add_entity(GluePuddle(self.game, self.position, t0))


class FallingToaster(FallingTool):
    color1 = 100, 100, 100
    name = "toaster"
    image = None
    angle = 0
    anchor = "center"
    z = 2
    def land(self):
        FallingTool.land(self)
        r = self.getrect()
        puddles = [e for e in self.game.entities if isinstance(e, Puddle) and e.getrect().colliderect(r)]
        if puddles:
            self.game.remove_entity(puddles[0])
            self.game.add_entity(ShockingPuddle(self.game, puddles[0].position))
        self.image = Toaster.get_image()
        self.image.set_alpha(None)
        self.image.set_colorkey(self.image.get_at((0,0)))
        self.anchor = "midbottom"


    def update(self, dt):
        FallingTool.update(self, dt)
        if self.vy:  
            self.angle += 300 * dt
            self.image = pygame.transform.rotate(Toaster.get_image(), self.angle)
            self.image.set_alpha(None)
            self.image.set_colorkey(self.image.get_at((0,0)))
    

class Lightbulb(FallingTool):
    name = "lightbulb"
    image = None
    imagename = "bulb.png"
    def __init__(self, game, pos):
        FallingTool.__init__(self, game, pos)
        self.vy = -200
        self.angle = 0
        self._orig_image = self.image
    def land(self):
        sound.lightbulb()
        self.game.remove_entity(self)
    def collidemonster(self, monster):
        monster.collide_lightbulb(self)
        if __debug__: print "Monster takes damage"
        self.game.remove_entity(self)
    def update(self, dt):
        FallingTool.update(self, dt)
        if self.vy:  
            self.angle += 300 * dt
            self.image = pygame.transform.rotate(self._orig_image, self.angle)
            self.image.set_alpha(None)
            self.image.set_colorkey(self.image.get_at((0,0)))
    

class Puddle(FloatingTool):
    # TODO: make that configurable
    name = "puddle"
    image = None
#    slowdown = .5
    slowdown = 0.97
    w = 120
    h = 50
    anchor = "center"
    color1 = 0, 0, 100
    duration = 5
    noisy = True
    def __init__(self, game, pos):
        FloatingTool.__init__(self, game, pos)
        file_name = gummworld2.data.filepath('toolimages', 'puddle.png')
        self.image = pygame.image.load(file_name)
        self.image.set_colorkey(self.image.get_at((0,0)))
        self.y -= 10
        self.position.y = self.y
    def collidemonster(self, monster):
        # cumulative slowdown
        monster.slowdown *= self.slowdown
        self.play_sound()
    def play_sound(self):
        if self.noisy:
            sound.liquid(0)
            self.noisy = False

class ShockingPuddle(Puddle):
    slowdown = 0.3
    name = "shocking puddle"
    sound_channel = None
#    triggered = False
    def collidemonster(self, monster):
#        if not self.triggered:
#            monster.collide_shocking_puddle(self)
#            self.triggered = True
        monster.collide_shocking_puddle(self)
        self.play_sound()
    def play_sound(self):
        if self.sound_channel is None or not self.sound_channel.get_busy():
            self.sound_channel = sound.liquid(2)
    
        
class GluePuddle(Puddle):
    name = "gluepuddle"
    image = None
    color1 = 200, 200, 200
    slowdown = 0.3
    duration = 0
    noisy = True
    h = 20
    def __init__(self, game, pos, t0=4):
        FloatingTool.__init__(self, game, pos)
        file_name = gummworld2.data.filepath('toolimages', 'glue_puddle.png')
        self.image = pygame.image.load(file_name)
        self.image.set_colorkey(self.image.get_at((0,0)))
        self.y -= 10
        self.position.y = self.y
        self.t = t0
        self.alpha = 255
    def update(self, dt):
        Puddle.update(self, dt)
        self.t -= dt
        if self.t <= 0:
            self.time_up()
        self.w = int(60 + 20 * self.t)
        self.alpha = min(max(0, self.t * 128), 255)
    def collidemonster(self, monster):
        # cumulative slowdown
        monster.slowdown *= self.slowdown
        self.play_sound()
    def play_sound(self):
        if self.noisy:
            sound.liquid(1)
            self.noisy = False
    def draw(self, surf, highlight = False):
        rect = pygame.Rect(0, 0, self.w, self.h)
        setattr(rect, self.anchor, self.screenpos())
        img = pygame.transform.scale(self.image, (self.w, self.h))
        img.set_alpha(int(self.alpha))
        surf.blit(img, rect)

class Tickler(FloatingTool):
    name = 'tickler'
    image = None
#    slowdown = 0.3
    slowdown = 0.95
    anchor = "center"
    duration = 1.5
    def collidemonster(self, monster):
        # cumulative slowdown
        monster.slowdown *= self.slowdown
        for e in monster.game.entities:
            if isinstance(e, FeatherEffect):
                return
        FeatherEffect(monster)
        entity.ComicString(monster.game, monster, 0, -60, 1, entity.hi_image)
        gummworld2.State.clock.schedule_interval(_createHaHa, 0.1, 1, [monster.game, monster, 0, -60, 1])
        gummworld2.State.clock.schedule_interval(_createHaHa, 0.3, 1, [monster.game, monster, 0, -60, 1])
        gummworld2.State.clock.schedule_interval(_createHaHa, 0.5, 1, [monster.game, monster, 0, -60, 1])
        gummworld2.State.clock.schedule_interval(_createHaHa, 0.7, 1, [monster.game, monster, 0, -60, 1])
        gummworld2.State.clock.schedule_interval(_createHaHa, 1.0, 1, [monster.game, monster, 0, -60, 1])
        
def _createHaHa(self, *args):
    if __debug__: print '????', args
    game, ent, x, y, dur = args
    entity.ComicString(game, ent, x, y, dur, entity.hi_image)




class Rock(FallingTool):
    name = "rock"
    image = None
    imagename = "AngeloGemmistone.png"
    slowdown = 0.5
    def __init__(self, game, pos):
        FallingTool.__init__(self, game, pos)
    def land(self):
        self.game.remove_entity(self)
        sound.rock()
    def collidemonster(self, monster):
        monster.collide_rock(self)
        if __debug__: print "Monster takes damage"
        self.game.remove_entity(self)
    

