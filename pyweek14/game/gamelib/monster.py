# -*- coding: utf-8 -*-
import random

import pygame
import gummworld2
import settings

import entity
from entity import Entity
import animation
import sound

class MonsterEffect(Entity):

    def __init__(self, game, monster, anim, pos, disappear=True):
        entity.Entity.__init__(self, game, pos[0])
        self.monster = monster
        self.anim = anim
        self.pos = list(pos)
        self.anim.event_loop_end.add(self.on_anim_end)
        self.disappear = disappear
        self.game.add_entity(self)
        self.z = monster.z + 1 # always in front of monster
        if __debug__: print 'effect init'
        
    def update(self, dt):
        self.position.x = self.monster.position.x + self.pos[0]
        self.position.y = self.monster.position.y + self.pos[1]
        self.anim.think(dt)
        
    def draw(self, surf, highlight=False):
        rect = self.anim.image().get_rect(center = gummworld2.State.camera.world_to_screen(self.position))
        
        self.anim.draw(surf, rect)
        
    def on_anim_end(self, animation):
        if self.disappear:
            self.game.remove_entity(self)

class Monster(entity.Entity):
    vx0 = 100
    color1 = 0, 100, 0
    slowdown = 1.0
    anim = animation.monsterwalkright

    def __init__(self, game, x, vx=None):
        entity.Entity.__init__(self, game, x, vx)
        if vx is not None:
            self.vx0 = vx
        self.reschedule()
        
        self.active_shock = None
    
    def update(self, dt):
        if __debug__ and self.slowdown < 1.0 and settings.debug_monster_slowdown:
            print 'Monster: slowed {0:0.2f}'.format(self.slowdown)
        self.vx = self.vx0 * self.slowdown
        self.animfactor = min(abs(self.vx) / 25, 5)
        entity.Entity.update(self, dt)
        
    def reset(self, *args):
        self.anim = animation.monsterwalkright
#        self.vx0 = Monster.vx0
        self.slowdown = 1.0

    # add such a double dispatch collision function for each entity affecting him
    def collide_lightbulb(self, bulb):
        # do whatever effect it has on the monster in here
        if __debug__: print "Monster takes damage from bulb!!"
        self.anim = animation.monsterhit
#        self.game.clock.schedule_interval(self.reset , 0.75, life=1)
        self.game.clock.schedule_interval(self.reset , 1.5, life=1)
#        self.vx0 = 0
        self.slowdown = 0.0
        MonsterEffect(self.game, self, animation.blood_a, (0,-75))
        sound.lightbulb()
        sound.monster_hit()

    def reset_shocking_puddle(self, *args):
        self.active_shock = False
        self.reset()

    def collide_shocking_puddle(self, puddle):
        if __debug__: print "Monster shocked by electric puddle!!"
        if not self.active_shock:
            ## really reward this tactic: stun 10 seconds
            self.game.clock.schedule_interval(self.reset_shocking_puddle, 10, life=1)
            self.active_shock = True
        self.anim = animation.monsterhit
        self.slowdown = 0.0
        for e in self.game.entities:
            if isinstance(e, MonsterEffect) and e.anim == animation.bolt_tesla:
                return
        self.active_shock = MonsterEffect(self.game, self, animation.bolt_tesla, (0,-75))
        
    def collide_rock(self, rock):
        self.anim = animation.monsterhit
        self.game.clock.schedule_interval(self.reset , 2, life=1)
#        self.vx0 = 0
        self.slowdown = 0.0
        MonsterEffect(self.game, self, animation.blood_a, (0,-75))
        
    def collide_civilian(self, civilian):
        if random.random() < 0.041:
            entity.ComicString(self.game, self, 0, -80, 2, entity.grrr_image)

    def reschedule(self, *args):
        entity.ComicString(self.game, self, 0, -80, 1, entity.grrr_image)
        gummworld2.State.clock.schedule_interval(self.reschedule, random.randint(3, 10), 1)

