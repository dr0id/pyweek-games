import pygame

import gummworld2

import menu
import settings
import scenemanager
import levels
import credits


class MainMenu(menu.Menu):
    
    def __init__(self):
        menu.Menu.__init__(self, self._gen_entries())
        pygame.display.set_caption('{0} - {1} Mode'.format(
            settings.game_title,
            'Story' if settings.game_mode==settings.STORY_MODE else 'Arcade'))
    
    def resume(self):
        menu.Menu.resume(self)
        entries = self._gen_entries()
        print '###############',entries[1]
        self.set_entries(entries)
    
    def _gen_entries(self):
        State = gummworld2.State
        return [
            ('NEW GAME', self.action_new_game),
            ('LEVEL 1 - Score {0[0]:d}/{0[1]:d}'.format(State.scores[0]), self.action_play_1),
            ('LEVEL 2 - Score {0[0]:d}/{0[1]:d}'.format(State.scores[1]), self.action_play_2),
            ('LEVEL 3 - Score {0[0]:d}/{0[1]:d}'.format(State.scores[2]), self.action_play_3),
            ('LEVEL 4 - Score {0[0]:d}/{0[1]:d}'.format(State.scores[3]), self.action_play_4),
            ('LEVEL 5 - Score {0[0]:d}/{0[1]:d}'.format(State.scores[4]), self.action_play_5),
            ('CREDITS',   self.action_show_credits),
            ('QUIT GAME', self.action_quit_game),
        ]
    
    def action_new_game(self):
        ## wipe previous progress / score
        self.push(scenemanager.SceneManager())
    
    def action_play_1(self):
        gummworld2.State.current_level = 1
        self.push(levels.Level01())
    
    def action_play_2(self):
        gummworld2.State.current_level = 2
        self.push(levels.Level02())
    
    def action_play_3(self):
        gummworld2.State.current_level = 3
        self.push(levels.Level03())
    
    def action_play_4(self):
        gummworld2.State.current_level = 4
        self.push(levels.Level04())
    
    def action_play_5(self):
        gummworld2.State.current_level = 5
        self.push(levels.Level05())
    
    def action_show_credits(self):
        gummworld2.State.current_level = 6
        self.push(credits.Credits())
    
    def action_quit_game(self):
        self.pop()
