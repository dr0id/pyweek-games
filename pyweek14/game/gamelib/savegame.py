import os

import gummworld2


def save_file_name():
    return gummworld2.data.filepath('save', 'save.txt')

def save():
    file_name = save_file_name()
    scores = gummworld2.State.scores
    if not os.path.exists(file_name):
        directory = os.path.split(file_name)[0]
        try:
            os.mkdir(directory) 
            print 'created save directory', directory
        except Exception as e:
            print 'could not create directory', directory
    
    try:
        f = open(file_name, 'wb')
    except Exception as e:
        print 'could not open {0} for writing'.format(file_name)
        print e
        return
    try:
        for best,total in scores:
            f.write('{0:d} {1:d}\n'.format(best, total))
    except e:
        print 'write failed for {0}'.format(file_name)
        print e
    f.close()


def load():
    file_name = save_file_name()
    if not os.access(file_name, os.F_OK):
        return
    scores = gummworld2.State.scores
    try:
        f = open(file_name, 'rb')
    except:
        return
    n = 0
    # should be one line for each level, two ints per line; space-delimited
    for line in f:
        scores[n] = [int(s) for s in line.split(' ')]
        n += 1
    f.close()
