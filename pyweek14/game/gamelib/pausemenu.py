import gummworld2

import menu


class PauseMenu(menu.Menu):
    
    def __init__(self):
        menu.Menu.__init__(
            self, [
                ('RESUME GAME', self.action_resume_game),
                ('RESTART LEVEL', self.action_restart_level),
                ('EXIT LEVEL', self.action_exit_level),
            ])
    
    def action_resume_game(self):
        self.pop()
    
    def action_restart_level(self):
        if gummworld2.State.scene_manager:
            ## pop 3: self, game, and scenemanager
            gummworld2.State.scene_manager.replay()
            self.pop(2, False)
        else:
            ## pop 2: self, game
            self.pop(2, False)
    
    def action_exit_level(self):
        if gummworld2.State.scene_manager:
            ## pop 3: self, game, and scenemanager
            self.pop(3, False)
        else:
            ## pop 2: self, game
            self.pop(2, False)
        
    def action_quit_game(self):
        self.action_resume_game()
