# Dialogue/tips

import pygame, gummworld2
import settings
import animation, textrect

# Tip logic
class Tipper(object):
    fonts = []
    seentips = set()  # tips that have already been shown once
    dialogue = {}  # loaded dialogue from the file
    textcache = {}  # cached renders
    def __init__(self, game):
        self.game = game
        self.static = animation.static
        self.static.play()
        self.z = 100
        self.loadtips()
        self.tipqueue = []
        self.anims = [
                animation.woman_head,
                animation.general_head,
                animation.scientist_head,
                animation.soldier_head
                ]
        self.tnext = 0   # time until the tip is advanced

    def loadtips(self):
        if self.dialogue: return
        for line in open(gummworld2.data.filepath("dialogue", "dialogue.txt"), "r"):
#            print line
            if line.startswith("#"): continue
            words = line.strip().split()
            if not words: continue
            tipname = words[0]
            if tipname not in self.dialogue:
                self.dialogue[tipname] = []
            self.dialogue[tipname].append((int(words[1]), " ".join(words[2:])))
#        print self.dialogue

    def showtip(self, tipname, repeat=False):
        # by default tips will be shown only once.
        if tipname in self.seentips:
            return
        if not repeat:
            self.seentips.add(tipname)
        empty = len(self.tipqueue) == 0
        self.tipqueue.extend(self.dialogue[tipname])
        if empty:
            self.scheduleadvance()

    def scheduleadvance(self):
## Gumm note: Hope you don't mind. I thought I'd try calculating the time based
## on words. It is working better for me. Not way too fast ortoo long. Feel free
## to work with it some more.
#        tlength = len(self.tipqueue[0])
#        self.tnext = 2.5 + 0.03 * tlength
        tlength = len(self.tipqueue[0][1].split(' '))
        self.tnext = 0.4 * tlength

    def advance(self, *args):
        self.tipqueue.pop(0)
        if self.tipqueue:
            self.scheduleadvance()
        else:
            self.tnext = 0

    def clear(self):
        del self.tipqueue[:]
        self.tnext = 0

    def getrect(self):
        return pygame.Rect(0, 0, 0, 0)
            
    def update(self, dt):
        if self.tnext:
            self.tnext = max(self.tnext - dt, 0)
            if self.tnext == 0:
                self.advance()
        self.static.think(dt)
            
    def draw(self, dt):
        # this whole thing can be replaced by an image that's made in Gimp or something
        surf = gummworld2.State.screen.surface
        # filled rectangle
        rect = pygame.Rect(0, 0, settings.resolution[0] - 40, 80)
        rect.bottomleft = 20, settings.resolution[1] - 20
        surf.fill((60, 0, 60), rect)
        # circle (put the scientist's head in this)
        p = 80, rect.centery
        pygame.draw.circle(surf, (60, 0, 60), p, 55)
        pygame.draw.circle(surf, (60, 60, 60), p, 50)

        if not self.tipqueue:
            self.static.draw(surf, (p[0]-50, p[1]-51))
            return
        if not self.fonts:
            for f in settings.fonts.dialogue:
                self.fonts.append(pygame.font.Font(gummworld2.data.filepath("font", f), 30))

        key = head, text = self.tipqueue[0]
        
        if key not in self.textcache:
            self.textcache[key] = textrect.render_textrect(text, self.fonts[head], 800, (255, 255, 255))

        self.anims[head].draw(surf, (p[0]-50, p[1]-51))
        self.static.draw(surf, (p[0]-50, p[1]-51))
        
        textimg = self.textcache[key]
        r = textimg.get_rect()
        r.midleft = 160, settings.resolution[1] - 60
        surf.blit(textimg, r)

    def __nonzero__(self):
        return bool(self.tipqueue)


