import gummworld2


import scene
import cutscenes
import levels
import credits
import mainmenu


scenes = [
    levels.Level01,
    levels.Level02,
    levels.Level03,
    levels.Level04,
    levels.Level05,
    credits.Credits,
]


class SceneManager(gummworld2.Context):
    
    def __init__(self):
        self.scene_nr = 0
        self.scene = None
        ## valid actions are 'reload', 'next'
        self.next_action = 'reload'
        gummworld2.State.scene_manager = self
    
    def enter(self):
        self._load_scene(scenes[self.scene_nr])
        if self.next_action is not 'playsingle':
            self.play_next()
    
    def resume(self):
        if self.next_action == 'next':
            self._load_next()
        elif self.next_action == 'reload':
            self._reload()
            self.play_next()
        elif self.next_action == 'playsingle':
            # we were called from mainmenu to play a single level
            self.pop()
    
    def exit(self):
        gummworld2.State.scene_manager = None
    
    def set_level(self, n):
        self.scene_nr = n-1
#        self.play_single()
    
    def play_next(self):
        self.next_action = 'next'
    
    def replay(self):
        self.next_action = 'reload'
    
    def play_single(self):
        self.next_action = 'playsingle'
    
    def _load_next(self):
        self.scene_nr += 1
        if self.scene_nr < len(scenes):
            self._load_scene(scenes[self.scene_nr])
        else:
            self.pop()
            self.push(mainmenu.MainMenu())
    
    def _reload(self):
        self._load_scene(scenes[self.scene_nr])
    
    def _load_scene(self, context_class):
        if __debug__: print 'SceneManager: loading', str(context_class.__name__)
        self.scene = scene.Scene(context_class)
        gummworld2.State.current_level = self.scene_nr + 1
        self.push(self.scene.get_context())
