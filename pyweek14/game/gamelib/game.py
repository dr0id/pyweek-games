import random

import pygame
from pygame.locals import *

import gummworld2
from gummworld2 import Vec2d

import entity
import monster
import settings
import sound
import fonts
#import tool
import tip
import animation
import pausemenu
import savegame

class Game(gummworld2.Engine):
    
    def __init__(self, entities, tools, levelnumber):
        self.tracker = gummworld2.model.Object()  # for camera tracking
        self.tracker.position.x = 100000
        gummworld2.Engine.__init__(self,
            update_speed=30, frame_speed=0,
            camera_target=self.tracker,
        )
        
        self.entities = []
        self.civilians = 0
        for ent_cls, args in entities:
            ent = ent_cls(self, *args)
            self.entities.append(ent)
            if isinstance(ent, monster.Monster):
                self.monster = ent
            elif isinstance(ent, entity.Civilian):
                self.civilians += 1
        self.civilians_survived = self.civilians
        self.tracker.position.x = self.monster.position.x + settings.resolution[0] * 0.45
        self.camera.init_position(self.tracker.position)
        
        self.tools = []
        self.tool_ready = {}
        self.toolrects = []
        for t in tools:
            self.add_tool(t)
        self.currenttool = self.tools[0]
        
        self.tool_labels = []
        font_file = gummworld2.data.filepath("font", settings.fonts.toolnumbers)
        font = pygame.font.Font(font_file, 33)
        for i in range(8):
            img = fonts.render_with_border(
                str(i+1), font, (255,155,0), (1,1,1), (0,0,0))
            rect = img.get_rect()
            img.set_alpha(140)
            self.tool_labels.append((img,rect))
        
        self.screen_click = None
        self.world_click = None
        
        self.tipper = tip.Tipper(self)
        self.tipper.showtip("level%s" % levelnumber)
        
        # Scenery
        self.background = pygame.image.load(
            gummworld2.data.filepath('scenery', 'scenery1.png'))
        self.background_rect = self.background.get_rect()
        
        self.paused = None
        self._monster_announce = True
        
        self.start_action_delay = settings.start_action_delay  # secs
        
        self.update(self.clock._update_interval)
    
    def enter(self):
        gummworld2.Engine.enter(self)
        pygame.display.set_caption('{0} - {1} Mode - Level {2}'.format(
            settings.game_title,
            'Story' if settings.game_mode==settings.STORY_MODE else 'Arcade',
            str(gummworld2.State.current_level)))
    
    def exit(self):
        pygame.display.set_caption('{0} - {1} Mode'.format(
            settings.game_title,
            'Story' if settings.game_mode==settings.STORY_MODE else 'Arcade'))
    
    def kill_civilian(self):
        self.civilians_survived -= 1
    
    def finish(self, *args):
        if __debug__:
            print 'Game: ------ Level finished ------'
            print 'Game: {0:d} / {1:d} civilians survived'.format(
                self.civilians_survived, self.civilians)
        if self.civilians_survived <= 0:
            if __debug__: print 'Game: REPLAY LEVEL'
            if gummworld2.State.scene_manager is not None:
                gummworld2.State.scene_manager.replay()
        else:
            if __debug__: print 'Game: NEXT LEVEL'
            if gummworld2.State.scene_manager is not None:
                gummworld2.State.scene_manager.play_next()
#        self.pop()
        ## 1. set up a paused callback: self.finished_update
        ## 2. create a view for blitting: self.view
        ## 3. render some text and get its rect: self.txt, self.txt_rect
        ## 4. save game score in gummworld2.State
        ## 5. pause the clock
        self.clock.paused_callback = self.finished_update
        self.view = gummworld2.View(self.screen.surface, Rect(250,100,500,200))
        font = pygame.font.Font(
            gummworld2.data.filepath('font',settings.fonts.pause), 35)
        self.txt = [
            font.render(
                '- Mission Succeeded -' if self.civilians_survived > 0 else '* Mission Failed *',
                True, (255,255,255)),
            font.render(
                'Surviving Civilians {0:d} of {1:d}'.format(
                    self.civilians_survived, self.civilians),
                    True, (255,255,255)),
        ]
        if self.civilians_survived == 0:
            if gummworld2.State.current_level > 2:
                self.txt.append(font.render('(See HELP.txt if you get stuck)', True, (255,255,255)))
            else:
                self.txt.append(font.render('(Help One Or More Escape!)', True, (255,255,255)))
        elif self.civilians_survived == self.civilians:
            self.txt.append(font.render('(Perfect! Commendation Awarded!)', True, (255,255,255)))
        else:
            self.txt.append(font.render('(Uh...who pays your salary?)', True, (255,255,255)))
        self.txt_rects = [t.get_rect(centerx=self.view.center[0]) for t in self.txt]
        scores = gummworld2.State.scores
        n = gummworld2.State.current_level
        # update high score only if it's improved
        old_survived,old_total = scores[n-1]
        if self.civilians_survived > old_survived:
            scores[n-1] = [self.civilians_survived,self.civilians]
        savegame.save()
        pygame.event.clear()
        self.clock.pause()
    
    def finished_update(self):
        ## this is end-of-level logic, called while clock is paused
        sound.update()
        view = self.view
        
        for e in pygame.event.get():
            # if e.type == MOUSEBUTTONDOWN or e.type == KEYDOWN:
            if e.type == KEYDOWN:
                # cheap transition: blank screen for a second, then exit context
                self.screen.clear()
                self.screen.flip()
                pygame.time.wait(1000)
                pygame.event.clear()
                self.pop()
#                self.clock.resume()
                return
        
        view.clear()
        pygame.draw.rect(view.surface, (255,255,255), view.rect, 1)
        ## drawing code in here; use view like a screen
        txt_h = self.txt[0].get_height()
        y = view.rect.centery - len(self.txt) * txt_h / 2
        for i,txt in enumerate(self.txt):
            rect = self.txt_rects[i]
            rect.centery = y + txt_h * i + 5*i
            view.blit(txt, rect)
        self.screen.flip()
        pygame.time.wait(30)
    
    def on_key_down(self, unicode, key, mod):
        if key == pygame.K_ESCAPE:
            self.push(pausemenu.PauseMenu())
        elif key in (K_1,K_2,K_3,K_4,K_5,K_6,K_7,K_8):
            i = key - K_1
            if i < len(self.tools):
                if self.currenttool is self.tools[i]:
                    self.on_mouse_button_down(pygame.mouse.get_pos(), 1)
                self.currenttool = self.tools[i]
        elif key == K_F12 and __debug__:
            self.finish()

    def on_quit(self):
        self.push(pausemenu.PauseMenu())

    # return the topmost entity at the given point
    def getpoint(self, pos):
        for e in sorted(self.entities, key = lambda e: -e.z):
            if e.clickable and e.getrect().collidepoint(pos):
                return e
        return None

    def on_mouse_button_down(self, pos, button):
        worldpos = gummworld2.State.camera.screen_to_world(pos)
        if button == 1:  # left click
            # store mouse position while button is pressed
            self.screen_click = Vec2d(pos)
            self.world_click = worldpos
            # get the tool if one is clicked
            for t, rect in self.toolrects:
                if rect.collidepoint(pos):
                    self.currenttool = t
                    return
            clicked = self.getpoint(pos)
            # call the click function for the current tool
            if self.currenttool and self.tool_ready[self.currenttool]:
                triggered = self.currenttool.click(self, worldpos, clicked)
                if triggered and self.currenttool.cooldown:
                    self.tool_ready[self.currenttool] = False
                    self.clock.schedule_interval(
                        self._enable_tool, self.currenttool.cooldown,
                        life=1, args=[self.currenttool])
    
    def on_mouse_button_up(self, pos, button):
        self.screen_click = None
        self.world_click = None
    
    def _enable_tool(self, dt, tool):
        self.tool_ready[tool] = True
    
    def add_entity(self, e):
        if e:
            # t.z = self.entities[0].z
            self.entities.append(e)
            # print self.entities
        
    def remove_entity(self, e):
        if e in self.entities:
            if __debug__: print 'game removing entitiy:', e
            e.destroy()
            self.entities.remove(e)
            # print self.entities
    
    def add_tool(self, t):
        i = len(self.tools)
        self.tools.append(t)
        self.tool_ready[t] = True
        self.toolrects.append((t, pygame.Rect(68 * i, 4, 64, 64)))
    
    def update(self, dt):
        sound.update()
        self.tipper.update(dt)
        if self.tipper:
            # start tips once when level starts
            self.pause_clock = pygame.time.Clock()
            self.clock.pause()
        else:
            self.monster_announce()
        
        if self.start_action_delay == settings.start_action_delay:
            self.start_action_delay -= dt
        elif self.start_action_delay > 0.0:
            self.start_action_delay -= dt
            ## ARGH STUPID BUG
            self.camera.init_position(self.tracker.position)
            return
        
        mrect = self.monster.getrect()
        for e in self.entities:
            if mrect.colliderect(e.getrect()):
                e.collidemonster(self.monster)

        for e in self.entities:
            e.update(dt)

        px = self.tracker.position.x + dt * self.monster.vx0 * 0.9
        px = max(px, self.monster.position.x + 0)
        px = min(px, self.monster.position.x + settings.resolution[0] * 0.45)
#        print px,self.tracker.position.x,dt,self.monster.position.x,self.monster.vx0
#        if abs(px) > 100:
#            quit()
        self.tracker.position.x = px

        self.tracker.position.y = self.monster.position.y - settings.resolution[1] * 0.21
        gummworld2.State.camera.update()
        
        if self.civilians_survived == 0:
            self.clock.schedule_interval(self.finish, 2.5)

    def paused_update(self):
        dt = 0.001 * self.pause_clock.tick(60)
        self.tipper.update(dt)
        for e in pygame.event.get():
            if e.type == KEYDOWN and e.key == K_SPACE:
                self.tipper.advance()
            if e.type == KEYDOWN and e.key == K_ESCAPE:
                self.tipper.clear()
            # if e.type == MOUSEBUTTONDOWN and e.button == 1:
                # self.tipper.advance()
        if not self.tipper:
            self.camera.init_position(self.camera.rect.center)
            self.clock.resume()
        self.draw(0)
    
    def monster_announce(self):
        # play monster sound once when action starts
        if self._monster_announce:
            if random.random() < 0.33:
                sound.monster_laugh()
            else:
                sound.monster()
            self._monster_announce = False
    
    def draw(self, dt):
        gummworld2.State.camera.interpolate(dt)
        
        self.draw_background()
        
        screen = self.screen.surface
        
        # highlight current object
        clicked = self.getpoint(pygame.mouse.get_pos())

        # draw entities
        for e in sorted(self.entities, key = lambda e: e.z):
            e.draw(screen, highlight = e is clicked)
        
        # draw tool rects
        font = pygame.font.Font(None, 24)
        for t, rect in self.toolrects:
            c = (255,155,0) if t is self.currenttool else (40,40,40)
#            pygame.draw.rect(screen, c, rect)
            screen.fill(c, rect)
            screen.fill((255,255,255), rect.inflate(-16,-16))
            pygame.draw.rect(screen, (100, 100, 100), rect, 2)
        
        # draw tools
        for i,(t,rect) in enumerate(self.toolrects):
            if t:
                if self.tool_ready[t]:
                    image = t.get_image()
                else:
                    image = t.get_image().copy()
                    image.set_alpha(75)
                trect = image.get_rect(center = rect.center)
                screen.blit(image, trect)
                label,lrect = self.tool_labels[i]
                lrect.center = trect.center
                screen.blit(label, lrect)
            else:
                toolname = str(t and t.name)
                c = 255 if t is self.currenttool else 144
                text = pygame.transform.rotozoom(font.render(toolname, True, (c, c, c)), 30, 1)
                trect = text.get_rect(center = rect.center)
                screen.blit(text, trect)

        self.tipper.draw(dt)

        self.screen.flip()
    
    def draw_background(self):
        self.screen.fill((0,0,50))
        camrect = gummworld2.State.camera.rect
        screen = gummworld2.State.screen
        bg = self.background
        bgrect = self.background_rect
        offsetx = camrect.right % camrect.w
        for x in range(camrect.x, camrect.right+camrect.w, bgrect.w):
            screen.blit(
                bg, (x-camrect.x-offsetx,0),
                bgrect.move((0,bgrect.h-camrect.h)))
