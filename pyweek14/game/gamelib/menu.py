import pygame

import gummworld2
from gummworld2 import State

import settings, sound


class ClickButton(pygame.sprite.Sprite):
    
    def __init__(self, text_img, y, action):
        self.image = text_img
        self.rect = text_img.get_rect(y=y, centerx=State.screen.rect.centerx)
        self.action = action
    
    def click(self, mouse_pos):
        if self.rect.collidepoint(mouse_pos):
            self.action()


class Menu(gummworld2.Engine):
    
    def __init__(self, entries):
        gummworld2.Engine.__init__(self,
            resolution=settings.resolution,
            update_speed=60, frame_speed=60,
        )
        
        self.buttons = []
        self.keyboard_focus = 0
        
        self.font_file = gummworld2.data.filepath('font', 'VeraBd.ttf')
        self.font = pygame.font.Font(self.font_file, 32)
        self.screen_centery = State.screen.rect.centery
        self.text_height = self.font.get_height()
        
        self.set_entries(entries)
    
    def set_entries(self, entries):
        del self.buttons[:]
        n = -(len(entries) / 2)
        for text,action in entries:
            text_img = self.font.render(text, True, pygame.Color('white'))
            self.buttons.append(ClickButton(
                text_img, self.screen_centery + n * self.text_height, action))
            n += 1
    
    def exit(self):
        ## Outro?
        pass
    
    def update(self, dt):
        sound.update()
        
    def on_key_down(self, unicode, key, mod):
        if key == pygame.K_ESCAPE:
            self.action_quit_game()
        elif key in (pygame.K_DOWN, pygame.K_RIGHT):
            self.keyboard_focus += 1
        elif key in (pygame.K_UP, pygame.K_LEFT):
            self.keyboard_focus -= 1
        elif key == pygame.K_RETURN:
            self.buttons[self.keyboard_focus].action()
        self.keyboard_focus %= len(self.buttons)


    def draw(self, dt):
        if not self.buttons:
            return
        State.screen.clear()
        for b in self.buttons:
            State.screen.blit(b.image, b.rect)
        surf = pygame.transform.rotozoom(self.buttons[self.keyboard_focus].image, 0, 1.25)
        rect = surf.get_rect(center = self.buttons[self.keyboard_focus].rect.center)
        State.screen.surface.fill((0, 0, 0), rect)
        State.screen.surface.blit(surf, rect)
        State.screen.flip()
    
    def on_mouse_button_down(self, pos, button):
        for b in self.buttons:
            b.click(pos)

    def on_mouse_motion(self, pos, rel, buttons):
        for idx, btn in enumerate(self.buttons):
            if btn.rect.collidepoint(pos):
                self.keyboard_focus = idx
                break
                
    def on_quit(self):
        self.action_quit_game()

