# -*- coding: utf-8 -*-

import string
import os

import pygame
from gummworld2 import data
from gummworld2 import events
import spritesheet, settings



class ImageCache(object):
    def __init__(self):
        self.spritesheets = {}  # Spritesheet cache, auto-populated when requested
        self.frames = {}  # Cache of frames of animation
        self.images = {}  # Cache of still images TODO: implement

    def getspritesheet(self, filename, convert_alpha=True, zoom=1.0, rotation=0.0):
        key = filename, convert_alpha, zoom, rotation
        if key not in self.spritesheets:
            map_file_name = string.rsplit(filename, ".", 1)[0] + ".txt"
            map_file_name = data.filepath('image', map_file_name)

            if os.path.exists(map_file_name):
                self.spritesheets[key] = spritesheet.SpritesheetWithMap(data.filepath('image', filename), map_file_name, convert_alpha, zoom, rotation)
            else:
                self.spritesheets[key] = spritesheet.SpriteSheet(data.filepath('image', filename), convert_alpha)
        return self.spritesheets[key]

    def getframe(self, filename, fnumber, image_count, size=None, mirror=False, convert_alpha=True, zoom=1.0, rotation=0.0):
        key = filename, convert_alpha, size, image_count, fnumber, mirror, zoom, rotation
        if key not in self.frames:
            sheet = self.getspritesheet(filename, convert_alpha, zoom, rotation)
            if isinstance(sheet, spritesheet.SpritesheetWithMap):
                self.frames[key] = sheet.image_from_frame(fnumber, (0, 0, 0))
            else:
                if size is None:
                    x,y = sheet.sheet.get_size()
                    size = x/image_count,y
                self.frames[key] = sheet.image_at((size[0]*fnumber, 0, size[0], size[1]), (0,0,0))
            if mirror:
                self.frames[key] = pygame.transform.flip(self.frames[key], True, False)
                self.frames[key].set_colorkey((0,0,0))
        return self.frames[key]

    def getimage(self, filename):
        if __debug__: print filename
        key = filename
        return None

imagecache = ImageCache()

class Animation(object):

    def __init__(self, filename, nframes=None, frames=None, size=None, fps=8, loop=True, mirror=False, zoom=1.0, rotation=0.0):
        self.loop = loop  # TODO: loop?
        self.playing = True

        self.frame_duration = 1.0 / fps
        self._time = 0.0
        self.index = 0

        if frames is None and nframes is None:
            frames, nframes = (0,), 1
        elif frames is None:
            frames = range(nframes)
        elif nframes is None:
            nframes = len(frames)
        self.nframes = nframes
        self.framekeys = [(filename, frame, nframes, size, mirror, True, zoom, rotation) for frame in frames]
        self.frames = None
        self.filename = filename
        self.event_loop_end = events.Signal()
        self.loadframes()

    def loadframes(self):
        self.frames = [imagecache.getframe(*key) for key in self.framekeys]

    def play(self):
        self.playing = True

    def pause(self):
        self.playing = False

    def image(self):
        if self.frames: return self.frames[self.index % self.nframes]
        key = self.framekeys[self.index % self.nframes]
        return imagecache.getframe(*key)

    def think(self, dt):
        if not self.playing: return
        if not self.frames and settings.cacheframes: self.loadframes()
        self._time += dt
        while self._time > 0:
            self._time -= self.frame_duration
            self.index += 1
            if self.index % self.nframes == self.nframes - 1:
                self.event_loop_end.fire(self)

    def draw(self, surface, position):
        surface.blit(self.image(), position)

# __init__(self, filename, nframes=None, frames=None, size=None, fps=8, loop=True, mirror=False):
## ver1: frames=(2,0,1,2,2,3,4,2), fps=4
## ver2: frames=(2,1,0,1,2,3,4,3), fps=4
## ver3: frames=(2,0,1,2,4,3), fps=3
#monsterwalkleft = Animation("monster_x5.png", frames=(2,0,1,2,4,3), fps=3, size=(80,100))
monsterwalkright = Animation("monster_x5.png", frames=(2,0,1,2,4,3), fps=3, size=(80,100), mirror=True)
civilian11walkright = Animation("civilian11.png", frames=(2,0,1,2,4,3), fps=3, size=(30,60))
civilian12walkright = Animation("civilian12.png", frames=(2,0,1,2,4,3), fps=3, size=(30,60))
civilian21walkright = Animation("civilian21.png", frames=(2,0,1,2,4,3), fps=3, size=(30,60))
civilian22walkright = Animation("civilian22.png", frames=(2,0,1,2,4,3), fps=3, size=(30,60))
monsterhit = Animation("monster_hit.png", frames=(0, ), fps=1, size=(80,100), mirror=True)
woman_head = Animation("heads.png", frames=(1, ), fps=1, size=(100,100))
general_head = Animation("heads.png", frames=(2, ), fps=1, size=(100,100))
scientist_head = Animation("heads.png", frames=(3, ), fps=1, size=(100,100))
soldier_head = Animation("heads.png", frames=(4, ), fps=1, size=(100,100))
static = Animation("static.png", frames=(0,1,2,3), fps=10, size=(100,100))

blood_a = Animation(os.path.join("spritesheets_powstudio", "blood_a_spritesheet.png"), nframes=6, fps=15, zoom=0.5)
blood_b = Animation(os.path.join("spritesheets_powstudio", "blood_b_spritesheet.png"), nframes=6, fps=15, zoom=1.0)
blood_c = Animation(os.path.join("spritesheets_powstudio", "blood_c_spritesheet.png"), nframes=6, fps=15, zoom=1.0)
blood_d = Animation(os.path.join("spritesheets_powstudio", "blood_d_spritesheet.png"), nframes=6, fps=15, zoom=1.0)
# bolt_ball = Animation(os.path.join("spritesheets_powstudio", "bolt_ball_spritesheet.png"), nframes=10, fps=15, zoom=1.0)
bolt_sizzle = Animation(os.path.join("spritesheets_powstudio", "bolt_sizzle_spritesheet.png"), nframes=10, fps=15, zoom=1.0)
# bolt_strike = Animation(os.path.join("spritesheets_powstudio", "bolt_strike_spritesheet.png"), nframes=10, fps=15, zoom=1.0)
bolt_tesla = Animation(os.path.join("spritesheets_powstudio", "bolt_tesla_spritesheet.png"), nframes=10, fps=15, zoom=1.0)
bolt_tesla_vert = Animation(os.path.join("spritesheets_powstudio", "bolt_tesla_spritesheet.png"), nframes=10, fps=15, zoom=1.0, rotation=90)
cut_a = Animation(os.path.join("spritesheets_powstudio", "cut_a_spritesheet.png"), nframes=5, fps=15, zoom=1.0)
cut_b = Animation(os.path.join("spritesheets_powstudio", "cut_b_spritesheet.png"), nframes=5, fps=15, zoom=1.0)
cut_c = Animation(os.path.join("spritesheets_powstudio", "cut_c_spritesheet.png"), nframes=5, fps=15, zoom=1.0)
cut_d = Animation(os.path.join("spritesheets_powstudio", "cut_d_spritesheet.png"), nframes=5, fps=15, zoom=1.0)
# fireball_hit_ = Animation(os.path.join("spritesheets_powstudio", "fireball_hit__spritesheet.png"), nframes=9, fps=15, zoom=1.0)
# fireball = Animation(os.path.join("spritesheets_powstudio", "fireball_spritesheet.png"), nframes=6, fps=15, zoom=1.0)
# flame_1 = Animation(os.path.join("spritesheets_powstudio", "flame_1_spritesheet.png"), nframes=6, fps=15, zoom=1.0)
# flamethrower = Animation(os.path.join("spritesheets_powstudio", "flamethrower_spritesheet.png"), nframes=28, fps=15, zoom=1.0)
# flash_a = Animation(os.path.join("spritesheets_powstudio", "flash_a_spritesheet.png"), nframes=6, fps=15, zoom=1.0)
# flash_b = Animation(os.path.join("spritesheets_powstudio", "flash_b_spritesheet.png"), nframes=6, fps=15, zoom=1.0)
# flash_c = Animation(os.path.join("spritesheets_powstudio", "flash_c_spritesheet.png"), nframes=6, fps=15, zoom=1.0)
# flash_d = Animation(os.path.join("spritesheets_powstudio", "flash_d_spritesheet.png"), nframes=6, fps=15, zoom=1.0)
smoke_jump = Animation(os.path.join("spritesheets_powstudio", "smoke_jump_spritesheet.png"), nframes=10, fps=15, zoom=1.0)
smoke_plume = Animation(os.path.join("spritesheets_powstudio", "smoke_plume_spritesheet.png"), nframes=10, fps=15, zoom=1.0)
smoke_puff_right = Animation(os.path.join("spritesheets_powstudio", "smoke_puff_right_spritesheet.png"), nframes=10, fps=15, zoom=1.0)
smoke_puff_up = Animation(os.path.join("spritesheets_powstudio", "smoke_puff_up_spritesheet.png"), nframes=10, fps=15, zoom=1.0)

if __name__ == '__main__':

    import pygame
    from pygame.locals import *
    pygame.init()
    screen = pygame.display.set_mode((600,600))
    clock = pygame.time.Clock()
    current_anim = 0
    animations = [  
                    civilian11walkright,
                    civilian12walkright,
                    civilian21walkright,
                    civilian22walkright,
                    monsterwalkright,
#                    monsterwalkleft,
                    blood_a,
                    blood_b,
                    blood_c,
                    blood_d,
                    # bolt_ball,
                    bolt_sizzle,
                    # bolt_strike,
                    bolt_tesla,
                    bolt_tesla_vert,
                    # cut_a,
                    # cut_b,
                    # cut_c,
                    # cut_d,
                    # fireball_hit_,
                    # fireball,
                    # flame_1,
                    # flamethrower,
                    # flash_a,
                    # flash_b,
                    # flash_c,
                    # flash_d,
                    smoke_jump,
                    smoke_plume,
                    smoke_puff_right,
                    smoke_puff_up,
                ]
    animation = animations[current_anim]

    while 1:
        for e in pygame.event.get():
            if e.type == KEYDOWN:
                if e.key == K_SPACE:
                    current_anim += 1
                    current_anim %= len(animations)
                    animation = animations[current_anim]
                    pygame.display.set_caption(animation.filename)
                elif e.key == K_ESCAPE:
                    quit()
        dt = clock.tick(32) / 1000.0
        screen.fill((0,0,0))
        animation.think(dt)
        animation.draw(screen, (0,0))
        pygame.display.flip()
