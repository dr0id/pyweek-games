import os
import random

import pygame

import gummworld2


songs = [
    'dark_city_amped.ogg',
    'a-goblin.it',
    'apb.it',
]

now_playing = -1

pygame.mixer.pre_init(44100, 0, 0, 64)
pygame.mixer.init()
pygame.mixer.music.set_volume(1.0)

def update():
    global now_playing
    if now_playing == -1 or not pygame.mixer.music.get_busy():
        now_playing += 1
        if now_playing >= len(songs):
            now_playing = 0
        path = gummworld2.data.filepath('music', songs[now_playing])
        pygame.mixer.music.load(path)
        pygame.mixer.music.play()

_monster = [
    ('LO5.ogg',0.2),
    ('monstermoan.ogg',0.2),
    ('72036__cmusounddesign__fp-monster_a.ogg',1.0),
    ('118703__teqstudios__evil-laughs-demonic-echos_a.ogg',0.6),
]

_monster_laugh = [
    ('deeplaugh.ogg',0.2),
    ('118703__teqstudios__evil-laughs-demonic-echos_e.ogg',0.6),
]

_monster_hit = [
    ('m_okay.ogg',0.5),
    ('72036__cmusounddesign__fp-monster_b.ogg',0.5),
    ('34137__sylvermonk__sylvermonk-cavegrunt.ogg',0.5),
]

_civilian_whacked = [
    ('118703__teqstudios__evil-laughs-demonic-echos_c.ogg',0.20),
    ('118703__teqstudios__evil-laughs-demonic-echos_d.ogg',0.20),
    ('118703__teqstudios__evil-laughs-demonic-echos_f.ogg',0.20),
    ('attack.ogg',0.4),
    ('Ayaah.ogg',0.5),
    ('Oops.ogg',0.10),
    ('SCREAM3.ogg',0.20),
    ('aud_chomp.ogg',0.5),
    ('burp2.ogg',0.4),
    ('chomp3.ogg',0.80),
    ('cry2.ogg',0.2),
    ('hic3.ogg',0.20),
    ('hic6.ogg',0.30),
    ('screamMale.ogg',0.2),
    ('waaaahh!.ogg',0.3),
]

_bomb = [
    ('quake3.ogg',1.0),
    ('Grenade2.ogg',1.0),
    ('bomb.ogg',0.6),
    ('bomb7.ogg',0.6),
]

_squish = [
    ('SQUISH11.ogg',0.7),
    ('88511__gagaman__squish_a.ogg',0.7),
]

_liquid = [
    ('walk_wat.ogg',0.2),           # water puddle
    ('Rubber-suit-01.ogg',0.6),     # glue puddle
    ('Electric-strobe-03.ogg',1.0), # shocking puddle
]

_tickle = [
    ('118703__teqstudios__evil-laughs-demonic-echos_b_amped.ogg',0.5),
    ('118703__teqstudios__evil-laughs-demonic-echos_g_amped.ogg',0.6),
    ('118703__teqstudios__evil-laughs-demonic-echos_h_amped.ogg',0.5),
]

_wrench = [
    ('1844__edwin-p-manchester__crossing-bar.ogg',1.0),
]

_lightbulb = [
    ('42906__freqman__large-light-bulb-break.ogg',0.20),
]

_crate = [
    ('115917__issalcake__weak-table-crash-break.ogg',0.2),
    ('throwknife.ogg',0.5),
]

_rock = [
    ('thunk.ogg',0.6),
]

_balloon = [
    ('Balloon-Dflate-01.ogg',0.8),
]

def _full_name(file_name):
    return gummworld2.data.filepath('sound', file_name)
def play_sound(file_name, volume):
    try:
        if __debug__: print 'sound.play_sound:',file_name,volume
        full_name = _full_name(file_name)
        s = pygame.mixer.Sound(full_name)
        s.set_volume(volume)
        return s.play()
    except:
        if __debug__: print file_name,'failed'

def _random_pick(choices, n=None):
    if n is None:
        name,vol = random.choice(choices)
    else:
        name,vol = choices[n]
    return _full_name(name),vol

def monster(n=None):
    file_name,volume = _random_pick(_monster, n)
    return play_sound(file_name, volume)
def monster_laugh(n=None):
    file_name,volume = _random_pick(_monster_laugh, n)
    return play_sound(file_name, volume)
def monster_hit(n=None):
    file_name,volume = _random_pick(_monster_hit, n)
    return play_sound(file_name, volume)
def civilian(n=None):
    file_name,volume = _random_pick(_civilian_whacked, n)
    return play_sound(file_name, volume)

def bomb(n=None):
    file_name,volume = _random_pick(_bomb, n)
    return play_sound(file_name, volume)
def wrench(n=None):
    file_name,volume = _random_pick(_wrench, n)
    return play_sound(file_name, volume)
def lightbulb(n=None):
    file_name,volume = _random_pick(_lightbulb, n)
    return play_sound(file_name, volume)
def tickle(n=None):
    file_name,volume = _random_pick(_tickle, n)
    return play_sound(file_name, volume)
def crate(n=None):
    file_name,volume = _random_pick(_crate, n)
    return play_sound(file_name, volume)
def rock(n=None):
    file_name,volume = _random_pick(_rock, n)
    return play_sound(file_name, volume)
def squish(n=None):
    file_name,volume = _random_pick(_squish, n)
    return play_sound(file_name, volume)
def liquid(n=None):
    file_name,volume = _random_pick(_liquid, n)
    return play_sound(file_name, volume)
def balloon(n=None):
    file_name,volume = _random_pick(_balloon, n)
    return play_sound(file_name, volume)


if __name__ == '__main__':
    import gummworld2
    gummworld2.data.set_subdir('music', 'music')
    pygame.init()
    screen = pygame.display.set_mode((400,400))
    for ls in (
#            _monster,
#            _monster_laugh,
#            _monster_hit,
#            _civilian_whacked,
#            _bomb,
#            _squish,
#            _liquid,
#            _tickle,
#            _wrench,
#            _lightbulb,
#            _crate,
#            _balloon,
            _rock,
            ):
        for name,volume in ls:
            chan = play_sound(name, volume)
            if chan:
                while chan.get_busy():
                    update()
                    pygame.time.wait(100)
