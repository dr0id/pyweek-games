# -*- coding: utf-8 -*-

"""
Various graphical effects for transitions using pygame.

"""

# import pyknicpygame
# from pyknicpygame import pyknic
# from pyknic import context
# from pyknic.context import transitions
from . import transitions

# # ----------------------------------------------------------------------------
# # ----------------------------------------------------------------------------
# slide dx, dy
# slide in
# slide out
# fade

# delete squares
# flip squares
# explode squares
# implode squares
# resolve squares
# fly through sqares
# fall down squares

# slide stripe (random)
# jalousie (flip stipe)
# delete stripe

# melt down

# zoom in (and fade)
# fly through

# rotate, one corner fixed, falling image
# rotate outwarts, both top corners fixed, split up in middle
# rotate and zoom out/in

# blizzard (turn white for a moment)

# fade using a template


# ------------------------------------------------------------------------------


class Slide(transitions.TransitionEffect):
    """
    Sliding effect.
    """

    LEFT = (-1, 0)
    RIGHT = (1, 0)
    DOWN = (0, 1)
    UP = (0, -1)

    def __init__(self, duration, distance, direction=LEFT, \
                                                move_old=True, move_new=True):
        """
        .. todo:: make it an exception if both move_old and move_new are False
        """
        transitions.TransitionEffect.__init__(self)
        self.duration = duration
        dirx, diry = direction
        self.dir_x = dirx
        self.dir_y = diry
        
        self.posx = 0
        self.posy = 0
        self.speed = distance / duration
        self.distance = distance
        self._duration = duration
        self.move_old = move_old
        self.move_new = move_new
        # TODO:  make this an exception!
        assert move_old or move_new, "moving neither is not allowd" 
        
    def enter(self):
        self.duration = self._duration
        self.posx = 0
        self.posy = 0
        
    def update(self, delta_time):
        self.posx += self.speed * self.dir_x * delta_time
        self.posy += self.speed * self.dir_y * delta_time
        self.duration -= delta_time
        if self.duration > 0:
            return False
        return True # done
        
    def draw(self, screen, old_context, new_context):
        screen_rect = screen.get_rect()
        old_rect = screen_rect.copy()
        new_rect = screen_rect.copy()
        
        old_surf = screen.copy()
        old_surf.fill((0, 0, 0, 0))
        
        new_surf = screen.copy()
        new_surf.fill((0, 0, 0, 0))
        
        old_context.draw(old_surf)
        new_context.draw(new_surf)
        
        if self.move_old and not self.move_new:
            screen.blit(new_surf, new_rect)
            screen.blit(old_surf, old_rect.move((self.posx, self.posy)))
        else:
            if self.move_old:
                screen.blit(old_surf, old_rect.move((self.posx, self.posy)))
            else:
                screen.blit(old_surf, old_rect)
            
            if self.move_new:
                screen.blit(new_surf, new_rect.move( \
                                    (self.posx - self.dir_x * new_rect.width, \
                                     self.posy - self.dir_y * new_rect.height)))
            else:
                screen.blit(new_surf, new_rect)
            
    def __str__(self):
        return "%s(%s, %s, (%s, %s), %s, %s)" % (self.__class__.__name__, 
                                                    self._duration, 
                                                    self.distance, 
                                                    self.dir_x, 
                                                    self.dir_y, 
                                                    self.move_old, 
                                                    self.move_new)

# ------------------------------------------------------------------------------
