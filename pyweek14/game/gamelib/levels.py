# scenemanager.SceneManager loads these

import random

import game
import entity
import tool
import monster


def LevelExample():
    entities = [
        (entity.Mailbox, 100),
        (entity.FinishLine, 3000),
    ]
    x = 400
    while x < 10000:
        x += random.uniform(300, 600)
        entities.append((entity.Lamppost, x))
    x = 400
    while x < 10000:
        x += random.uniform(300, 600)
        entities.append((entity.Crate, x))
    x = 400
    while x < 10000:
        x += random.uniform(300, 600)
        entities.append((entity.Hydrant, x))
    x = 400
    while x < 3000:
        x += random.uniform(100, 200)
        entities.append((entity.Civilian, x))
    x = 400
    while x < 3000:
        x += random.uniform(300, 600)
        entities.append((entity.Balloon, x))
    
    tools = [None, tool.Feather, tool.Glue, tool.Wrench, tool.Crowbar, tool.Mallet, tool.Toaster, tool.Toothpick]
    
    ## entities is a list of tuples: [(entity_class, x), ...]
    ## tools is a list of tool classes
    return game.Game(entities, tools)


def Level01():
    run_seconds = 30
    monster_vx = 50
    monster_x = 0
    size_pixels = run_seconds * monster_vx
    entities = [
        (monster.Monster, (monster_x,monster_vx)),
        (entity.FinishLine, (monster_x + size_pixels,)),
    ]
    
    # plan civilians and gadgets
    nr_civilians = 3
    civ_vx = 30
    dvx = monster_vx - civ_vx  # rate at which monster catches up if not slowed down
    closest = run_seconds * 0.7 * dvx
    farthest = run_seconds *0.95 * dvx
    for i,j in enumerate(range(nr_civilians)):
        dx = j * (farthest - closest) / (nr_civilians - 1)  # fencepost
        x = monster_x + closest + dx
        entities.append((entity.Civilian, (x, civ_vx)))
    
    entities.append((entity.Lamppost, (size_pixels*0.2,) ))
    entities.append((entity.Lamppost, (size_pixels*0.35,) ))
    entities.append((entity.Lamppost, (size_pixels*0.5,) ))
    entities.append((entity.Lamppost, (size_pixels*0.65,) ))
    entities.append((entity.Lamppost, (size_pixels*0.8,) ))
    entities.append((entity.Lamppost, (size_pixels*0.95,) ))

    # some decoration
    entities.append((entity.Hydrant, (size_pixels*0.275,)))
    entities.append((entity.Hydrant, (size_pixels*0.875,)))
    entities.append((entity.Crate, (size_pixels*0.05,)))
    entities.append((entity.Crate, (size_pixels*0.4,)))
    entities.append((entity.Crate, (size_pixels*0.8,)))

#    tools = [None, tool.Feather, tool.Glue, tool.Wrench, tool.Crowbar, tool.Mallet, tool.Toaster, tool.Toothpick]
    tools = [tool.Mallet]
    
    ## entities is a list of tuples: [(entity_class, x), ...]
    ## tools is a list of tool classes
    return game.Game(entities, tools, levelnumber=1)


def Level02():
    run_seconds = 40
    monster_vx = 55
    monster_x = 0
    size_pixels = run_seconds * monster_vx
    entities = [
        (monster.Monster, (monster_x,monster_vx)),
        (entity.FinishLine, (monster_x + size_pixels,)),
    ]
    
    # plan civilians and gadgets
    nr_civilians = 4
    civ_vx = 30
    dvx = monster_vx - civ_vx  # rate at which monster catches up if not slowed down
    closest = run_seconds * 0.68 * dvx
    farthest = run_seconds * 0.88 * dvx
    for i,j in enumerate(range(nr_civilians)):
        dx = j * (farthest - closest) / (nr_civilians - 1)  # fencepost
        x = monster_x + closest + dx
        entities.append((entity.Civilian, (x, civ_vx)))
    
    # make a crate with a tool.Feather in it
    entities.append((entity.Crate, (size_pixels*0.3,)))
    entities.append((entity.Crate, (size_pixels*0.55, tool.Feather)))
    entities.append((entity.Crate, (size_pixels*0.8,)))

    # a couple lampposts for good measure
    entities.append((entity.Lamppost, (size_pixels*0.1,) ))
    entities.append((entity.Lamppost, (size_pixels*0.44,) ))
    entities.append((entity.Lamppost, (size_pixels*0.9,) ))

    # decoration
    entities.append((entity.Hydrant, (size_pixels*0.33,) ))
    entities.append((entity.Hydrant, (size_pixels*0.5,) ))
    entities.append((entity.Hydrant, (size_pixels*0.7,) ))
    
    tools = [tool.Mallet, tool.Crowbar]
    
    return game.Game(entities, tools, levelnumber=2)


def Level03():
    run_seconds = 50
    monster_vx = 65
    monster_x = 0
    size_pixels = run_seconds * monster_vx
    entities = [
        (monster.Monster, (monster_x,monster_vx)),
        (entity.FinishLine, (monster_x + size_pixels,)),
    ]
    
    # plan civilians and gadgets
    nr_civilians = 6
    civ_vx = 30
    dvx = monster_vx - civ_vx  # rate at which monster catches up if not slowed down
    closest = run_seconds * 0.26 * dvx
    farthest = run_seconds *0.7 * dvx
    for i,j in enumerate(range(nr_civilians)):
        dx = j * (farthest - closest) / (nr_civilians - 1)  # fencepost
        x = monster_x + closest + dx
        entities.append((entity.Civilian, (x, civ_vx)))
    
    testing = False
    if testing:
        entities.append((entity.Hydrant, (size_pixels*0.35,)))
        entities.append((entity.Hydrant, (size_pixels*0.55,)))
        entities.append((entity.Hydrant, (size_pixels*0.65,)))
        tools = [tool.Wrench,tool.Mallet,tool.Crowbar,tool.Feather]
    else:
        entities.append((entity.Crate, (size_pixels*0.65,tool.Glue)))


        entities.append((entity.Crate, (size_pixels*0.35,)))
        entities.append((entity.Crate, (size_pixels*0.55,)))
        entities.append((entity.Crate, (size_pixels*1.1,)))
        entities.append((entity.Hydrant, (size_pixels*0.1,)))
        entities.append((entity.Hydrant, (size_pixels*0.22,)))
        entities.append((entity.Hydrant, (size_pixels*0.55,)))
        entities.append((entity.Hydrant, (size_pixels*0.7,)))
        entities.append((entity.Hydrant, (size_pixels*0.8,)))
        entities.append((entity.Lamppost, (size_pixels*0.25,)))
        entities.append((entity.Lamppost, (size_pixels*0.45,)))

        entities.append((entity.Balloon, (size_pixels*0.5,)))
        entities.append((entity.Balloon, (size_pixels*0.9,)))

        tools = [tool.Mallet,tool.Crowbar,tool.Feather,tool.Wrench]
    
    return game.Game(entities, tools, levelnumber=3)


def Level04():
    run_seconds = 60
    monster_vx = 75
    monster_x = 0
    size_pixels = run_seconds * monster_vx
    entities = [
        (monster.Monster, (monster_x,monster_vx)),
        (entity.FinishLine, (monster_x + size_pixels,)),
    ]
    
    # plan civilians and gadgets
    nr_civilians = 10
    civ_vx = 30
    dvx = monster_vx - civ_vx  # rate at which monster catches up if not slowed down
    closest = run_seconds * 0.15 * dvx
    farthest = run_seconds *0.5 * dvx
    for i,j in enumerate(range(nr_civilians)):
        dx = j * (farthest - closest) / (nr_civilians - 1)  # fencepost
        x = monster_x + closest + dx
        entities.append((entity.Civilian, (x, civ_vx)))
    
    testing = False
    if testing:
        entities.append((entity.Crate, (size_pixels*0.35,tool.Glue)))
        entities.append((entity.Hydrant, (size_pixels*0.55,)))
        entities.append((entity.Hydrant, (size_pixels*0.65,)))
        entities.append((entity.Lamppost, (size_pixels*0.8,) ))
        tools = [tool.Mallet,tool.Crowbar,tool.Feather,tool.Wrench]
    else:
        entities.append((entity.Crate, (size_pixels*0.4,tool.Toothpick)))
        entities.append((entity.Crate, (size_pixels*0.8,entity.Civilian)))
        entities.append((entity.Crate, (size_pixels*0.02,)))
        entities.append((entity.Crate, (size_pixels*0.3,)))
        entities.append((entity.Crate, (size_pixels*0.55,)))

        entities.append((entity.Hydrant, (size_pixels*0.06,)))
        entities.append((entity.Hydrant, (size_pixels*0.14,)))
        entities.append((entity.Hydrant, (size_pixels*0.3,)))
        entities.append((entity.Hydrant, (size_pixels*0.38,)))
        entities.append((entity.Hydrant, (size_pixels*0.5,)))
        entities.append((entity.Hydrant, (size_pixels*0.7,)))
        entities.append((entity.Hydrant, (size_pixels*0.79,)))
        entities.append((entity.Hydrant, (size_pixels*0.92,)))

        entities.append((entity.Balloon, (size_pixels*0.3,)))
        entities.append((entity.Balloon, (size_pixels*0.45,)))
        entities.append((entity.Balloon, (size_pixels*0.5,)))
        entities.append((entity.Balloon, (size_pixels*0.7,)))
        entities.append((entity.Balloon, (size_pixels*0.9,)))
        entities.append((entity.Balloon, (size_pixels*1.2,)))
        entities.append((entity.Balloon, (size_pixels*1.34,)))
        entities.append((entity.Balloon, (size_pixels*1.6,)))

        entities.append((entity.Lamppost, (size_pixels*0.1,)))
        entities.append((entity.Lamppost, (size_pixels*0.27,)))
        entities.append((entity.Lamppost, (size_pixels*0.4,)))
        entities.append((entity.Lamppost, (size_pixels*0.52,)))
        entities.append((entity.Lamppost, (size_pixels*0.66,)))
        entities.append((entity.Lamppost, (size_pixels*0.75,)))
        entities.append((entity.Lamppost, (size_pixels*0.9,)))


        tools = [tool.Mallet,tool.Crowbar,tool.Feather,tool.Wrench,tool.Glue,tool.Toaster]
    
    return game.Game(entities, tools, levelnumber=4)


def Level05():
    run_seconds = 75
    monster_vx = 70
    monster_x = 0
    size_pixels = run_seconds * monster_vx
    entities = [
        (monster.Monster, (monster_x,monster_vx)),
        (entity.FinishLine, (monster_x + size_pixels,)),
    ]
    
    # plan civilians and gadgets
    nr_civilians = 10
    civ_vx = 30
    dvx = monster_vx - civ_vx  # rate at which monster catches up if not slowed down
    closest = run_seconds * 0.12 * dvx
    farthest = run_seconds * 0.5 * dvx
    for i,j in enumerate(range(nr_civilians)):
        dx = j * (farthest - closest) / (nr_civilians - 1)  # fencepost
        x = monster_x + closest + dx
        entities.append((entity.Civilian, (x, civ_vx)))
    
    testing = False
    if testing:
        entities.append((entity.Crate, (size_pixels*0.35,tool.Glue)))
        entities.append((entity.Hydrant, (size_pixels*0.55,)))
        entities.append((entity.Hydrant, (size_pixels*0.65,)))
        entities.append((entity.Lamppost, (size_pixels*0.8,) ))
        tools = [tool.Mallet,tool.Crowbar,tool.Feather,tool.Wrench]
    else:
        entities.append((entity.Lamppost, (size_pixels*0.1,) ))
        entities.append((entity.Lamppost, (size_pixels*0.18,) ))
        entities.append((entity.Lamppost, (size_pixels*0.3,) ))
        entities.append((entity.Lamppost, (size_pixels*0.36,) ))
        entities.append((entity.Lamppost, (size_pixels*0.5,) ))
        entities.append((entity.Lamppost, (size_pixels*0.6,) ))
        entities.append((entity.Lamppost, (size_pixels*0.77,) ))
        entities.append((entity.Lamppost, (size_pixels*0.96,) ))

        entities.append((entity.Hydrant, (size_pixels*0.22,)))
        entities.append((entity.Hydrant, (size_pixels*0.38,)))
        entities.append((entity.Hydrant, (size_pixels*0.52,)))
        entities.append((entity.Hydrant, (size_pixels*0.65,)))
        entities.append((entity.Hydrant, (size_pixels*0.83,)))
        entities.append((entity.Hydrant, (size_pixels*0.88,)))

        entities.append((entity.Crate, (size_pixels*0.24, entity.Civilian)))
        entities.append((entity.Crate, (size_pixels*0.44, entity.Civilian)))
        entities.append((entity.Crate, (size_pixels*0.54, entity.Civilian)))
        entities.append((entity.Crate, (size_pixels*0.65, entity.Civilian)))
        entities.append((entity.Crate, (size_pixels*0.82, entity.Civilian)))

        entities.append((entity.Crate, (size_pixels*0.32,)))
        entities.append((entity.Crate, (size_pixels*0.63,)))
        entities.append((entity.Crate, (size_pixels*0.70,)))
        entities.append((entity.Crate, (size_pixels*0.77,)))

        entities.append((entity.Balloon, (size_pixels*0.3,)))
        entities.append((entity.Balloon, (size_pixels*0.5,)))
        entities.append((entity.Balloon, (size_pixels*0.62,)))
        entities.append((entity.Balloon, (size_pixels*0.73,)))
        entities.append((entity.Balloon, (size_pixels*0.8,)))
        entities.append((entity.Balloon, (size_pixels*0.91,)))
        entities.append((entity.Balloon, (size_pixels*1.04,)))
        entities.append((entity.Balloon, (size_pixels*1.28,)))
        entities.append((entity.Balloon, (size_pixels*1.42,)))
        entities.append((entity.Balloon, (size_pixels*1.68,)))

        tools = [tool.Mallet,tool.Crowbar,tool.Feather,tool.Wrench,tool.Glue,tool.Toaster,tool.Toothpick]
    
    return game.Game(entities, tools, levelnumber=5)
