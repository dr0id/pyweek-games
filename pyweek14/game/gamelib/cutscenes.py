import pygame

import gummworld2


class CutScene(gummworld2.Engine):
    
    name = 'Cut Scene - Default'
    
    def __init__(self):
        gummworld2.Engine.__init__(self)
        # Set up font and dummy message
        self.font = pygame.font.Font(
            gummworld2.data.filepath('font', settings.fonts.cutscene), 24)
        self.name_img = self.font.render(self.name, True, (255,255,255))
        self.name_rect = self.name_img.get_rect(
            center=gummworld2.State.screen.rect.center)
    
    def draw(self, interp):
        # Display dummy message
        screen = gummworld2.State.screen
        screen.clear()
        screen.blit(self.name_img, self.name_rect)
        screen.flip()
    
    def on_key_down(self, unicode, key, mod):
        self.on_quit()
    
    def on_mouse_button_down(self, pos, button):
        self.on_quit()
    
    def on_quit(self):
        self.running = False
        self.pop()


class Intro(CutScene):
    
    name = 'Cut Scene - Intro'


class Scene01(CutScene):
    
    name = 'Cut Scene - 01'


class Scene02(CutScene):
    
    name = 'Cut Scene - 02'


class SceneWon(CutScene):
    
    name = 'Cut Scene - Won'
