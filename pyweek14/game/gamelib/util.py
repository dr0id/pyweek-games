import pygame

def highlight(surf):
    try:
        s = pygame.Surface(surf.get_size())
        s.blit(surf, (0, 0))
        s.set_colorkey((127, 127, 127))
        arr = pygame.surfarray.pixels3d(s)
        arr[:,:,:] = arr[:,:,:] // 2 + 127
        return s
    except:
        return surf

