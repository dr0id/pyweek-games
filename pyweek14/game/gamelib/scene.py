import gummworld2


class Scene(object):
    
    def __init__(self, context_class):
        self._context = context_class()
        self._context.running = True
    
    def is_ended(self):
        return self._context.running == False
    
    def get_context(self):
        return self._context
