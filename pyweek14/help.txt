Lunatic's Arsenal
hints

The objective of each of the five missions is to slow the creature down enough to let at least one
civilian pass the checkpoint before him. Each tool will only work in certain ways. Some tools take
time to recharge. The icon will be ghosted if the tool is currently charging.

Sometimes different effects can interfere with each other. For best result, wait for one tool's
effect to wear off the creature before applying the next tool.


1. Rubber mallet: hit a lamp post when the creature is a few steps away, and knock the light bulb
out onto his head.

2. Crowbar: open crates. Some crates have additional tools or civilians in them, but some are empty.

3. Feather: apply directly to the creature. A weak attack, but it recharges quickly.

4. Wrench: bust open a fire hydrant to make a puddle in the road that will slow the creature down as
he walks through it.

5. Glue: create a glue puddle that will slow the monster down. The higher you drop the glue from
(the higher you click), the larger the puddle will be and the more he'll slow down. This is a very
powerful attack, but it takes 30 seconds to recharge.

6. Toaster: drop this into a water puddle to give the creature an electric shock if he's nearby.
Takes 20 seconds to recharge.

7. Toothpick: pop balloons that are floating by. These balloons are tethered to rocks that can drop
on his head to slow him down.




