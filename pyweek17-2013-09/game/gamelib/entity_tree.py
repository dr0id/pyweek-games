# -*- coding: utf-8 -*-

"""
TODO: docstring
"""


import pygame

import settings
import pyknicpygame
from pyknicpygame.pyknic.mathematics import Vec2, Point2
import resources


class TrunkSprite(pyknicpygame.spritesystem.Sprite):

    kind = settings.KIND_TREE

    def __init__(self, image, position, anchor=None, z_layer=None, parallax_factors=None, do_init=True):
        pyknicpygame.spritesystem.Sprite.__init__(self, image, position, anchor, z_layer, parallax_factors, do_init)
        self.sub_sprites = [self]
        self.radius = settings.TREE_RADIUS
        
    def append(self, spr):
        self.sub_sprites.append(spr)
        
    def set_alpha(self, alpha):
        for spr in self.sub_sprites[1:]:
            spr.alpha = alpha
            
    @property
    def sprites(self):
        return self.sub_sprites



# TODO: get this from resources??
surf_trunk = pygame.Surface((20, 20), pygame.SRCALPHA|pygame.RLEACCEL)
surf_trunk.fill((255, 255, 255, 0))
pygame.draw.circle(surf_trunk, (91, 36, 0, 255), (10, 10), 10)

surf_tree0 = pygame.Surface((75, 75), pygame.SRCALPHA|pygame.RLEACCEL)
surf_tree0.fill((255, 255, 255, 0))
# surf_tree0.fill((36, 96, 49, 255)) # green
# pygame.draw.circle(s, (36, 96, 49), (25, 25), 25)
pygame.draw.polygon(surf_tree0, (36, 96, 49, 255), [(37, 0), (52, 22), (75, 37), (52, 52), (37, 75), (22, 52), (0, 37), (22, 22)], 0)

surf_tree1 = pygame.Surface((50, 50), pygame.SRCALPHA|pygame.RLEACCEL)
surf_tree1.fill((255, 255, 255, 0))
# surf_tree1.fill((49, 130, 66)) # green
# pygame.draw.circle(s, (49, 130, 66), (20, 20), 20)
pygame.draw.polygon(surf_tree1, (49, 130, 66, 255), [(0, 0), (25, 15), (50, 0), (35, 25), (50, 50), (25, 35), (0, 50), (15, 25)], 0)

surf_tree2 = pygame.Surface((20, 20), pygame.SRCALPHA|pygame.RLEACCEL)
# surf_tree2.fill((255, 255, 255, 0))
# surf_tree2.fill((52, 157, 70)) # green
# pygame.draw.circle(s, (52, 157, 70), (15, 15), 15)
pygame.draw.polygon(surf_tree2, (52, 157, 70, 255), [(10, 0), (12, 8), (20, 10), (12, 12), (10, 20), (8, 12), (0, 10), (8, 8)], 0)

f_low = 1.05
f_mid = 1.10
f_high = 1.15
        
def create(pos):
    resources.load()
    
    # TODO: a bit of radomness? No because of shadows in image!
    surf_trunk = resources.images["tree-trunk"]
    surf_tree0 = resources.images["tree-low"]
    surf_tree1 = resources.images["tree-mid"]
    surf_tree2 = resources.images["tree-top"]
    
    trunk = TrunkSprite(surf_trunk, pos, z_layer=1.2, anchor=Vec2(0.5 * 75.0, -0.5 * 75.0))

    pos0 = f_low * pos
    tree0 = pyknicpygame.spritesystem.Sprite(surf_tree0, pos0, z_layer=2.0, parallax_factors=Vec2(f_low, f_low))
    tree0.kind = settings.KIND_NONE
    trunk.append(tree0)

    pos1 = f_mid * pos
    tree1 = pyknicpygame.spritesystem.Sprite(surf_tree1, pos1, z_layer=3.0, parallax_factors=Vec2(f_mid, f_mid))
    tree1.kind = settings.KIND_NONE
    trunk.append(tree1)

    pos2 = f_high * pos
    tree2 = pyknicpygame.spritesystem.Sprite(surf_tree2, pos2, z_layer=4.0, parallax_factors=Vec2(f_high, f_high))
    tree2.kind = settings.KIND_NONE
    trunk.append(tree2)
    
    return trunk
