# -*- coding: utf-8 -*-

"""
TODO: docstring
"""

import os

import pygame

import pyknicpygame


def get_path(name):
    if name:
        joined_path = os.path.join(pyknicpygame.pyknic.settings['appdir'], '..', 'data', name)
        return os.path.abspath(joined_path)
    return name

def load_spritesheet(source_image, indexes, coordinates):
    images = []
    for idx in indexes:
        x, y, w, h = coordinates[idx]
        img = pygame.Surface((w, h), source_image.get_flags(), source_image) # make sure that is the same format as source
        img.blit(source_image, (0, 0), pygame.Rect(x, y, w, h))
        images.append(img)
    return images
    
images = {}
    
def load():
    """ return res = {} # {name: image}"""
    
    global images
    if images:
        # already loaded
        return images
    
    image_file_names = ["acre.png",
                        "bar.png",
                        "big-grass-horizontal.png",
                        "cave.png",
                        "cliff.png",
                        "house-basement.png",
                        "house-roof.png",
                        "mountains.png",
                        "mushrooms.png",
                        "river.png",
                        "ruins.png",
                        "sheep.png",
                        "small-acre-vertical.png",
                        "street.png",
                        "terrain.png",
                        "tree.png",
                        "wolf-hunter.png",
                        ]

    for f in image_file_names:
        name = f.rsplit('.')[0]
        images[name] = pygame.image.load(get_path(f)).convert_alpha()
        # print('???', f, 'loaded')

    # special treatments
    # wolf
    w = h = 80
    x = y = 0
    coordinates = [
                    ( x + w * 0, y + h * 0, w , h),
                    ( x + w * 1, y + h * 0, w , h),
                    ( x + w * 2, y + h * 0, w , h),
                    ( x + w * 3, y + h * 0, w , h),
                    ( x + w * 4, y + h * 0, w , h),
                    ( x + w * 5, y + h * 0, w , h),
                    ( x + w * 6, y + h * 0, w , h),
                    ( x + w * 7, y + h * 0, w , h),
                    ( x + w * 8, y + h * 0, w , h),
                    ( x + w * 9, y + h * 0, w , h),
                    ( x + w * 0, y + h * 1, w , h),
                    ( x + w * 1, y + h * 1, w , h),
                    ( x + w * 2, y + h * 1, w , h),
                    ( x + w * 3, y + h * 1, w , h),
                    ( x + w * 4, y + h * 1, w , h),
                    ( x + w * 5, y + h * 1, w , h),
                    ( x + w * 6, y + h * 1, w , h),
                    ( x + w * 7, y + h * 1, w , h),
                    ( x + w * 8, y + h * 1, w , h),
                    ( x + w * 9, y + h * 1, w , h),
                    ]
    wolf_walk_anim_idx = [10, 11, 12, 11, 10, 13, 14, 13]
    wolf_stand_idx = [10, ]
    wolf_dead_idx = [15, ]
    wolf_attack_idx = [16, ]
    
    img = images["wolf-hunter"]
    images["wolf-walk-anim"] = load_spritesheet(img, wolf_walk_anim_idx, coordinates) # -> list of images
    images["wolf-stand"] = load_spritesheet(img, wolf_stand_idx, coordinates)[0] # -> list of images
    images["wolf-dead"] = load_spritesheet(img, wolf_dead_idx, coordinates)[0] # -> list of images
    images["wolf-attack"] = load_spritesheet(img, wolf_attack_idx, coordinates)[0] # -> list of images
    
    # sheep
    w = h = 25
    x = y = 0
    coordinates = [
                    ( x + w * 0, y + h * 0, w , h),
                    ( x + w * 1, y + h * 0, w , h),
                    ( x + w * 2, y + h * 0, w , h),
                    ]
    sheep_stand_idx = [0]
    sheep_eat_idx = [1]
    sheep_dead_idx = [2]
    
    img = images["sheep"]
    images["sheep-stand"] = load_spritesheet(img, sheep_stand_idx, coordinates)[0]
    images["sheep-eat"] = load_spritesheet(img, sheep_eat_idx, coordinates)[0]
    images["sheep-dead"] = load_spritesheet(img, sheep_dead_idx, coordinates)[0]
    
    # hunter
    w = h = 80
    x = y = 0
    coordinates = [
                    ( x + w * 0, y + h * 0, w , h),
                    ( x + w * 1, y + h * 0, w , h),
                    ( x + w * 2, y + h * 0, w , h),
                    ( x + w * 3, y + h * 0, w , h),
                    ( x + w * 4, y + h * 0, w , h),
                    ( x + w * 5, y + h * 0, w , h),
                    ( x + w * 6, y + h * 0, w , h),
                    ( x + w * 7, y + h * 0, w , h),
                    ( x + w * 8, y + h * 0, w , h),
                    ( x + w * 9, y + h * 0, w , h),
                    ( x + w * 0, y + h * 1, w , h),
                    ( x + w * 1, y + h * 1, w , h),
                    ( x + w * 2, y + h * 1, w , h),
                    ( x + w * 3, y + h * 1, w , h),
                    ( x + w * 4, y + h * 1, w , h),
                    ( x + w * 5, y + h * 1, w , h),
                    ( x + w * 6, y + h * 1, w , h),
                    ( x + w * 7, y + h * 1, w , h),
                    ( x + w * 8, y + h * 1, w , h),
                    ( x + w * 9, y + h * 1, w , h),
                    ]
    hunter_walk_anim_idx = [0, 1, 2, 1, 0, 3, 4, 3]
    hunter_stand_idx = [0, ]
    hunter_dead_idx = [19, ]
    hunter_sleep_idx = [7, ]
    hunter_shoot_idx = [5, ]
    hunter_reload_idx = [6, ]
    hunter_flee_idx = [8, 9]
    
    img = images["wolf-hunter"]
    images["hunter-walk-anim"] = load_spritesheet(img, hunter_walk_anim_idx, coordinates) # -> list of images
    images["hunter-stand"] = load_spritesheet(img, hunter_stand_idx, coordinates)[0] # -> list of images
    images["hunter-dead"] = load_spritesheet(img, hunter_dead_idx, coordinates)[0] # -> list of images
    images["hunter-sleep"] = load_spritesheet(img, hunter_sleep_idx, coordinates)[0] # -> list of images
    images["hunter-shoot"] = load_spritesheet(img, hunter_shoot_idx, coordinates)[0] # -> list of images
    images["hunter-reload"] = load_spritesheet(img, hunter_reload_idx, coordinates)[0] # -> list of images
    images["hunter-flee-anim"] = load_spritesheet(img, hunter_flee_idx, coordinates) # -> list of images

    
    # tree
    w = h = 75
    x = y = 0
    coordinates = [
                    ( x + w * 0, y + h * 0, w , h),
                    ( x + w * 1, y + h * 0, w , h),
                    ( x + w * 2, y + h * 0, w , h),
                    ( x + w * 3, y + h * 0, 2 * w , 2 * h),
                    ]
    img = images["tree"]
    images["tree-trunk"] = load_spritesheet(img, [3], coordinates)[0] # -> list of images
    images["tree-low"] = load_spritesheet(img, [0], coordinates)[0] # -> list of images
    images["tree-mid"] = load_spritesheet(img, [1], coordinates)[0] # -> list of images
    images["tree-top"] = load_spritesheet(img, [2], coordinates)[0] # -> list of images
    
    return images
  
class Animation(object):

    def __init__(self, scheduler, sprite, images, fps, loop=False):
        self.scheduler = scheduler
        self.sprite = sprite
        self.images = images
        self.dt = 1.0 / fps 
        self.loop = loop
        self.cur_idx = 0
        self.schedule_id = None
        self._is_playing = False
        
    @property
    def fps(self):
        return 1.0/self.dt
        
    @fps.setter
    def fps(self, value):
        self.dt = 1.0 / value
        
    @property
    def is_playing(self):
        return self._is_playing
    
    def play(self):
        if not self._is_playing:
            self.schedule_id = self.scheduler.schedule(self.update, self.dt)
            # show first frame
            self.sprite.set_image(self.images[self.cur_idx])
            self.sprite.rotation = -self.sprite.direction.angle
            self._is_playing = True
        
    def stop(self):
        if self._is_playing:
            self.scheduler.remove(self.schedule_id)
            self._is_playing = False
        
    def rewind(self):
        self.cur_idx = 0
        
    def update(self):
        self.cur_idx += 1
        # self.cur_idx %= len(self.images)
        if self.loop:
            self.cur_idx %= len(self.images)
        elif self.cur_idx >= len(self.images):
            self._is_playing = False
            return 0 # stop re-scheduling
        self.sprite.set_image(self.images[self.cur_idx])
        self.sprite.rotation = -self.sprite.direction.angle
        return self.dt # re-scheduling
        
        
    