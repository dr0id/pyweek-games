# -*- coding: utf-8 -*-

"""
TODO: docstring
"""


import random

import pygame

import pyknicpygame
from pyknicpygame.pyknic.mathematics import Vec2, Point2, distance_from_line

import statemachine
import settings
from settings import MATH_TOLERANCE, KIND_HUNTER, KIND_TREE, \
HUNTER_WALK_SPEED, HUNTER_RUN_SPEED, HUNTER_ASLEEP_PROBABILITY, HUNTER_RADIUS, \
HUNTER_FLEE_DISTANCE_SQ, HUNTER_NEAR_RADIUS_SQ, HUNTER_RELOAD_DELAY, HUNTER_SHOOT_DELAY, \
HUNTER_MIN_SLEEP_TIME, HUNTER_MAX_SLEEP_TIME, HUNTER_STUCK_DELAY, HUNTER_RUN_TIMEOUT, \
HUNTER_SHOT_DELAY, HUNTER_DEAD_DELAY, HUNTER_STUCK_ROTATION, HUNTER_FLEE_DURATION, \
HUNTER_FLEE_DIRECTION_CHANGE_AMOUNT, HUNTER_WALK_ANIM_FPS, HUNTER_RUN_ANIM_FPS, \
HUNTER_FLEE_ANIM_FPS, HUNTER_GUN_FIRE_ANGLE, HUNTER_GUN_MAX_HITPOINTS, HUNTER_GUN_SHOOTING_RANGE


import collider
from collider import collide_detail

import resources


# symbols
CAN_SEE_FAR = 0
CAN_SEE_NEAR = 1
WAKEUP = 2
FALL_ASLEEP = 3
UPDATE = 4
TIMEOUT = 5
STUCK = 6
KILLED = 7
FLEE = 8
NOT_SEEING = 9
RELOAD = 10

dummy_logger = statemachine.symplehfsm.DummyLogger()

# structure
structure = statemachine.symplehfsm.Structure(logger=dummy_logger)

    # def add_state(self, state_identifier, parent, initial, entry_action=None, exit_action=None):
structure.add_state("root", None, False, entry_action=None, exit_action=None)
structure.add_state(    "dead", "root", False, entry_action="dead_entry", exit_action="dead_exit")
structure.add_state(    "alive", "root", True, entry_action=None, exit_action=None)
structure.add_state(        "asleep", "alive", False, entry_action="asleep_enter", exit_action="asleep_exit")
structure.add_state(        "awake", "alive", True, entry_action=None, exit_action=None)
structure.add_state(            "hunting", "awake", True, entry_action=None, exit_action=None)
structure.add_state(                "patrolling", "hunting", True, entry_action="patrolling_entry", exit_action=None)
structure.add_state(                "stuck", "hunting", False, entry_action="stuck_entry", exit_action=None)
structure.add_state(                "runtowards", "hunting", False, entry_action="runtowards_entry", exit_action="runtowards_exit")
structure.add_state(                "shooting", "hunting", False, entry_action="shooting_entry", exit_action="shooting_exit")
structure.add_state(                "shot", "hunting", False, entry_action="shot_entry", exit_action="shot_exit")
structure.add_state(                "reload", "hunting", False, entry_action="reload_entry", exit_action="reload_exit")
structure.add_state(            "flee", "awake", False, entry_action="flee_entry", exit_action="flee_exit")
structure.add_state(            "stuckflee", "awake", False, entry_action="stuck_entry", exit_action=None)

    # def add_trans(self, state, event, target, action=None, guard=None, name=None):
structure.add_trans("patrolling", UPDATE, None,  action="patrol_update", guard=None, name=None)
structure.add_trans("patrolling", FALL_ASLEEP, "asleep",  action=None, guard=None, name=None)
structure.add_trans("patrolling", CAN_SEE_FAR, "runtowards",  action=None, guard=None, name=None)
structure.add_trans("patrolling", CAN_SEE_NEAR, "shooting",  action=None, guard=None, name=None)
structure.add_trans("patrolling", STUCK, "stuck",  action=None, guard=None, name=None)

structure.add_trans("asleep", TIMEOUT, "patrolling",  action=None, guard=None, name=None)

# structure.add_trans("stuck", UPDATE, None,  action="stuck_update", guard=None, name=None)
structure.add_trans("stuck", CAN_SEE_FAR, "shooting",  action=None, guard=None, name=None)
structure.add_trans("stuck", CAN_SEE_NEAR, "shooting",  action=None, guard=None, name=None)
structure.add_trans("stuck", TIMEOUT, "patrolling",  action=None, guard=None, name=None)
structure.add_trans("stuck", STUCK, None,  action="stuck_stuck", guard=None, name=None)

structure.add_trans("runtowards", TIMEOUT, "patrolling",  action=None, guard=None, name=None)
structure.add_trans("runtowards", CAN_SEE_NEAR, "shooting",  action=None, guard=None, name=None)
structure.add_trans("runtowards", CAN_SEE_FAR, None,  action="runtowards_can_see_far", guard=None, name=None)
structure.add_trans("runtowards", NOT_SEEING, None,  action="runtowards_not_seeing", guard=None, name=None)
structure.add_trans("runtowards", UPDATE, None,  action="runtowards_update", guard=None, name=None)

structure.add_trans("shooting", TIMEOUT, "shot",  action=None, guard=None, name=None)

structure.add_trans("shot", RELOAD, "reload",  action=None, guard=None, name=None)
structure.add_trans("shot", TIMEOUT, "runtowards",  action=None, guard=None, name=None)

structure.add_trans("reload", TIMEOUT, "shot",  action=None, guard=None, name=None)

structure.add_trans("hunting", FLEE, "flee",  action=None, guard=None, name=None)

structure.add_trans("flee", STUCK, "stuckflee",  action=None, guard=None, name=None)
structure.add_trans("flee", TIMEOUT, "patrolling",  action=None, guard=None, name=None)
structure.add_trans("flee", UPDATE, None,  action="flee_update", guard=None, name=None)

structure.add_trans("stuckflee", UPDATE, None,  action=None, guard=None, name=None)
structure.add_trans("stuckflee", TIMEOUT, "flee",  action=None, guard=None, name=None)

structure.add_trans("alive", KILLED, "dead",  action=None, guard=None, name=None)




class Actions(object):
    
    def __init__(self, hunter):
        self.hunter = hunter
        self.timed_id = None

    def asleep_enter(self):
        self.hunter.velocity = Vec2(0.0, 0.0)
        self.start_timer(random.randint(HUNTER_MIN_SLEEP_TIME, HUNTER_MAX_SLEEP_TIME))
        pygame.draw.circle(self.hunter.image, (0, 0, 255, 255), (15, 15), 15)        
        self.hunter.set_frame(resources.images["hunter-sleep"])
        
    def asleep_exit(self):
        pygame.draw.circle(self.hunter.image, (100, 100, 100, 255), (15, 15), 15)        
        # self.hunter.scheduler.remove(self.timed_id)

    def patrolling_entry(self):
        self.hunter.set_animation(self.hunter.walk_anim)
        
    def stuck_entry(self):
        self.start_timer(HUNTER_STUCK_DELAY)
        # print('>'*10, self.hunter.velocity)
        # self.hunter.velocity.rotate(random.randint(-HUNTER_STUCK_ROTATION, HUNTER_STUCK_ROTATION))
        self.hunter.velocity.rotate(HUNTER_STUCK_ROTATION)
        # print('>'*20, self.hunter.velocity)
        self.hunter.set_frame(resources.images["hunter-stand"])
        
    def stuck_stuck(self):
        if random.random() < 0.01:
            # print('<'*20, self.hunter.velocity)
            # self.hunter.velocity.rotate(random.randint(-HUNTER_STUCK_ROTATION, HUNTER_STUCK_ROTATION))
            self.hunter.velocity.rotate(HUNTER_STUCK_ROTATION)
        
    def runtowards_entry(self):
        self.hunter.velocity = (self.hunter.last_seen_at - self.hunter.position).normalized
        self.hunter.do_run = True
        self.start_timer(HUNTER_RUN_TIMEOUT)
        self.hunter.set_animation(self.hunter.walk_anim)
    
    def runtowards_exit(self):
        self.hunter.velocity = (self.hunter.last_seen_at - self.hunter.position).normalized
        self.hunter.do_run = False
        self.stop_timer()
        
    def runtowards_can_see_far(self):
        self.hunter.do_run = True

    def runtowards_not_seeing(self):
        self.hunter.do_run = False
        
    def runtowards_update(self):
        if self.hunter.seeing:
            self.hunter.velocity = (self.hunter.last_seen_at - self.hunter.position).normalized
        
    def shooting_entry(self):
        self.hunter.velocity = Vec2(0.0, 0.0)
        self.hunter.direction = (self.hunter.last_seen_at - self.hunter.position).normalized
        self.start_timer(HUNTER_SHOOT_DELAY)
        self.hunter.set_frame(resources.images["hunter-shoot"])
        # TODO: check shot and hit
        # abviation = distance_from_line(self.hunter.position, self.hunter.direction, self.context.player.position)
        # dist = self.hunter.position.get_distance(self.context.player.position)
        
        real_direction = self.hunter.context.player.position - self.hunter.position
        dist = real_direction.length
        
        if dist >= HUNTER_GUN_SHOOTING_RANGE:
            return # out of range
            
        real_direction.normalize()
        dot_angle = self.hunter.direction.dot(real_direction)
        if dot_angle <= HUNTER_GUN_FIRE_ANGLE:
            return # out of cone
        
        angle_factor = (dot_angle - HUNTER_GUN_FIRE_ANGLE) / (1.0 - HUNTER_GUN_FIRE_ANGLE) 
        distance_factor = (HUNTER_GUN_SHOOTING_RANGE - dist) / HUNTER_GUN_SHOOTING_RANGE
        factor = angle_factor * distance_factor
        
        # print('?? dir: {0} \nreal_dir: {1} \ndot_angle: {2} \nangle_factor: {3} \ndist: {4} \ndist_factor: {5} \nfactor: {6} \nhitpoints: {7}'.format(\
                # self.hunter.direction, 
                # real_direction,
                # dot_angle,
                # angle_factor,
                # dist,
                # distance_factor,
                # factor,
                # factor * HUNTER_GUN_MAX_HITPOINTS))
        
        self.hunter.context.player.hurt(factor * HUNTER_GUN_MAX_HITPOINTS)
        
        
        

    def shooting_exit(self):
        self.hunter.num_bullets -= 1

    def shot_entry(self):
        self.start_timer(HUNTER_SHOT_DELAY)
        self.hunter.set_frame(resources.images["hunter-shoot"])
        
    def shot_exit(self):
        self.stop_timer()
        
    def reload_entry(self):
        self.start_timer(HUNTER_RELOAD_DELAY)
        self.hunter.set_frame(resources.images["hunter-reload"])
        
    def reload_exit(self):
        self.hunter.num_bullets = 2
        
    def flee_entry(self):
        self.start_timer(HUNTER_FLEE_DURATION)
        self.hunter.do_run = True
        self.hunter.set_animation(self.hunter.flee_anim)
        
    def flee_exit(self):
        self.stop_timer()
        self.hunter.do_run = False
        
    def flee_update(self):
        self.hunter.velocity = (self.hunter.position - self.hunter.last_seen_at).normalized
        x = HUNTER_FLEE_DIRECTION_CHANGE_AMOUNT
        self.hunter.velocity.rotate(random.randint(-x, x))
        
    def patrol_update(self):
        # self.hunter.velocity.copy_values(self.hunter.wpf.update(self.hunter.position))
        self.hunter.velocity += self.hunter.wpf.update(self.hunter.position)
        
    def dead_entry(self):
        self.start_timer(HUNTER_DEAD_DELAY)
        self.hunter.z_layer = 0.9
        self.hunter.veloccity = Vec2(0.0, 0.0)
        self.hunter.set_frame(resources.images["hunter-dead"])
        
    def dead_exit(self):
        self.stop_timer()
        # TODO: remove self from world!
        
        

        
    def start_timer(self, timeout):
        self.stop_timer()
        self.timed_id = self.hunter.context.scheduler.schedule(self.hunter.timer_elapsed, timeout)
        
    def stop_timer(self):
        self.hunter.context.scheduler.remove(self.timed_id)

        



# hunter_surf = pygame.Surface((2 * settings.HUNTER_RADIUS, 2 * settings.HUNTER_RADIUS), pygame.SRCALPHA|pygame.RLEACCEL)
# hunter_surf.fill((100, 100, 100, 0))
# pygame.draw.circle(hunter_surf, (100, 100, 100, 255), (settings.HUNTER_RADIUS, settings.HUNTER_RADIUS), settings.HUNTER_RADIUS)        
        
class Hunter(pyknicpygame.spritesystem.Sprite):

    run_speed = settings.HUNTER_RUN_SPEED
    walk_speed = settings.HUNTER_WALK_SPEED
    asleep_probability = settings.HUNTER_ASLEEP_PROBABILITY
    kind = settings.KIND_HUNTER

    def __init__(self, position, waypoints, context):
        resources.load()
        pyknicpygame.spritesystem.Sprite.__init__(self, resources.images["hunter-stand"], Point2(position.x, position.y), z_layer=1.1)
        self.old_position = Point2(0, 0)
        self.position = Point2(position.x, position.y)
        self.velocity = Vec2(0, 0)
        self.do_run = False
        self.direction = Vec2(1.0, 0)
        self.old_position.copy_values(self.position)
        
        self.num_bullets = 2
        
        self.radius = settings.HUNTER_RADIUS
        if __debug__:
            self.sprites = [
                    pyknicpygame.spritesystem.TextSprite(str(id(self)), self.position, anchor=Vec2(0, -20)), 
                    pyknicpygame.spritesystem.VectorSprite(self.direction, self.position),
                    pyknicpygame.spritesystem.VectorSprite(self.velocity, self.position, color=(0, 0, 200)),
                    ]
            
        # patrolling stuff
        self.wpf = WayPointFollower(waypoints)
        
        self.context = context
        
        self.seeing = False
        self.last_seen_at = Point2(0, 0)
        self.stamina = 100

        self.walk_anim = resources.Animation(context.scheduler, self, resources.images["hunter-walk-anim"], HUNTER_WALK_ANIM_FPS, True) 
        self.flee_anim = resources.Animation(context.scheduler, self, resources.images["hunter-flee-anim"], HUNTER_FLEE_ANIM_FPS, True) 
        
        self.current_anim = self.walk_anim
        self.current_anim.play()
        
        self.actions = Actions(self)
        self.sm = statemachine.symplehfsm.SympleHFSM(structure, self.actions, logger=dummy_logger)
        self.sm.init(not __debug__)

        

    def set_animation(self, anim, play=True):
        self.current_anim.stop()
        self.current_anim = anim
        if play:
            self.current_anim.play()
        
    def set_frame(self, img):
        self.current_anim.stop()
        self.set_image(img)
        self.rotation = -self.direction.angle
        
    def is_dead(self):
        return self.sm.current_state == "dead"

    def can_see(self, seeing, position):
        # print('?'*10, 'seeing', seeing, position)
        dist_sq = self.position.get_distance_sq(position)
        if seeing:
            if dist_sq < HUNTER_NEAR_RADIUS_SQ:
                self.sm.handle_event(CAN_SEE_NEAR)
            else:
                self.sm.handle_event(CAN_SEE_FAR)
            self.last_seen_at.copy_values(position)
        else:
            self.sm.handle_event(NOT_SEEING)
        if dist_sq < HUNTER_FLEE_DISTANCE_SQ:
            self.sm.handle_event(FLEE)
        self.seeing = seeing
        
    def kill(self):
        self.sm.handle_event(KILLED)

    def update(self, delta_time):
        if not self.is_dead():
            self.old_position.copy_values(self.position)
            self.sm.handle_event(UPDATE)
            
            speed = self.walk_speed
            self.current_anim.fps = HUNTER_WALK_ANIM_FPS
            if self.do_run and self.stamina > 0:
                speed = self.run_speed
                self.stamina -= 1.0 + 0.25
                if self.sm.current_state == "flee":
                    self.current_anim.fps = HUNTER_FLEE_ANIM_FPS
                else:
                    self.current_anim.fps = HUNTER_RUN_ANIM_FPS
                
            self.stamina += 0.25
            self.stamina = 100 if self.stamina > 100 else self.stamina
            self.stamina = 0 if self.stamina < 0 else self.stamina
                    
            # print('>'*5, id(self), self.sm.current_state, self.position.get_distance(self.old_position), self.velocity, speed)
            self.velocity.normalize()
            self.position += self.velocity * speed * delta_time
            
            if self.velocity.length_sq > MATH_TOLERANCE:
                self.direction.copy_values(self.velocity)
            self.direction.normalize()
            
            # self.rotation = -self.direction.angle
                
            if self.num_bullets < 1:
                self.sm.handle_event(RELOAD)

            # print('???', self.sm.current_state, self.position)
            for tree in self.trees:
                collide_detail(self, tree, KIND_HUNTER, KIND_TREE)

            if random.random() < self.asleep_probability:
                self.sm.handle_event(FALL_ASLEEP)
                
            # print('>'*5, id(self), self.sm.current_state, self.position.get_distance(self.old_position), self.velocity, speed)
            if self.position.get_distance(self.old_position) <= MATH_TOLERANCE:
                self.sm.handle_event(STUCK)
                # print('!'*5, id(self), self.sm.current_state)
                
        if __debug__:
            self.sprites[0].text = str(self.sm.current_state)+" "+str(id(self))
            
            self.sprites[1].vector.copy_values(self.direction)
            self.sprites[1].vector *= 20
            self.sprites[1].position.copy_values(self.position)
            
            self.sprites[2].vector.copy_values(self.velocity)
            self.sprites[2].vector *= 20
            self.sprites[2].position.copy_values(self.position)
            
            self.context.renderer.add_sprites(self.sprites)
            

    def timer_elapsed(self):
        self.sm.handle_event(TIMEOUT)
        return -1
        

class WayPointFollower(object):

    def __init__(self, waypoints, loop=True):
        self.waypoints = waypoints
        self.current_waypoint_idx = 0
        self.loop = loop
    
    def update(self, position):
        dir = self.waypoints[self.current_waypoint_idx] - position
        if dir.length_sq < 0.01:
            if self.loop: 
                self.current_waypoint_idx += 1
                self.current_waypoint_idx %= len(self.waypoints)
            elif self.current_waypoint_idx < len(self.waypoints) - 1:
                self.current_waypoint_idx += 1
            dir = self.waypoints[self.current_waypoint_idx] - position
        return dir.normalized
        
    def set_nearest(self, position):
        wp_idx = 0
        wp_dist = position.get_distance_sq(self.waypoints[wp_idx])
        for idx, waypoint in enumerate(self.waypoints):
            dist = position.get_distance_sq(waypoint)
            if dist < wp_dist:
                wp_dist = dist
                wp_idx = idx
        self.current_waypoint_idx = wp_idx
                
            
if __name__ == '__main__':

    pyknicpygame.init()

    scheduler = pyknicpygame.pyknic.timing.Scheduler()

    waypoints = [Point2(0, 0), Point2(1, 0), Point2(1, 1), Point2(0, 1), ]
    hunter = Hunter(Point2(0, 0), 10, waypoints, scheduler)
    hunter.speed = 1.0
    
    dt = 0.1
    for i in range(45):
        hunter.update(dt)
        print(hunter.position)
        
    hunter.asleep_probability = 2
    hunter.update(dt)
    hunter.asleep_probability = 0.01
    while hunter.sm.current_state == "asleep":
        scheduler.update(1.0)
        hunter.update(dt)
        print(hunter.sm.current_state)
    