# -*- coding: utf-8 -*-

"""
TODO: docstring
"""


collision_lookup = {} # {type_bits: function}

def register_collision_function(type_bits, function):
    """
    Registers a callback for two collision types.
    
    type_bits : int
        normally something like entity1.collision_type | entity1.collision_type
    function : callable
        a callback that takes two arguments: the two colliding entities
        the arguments should be ordered by the collision_type
    """
    if type_bits in collision_lookup:
        raise KeyError("key '{0}' already defined".format(type_bits))
    collision_lookup[type_bits] = function
    
    
def collide(entity1, entity2):
    func = collision_lookup[entity1.kind|entity2.kind]
    if entity1.kind < entity2.kind:
        return func(entity1, entity2)
    else:
        return func(entity2, entity1)


def collide_detail(entity1, entity2, type1, type2):
    func = collision_lookup[type1|type2]
    if type1 < type2:
        return func(entity1, entity2)
    else:
        return func(entity2, entity1)

def clear_collision_functions():
    collision_lookup.clear()

        

