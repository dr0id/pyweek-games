# -*- coding: utf-8 -*-

"""
TODO: docstring
"""

import collections

import pyknicpygame
from pyknicpygame.pyknic.mathematics import Vec2, Point2


class BinSpace(object):

    def __init__(self, cell_width, cell_height):
        self.cell_width = cell_width 
        self.cell_height = cell_height
        self._cells = collections.defaultdict(lambda: []) # {(x,y):[]}
        self._entities = collections.defaultdict(lambda: []) # {entity: [cells]} # cells are lists
        
    def __len__(self):
        return sum([len(x) for x in self._cells.values()])

    def update_by_radius_detail(self, entity, x, y, radius):
        # remove from cells
        for cell in self._entities[entity]:
            cell.remove(entity)
        # add
        x_min = int((x - radius) // self.cell_width)
        x_max = int((x + radius) // self.cell_width)
        y_min = int((y - radius) // self.cell_height)
        y_max = int((y + radius) // self.cell_height)

        # print("adding radius {0} into: ".format(entity.rect), x_min, x_max, y_min, y_max)

        
        all_cells = self._cells
        entity_cells = []
        for cell_y in range(y_min, y_max + 1):
            for cell_x in range(x_min, x_max + 1):
                cell = all_cells[(cell_x, cell_y)]
                cell.append(entity)
                entity_cells.append(cell)
        self._entities[entity] = entity_cells

    def update_by_radius(self, entity):
        # remove from cells
        for cell in self._entities[entity]:
            cell.remove(entity)
        # add
        x = entity.position.x
        y = entity.position.y
        radius = entity.radius
        x_min = int((x - radius) // self.cell_width)
        x_max = int((x + radius) // self.cell_width)
        y_min = int((y - radius) // self.cell_height)
        y_max = int((y + radius) // self.cell_height)

        # print("adding radius {0} into: ".format(entity.position), x_min, x_max, y_min, y_max)

        
        all_cells = self._cells
        entity_cells = []
        for cell_y in range(y_min, y_max + 1):
            for cell_x in range(x_min, x_max + 1):
                cell = all_cells[(cell_x, cell_y)]
                cell.append(entity)
                entity_cells.append(cell)
        self._entities[entity] = entity_cells

    def update_by_rect(self, entity):
        # remove from cells
        for cell in self._entities[entity]:
            cell.remove(entity)
        # add, rect coordinates are already integers
        left, top = entity.rect.topleft
        right, bottom = entity.rect.bottomright

        y_min = top // self.cell_height
        y_max = bottom // self.cell_height
        x_min = left // self.cell_width
        x_max = right // self.cell_width
        
        # print("adding rect {0} into: ".format(entity.rect), x_min, x_max, y_min, y_max)

        all_cells = self._cells
        entity_cells = []
        for y in range(y_min, y_max + 1):
            for x in range(x_min, x_max + 1):
                cell = all_cells[(x, y)]
                cell.append(entity)
                entity_cells.append(cell)
        self._entities[entity] = entity_cells

    def update_by_rect_detail(self, entity, left, top, right, bottom):
        # remove from cells
        for cell in self._entities[entity]:
            cell.remove(entity)
        # add, rect coordinates are already integers

        y_min = int(top // self.cell_height)
        y_max = int(bottom // self.cell_height)
        x_min = int(left // self.cell_width)
        x_max = int(right // self.cell_width)
        
        # print("adding rect {0} into: ".format(entity.rect), x_min, x_max, y_min, y_max)

        all_cells = self._cells
        entity_cells = []
        for y in range(y_min, y_max + 1):
            for x in range(x_min, x_max + 1):
                cell = all_cells[(x, y)]
                cell.append(entity)
                entity_cells.append(cell)
        self._entities[entity] = entity_cells

    def remove(self, entity):
        # remove from cells

        try:
            for cell in self._entities[entity]:
                cell.remove(entity)
                # remove cell from dict
                if len(cell) == 0:
                    for key, value in self._cells.iteritems():
                        if value == cell:
                            del self._cells[key]
                            break
            del self._entities[entity]
        except KeyError:
            # TODO: log 
            raise KeyError
            pass
            
    def collide_all_rect(self, rect, kind=None):
        left, top = rect.topleft
        right, bottom = rect.bottomright

        y_min = top // self.cell_height
        y_max = bottom // self.cell_height
        x_min = left // self.cell_width
        x_max = right // self.cell_width
        
        # print('??? rect', (x_min, x_max, y_min, y_max))

        # print("??? rect", rect, " into: ", x_min, x_max, y_min, y_max, left, top, bottom, right)
        
        all_cells = self._cells
        if kind is None:
            return set(e for cell_y in range(y_min, y_max + 1) \
                      for cell_x in range(x_min, x_max + 1) \
                      for e in all_cells[(cell_x, cell_y)])
                      # for e in all_cells[(cell_x, cell_y)] if rect.collidepoint(e.position.as_tuple())]
        else:
            return set(e for cell_y in range(y_min, y_max + 1) \
                      for cell_x in range(x_min, x_max + 1) \
                      for e in all_cells[(cell_x, cell_y)] \
                      if e.kind & kind)
                      # for e in all_cells[(cell_x, cell_y)] if rect.collidepoint(e.position.as_tuple())]

    def collide_all_radius(self, x, y, radius, kind=None):
        x_min = int((x - radius) // self.cell_width)
        x_max = int((x + radius) // self.cell_width)
        y_min = int((y - radius) // self.cell_height)
        y_max = int((y + radius) // self.cell_height)
        
        # print('???  rad', (x_min, x_max, y_min, y_max))
        p = Vec2(x, y)
        all_cells = self._cells
        if kind is None:
            return set(e for cell_y in range(y_min, y_max + 1) \
                      for cell_x in range(x_min, x_max + 1) \
                      for e in all_cells[(cell_x, cell_y)] \
                      if e.position.get_distance_sq(p) < (radius + e.radius) ** 2)
        else:
            return set(e for cell_y in range(y_min, y_max + 1) \
                      for cell_x in range(x_min, x_max + 1) \
                      for e in all_cells[(cell_x, cell_y)] \
                      if e.kind & kind and \
                      e.position.get_distance_sq(p) < (radius + e.radius) ** 2
                      )
                  
    def remove_empty_cells(self):
        """
        Removes empty cells.
        """
        to_remove = []
        for key, cell in self._cells.items():
            if len(cell) == 0:
                to_remove.append(key)
        for key in to_remove:
            del self._cells[key]

    # TODO: idea of frame cache and kind filtering

    
    # def on_enter_frame(self):
        # self._frame_cache = {} #
    
    # def collide_all_radius_filtered(self, x, y, radius, kind):
        # x_min = int((x - radius) // self.cell_width)
        # x_max = int((x + radius) // self.cell_width)
        # y_min = int((y - radius) // self.cell_height)
        # y_max = int((y + radius) // self.cell_height)

        # key = (x_min, x_max, y_min, y_max, kind)
        # if key in self._frame_cache:
            # return self._frame_cache[key]
        # else:
            # entities =  [e for cell_y in range(y_min, y_max + 1) \
                            # for cell_x in range(x_min, x_max + 1) \
                            # for e in all_cells[(cell_x, cell_y)] if e.kind | kind]
            # self._frame_cache[key] = entities
            # return entities
                            
        
            
if __name__ == '__main__':
    
    import pygame
    import pyknicpygame
    from pyknicpygame.pyknic.mathematics import Vec2
    
    class Ent(object):
        def __init__(self, x, y, w, h):
            self.rect = pygame.Rect(x, y, w, h)
            self.position = Vec2(x, y)
            self.radius = w // 2
        def __str__(self):
            return "<{0}>[{1}]".format(id(self), self.rect)
    
    bs = BinSpace(10, 10)
    
    e = Ent(5, 5, 1, 1)
    
    bs.update_by_rect(e)
    
    for c in bs.collide_all_rect(pygame.Rect(0, 0, 20, 20)):
        print(c)
    assert bs.collide_all_rect(pygame.Rect(0, 0, 20, 20))[0] == e
    assert bs.collide_all_rect(pygame.Rect(20, 20, 20, 20)) == []
    assert bs.collide_all_rect(pygame.Rect(10, 10, 20, 20)) == []
    
    for c in bs.collide_all_radius(Vec2(10, 10), 10):
        print(c)
    assert bs.collide_all_radius(Vec2(10, 10), 10)[0] == e
    assert bs.collide_all_radius(Vec2(30, 30), 10) == []
    assert bs.collide_all_radius(Vec2(20, 20), 10) == []
    bs.remove_empty_cells()
    print('num cells', len(bs._cells), bs._cells)
            
            