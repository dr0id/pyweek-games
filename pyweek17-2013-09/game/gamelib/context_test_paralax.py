# -*- coding: utf-8 -*-

"""
TODO: docstring
"""

import random

import pygame

import pyknicpygame
from pyknicpygame.pyknic.mathematics import Vec2, Point2

import settings
from settings import KIND_PLAYER, KIND_HUNTER, KIND_TREE, ACTIVE_ENTITIES_UPDATE_INTERVAL, \
PLAYER_VIEW_RADIUS, PLAYER_ALPHA_FACTOR, PLAYER_MIN_ALPHA, PLAYER_UPDATE_ALPHA_INTERVAL, \
FILL_COLOR, TARGET_RADIUS_SQ, TARGET_RADIUS
clock_get_fps = None
import binspace
import collider
from collider import *
import entity_hunter
import entity_tree
import entity_player
import resources
import dialog



assert settings.KIND_PLAYER < settings.KIND_TREE
assert settings.KIND_HUNTER < settings.KIND_TREE

import logging
logger = logging.getLogger()
logger.setLevel(pyknicpygame.pyknic.settings['log_level'])
if 'log_console_handler' in pyknicpygame.pyknic.settings:
    logger.addHandler(pyknicpygame.pyknic.settings['log_console_handler'])

logger_debug = logger.debug
logger_info = logger.info



class BarSprite(pyknicpygame.spritesystem.Sprite):

    def __init__(self, position, color, context, padding=1):
        pyknicpygame.spritesystem.Sprite.__init__(self, pygame.Surface((1, 1)), position)
        self.draw_special = True
        self.color = color
        self.padding = padding
        self.context = context
        
        
        
    def draw(self, surf, cam, interpolation_factor=1.0):
        img = resources.images["bar"].copy()
        w, h = img.get_size()
        life = int(h * self.context.player.life / 100.0)
        rect = pygame.Rect(0, 0, w, life)
        rect.bottomleft = (0, h)
        # print('??', rect, self.color, life, w, h, img.get_rect().bottomleft)
        img.fill(self.color, rect, pygame.BLEND_RGBA_MULT)
        return surf.blit(img, self.position.as_tuple())
    




class ContextTest(pyknicpygame.pyknic.context.Context):


    def __init__(self):
        self.renderer = None
        self.hud_renderer = None
        self.cams = []
        self.cam = None
        self.player = None
        self.trees = []
        self.trees_bs = None
        self.scheduler = None
        self.binspace = None
        self.hunters = []

    def enter(self):
        resources.load()
        
        global clock_get_fps
        clock_get_fps = settings.clock.get_fps
        cell_w = settings.BINSPACE_CELL_WIDTH
        cell_h = settings.BINSPACE_CELL_HEIGHT
        self.binspace = binspace.BinSpace(cell_w, cell_h)
        self.trees_bs = binspace.BinSpace(cell_w, cell_h)
        self.scheduler = pyknicpygame.pyknic.timing.Scheduler()
        self.renderer = pyknicpygame.spritesystem.DefaultRenderer()
        self.hud_renderer = pyknicpygame.spritesystem.HudRenderer()
        sw, sh = settings.resolution
        self.cams.append(pyknicpygame.spritesystem.Camera(pygame.Rect(0, 0, sw, sh), position=Point2(sw/2, sh/2), padding=settings.CAM_PADDING))
        # self.cams.append(pyknicpygame.spritesystem.Camera(pygame.Rect(0, 0, 100, 100), position=Point2(sw/2, sh/2), padding=settings.CAM_PADDING))
        self.cam = self.cams[0]
        self.player = entity_player.Player(Point2(sw + 200 , sh + 200), self)
        
        self.renderer.add_sprite(self.player)
        
        self.cam.track = self.player

        self.hud_renderer.add_sprite(BarSprite(Point2(20, sh - 20 - 140), (200, 0, 0), self))
        
        # add trees
        for i in range(600):
            pos = Point2(random.randint(600, 4 * sw), random.randint(600, 4 * sh))

            trunk = entity_tree.create(pos)
            self.trees_bs.update_by_radius_detail(trunk, trunk.position.x, trunk.position.y, trunk.radius)
            for spr in trunk.sprites:
                self.binspace.update_by_radius_detail(spr, trunk.position.x, trunk.position.y, trunk.radius)
            
        # add ground
        tile_w = tile_h = 256
        for x in range(0, 4 * sw, tile_w):
            for y in range(0, 4 * sh, tile_h):
                s = pygame.Surface((tile_w, tile_h), pygame.RLEACCEL)
                c = (random.randint(117, 137),
                     random.randint(41, 61),
                     random.randint(0, 10))
                s.fill(c) # brown
                spr = pyknicpygame.spritesystem.Sprite(s, Point2(x, y), z_layer=-1.0)
                spr.kind = settings.KIND_GROUND
                self.binspace.update_by_rect_detail(spr, x, y, x + tile_w, y + tile_h)

        # hunters
        waypoints = [Point2(100, 100), Point2(200, 100), Point2(200, 200), Point2(100, 200), ]
        pos = Point2(100, 100)
        for i in range(100):
            hunter = entity_hunter.Hunter(pos, waypoints, self)
            
            self.binspace.update_by_radius(hunter)
            pos = Point2(random.randint(0, 4 * sw), random.randint(0, 4 * sh))
            waypoints = [pos+Vec2(0, 0), pos+Vec2(100, 0), pos+Vec2(100, 100), pos+Vec2(0, 100), ]
        
        
        # houses
        
        for y in range(50, 700 , 200):
            for x in range(50, 1000, 250):
                pos = Point2(x, y)
                spr = pyknicpygame.spritesystem.Sprite(resources.images["house-basement"], pos, z_layer=1.0)
                spr.kind = settings.KIND_HOUSE
                self.binspace.update_by_rect_detail(spr, pos.x - 100, pos.y - 75, pos.x + 100, pos.y + 75)
                
                f_low = 1.05
                spr = pyknicpygame.spritesystem.Sprite(resources.images["house-roof"], f_low * pos, z_layer=1.5, parallax_factors=Vec2(f_low, f_low))
                spr.kind = settings.KIND_HOUSE
                
                self.binspace.update_by_rect_detail(spr, pos.x - 100, pos.y - 75, pos.x + 100, pos.y + 75)
        
        


        # target
        self.target = Vec2(2400.0, 1800.0)
        spr = pyknicpygame.spritesystem.Sprite(resources.images["mushrooms"], Point2(self.target.x, self.target.y), z_layer=3.1, anchor='midbottom')
        spr.kind = settings.KIND_NONE
        self.binspace.update_by_radius_detail(spr, self.target.x, self.target.y, TARGET_RADIUS)


        
        
        self.scheduler.schedule(self.update_tree_alpha, 0.25)
        self.scheduler.schedule(self.update_active_entities, 0.1, 0.25)
        self.scheduler.schedule(self.check_conditions, 1.5, 0.33)
        
        
        register_collision_function(settings.KINDS_PLAYER_AND_TREE, self.coll_player_tree)
        register_collision_function(settings.KINDS_HUNTER_AND_TREE, self.coll_player_tree)
        
        
        
        
        self.update_active_entities()
        
    def coll_player_tree(self, player, tree):
        n = tree.position - player.position
        depth = 1.0 * (n.length - player.radius - tree.radius)
        if depth < 0.0:
            n.normalize()
            
            ai = player.velocity.dot(n)
            player.velocity -= ai * n
            n *= depth # changes direction of n
            player.position += n

        
    def exit(self):
        clear_collision_functions()
        logger.debug('exit')
    def suspend(self):
        logger.debug('suspend')
    def resume(self):
        logger.debug('resume')
        
    def check_conditions(self):
        # loose condition
        if self.player.life <= 0:
            logger.warn('!!!! LOOSE '*5)
            self.pop()
            self.push(ContextTest())
            co = dialog.Dialog(dialog.loose_dead_text)
            pyknicpygame.pyknic.context.push(co)
            
        if self.player.killed_hunters > 3:
            logger.warn('!!!! LOOSE '*5)
            self.pop()
            self.push(ContextTest())
            co = dialog.Dialog(dialog.loose_eat_text)
            pyknicpygame.pyknic.context.push(co)
            
        # win condition (arrive at a certain location)
        if self.target.get_distance_sq(self.player.position) < TARGET_RADIUS_SQ:
            logger.warn('!!!! WIN  '*5)
            self.pop()
            co = dialog.Dialog(dialog.epilog)
            pyknicpygame.pyknic.context.push(co)
            co = dialog.Dialog(dialog.win_text)
            pyknicpygame.pyknic.context.push(co)
        return settings.CHECK_CONDITIONS_DELAY

    def think(self, delta_time):
        # logger_debug('-' * 80)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.pop()
                self.push(dialog.Dialog(dialog.epilog))
                return
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self.pop()
                    self.push(dialog.Dialog(dialog.epilog))
                    return
                elif event.key == pygame.K_F2:
                    logger.warn('fps: %s', str(settings.clock.get_fps()))
        if clock_get_fps() < 20:
            logger.warn('!! fps: %s', settings.clock.get_fps())

            
        self.player.update(delta_time)
        self.binspace.update_by_radius(self.player)
        
        for hunter in self.hunters:
            hunter.update(delta_time)
            self.binspace.update_by_radius(hunter)
            
        self.scheduler.update(delta_time)
        # self.update_tree_alpha()
        

            
            

    def draw(self, screen):
        screen.fill(FILL_COLOR)
        for cam in self.cams:
            self.renderer.draw(screen, cam, fill_color=None, do_flip=False)
        self.hud_renderer.draw(screen, self.cam)
        pygame.display.flip()
        
    def update_tree_alpha(self):
        logger_debug('update_tree_alpha')
        
        for spr in self.trees_bs.collide_all_radius(self.player.position.x, self.player.position.y, PLAYER_VIEW_RADIUS):
            alpha = int(self.player.position.get_distance_sq(spr.position) * PLAYER_ALPHA_FACTOR)
            spr.set_alpha(alpha if alpha > PLAYER_MIN_ALPHA else PLAYER_MIN_ALPHA)
        return PLAYER_UPDATE_ALPHA_INTERVAL # return next interval
            
    def update_active_entities(self):
        self.renderer.clear()
        sprites = set()
        for cam in self.cams:
            sprites.update(self.binspace.collide_all_rect(cam.world_rect))
        self.renderer.add_sprites(sprites)

        
        self.hunters = [h for h in sprites if h.kind & settings.KIND_HUNTER]
        trees_bs_collide_all_radius = self.trees_bs.collide_all_radius
        for hunter in self.hunters:
            hunter.trees = trees_bs_collide_all_radius(hunter.position.x, hunter.position.y, hunter.radius + hunter.run_speed * ACTIVE_ENTITIES_UPDATE_INTERVAL)
            
        self.player.trees = trees_bs_collide_all_radius(self.player.position.x, self.player.position.y, self.player.radius + self.player.run_speed * ACTIVE_ENTITIES_UPDATE_INTERVAL)
        
        logger_debug("active sprites: %s/%s", len(sprites), len(self.binspace))
        logger_debug("rendered sprites: %s offscreen: %s", len(self.renderer.get_sprites()), len(self.renderer.get_offscreen_sprites(self.cams)))
        logger_debug("active hunters: %s", len(self.hunters))
        logger_debug("player trees: %s", len(self.player.trees))
            
        return ACTIVE_ENTITIES_UPDATE_INTERVAL # return next interval
        
    def add_temp_message(self, msg, duration):
        logger_debug('IMPLEMENT!')
        


