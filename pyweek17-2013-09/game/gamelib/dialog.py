# -*- coding: utf-8 -*-

"""
TODO: docstring
"""

import random
import logging
import math

import pygame

import pyknicpygame
from pyknicpygame.pyknic.mathematics import Point2
from pyknicpygame.pyknic.mathematics import Vec2

import settings
import tweening
import resources

logger = logging.getLogger("intro")
logger.setLevel(pyknicpygame.pyknic.settings['log_level'])


        
text1 = [
"""
Pyweek 17


VEGETARIAN WEREWOLF




by
DR0ID
""",

"""
It all started about a month ago.
I was out with a friend eating dinner
at the new vegetarian restaurant. The 
evening went well until I went home.
On the ride home on my bicycle, I  fell
off my bike. And there is was!
""", 

"""
Two yellow eyes stared at me. Then, a split second
later, it was already over me and I felt its tooth sink
into my leg. At this moment a car came along and nearly
drove me over. But this probably saved me my life. 
So I thought. The creature ran off to the forest. 
""", 

"""
Two weeks later the bite had healed. And I did forget 
about the incident. 

So time went by. 

Two weeks later on I woke up in the night. I was feeling
cold and hot at once. Wired! The full moon was shining through 
the open window. So I got up to close the window. As soon I saw
the moon I froze staring at it. And then it started. 
This feeling. Deep in me.
And it hurt!
""",

"""
AAAAAAARRRRRRGGGGGGGGGGGGGGG!

I Bend over. The next thing I remember was me at the window.
I jumped out of the window. Following this smell! Irresistible smell.
And I felt hungry! Then I realized. Something was different! 
I run four legged to the sheep flock... next thing was me eating a
sheep. My tooth sunk into the flesh. WUUURRRGGG! I was vegetarian 
because I was allergic to sheep meat! All the sheep went up the stomach
again. 

At this time I realized what happened to me.

I had converted to a WEREWOLF!
"""
,
"""
Hunters had been alarmed as I attacked the sheep flock. 
If I wanted to stay alive, I had to run. I ran 
into the forest because I didn't want to hurt someone. 
Then I remembered, that there where rumors about magic
mushrooms. Maybe they could help me! 
"""
,
"""
Instructions
------------

Arrow key to move.

CTRL to sprint for a short time, your sprint ability regenerates.

Avoid the hunter as they will kill you.
Avoid eating the hunters. You lose if you 
eat more than 3 hunters!

Find the magic mushrooms somewhere in the south east.

Good luck!

"""
]

win_text = [
"""
Finally I found the magic mushrooms. 
No real harm done to anyone.

The mushrooms did indeed help me convert back
to my normal form. 

And they did even more. I never converted 
to a werewolf again.


""",

]
        
loose_dead_text = [
"""
The hunters have been very successful in hunting me.
Unfortunately the come out was not healthy at all.



Try again.
""",

]

loose_eat_text = [
"""
I couldn't control myself to much and ate my 
neighbor (well, I didn't really hate him, but
I don't mind either), some other guy from the
town. And the sleepy hunter was too easy!


Try again.
""",
]

epilog = [
"""
Thanks for playing. 


This game turned out more to be a tech demo
than an actual game. But I hope you have
enjoyed it. 

Some techniques that have been used:
    parallax scrolling
    painters algorithm (z-sorting)
    surface blend modes 
    (surface alpha combined with pixel alpha)
    space division using a cells
    broad and narrow collision detection
    tweening
    
    ...and many more things.
    
    
    Powered by PYGAME.
""",

"""
Disclaimer:

No animals nor humans were harmed
during production (nor during the
gaming if won, otherwise... not sure)




DR0ID (C) 2013

""",
]


class Dialog(pyknicpygame.pyknic.context.Context):
    """
    The context class.
    """
    def __init__(self, textstrip, renderer=None, cam=None):
        
        self.renderer = renderer
        self.cam = cam
        self.text_strip = textstrip
        self.current_text = 0
        self.tweener = tweening.Tweener()
        self.sprites = []
        self.old_sprites = []
        self.int_transition = False
        self.font = None
        


    def enter(self):
        """Called when this context is pushed onto the stack."""
        pygame.event.clear()

        sw, sh = settings.resolution

        if self.renderer is None:
            self.renderer = pyknicpygame.spritesystem.DefaultRenderer()
        if self.cam is None:
            self.cam = pyknicpygame.spritesystem.Camera(pygame.Rect(0, 0, sw, sh), position=Point2(sw/2, sh/2))
            
        self.font = pygame.font.Font(resources.get_path("nvvzs.ttf"), 20)
        
        self.current_text = 0
        self.prepare_next_text(self.text_strip[self.current_text])
        
        r_img = pygame.image.load(resources.get_path("right-arrow.png")).convert_alpha()
        self.right_arrow = pyknicpygame.spritesystem.Sprite(r_img, Point2(sw // 2, sh - 150))
        self.renderer.add_sprite(self.right_arrow)
        

    def _next(self):
        self.current_text += 1
        if self.current_text >= len(self.text_strip):
            self.pop()
        else:
            if not self.int_transition:
                self.prepare_next_text(self.text_strip[self.current_text])
                self.int_transition = True
        
    def prepare_next_text(self, text):
        sw, sh = settings.resolution
        x = sw // 2
        y = 70
        dy = 20
        dt = 1
        for spr in self.renderer.get_sprites():
            if spr is not self.right_arrow:
                self.tweener.create_tween(spr.position, 'x', spr.position.x, -sw, dt, tweening.IN_OUT_QUAD, cb_end=self.remove_sprite, cb_args=[spr])
        
        for idx, t in enumerate(text.split("\n")):
            spr = pyknicpygame.spritesystem.TextSprite(t, Point2(x + sw, dy * idx + y), font=self.font)
            self.tweener.create_tween(spr.position, 'x', x + sw, -sw, dt, tweening.IN_OUT_QUAD)
            self.renderer.add_sprite(spr)
            self.sprites.append(spr)

    def remove_sprite(self, *args):
        self.int_transition = False
        spr = args[-1][0]
        self.renderer.remove_sprite(spr)
        
    def exit(self):
        """Called when this context is popped off the stack."""
        pass

    def suspend(self):
        """Called when another context is pushed on top of this one."""
        pass

    def resume(self):
        """Called when another context is popped off the top of this one."""
        pass

    def think(self, delta_time):
        """Called once per frame"""
        self.tweener.update(delta_time)
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pyknicpygame.pyknic.context.pop()
                elif event.key == pygame.K_SPACE:
                    self._next()
                elif event.key == pygame.K_RIGHT:
                    self._next()
                if __debug__:
                    if event.key == pygame.K_F11:
                        self.done = True
            elif event.type == pygame.MOUSEBUTTONDOWN:
                hit_items = self.renderer.get_sprites_in_rect(pygame.Rect(event.pos, (1, 1)))
                if hit_items:
                    item = hit_items[0]
                    if item == self.right_arrow:
                        self._next()
            elif event.type == pygame.QUIT:
                self.pop()
                self.pop()
                
    def draw(self, screen):
        """Refresh the screen"""
        self.renderer.draw(screen, self.cam, fill_color=(0, 0, 0), do_flip=True)

            
