# -*- coding: utf-8 -*-

"""
TODO: docstring
"""

import pygame

import settings
from settings import KIND_PLAYER, KIND_TREE, PLAYER_UPDATE_VICTIM_INTERVAL, KINDS_VICTIMS, \
        PLAYER_SMELL_RADIUS, PLAYER_TO_VICTIM_FORCE_FACTOR, HUNTER_SEE_RADIUS, KIND_HUNTER, \
        HUNTER_VIEW_FIELD, KINDS_VIEW_OBSTACLES, PLAYER_UPDATE_CAN_SEE_ME_INTERVAL, \
        PLAYER_WALK_ANIM_FPS, PLAYER_RUN_ANIM_FPS, MATH_TOLERANCE, PLAYER_REGENERATION_SPEED
import pyknicpygame
from pyknicpygame.pyknic.mathematics import Vec2, Point2, distance_from_line
import collider
from collider import collide_detail
import resources

#  image, position, anchor=None, z_layer=None, parallax_factors=None, do_init=True):
player_surf = pygame.Surface((2 * settings.PLAYER_RADIUS, 2 * settings.PLAYER_RADIUS), pygame.SRCALPHA|pygame.RLEACCEL)
player_surf.fill((255, 255, 255, 0))
pygame.draw.circle(player_surf, (255, 0, 0), (settings.PLAYER_RADIUS, settings.PLAYER_RADIUS), settings.PLAYER_RADIUS)


class Player(pyknicpygame.spritesystem.Sprite):

    run_speed = settings.PLAYER_RUN_SPEED
    walk_speed = settings.PLAYER_WALK_SPEED
    surf = player_surf
    kind = settings.KIND_PLAYER

    def __init__(self, position, context):
        resources.load()
        pyknicpygame.spritesystem.Sprite.__init__(self, resources.images["wolf-stand"], Point2(position.x, position.y), z_layer=1.0)
        self.old_position = Point2(0, 0)
        self.position = Point2(position.x, position.y)
        self.velocity = Vec2(0, 0)
        self.speed = self.walk_speed
        self.direction = Vec2(1.0, 0.0) # view direction, unit length
        self.old_position.copy_values(self.position)
        
        self.radius = settings.PLAYER_RADIUS
        self.victim = None
        self.victim_dist = 0
        self.context = context
        context.scheduler.schedule(self.update_victim, PLAYER_UPDATE_VICTIM_INTERVAL, 0)
        context.scheduler.schedule(self.can_hunter_see_me, PLAYER_UPDATE_CAN_SEE_ME_INTERVAL, 0)
        # context.scheduler.schedule(self.change_color, 1.0, 0)
        self.life = 100
        self.stamina = 100
        
        self.killed_sheep = 0
        self.killed_hunters = 0
        
        self.anim = resources.Animation(context.scheduler, self, resources.images["wolf-walk-anim"], PLAYER_WALK_ANIM_FPS, True) 
        self.anim.play()

        
        self.col = 0
        
    def change_color(self):
        self.col += 1
        pygame.draw.circle(self.image, (255, 0, 0) if self.col % 2 == 0 else (0, 255, 0), (settings.PLAYER_RADIUS, settings.PLAYER_RADIUS), settings.PLAYER_RADIUS)
        return 1.0
        
    def hurt(self, hit_points):
        self.life -= hit_points
            

    def update(self, delta_time):
        self.old_position.copy_values(self.position)
        
        if self.life <= 0:
            self.set_image(resources.images["wolf-dead"])
            self.rotation = -self.direction.angle
            self.anim.stop()
            return
            
        if self.life < 100:
            self.life += PLAYER_REGENERATION_SPEED
        else:
            self.life = 100
        
        pressed = pygame.key.get_pressed()
        speed = self.walk_speed
        self.anim.fps = PLAYER_WALK_ANIM_FPS
        
        if pygame.key.get_mods() & pygame.KMOD_CTRL and self.stamina > 0:
            speed = self.run_speed
            self.stamina -= 0.75 + 0.5
            self.anim.fps = PLAYER_RUN_ANIM_FPS
            
        self.stamina += 0.5
        self.stamina = 100 if self.stamina > 100 else self.stamina
        
        input = Vec2(0, 0)
        if pressed[pygame.K_RIGHT]:
            input += Vec2(1, 0)
        if pressed[pygame.K_LEFT]:
            input += Vec2(-1, 0)
        if pressed[pygame.K_DOWN]:
            input += Vec2(0, 1)
        if pressed[pygame.K_UP]:
            input += Vec2(0, -1)
           
        input.length = speed
        
        self.velocity += input
        # self.velocity.normalize()

        for v in self.context.binspace.collide_all_radius(self.position.x, self.position.y, PLAYER_SMELL_RADIUS, KINDS_VICTIMS):
            if not v.is_dead():
                dir = v.position - self.position
                if dir.length_sq < 9.0:
                    # kill victim
                    # self.context.hunters.remove(self.victim)
                    # self.context.binspace.remove(self.victim)
                    v.kill()
                    
                    if v.kind == settings.KIND_HUNTER:
                        self.killed_hunters += 1
                        # self.context.add_temp_message(msg, duration)
                    elif v.kind == settings.KIND_SHEEP:
                        self.killed_sheep += 1
                        # self.context.add_temp_message(msg, duration)

        if self.victim is not None:
            if self.victim.is_dead():
                self.victim = None
                self.velocity = Vec2(0, 0)
            else:
                dir = self.victim.position - self.position
                dir.length = PLAYER_TO_VICTIM_FORCE_FACTOR / self.victim_dist
                self.velocity += dir
        
        if self.velocity.length_sq <= MATH_TOLERANCE:
            self.velocity = Vec2(0.0, 0.0)
            self.anim.stop()
        else:
            self.anim.play()
            
            
        
        self.position += self.velocity * delta_time
        self.direction.copy_values(self.velocity)
        self.direction.normalize()
        # self.velocity -= input # reset
        self.velocity = Vec2(0.0, 0.0)
            
        for tree in self.trees:
            collide_detail(self, tree, KIND_PLAYER, KIND_TREE)

    def update_victim(self):
        vics = self.context.binspace.collide_all_radius(self.position.x, self.position.y, PLAYER_SMELL_RADIUS, KINDS_VICTIMS)
        self.victim = None
        self.velocity = Vec2(0, 0)
        if vics:
            self.victim = vics.pop()
            self.victim_dist = self.victim.position.get_distance(self.position)
            for vic in vics:
                if not vic.is_dead():
                    dist = self.position.get_distance(vic.position)
                    if dist < self.victim_dist:
                        self.victim_dist = dist
                        self.victim = vic
        return PLAYER_UPDATE_VICTIM_INTERVAL
        
    def can_hunter_see_me(self):
        # print('='*10 )
        kinds = KINDS_VIEW_OBSTACLES | KIND_HUNTER
        objects = self.context.binspace.collide_all_radius(self.position.x, self.position.y, HUNTER_SEE_RADIUS, kinds)
        hunters = set([h for h in objects if h.kind & KIND_HUNTER])
        objects -= hunters
        
        # print('X'*10, len(hunters), len(objects), [id(h) for h in hunters])
        for hunter in hunters:
            if self.life <= 0:
                can_see = False
            else:
                dir = self.position - hunter.position
                dir.normalize()
                can_see = True
                # print('* check viewfield', id(hunter), dir.dot(hunter.direction.normalized), dir, hunter.direction, HUNTER_VIEW_FIELD)
                hunter.direction.normalize()
                if dir.dot(hunter.direction) >= HUNTER_VIEW_FIELD: # hunter looks in players direction
                    for obj in objects:
                        obj_dir = obj.position - hunter.position
                        obj_dir.normalize()
                        if obj_dir.dot(hunter.direction) >= HUNTER_VIEW_FIELD:
                            # object within viewfield, potential view obstacle
                            dist = distance_from_line(hunter.position, dir, obj.position)
                            # print('** check obj in viewfield', id(hunter), dist,  hunter.position, obj.position)
                            if dist <= obj.radius:
                                # player hidden by obj
                                # print('*** cant see me!', id(hunter))
                                can_see = False
                                break # don't need to check other objects  
                    pass
                else:
                    # print('x'*10, id(hunter), False)
                    can_see = False
                
            # if can_see is True:
            # send message to hunter
            hunter.can_see(can_see, self.position)
            
        return PLAYER_UPDATE_CAN_SEE_ME_INTERVAL
            
            
# def create(context):
    # spr = Player(Point2(
    # anim = resources.Animation(context.scheduler, spr, resources.images["wolf-walk-anim"], 10, True) 
