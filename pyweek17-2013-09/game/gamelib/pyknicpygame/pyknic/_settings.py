# -*- coding: utf-8 -*-

"""
.. TODO:: document
.. todo:: use a signaling dict for settings!? 
"""

import os
import logging

_user_home_path = os.path.expanduser('~')
_user_home_path = os.path.join(_user_home_path, ".pyknic")

if not os.path.exists(_user_home_path):
    try:
        os.makedirs(_user_home_path)
    except OSError as ex:
        logging.getLogger('').warn(str(ex))

# settings
# TODO: signaling dict!? 
settings = {} # pylint: disable=C0103

# logging
settings['log_path'] = os.getcwd()

if __debug__:
    settings['log_to_console'] = True
else:
    settings['log_to_console'] = False
    
settings['log_to_file'] = True
settings['log_filename'] = "pyknic.log"

if __debug__:
    settings['log_level'] = logging.DEBUG
else:
    settings['log_level'] = logging.INFO
    
settings['log_format'] = "%(asctime)s: %(levelname)-8s [%(module)-8s (%(threadName)s[%(thread)d])] %(message)s [%(name)s: %(funcName)s in %(filename)s(%(lineno)d)]"
# prefere to fail as to return a default value causing strange errors!!
# settings['log_console_handler'] = None
# settings['log_file_handler'] = None

# paths
settings['user_home_path'] = _user_home_path

# internal
settings['documenting'] = False
settings['testing'] = False

# -----------------------------------------------------------------------------
