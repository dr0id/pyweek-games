# -*- coding: utf-8 -*-

"""
TODO: docstring
"""


import pygame



resolution = (800, 660)
caption = "pyweek17 - Vegetarian Werewolf"  # TODO: game name!
flags = pygame.DOUBLEBUF
bit_depth = 32

clock = None

sim_time_step = 0.025

domain_name = "DR0IDs-pyweek17-vegetarian-werewolf" # used for translation files

# kinds of entities
KIND_NONE   = 0
KIND_PLAYER = 1 << 0
KIND_HUNTER = 1 << 1
KIND_TREE   = 1 << 2
KIND_GROUND = 1 << 3
KIND_SHEEP  = 1 << 4
KIND_TRAP   = 1 << 5
KIND_HOUSE  = 1 << 6
_max_shift = 29 # the last shift you used for the types

# kind combinations
KINDS_ALL = ((~(1 << (_max_shift + 1))) + 2) * -1 # == 2147483647 this is sys.maxsize (on 32 bit)
KINDS_PLAYER_AND_TREE = KIND_PLAYER | KIND_TREE
KINDS_HUNTER_AND_TREE = KIND_HUNTER | KIND_TREE
KINDS_VICTIMS = KIND_HUNTER | KIND_SHEEP
KINDS_VIEW_OBSTACLES = KIND_TREE



# variables
TREE_RADIUS = 15
TARGET_RADIUS = 50
TARGET_RADIUS_SQ = TARGET_RADIUS**2

PLAYER_RUN_SPEED = 150.0 #75.0
PLAYER_WALK_SPEED = 56.0 #28.0

PLAYER_VIEW_RADIUS = 135.0
PLAYER_ALPHA_FACTOR = 255.0 / (PLAYER_VIEW_RADIUS * PLAYER_VIEW_RADIUS)
PLAYER_MIN_ALPHA = 40
PLAYER_UPDATE_ALPHA_INTERVAL = 0.1 # seconds
PLAYER_TO_VICTIM_FORCE_FACTOR = 2800.0

PLAYER_SMELL_RADIUS = 200
PLAYER_RADIUS = 15
PLAYER_UPDATE_VICTIM_INTERVAL = 3.0 # seconds
PLAYER_UPDATE_CAN_SEE_ME_INTERVAL = 0.25 # seconds
PLAYER_WALK_ANIM_FPS = 8
PLAYER_RUN_ANIM_FPS = 20
PLAYER_REGENERATION_SPEED = 0.05

HUNTER_WALK_SPEED = 16.0 #8.0
HUNTER_RUN_SPEED = 48.0 #24.0
HUNTER_ASLEEP_PROBABILITY = 0.0005
HUNTER_RADIUS = 15
HUNTER_SEE_RADIUS = 300
HUNTER_VIEW_FIELD = 0.866025403784 # ~ 2 * 22° 
# see below for more values
HUNTER_GUN_FIRE_ANGLE = 0.984807753012 #0.94999999999999996
HUNTER_GUN_SHOOTING_RANGE = 250
HUNTER_GUN_MAX_HITPOINTS = 50 # [0, 100] 100 life points available, 0 means no harm and negative hitpoints would heal :D
HUNTER_FLEE_DISTANCE_SQ = 60**2
HUNTER_FLEE_DURATION = 8.0 # seconds
HUNTER_NEAR_RADIUS_SQ = 200**2
HUNTER_RELOAD_DELAY = 2.0 # seconds
HUNTER_SHOOT_DELAY = 0.5 # seconds
HUNTER_MIN_SLEEP_TIME = 5.0 # seconds
HUNTER_MAX_SLEEP_TIME = 20.0 # seconds
HUNTER_STUCK_DELAY = 3.0 # seconds
HUNTER_RUN_TIMEOUT = 10.0 # seconds
HUNTER_SHOT_DELAY = 0.5 # seconds
HUNTER_DEAD_DELAY = 10.0 # seconds
HUNTER_STUCK_ROTATION = 100 # degrees randint(-HUNTER_STUCK_ROTATION, HUNTER_STUCK_ROTATION)
HUNTER_FLEE_DIRECTION_CHANGE_AMOUNT = 15
HUNTER_WALK_ANIM_FPS = 8
HUNTER_RUN_ANIM_FPS = 20
HUNTER_FLEE_ANIM_FPS = 8

BINSPACE_CELL_WIDTH = 150
BINSPACE_CELL_HEIGHT = 150
CAM_PADDING = 100

MATH_TOLERANCE = 0.05

ACTIVE_ENTITIES_UPDATE_INTERVAL = 0.8 # seconds

FILL_COLOR = (0, 0, 0, 255)

CHECK_CONDITIONS_DELAY = 1.5

# degrees         dot value between vectors
# 0               1.0
# 1               0.999847695156
# 2               0.999390827019
# 3               0.998629534755
# 4               0.99756405026
# 5               0.996194698092
# 6               0.994521895368
# 7               0.992546151641
# 8               0.990268068742
# 9               0.987688340595
# 10              0.984807753012
# 11              0.981627183448
# 12              0.978147600734
# 13              0.974370064785
# 14              0.970295726276
# 15              0.965925826289
# 16              0.961261695938
# 17              0.956304755963
# 18              0.951056516295
# 19              0.945518575599
# 20              0.939692620786
# 21              0.933580426497
# 22              0.927183854567
# 23              0.920504853452
# 24              0.913545457643
# 25              0.906307787037
# 26              0.898794046299
# 27              0.891006524188
# 28              0.882947592859
# 29              0.874619707139
# 30              0.866025403784
# 31              0.857167300702
# 32              0.848048096156
# 33              0.838670567945
# 34              0.829037572555
# 35              0.819152044289
# 36              0.809016994375
# 37              0.798635510047
# 38              0.788010753607
# 39              0.777145961457
# 40              0.766044443119
# 41              0.754709580223
# 42              0.743144825477
# 43              0.731353701619
# 44              0.719339800339
# 45              0.707106781187
# 46              0.694658370459
# 47              0.681998360062
# 48              0.669130606359
# 49              0.656059028991
# 50              0.642787609687
# 51              0.62932039105
# 52              0.615661475326
# 53              0.601815023152
# 54              0.587785252292
# 55              0.573576436351
# 56              0.559192903471
# 57              0.544639035015
# 58              0.529919264233
# 59              0.51503807491
# 60              0.5
# 61              0.484809620246
# 62              0.469471562786
# 63              0.45399049974
# 64              0.438371146789
# 65              0.422618261741
# 66              0.406736643076
# 67              0.390731128489
# 68              0.374606593416
# 69              0.358367949545
# 70              0.342020143326
# 71              0.325568154457
# 72              0.309016994375
# 73              0.292371704723
# 74              0.275637355817
# 75              0.258819045103
# 76              0.2419218956
# 77              0.224951054344
# 78              0.207911690818
# 79              0.190808995377
# 80              0.173648177667
# 81              0.15643446504
# 82              0.13917310096
# 83              0.121869343405
# 84              0.104528463268
# 85              0.0871557427477
# 86              0.0697564737441
# 87              0.0523359562429
# 88              0.0348994967025
# 89              0.0174524064373
# 90              0.0









if __name__ == '__main__':
    # _type_generator = _create_type_generator()
    import sys
    # print('maxsize:', sys.maxsize)
    # try:
        # for i in range(100):
            # val = next(_type_generator)
            # print("{0:15}, {1:15}, {2:>50}".format(i, val, bin(val)))
            # if val > sys.maxsize:
                # print('!!! ' + str(i))
    # except:
        # pass
    print("{0:>32}, {1:>50}".format(type_all, bin(type_all)))
    print("{0:>32}, {1:>50}".format(sys.maxsize, bin(sys.maxsize)))
