﻿Vegetarian Werewolf
===================

A game written for pyweek 17 (www.pyweek.org)

The theme was moon.



Members: DR0ID


DEPENDENCIES: 
python (tested with: 2.6.4 and 3.1)
pygame (tested with: 1.9.1)

You might need to install some of these before running the game:

  Python:     http://www.python.org/
  PyGame:     http://www.pygame.org/


RUNNING THE GAME:

On Windows or Mac OS X, locate the "run_game.pyw" file and double-click it.

Othewise open a terminal / console and "cd" to the game directory and run:

  python run_game.py



HOW TO PLAY THE GAME:

Please follow the in game instructions.

Arrow key to move.
CTRL to sprint.

Find the mushrooms and avoid either getting killed or eating to many hunters.







