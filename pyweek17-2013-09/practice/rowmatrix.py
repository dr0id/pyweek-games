# -*- coding: utf-8 -*-
"""
One line matrix operations using a row matrix, e.g. 

m = [[1, 2, 3],
     [4, 5, 6],
     [7, 8, 9]]

     
     
     
     
member access by m[y][x]:

m[2][0] == 7

defining a column vector as a 1 x 3 matrix:

v = [[1],
     [2],
     [3]]

or as a row vector:

w = [[1, 2, 3]]


tricks:

    define a vector as row vector and use it as a column vector:
    row_v = [[1, 2, 3]]
    col_v = transpose(row_v)    # [[1], 
                                #  [2], 
                                #  [3]]
                                
    define a matrix as row matrix but use it as a column matrix:
    row_m = [[1, 4, 7],
             [2, 5, 8],
             [3, 6, 9]]
             
    col_m = transpose(row_m)    # 1  2  3
                                # 4  5  6
                                # 7  8  9
                                
    to iterate with x first do:
        m_transpose = transpose(m)
        for x in range(x_dim):
            for y in range(y_dim):
                value = m_transposed[x][y]
                
    instead of:
        for y in range(y_dim):
            for x in range(x_dim):
                value = m[y][x]
                
    both return the same values in the same order!
        
"""

import sys
import operator
from operator import mul as operator_mul
from operator import add as operator_add
from operator import sub as operator_sub

def m_sub_m(m1, m2):
    assert len(m1[0]) == len(m2[0]) and len(m1) == len(m2), "matrix dimensions do not match"
    return map(lambda m1_row, m2_row: map(operator_sub, m1_row, m2_row), m1, m2)

def m_add_m(m1, m2):
    assert len(m1[0]) == len(m2[0]) and len(m1) == len(m2), "matrix dimensions do not match"
    return map(lambda m1_row, m2_row: map(operator_add, m1_row, m2_row), m1, m2)

def transpose(m1):
    return map(lambda *vals: list(vals), *m1)

def m_diagonal(dim, value):
    assert dim > 0, "dimension should be positive value"
    assert isinstance(dim, int), "dimension should be an integer"
    return [([0.0]*i) + [value] + ([0.0]*(dim-i-1)) for i in range(dim)]
    
def m_mul_s(m1, s):
    return [[v * s for v in row] for row in m1]

def m_mul_m(m1, m2):
    assert len(m1[0]) == len(m2), "matrix dimensions do not match"
    return [[sum(map(operator_mul, m1_row, m2_row)) for m2_row in map(lambda *vals: vals, *m2)] for m1_row in m1]

def mv_mul_mv(m1, m2):
    assert len(m1[0]) == len(m2[0]) and len(m1) == len(m2), "matrix dimensions do not match"
    return map(lambda m1_row, m2_row: map(operator_mul, m1_row, m2_row), m1, m2)

    
    
def m_div_s(m1, s):
    assert isinstance(s, float), "scalar should be float"
    return [[v / s for v in row] for row in m1]
    
def m_mul_v(m1, v):
    assert len(m1[0]) == len(v), "vector and matrix dimensions do not match"
    return [sum(map(operator_mul, row, v)) for row in m1]
    
def m_identity(dim):
    return m_diagonal(dim, 1)
    

def m_dimensions(m1):
    return len(m1[0]), len(m1)
    
def m_max_value(m1):
    return max(v for row in m1 for v in row)
    
def m_min_value(m1):
    return min(v for row in m1 for v in row)

def m_sum(m1):
    return sum(v for row in m1 for v in row)
    
def m_median(m1):
    return sum(v for row in m1 for v in row) / float(len(m1) * len(m1[0]))
    
def m_normalize(m1):
    return m_mul_s(m1, 1.0 / sum(v for row in m1 for v in row))
    
def m_pos_part(m1):
    return [[v if v > 0 else 0.0 for v in row] for row in m1]
    
def m_neg_part(m1):
    return [[v if v < 0 else 0.0 for v in row] for row in m1]

def m_stretch(m1, min_value, max_value):
    m_max = m_max_value(m1)
    m_min = m_min_value(m1)
    factor = (max_value - min_value) / (m_max - m_min)
    return [[(v - m_min) * factor + min_value for v in row] for row in m1]
    
def m_fill(dimx, dimy, value):
    assert dimx > 0, "dimension x should be positive value"
    assert dimy > 0, "dimension y should be positive value"
    assert isinstance(dimx, int), "dimension x should be an integer"
    assert isinstance(dimy, int), "dimension y should be an integer"
    return [[value]*dimx for i in range(dimy)]

def m_round(m1, func=round, *args):
    return [[func(v, *args) for v in col] for col in m1]

def print_matrix(values, prefix="", space="\t", newline="\n", postfix=""):
    sys.stdout.write(prefix + newline.join(space.join(str(val) for val in row) + "\n" for row in values) + postfix)

    
    

    

def test_matrix_functions():    
    m = []
    for y in range(4):
        r = []
        for x in range(4):
            r.append(x+y+x^y)
        m.append(r)

    print('??')
    print('m')
    print_matrix(m)
    j = transpose(m)
    print("j: transposed m")
    print_matrix(j)
    print("identity i")
    i = m_identity(4)
    print_matrix(i)
    print("m x m")
    print_matrix(m_mul_m(i, j))
    print("mv x mv")
    print_matrix(mv_mul_mv(i, transpose(j)))
    print('j add i')
    print_matrix(m_add_m(i, j))
    print('j sub i')
    print_matrix(m_sub_m(i, j))
    print('??')



    m = [[1, 1, 1],
         [2, 2, 2],
         [3, 3, 3]]
    n = [[2, 2, 2],
         [2, 2, 2],
         [2, 2, 2]]
    assert m_identity(4) == transpose(m_identity(4)), "identity wrong"
    assert m_sub_m(m, m) == [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
    assert m_add_m(n, n) == [[4, 4, 4], [4, 4, 4], [4, 4, 4]]
    assert m_add_m(m, n) == [[3, 3, 3], [4, 4, 4], [5, 5, 5]]
    assert transpose(m) == [[1, 2, 3], [1, 2, 3], [1, 2, 3]]
    assert m_diagonal(3, 5) == [[5, 0, 0], [0, 5, 0], [0, 0, 5]]
    assert m_mul_s(m, 3) == [[3, 3, 3], [6, 6, 6], [9, 9, 9]]    
    assert m_div_s(m, 2.0) == [[0.5, 0.5, 0.5], [1, 1, 1], [1.5, 1.5, 1.5]]    
    w = [1, 2, 3]
    assert m_mul_v(m, w) == [6, 12, 18]
    w = [[1, 2, 3]] # row vector as a 3x1 matrix
    assert m_mul_m(m, transpose(w)) == [[6], [12], [18]]
    assert m_mul_m(w, m) == [[14, 14, 14]]
    assert m_mul_m(m, n) == [[6, 6, 6], [12, 12, 12], [18, 18, 18]]
    assert mv_mul_mv(m, n) == [[2, 2, 2, ], [4, 4, 4], [6, 6, 6]]
    assert m_sum(n) == 18
    assert m_median(n) == 18 / 9.0
    o = [[1, 2, 3],
         [4, 5, 6],
         [7, 8, 9]]

    #        y  x
    assert o[0][0] == 1
    assert o[0][1] == 2
    assert o[0][2] == 3
    assert o[1][0] == 4
    assert o[1][1] == 5
    assert o[1][2] == 6
    assert o[2][0] == 7
    assert o[2][1] == 8
    assert o[2][2] == 9
    
    p = transpose(o)

    assert p[0][0] == 1
    assert p[0][1] == 4
    assert p[0][2] == 7
    assert p[1][0] == 2
    assert p[1][1] == 5
    assert p[1][2] == 8
    assert p[2][0] == 3
    assert p[2][1] == 6
    assert p[2][2] == 9
    
    assert m_div_s(m, 2.0) == transpose([[0.5, 1, 1.5], [0.5, 1, 1.5], [0.5, 1.0, 1.5]])
    w = [1, 2, 3]
    assert m_mul_v(m, w) == [6, 12, 18]
    # identity
    assert m_dimensions(p) == (3, 3)
    assert m_max_value(p) == 9
    assert m_min_value(p) == 1
    assert m_sum(p) == 45
    assert m_median(p) == 5
    print(m_normalize(p))
    print(m_round(transpose([[1/45.0, 2/45.0, 3/45.0],[4/45.0, 5/45.0, 6/45.0],[7/45.0, 8/45.0, 9/45.0]]), round, 5))
    assert m_round(m_normalize(p), round, 5) == m_round(transpose([[1/45.0, 2/45.0, 3/45.0],[4/45.0, 5/45.0, 6/45.0],[7/45.0, 8/45.0, 9/45.0]]), round, 5)
    m = [[-1, 1, -1], [1, -1, 1], [-1, 1, -1]]
    assert m_pos_part(m) == [[0, 1, 0], [1, 0, 1], [0, 1, 0]]
    assert m_neg_part(m) == [[-1, 0, -1], [0, -1, 0], [-1, 0, -1]]
    assert m_fill(3, 3, 2) == [[2, 2, 2], [2, 2, 2], [2, 2, 2]]
    assert m_round([[1.234, 1.787], [1.49999999, 1.1111]]) == [[1, 2], [1, 1]]
    assert m_round([[1.234, 1.787], [1.49999999, 1.1111]], round, 2) == [[1.23, 1.79], [1.5, 1.11]]
    
    # print(m_stretch(m, 10.0, 255.0))
    # print(m_mul_s(m, -1))
    # print(m_stretch(m_mul_s(m, -1), 10.0, 255.0))
    # print(m_stretch(m, 0.0, 255.0))
    # print(m_stretch([[0.4, -0.1], [0.5, 1.0]], 0.0, 10.0))
    # print([[0, 255, 0], [255, 0, 255], [255, 0, 255]])
    print(m_stretch(m, 0.0, 255.0))
    assert m_stretch(m, 0.0, 255.0) == [[0.0, 255.0, 0.0], [255.0, 0.0, 255.0], [0.0, 255.0, 0.0]]
    assert m_stretch(m, -10.0, 10.0) == [[-10.0, 10.0, -10.0], [10.0, -10.0, 10.0], [-10.0, 10.0, -10.0]]

    
if __name__ == '__main__':
    test_matrix_functions()
