# -*- coding: utf-8 -*-
"""


"""

import operator
from operator import mul as operator_mul
from operator import add as operator_add
from operator import sub as operator_sub
from itertools import izip

# generic implementation, for any dimension

def v_add_v(v, w):
    assert len(v) == len(w), "vector dimensions do not match"
    return map(operator_add, v, w)

def v_add_v_test(v, w):
    assert len(v) == len(w), "vector dimensions do not match"
    return [a + b for a, b in izip(v, w)]

    
def v_sub_v(v, w):
    assert len(v) == len(w), "vector dimensions do not match"
    return map(operator_sub, v, w)

def v_mul_s(v, s):
    return [x * s for x in v]
    
def vv_mul_vv(v, w):
    assert len(v) == len(w), "vector dimensions do not match"
    return map(operator_mul, v, w)
    
def v_add_v_mul_s(v, w, s):
    assert len(v) == len(w), "vector dimensions do not match"
    return map(operator_add, v, [x * s for x in w])
    
def v_dot_v(v, w):
    assert len(v) == len(w), "vector dimensions do not match"
    return sum(map(operator_mul, v, w))

def v2_cross_v2(v, w):
    assert len(v) == len(w), "vector dimensions do not match"
    assert len(v) == 2, "vectors should have dimension 2"
    return v[0] * w[1] - v[1] * w[0]
    
def v3_cross_v3(v, w):
    assert len(v) == len(w), "vector dimensions do not match"
    assert len(v) == 3, "vectors should have dimension 3"
    return [v[1] * w[2] - v[2] * w[1], v[2] * w[0] - v[0] * w[2], v[0] * w[1] - v[1] * w[0]]

    

# dimension 2 explicit vector functions
def add2(vx, vy, wx, wy):
    return vx + wx, vy + wy
    
def sub2(vx, vy, wx, wy):
    return vx - wx, vy - wy
    
def mul2(vx, vy, s):
    return s * vx, s * vy
    
def mulv2(vx, vy, wx, wy):
    return vx * wx, vy * wy
    
def add2_muls(vx, vy, wx, wy, s):
    return vx + wx * s, vy + wy * s
    
def dot2(vx, vy, wx, wy):
    return vx * wx + vy * wy
    
def cross2(vx, vy, wx, wy):
    return vx * wy - vy * wx
    
    
# dimension 3 explicit vector functions
def add3(vx, vy, vz, wx, wy, wz):
    return vx + wx, vy + wy, vz + wz
    
def sub3(vx, vy, vz, wx, wy, wz):
    return vx - wx, vy - wy, vz - wz
    
def mul3(vx, vy, vz, s):
    return s * vx, s * vy, s * vz
    
def mulv3(vx, vy, vz, wx, wy, wz):
    return vx * wx, vy * wy, vz * wz

def add3_muls(vx, vy, vz, wx, wy, wz, s):
    return vx + wx * s, vy + wy * s, vz + wz * s
    
def dot3(vx, vy, vz, wx, wy, wz):
    return vx * wx + vy * wy + vz * wz
    
def cross3(vx, vy, vz, wx, wy, wz):
    return vy * wz - vz * wy, vz * wx - vx * wz, vx * wy - vy * wx

    
def test_vector_functions():
    v = [1, 1, 1]
    w = [1, 2, 3]
    assert v_add_v(w, v) == [2, 3, 4]
    assert v_sub_v(w, v) == [0, 1, 2]
    assert v_mul_s(v, 4) == [4, 4, 4]
    assert vv_mul_vv(v, w) == [1, 2, 3]
    assert v_add_v_mul_s(v, w, 4) == [5, 9, 13]
    assert v_dot_v(v, w) == 6

    assert v3_cross_v3(v, w) == [1, -2, 1]
    assert v3_cross_v3(w, v) == [-1, 2, -1]
    
    v = [1, 1]
    w = [1, 2]
    assert v2_cross_v2(v, w) == 1
    assert v2_cross_v2(w, v) == -1


    assert add2(1, 1, 1, 2) == (2, 3)
    assert sub2(1, 2, 1, 1) == (0, 1)
    assert mul2(1, 1, 4.0) == (4, 4)
    assert mulv2(1, 1, 2, 3) == (2, 3)
    assert add2_muls(1, 1, 2, 3, 4.0) == (9, 13)
    assert dot2(1, 1, 1, 2) == 3
    assert cross2(1, 1, 1, 2) == 1
    assert cross2(1, 2, 1, 1) == -1


    v = [1, 1, 1]
    w = [1, 2, 3]
    assert add3(1, 1, 1, 1, 2, 3) == (2, 3, 4)
    assert sub3(1, 2, 3, 1, 1, 1) == (0, 1, 2)
    assert mul3(1, 1, 1, 4.0) == (4, 4, 4)
    assert mulv3(1, 1, 1, 1, 2, 3) == (1, 2, 3)
    assert add3_muls(1, 1, 1, 1, 2, 3, 4.0) == (5, 9, 13)
    assert dot3(1, 1, 1, 1, 2, 3) == 6
    assert cross3(1, 1, 1, 1, 2, 3) == (1, -2, 1)
    assert cross3(1, 2, 3, 1, 1, 1) == (-1, 2, -1)
    
    # v = [1] * 1000
    # w = [2] * 1000
    # count = 10000
    # import time
    # start = time.time()
    # for i in range(count):
        # v_add_v_test(v, w)
    # dur = time.time() - start
    # print(dur)
    # start = time.time()
    # for i in range(count):
        # v_add_v(v, w)
    # dur = time.time() - start
    # print(dur)

    
if __name__ == '__main__':
    test_vector_functions()