# -*- coding: utf-8 -*-

import math

RED_FACTOR = 0.299
GREEN_FACTOR = 0.587
BLUE_FACTOR = 0.114

# HDTV color factors
# RED_FACTOR = 0.2126
# GREEN_FACTOR = 0.7152
# BLUE_FACTOR = 0.0722

assert RED_FACTOR + GREEN_FACTOR + BLUE_FACTOR < 1.0 + 0.005, "color factors summed should be 1.0 but are " + str(
    RED_FACTOR + GREEN_FACTOR + BLUE_FACTOR)
assert RED_FACTOR + GREEN_FACTOR + BLUE_FACTOR > 1.0 - 0.005, "color factors summed should be 1.0 but are " + str(
    RED_FACTOR + GREEN_FACTOR + BLUE_FACTOR)

GREY_PALETTE = [(i, i, i) for i in range(255)]
COLOR_FACTORS = (int(round(RED_FACTOR * 255)), int(round(GREEN_FACTOR * 255)), int(round(BLUE_FACTOR * 255)))


def convert_surf_to_grey_accurate(color_surf, r_factor=RED_FACTOR, g_factor=GREEN_FACTOR, b_factor=BLUE_FACTOR,
                                  dest=None):
    if dest is None:
        dest = color_surf.copy()

    assert dest is not color_surf, "dest and surf are the same! they should not!"
    assert dest.get_size() == color_surf.get_size(), "surfaces have not same dimensions!"

    sw, sh = color_surf.get_size()
    color_surf_get_at = color_surf.get_at
    dest_set_at = dest.set_at
    color_surf.lock()
    dest.lock()
    for y in range(sh):
        for x in range(sw):
            c = color_surf_get_at((x, y))
            grey = int(round(c[0] * r_factor + c[1] * g_factor + c[2] * b_factor))
            grey = grey if grey <= 255 else 255
            dest_set_at((x, y), (grey, grey, grey, c[3] if len(c) == 4 else 255))
    dest.unlock()
    color_surf.unlock()
    return dest


def convert_surf_to_grey(color_surf, color_factors=COLOR_FACTORS, dest=None, palette=GREY_PALETTE):
    temp1 = color_surf.copy()
    # this adjusts the color values
    temp1.fill(color_factors, None, pygame.BLEND_RGB_MULT)

    if dest is None:
        dest = pygame.Surface(color_surf.get_size(), 0, 8)
        dest.set_palette(palette)

    assert dest is not color_surf, "dest and surf are the same! they should not!"
    assert dest.get_size() == color_surf.get_size(), "surfaces have not same dimensions!"

    # blit 3 times because a color blit to a 8 bit surface does (r+g+b) / 3
    # this is not that accurate, but almost accurate
    dest.blit(temp1, (0, 0), None, pygame.BLEND_RGBA_ADD)
    dest.blit(temp1, (0, 0), None, pygame.BLEND_RGBA_ADD)
    dest.blit(temp1, (0, 0), None, pygame.BLEND_RGBA_ADD)

    return dest


RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
ALPHA = (0, 0, 0, 255)


def get_surf_mult(color_surf, color, destination=None):
    """
    Multiplies the surface with the given color. It does actually this: dest.fill(color, None, pygame.BLEND_RGBA_MULT)
    :param color_surf: the color surface to apply the color to
    :param color: the color to apply
    :param destination: the destination surface (to avoid making a copy
    :return: a surface with the color multiplied
    """
    if destination is None:
        destination = color_surf.copy()
    else:
        destination.blit(color_surf, (0, 0))
    assert destination is not color_surf, "destination and surf are the same! they should not!"
    assert destination.get_size() == color_surf.get_size(), "surfaces have not same dimensions!"
    destination.fill(color, None, pygame.BLEND_RGBA_MULT)
    return destination


def split_to_rgb_channels(color_surf, dest_r=None, dest_g=None, dest_b=None):
    return get_surf_mult(color_surf, RED, dest_r), get_surf_mult(color_surf, GREEN, dest_g), get_surf_mult(color_surf,
                                                                                                           BLUE, dest_b)


def split_to_rgba_channels(color_surf, dest_r=None, dest_g=None, dest_b=None, dest_a=None):
    return get_surf_mult(color_surf, RED, dest_r), get_surf_mult(color_surf, GREEN, dest_g), get_surf_mult(color_surf,
                                                                                                           BLUE,
                                                                                                           dest_b), get_surf_mult(
        color_surf, ALPHA, dest_a)


def n_times_surf(surf, n, destination=None):
    """
    Multiplies the surface n times.
    :param surf: the surface
    :param n: the multiplication factor, float work too
    :param destination: a destination surface to put the result on, should have the same size as the original surface
            and should be black (otherwise the surface will be added n times to this one)
    :return: a surface with n blits of the original surface on it
    """
    if destination is None:
        destination = surf.copy()
    assert destination is not surf, "destination and surf are the same! they should not!"
    assert destination.get_size() == surf.get_size(), "surfaces have not same dimensions!"
    integer = math.floor(n)
    fraction = n - integer

    for k in range(integer):
        destination.blit(surf, (0, 0), None, pygame.BLEND_RGB_ADD)

    f_surf = surf.copy()
    f_surf.fill((fraction * 255.0,) * 3, None, pygame.BLEND_RGB_MULT)

    destination.blit(f_surf, (0, 0), None, pygame.BLEND_RGB_ADD)
    return destination


def invert_surf(surf, invert_alpha=False, dest=None):
    if dest is None:
        dest = surf.copy()
    assert dest is not surf, "dest and surf are the same! they should not!"
    assert dest.get_size() == surf.get_size(), "surfaces have not same dimensions!"
    dest.fill((255,) * (4 if invert_alpha else 3))
    dest.blit(surf, (0, 0), None, pygame.BLEND_RGBA_SUB if invert_alpha else pygame.BLEND_RGB_SUB)
    return dest


def get_black_white_surf(color_surf, s=128):
    # assert dest is not surf, "dest and surf are the same! they should not!"
    # assert dest.get_size() == surf.get_size(), "surfaces have not same dimensions!"

    dest = convert_surf_to_grey(color_surf)
    dest.fill((s, s, s, 0), None, pygame.BLEND_RGBA_SUB)

    # make sure that a pixel (1, 1, 1) gets white (255, 255, 255)
    # 2, 4, 8, 16, 32, 64, 128, 256
    for i in range(8):
        dest.blit(dest, (0, 0), None, pygame.BLEND_RGB_ADD)

    return dest


def blur_surf(surf, radius, dest=None):
    if dest is None:
        dest = surf.copy()
        # dest = pygame.Surface(surf.get_size())
    assert dest is not surf, "dest and surf are the same! they should not!"
    assert dest.get_size() == surf.get_size(), "surfaces have not same dimensions!"
    assert radius > 0
    if radius <= 1:
        return dest  # TODO: really return copy?
    assert radius > 1

    sw, sh = surf.get_size()

    kernel = pygame.Surface((2 * radius,) * 2)
    area = kernel.get_rect()
    pixel_size = (1, 1)
    pixel_surf = pygame.Surface(pixel_size)

    for y in range(sh):
        for x in range(sw):
            pos = (x, y)
            area.center = pos
            kernel.fill((0, 0, 0))
            kernel.blit(surf, (0, 0), area)
            #pixel_surf = pygame.transform.smoothscale(kernel, pixel_size, pixel_surf)
            #dest.blit(pixel_surf, pos)
            #average_color = pygame.transform.average_color(surf, area)
            average_color = pygame.transform.average_color(kernel)
            dest.set_at(pos, average_color)

    return dest


def blur_direction(surf, dx, dy=None):
    dy = dy or dx
    if dx == 0 and dy == 0:
        return surf.copy() # TODO: really return copy?

    return surf

# ------------------------------------------------

import random
import pygame


random.seed(32131564)

pygame.init()

screen_h = 800
screen_w = 1200

screen = pygame.display.set_mode((screen_w, screen_h))

surf_w = 50
surf_h = 50
surf_size = (surf_w, surf_h)

# prepare colorful surface
color_surf = pygame.Surface(surf_size)
# color_surf.fill((0, 0, 0))

# H = [0, 360], S = [0, 100], V = [0, 100], A = [0, 100]. 
color_surf.lock()
for h in range(surf_w):
    for s in range(100):
        c = pygame.Color(0, 0, 0, 255)
        c.hsva = (h, s, 100, 100)
        # c.hsva = (h, 100, s, 100)
        color_surf.set_at((h, s), c)
for h in range(surf_w, 360):
    for s in range(100):
        c = pygame.Color(0, 0, 0, 255)
        c.hsva = (h, s, 100, 100)
        # c.hsva = (h, 100, s, 100)
        color_surf.set_at((h - surf_w, s + 100), c)
color_surf.unlock()


# rint = random.randint
# for i in range(20):
# area = pygame.Rect(rint(10,190), rint(10, 190), rint(5, 100), rint(5, 100))
# col = (rint(0, 255), rint(0, 255), rint(0, 255))
# color_surf.fill(col, area)

color_surf.fill((255, 0, 0), (0, 0, 20, 20))
color_surf.fill((0, 255, 0), (20, 0, 20, 20))
color_surf.fill((0, 0, 255), (40, 0, 20, 20))


# start some work
real_grey = convert_surf_to_grey_accurate(color_surf)
grey2 = convert_surf_to_grey(color_surf)
grey_surf = invert_surf(color_surf)
temp = get_surf_mult(color_surf, COLOR_FACTORS)
tempr = get_black_white_surf(real_grey)
tempg = blur_surf(tempr, 10)

surfs = []
for i in range(20):
    surfs.append(blur_surf(tempr, i + 1))

font = pygame.font.Font(None, 20)
count = 0
running = True
while running:

    # # # more accurate, but much more blits
    # factor_color = ((int)(round(RED_FACTOR * 255)), (int)(round(GREEN_FACTOR * 255)), (int)(round(BLUE_FACTOR * 255)))
    # temp = color_surf.copy()
    # temp.fill(factor_color, None, pygame.BLEND_RGBA_MULT)
    # # red channel
    # tempr = temp.copy()
    # tempr.fill((255, 0, 0), None, pygame.BLEND_RGBA_MULT)
    # # green channel
    # tempg = temp.copy()
    # tempg.fill((0, 255, 0), None, pygame.BLEND_RGBA_MULT)
    # # blue channel
    # tempb = temp.copy()
    # tempb.fill((0, 0, 255), None, pygame.BLEND_RGBA_MULT)
    # # grey surface
    # grey_surf = pygame.Surface((200, 200), 0, 8)
    # grey_surf.set_palette(GREY_PALETTE)
    # # add each colour channel
    # grey_surf.blit(tempr, (0, 0), None, pygame.BLEND_RGBA_ADD)
    # grey_surf.blit(tempg, (0, 0), None, pygame.BLEND_RGBA_ADD)
    # grey_surf.blit(tempb, (0, 0), None, pygame.BLEND_RGBA_ADD)
    # # mult by 3 because blit on grey surf does actually (r+g+b)/3
    # g = grey_surf.copy()
    # grey_surf.blit(g, (0, 0), None, pygame.BLEND_RGBA_ADD)  
    # grey_surf.blit(g, (0, 0), None, pygame.BLEND_RGBA_ADD) 




    # display
    screen.blit(color_surf, (30, 30))
    screen.blit(real_grey, (230, 30))
    screen.blit(grey2, (430, 30))
    screen.blit(grey_surf, (630, 30))

    screen.blit(temp, (630, 230))
    screen.blit(tempr, (30, 230))
    screen.blit(tempg, (230, 230))
    for idx, s in enumerate(surfs):
        x = idx * surf_w
        y = 430
        if x + surf_w > screen_w:
            y += surf_h
            x -= screen_w

        screen.blit(s, (x, y))
        # screen.blit(tempb, (430, 230))

    # tempr = get_black_white_surf(real_grey, count)
    # count += 1
    # count %= 255
    # print(count)

    for event in [pygame.event.wait()]:
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                running = False
        elif event.type == pygame.MOUSEMOTION:

            c = screen.get_at(event.pos)
            text_img = font.render(str(event.pos) + ": " + str(c), 10, (255, 255, 255))
            screen.blit(text_img, (10, screen_h - 20))

    pygame.display.flip()
    screen.fill((255, 0, 255))
