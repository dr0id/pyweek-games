# -*- coding: utf-8 -*-

import pygame


pygame.init()

screen_h = 800
screen_w = 1000

screen = pygame.display.set_mode((screen_w, screen_h))

pointlist = (
    (400, 500),

    # these two combinations do not cause a semgmentation fault
    # (float("nan"), float("inf")),
    # (float("nan"), float("-inf")),


    # the following combinations do cause a segmentation fault
    # (float("inf"), float("nan")), # segmentation fault
    # (float("-inf"), float("nan")), # segmentation fault
    # (float("inf"), float("inf")), # segmentation fault
    # (float("-inf"), float("-inf")), # segmentation fault
    # (float("-inf"), float("inf")), # segmentation fault
    # (float("inf"), float("-inf")), # segmentation fault

    (7.550243920784169, 305.8073443796085),
    (2.6063942851991732e+38, -1.0416278572060449e+39),
    # (3.256830535406936e+38, -7.48640964339544e+38),
    # (6.527608923918239e+37, -8.138008214695233e+38),
    # (2.6063942851991732e+38, -1.0416278572060449e+39),

    (792.8146596910285, 219.20903608134563),
    (1.2648397595965475e+40, 1.7354729617516766e+40),
    # (7.31695699478451e+39, 1.4597096912633259e+40),
    # (1.1655639399163701e+40, 1.143499751364189e+40),
    # (1.2648397595965475e+40, 1.7354729617516766e+40),
)

running = True
while running:


    for event in [pygame.event.wait()]:
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                running = False

    rect = pygame.draw.aalines(screen, (255, 255, 255), False, pointlist)

    pygame.display.flip()
    screen.fill((255, 0, 255))
