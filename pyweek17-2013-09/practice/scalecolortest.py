# -*- coding: utf-8 -*-



# ------------------------------------------------
import random
import math
import sys

import pygame




# print('??', m_max(m))

def n_times_surf(surf, n, dest=None):
    if dest is None:
        dest = surf.copy()
    assert dest is not surf, "dest and surf are the same! they should not!"
    assert dest.get_size() == surf.get_size(), "surfaces have not same dimensions!"
    i = int(math.floor(n))
    f = n - i
    
    for k in range(i-1):
        dest.blit(surf, (0, 0), None, pygame.BLEND_RGB_ADD)
    
    f_surf = surf.copy()
    f_surf.fill((f * 255.0,) * 3, None, pygame.BLEND_RGB_MULT)
    
    dest.blit(f_surf, (0, 0), None, pygame.BLEND_RGB_ADD)
    return dest

_GAUSS_DENOM = (2 * math.pi) ** 0.5
def gaussian(x, sigma, mu=0):
    return 1.0 / (sigma * _GAUSS_DENOM) * math.e ** (-1.0 * (x - mu)**2 / (2.0 * sigma * sigma))
    
def gaussian2d(x, y, sigma, mux=0, muy=0):
    return 1.0 / (sigma * _GAUSS_DENOM) * math.e ** (-1.0 * ((x - mux)**2 + (y - muy)**2) / (2.0 * sigma * sigma))
    
    
def G(x, sigma, mu=0):
    peek = gaussian(mu, sigma, mu)
    factor = 1.0 / peek
    return factor * gaussian(x, sigma, mu)

    
MIRROR = -1
REPEAT = -2
EXPAND = -3
BLACK = -4
WHITE = -5
def convolute_surf(surf, kernel, border=BLACK, dest=None):
    """
    surf: source surface
    kernel: the kernel as a surface, black and white prefered since this will be the weights that are multiplied
    border: default: BLACK
            one of the constants: REPEAT, EXPAND, BLACK, WHITE, MIRROR
            otherwise a color argument to fill the surroundings
    dset: a surface to blit onto instead to create a new one, should have same size as surf
    
    """

    area = kernel.get_rect()
    sw, sh = surf.get_size()
    extended_surf = pygame.Surface((sw + area.width, sh + area.height)) # TODO: other flags
    offsetx = area.width // 2
    offsety = area.height // 2
    
    if dest is None:
        dest = pygame.Surface(surf.get_size())
        
    assert dest is not surf, "dest and surf are the same! they should not!"
    assert dest.get_size() == surf.get_size(), "surfaces have not same dimensions!"


    # TODO: border conditions: REPEAT, EXPAND, WHITE
    if border == BLACK:
        pass
    elif border == REPEAT:
        extended_surf.blit(surf, (offsetx, 0), (0, sh - offsety, sw, offsety))
        extended_surf.blit(surf, (offsetx, sh + offsety), (0, 0, sw, offsety))
        extended_surf.blit(surf, (0, offsety), (sw - offsetx, 0, offsetx, sh))
        extended_surf.blit(surf, (sw + offsetx, offsety), (0, 0, offsetx, sh))
        
        extended_surf.blit(surf, (0, 0), (sw - offsetx, sh - offsety, offsetx, offsety))
        extended_surf.blit(surf, (sw + offsetx, 0), (0, sh - offsety, offsetx, offsety))
        extended_surf.blit(surf, (sw + offsetx, sh + offsety), (0, 0, offsetx, offsety))
        extended_surf.blit(surf, (0, sh + offsety), (sw - offsetx, 0, offsetx, offsety))
    elif border == MIRROR:
        for size, source, flipx, flipy, pos in [
                                        ((offsetx, sh), (0, 0, offsetx, sh), True, False, (0, offsety)),
                                        ((offsetx, sh), (sw - offsetx, 0, offsetx, sh), True, False, (sw + offsetx, offsety)),
                                        ((sw, offsety), (0, 0, sw, offsety), False, True, (offsetx, 0)),
                                        ((sw, offsety), (0, sh - offsety, sw, offsety), False, True, (offsetx, sh + offsety)),
                                        ((offsetx, offsety), (0, 0, offsetx, offsety), True, True, (0, 0)),
                                        ((offsetx, offsety), (sw - offsetx, 0, offsetx, offsety), True, True, (sw + offsetx, 0)),
                                        ((offsetx, offsety), (sw - offsetx, sh - offsety, offsetx, offsety), True, True, (sw + offsetx, sh + offsety)),
                                        ((offsetx, offsety), (0, sh - offsety, offsetx, offsety), True, True, (0, sh + offsety)),
                                        ]:
            temp = pygame.Surface(size)
            temp.blit(surf, (0, 0), source)
            temp = pygame.transform.flip(temp, flipx, flipy)
            extended_surf.blit(temp, pos)
        
    
    elif border == EXPAND:
        extended_surf.fill(surf.get_at((0, 0)), (0, 0, offsetx, offsety))
        extended_surf.fill(surf.get_at((sw - 1, 0)), (sw + offsetx, 0, offsetx, offsety))
        extended_surf.fill(surf.get_at((sw - 1, sh - 1)), (sw + offsetx, sh + offsety, offsetx, offsety))
        extended_surf.fill(surf.get_at((0, sh - 1)), (0, sh + offsety, offsetx, offsety))
        for ssize, source_rect, resize, pos in [
                                                ((1, sh), (0, 0, 1, sh), (offsetx, sh), (0, offsety)),
                                                ((1, sh), (sw - 1, 0, 1, sh), (offsetx, sh), (sw + offsetx, offsety)),
                                                ((sw, 1), (0, 0, sw, 1), (sw, offsety), (offsetx, 0)),
                                                ((sw, 1), (0, sh - 1, sw, 1), (sw, offsety), (offsetx, sh + offsety)),
                                                ]:
            s = pygame.Surface(ssize)
            s.blit(surf, (0, 0), source_rect)
            s = pygame.transform.scale(s, resize)
            extended_surf.blit(s, pos)
    elif border == WHITE:
        extended_surf.fill((255, ) * 3)
    else:
        extended_surf.fill(border)
    extended_surf.blit(surf, (offsetx, offsety))

    # convolution, multiply with kernel and sum the pixels through smoothscale
    temp = kernel.copy()
    pixel_size = (1, 1)
    pixel_surf = pygame.Surface(pixel_size)
    
    for y in range(offsety, sh + offsetx):
        for x in range(offsetx, sw + offsetx):
            pos = (x, y)
            area.center = pos
            # convolute
            temp.blit(extended_surf, (0, 0), area)
            temp.blit(kernel, (0, 0), None, pygame.BLEND_RGB_MULT)
            # pygame.transform.smoothscale(temp, pixel_size, pixel_surf)
            # # pygame.transform.scale(temp, pixel_size, pixel_surf)
            # dest.blit(pixel_surf, (x, y))
            
            average_color = pygame.transform.average_color(temp)
            dest.set_at(pos, average_color)
            
    return dest
    
def create_gaussian_kernel_values(diameter, sigma=None):
    radius = diameter // 2
    if sigma is None:
        sigma = 0.4 * radius
    values = []
    for y in range(diameter):
        row = []
        for x in range(diameter):
            val = gaussian2d(x, y, sigma, radius, radius)
            row.append(val)
        values.append(row)
    return values

def weight_kernel_values(values, weight):
    return [[v * weight for v in row] for row in values]
    
def normalize_kernel_values(values, n=1.0):
    su = n / sum(values)
    return weight_kernel_values(values, su)
    
def clamp_top_values(values, top):
    return [[top if v>top else v for v in row] for row in values]

def create_kernel_surf(values, width, height):
    kernel = pygame.Surface((width, height)) # TODO: flags
    # lines = [values[v * width : width * (v + 1)] for v in range(len(values) // width)]
    for y, row in enumerate(values):
        for x, val in enumerate(row):
            v = 255 if val > 255 else val
            # TODO: check negative values too?
            kernel.set_at((x, y), (v,) * 3)
    return kernel
    
def blur_surf(surf, kernel, dest=None):
    if dest is None:
        dest = pygame.Surface(surf.get_size())
    assert dest is not surf, "dest and surf are the same! they should not!"
    assert dest.get_size() == surf.get_size(), "surfaces have not same dimensions!"
    
    sw, sh = surf.get_size()
    
    kernelsurf = pygame.Surface(kernel.get_size())
    area = kernelsurf.get_rect()
    pixel_size = (1, 1)
    pixel_surf = pygame.Surface(pixel_size)
    surf_rect = dest.get_rect()
    
    # kernel = pygame.Surface((diameter,) * 2)
    # kw, kh = kernelsurf.get_size()
    # kw2 = kw // 2.0
    # kh2 = kh // 2.0
    # vals = []
    # # sigma =  0.84089642 * kw2
    # sigma =  0.4 * kw2
    # # sigma =  1.55
    # peek = gaussian(0, sigma)
    # print('?', kw, kh, kw2, kh2, len(vals))
    # for y in range(kh):
        # for x in range(kw):
            # d = math.hypot(x - kw2, y - kh2)
            # g = gaussian(d, sigma)
            # v = g
            # vals.append(v)
            # print("{0}\t{1}\t{2}\t{3}\t{4:8}\t{5:8}\t{6:8}".format(x, y, x - kw2, y - kh2, round(d, 8), v, round(g, 8)))
            
    # # normalize
    # # su = sum(vals)
    # # print('??', su)
    # # f1 = 1.0 / su
    # # vals = [f1 * v for v in vals]
    
    # f2 = 255.0 / max(vals)
    # # f3 = f1 / f2 # max(vals) / su 
    
    # # # f4 = 255.0 / 255.0 # 3 diameter
    # # f4 = 245.0 / 255.0 # 5 diameter
    # # f4 = 230.0 / 255.0 # 7 diameter
    # # f4 = 223.0 / 255.0 # 9 diameter
    # # f4 = 218.0 / 255.0 # 11 diameter
    # # f4 = 213.0 / 255.0 # 13 diameter
    # # f4 = 212.0 / 255.0 # 15 diameter
    # # f4 = 210.0 / 255.0 # 21 diameter
    
    # # vals = [255.0 * f4 * f2 * v for v in vals]
    # vals = [f2 * v for v in vals]
    
    # # su = sum(vals)
    # su = max(vals)
    # print('??', su)
    
    # for i in [vals[v*kw:v*kw+kw] for v in range(len(vals) // kw)]: print(i)
    
    # for y, row in enumerate([vals[v*kw:v*kw+kw] for v in range(len(vals) // kw)]):
        # for x, val in enumerate(row):
            # # val *= 255.0
            # v = 255 if val > 255 else val
            # print('!', v)
            # kernel.set_at((x, y), (v, ) * 3)

    for y in range(sh):
        for x in range(sw):
            pos = (x, y)
            
            area = kernelsurf.get_rect(center=pos)
            # border
            area = area.clip(surf_rect)
            
            kernelsurf.blit(surf, (0, 0), area)
            
            kernelsurf.blit(kernel, (0, 0), None, pygame.BLEND_RGB_MULT)
            
            # pygame.transform.smoothscale(kernelsurf, pixel_size, pixel_surf)
            # dest.blit(pixel_surf, pos)
            
            average_color = pygame.transform.average_color(kernelsurf)
            dest.set_at(pos, average_color)

            # surfs.append(pygame.transform.scale(kernelsurf, (diameter * 4, ) * 2))
            # surfs.append(pixel_surf.copy())
            
    dest2 = dest.copy()
    # for i in range(3):
        # dest2.blit(dest, (0, 0), None, pygame.BLEND_RGB_ADD)
    # dest2.blit(dest, (0, 0), None, pygame.BLEND_RGB_ADD)
    # n = 128.0/31.0
    n = 5
    dest2 = n_times_surf(dest, n)
            
    # kernelsurf.fill((255,)*3)
    # kernelsurf.blit(kernel, (0, 0), None, pygame.BLEND_RGB_MULT)
    
    return dest2
    
    # m = len(surfs) // 2
    # return surfs[m:m+100] + [dest2] + [pygame.transform.scale(kernelsurf, (4*kw, 4*kh))]


def split_surf_to_rgba_matrices(surf):
    """
    Returns a matrix for each channel r, g, b, a.
    """
    p = pygame.PixelArray(surf)
    r, g, b, a = zip(*[surf.unmap_rgb(px) for row in p for px in row])
    del p # unlock surf
    sw, sh = surf.get_size()
    return _matrix_from_flat(r, sw, sh), \
            _matrix_from_flat(g, sw, sh), \
            _matrix_from_flat(b, sw, sh), \
            _matrix_from_flat(a, sw, sh)
            
def rgba_matrices_to_surf(r, g, b, a, surf=None):
    w = len(r[0])
    h = len(r)
    assert w == len(g[0]), "g matrix width does not match"
    assert w == len(g), "g matrix height does not match"
    assert w == len(b[0]), "b matrix width does not match"
    assert w == len(b), "b matrix height does not match"
    assert w == len(a[0]), "a matrix width does not match"
    assert w == len(a), "a matrix height does not match"
    arr = zip(_matrix_to_flat(r), \
                _matrix_to_flat(g), \
                _matrix_to_flat(b), \
                _matrix_to_flat(a))
    if surf is None:
        s = pygame.Surface((w, h))
    else:
        sw, sh = surf.get_size()
        assert sw == w, "surface width does not match matrix size"
        assert sh == h, "surface height does not match matrix size"
    arr = [s.map_rgb(pygame.Color(*c)) for c in arr]
    m = _matrix_from_flat(arr, w, h, tuple)
    p = pygame.PixelArray(s)
    for i, row in enumerate(m):
        p[:, i] = row
    # TODO: test if this surface is locked
    return p.make_surface()
    
    
def _matrix_from_flat(arr, width, height, type=list):
    return type([type(arr[h*width:h*width+width]) for h in range(height)])
    
def _matrix_to_flat(arr):
    return [x for row in arr for x in row]

pygame.init()
    
    
def main():
    screen_h = 800
    screen_w = 1000

    screen = pygame.display.set_mode((screen_w, screen_h))

    surf_w = 200
    surf_h = 200
    surf_size = (surf_w, surf_h)

    # prepare colorful surface
    color_surf = pygame.Surface(surf_size)
    color_surf.lock()
    for h in range(surf_w):
        for s in range(100):
            c = pygame.Color(0, 0, 0, 255)
            c.hsva = (h, s, 100, 100)
            # c.hsva = (h, 100, s, 100)
            color_surf.set_at((h,s), c)
    for h in range(surf_w, 360):
        for s in range(100):
            c = pygame.Color(0, 0, 0, 255)
            c.hsva = (h, s, 100, 100)
            # c.hsva = (h, 100, s, 100)
            color_surf.set_at((h - surf_w,s + 100), c)
    color_surf.unlock()            

    checkboard = pygame.Surface(surf_size)
    for y in range(surf_h):
        for x in range(surf_w):
            color = (0 if (x + y) % 2 == 1 else 255, ) * 3
            # color = (128, ) *3
            checkboard.set_at((x, y), color)

    for i in range(500):
        w = random.randint(5, 50)
        h = random.randint(5, 50)
        r = pygame.Rect(random.randint(0, surf_w - w), random.randint(0, surf_h - h), w, h)
        r = pygame.Rect(random.randint(-10, surf_w), random.randint(-10, surf_h), w, h)
        checkboard.fill((255-32 if random.random() > 0.5 else 32, ) * 3, r)
        
    checkboard.fill((25,)*3, pygame.Rect(75, 25, 50, 50))
    checkboard.fill((255,)*3, pygame.Rect(95, 45, 10, 10))
    checkboard.fill((0,)*3, pygame.Rect(95, 145, 10, 10))

    surfaces = []
    
    surfaces.append(checkboard)
    diameter = 7
    vals = create_gaussian_kernel_values(diameter)
    vals = weight_kernel_values(vals, 5*255/max([v for row in vals for v in row]))
    vals = clamp_top_values(vals, 255)
    kernel = create_kernel_surf(vals, diameter, diameter)
    

    surfaces.append(blur_surf(checkboard, kernel))
    # box = pygame.Surface((3, 3))
    # box.fill((255, ) * 3)
    
    # surfaces.extend([n_times_surf(convolute_surf(checkboard, kernel), 5)])
    surfaces.extend([convolute_surf(checkboard, kernel)])
    surfaces.extend([kernel])
    
    # # surfaces.extend([n_times_surf(convolute_surf(checkboard, kernel), 5)])
    # surfaces.extend([n_times_surf(convolute_surf(checkboard, kernel), 5)])
    # surfaces.extend([n_times_surf(convolute_surf(checkboard, kernel), 5)])
    # surfaces.extend([convolute_surf(checkboard, box, MIRROR)])
    # surfaces.extend([convolute_surf(surfaces[-1], box, MIRROR)])

    # edgesf = pygame.Surface((3, 3))
    # edgesf.fill((0,) * 3)
    # edgesf.set_at((0, 0), (253,)*3)
    # edgesf.set_at((0, 1), (253,)*3)
    # edgesf.set_at((0, 2), (253,)*3)
    # edgesf.set_at((0, 2), (254,)*3)
    # edgesf.set_at((2, 2), (128-64,)*3)
    # edgesf.set_at((2, 0), (128+64,)*3)
    # edgesf.set_at((0, 2), (128+64,)*3)
    # edgesf.set_at((1, 0), (255,)*3)
    # edgesf.set_at((1, 1), (255,)*3)
    # edgesf.set_at((1, 2), (255,)*3)

    # checkboard.fill((128,)*3, None, pygame.BLEND_RGB_ADD)
    # # neg = convolute_surf(checkboard, edgesf, MIRROR)

    # edgesf.fill((0,) * 3)
    # edgesf.set_at((2, 0), (253,)*3)
    # edgesf.set_at((2, 1), (253,)*3)
    # edgesf.set_at((2, 2), (253,)*3)
    # # neg2 = convolute_surf(checkboard, edgesf, MIRROR)

    # edgesf.fill((0,) * 3)
    # edgesf.set_at((1, 0), (254,)*3)
    # edgesf.set_at((1, 1), (254,)*3)
    # edgesf.set_at((1, 2), (254,)*3)
    # edgesf.set_at((1, 2), (255,)*3)
    # # pos = convolute_surf(checkboard, edgesf, MIRROR)
    # # neg.blit(neg2, (0, 0), None, pygame.BLEND_RGB_ADD)
    # # pos.blit(neg, (0, 0), None, pygame.BLEND_RGB_SUB)

    # # pos = n_times_surf(pos, 5)
    # # surfaces.extend([edgesf, pos])

    # for i in range(1, 200, 10):
        # size = (int(surf_w / i), int(surf_h / i))
        # surfaces.append(pygame.transform.smoothscale(color_surf, size))
    # for i in range(1, 200, 10):
        # size = (int(surf_w / i), int(surf_h / i))
        # surfaces.append(pygame.transform.smoothscale(checkboard, size))

    # print('-'*10)
    # rmin = -20
    # rmax = 21
    # sigma = 0.5
    # for i in range(rmin, rmax):
        # print(i, gaussian(i, sigma))
    # print('-'*10)
    # print(sum([gaussian(i, sigma) for i in range(rmin, rmax)]))

    font = pygame.font.Font(None, 20)
    count = 0
    running = True
    while running:

        x = 0
        y = 0
        for surf in surfaces:
            sw, sh = surf.get_size()
            if x + sw > screen_w:
                x = 0
                y += sh
            screen.blit(surf, (x, y))
            x += sw
            
            

        
        for event in [pygame.event.wait()]:
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
            elif event.type == pygame.MOUSEMOTION:
                c = screen.get_at(event.pos)
                text_img = font.render(str(event.pos) + ": " + str(c), 10, (255, 255, 255))
                screen.blit(text_img, (10, screen_h - 20))
                
        pygame.display.flip()
        screen.fill((255, 0, 255))

def main2():
    w = 10
    h = 10
    s = pygame.Surface((w, h))
    for y in range(h):
        for x in range(w):
            s.set_at((x, y), (x, y, 128))
    r, g, b, a = split_surf_to_rgba_matrices(s)
    print r
    print g
    print b
    print a
    for y in range(h):
        for x in range(w):
            r[x][y] = x
            g[x][y] = y
            b[x][y] = 3
            a[x][y] = 4
            
    s = rgba_matrices_to_surf(r, g, b, a)
    
        
if __name__ == "__main__":
    main()

