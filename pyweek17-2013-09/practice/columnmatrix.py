# -*- coding: utf-8 -*-
"""
One line matrix operations using a column matrix, e.g. 

m = [[1, 
      4, 
      7], [2, 
           5, 
           8], [3, 
                6, 
                9]]

member access by m[x][y]:

m[0][2] == 7

defining a column vector as a 1 x 3 matrix:

v = [[1, 
      2, 
      3]]

or as a row vector:

w = [[1], [2], [3]]


tricks:

    define a vector as column vector and use it as row vector:
    col_v = [[1, 
              2, 
              3]]
    row_v = transpose(col_v)    # [[1], [2], [3]]
    
    define a matrix as column matrix but use it as row matrix
    col_m = [[1, 2, 3],     # 1  4  7
             [4, 5, 6],     # 2  5  8
             [7, 8, 9]]     # 3  6  9

    row_m = transpose(col_m)    # 1  2  3
                                # 4  5  6
                                # 7  8  9

                                
                                




                                
                                
                                
                                
                                
                                
                                
"""

import sys
import operator
from operator import mul as operator_mul
from operator import add as operator_add
from operator import sub as operator_sub

def m_sub_m(m1, m2):
    assert len(m1[0]) == len(m2[0]) and len(m1) == len(m2), "matrix dimensions do not match"
    return map(lambda m1_col, m2_col: map(operator_sub, m1_col, m2_col), m1, m2)

def m_add_m(m1, m2):
    assert len(m1[0]) == len(m2[0]) and len(m1) == len(m2), "matrix dimensions do not match"
    return map(lambda m1_col, m2_col: map(operator_add, m1_col, m2_col), m1, m2)

def transpose(m1):
    return map(lambda *values: list(values), *m1)

def m_diagonal(dim, value):
    assert dim > 0, "dimension should be positive value"
    assert isinstance(dim, int), "dimension should be an integer"
    return [([0.0]*i) + [value] + ([0.0]*(dim-i-1)) for i in range(dim)]
    
def m_mul_s(m1, s):
    return [[v * s for v in col] for col in m1]

def m_mul_m(m1, m2):
    assert len(m1) == len(m2[0]), "matrix dimensions do not match"
    return [[sum(map(operator_mul, m1_col, m2_col)) for m1_col in map(lambda *vals: vals, *m1)] for m2_col in m2]

def mv_mul_mv(m1, m2):
    assert len(m1[0]) == len(m2[0]) and len(m1) == len(m2), "matrix dimensions do not match"
    return map(lambda m1_col, m2_col: map(operator_mul, m1_col, m2_col), m1, m2)

    
    
def m_div_s(m1, s):
    assert isinstance(s, float), "scalar should be float"
    return [[v / s for v in col] for col in m1]
    
def m_mul_v(m1, v):
    assert len(m1[0]) == len(v), "vector and matrix dimensions do not match"
    return [sum(map(operator_mul, col, v)) for col in map(lambda *vals: vals, *m1)]

def m_identity(dim):
    return m_diagonal(dim, 1.0)
    
    
def m_dimensions(m1):
    return len(m1), len(m1[0])
    
def m_max_value(m1):
    return max(v for col in m1 for v in col)
    
def m_min_value(m1):
    return min(v for col in m1 for v in col)

def m_sum(m1):
    return sum(v for col in m1 for v in col)
    
def m_median(m1):
    return sum(v for col in m1 for v in col) / float(len(m1) * len(m1[0]))
    
def m_normalize(m1):
    return m_mul_s(m1, 1.0 / sum(v for col in m1 for v in col))

def m_pos_part(m1):
    return [[v if v > 0 else 0.0 for v in col] for col in m1]
    
def m_neg_part(m1):
    return [[v if v < 0 else 0.0 for v in col] for col in m1]
    
def m_stretch(m1, min_value, max_value):
    m_max = m_max_value(m1)
    m_min = m_min_value(m1)
    factor = (max_value - min_value) / (m_max - m_min)
    return [[(v - m_min) * factor + min_value for v in col] for col in m1]
    
def m_fill(dimx, dimy, value):
    assert dimx > 0, "dimension x should be positive value"
    assert dimy > 0, "dimension y should be positive value"
    assert isinstance(dimx, int), "dimension x should be an integer"
    assert isinstance(dimy, int), "dimension y should be an integer"
    return [[value]*dimy for i in range(dimx)]
    
def m_round(m1, func=round, *args):
    return [[func(v, *args) for v in col] for col in m1]

def print_matrix(m1, prefix="", space="\t", newline="\n", postfix=""):
    sys.stdout.write(prefix + newline.join(space.join(str(val) for val in col) + "\n" for col in map(lambda *vals: list(vals), *m1)) + postfix)



        
    

def test_matrix_functions():    
    m = []
    for x in range(4):
        r = []
        for y in range(4):
            r.append(x+y+x^y)
        m.append(r)

    print('??')
    print('m')
    print_matrix(m)
    j = transpose(m)
    print("j: transposed m")
    print_matrix(j)
    print("identity i")
    i = m_identity(4)
    print_matrix(i)
    print("m x m")
    print_matrix(m_mul_m(i, j))
    print("mv x mv")
    print_matrix(mv_mul_mv(i, transpose(j)))
    print('j add i')
    print_matrix(m_add_m(i, j))
    print('j sub i')
    print_matrix(m_sub_m(i, j))
    print('??')


    
    m = [[1, 2, 3], [1, 2, 3], [1, 2, 3]]
    
    
    n = [[2, 2, 2], [2, 2, 2], [2, 2, 2]]
         
    assert m_identity(4) == transpose(m_identity(4)), "identity wrong"
    assert m_sub_m(m, m) == [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
    assert m_add_m(n, n) == [[4, 4, 4], [4, 4, 4], [4, 4, 4]]
    assert m_add_m(m, n) == [[3, 4, 5], [3, 4, 5], [3, 4, 5]]
    assert transpose(m) == [[1, 1, 1], [2, 2, 2], [3, 3, 3]]
    assert m_diagonal(3, 5) == [[5, 0, 0], [0, 5, 0], [0, 0, 5]]
    assert m_mul_s(m, 3) == [[3, 6, 9], [3, 6, 9], [3, 6, 9]]    
    w = [[1, 2, 3]] # column vector as a 1x3 matrix
    assert m_mul_m(m, w) == [[6, 12, 18]]
    assert m_mul_m(transpose(w), m) == [[14], [14], [14]]
    assert m_mul_m(m, n) == [[6, 12, 18], [6, 12, 18], [6, 12, 18]]
    assert mv_mul_mv(m, n) == [[2, 4, 6, ], [2, 4, 6], [2, 4, 6]]
    assert m_sum(n) == 18
    assert m_median(n) == 18 / 9.0
    o = [[1, 
          4, 
          7], [2, 
               5, 
               8], [3, 
                    6, 
                    9]]

    #        x  y
    assert o[0][0] == 1
    assert o[0][1] == 4
    assert o[0][2] == 7
    assert o[1][0] == 2
    assert o[1][1] == 5
    assert o[1][2] == 8
    assert o[2][0] == 3
    assert o[2][1] == 6
    assert o[2][2] == 9
    
    p = transpose(o)
    
    assert p[0][0] == 1
    assert p[0][1] == 2
    assert p[0][2] == 3
    assert p[1][0] == 4
    assert p[1][1] == 5
    assert p[1][2] == 6
    assert p[2][0] == 7
    assert p[2][1] == 8
    assert p[2][2] == 9
    
    assert m_div_s(m, 2.0) == [[0.5, 1, 1.5], [0.5, 1, 1.5], [0.5, 1.0, 1.5]]    
    w = [1, 2, 3]
    assert m_mul_v(m, w) == [6, 12, 18]
    # identity
    assert m_dimensions(p) == (3, 3)
    assert m_max_value(p) == 9
    assert m_min_value(p) == 1
    assert m_sum(p) == 45
    assert m_median(p) == 5
    print(m_normalize(p))
    print(m_round([[1/45.0, 2/45.0, 3/45.0],[4/45.0, 5/45.0, 6/45.0],[7/45.0, 8/45.0, 9/45.0]], round, 5))
    assert m_round(m_normalize(p), round, 5) == m_round([[1/45.0, 2/45.0, 3/45.0],[4/45.0, 5/45.0, 6/45.0],[7/45.0, 8/45.0, 9/45.0]], round, 5)
    m = [[-1, 1, -1], [1, -1, 1], [-1, 1, -1]]
    assert m_pos_part(m) == [[0, 1, 0], [1, 0, 1], [0, 1, 0]]
    assert m_neg_part(m) == [[-1, 0, -1], [0, -1, 0], [-1, 0, -1]]
    assert m_fill(3, 3, 2) == [[2, 2, 2], [2, 2, 2], [2, 2, 2]]
    assert m_round([[1.234, 1.787], [1.49999999, 1.1111]]) == [[1, 2], [1, 1]]
    assert m_round([[1.234, 1.787], [1.49999999, 1.1111]], round, 2) == [[1.23, 1.79], [1.5, 1.11]]
    
    # print(m_stretch(m, 10.0, 255.0))
    # print(m_mul_s(m, -1))
    # print(m_stretch(m_mul_s(m, -1), 10.0, 255.0))
    # print(m_stretch(m, 0.0, 255.0))
    # print(m_stretch([[0.4, -0.1], [0.5, 1.0]], 0.0, 10.0))
    # print([[0, 255, 0], [255, 0, 255], [255, 0, 255]])
    print(m_stretch(m, 0.0, 255.0))
    assert m_stretch(m, 0.0, 255.0) == [[0.0, 255.0, 0.0], [255.0, 0.0, 255.0], [0.0, 255.0, 0.0]]
    assert m_stretch(m, -10.0, 10.0) == [[-10.0, 10.0, -10.0], [10.0, -10.0, 10.0], [-10.0, 10.0, -10.0]]
    

if __name__ == '__main__':
    test_matrix_functions()
