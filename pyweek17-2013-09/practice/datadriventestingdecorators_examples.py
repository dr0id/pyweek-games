# -*- coding: utf-8 -*-
# 
# New BSD license
#  
# Copyright (c) DR0ID
# This file is part of HG_pyweek_games
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This module contains examples for the data driven test decorators. It is intentional, that there is one failure and one
error if the tests are run.

Following is the output us such a test run::

    .FE.......
    ======================================================================
    ERROR: test_add_DDT_args__1__3_ (__main__.ArithmeticTests)
    ----------------------------------------------------------------------
    Traceback (most recent call last):
      File "...\datadriventestingdecorators.py", line 262, in wrapped_parametrized_test_method
        _verify_method_call(function_kwargs, self)
      File "...\datadriventestingdecorators.py", line 215, in _verify_method_call
        function.__name__))
    UserWarning: method test_add returned a value but no 'result' keyword has been provided

    ======================================================================
    FAIL: test_add_DDT_args__1__2____result___5_ (__main__.ArithmeticTests)
    ----------------------------------------------------------------------
    Traceback (most recent call last):
      File "...\datadriventestingdecorators.py", line 260, in wrapped_parametrized_test_method
        _verify_methods_return_value(function_kwargs, self)
      File "...\datadriventestingdecorators.py", line 207, in _verify_methods_return_value
        raise AssertionError('expected: %r != actual: %r' % (expected_result, actual))
    AssertionError: expected: 5 != actual: 3

    ----------------------------------------------------------------------
    Ran 10 tests in 0.029s

    FAILED (failures=1, errors=1)



"""
from __future__ import print_function
import unittest

import datadriventestingdecorators as ddt


__version_number__ = (0, 0, 0, 0)
__version__ = ".".join([str(num) for num in __version_number__])

__author__ = 'dr0iddr0id {at} gmail [dot] com'
__copyright__ = "DR0ID @ 2014"
__credits__ = ["DR0ID"]     # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []    # list of public visible parts of this module 


@ddt.use_data_driven_testing_decorators
class ArithmeticTests(unittest.TestCase):
    """
    These class demonstrates the usage of the test_case decorator.
    It will be used in different ways on the methods. For the details see
    the comments behind the decorators.
    """

    @ddt.test_case(1, 4, 4)
    @ddt.test_case(2, 6, 3)
    @ddt.test_case(3, 6, 2)
    @ddt.test_case(3, 6, 0, expected_exception=ZeroDivisionError) # expecting the ZeroDivisionError
    def test_division(self, expected, numerator, denominator):
        """
        Simple test method that verifies the division. This test method gets the expected value as an argument.
        :param expected: expected result
        :param numerator: the numerator of the division
        :param denominator: the denominator of the division
        """
        # arrange
        # act
        actual = numerator / denominator
        # verify
        self.assertEqual(expected, actual)

    @ddt.test_case(1, 2, result=5)  # fail because result is different
    @ddt.test_case(1, 2, result=3)
    @ddt.test_case(1, 3)  # error because result=x is missing but method has a return value
    def test_add(self, a, b):
        """
        Simple test method that tests the addition of two numbers. Note that the method returns the calculated
        value. To test this value note the 'result=...' argument in the test_case decorator. Note also, that two out
        of the three test_cases do fail.
        :param a: first argument for the addition
        :param b: second argument for the addition
        :return: the added value
        """
        return a + b


def example_csv_file_parser(file_name, *args, **kwargs):
    """
    Example generator function that reads from a csv file. Yields the values from the file.
    There are better ways to read from a file, this implementation is kept simple.
    :param file_name: name of the file to read
    :param args: the argument list, it is ignored
    :param kwargs: the keyword argument list, it is ignored
    """
    import csv

    with open(file_name, "r") as csv_file:
        reader = csv.reader(csv_file)
        for row in reader: # maybe file should not stay opened until all values have been yielded
            if not row[0].startswith('#'):
                args = (None if row[0] == "None" else int(row[0]), int(row[1]), int(row[2]))
                expected_exception = eval(row[3])  # eval(...) very evil... I know!
                test_name = row[4].strip()
                kwargs = {ddt.EXPECTED_EXCEPTION: expected_exception, ddt.TEST_NAME: test_name}
                yield (args, kwargs)


@ddt.use_data_driven_testing_decorators
class ArithmeticUsingCsvFileTests(unittest.TestCase):
    """
    This class will demonstrate the usage of the test_case_from_generator decorator.
    """

    @ddt.test_case_from_generator(example_csv_file_parser, "sampledata.csv")
    def test_division(self, expected_result, dividend, divisor):
        """
        This is the test method which will be tested against all values of the generator.
        :param expected_result: the expected result
        :param dividend: the dividend
        :param divisor: the divisor
        """
        self.assertEqual(expected_result, dividend / divisor)

if __name__ == '__main__':
    # unittest.main(verbosity=10)
    unittest.main()
