# -*- coding: utf-8 -*-
"""
bla
"""

import math


EPSILON = 10 ** -4


def clip_segment_against_aabb(px, py, ex, ey, x_min, y_min, x_max, y_max):
    """
    Clips a line segment on an AABB (axis aligned bounding box).

    :param px:
        x-coordinate of the starting point of the segment
    :param py:
        y-coordinate of the starting point of the segment
    :param ex:
        x-coordinate of the end point of the segment
    :param ey:
        y-coordinate of the end point of the segment
    :param x_min:
        the minimal x coordinate of the AABB
    :param y_min:
        the minimal y coordinate of the AABB
    :param x_max:
        the maximal x coordinate of the AABB
    :param y_max:
        the maximal y coordinate of the AABB

    :Returns:
        A tuple like (bool, ux, uy, wx, wy)
        Start point and end point of the clipped line segment if there
        is a line segment within the axis aligned bounding box. Otherwise 
        returns none.
        

    Explanation::

        +------->       \|            |                                   ¦            ¦
        |       x        \            |                                   ¦            ¦
        |                |\           |                                   ¦            ¦
        |                | \          |                                   ¦         ^  ¦
        V y              |  \         |                                   ¦      N1 |  ¦
                         |   \(px,py) |                                   ¦         |  ¦
            (x_min,y_min)|    x       |                      (x_min,y_min)¦         |  ¦
            -------------+-----\------+-------------         -------------+======>-----+----------
                         |      \     |                                   |   U1       |
                         |       \    |                                   |            |
                         |        \   |                                   | U2      U4 |
                         |         \  |                                   V            V
                         |          \ |                                   ¦            ¦
                         |           \|                                   ¦            ¦
                         |            \                            <======¦            ¦=======>
                         |            |\                             N2   ¦            ¦   N4
                         |            | \                                 ¦   U3       ¦
            -------------+------------+--\----------         -------------+======>-----+----------
                         |   (x_max,  |   \                               ¦         |  ¦(x_max,y_max)
                         |     y_max) |    \                              ¦         |  ¦
                         |            |     \ (ex,ey)                     ¦      N3 |  ¦
                         |            |      x                            ¦         V  ¦
                         |            |       \                           ¦            ¦

                        segment intersecting the AABB                     normal vectors Ni and Ui vectors


        using a line in parametric form:

            P + t * V      where    P start point (px, py)
                                    t parameter in range [0, 1]
                                    V as direction vector (vx, vy) = (ex-px,ey-py)


        intersection of two parametric lines:

            P + t * V = Q + s * U

                =>   s = (V cross (P - Q)) / (V cross U) = (vx * (py - qy) - vy * (px - qx)) / (vx * uy - vy * ux)

                =>   t = (qy - py + s * uy) / vy


        the formulas for the 4 intersection points Xi with their ti parameters for the 4 sides of the AABB
        (because of the 0 in the U vector some terms cancel out):

            case 1: Q1 = (x_min, y_min)    U1 = (1, 0)    N1 = (0, -1)

                    t1 = (y_min - py) / vy
                    X1 = (px + vx/vy * (y_min - py), y_min)

            case 2: Q2 = (x_min, y_min)    U2 = (0, 1)    N2 = (-1, 0)

                    t2 = (x_min - px) / vx
                    X2 = (x_min, py + vy/vx * (x_min - px))

            case 3: Q3 = (x_min, y_max)    U3 = (1, 0)    N3 = (0, 1)

                    t3 = (y_max - py) / vy
                    X3 = (px + vx/vy * (y_max - py), y_max)

            case 4: Q4 = (x_max, y_max)    U4 = (0, 1)    N4 = (1, 0)

                    t2 = (x_max - px) / vx
                    X4 = (x_max, py + vy/vx * (x_max - px))

        there are two special cases:

            case 5: V parallel to x axis (vy = 0)

            case 6: V parallel to y axis (vx = 0)


        +------->       \|            |
        |       x        * X2         |
        |                |\           |
        |                | \ (px,py)  |
        V y              |  x         |
                         |   \        |
                         |    \ X1    |
            -------------+-----*------+-------------
            (x_min,y_min)|      \     |
                         |       \    |
                         |        \   |
                         |         \  |
                         |          \ |
                         |           \|
                         |            *  X4
                         |            |\
                         |            | \  X3
            -------------+------------+--*----------
                         |    (x_max, |   \
                         |     y_max) |    \
                         |            |     \ (ex,ey)
                         |            |      x
                         |            |       \

                    the 4 intersection points X1, X2, X3 and X4


            the line always has 4 intersection points, but only two are of interest:


                        (px,py)                                      (ex,ey)
            ------*--------x=======*================*=========*=========x--------------
                  X2               X1               X4        X3



                                    V = (vx,vy) = (ex-px, ey-py)
            ---------------x============================================>--------------
               t < 0       |            t in [0,1]                      |    t > 1



            an entry point is defined by: N dot V < 0
            en leave point is defined by: N dot V > 0

            the points of interest are:

                entry point: E = Xi where Xi = P + te * V where te = max(ti) where Ni dot Vi < 0 for i=1,2,3,4

                leave point: L = Xi where Xi = P + tl * V where tl = min(ti) where Ni dot Vi > 0 for i=1,2,3,4

            (in the example above the entry point is E = X1 and the leaving point is L = X4)


            finally decide if P and P + V are clipped or not:

                check special cases of parallel V first:

                if vy == 0: # V parallel x-axis:
                    if y_min <= py <= y_max:
                        if px > x_max or px + vx < x_min:
                            completely outside
                        if px < x_min:
                            Xe = (x_min, py)
                        elif px <= x_max:
                            Xe = (px, py)
                        if px + vx > x_max:
                            Xl = (x_max, py)
                        elif px + vx > x_min:
                            Xl = (px + vx, py)
                    else:
                        completely outside

                check analog for y-axis

                check entry points
                if te > 1 : segment totally outside
                if 0 <= te <= 1: P outside so use Xe as segment start point
                if te < 0: P might be inside (leaving point will check), use P as segment start point

                check if the intersection point Xe is actually outside


                if tl < 0 : segment totally outside
                if 0 <= tl <= 1: end point outside, use Xl as end point of segment
                if tl > 1 : end point might be inside (entry point will check), use P + V as segment end point

    """

    # can't handle inf values
    assert not math.isinf(px), "can't handle 'inf' for px"
    assert not math.isinf(py), "can't handle 'inf' for py"
    assert not math.isinf(ex), "can't handle 'inf' for ex"
    assert not math.isinf(ey), "can't handle 'inf' for ey"
    assert not math.isinf(x_min), "can't handle 'inf' for x_min"
    assert not math.isinf(y_min), "can't handle 'inf' for y_min"
    assert not math.isinf(x_max), "can't handle 'inf' for x_max"
    assert not math.isinf(y_max), "can't handle 'inf' for y_max"

    assert not math.isnan(px), "can't handle 'nan' for px"
    assert not math.isnan(py), "can't handle 'nan' for py"
    assert not math.isnan(ex), "can't handle 'nan' for ex"
    assert not math.isnan(ey), "can't handle 'nan' for ey"
    assert not math.isnan(x_min), "can't handle 'nan' for x_min"
    assert not math.isnan(y_min), "can't handle 'nan' for y_min"
    assert not math.isnan(x_max), "can't handle 'nan' for x_max"
    assert not math.isnan(y_max), "can't handle 'nan' for y_max"
    
    # this calculation could be saved if passing in the vector instead of the endpoint (??)
    vx = float(ex - px)
    vy = float(ey - py)

    # resulting points
    xx = px
    xy = py
    yx = ex
    yy = ey

    if EPSILON > vy > -EPSILON:
        # case 5, parallel to x axis
        if y_min <= py <= y_max:
            if vx < 0:
                if px < x_min:
                    return False, px, py, ex, ey
                elif px > x_max:
                    xx = x_max
                if ex > x_max:
                    return False, px, py, ex, ey
                elif ex < x_min:
                    yx = x_min
            else:
                if px > x_max:
                    return False, px, py, ex, ey
                elif px < x_min:
                    xx = x_min
                if ex < x_min:
                    return False, px, py, ex, ey
                elif ex > x_max:
                    yx = x_max
            return True, xx, xy, yx, yy
        else:
            # completely outside
            return False, px, py, ex, ey

    # case 6, parallel to y axis
    if EPSILON > vx > -EPSILON:
        if x_min <= px <= x_max:
            if vy < 0:
                if py < y_min:
                    return False, px, py, ex, ey
                elif py > y_max:
                    xy = y_max
                if ey > y_max:
                    return False, px, py, ex, ey
                elif ey < y_min:
                    yy = y_min
            else:
                if py > y_max:
                    return False, px, py, ex, ey
                elif py < y_min:
                    xy = y_min
                if ey < y_min:
                    return False, px, py, ex, ey
                elif ey > y_max:
                    yy = y_max
            return True, xx, xy, yx, yy
        else:
            # completely outside
            return False, px, py, ex, ey

    # N1 dot V -> entry point on U1 and leave point on U3
    if -vy < 0:
        # N2 dot V -> entry point on U2 and leave point on U4
        if -vx < 0:

            # entry points X1 and X2
            t1 = (y_min - py) / vy
            t2 = (x_min - px) / vx
            if t1 > t2:
                # X1 is entry point of interest
                # segment checks
                if t1 > 1:
                # outside, entry point is 'after' the end point
                    return False, px, py, ex, ey
                elif 0 <= t1 <= 1:
                    # start point is outside, but it is intersecting
                    xx = px + t1 * vx
                    if xx < x_min or xx > x_max:
                        return False, px, py, ex, ey
                    xy = y_min
            else:
                # X2 is entry point of interest
                if t2 > 1:
                    # outside, entry point is 'after' the end point
                    return False, px, py, ex, ey
                elif 0 <= t2 <= 1:
                    # start point is outside, but it is intersecting
                    xx = x_min
                    xy = py + t2 * vy
                    if xy < y_min or xy > y_max:
                        return False, px, py, ex, ey

            # leave points X3 and X4
            t3 = (y_max - py) / vy
            t4 = (x_max - px) / vx
            if t3 < t4:
                # X3 is leave point of interest
                if t3 < 0:
                    # outside, leave point is 'before' start point
                    return False, px, py, ex, ey
                elif 0 <= t3 <= 1:
                    # end point is outside, start point intersecting
                    yx = px + t3 * vx
                    # if yx < x_min or yx > x_max:
                    # return (False, px, py, ex, ey)
                    yy = y_max
            else:
            # X4 is leave point of interest
                if t4 < 0:
                    return False, px, py, ex, ey
                elif 0 <= t4 <= 1:
                    yx = x_max
                    yy = py + t4 * vy
                    # if yy < y_min or yy > y_max:
                    # return (False, px, py, ex, ey)

        else:
            # N2 dot V -> entry point on U4 and leave point on U2

            # entry points X1 and X4
            t1 = (y_min - py) / vy
            t4 = (x_max - px) / vx
            if t1 > t4:
                # X1 is entry point of interest
                # segment checks
                if t1 > 1:
                    # outside, entry point is 'after' the end point
                    return False, px, py, ex, ey
                elif 0 <= t1 <= 1:
                    # start point is outside, but it is intersecting
                    xx = px + t1 * vx
                    if xx < x_min or xx > x_max:
                        return False, px, py, ex, ey
                    xy = y_min
            else:
                # X4 is entry point of interest
                if t4 > 1:
                    # outside, entry point is 'after' the end point
                    return False, px, py, ex, ey
                elif 0 <= t4 <= 1:
                    # start point is outside, but it is intersecting
                    xx = x_max
                    xy = py + t4 * vy
                    if xy < y_min or xy > y_max:
                        return False, px, py, ex, ey

            # leave points X3 and X2
            t2 = (x_min - px) / vx
            t3 = (y_max - py) / vy
            if t3 < t2:
                # X3 is leave point of interest
                if t3 < 0:
                    # outside, leave point is 'before' start point
                    return False, px, py, ex, ey
                elif 0 <= t3 <= 1:
                    # end point is outside, start point intersecting
                    yx = px + t3 * vx
                    # if yx < x_min or yx > x_max:
                    # return (False, px, py, ex, ey)
                    yy = y_max
            else:
            # X2 is leave point of interest
                if t2 < 0:
                    return False, px, py, ex, ey
                elif 0 <= t2 <= 1:
                    yx = x_min
                    yy = py + t2 * vy
                    # if yy < y_min or yy > y_max:
                    # return (False, px, py, ex, ey)

    elif -vy > 0:
        # N1 dot V -> entry point on U3 and leave point on U1
        if -vx < 0:
            # N2 dot V -> entry point on U2 and leave point on U4

            # entry points X2 and X3
            t2 = (x_min - px) / vx
            t3 = (y_max - py) / vy
            if t3 > t2:
                # X3 is entry point of interest
                # segment checks
                if t3 > 1:
                    # outside, entry point is 'after' the end point
                    return False, px, py, ex, ey
                elif 0 <= t3 <= 1:
                    # start point is outside, but it is intersecting
                    xx = px + t3 * vx
                    if xx < x_min or xx > x_max:
                        return False, px, py, ex, ey
                    xy = y_max
            else:
                # X2 is entry point of interest
                if t2 > 1:
                    # outside, entry point is 'after' the end point
                    return False, px, py, ex, ey
                elif 0 <= t2 <= 1:
                    # start point is outside, but it is intersecting
                    xx = x_min
                    xy = py + t2 * vy
                    if xy < y_min or xy > y_max:
                        return False, px, py, ex, ey

            # leave points X1 and X4
            t1 = (y_min - py) / vy
            t4 = (x_max - px) / vx
            if t1 < t4:
                # X1 is leave point of interest
                if t1 < 0:
                    # outside, leave point is 'before' start point
                    return False, px, py, ex, ey
                elif 0 <= t1 <= 1:
                    # end point is outside, start point intersecting
                    yx = px + t1 * vx
                    # if yx < x_min or yx > x_max:
                    # return (False, px, py, ex, ey)
                    yy = y_min
            else:
            # X4 is leave point of interest
                if t4 < 0:
                    return False, px, py, ex, ey
                elif 0 <= t4 <= 1:
                    yx = x_max
                    yy = py + t4 * vy
                    # if yy < y_min or yy > y_max:
                    # return (False, px, py, ex, ey)

        else:
            # N2 dot V -> entry point on U4 and leave point on U2

            # entry points X1 and X4
            t3 = (y_max - py) / vy
            t4 = (x_max - px) / vx
            if t3 > t4:
                # X1 is entry point of interest
                # segment checks
                if t3 > 1:
                    # outside, entry point is 'after' the end point
                    return False, px, py, ex, ey
                elif 0 <= t3 <= 1:
                    # start point is outside, but it is intersecting
                    xx = px + t3 * vx
                    if xx < x_min or xx > x_max:
                        return False, px, py, ex, ey
                    xy = y_max
            else:
                # X4 is entry point of interest
                if t4 > 1:
                    # outside, entry point is 'after' the end point
                    return False, px, py, ex, ey
                elif 0 <= t4 <= 1:
                    # start point is outside, but it is intersecting
                    xx = x_max
                    xy = py + t4 * vy
                    if xy < y_min or xy > y_max:
                        return False, px, py, ex, ey

            # leave points X3 and X2
            t1 = (y_min - py) / vy
            t2 = (x_min - px) / vx
            if t1 < t2:
                # X3 is leave point of interest
                if t1 < 0:
                    # outside, leave point is 'before' start point
                    return False, px, py, ex, ey
                elif 0 <= t1 <= 1:
                    # end point is outside, start point intersecting
                    yx = px + t1 * vx
                    # if yx < x_min or yx > x_max:
                    # return (False, px, py, ex, ey)
                    yy = y_min
            else:
            # X2 is leave point of interest
                if t2 < 0:
                    return False, px, py, ex, ey
                elif 0 <= t2 <= 1:
                    yx = x_min
                    yy = py + t2 * vy
                    # if yy < y_min or yy > y_max:
                    # return (False, px, py, ex, ey)

    return True, xx, xy, yx, yy

    # (bool, sx, sy, ex, ey)


def is_segment_intersecting_aabb(px, py, ex, ey, x_min, y_min, x_max, y_max):
    """

    Clips a line segment on an AABB (axis aligned bounding box).

    :param px:
        x-coordinate of the starting point of the segment
    :param py:
        y-coordinate of the starting point of the segment
    :param ex:
        x-coordinate of the end point of the segment
    :param ey:
        y-coordinate of the end point of the segment
    :param x_min:
        the minimal x coordinate of the AABB
    :param y_min:
        the minimal y coordinate of the AABB
    :param x_max:
        the maximal x coordinate of the AABB
    :param y_max:
        the maximal y coordinate of the AABB

    :Returns:
        A tuple like (bool, ux, uy, wx, wy)
        Start point and end point of the clipped line segment if there
        is a line segment within the axis aligned bounding box. Otherwise 
        returns none.
        

    Explanation::
    
    
        This method uses the separating axis theorem (SAT).
    
    
                                                    X
            +---.----.--.---------------.------------>
            |   '    '  '               '
            -. .'. . x  '               '
            |   '   /   '               '
            |   '  /    '               '
            -. .'./. . .+---------------+
            |   '/     ´|              ´|
            -. .x     ´ |             ´ |
            |  ´     ´  |            ´  |
          \ -.´. . .´. .+-----------´---+
           \|´     ´   ´           ´   ´
            ´     ´   ´           ´   ´
            |\   ´   ´           ´   ´
            | \ ´   ´           ´   ´
            |  ´   ´           ´   ´
            |   \ ´           ´   ´
            |    ´           ´   ´
            |     \         ´   ´
         Y  v      \       ´   ´
                    \     ´   ´
                     \   ´   ´
                      \ ´   ´
                       ´   ´
                        \ ´
                         ´
                          \
                  
    SAT works as follows::
        
        The corners are projected onto the axis given by the normals.
        If any axis has no intersection, then they do not intersect.
        
    
    
    """
    # can't handle inf values
    assert not math.isinf(px), "can't handle 'inf' for px"
    assert not math.isinf(py), "can't handle 'inf' for py"
    assert not math.isinf(ex), "can't handle 'inf' for ex"
    assert not math.isinf(ey), "can't handle 'inf' for ey"
    assert not math.isinf(x_min), "can't handle 'inf' for x_min"
    assert not math.isinf(y_min), "can't handle 'inf' for y_min"
    assert not math.isinf(x_max), "can't handle 'inf' for x_max"
    assert not math.isinf(y_max), "can't handle 'inf' for y_max"

    assert not math.isnan(px), "can't handle 'nan' for px"
    assert not math.isnan(py), "can't handle 'nan' for py"
    assert not math.isnan(ex), "can't handle 'nan' for ex"
    assert not math.isnan(ey), "can't handle 'nan' for ey"
    assert not math.isnan(x_min), "can't handle 'nan' for x_min"
    assert not math.isnan(y_min), "can't handle 'nan' for y_min"
    assert not math.isnan(x_max), "can't handle 'nan' for x_max"
    assert not math.isnan(y_max), "can't handle 'nan' for y_max"

    # x axis
    if ex > px:
        if ex < x_min:
            return False
        elif px > x_max:
            return False
    else:
        if px < x_min:
            return False
        elif ex > x_max:
            return False

    # y axis
    if ey > py:
        if ey < y_min:
            return False
        elif py > y_max:
            return False
    else:
        if py < y_min:
            return False
        elif ey > y_max:
            return False

    # normal axis
    nx = float(ey - py)
    ny = float(px - ex)

    l_sq = nx * nx + ny * ny

    if l_sq == 0:
        # point is inside aabb
        return True

    # project aabb onto normal axis
    # return self.dot(other) / other.length_sq * other
    # p1 = (nx * vx + ny + vy) / l_sq
    x_min_px_nx = (x_min - px) * nx
    x_max_px_nx = (x_max - px) * nx
    y_min_py_ny = (y_min - py) * ny
    y_max_py_ny = (y_max - py) * ny
    # actually the projections would need a division by l_sq
    # but this is only a scaling and does not change the 
    # sign nor the order so we can save these cpu cycles
    # also determine the max and min points
    p1 = x_min_px_nx + y_min_py_ny
    p4 = x_max_px_nx + y_max_py_ny
    if p1 > p4:
        p1, p4 = p4, p1

    p = x_min_px_nx + y_max_py_ny
    if p > p4:
        p4 = p
    elif p < p1:
        p1 = p

    p = x_max_px_nx + y_min_py_ny
    if p > p4:
        p4 = p
    elif p < p1:
        p1 = p

    if p1 > 0:
        return False
    if p4 < 0:
        return False

    # here we have intersection!!
    # either it is within the aabb 
    # or it intersects one side of the aabb
    # or it intersects both sides
    return True


def clip_segment_against_aabb_sat(px, py, ex, ey, x_min, y_min, x_max, y_max):
    """

    SAT
                                                    X
            +---'----'--'---------------'------------>
            |   '    '  '               '
            -. .'. . x  '               '
            |   '   /   '               '
            |   '  /    '               '
            -. .'./. . .+---------------+
            |   '/     ´|              ´|
            -. .x     ´ |             ´ |
            |  ´     ´  |            ´  |
          \ -.´. . .´. .+-----------´---+
           \|´     ´   ´           ´   ´
            ´     ´   ´           ´   ´
            |\   ´   ´           ´   ´
            | \ ´   ´           ´   ´
            |  ´   ´           ´   ´
            |   \ ´           ´   ´
            |    ´           ´   ´
            |     \         ´   ´
         Y  v      \       ´   ´
                    \     ´   ´
                     \   ´   ´
                      \ ´   ´
                       ´   ´
                        \ ´
                         ´
                          \
                           \ normal axis to the segment
    :param px:
    :param py:
    :param ex:
    :param ey:
    :param x_min:
    :param y_min:
    :param x_max:
    :param y_max:
    """
    # can't handle inf values
    assert not math.isinf(px), "can't handle 'inf' for px"
    assert not math.isinf(py), "can't handle 'inf' for py"
    assert not math.isinf(ex), "can't handle 'inf' for ex"
    assert not math.isinf(ey), "can't handle 'inf' for ey"
    assert not math.isinf(x_min), "can't handle 'inf' for x_min"
    assert not math.isinf(y_min), "can't handle 'inf' for y_min"
    assert not math.isinf(x_max), "can't handle 'inf' for x_max"
    assert not math.isinf(y_max), "can't handle 'inf' for y_max"

    assert not math.isnan(px), "can't handle 'nan' for px"
    assert not math.isnan(py), "can't handle 'nan' for py"
    assert not math.isnan(ex), "can't handle 'nan' for ex"
    assert not math.isnan(ey), "can't handle 'nan' for ey"
    assert not math.isnan(x_min), "can't handle 'nan' for x_min"
    assert not math.isnan(y_min), "can't handle 'nan' for y_min"
    assert not math.isnan(x_max), "can't handle 'nan' for x_max"
    assert not math.isnan(y_max), "can't handle 'nan' for y_max"

    # x axis
    if ex > px:
        if ex < x_min:
            return False, px, py, ex, ey
        elif px > x_max:
            return False, px, py, ex, ey
    else:
        if px < x_min:
            return False, px, py, ex, ey
        elif ex > x_max:
            return False, px, py, ex, ey

    # y axis
    if ey > py:
        if ey < y_min:
            return False, px, py, ex, ey
        elif py > y_max:
            return False, px, py, ex, ey
    else:
        if py < y_min:
            return False, px, py, ex, ey
        elif ey > y_max:
            return False, px, py, ex, ey

    # normal axis
    nx = float(ey - py)
    ny = float(px - ex)

    l_sq = nx * nx + ny * ny

    # if l_sq == 0:
    if l_sq < EPSILON:
        # point is inside aabb
        return True, px, py, ex, ey

    # project aabb onto normal axis
    # return self.dot(other) / other.length_sq * other
    # p1 = (nx * vx + ny + vy) / l_sq

    # terms for the projection to project all 4 corners of the aabb
    x_min_px = x_min - px
    x_max_px = x_max - px
    y_min_py = y_min - py
    y_max_py = y_max - py

    x_min_px_nx = x_min_px * nx
    x_max_px_nx = x_max_px * nx
    y_min_py_ny = y_min_py * ny
    y_max_py_ny = y_max_py * ny

    # actually the projections would need a division by l_sq
    # but this is only a scaling and does not change the 
    # sign nor the order so we can save these cpu cycles
    # do also determine the max and min points
    p1 = x_min_px_nx + y_min_py_ny
    p4 = x_max_px_nx + y_max_py_ny
    if p1 > p4:
        p1, p4 = p4, p1

    p = x_min_px_nx + y_max_py_ny
    if p > p4:
        p4 = p
    elif p < p1:
        p1 = p

    p = x_max_px_nx + y_min_py_ny
    if p > p4:
        p4 = p
    elif p < p1:
        p1 = p

    if p1 > 0:
        return False, px, py, ex, ey
    if p4 < 0:
        return False, px, py, ex, ey

    #print('!!', p1, p4)

    # here we have intersection!!
    # either it is within the aabb 
    # or it intersects one side of the aabb
    # or it intersects both sides
    #
    #    using a line in parametric form:
    #
    #        P + t * V      where    P start point (px, py)
    #                                t parameter in range [0, 1]
    #                                V as direction vector (vx, vy) = (ex-px,ey-py)
    #
    #
    #    intersection of two parametric lines:
    #
    #        P + t * V = Q + s * U
    #
    #            =>   s = (V cross (P - Q)) / (V cross U) = (vx * (py - qy) - vy * (px - qx)) / (vx * uy - vy * ux)
    #
    #            =>   t = (qy - py + s * uy) / vy
    #
    #
    # Q = px, py
    # U = ex-px, ey-py  = -ny, nx # reusing the variables and their value
    # P = ox, oy
    # V = vx, vy

    # initial values
    s2 = 0.0
    s3 = 1.0

    # ox = x_min, oy = y_min, vx = x_max - x_min
    # x_max_x_min = x_max - x_min
    # denominator = x_max_x_min * nx
    if nx != 0.0:
        s2 = y_min_py / nx
        # ox = x_max, oy = y_max, vx = x_min - x_max, vy = 0.0
        # denominator = (x_min - x_max) * nx
        # if denominator != 0.0:
        s3 = y_max_py / nx
    if s2 > s3:
        s2, s3 = s3, s2
        # ox = x_min, oy = y_min, vx = 0.0, vy = y_max - y_min
    # y_max_y_min = y_max - y_min
    # denominator = y_max_y_min * ny
    if ny != 0.0:
        s1 = -x_min_px / ny
        if s1 > s3:
            s1, s2, s3 = s2, s3, s1
        elif s1 > s2:
            s1, s2 = s2, s1

            # ox = x_max, oy = y_max, vx = 0.0, vy = y_min - y_max
            # denominator = (y_min - y_max) * ny
            # if denominator != 0.0:
        s4 = -x_max_px / ny
        if s4 < s1:
            s3 = s2
            s2 = s1
        elif s4 < s2:
            s3 = s2
            s2 = s4
        elif s4 < s3:
            s3 = s4
            # print('!', s2, s3)

    # clamp to range [0, 1], outside of range is not in the segment
    if s2 < 0.0:
        s2 = 0.0

    if s3 > 1.0:
        s3 = 1.0

    # print('!', s2, s3, px, ny)

    # calculate the points to return
    return True, px - s2 * ny, py + s2 * nx, px - s3 * ny, py + s3 * nx

    
# from: https://bitbucket.org/marcusva/py-sdl2/src/fce4c7cd5bf8349b929ddc01e9c2f4e744dacbd4/sdl2/ext/algorithms.py?at=default

def cohensutherland(left, top, right, bottom, x1, y1, x2, y2):
    """Clips a line to a rectangular area.

    This implements the Cohen-Sutherland line clipping algorithm.  left,
    top, right and bottom denote the clipping area, into which the line
    defined by x1, y1 (start point) and x2, y2 (end point) will be
    clipped.

    If the line does not intersect with the rectangular clipping area,
    four None values will be returned as tuple. Otherwise a tuple of the
    clipped line points will be returned in the form (cx1, cy1, cx2, cy2).
    """
    LEFT, RIGHT, LOWER, UPPER = 1, 2, 4, 8

    def _getclip(xa, ya):
        p = 0
        if xa < left:
            p = LEFT
        elif xa > right:
            p = RIGHT
        if ya < top:
            p |= LOWER
        elif ya > bottom:
            p |= UPPER
        return p

    k1 = _getclip(x1, y1)
    k2 = _getclip(x2, y2)
    while (k1 | k2) != 0:
        if (k1 & k2) != 0:
            return None, None, None, None

        opt = k1 or k2
        if opt & UPPER:
            x = x1 + (x2 - x1) * (1.0 * (bottom - y1)) / (y2 - y1)
            y = bottom
        elif opt & LOWER:
            x = x1 + (x2 - x1) * (1.0 * (top - y1)) / (y2 - y1)
            y = top
        elif opt & RIGHT:
            y = y1 + (y2 - y1) * (1.0 * (right - x1)) / (x2 - x1)
            x = right
        elif opt & LEFT:
            y = y1 + (y2 - y1) * (1.0 * (left - x1)) / (x2 - x1)
            x = left
        else:
            raise Exception()

        if opt == k1:
            # x1, y1 = int(x), int(y)
            x1, y1 = x, y
            k1 = _getclip(x1, y1)
        else:
            # x2, y2 = int(x), int(y)
            x2, y2 = x, y
            k2 = _getclip(x2, y2)

    return x1, y1, x2, y2

# from: https://bitbucket.org/marcusva/py-sdl2/src/fce4c7cd5bf8349b929ddc01e9c2f4e744dacbd4/sdl2/ext/algorithms.py?at=default
def liangbarsky(left, top, right, bottom, x1, y1, x2, y2):
    """Clips a line to a rectangular area.

    This implements the Liang-Barsky line clipping algorithm.  left,
    top, right and bottom denote the clipping area, into which the line
    defined by x1, y1 (start point) and x2, y2 (end point) will be
    clipped.

    If the line does not intersect with the rectangular clipping area,
    four None values will be returned as tuple. Otherwise a tuple of the
    clipped line points will be returned in the form (cx1, cy1, cx2, cy2).
    """
    dx = x2 - x1 * 1.0
    dy = y2 - y1 * 1.0
    dt0, dt1 = 0.0, 1.0
    xx1 = x1
    yy1 = y1
    

    checks = ((-dx, x1 - left),
              (dx, right - x1),
              (-dy, y1 - top),
              (dy, bottom - y1))

    for p, q in checks:
        if p == 0 and q < 0:
            return None, None, None, None
        if p != 0:
            dt = q / (p * 1.0)
            if p < 0:
                if dt > dt1:
                    return None, None, None, None
                dt0 = max(dt0, dt)
            else:
                if dt < dt0:
                    return None, None, None, None
                dt1 = min(dt1, dt)
    if dt0 > 0:
        x1 += dt0 * dx
        y1 += dt0 * dy
    if dt1 < 1:
        x2 = xx1 + dt1 * dx
        y2 = yy1 + dt1 * dy
    return x1, y1, x2, y2
    
    

def test_clipping():
    """


    :return: :raise Exception:
    """
    x_min = 10
    y_min = 10
    x_max = 20
    y_max = 20

    inf = float("inf")

    # values = [(args, expected),...]
    values = [
        # parallel to x axis
        (( 5, 5, 15, 5, x_min, y_min, x_max, y_max), (False, 5, 5, 15, 5)),  # s1
        (( 5, 15, 15, 15, x_min, y_min, x_max, y_max), (True, 10, 15, 15, 15)),  # s2
        ((15, 15, 25, 15, x_min, y_min, x_max, y_max), (True, 15, 15, 20, 15)),  # s3
        (( 5, 15, 25, 15, x_min, y_min, x_max, y_max), (True, 10, 15, 20, 15)),  # s4
        (( 5, 15, 9, 15, x_min, y_min, x_max, y_max), (False, 5, 15, 9, 15)),  # s5
        ((21, 15, 29, 15, x_min, y_min, x_max, y_max), (False, 21, 15, 29, 15)),  # s6
        ((11, 15, 19, 15, x_min, y_min, x_max, y_max), (True, 11, 15, 19, 15)),  # s7
        ((11, 25, 19, 25, x_min, y_min, x_max, y_max), (False, 11, 25, 19, 25)),  # s8

        # parallel to y axis
        (( 5, 5, 5, 15, x_min, y_min, x_max, y_max), (False, 5, 5, 5, 15)),  # s11
        ((15, 5, 15, 15, x_min, y_min, x_max, y_max), (True, 15, 10, 15, 15)),  # s12
        ((15, 15, 15, 25, x_min, y_min, x_max, y_max), (True, 15, 15, 15, 20)),  # s13
        ((15, 5, 15, 25, x_min, y_min, x_max, y_max), (True, 15, 10, 15, 20)),  # s14
        ((15, 5, 15, 9, x_min, y_min, x_max, y_max), (False, 15, 5, 15, 9)),  # s15
        ((15, 21, 15, 29, x_min, y_min, x_max, y_max), (False, 15, 21, 15, 29)),  # s16
        ((15, 11, 15, 19, x_min, y_min, x_max, y_max), (True, 15, 11, 15, 19)),  # s17
        ((25, 5, 25, 25, x_min, y_min, x_max, y_max), (False, 25, 5, 25, 25)),  # s18

        # other line segments
        # crossing aabb line segments in 45 degrees angle
        (( 5, 20, 20, 5, x_min, y_min, x_max, y_max), (True, 10, 15, 15, 10)),
        ((20, 5, 5, 20, x_min, y_min, x_max, y_max), (True, 15, 10, 10, 15)),
        ((10, 5, 25, 20, x_min, y_min, x_max, y_max), (True, 15, 10, 20, 15)),
        ((25, 20, 10, 5, x_min, y_min, x_max, y_max), (True, 20, 15, 15, 10)),
        (( 5, 10, 20, 25, x_min, y_min, x_max, y_max), (True, 10, 15, 15, 20)),
        ((20, 25, 5, 10, x_min, y_min, x_max, y_max), (True, 15, 20, 10, 15)),
        ((10, 25, 25, 10, x_min, y_min, x_max, y_max), (True, 15, 20, 20, 15)),
        ((25, 10, 10, 25, x_min, y_min, x_max, y_max), (True, 20, 15, 15, 20)),

        (( 5, 20, 25, 10, x_min, y_min, x_max, y_max), (True, 10.0, 17.5, 20.0, 12.5)),
        (( 5, 10, 25, 20, x_min, y_min, x_max, y_max), (True, 10.0, 12.5, 20.0, 17.5)),
        (( 10, 5, 20, 25, x_min, y_min, x_max, y_max), (True, 12.5, 10.0, 17.5, 20.0)),
        ((20, 5, 10, 25, x_min, y_min, x_max, y_max), (True, 17.5, 10.0, 12.5, 20.0)),

        ((25, 10, 5, 20, x_min, y_min, x_max, y_max), (True, 20.0, 12.5, 10.0, 17.5)),
        ((25, 20, 5, 10, x_min, y_min, x_max, y_max), (True, 20.0, 17.5, 10.0, 12.5)),
        ((20, 25, 10, 5, x_min, y_min, x_max, y_max), (True, 17.5, 20.0, 12.5, 10.0)),
        ((10, 25, 20, 5, x_min, y_min, x_max, y_max), (True, 12.5, 20.0, 17.5, 10.0)),

        # line segments starting within and ending outside aabb
        ((15, 15, 20, 5, x_min, y_min, x_max, y_max), (True, 15, 15, 17.5, 10)),
        ((15, 15, 10, 5, x_min, y_min, x_max, y_max), (True, 15, 15, 12.5, 10)),
        ((15, 15, 25, 10, x_min, y_min, x_max, y_max), (True, 15, 15, 20, 12.5)),
        ((15, 15, 25, 20, x_min, y_min, x_max, y_max), (True, 15, 15, 20, 17.5)),
        ((15, 15, 20, 25, x_min, y_min, x_max, y_max), (True, 15, 15, 17.5, 20)),
        ((15, 15, 10, 25, x_min, y_min, x_max, y_max), (True, 15, 15, 12.5, 20)),
        ((15, 15, 5, 20, x_min, y_min, x_max, y_max), (True, 15, 15, 10, 17.5)),
        ((15, 15, 5, 10, x_min, y_min, x_max, y_max), (True, 15, 15, 10, 12.5)),

        # line segments starting outside and ending within aabb
        ((20, 5, 15, 15, x_min, y_min, x_max, y_max), (True, 17.5, 10, 15, 15)),
        ((10, 5, 15, 15, x_min, y_min, x_max, y_max), (True, 12.5, 10, 15, 15)),
        ((25, 10, 15, 15, x_min, y_min, x_max, y_max), (True, 20, 12.5, 15, 15)),
        ((25, 20, 15, 15, x_min, y_min, x_max, y_max), (True, 20, 17.5, 15, 15)),
        ((20, 25, 15, 15, x_min, y_min, x_max, y_max), (True, 17.5, 20, 15, 15)),
        ((10, 25, 15, 15, x_min, y_min, x_max, y_max), (True, 12.5, 20, 15, 15)),
        (( 5, 20, 15, 15, x_min, y_min, x_max, y_max), (True, 10, 17.5, 15, 15)),
        (( 5, 10, 15, 15, x_min, y_min, x_max, y_max), (True, 10, 12.5, 15, 15)),

        # only corner is crossed
        (( 5, 15, 15, 5, x_min, y_min, x_max, y_max), (True, 10, 10, 10, 10)),
        ((15, 5, 25, 15, x_min, y_min, x_max, y_max), (True, 20, 10, 20, 10)),
        ((25, 15, 15, 25, x_min, y_min, x_max, y_max), (True, 20, 20, 20, 20)),
        ((15, 25, 5, 15, x_min, y_min, x_max, y_max), (True, 10, 20, 10, 20)),

        ((15, 5, 5, 15, x_min, y_min, x_max, y_max), (True, 10, 10, 10, 10)),
        ((25, 15, 15, 5, x_min, y_min, x_max, y_max), (True, 20, 10, 20, 10)),
        ((15, 25, 25, 15, x_min, y_min, x_max, y_max), (True, 20, 20, 20, 20)),
        (( 5, 15, 15, 25, x_min, y_min, x_max, y_max), (True, 10, 20, 10, 20)),

        # line segments completely within aabb
        ((12.5, 12.5, 17.5, 17.5, x_min, y_min, x_max, y_max), (True, 12.5, 12.5, 17.5, 17.5)),
        ((12.5, 15.0, 17.5, 17.5, x_min, y_min, x_max, y_max), (True, 12.5, 15.0, 17.5, 17.5)),
        ((15.0, 17.5, 17.5, 15.0, x_min, y_min, x_max, y_max), (True, 15.0, 17.5, 17.5, 15.0)),

        ((17.5, 17.5, 12.5, 12.5, x_min, y_min, x_max, y_max), (True, 17.5, 17.5, 12.5, 12.5)),
        ((17.5, 17.5, 12.5, 15.0, x_min, y_min, x_max, y_max), (True, 17.5, 17.5, 12.5, 15.0)),
        ((17.5, 15.0, 15.0, 17.5, x_min, y_min, x_max, y_max), (True, 17.5, 15.0, 15.0, 17.5)),

        # line segments completely outside aabb
        (( 5, 5, 10, 5, x_min, y_min, x_max, y_max), (False, 5, 5, 10, 5)),
        (( 5, 5, 5, 20, x_min, y_min, x_max, y_max), (False, 5, 5, 5, 20)),
        ((15, 22, 15, 25, x_min, y_min, x_max, y_max), (False, 15, 22, 15, 25)),


        (( 5, 10, 10, 5, x_min, y_min, x_max, y_max), (False, 5, 10, 10, 5)),
        ((20, 5, 25, 10, x_min, y_min, x_max, y_max), (False, 20, 5, 25, 10)),
        ((25, 20, 20, 25, x_min, y_min, x_max, y_max), (False, 25, 20, 20, 25)),
        ((10, 25, 5, 20, x_min, y_min, x_max, y_max), (False, 10, 25, 5, 20)),

        ((10, 5, 5, 10, x_min, y_min, x_max, y_max), (False, 10, 5, 5, 10)),
        ((25, 10, 20, 5, x_min, y_min, x_max, y_max), (False, 25, 10, 20, 5)),
        ((20, 25, 25, 20, x_min, y_min, x_max, y_max), (False, 20, 25, 25, 20)),
        (( 5, 20, 10, 25, x_min, y_min, x_max, y_max), (False, 5, 20, 10, 25)),

        # points, inside
        ((15, 15, 15, 15, x_min, y_min, x_max, y_max), (True, 15, 15, 15, 15)),

        # points, outside
        (( 5, 5, 5, 5, x_min, y_min, x_max, y_max), (False, 5, 5, 5, 5)),

    ]

    values_other_args_order = [ ((val[0][4], val[0][5], val[0][6], val[0][7], val[0][0], val[0][1], val[0][2], val[0][3] ), \
                                 (val[1][1], val[1][2], val[1][3], val[1][4], ) if val[1][0] else (None, None, None, None)) for val in values]
                                 
    # print(values_other_args_order)
    
    # # cant satisfy both methods because of rounding issues, still the values are the same until
    # # the 5th digit after the dot, probably precise enough
    #((13.353897290260468, 11.812311612950818, 13.353894247932462, 0.04846565462539365, 10, 10, 20, 20),
    #        (True, 13.353899999999999, 11.81231, 13.35389, 10)),

    #(AssertionError('using function: clip_segment_against_aabb_sat(...) \n '
    #    'segment ((13.353897290260468, 11.812311612950818, 13.353894247932462, 0.04846565462539365)) clipped at aabb (10, 10, 20, 20)? '
    #    '\n\t expected: (True, 13.353897290260468, 11.812311612950818, 13.353894247932462, 10) '
    #    '\n\t      got: (True, 13.353897290260468, 11.812311612950818, 13.353896821566268, 10.0)',), '\n')

    #(AssertionError('using function: clip_segment_against_aabb_sat(...) '
    #                '\n segment ((13.353897290260468, 11.812311612950818, 13.353894247932462, 0.04846565462539365)) clipped at aabb (10, 10, 20, 20)? '
    #                '\n\t expected: (True, 13.35389, 11.81231, 13.35389, 10) '
    #                '\n\t      got: (True, 13.353899999999999, 11.81231, 13.353899999999999, 10.0)'

    #(AssertionError('using function: clip_segment_against_aabb(...) '
    #                '\n segment ((13.353897290260468, 11.812311612950818, 13.353894247932462, 0.04846565462539365)) clipped at aabb (10, 10, 20, 20)? '
    #                '\n\t expected: (True, 13.353899999999999, 11.81231, 13.353899999999999, 10) '
    #                '\n\t      got: (True, 13.353899999999999, 11.81231, 13.35389, 10.0)'

    inf_values = [
        # inf
        # one point inside, diagonal
        ((15, 15, inf, inf, x_min, y_min, x_max, y_max), AssertionError), # (True,   15,  15,  20,  20)),
        ((15, 15, -inf, inf, x_min, y_min, x_max, y_max), AssertionError), # (True,   15,  15,  10,  20)),
        ((15, 15, inf, -inf, x_min, y_min, x_max, y_max), AssertionError), # (True,   15,  15,  20,  10)),
        ((15, 15, -inf, -inf, x_min, y_min, x_max, y_max), AssertionError), # (True,   15,  15,  10,  10)),

        (( inf, inf, 15, 15, x_min, y_min, x_max, y_max), AssertionError), # (True, 20, 20, 15, 15)),
        ((-inf, inf, 15, 15, x_min, y_min, x_max, y_max), AssertionError), # (True, 10, 20, 15, 15)),
        (( inf, -inf, 15, 15, x_min, y_min, x_max, y_max), AssertionError), # (True, 20, 10, 15, 15)),
        ((-inf, -inf, 15, 15, x_min, y_min, x_max, y_max), AssertionError), # (True, 10, 10, 15, 15)),

        # one point inside, parallel to axis
        ((15, 15, 15, inf, x_min, y_min, x_max, y_max), AssertionError), # (True,   15,  15,  15,  20)),
        ((15, 15, 15, -inf, x_min, y_min, x_max, y_max), AssertionError), # (True,   15,  15,  15,  10)),
        ((15, 15, inf, 15, x_min, y_min, x_max, y_max), AssertionError), # (True,   15,  15,  20,  15)),
        ((15, 15, -inf, 15, x_min, y_min, x_max, y_max), AssertionError), # (True,   15,  15,  10,  15)),

        ((  15, inf, 15, 15, x_min, y_min, x_max, y_max), AssertionError), # (True,   15,  20,  15,  15)),
        ((  15, -inf, 15, 15, x_min, y_min, x_max, y_max), AssertionError), # (True,   15,  10,  15,  15)),
        (( inf, 15, 15, 15, x_min, y_min, x_max, y_max), AssertionError), # (True,   20,  15,  15,  15)),
        ((-inf, 15, 15, 15, x_min, y_min, x_max, y_max), AssertionError), # (True,   10,  15,  15,  15)),


        # intersecting, points outside, parallel to axis
        (( inf, 15, -inf, 15, x_min, y_min, x_max, y_max), AssertionError), # (True,   20,  15,  10,  15)),
        ((-inf, 15, inf, 15, x_min, y_min, x_max, y_max), AssertionError), # (True,   10,  15,  20,  15)),
        ((  15, -inf, 15, inf, x_min, y_min, x_max, y_max), AssertionError), # (True,   15,  10,  15,  20)),
        ((  15, inf, 15, -inf, x_min, y_min, x_max, y_max), AssertionError), # (True,   15,  20,  15,  10)),

        # intersecting, points outside, diagonal
        (( inf,  inf,    1,    1, x_min, y_min, x_max, y_max), AssertionError), # (True,   20,  20,  10,  10)),
        ((-inf, -inf,   21,   21, x_min, y_min, x_max, y_max), AssertionError), # (True,   10,  10,  20,  20)),
        ((   1,    1,  inf,  inf, x_min, y_min, x_max, y_max), AssertionError), # (True,   10,  10,  20,  20)),
        ((   1,    1, -inf, -inf, x_min, y_min, x_max, y_max), AssertionError), # (True,   20,  20,  10,  10)),


        # not intersecting, points outside, parallel to axis
        (( inf,   25, -inf,   25, x_min, y_min, x_max, y_max), AssertionError), # (False,  inf,   25, -inf, 25)),
        ((-inf,    5,  inf,    5, x_min, y_min, x_max, y_max), AssertionError), # (False, -inf,    5,  inf,  5)),
        ((  25, -inf,   25,  inf, x_min, y_min, x_max, y_max), AssertionError), # (False,   25, -inf,   25,  inf)),
        ((   5,  inf,    5, -inf, x_min, y_min, x_max, y_max), AssertionError), # (False,    5,  inf,    5, -inf)),

        # not intersecting, points outside, diagonal
        ((   inf,  inf,   21,   21, x_min, y_min, x_max, y_max), AssertionError), # (False,  inf,  inf,   21,   21)),
        ((  -inf, -inf,    1,    1, x_min, y_min, x_max, y_max), AssertionError), # (False, -inf, -inf,    1,    1)),
        ((    21,   21,  inf,  inf, x_min, y_min, x_max, y_max), AssertionError), # (False,   21,   21,  inf,  inf)),
        ((     1,    1, -inf, -inf, x_min, y_min, x_max, y_max), AssertionError), # (False,    1,    1, -inf, -inf)),

        # (( , , , , x_min, y_min, x_max, y_max), (True,  , , , )),
    ]

    def _test_func(the_values, func):
        err_cnt = 0
        count = 0
        digits = 5
        for args, exp in the_values:
            try:
                count += 1
                res = func(*args)
                # print('?  ', res)
                if len(res) == 5:
                    res = (res[0], round(res[1], digits), round(res[2], digits), round(res[3], digits), round(res[4], digits))
                    assert res[0] is exp[0] and res[1] == exp[1] and res[2] == exp[2] and res[3] == exp[3] and res[4] == \
                           exp[4], \
                        "using function: {4}(...) \n segment ({0}) clipped at aabb {1}? \n\t expected: {2} \n\t      got: {3}".format(
                            args[:4], args[4:], exp, res, func.__name__)
                    assert is_segment_intersecting_aabb(*args) is exp[0], "is_intersecting failed"
                else:
                    if res[0] is not None:
                        res = (round(res[0], digits), round(res[1], digits), round(res[2], digits), round(res[3], digits))
                    # print(res, exp)
                    assert res[0] == exp[0] and res[1] == exp[1] and res[2] == exp[2] and res[3] == exp[3] , \
                        "using function: {4}(...) \n segment ({1}) clipped at aabb {0}? \n\t expected: {2} \n\t      got: {3}".format(
                            args[:4], args[4:], exp, res, func.__name__)

            except Exception as ex:
                print(ex, "\n")
                err_cnt += 1
        print("\nerrors {0}/{1}    {2}\n\n".format(err_cnt, count, func.__name__))


    def _test_func_throws_error(the_values, func):
        err_cnt = 0
        count = 0
        last = None
        for args, exp in the_values:
            try:
                count += 1
                func(*args)
                raise Exception("should have raised '{0}'".format(exp))
            except Exception as ex:
                try:
                    assert isinstance(ex, exp), "function '{3}' threw exception '{0}' but expected exception '{1}' for args '{2}'".format(ex.__class__, str(exp), args, func.__name__)
                except Exception as ex2:
                    print(ex2, "\n")
                    err_cnt += 1
        print("\nerrors {0}/{1}    {2}\n\n".format(err_cnt, count, func.__name__))


    def _test_random_values(iterations):
        import random

        def _r(r_min, r_max):
            return (random.random() * (r_max - r_min)) + r_min

        digits = 5
        for i in range(iterations):

            aabb_x_min = _r(-10000, 10000)
            aabb_y_min = _r(-10000, 10000)
            aabb_x_max = _r(-10000, 10000)
            aabb_y_max = _r(-10000, 10000)

            # normalize
            if aabb_x_min > aabb_x_max:
                aabb_x_min, aabb_x_max = aabb_x_max, aabb_x_min
            
            if aabb_y_min > aabb_y_max:
                aabb_y_min, aabb_y_max = aabb_y_max, aabb_y_min
            
            interval = (_r(min(aabb_x_min, aabb_y_min) - 10000, max(aabb_x_max, aabb_y_max) + 10000), \
                        _r(min(aabb_x_min, aabb_y_min) - 10000, max(aabb_x_max, aabb_y_max) + 10000))
            args = (_r(*interval), _r(*interval), _r(*interval), _r(*interval), aabb_x_min, aabb_y_min, aabb_x_max, aabb_y_max)
            # print(args)
            try:
                r0, r1, r2, r3, r4 = clip_segment_against_aabb(*args)
                s0, s1, s2, s3, s4 = clip_segment_against_aabb_sat(*args)
                r1 = round(r1, digits)
                r2 = round(r2, digits)
                r3 = round(r3, digits)
                r4 = round(r4, digits)
                s1 = round(s1, digits)
                s2 = round(s2, digits)
                s3 = round(s3, digits)
                s4 = round(s4, digits)
                assert r0 == s0 and r1 == s1 and r2 == s2 and r3 == s3 and r4 == s4, "error for args: {0}\n clip: {1}\n  SAT: {2}".format(
                    args, clip_segment_against_aabb(*args),
                    clip_segment_against_aabb_sat(
                        *args))
                assert is_segment_intersecting_aabb(*args) is r0, "is_intersecting failed"
            except Exception as ex:
                print(str(ex))
        print("random test values executed successfully")


    def _test_performance(iterations, func, *args):
        time_time = time.time
        i = 0
        start = time_time()
        while i < iterations:
            i += 1
            func(*args)
        end = time_time()
        return end - start

    def _test_performance_special(func, the_values):
        """
        Calls a function with the arguments fro  the values list.
        :param func:
            the function to test
        :param the_values:
            list of tuple, each tuple containing the function args and the expected result as tuple, e.g.:
                [((arg1, arg2,...), (result1, result2,...)), (...), ...]
        """
        for argus, exp in the_values:
            func(*argus)

    # test that correct values are calculated
    _test_func(values, clip_segment_against_aabb)
    _test_func(values, clip_segment_against_aabb_sat)
    
    _test_func(values_other_args_order, cohensutherland)
    _test_func(values_other_args_order, liangbarsky)

    # test handling of inf and NaN
    _test_func_throws_error(inf_values, clip_segment_against_aabb)
    _test_func_throws_error(inf_values, clip_segment_against_aabb_sat)
    
    # test random values
    num_iterations = 10000
    _test_random_values(num_iterations)

    # test performance
    import time

    print("\ntesting {0} times:".format(num_iterations))
    print("performance clip_segment_against_aabb_SAT() :",
          _test_performance(num_iterations, _test_performance_special, clip_segment_against_aabb_sat, values))
    print("performance clip_segment_against_aabb() :    ",
          _test_performance(num_iterations, _test_performance_special, clip_segment_against_aabb, values))
          
    print("performance cohensutherland() :    ",
          _test_performance(num_iterations, _test_performance_special, cohensutherland, values_other_args_order))
    print("performance liangbarsky() :    ",
          _test_performance(num_iterations, _test_performance_special, liangbarsky, values_other_args_order))
          
    print("performance is_segment_intersecting_aabb() :",
          _test_performance(num_iterations, _test_performance_special, is_segment_intersecting_aabb, values))

    # print(_test_performance(n, _test_performance_clip_segment_against_aabb_SAT, values))
    # print(_test_performance(n, _test_performance_clip_segment_against_aabb, values))
    # print(_test_performance(values, n, _test_performance_clip_segment_against_aabb))

    
def run_unittests():
    pass

if __name__ == '__main__':
    test_clipping()
    
    run_unittests()
