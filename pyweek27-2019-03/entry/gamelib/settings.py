# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'settings.py' is part of codogupywk27
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Settings and constants.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

import pygame

from gamelib import pygametext
from pyknic.generators import FlagGenerator, GlobalIdGenerator
from pyknic.pyknic_pygame.eventmapper import EventMapper, ANY_MOD

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")

#
#
# # ------------------------------------------------------------------------------
# # Tunables
# # ------------------------------------------------------------------------------
#
# print_fps = False
max_fps = 60
master_volume = 0.5  # min: 0.0; max: 1.0
music_volume = 1.0   # resulting level = master_volume * music_volume
sfx_volume = 1.0     # resulting level = master_volume * sfx_volume
log_level = [logging.WARNING, logging.DEBUG, logging.INFO][1]

#
#
# # ------------------------------------------------------------------------------
# # Display
# # ------------------------------------------------------------------------------
#
# os.environ['SDL_VIDEO_CENTERED'] = '1'
#
screen_width = 1024
screen_height = 768
screen_size = screen_width, screen_height
screen_flags_presets = (
    0,
    pygame.FULLSCREEN | pygame.DOUBLEBUF,
)
screen_flags = screen_flags_presets[0]
default_fill_color = 100, 100, 100

title = "cANT3 x2 Onslaught: The Invasion from Six Chips Deep"
path_to_icon = "./data/icon.png"

# ------------------------------------------------------------------------------
# Sound
# ------------------------------------------------------------------------------

music_player = None

# Mixer
MIXER_FREQUENCY = 0  # default:22050
MIXER_SIZE = -16  # default: -16
MIXER_CHANNELS = 2  # default: 2 stereo or 1 mono
MIXER_BUFFER_SIZE = 16  # default: 4096

MIXER_NUM_CHANNELS = 24
MIXER_RESERVED_CHANNELS = 6  # default:

# Player
sfx_path = 'data/sfx'
music_path = 'data/music'
(
    channel_ants_speaking,
    channel_building,
    channel_bullet,
    channel_power,
    channel_chips,
    channel_6,
) = range(MIXER_RESERVED_CHANNELS)


# ------------------------------------------------------------------------------
# Events
# ------------------------------------------------------------------------------

# actions
_global_id_generator = GlobalIdGenerator(1000)  # avoid collision with event ids

ACTION_TOGGLE_DEBUG_RENDER = _global_id_generator.next()
ACTION_QUIT = _global_id_generator.next()
ACTION_MOUSE_MOTION = _global_id_generator.next()
ACTION_MBUTTON1_DOWN = _global_id_generator.next()
ACTION_OTHER_MBUTTON_DOWN = _global_id_generator.next()
ACTION_UPGRADE_BUILDING = _global_id_generator.next()

gameplay_event_map = EventMapper({
    pygame.MOUSEBUTTONDOWN: {
        1: ACTION_MBUTTON1_DOWN,
        2: ACTION_OTHER_MBUTTON_DOWN,
        3: ACTION_OTHER_MBUTTON_DOWN,
        # 4: ACTION_OTHER_MBUTTON_DOWN,
    },
    # #     pygame.MOUSEBUTTONUP: {
    # #         1: ACTION_HOLD_FIRE,
    # #     },
    pygame.MOUSEMOTION: {
        None: ACTION_MOUSE_MOTION,
    },
    pygame.KEYDOWN: {

        (pygame.K_F1, ANY_MOD): ACTION_TOGGLE_DEBUG_RENDER,
        (pygame.K_F4, pygame.KMOD_ALT): ACTION_QUIT
    },
    # #     pygame.KEYUP: {
    # #         (pygame.K_ESCAPE, ANY_MOD): ACTION_WIN_LEVEL,
    # #         (pygame.K_a, ANY_MOD): ACTION_STOP_LEFT,
    # #         (pygame.K_d, ANY_MOD): ACTION_STOP_RIGHT,
    # #         (pygame.K_w, ANY_MOD): ACTION_STOP_UP,
    # #         (pygame.K_s, ANY_MOD): ACTION_STOP_DOWN,
    # #     },
    pygame.QUIT: {None: ACTION_QUIT},

})

music_ended_pygame_event = pygame.USEREVENT

# game events
EVT_QUIT = _global_id_generator.next()

EVT_TOWER_CREATED = _global_id_generator.next()  # args: building
EVT_UPGRADE_CENTER_CREATED = _global_id_generator.next()
EVT_POWER_STATION_CREATED = _global_id_generator.next()
EVT_SCIENCE_CENTER_CREATED = _global_id_generator.next()
EVT_CHIPS_FACTORY_CREATED = _global_id_generator.next()
EVT_BATTERY_CREATED = _global_id_generator.next()

EVT_TOWER_DESTROYED = _global_id_generator.next()
EVT_UPGRADE_CENTER_DESTROYED = _global_id_generator.next()
EVT_POWER_STATION_DESTROYED = _global_id_generator.next()
EVT_SCIENCE_CENTER_DESTROYED = _global_id_generator.next()
EVT_CHIPS_FACTORY_DESTROYED = _global_id_generator.next()
EVT_BATTERY_DESTROYED = _global_id_generator.next()

EVT_BUILDING_UPGRADED = _global_id_generator.next()
EVT_TYPE_UNLOCKED = _global_id_generator.next()

EVT_UNLOCKED = _global_id_generator.next()  # args: button, unlock_type
EVT_UI_SWITCHED_TO_SELECTION_MOUSE_MODE = _global_id_generator.next()  #
EVT_UI_SWITCHED_TO_PLACING_MOUSE_MODE = _global_id_generator.next()  #
EVT_BUILDING_PLACED = _global_id_generator.next()  #
EVT_BUILDING_REMOVED = _global_id_generator.next()  #
EVT_BUILDING_PLACED_IN_WORLD = _global_id_generator.next()  #

EVT_FOCUS_GAINED = _global_id_generator.next()  # event_type, sprite
EVT_FOCUS_LOST = _global_id_generator.next()  # event_type, sprite
EVT_SELECTED = _global_id_generator.next()  # event_type, sprite
EVT_DESELECTED = _global_id_generator.next()  # event_type, sprite

EVT_ANT_CREATED = _global_id_generator.next()  # event_type, ant

EVT_TIME_INDICATOR_ADDED = _global_id_generator.next()  # event_type, entity
EVT_TIME_INDICATOR_REMOVED = _global_id_generator.next()  # event_type, entity

EVT_POOL_CREATED = _global_id_generator.next()  # event_type, entity

EVT_NEW_SYSTEM_MESSAGE = _global_id_generator.next()  # event_type, entity
EVT_WAVE_STARTED = _global_id_generator.next()  # event_type, entity
EVT_BULLETS_CREATED = _global_id_generator.next()  # event_type, entity
EVT_BULLETS_DESTROYED = _global_id_generator.next()  # event_type, entity
EVT_ANTS_DIED = _global_id_generator.next()  # event_type, entity
EVT_OUT_OF_POWER = _global_id_generator.next()  # event_type, entity
EVT_POWER_RESTORED = _global_id_generator.next()  # event_type, entity
EVT_NO_CHIPS_TO_FIRE = _global_id_generator.next()  # event_type, entity
EVT_READY_TO_FIRE = _global_id_generator.next()  # event_type, entity
EVT_PATH_CREATED = _global_id_generator.next()  # event_type, entity
EVT_BUILDING_HIT = _global_id_generator.next()  # event_type, entity
EVT_ANT_TURNED = _global_id_generator.next()  # event_type, entity

EVT_SOUND_STARTED = _global_id_generator.next()  # event_type, entity

# ------------------------------------------------------------------------------
# Kinds
# ------------------------------------------------------------------------------
generator = FlagGenerator()

KIND_TOWER_NORMAL = generator.next()
KIND_TOWER_SNIPER = generator.next()
KIND_TOWER_MACHINE_GUN = generator.next()
KIND_TOWER_FAN = generator.next()
KIND_E = generator.next()
KIND_F = generator.next()
KIND_UPGRADE = generator.next()
KIND_POWER = generator.next()
KIND_SCIENCE = generator.next()
KIND_CHIPS_FACTORY = generator.next()
KIND_BATTERY = generator.next()
KIND_ENERGY_STORAGE_POOL = generator.next()
KIND_CRYPTO_CHIPS_STORAGE_POOL = generator.next()
KIND_TIME_INDICATOR = generator.next()
KIND_BULLET_A = generator.next()
KIND_ANT = generator.next()

# ------------------------------------------------------------------------------
# World
# ------------------------------------------------------------------------------
cell_size = 100
initial_energy_pool_capacity = 10
ant_max_search_radius = screen_width * 3  # should be wide enough to cover all the screen and world
ant_turn_angle = 30
ant_turn_angle_step = 10

initial_crypto_chips = 20 + 20  # orig: 20; bonus 20 so player has time to read the messages
prevent_stacking = True

# ------------------------------------------------------------------------------
# Map
# ------------------------------------------------------------------------------
map_dir = 'data/maps'
map_files = (
    'paths_1.json',
)

# ------------------------------------------------------------------------------
# Stepper
# ------------------------------------------------------------------------------
STEP_DT = 1.0 / 50.0             # [1.0 / fps] = [s]
MAX_DT = 20 * STEP_DT                      # [s]
WAVE_TIMER_INTERVAL = 1.0 / 25.0  # [ 1.0 / ticks_per_sec] = [s]

# ------------------------------------------------------------------------------
# UI
# ------------------------------------------------------------------------------
hud_height = 66
main_screen_rect = (0, 0, screen_width, screen_height - hud_height)
hud_screen_rect = (0, main_screen_rect[3], screen_width, screen_height - main_screen_rect[3])
build_button_size = (hud_height * 4 // 5, hud_height)
scroll_button_size = (hud_height * 2 // 5, hud_height)

health_bar_size = (50, 10)
health_bar_back_color = (0, 150, 0)
health_bar_color = (0, 200, 0)

level_indicator_color = (255, 255, 0)
use_level_indicator_back_ground = True
level_indicator_back_color = (0, 0, 150)
level_indicator_radius = 3
level_indicator_step_size = 8
level_indicator_x_offset = -health_bar_size[0] // 2 + level_indicator_radius + 2
level_indicator_y_offset = 10 - health_bar_size[1]
max_levels = 6

build_bar_size = (50, 10)
build_bar_back_color = (0, 38, 255)
build_bar_color = (0, 255, 255)

normal_message_wait = 4.0
short_message_wait = 0.8

game_won_message = "...GAME WON..."
game_lost_message = "...GAME LOST..."
game_over_messages = game_won_message, game_lost_message

# hud
LAYER_BUILD_BUTTONS = 90
LAYER_SCROLL_BUTTONS = 100
LAYER_HUD = LAYER_SCROLL_BUTTONS + 10
LAYER_LABEL_HUD = LAYER_HUD + 1
LAYER_MOUSE_SPRITE = 2000000000

# main
LAYER_PATH = 1
LAYER_TOWER = 100
LAYER_BULLET = LAYER_TOWER
LAYER_INDICATOR = LAYER_TOWER + 100
LAYER_TOWER_LOW_CHIPS = LAYER_TOWER + 10
LAYER_TOWER_POWER_LOSS = LAYER_TOWER + 20
LAYER_HEALTH = 700
LAYER_SYSTEM_MESSAGE = 800


playfield_rect = pygame.Rect(main_screen_rect)

chipworks_rect = playfield_rect.copy()
contested_lands_rect = playfield_rect.copy()
ant_country_rect = playfield_rect.copy()
chipworks_rect.h *= 0.3
contested_lands_rect.h *= 0.5
contested_lands_rect.y = chipworks_rect.bottom
ant_country_rect.h -= chipworks_rect.h + contested_lands_rect.h
ant_country_rect.y = contested_lands_rect.bottom

# ------------------------------------------------------------------------------
# Graphics
# ------------------------------------------------------------------------------

gfx_path = "data/gfx"

# for use with rotozoom, to scale tower and mouse sprite size
widget_scale = 0.75


# queue holds the texts
system_message_y = screen_height * 0.2

# cached surfaces (required by spritefx)
# Note: pygametext does its own caching. No need to cache surfaces from pygametext.getsurf() unless you transform them.
# Note: these settings are not tunable on the fly. If you want to change the image cache settings during runtime, you'll
# have to call the image_cache instance's methods.
# Note: If you rely on aging or memory cap, remember to call image_cache.update(), or purge_memory() or purge_old().
# TODO: if we need image cache...
# image_cache_expire = 1 * 60  # expire unused textures after N seconds; recommend 1 min (1 * 60)
# image_cache_memory = 5 * 2 ** 20  # force expiration if memory is exceeded; recommend 5 MB (5 * 2**20)
# image_cache = imagecache.ImageCache(image_cache_expire, image_cache_memory)
# image_cache.enable_age_cap(True)

pygametext.FONT_NAME_TEMPLATE = 'data/font/%s'
pygametext.MEMORY_LIMIT_MB = 32  # it takes about 5 min to hit the default 64 MB, so 32 is not too aggressive

# font_themes is used with the functions in module pygametext
# Example call to render some text using a theme:
#   image = pygametext.getsurf('some text', **settings.font_themes['intro'])
# font_themes can be accessed by nested dict keys:
#   font_theme['intro']['fontname']
font_themes = dict(
    intro=dict(
        fontname='Boogaloo.ttf',
        fontsize=52,
        color='red2',
        gcolor='deepskyblue',
        ocolor='yellow',
        owidth=0.5,
        scolor=None,
        shadow=None,
    ),
    mainmenu=dict(
        fontname='Bubblegum_Sans.ttf',
        fontsize=80,
        color='red2',
        gcolor='orange1',
        ocolor='yellow',
        owidth=0.5,
        scolor=None,
        shadow=None,
    ),
    game=dict(
        fontname='VeraBd.ttf',
        fontsize=38,
        color='yellow',
        gcolor='orange3',
        ocolor=None,
        owidth=0.5,
        scolor='grey20',
        shadow=(-1, 1),
        italic=True,
    ),
    gamehud=dict(
        fontname='VeraBd.ttf',
        fontsize=25,
        color='yellow',
        gcolor='orange2',
        ocolor=None,
        owidth=0.5,
        scolor='grey20',
        shadow=(1, 1),
    ),
    floatingtext=dict(
        fontname='Vera.ttf',
        fontsize=18,
        color='red',
        gcolor='lightblue4',
        ocolor='yellow',
        owidth=0.5,
        scolor=None,
        shadow=None,
    ),
)

# ------------------------------------------------------------------------------
# DEBUGS
# ------------------------------------------------------------------------------
use_test_ants = False

# win game after completing this wave
# e.g. to play up to Wave5 -> last_wave_num = 6
# note that you can use this to add higher waves:
# 1. WaveN classes can be tweaked or added in wave.py
# 2. the self.wave_sequence list must contain the number of waves objects to play; they can be ordered to your liking,
#    i.e. don't have to be in numeric order [Wave0(), Wave1(), ...]
last_wave_num = 13


# noinspection PyBroadException
try:
    # noinspection PyUnresolvedReferences,PyProtectedMember
    from gamelib._custom import *
except ImportError:
    pass

logger.debug("imported")
