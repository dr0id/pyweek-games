# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'resource_gfx.py' is part of codogupywk27
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The gfx resources.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

from gamelib.settings import KIND_E, KIND_F, KIND_TOWER_FAN, KIND_TOWER_MACHINE_GUN, KIND_TOWER_NORMAL, \
    KIND_TOWER_SNIPER, KIND_UPGRADE, KIND_CHIPS_FACTORY, KIND_POWER, KIND_SCIENCE, KIND_BATTERY

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")

image_names = {
    (KIND_E, 0): "TowerE0.png",
    (KIND_E, 1): "TowerE1.png",
    (KIND_E, 2): "TowerE2.png",
    (KIND_E, 3): "TowerE3.png",
    (KIND_E, 4): "TowerE4.png",
    (KIND_E, 5): "TowerE5.png",
    (KIND_E, 6): "TowerE6.png",
    (KIND_F, 0): "TowerF0.png",
    (KIND_F, 1): "TowerF1.png",
    (KIND_F, 2): "TowerF2.png",
    (KIND_F, 3): "TowerF3.png",
    (KIND_F, 4): "TowerF4.png",
    (KIND_F, 5): "TowerF5.png",
    (KIND_F, 6): "TowerF6.png",
    (KIND_TOWER_FAN, 0): "TowerFan0.png",
    (KIND_TOWER_FAN, 1): "TowerFan1.png",
    (KIND_TOWER_FAN, 2): "TowerFan2.png",
    (KIND_TOWER_FAN, 3): "TowerFan3.png",
    (KIND_TOWER_FAN, 4): "TowerFan4.png",
    (KIND_TOWER_FAN, 5): "TowerFan5.png",
    (KIND_TOWER_FAN, 6): "TowerFan6.png",
    (KIND_TOWER_MACHINE_GUN, 0): "TowerMachineGun0.png",
    (KIND_TOWER_MACHINE_GUN, 1): "TowerMachineGun1.png",
    (KIND_TOWER_MACHINE_GUN, 2): "TowerMachineGun2.png",
    (KIND_TOWER_MACHINE_GUN, 3): "TowerMachineGun3.png",
    (KIND_TOWER_MACHINE_GUN, 4): "TowerMachineGun4.png",
    (KIND_TOWER_MACHINE_GUN, 5): "TowerMachineGun5.png",
    (KIND_TOWER_MACHINE_GUN, 6): "TowerMachineGun6.png",
    (KIND_TOWER_NORMAL, 0): "TowerNormal0.png",
    (KIND_TOWER_NORMAL, 1): "TowerNormal1.png",
    (KIND_TOWER_NORMAL, 2): "TowerNormal2.png",
    (KIND_TOWER_NORMAL, 3): "TowerNormal3.png",
    (KIND_TOWER_NORMAL, 4): "TowerNormal4.png",
    (KIND_TOWER_NORMAL, 5): "TowerNormal5.png",
    (KIND_TOWER_NORMAL, 6): "TowerNormal6.png",
    (KIND_TOWER_SNIPER, 0): "TowerSniper0.png",
    (KIND_TOWER_SNIPER, 1): "TowerSniper1.png",
    (KIND_TOWER_SNIPER, 2): "TowerSniper2.png",
    (KIND_TOWER_SNIPER, 3): "TowerSniper3.png",
    (KIND_TOWER_SNIPER, 4): "TowerSniper4.png",
    (KIND_TOWER_SNIPER, 5): "TowerSniper5.png",
    (KIND_TOWER_SNIPER, 6): "TowerSniper6.png",
    # todo other levels
    (KIND_UPGRADE, 0): "Upgrade0.png",
    (KIND_UPGRADE, 1): "Upgrade1.png",
    (KIND_UPGRADE, 2): "Upgrade2.png",
    (KIND_UPGRADE, 3): "Upgrade3.png",
    (KIND_UPGRADE, 4): "Upgrade4.png",
    (KIND_UPGRADE, 5): "Upgrade5.png",
    (KIND_UPGRADE, 6): "Upgrade6.png",
    (KIND_CHIPS_FACTORY, 0): "ChipsFactory0.png",
    (KIND_CHIPS_FACTORY, 1): "ChipsFactory1.png",
    (KIND_CHIPS_FACTORY, 2): "ChipsFactory2.png",
    (KIND_CHIPS_FACTORY, 3): "ChipsFactory3.png",
    (KIND_CHIPS_FACTORY, 4): "ChipsFactory4.png",
    (KIND_CHIPS_FACTORY, 5): "ChipsFactory5.png",
    (KIND_CHIPS_FACTORY, 6): "ChipsFactory6.png",
    (KIND_POWER, 0): "Power0.png",
    (KIND_POWER, 1): "Power1.png",
    (KIND_POWER, 2): "Power2.png",
    (KIND_POWER, 3): "Power3.png",
    (KIND_POWER, 4): "Power4.png",
    (KIND_POWER, 5): "Power5.png",
    (KIND_POWER, 6): "Power6.png",
    (KIND_SCIENCE, 0): "ScienceCenter0.png",
    (KIND_SCIENCE, 1): "ScienceCenter1.png",
    (KIND_SCIENCE, 2): "ScienceCenter2.png",
    (KIND_SCIENCE, 3): "ScienceCenter3.png",
    (KIND_SCIENCE, 4): "ScienceCenter4.png",
    (KIND_SCIENCE, 5): "ScienceCenter5.png",
    (KIND_SCIENCE, 6): "ScienceCenter6.png",
    (KIND_BATTERY, 0): "Battery0.png",
    (KIND_BATTERY, 1): "Battery1.png",
    (KIND_BATTERY, 2): "Battery2.png",
    (KIND_BATTERY, 3): "Battery3.png",
    (KIND_BATTERY, 4): "Battery4.png",
    (KIND_BATTERY, 5): "Battery5.png",
    (KIND_BATTERY, 6): "Battery6.png",

    "bullet": "potato_chip.png",
    "noPower": "nopower.png",
    "noChips": "nochips.png",
    "locked": "lock.png",
    "scrollR": "scrollr.png",

    (KIND_E, -1): "LabeledTowerE0.png",
    (KIND_F, -1): "LabeledTowerF0.png",
    (KIND_TOWER_FAN, -1): "LabeledTowerFan0.png",
    (KIND_TOWER_MACHINE_GUN, -1): "LabeledTowerMachineGun0.png",
    (KIND_TOWER_NORMAL, -1): "LabeledTowerNormal0.png",
    (KIND_TOWER_SNIPER, -1): "LabeledTowerSniper0.png",
    (KIND_UPGRADE, -1): "LabeledUpgrade0.png",
    (KIND_CHIPS_FACTORY, -1): "LabeledChipsFactory0.png",
    (KIND_POWER, -1): "LabeledPower0.png",
    (KIND_SCIENCE, -1): "LabeledScienceCenter0.png",
    (KIND_BATTERY, -1): "LabeledBattery0.png",

    "ant": "ant.png"

}


logger.debug("imported")
