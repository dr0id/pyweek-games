# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'game_context.py' is part of codogupywk27
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The main game context

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging
from itertools import chain

import pygame

from gamelib import entitytypes, resource_sound
from gamelib import pygametext
from gamelib import settings
from gamelib.ants import TestAnt
from gamelib.buildings import PowerStation, Tower, UpgradeCenter, ScienceCenter, ChipsFactory, Battery, TimeIndicator, \
    Building
from gamelib.entitytypes import ChipsFactory0, EnergyStoragePool, CryptoChipsStoragePool, TowerNormal0, PowerStation0, \
    UpgradeCenter0, TowerSniper0, ScienceCenter0, TowerMachineGun0, Battery0, TowerFan0, TowerF0, TowerE0, \
    entity_values
from gamelib.eventing import event_dispatcher
from gamelib.level import Level
from gamelib.settings import EVT_QUIT, EVT_BUILDING_PLACED, KIND_POWER, KIND_TOWER_NORMAL, KIND_TOWER_SNIPER, \
    KIND_TOWER_MACHINE_GUN, KIND_TOWER_FAN, KIND_E, KIND_F, KIND_ANT, \
    KIND_UPGRADE, KIND_SCIENCE, KIND_CHIPS_FACTORY, KIND_BATTERY, EVT_ANT_CREATED, EVT_TIME_INDICATOR_ADDED, \
    ACTION_UPGRADE_BUILDING, EVT_TOWER_CREATED, EVT_TOWER_DESTROYED, EVT_POOL_CREATED, \
    initial_energy_pool_capacity, EVT_TYPE_UNLOCKED, EVT_CHIPS_FACTORY_CREATED, EVT_BUILDING_UPGRADED, \
    EVT_SCIENCE_CENTER_CREATED, EVT_BULLETS_CREATED, EVT_BULLETS_DESTROYED, EVT_SOUND_STARTED, \
    EVT_ANTS_DIED, EVT_NEW_SYSTEM_MESSAGE, EVT_UPGRADE_CENTER_DESTROYED, EVT_POWER_STATION_DESTROYED, \
    EVT_SCIENCE_CENTER_DESTROYED, EVT_CHIPS_FACTORY_DESTROYED, EVT_BATTERY_DESTROYED, EVT_PATH_CREATED, \
    EVT_UPGRADE_CENTER_CREATED, initial_crypto_chips, chipworks_rect, EVT_BUILDING_PLACED_IN_WORLD, prevent_stacking
from gamelib.timestep_context import TimeSteppedContext
from gamelib.wave import Wave
from pyknic.mathematics import Point3
from pyknic.timing import Scheduler, Timer
from pyknic.tweening import Tweener

chain_from_iterable = chain.from_iterable

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class StartInfo(object):

    def __init__(self):
        self.existing_buildings = []  # [chipsFactory, battery, ...]
        self.new_buildings = []  # [(building_type, Point()), ]
        self.wave_num = 1  # starts with 1-6
        self.map_num = 0


class GameContext(TimeSteppedContext):

    def __init__(self, client, space, start_info):
        TimeSteppedContext.__init__(self)
        self.start_info = start_info
        self.space = space
        self.client = client
        self.game_clock = self._game_time
        self.update_entities = set()
        self.scheduler = Scheduler()
        self.tweener = Tweener()
        self.level = None
        self.wave = None
        self._towers = set()
        self._bullets = set()
        self.energy_pool = EnergyStoragePool()
        self.energy_pool.capacity = initial_energy_pool_capacity
        self.chips_pool = CryptoChipsStoragePool()
        self.chips_pool.amount = initial_crypto_chips
        self.type_un_locker = TypeUnLocker()
        self.debug_hud_timer = None
        self._factories = []
        self._upgrades_enabled = 0
        self.game_over = False

    def enter(self):
        TimeSteppedContext.enter(self)
        event_dispatcher.register_event_type(EVT_QUIT)

        event_dispatcher.add_listener(EVT_QUIT, self._on_quit)
        event_dispatcher.add_listener(EVT_BUILDING_PLACED, self.on_building_placed)
        event_dispatcher.add_listener(ACTION_UPGRADE_BUILDING, self.on_upgrade)
        event_dispatcher.add_listener(EVT_TOWER_CREATED, self.on_tower_created)
        event_dispatcher.add_listener(EVT_UPGRADE_CENTER_CREATED, self.on_upgrade_center_created)
        event_dispatcher.add_listener(EVT_TOWER_DESTROYED, self.on_tower_removed)
        # event_dispatcher.add_listener(EVT_BUILDING_REMOVED, self.on_tower_removed)  # todo: not needed anymore?

        # event_dispatcher.add_listener(EVT_TOWER_DESTROYED, self.on_building_destroyed)
        event_dispatcher.add_listener(EVT_UPGRADE_CENTER_DESTROYED, self.on_building_destroyed)
        event_dispatcher.add_listener(EVT_UPGRADE_CENTER_DESTROYED, self.on_upgrade_center_destroyed)
        event_dispatcher.add_listener(EVT_POWER_STATION_DESTROYED, self.on_building_destroyed)
        event_dispatcher.add_listener(EVT_SCIENCE_CENTER_DESTROYED, self.on_building_destroyed)
        event_dispatcher.add_listener(EVT_CHIPS_FACTORY_DESTROYED, self.on_building_destroyed)
        event_dispatcher.add_listener(EVT_BATTERY_DESTROYED, self.on_building_destroyed)

        # todo: remove test event, this should come from the game context at start up when placing first chips factory
        for t, p in self.start_info.new_buildings:
            event_dispatcher.fire(EVT_BUILDING_PLACED, t, p)
        # event_dispatcher.fire(EVT_BUILDING_PLACED, ChipsFactory0, Point(100, 100))
        event_dispatcher.fire(EVT_POOL_CREATED, self.energy_pool)
        event_dispatcher.fire(EVT_POOL_CREATED, self.chips_pool)

        # paths for ants to follow
        map_file = '{}/{}'.format(settings.map_dir, settings.map_files[self.start_info.map_num])
        logger.debug('ANT PATHS: loading from map file: {}', map_file)
        self.level = Level()
        self.level.load_map(map_file, self.scheduler)

        # translate the paths to the Contested Territory
        contested_lands_rect = self.client.contested_lands_rect
        for path in self.level.ant_paths:
            for node in path:
                # node.position.y += contested_lands_rect.y
                node.position.y = \
                    (node.position.y / self.level.height) * contested_lands_rect.h + contested_lands_rect.y

            event_dispatcher.fire(EVT_PATH_CREATED, path)

        if settings.use_test_ants:
            # todo: remove test ants
            # for i in range(10000):
            #     # for ant_path in self.level.ant_paths:
            #     #     TODO: use the paths
            #     ant_path = self.level.ant_paths[i % len(self.level.ant_paths)]
            #
            #     def _create_ant(path):
            #         ant = Ant(path[0].position.clone(), path, AntTemplate, Timer(scheduler=self.scheduler),self.space)
            #         self.update_entities.add(ant)
            #         event_dispatcher.fire(EVT_ANT_CREATED, ant)
            #         return 0
            #
            #     self.scheduler.schedule(_create_ant, 0.5 * i, ant_path)

            # x, y, w, h = contested_lands_rect
            # for _y in range(y, y + h, 100):
            #     for _x in range(x, x+w, 120):
            #         ant = TestAnt(Point3(_x, _y))
            #         self.update_entities.add(ant)
            #         event_dispatcher.fire(EVT_ANT_CREATED, ant)

            x, y, w, h = chipworks_rect
            x_coords = range(x, x + w, 10)
            self.chips_pool.amount = 100000000000000000000000000
            self.energy_pool.capacity = 1000000000000000000
            self.energy_pool.amount = self.energy_pool.capacity
            TypeUnLocker.unlock_everything()

            import math
            _xx = 0
            _y = 100
            for k, levels in entity_values.items():
                if k == KIND_E or k == KIND_F:
                    continue
                _x = 80 + (40 if _xx % 2 == 0 else 0)
                for level in levels:
                    event_dispatcher.fire(EVT_BUILDING_PLACED, level, Point3(_x, _y))
                    if k == KIND_TOWER_FAN:
                        r = 35
                        for a in range(0, 360, 40):
                            ant = TestAnt(
                                Point3(_x + r * math.sin(math.radians(a)), _y + r * math.cos(math.radians(a))))
                            self.update_entities.add(ant)
                            event_dispatcher.fire(EVT_ANT_CREATED, ant)

                    ant = TestAnt(Point3(_x + 5, _y + 30))
                    self.update_entities.add(ant)
                    event_dispatcher.fire(EVT_ANT_CREATED, ant)

                    _x += 120
                _y += 50 #120  # 50 to show all
                _xx += 1  # x offset

        else:
            self.wave = Wave(self.start_info.wave_num, self.scheduler, self.tweener, self.update_entities, self.level,
                             self.space)
            self.wave.timer.start()

        self.debug_hud_timer = Timer(1.0 / 5.0, True, 'debug hud', self.scheduler)
        self.debug_hud_timer.event_elapsed += self.client.debug_hud.update
        self.debug_hud_timer.start()

        def clean_debug_hud(*args):
            pygametext.clean()

        self.clean_debug_hud_timer = Timer(10.0, True, 'clean debug hud', self.scheduler)
        self.clean_debug_hud_timer.event_elapsed += clean_debug_hud
        self.clean_debug_hud_timer.start()

        self.scheduler.schedule(self.check_game_end_conditions, 0.5)

    def update_step(self, delta, sim_time, *args):
        self.scheduler.update(delta, sim_time, *args)
        for e in self.update_entities:
            e.update(delta, sim_time)

        self.space.adds(self.update_entities)  # update positions

        other = self.space.collides(self._towers)
        new_bullets = set()
        for tower, others in other.items():
            if not others:
                continue
            ants = set((a for a in others if a.kind == KIND_ANT))
            if not ants:
                continue
            tower.on_in_range_enemies_update(ants)
            if tower.bullets:
                new_bullets.update(tower.bullets)
                tower.bullets[:] = []

        if new_bullets:
            event_dispatcher.fire(EVT_BULLETS_CREATED, new_bullets)
            self._bullets.update(new_bullets)
        # todo: bullets created event (so they are drawn)

        for b in self._bullets:
            b.update(delta, sim_time)

        # todo: bullets effect on ant
        done_bullets = set((b for b in self._bullets if b.duration <= 0))
        if done_bullets:
            self._bullets.difference_update(done_bullets)
            event_dispatcher.fire(EVT_BULLETS_DESTROYED, done_bullets)

            dead_ants = set(chain_from_iterable((b.impact() for b in done_bullets)))
            dead_ants.discard(None)
            self.update_entities.difference_update(dead_ants)
            self.space.removes(dead_ants)
            event_dispatcher.fire(EVT_ANTS_DIED, dead_ants)
            self.client.num_ants_died += len(dead_ants)

        self.client.update_step(delta, sim_time, *args)

        self.tweener.update(delta)

    def check_game_end_conditions(self):
        if self.game_over:
            return 10.0

        if len(self._factories) <= 0:
            logger.error("GAME LOST!")
            event_dispatcher.fire(EVT_NEW_SYSTEM_MESSAGE, settings.game_lost_message)
            self.wave.timer.stop()
            # necessary, because MusicPlayer.fadeout() blocks
            pygame.mixer.music.fadeout(1000)
            settings.music_player.fill_music_carousel([resource_sound.song_lose])
            self.game_over = True

        if self.wave and self.wave.is_game_won():
            logger.debug("GAME WON!")
            event_dispatcher.fire(EVT_NEW_SYSTEM_MESSAGE, settings.game_won_message)
            event_dispatcher.fire(EVT_SOUND_STARTED, resource_sound.SFX_BUILDING_DESTROYED)
            self.wave.timer.stop()
            # necessary, because MusicPlayer.fadeout() blocks
            pygame.mixer.music.fadeout(1000)
            settings.music_player.fill_music_carousel([resource_sound.song_win])
            self.game_over = True

        return 0.5

        # if self.wave:
        #     if self.wave.is_game_won():
        #         logger.debug('condition: GAME WON')
        #         self.pop()
        #     elif self.wave.is_wave_won():
        #         logger.debug('condition: WAVE WON')
        #         space = SphereBinSpace2(cell_size)
        #         client = Client(self.client.music_player, space)
        #         info = StartInfo()
        #         info.new_buildings = [(ChipsFactory0, Point3(100, 100))]
        #         info.wave_num = self.start_info.wave_num + 1
        #         game = GameContext(client, space, info)
        #         self.exchange(game)
        #     elif self.wave.is_wave_lost():
        #         logger.debug('condition: WAVE LOST')
        #         self.pop()
        #     elif self.wave.is_wave_quit():
        #         logger.debug('condition: WAVE QUIT')
        #         self.pop()

    def draw(self, screen, do_flip=True, interpolation_factor=1.0):
        self.client.draw(screen, do_flip, interpolation_factor)

    def on_building_placed(self, event_type, building_type, position):
        # HACK: this only works because world and screen coordinates are the same!
        if settings.ant_country_rect.collidepoint(position.as_xy_tuple(int)):
            event_dispatcher.fire(EVT_NEW_SYSTEM_MESSAGE, "Can't build in ant country!", True)
            return

        if self.chips_pool.amount < building_type.build_cost:
            msg = "Need {0} crypto chips to build!".format(building_type.build_cost)
            event_dispatcher.fire(EVT_NEW_SYSTEM_MESSAGE, msg, True)
            return

        if prevent_stacking:
            x, y = position.as_xy_tuple()
            hits = self.space.get_in_rect(x, y, building_type.radius * 2, building_type.radius * 2)
            if hits:
                for hit in hits:
                    if position.get_distance(hit.position) < (hit.radius + building_type.radius) * 0.75:
                        msg = "Spot is taken already!".format(building_type.build_cost)
                        event_dispatcher.fire(EVT_NEW_SYSTEM_MESSAGE, msg, True)
                        return

        self.chips_pool.amount -= building_type.build_cost

        if True:
            building = None
            if building_type.kind == KIND_TOWER_NORMAL or \
                    building_type.kind == KIND_TOWER_SNIPER or \
                    building_type.kind == KIND_TOWER_MACHINE_GUN or \
                    building_type.kind == KIND_TOWER_FAN or \
                    building_type.kind == KIND_E or \
                    building_type.kind == KIND_F:
                building = Tower(building_type, position, Timer(scheduler=self.scheduler),
                                 Timer(scheduler=self.scheduler),
                                 Timer(scheduler=self.scheduler), Timer(scheduler=self.scheduler), self.energy_pool,
                                 self.chips_pool)
            elif building_type.kind == KIND_UPGRADE:
                building = UpgradeCenter(building_type, position, Timer(scheduler=self.scheduler),
                                         Timer(scheduler=self.scheduler),
                                         Timer(scheduler=self.scheduler), Timer(scheduler=self.scheduler),
                                         self.energy_pool, self.chips_pool)
            elif building_type.kind == KIND_POWER:
                building = PowerStation(building_type, position, Timer(scheduler=self.scheduler),
                                        Timer(scheduler=self.scheduler),
                                        Timer(scheduler=self.scheduler), Timer(scheduler=self.scheduler),
                                        self.energy_pool, self.chips_pool)
            elif building_type.kind == KIND_SCIENCE:
                building = ScienceCenter(building_type, position, Timer(scheduler=self.scheduler),
                                         Timer(scheduler=self.scheduler),
                                         Timer(scheduler=self.scheduler), Timer(scheduler=self.scheduler),
                                         self.energy_pool, self.chips_pool)
            elif building_type.kind == KIND_CHIPS_FACTORY:
                building = ChipsFactory(building_type, position, Timer(scheduler=self.scheduler),
                                        Timer(scheduler=self.scheduler),
                                        Timer(scheduler=self.scheduler), Timer(scheduler=self.scheduler),
                                        self.energy_pool, self.chips_pool)
                self._factories.append(building)
            elif building_type.kind == KIND_BATTERY:
                building = Battery(building_type, position, Timer(scheduler=self.scheduler),
                                   Timer(scheduler=self.scheduler),
                                   Timer(scheduler=self.scheduler), Timer(scheduler=self.scheduler), self.energy_pool,
                                   self.chips_pool)
            else:
                raise Exception("unknown kind to build {0}", building_type.kind)
            t = TimeIndicator(self.game_clock, building.position, building.template.build_time)
            event_dispatcher.fire(EVT_TIME_INDICATOR_ADDED, t)
            event_dispatcher.fire(EVT_BUILDING_PLACED_IN_WORLD, building)
            self.space.add(building)

    def on_upgrade(self, event_type, building, *args):
        if not isinstance(building, Building):
            return

        if self._upgrades_enabled <= 0:
            event_dispatcher.fire(EVT_NEW_SYSTEM_MESSAGE, "Need an upgrade center!", True)
            return

        if building.is_in_construction():
            event_dispatcher.fire(EVT_NEW_SYSTEM_MESSAGE, "Cannot upgrade while under construction!", True)
            return

        if entitytypes.is_upgrade_available(building.kind, building.level):

            upgraded_type = entitytypes.entity_values[building.kind][building.level + 1]
            logger.info("upgrading to level {0}", building.level + 1)

            if self.chips_pool.amount < upgraded_type.build_cost:
                msg = "Need {0} crypto chips to upgrade!".format(upgraded_type.build_cost)
                event_dispatcher.fire(EVT_NEW_SYSTEM_MESSAGE, msg, True)
                return

            building.on_upgrade(upgraded_type)

            t = TimeIndicator(self.game_clock, building.position, building.template.build_time)
            event_dispatcher.fire(EVT_TIME_INDICATOR_ADDED, t)
        else:
            event_dispatcher.fire(EVT_NEW_SYSTEM_MESSAGE, "Cannot upgrade!", True)

    def on_tower_created(self, event_type, tower, *args):
        self._towers.add(tower)

    def on_tower_removed(self, event_type, building, *args):
        if building in self._towers:
            self._towers.remove(building)
        self.on_building_destroyed(event_type, building, *args)

    def on_upgrade_center_created(self, event_type, building, *args):
        self._upgrades_enabled += 1

    def on_upgrade_center_destroyed(self, event_type, building, *args):
        self._upgrades_enabled -= 1

    def on_building_destroyed(self, event_type, building, *args):
        self.space.remove(building)
        if building.kind == KIND_CHIPS_FACTORY:
            self._factories.remove(building)

    def _on_quit(self, *args):
        self.pop()


class TypeUnLocker(object):

    def __init__(self):
        event_dispatcher.add_listener(EVT_CHIPS_FACTORY_CREATED, self._on_upgraded)
        event_dispatcher.add_listener(EVT_SCIENCE_CENTER_CREATED, self._on_upgraded)
        event_dispatcher.add_listener(EVT_BUILDING_UPGRADED, self._on_upgraded)
        event_dispatcher.add_listener(EVT_TYPE_UNLOCKED, self._on_unlocked)
        self._unlocked = {}  # {building_type: int}

    def _on_unlocked(self, event_type, building_type, *args):
        count = self._unlocked.setdefault(building_type, 0)
        self._unlocked[building_type] = count + 1

    def _on_upgraded(self, event_type, building, *args):
        if building.kind == KIND_CHIPS_FACTORY:
            if building.level == 0:
                self._unlock_building(TowerNormal0)
                self._unlock_building(PowerStation0)
                self._unlock_building(UpgradeCenter0)
            elif building.level == 1:
                self._unlock_building(TowerSniper0)
            elif building.level == 2:
                self._unlock_building(ScienceCenter0)
            elif building.level == 3:
                self._unlock_building(TowerMachineGun0)
        elif building.kind == KIND_SCIENCE:
            if building.level == 1:
                self._unlock_building(Battery0)
            elif building.level == 2:
                self._unlock_building(TowerFan0)
            # elif building.level == 3:
            #     self._unlock_building(TowerE0)
            # elif building.level == 4:
            #     self._unlock_building(TowerF0)
            elif building.level == 6:
                self._unlock_building(ChipsFactory0)

    def _unlock_building(self, building_type):
        if self._unlocked.get(building_type, 0) == 0:
            event_dispatcher.fire(EVT_TYPE_UNLOCKED, building_type)

    @staticmethod
    def unlock_everything():
        event_dispatcher.fire(EVT_TYPE_UNLOCKED, TowerNormal0)
        event_dispatcher.fire(EVT_TYPE_UNLOCKED, PowerStation0)
        event_dispatcher.fire(EVT_TYPE_UNLOCKED, UpgradeCenter0)
        event_dispatcher.fire(EVT_TYPE_UNLOCKED, TowerSniper0)
        event_dispatcher.fire(EVT_TYPE_UNLOCKED, ScienceCenter0)
        event_dispatcher.fire(EVT_TYPE_UNLOCKED, TowerMachineGun0)
        event_dispatcher.fire(EVT_TYPE_UNLOCKED, Battery0)
        event_dispatcher.fire(EVT_TYPE_UNLOCKED, TowerFan0)
        event_dispatcher.fire(EVT_TYPE_UNLOCKED, TowerE0)
        event_dispatcher.fire(EVT_TYPE_UNLOCKED, TowerF0)
        event_dispatcher.fire(EVT_TYPE_UNLOCKED, ChipsFactory0)


logger.debug("imported")
