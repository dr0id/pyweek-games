# -*- coding: utf-8 -*-

import logging

from gamelib import settings
from gamelib.ants import Ant, AntTemplate
from gamelib.eventing import event_dispatcher
from gamelib.settings import EVT_QUIT, KIND_ANT
from pyknic.timing import Timer

logger = logging.getLogger(__name__)
logger.debug('loading...')


class _StateBase(object):
    @staticmethod
    def enter(self):
        logger.debug('>>> Wave state ENTERED: {}', _StateBase)

    @staticmethod
    def exit(self):
        logger.debug('>>> Wave state EXITED: {}', _StateBase)

    @staticmethod
    def think(self, dt):
        new_state = self.start_state
        if new_state:
            self.start_state = None
            return new_state


class _StatePrepare(_StateBase):
    @staticmethod
    def enter(self):
        logger.debug('>>> Wave state ENTERED: {}', _StatePrepare)

        def evt_end_prep(timer):
            logger.debug('>>> Event END: {}', _StatePrepare)
            self.set_state(_StatePlay)

        def delayed_msg(text, wait):
            def _cb(*args):
                event_dispatcher.fire(settings.EVT_NEW_SYSTEM_MESSAGE, text, True)
            t = Timer(wait, scheduler=self.scheduler)
            t.event_elapsed += _cb
            t.start()

        t = Timer(4.0, scheduler=self.scheduler)
        t.event_elapsed += evt_end_prep
        t.start()
        event_dispatcher.fire(settings.EVT_NEW_SYSTEM_MESSAGE, 'GET READY! WAVE 1!')
        delayed_msg('Unlocked: Power Station', 12.0)
        delayed_msg('Unlocked: Normal Gun Tower', 22.0)
        # delayed_msg('Unlocked: Upgrade Center', 6.0)

    @staticmethod
    def exit(self):
        logger.debug('>>> Wave state EXITED: {}', _StatePrepare)

    @staticmethod
    def think(self, dt):
        pass


class _StatePlay(_StateBase):
    @staticmethod
    def enter(self):
        logger.debug('>>> Wave state ENTERED: {}', _StatePlay)
        self._start_wave()
        if self.wave_num > 1:
            event_dispatcher.fire(settings.EVT_NEW_SYSTEM_MESSAGE, 'WAVE {}!'.format(self.wave_num))
        event_dispatcher.fire(settings.EVT_WAVE_STARTED, self.wave_num)

    @staticmethod
    def exit(self):
        logger.debug('>>> Wave state EXITED: {}', _StatePlay)

    @staticmethod
    def think(self, dt):
        if self.wave_is_running():
            pass
        else:
            return _StateWonWave


class _StateWonWave(_StateBase):
    @staticmethod
    def enter(self):
        logger.debug('>>> Wave state ENTERED: {}', _StateWonWave)
        self._on_wave_ended()

    @staticmethod
    def exit(self):
        logger.debug('>>> Wave state EXITED: {}', _StateWonWave)

    @staticmethod
    def think(self, dt):
        if self.wave_num == settings.last_wave_num:
            return _StateWonGame
        else:
            self.wave_num += 1
            return _StatePlay


class _StateWonGame(_StateBase):
    @staticmethod
    def enter(self):
        logger.debug('>>> Wave state ENTERED: {}', _StateWonGame)
        self._on_game_ended()

    @staticmethod
    def exit(self):
        logger.debug('>>> Wave state EXITED: {}', _StateWonGame)

    @staticmethod
    def think(self, dt):
        pass


class WaveData(object):
    name = 'None'


class Ants(WaveData):
    name = 'ants'

    def __init__(self, templates, interval=1.0):
        self.templates = templates
        self.interval = interval
        self.n = 0


class Wait(WaveData):
    name = 'wait'

    def __init__(self, duration=1):
        self.duration = duration


class Message(WaveData):
    name = 'message'

    def __init__(self, text='None'):
        self.text = text


class FastMessage(WaveData):
    name = 'fastmessage'

    def __init__(self, text='None'):
        self.text = text


class Path(object):
    """data for the operation of a path: messages, waits, ants"""

    # Wave sets these before creating any Wave objects
    scheduler = None
    ant_paths = None
    update_entities = None
    space = None

    def __init__(self, num, actions=None):
        self.num = num
        self.actions = actions
        self.action = None
        self.is_running = True

    def on_action_ended(self, timer):
        logger.debug('>> >> path{} on_action_ended: {}', self.num, self.action)
        if self.action is None:
            self.next_action()
        elif self.action.name == 'ants':
            if self.action.n + 1 < len(self.action.templates):
                self.action.n += 1
                self._make_ant()
            else:
                self.action = None
                timer.stop()
                self.next_action()
        else:
            self.action = None
            self.next_action()

    def _make_ant(self):
        ant_path = self.ant_paths[self.num]
        ant = Ant(ant_path[0].position.clone(), ant_path, self.action.templates[self.action.n], Timer(scheduler=self.scheduler), self.space)
        self.update_entities.add(ant)
        event_dispatcher.fire(settings.EVT_ANT_CREATED, ant)

    def next_action(self):
        if self.actions:
            action = self.actions.pop(0)
            name = action.name
            if name == 'message':
                logger.debug('>> >> path{} message: {}', self.num, action.text)
                event_dispatcher.fire(settings.EVT_NEW_SYSTEM_MESSAGE, action.text)
                self.action = None
                self.next_action()
            elif name == 'fastmessage':
                logger.debug('>> >> path{} fastmessage: {}', self.num, action.text)
                event_dispatcher.fire(settings.EVT_NEW_SYSTEM_MESSAGE, action.text, True)
                self.action = None
                self.next_action()
            elif name == 'wait':
                logger.debug('>> >> path{} wait: {}', self.num, action.duration)
                self.action = action
                timer = Timer(action.duration, name='wave: wait', scheduler=self.scheduler)
                timer.event_elapsed += self.on_action_ended
                timer.start()
            elif name == 'ants':
                logger.debug('>> >> path{} ants: {}', self.num, action.templates)
                self.action = action
                timer = Timer(action.interval, repeat=True, name='wave: ants', scheduler=self.scheduler)
                timer.event_elapsed += self.on_action_ended
                timer.start()
                self._make_ant()
        else:
            self.is_running = False


class WaveTemplate(object):
    path0 = Path(0, [])
    path1 = Path(1, [])
    path2 = Path(2, [])
    path3 = Path(3, [])
    path4 = Path(4, [])
    path5 = Path(5, [])

    def __init__(self):
        self.all_paths = (
            self.path0,
            self.path1,
            self.path2,
            self.path3,
            self.path4,
            self.path5,
        )


class Wave0(WaveTemplate):
    path2 = Path(2, [
        Message('Towers and Facilities Use Energy'),
        Wait(5),
        Message('Build Two Power Stations Near Home'),
        Wait(10),
        Message('Build Two Towers Near Ant Convoy'),
        Wait(10),
        Ants([AntTemplate] * 5, interval=5),
        Message('Watch For Low Energy and Chips'),
        Message('Build An Upgrade Center'),
        Wait(3),
        FastMessage('Unlocked: Upgrade Center'),
        Wait(7),
        Message('Right-click Chips Factory To Upgrade'),
        Wait(4),
        Ants([AntTemplate] * 5, interval=5),
        Message('Build Power Stations'),
        Message('Right-click Power Station To Upgrade'),
        Wait(4),
        Ants([AntTemplate] * 10, interval=3),
        Message('Right-click Gun Towers To Upgrade'),
        Wait(4),
        Ants([AntTemplate] * 10, interval=3),
        Message('Need More Towers, Guns, Upgrades'),
        Wait(4),
    ])


class Wave1(WaveTemplate):
    path0 = Path(0, [
        Message('Build Sniper Towers At Key Points'),
        Wait(10),
        Ants([AntTemplate] * 10, interval=3),
        Wait(4),
        Ants([AntTemplate] * 10, interval=3),
        Wait(4),
        Ants([AntTemplate] * 20, interval=2),
        Wait(4),
        Ants([AntTemplate] * 20, interval=2),
    ])


class Wave2(WaveTemplate):
    path5 = Path(5, [Ants([AntTemplate] * 30, interval=1)])


class Wave3(WaveTemplate):
    path1 = Path(1, [Ants([AntTemplate] * 40, interval=1)])


class Wave4(WaveTemplate):
    path2 = Path(2, [Ants([AntTemplate] * 50, interval=1)])
    path4 = Path(4, [Ants([AntTemplate] * 50, interval=1)])


class Wave5(WaveTemplate):
    path0 = Path(0, [Ants([AntTemplate] * 60, interval=1)])
    path3 = Path(3, [Ants([AntTemplate] * 60, interval=1)])


class Wave6(WaveTemplate):
    path0 = Path(0, [
        Ants([AntTemplate] * 10, interval=1),
        Wait(4),
        Ants([AntTemplate] * 20, interval=0.8),
    ])
    path2 = Path(2, [
        Ants([AntTemplate] * 10, interval=1),
        Wait(3),
        Ants([AntTemplate] * 10, interval=0.8),
    ])


class Wave7(WaveTemplate):
    path0 = Path(0, [
        Ants([AntTemplate] * 10, interval=1),
        Wait(4),
        Ants([AntTemplate] * 20, interval=0.8),
    ])
    path1 = Path(1, [
        Ants([AntTemplate] * 10, interval=1),
        Wait(3),
        Ants([AntTemplate] * 10, interval=0.8),
    ])
    path5 = Path(5, [
        Ants([AntTemplate] * 10, interval=1),
        Wait(2),
        Ants([AntTemplate] * 10, interval=0.8),
    ])


class Wave8(WaveTemplate):
    path2 = Path(2, [
        Ants([AntTemplate] * 20, interval=0.8),
        Wait(3),
        Ants([AntTemplate] * 20, interval=0.7),
    ])
    path3 = Path(3, [
        Ants([AntTemplate] * 20, interval=0.8),
        Wait(3),
        Ants([AntTemplate] * 20, interval=0.7),
    ])
    path4 = Path(4, [
        Ants([AntTemplate] * 20, interval=0.8),
        Wait(3),
        Ants([AntTemplate] * 20, interval=0.7),
    ])


class Wave9(WaveTemplate):
    path0 = Path(0, [
        Ants([AntTemplate] * 20, interval=0.8),
        Wait(3),
        Ants([AntTemplate] * 20, interval=0.7),
    ])
    path1 = Path(1, [
        Ants([AntTemplate] * 20, interval=0.8),
        Wait(3),
        Ants([AntTemplate] * 20, interval=0.7),
    ])
    path5 = Path(5, [
        Ants([AntTemplate] * 20, interval=0.8),
        Wait(3),
        Ants([AntTemplate] * 20, interval=0.7),
    ])


class Wave10(WaveTemplate):
    path0 = Path(0, [
        Ants([AntTemplate] * 20, interval=0.8),
        Wait(3),
        Ants([AntTemplate] * 30, interval=0.7),
    ])
    path2 = Path(2, [
        Ants([AntTemplate] * 20, interval=0.8),
        Wait(5),
        Ants([AntTemplate] * 30, interval=0.7),
    ])
    path3 = Path(3, [
        Ants([AntTemplate] * 20, interval=0.8),
        Wait(6),
        Ants([AntTemplate] * 30, interval=0.7),
    ])
    path4 = Path(4, [
        Ants([AntTemplate] * 20, interval=0.8),
        Wait(7),
        Ants([AntTemplate] * 30, interval=0.7),
    ])


class Wave11(WaveTemplate):
    path0 = Path(0, [
        Ants([AntTemplate] * 20, interval=0.7),
        Wait(3),
        Ants([AntTemplate] * 30, interval=0.65),
    ])
    path1 = Path(1, [
        Ants([AntTemplate] * 20, interval=0.7),
        Wait(5),
        Ants([AntTemplate] * 30, interval=0.65),
    ])
    path2 = Path(2, [
        Ants([AntTemplate] * 20, interval=0.7),
        Wait(7),
        Ants([AntTemplate] * 30, interval=0.65),
    ])
    path3 = Path(3, [
        Ants([AntTemplate] * 20, interval=0.7),
        Wait(7),
        Ants([AntTemplate] * 30, interval=0.65),
    ])
    path4 = Path(4, [
        Ants([AntTemplate] * 20, interval=0.7),
        Wait(7),
        Ants([AntTemplate] * 30, interval=0.65),
    ])
    path5 = Path(5, [
        Ants([AntTemplate] * 20, interval=0.7),
        Wait(5),
        Ants([AntTemplate] * 30, interval=0.65),
    ])


class Wave12(WaveTemplate):
    path0 = Path(0, [
        Ants([AntTemplate] * 20, interval=0.65),
        Wait(3),
        Ants([AntTemplate] * 30, interval=0.63),
        Wait(7),
        Ants([AntTemplate] * 30, interval=0.61),
    ])
    path1 = Path(1, [
        Ants([AntTemplate] * 20, interval=0.65),
        Wait(5),
        Ants([AntTemplate] * 30, interval=0.61),
        Wait(5),
        Ants([AntTemplate] * 30, interval=0.61),
    ])
    path2 = Path(2, [
        Ants([AntTemplate] * 20, interval=0.65),
        Wait(7),
        Ants([AntTemplate] * 30, interval=0.63),
        Wait(3),
        Ants([AntTemplate] * 30, interval=0.61),
    ])
    path3 = Path(3, [
        Ants([AntTemplate] * 20, interval=0.65),
        Wait(7),
        Ants([AntTemplate] * 30, interval=0.61),
        Wait(3),
        Ants([AntTemplate] * 30, interval=0.61),
    ])
    path4 = Path(4, [
        Ants([AntTemplate] * 20, interval=0.65),
        Wait(7),
        Ants([AntTemplate] * 30, interval=0.63),
        Wait(3),
        Ants([AntTemplate] * 30, interval=0.61),
    ])
    path5 = Path(5, [
        Ants([AntTemplate] * 20, interval=0.65),
        Wait(5),
        Ants([AntTemplate] * 30, interval=0.61),
        Wait(5),
        Ants([AntTemplate] * 30, interval=0.61),
    ])


class Wave(object):
    def __init__(self, wave_num, scheduler, tweener, update_entities, level, space):
        self.wave_num = wave_num
        self.scheduler = scheduler
        self.tweener = tweener
        self.update_entities = update_entities
        self.level = level

        Path.scheduler = self.scheduler
        Path.space = space
        Path.ant_paths = self.level.ant_paths
        Path.update_entities = self.update_entities
        self.wave_sequence = [
            Wave0(),
            Wave1(),
            Wave2(),
            Wave3(),
            Wave4(),
            Wave5(),
            Wave6(),
            Wave7(),
            Wave8(),
            Wave9(),
            Wave10(),
            Wave11(),
            Wave12(),
        ]
        self.wave_data = None

        # NOTE: parent is responsible for calling timer.scheduler.update()
        self.timer = Timer(settings.WAVE_TIMER_INTERVAL, scheduler=self.scheduler, repeat=True)
        self.timer.event_elapsed += self._on_wave_tick

        self.current_state = _StateBase
        self.start_state = _StatePrepare

        event_dispatcher.add_listener(EVT_QUIT, self._on_quit)

    def set_state(self, new_state):
        if self.current_state:
            self.current_state.exit(self)
        new_state.enter(self)
        self.current_state = new_state

    def _on_wave_tick(self, timer):
        logger.debug('WAVE TICK: {}', self.current_state)
        state = self.current_state.think(self, timer.interval)
        if state is not None:
            self.set_state(state)

    def _on_wave_ended(self):
        logger.debug('WAVE ENDED')

    def _on_game_ended(self):
        logger.debug('GAME ENDED')
        self.timer.stop()
        self.running = False

    def _on_quit(self, *args):
        self.timer.stop()

    def _start_wave(self):
        logger.debug('>> >> wave {}: _start_wave', self.wave_num)
        self.wave_data = self.wave_sequence[self.wave_num - 1]
        for path in self.wave_data.all_paths:
            path.next_action()

    def wave_is_running(self):
        return any([
            self.wave_data.path0.is_running,
            self.wave_data.path1.is_running,
            self.wave_data.path2.is_running,
            self.wave_data.path3.is_running,
            self.wave_data.path4.is_running,
            self.wave_data.path5.is_running,
        ]) or any([e for e in self.update_entities if e.kind == KIND_ANT])

    def is_wave_won(self):
        # TODO
        return self.current_state is _StateWonWave

    def is_wave_lost(self):
        # TODO
        return False

    def is_wave_quit(self):
        # TODO
        return False

    def is_game_won(self):
        # TODO
        return self.current_state is _StateWonGame and self.wave_num == settings.last_wave_num
