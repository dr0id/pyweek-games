# -*- coding: utf-8 -*-
import pygame

from gamelib import settings
from gamelib.settings import MAX_DT, STEP_DT
from pyknic import timing
from pyknic.context import Context

import logging

logger = logging.getLogger(__name__)
logger.debug("importing ...")
class TimeSteppedContext(Context):

    def __init__(self):
        Context.__init__(self)
        self._clock = pygame.time.Clock()

        self._lockStepper = timing.LockStepper(max_dt=MAX_DT, timestep=STEP_DT, max_steps=100000) #fixme: investigate spiral of death if max steps causes it
        self._lockStepper.event_integrate += self.update_step

        self._cap = timing.FrameCap(settings.max_fps)
        self._cap.event_update += self._draw

        self._game_time = timing.GameTime()
        self._game_time.event_update += self._lockStepper.update
        self._game_time.event_update += self._cap.update

        self._screen = None

    def enter(self):
        self._screen = pygame.display.get_surface()

    def update(self, ignore1, ignore2):
        """
        The update method called from the context stack, don't override it.
        User the update_step and draw_capped methods.
        :param ignore1:
        :param ignore2:
        :return:
        """
        dt = self._clock.tick()
        dt /= 1000.0   # convert to seconds
        if dt > MAX_DT:
            logger.warning("limiting dt {0} to max dt {1}", dt, MAX_DT)
            dt = MAX_DT
        self._game_time.update(dt)

    def draw(self, screen, do_flip=True, interpolation_factor=1.0):
        raise NotImplementedError()

    def update_step(self, delta, sim_time, *args):
        # raise NotImplementedError()
        pass

    def _draw(self, dt, sim_t, *args):
        self.draw(self._screen, True, self._lockStepper.alpha)

logger.debug("imported.")
