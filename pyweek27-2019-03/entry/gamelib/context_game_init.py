# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'context_game_init.py' is part of codogupywk26
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This is the music initialization and fadeout context.

"""
from __future__ import print_function, division

import logging
import os

import pygame

from gamelib import settings, resource_sound
from pyknic import context

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2018"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["GameInitContext"]  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class GameInitContext(context.Context):

    def __init__(self, music_player):
        context.Context.__init__(self)
        self.music_player = music_player
        settings.music_player = music_player

    def enter(self):
        self.music_player.fill_music_carousel(resource_sound.songs)

        try:
            icon = pygame.image.load(settings.path_to_icon)
            pygame.display.set_icon(icon)
        except pygame.error as pg_e:
            logger.error("could not find icon.png in %s: error: %s", os.path.abspath(settings.path_to_icon), str(pg_e))
        except Exception as ex:
            logger.error(ex)

        try:
            pygame.display.set_caption(settings.title)
        except Exception as ex:
            logger.error(ex)

        # settings.screen.fill(settings.default_fill_color)
        # pygame.display.flip()

        self.music_player.start_music_carousel()

    def exit(self):
        self.music_player.fadeout(2000)

    def update(self, delta_time, sim_time):
        self.pop()


logger.debug("imported")
