# -*- coding: utf-8 -*-
"""
This it the main module. This is the main entry point of your code (put in the main() method).
"""
from __future__ import division, print_function

import logging

import pygame

from gamelib import settings  # , context_game
from gamelib.client import Client
# from gamelib.context_intro import Intro
from gamelib.context_game_init import GameInitContext
from gamelib.context_init_pygame import PygameInitContext
from gamelib.entitytypes import ChipsFactory0
from gamelib.game_context import GameContext, StartInfo
from gamelib.settings import cell_size
from gamelib.sfx import MusicPlayer
from pyknic import context
from pyknic.mathematics import Point3
from pyknic.mathematics.geometry import SphereBinSpace2

logger = logging.getLogger(__name__)


def main():
    # put here your code
    import pyknic

    logging.getLogger().setLevel(settings.log_level)
    pyknic.logs.print_logging_hierarchy()

    context.push(PygameInitContext(_custom_display_init))
    music_player = MusicPlayer(settings.master_volume, settings.music_ended_pygame_event)
    context.push(GameInitContext(music_player))

    # todo: implement intro and such?
    space = SphereBinSpace2(cell_size)
    client = Client(music_player, space)
    info = StartInfo()
    info.new_buildings = [(ChipsFactory0, Point3(100, 100))]
    game = GameContext(client, space, info)
    context.push(game)

    while context.length():
        top = context.top()
        if top:
            top.update(0, 0)

    logger.debug('Finished. Exiting.')


def _custom_display_init(display_info):
    accommodated_height = int(display_info.current_h * 0.9)
    if accommodated_height < settings.screen_height:
        logger.info("accommodating height to {0}", accommodated_height)
        settings.screen_height = accommodated_height  # accommodate for title bar and task bar ?
    settings.screen_size = settings.screen_width, settings.screen_height
    logger.info("using screen size: {0}", settings.screen_size)

    # initialize the screen
    settings.screen = pygame.display.set_mode(settings.screen_size, settings.screen_flags)

    # mixer values to return
    frequency = settings.MIXER_FREQUENCY
    channels = settings.MIXER_CHANNELS
    mixer_size = settings.MIXER_SIZE
    buffer_size = settings.MIXER_BUFFER_SIZE
    # pygame.mixer.pre_init(frequency, mixer_size, channels, buffer_size)
    return frequency, mixer_size, channels, buffer_size


# this is needed in order to work with py2exe
if __name__ == '__main__':
    main()
