# -*- coding: utf-8 -*-
import logging
import os

import pygame

from pyknic import context

logger = logging.getLogger(__name__)


class PygameInitContext(context.Context):

    def __init__(self, display_init_callback):
        context.Context.__init__(self)
        self._display_init_cb = display_init_callback

    def enter(self):
        self.init_pygame()

    def exit(self):
        pygame.quit()

    def update(self, delta_time, sim_time):
        self.pop()

    def init_pygame(self):
        logger.info("%s pygame init %s", "#" * 10, "#" * 10)
        self._pre_init()
        imported_modules = self._import_modules()
        delayed_init = self._initialize_modules(imported_modules)
        display_info, driver_info, wm_info = self._get_display_info()

        # application specific
        frequency, mixer_size, channels, buffer_size = self._display_init_cb(display_info)
        # end application specific

        # check if a surface can be retrieved
        pygame.display.get_surface()

        self._initialize_delayed_modules(delayed_init, imported_modules, frequency, mixer_size, channels, buffer_size)
        logger.info("%s pygame init DONE %s", "#" * 10, "#" * 10)

    def _initialize_delayed_modules(self, delayed_init, imported_modules, frequency, mixer_size, channels, buffer_size):
        msg = "----- setting pre mixer pre init values: " \
              "frequency: %s, mixer_size: %s, channels: %s, buffer_size: %s -----"
        logger.info(msg, frequency, mixer_size, channels, buffer_size)
        pygame.mixer.pre_init(frequency, mixer_size, channels, buffer_size)

        logger.info("----- init delayed modules (after display set_mode) -----")
        for _name, _module in imported_modules:
            if any(s in _name for s in delayed_init):
                self._init_module(_module)

    @staticmethod
    def _get_display_info():
        logger.info("----- display info -----")
        display_info = pygame.display.Info()
        logger.info("display info: %s", str(display_info))
        driver_info = pygame.display.get_driver()
        logger.info("display driver: %s", driver_info)
        wm_info = pygame.display.get_wm_info()
        logger.info("display window manager info: %s", str(wm_info))
        return display_info, driver_info, wm_info

    def _initialize_modules(self, imported_modules):
        logger.info("----- initialize modules -----")
        delayed_init = ["mixer", "scrap"]
        for _name, _module in imported_modules:
            if not any(s in _name for s in delayed_init):
                self._init_module(_module)
            else:
                logger.debug("delay init for module %s", _name)
        return delayed_init

    @staticmethod
    def _import_modules():
        logger.info("----- import modules -----")
        # num_pass, num_fail = pygame.init()
        # logger.info("pygame.init(): pass: %s  failed: %s", num_pass, num_fail)
        # if num_fail != 0:
        #     logger.error("pygame failed init pass:%s failed:%s", num_pass, num_fail)
        #
        # if num_pass <= 0:
        #     logger.error("pygame didn't initialize anything!")
        import pkgutil
        import importlib
        imported_modules = []
        skip_modules = ["examples", "tests", "docs", "threads"]
        try:
            modules = pkgutil.walk_packages(pygame.__path__, "pygame.")
            for module_info in modules:
                _finder, name, is_pkg = module_info
                try:

                    if any(s in name for s in skip_modules):
                        logger.info("skipping import of excluded package/module '%s'", name)
                        continue  # skip

                    if is_pkg:
                        logger.info("skipping package: %s", name)
                    else:
                        logger.info("importing module: %s", name)
                        _module = importlib.import_module(name)
                        imported_modules.append((name, _module))

                except ImportError as ie:
                    logger.warning("import error while importing '%s': %s", name, ie)
                except AttributeError as ae:
                    logger.error("attribute error while importing '%s': %s", name, ae)
                except Exception as ex:
                    logger.error("exception while importing '%s': %s", name, ex)
        except Exception as fatal_ex:
            logger.fatal("fatal pygame import error: %s", fatal_ex)
            raise fatal_ex
        finally:
            del pkgutil
            del importlib
        return imported_modules

    @staticmethod
    def _pre_init():
        logger.info("----- pre init stuff -----")
        os.environ['SDL_VIDEO_CENTERED'] = '1'

    @staticmethod
    def _init_module(m):
        if hasattr(m, 'init'):
            try:
                logger.info("initializing module %s", m)
                logger.info("  init() %s", m)
                m.init()
                if m.get_init():
                    logger.info("    get_init() True %s", m)
                else:
                    logger.error("    get_init() False %s", m)
            except AttributeError as ae:
                # module has not get_init() method
                logger.warning("    missing get_init() in module %s: %s", m, ae)
            except Exception as ex:
                logger.error("    init() raised an exceptions: %s", ex)
        else:
            logger.info("module has no init(): %s", m)


if __name__ == '__main__':
    def _standalone_display_init():
        pygame.display.set_mode((800, 600))
        MIXER_FREQUENCY = 0  # default:22050
        MIXER_SIZE = -16  # default: -16
        MIXER_CHANNELS = 2  # default: 2 stereo or 1 mono
        MIXER_BUFFER_SIZE = 128  # default: 4096
        return MIXER_FREQUENCY, MIXER_SIZE, MIXER_CHANNELS, MIXER_BUFFER_SIZE


    PygameInitContext(_standalone_display_init).init_pygame()
