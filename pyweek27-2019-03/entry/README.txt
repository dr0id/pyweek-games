﻿cANT3 x2 Onslaught: The Invasion from Six Chips Deep - Pyweek 27, March 2019
============================================================================

CONTACT: IRC DR0ID_, Gummbum @ freenode.net #pyweek #pygame

Homepage: https://bitbucket.org/dr0id/codogupywk27/src/default/
Name: cANT3 x2 Onslaught: The Invasion from Six Chips Deep
Team: CoDoGuPywk27
Members: DR0ID, gummbum


DEPENDENCIES:

You might need to install some of these before running the game:

  Python:     http://www.python.org/
  PyGame:     http://www.pygame.org/

Tested with Python 2.7 and 3.6.


DESCRIPTION:
============

The first generation following in the footsteps of that great hero finds new
hope, even as it finds itself faced with a bigger menace. The first threat to
the people having been removed, they can focus on rebuilding. A new technology
breathes life into the remnant of the colony: Chipworks. Never before have
potato chips had such a prominent role in...well, everything. Being mostly
comprised of air, they are lightweight, and sturdiness of carbon 6, and the
high energy content of lard.

Business was instantly booming. All hands working together, the first Chipworks
factory outpost was built within a year.

Then, out of the hills, returned the menace. In droves, and from all six
directions - north, south, east, west, this-a-way, and that-a-way.

The more Chipworks production increased, the higher the chip hoards piled, and
the more relentless the ant hordes.

This is the story of one such outpost.

------------

cANT3 is a combination of Tower Defense, Resource Management, and Town
Building.

The ants just keep coming, so build your defenses and manage your resources
wisely.


RUNNING THE GAME:
=================

On Windows or Mac OS X, locate the "run.pyw" file and double-click it.

Otherwise open a terminal / console and "cd" to the game directory and run:

  python run.py

If the game crashes or there's a bug, see the TROUBLE section later in this
README.


HOW TO PLAY THE GAME:
=====================

When the game starts, the action starts. Pay attention to the messages! The
messages are your instructions.

Controls:
    MLB: select a tool; place a tower
    MRB: deselect the current tool; upgrade a tower
    M Hover: view the level of a tower

GUI:
    - The toolbar at the bottom contains a selection of towers to build. Some
       tools need to be unlocked.
    - Left-click an unlocked tool activates it on the mouse cursor.
    - After selecting a tower, left-click in a buildable area to place it.
    - The Status HUD at bottom center shows the current wave number, the
      energy and chips level.

Chips are needed to create and operate towers. Energy is also needed to operate
towers. DO NOT let your chip pool drop dangerously low; a spiral of death shall
ensue. DO NOT let your energy pool drop too low; some or all of your towers
will not have enough energy to cycle, and you'll be a sittin' chip.

You start the game with one Chipworks factory, a trickle of chips and energy,
and three unlocked towers: Normal Gun Tower, Power Station, and Upgrade Center.

If you're the experimental, explorer type you can go play now. Just pay
attention to the tutorial in Wave 1. You may need to replay it 2-3 times to get
off to a solid start, so don't be afraid to scrap it and start over early.

The game is fairly short. 13 waves of increasing mayhem, about 10-15 minutes of
gameplay.

For those who want more game detail before setting out, the toolbar and its
features are (from left to right):

Normal Gun Tower: One measley shot at a steady pace. A reliable workhorse.
Power Station: Produces Energy. Supplies shared power in real time.
Upgrade Center: Needed to upgrade your stuff with jerry-rigs and refurbs.
Sniper Tower: One shot, one kill. Wait for it...Boom. A nice long reach.
Machine Gun Tower: Peck away in really fast bursts. It's buggy chip tech.
Fan Gun Tower: Like a shotgun with smart pellets. Actually, it is just so.
Science Center: Needed to unlock certain esoteric alchemical mysteries.
Battery Station: Energy store to stop brownouts and that annoying alarm.
Chipworks Factory: More CHIIIIPS. Ya boy.

When the above towers are placed in the field, they can be upgraded by right-
clicking them, provided you have enough chips for the upgrade. The facility
makes a lot of chips, but it uses a lot also. Also the boys in the Upgrade
Center are apt to pinch a chip, and...nobody can eat just one.

Time to go win or lose!

Quit the game at any time by clicking the window's Close (X) button.

Don't be a loser or a quitter.


TROUBLE:
========

If the game crashes and you wish to assist us with a bug report, you can enable
debug level logging by editing gamelib/settings.py, change:

  # change this setting to DEBUG
  log_level = [logging.WARNING, logging.DEBUG][1]

Rerun the program until the error occurs, then contact DR0ID_ or Gummbum to
provide the run_debug.py.log file. Come to IRC for help. See our contact info
at the top of this README. We'll be happy to help.


LICENSE:
========

This game has the same license as pyknic. Some of the content is reused under
fair use principle and is subject to its own restrictions.


CHEATING AND TWEAKING:
======================

There are no easy cheats to let you skip levels or jump to the win condition.
Sorry, it just wasn't useful to us this time. But there are some other
potentially interesting items...

1. F1 displays a few run-time internals and some debugging graphics.

2. Sound volume (settings.py).

   master_volume adjusts the level of all sound (0.0 to 1.0).
   music_volume adjusts the level of all music (0.0 to 1.0).
   sfx_volume adjusts the level of all sound FX (0.0 to 1.0).

   The resulting volume is calculated: master_volume * [music|sfx]_volume.

   If you want to tweak music or sfx volume individually, manually hack
   gamelib/resource_sound.py. It should be pretty easy to figure out.

3. Window size (settings.py).

   screen_width and screen_height adjust the window size. Custom sizes have not
   been play-tested, but they appear to work. You may prefer 800x600, 640x480,
   or a square shape for a performance gain.

4. Hacking your own waves is loosely documented in gamelib/settings.py. See the
   comments accompanying the last_wave_num setting.


5. There is a bevy of things to tune in gamelib/entitypes.py, and
   gamelib/ants.py. Much fun to be had. If we had more time we would have spent
   a lot more time in here. :)

If anybody makes mods they want to share, we'll be happy to mention them
somewhere. We might even consider merging them with the project.

Hack a wave bro.
