# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'registry.py' is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This module contains an registry for entities.
"""
from __future__ import print_function, division

import logging
import warnings

from pyknic import events

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2017"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 

logger = logging.getLogger(__name__)
logger.debug("importing...")


class Registry(object):
    """
    The Registry class. Its main purpose is to register entities and provide a way to retrieve them fast by either
    id or kind.

    It has methods to register and retrieve entities. One fast way to retrieve entities is by using the attributes
    'by_id' and 'by_kind'. These are dictionaries in the form of::

        by_id = {id: entity}
        by_kind = {kind: [entities]}

    This should be used as read only, otherwise the consistency of the registry is not guaranteed.

    Usage example (see unittest too)::

        reg = Registry()
        ...
        ent = reg.by_id[id]
        ...
        entities = reg.by_kind[kind]

    There are also two events that indicate registration and removal of entities.

    usage::

        def on_entity_registered(entity, id, kind, registry):
            pass

        def on_entity_removed(entity, id, registry):
            pass

        ...
        reg = Registry()
        ...
        reg.event_entity_registered.add(on_entity_registered)
        reg.event_entity_removed.add(on_entity_removed)

    """

    def __init__(self, logger_instance=None):
        """
        The constructor.
        :param logger_instance: The loggers instance to use. If None then the module logger instance is used. If set
            to False no logging at all and if set to another logging_instance the this one is used.
        """
        self.by_id = {}  # {id: entity}
        self.by_kind = {}  # {kind: [entity]}
        self._logger = logger if logger_instance is None else (None if logger_instance is False else logger_instance)

        _reg_event_name = "Registry<{0}>.event_entity_registered".format(id(self))
        self.event_entity_registered = events.WeakSignal(_reg_event_name, events.NEW_LAST)
        _reg_event_name = "Registry<{0}>.event_entity_removed".format(id(self))
        self.event_entity_removed = events.WeakSignal(_reg_event_name, events.NEW_LAST)

    def register_entity(self, entity):
        self.register(entity, entity.id, getattr(entity, "kind", None))

    def register(self, entity, entity_id, entity_kind=None):
        """
        Register an entity by id and kind.
        :param entity: the entity to store
        :param entity_id: the id to store the entity under
        :param entity_kind: the kind of the entity
        :raises: KeyError if the id is already present.
        :raises: ValueError for a kind < 1.
        :return: None
        """
        if self.by_id.get(entity_id, None) is None:
            if __debug__:
                for r_id, r_ent in self.by_id.items():
                    if entity == r_ent:
                        diff_id_msg = "Entity '{0}' with id '{1}' has already been registered with different id '{2}'!"
                        warnings.warn(diff_id_msg.format(entity, entity_id, r_id))
                for r_kind, r_entities in self.by_kind.items():
                    if entity in r_entities:
                        diff_kind_msg = "Entity '{0}' with kind '{1}' has already been registered with kind '{2}'"
                        warnings.warn(diff_kind_msg.format(entity, entity_kind, r_kind))

            if entity_kind is not None:
                if entity_kind < 1:
                    raise ValueError("entity kind should be >= 1")
                self._register_by_kind(entity, entity_kind)

            self.by_id[entity_id] = entity
            self.event_entity_registered.fire(entity, entity_id, entity_kind, self)
            if self._logger:
                _log_msg = "Entity registered in registry<%s>: id:%s kind:%s ent:%s"
                self._logger.info(_log_msg, id(self), entity_id, entity_kind, entity)
        else:
            raise KeyError("There is already an entity registered for id '{0}'!".format(entity_id))

    def _register_by_kind(self, entity, entity_kind):
        k = 1
        while k <= entity_kind:
            if k & entity_kind:
                self.by_kind.setdefault(k, set()).add(entity)
            k <<= 1

    def clear(self):
        """
        Clears all entities from the registry.
        :return: None
        """
        _entites = list(self.by_id.items())
        self.by_id.clear()
        self.by_kind.clear()
        for _id, _e in _entites:
            self.event_entity_removed.fire(_e, _id, self)
        if self._logger:
            self._logger.info("All entities cleared from registry '%s'", id(self))

    def remove_entity(self, entity):
        self.remove_by_id(entity.id)

    def remove_by_id(self, entity_id):
        """
        Remove an entity by its id. Logs a warning if no entity with that id is registered.
        :param entity_id: The entity id to remove.
        :return: True if the entity was removed, False otherwise.
        """
        ent = self.by_id.get(entity_id, None)
        if ent is None:
            if self._logger:
                self._logger.warning("Unregistered entity removed: id:%s ent:%s", entity_id, ent)
            return False

        for v in self.by_kind.values():
            v.discard(ent)

        del self.by_id[entity_id]
        self.event_entity_removed.fire(ent, entity_id, self)
        if self._logger:
            self._logger.info("Entity removed: id:%s ent:%s", entity_id, ent)
        return True

    def save_remove_by_id(self, entity_id):
        """
        Remove by id but don't raise an exception if the id is not found.
        :param entity_id: The entity id to remove.
        :return: True if the id was found and removed, False otherwise.
        """
        return self.remove_by_id(entity_id)

    def get_all(self):
        """
        Gets all registered entities.
        :return: List of all entities.
        """
        return set(self.by_id.values())

    def get_by_id(self, entity_id, not_found_value=None):
        """
        Gets an entity by id and returns it.
        :param entity_id: The id to look for.
        :param not_found_value: The value that should be returned if the id is not found. Default: None
        :return: The registered entity or the not_found_value
        """
        return self.by_id.get(entity_id, not_found_value)

    def get_by_kind(self, kind):
        """
        Gets all entities of the given kind. If bit field type of kind is used use :func:`get_by_bit_kind`
        :param kind: The kind to look for.
        :return: List of entities of given kind.
        """
        return self.by_kind.get(kind, set())

    def get_by_bit_kind(self, group):
        """
        Gets all entities that match the expression 'group & kind'. This works best it kind is a bitfield, e.g.
        kind1 = 1 << 0, kind2 = 1 << 1 etc.
        :param group: The grouped bitfield, e.g. group = kind1 | kind2 .
        :return: List of entities that match the expression: group & kind
        """
        return set([_ent for _key in self.by_kind.keys() for _ent in self.by_kind[_key] if _key & group])

logger.debug("imported")
