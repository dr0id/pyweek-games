#! /usr/bin/env python

## py2exe compatible run-me script

import sys
import os
import datetime


# run in right directory
if not sys.argv[0]:
    script_name = __file__
else:
    script_name = sys.argv[0]
appdir = os.path.abspath(os.path.dirname(script_name))
os.chdir(appdir)
# make sure that sub modules in gamelib are imported corectly
appdir = os.path.join(appdir, 'gamelib')
if not appdir in sys.path:
    sys.path.insert(0,appdir)

if __name__ == '__main__':
    try:
        std_out = sys.stdout
        std_err = sys.stderr
        mode = 'wb'
        # not sure if a timestamp is appreciated since it generates lots of 
        # files, simply replace 'now' to use it
        now = ''
        #now = '-' + datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
        name = 'out%s.txt' %(now)
        sys.stdout = open(name, mode)
        name = 'err%s.txt' %(now)
        sys.stderr = open(name, mode)

        import main
        main.main()

    finally:
        sys.stdout.close()
        sys.stdout = std_out
        sys.stderr.close()
        sys.stderr = std_err
