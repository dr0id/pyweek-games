"Where am I? Who am I?"
You hear a voice inside your head: My name is Validon. I am here to give you your tasks.
"Why can't I remember anything? Why do I feel empty?"
Your soul has been shattered into nine pieces by... the process.
You are but one-ninth of your being.
To proceed you must collect the other eight pieces of yourself.
"Collect the pieces of my soul? What does that even mean?"
They are hidden behind challenges to test your resolve. Complete the tasks before you, and you will be made whole again. You will remember who you are.
When you have done that, you will understand why you are here.

