Another shattered fragment of your soul descends upon you. With it, another memory emerges:
"I'm an investigative journalist, working on a story about conflict diamonds."
"No wait... that was just a movie I saw."
"I work as a landscaper. That sounds right."
