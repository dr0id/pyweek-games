Fractured Soul
===============

Entry in PyWeek #12  <http://www.pyweek.org/12/>
Team: Multiverse Factory (leave the "Team: bit")
Members:
Christopher Night (Cosmologicon) <cosmologicon@gmail.com>
Gummbum <stabbingfinger@gmail.com>
DR0ID
Paulo Silva <nitrofurano@gmail.com>
JDruid
kiddo
ldlework <dlacewell@gmail.com>


DEPENDENCIES:

You might need to install some of these before running the game:

  Python 2.6+: http://www.python.org/
  PyGame 1.9+: http://www.pygame.org/


RUNNING THE GAME:

On Windows or Mac OS X, locate the "run_game.pyw" file and double-click it.

Othewise open a terminal / console and "cd" to the game directory and run:

  python run_game.py


HOW TO PLAY THE GAME:

Use the arrow keys to move and jump and follow the in-game information.

Additional controls:

Space: special ability
F1: level scan
Left-click: level scan
Esc: pause/back
Backspace: return to start

LICENSE:

Game code is GNU GPL v3
Gummworld2 game engine is LGPL
spritesheet.py available here: http://www.pygame.org/wiki/Spritesheet

Visit our project home for more information:
http://code.google.com/p/multifac/

typeface Nvvzs was made with OFL license
