0.option:

gamelib
    +-context
        __init__.py
        transition.py
        +-effects
            __init__.py  # general code of effects
            pygame.py
            pyglet.py
            opengl.py
            pygameadroid.py

import context
myeff = context.effects.pygame.Slide(...)
            
1.option:

gamelib
    +-context
        __init__.py
        transition.py
        effectspygame.py
        effectspyglet.py
        effectswhatever.py
    +-foo
        __init__.py
        bar.py
        barpygame.py
        barpyglet.py
        barwhatever.py

con:
    - code for differend libs is mixed

import context
myeff = context.effectspygame.Slide(...)




4.option:

gamelib
    +-context
        __init__.py
        effects.py
    +-foo
        __init__.py
        bar.py
    +-contextpygame
        __init__.py
        effects.py
    +-foopygame
        __init__.py
        bar.py
    +-contextpyglet
        __init__.py
        effects.py
    +-foopyglet
        __init__.py
        bar.py
    +-contextwhatever
        __init__.py
        effects.py
    +-foowhatever
        __init__.py
        bar.py

# absolute import, if the packages/modules are installed on pythonpath or stie-packages
import context
import contexpygame
myeff = contextpygame.effects.Slide(...)

# relative import, but that does not work!
from gamelib import context
from gamelib import contextpygame

# in a game the layout woul be (does not work):

gamename
    +-data
    +-gamelib
        +-context
        +-contextpygame
        ....
    ...
    run_game.py


# my working proposal
gamename
    +-data
    +-gamelib
        +-contextpygame
            +-context
        ....
    ...
    run_game.py



This is what I mean.
$ ls -F
context/
contextpygame/
contextpyglet/
contextwhatever/
run_game.py










==================================================================
DISCOURAGED, UGLY, UNHEALTHY, CONFUSING
==================================================================



2.option:

gamelib
    +-context
        __init__.py
        transition.py
    +-contextpygame
        __init__.py
        effects.py
    +-contextpyglet
        __init__.py
        effects.py
    +-contextwhatever
        __init__.py
        effects.py
    +-foo
        __init__.py
        +-bar
    +-foopygame
        __init__.py
        bar.py
    +-foopyglet
        __init__.py
        bar.py
    +-foowhatever
        __init__.py
        bar.py

import context
import contextpygame
myeff = contextpygame.effects.Slide(...)


3.option:

gamelib
    +-context
        __init__.py
        effects.py
    +-foo
        __init__.py
        bar.py
    +-utilspygame
        __init__.py
        +-context
            __init__.py
            effects.py
        +-foo
            __init__.py
            bar.py
    +-utilspyglet
        __init__.py
        +-context
            __init__.py
            effects.py
        +-foo
            __init__.py
            bar.py
    +-utilswhatever
        __init__.py
        +-context
            __init__.py
            effects.py
        +-foo
            __init__.py
            bar.py


con:
    - duplicating dir structure

pro:
    - code for different libs is completely separated


import context
import utilspygame
myeff = utilspygame.context.effects.Slide(...)

    
