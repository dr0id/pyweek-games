import csv, os
import settings

from gummworld2 import data

OFFICIAL_FIELDNAMES = 'World Number','Level Number','Level Name','TMX File'

class LevelInfo(object):
    
    def __init__(self, csv_file):
        """loads a data set from file name csv_file"""
        self.load(csv_file)
    
    def load(self, csv_file):
        """refresh data set from file"""
        self.csv_file = csv_file
        self.data = load(csv_file)
        self._names = self.data[0]
        self._info_by_name = self.data[1]
        self._info_by_number = self.data[2]
    
    def save(self, alternate_file=None):
        """write the data set to text file in CSV format to the same or
        alternate file. If alternate_file is specified, the default file name
        for this data set will be this new file."""
        rows = self.sorted()
        if alternate_file is not None:
            self.csv_file = alternate_file
        save(self.csv_file, [r for r in rows if r[0]>=0], append=False)
    
    def add(self, row):
        """add a row of data to the db
        
        Data should be a list containing the following info:
            [world_num,level_num,name,tmx_file]
        
        Typically you want these to be strings or numbers. If you intend to save
        them to a file, the save routine will convert them to strings. The load
        routine converts them back.
        """
        w,l,n,t = row
        if (w,l) in self._info_by_number:
            return
        self._names.append(n)
        self._info_by_name[n] = tuple(row)
        self._info_by_number[w,l] = tuple(row)
    
    def sorted(self):
        """get a list of rows sorted by number"""
        def cmp_rows(left, right):
            return cmp(left[0:2], right[0:2])
        rows = self._info_by_name.values()
        rows.sort(cmp_rows)
        return rows
    
    def high(self):
        """return the highest number in the loaded data set (e.g. the highest
        completed level in the save file). None will be returned if there is no
        data.
        """
        rows = self.sorted()
        try:
            row = rows[-1]
            w,l = row[0:2]
        except:
            return None
        return w,l
    
    def next(self, *args):
        """return the next number in the progression. Valid arguments are a
        sequence of two integers, or two integer values. None will be returned
        if there is no more data."""
        if len(args) == 2:
            world_num, level_num = args
        else:
            world_num, level_num = args[0]

        return 0, world_num  # Return to my lobby


        rows = self.sorted()
        nums = [(w,l) for w,l,n,t in rows]
        try:
            idx = nums.index((world_num,level_num))
            idx += 1
            w,l = nums[idx]
        except:
            return None
        return w,l
    
    def info_by_name(self, name):
        """return an info row for the specified name.  None will be returned if
        there is no data for name."""
        try:
            return self._info_by_name[name]
        except:
            return None
    
    def info_by_number(self, *args):
        """return an info row for the specified world and level None will be
        returned if there is no data for numbers. Valid arguments are a
        sequence of two integers, or two integer values. None will be returned
        if there is no data matching the arguments."""
        if len(args) == 2:
            world_num, level_num = args
        else:
            world_num, level_num = args[0]
        try:
            return self._info_by_number[world_num,level_num]
        except:
            return None

    def cutscene_on_win(self, (world, lev)):
        """What cutscene, if any, should show up if we beat this level?"""
        if self.info_by_number(world, lev):  # Already beat this level before
            return None
        if world == 1:  # All Act 1 levels end in a cutscene
            return "1-%s.txt" % lev
        if world == 2:  # The first 6 levels of Act 2 to be beaten end in a cutscene
            beaten = sum((1 if self.info_by_number(2, j) else 0) for j in range(1,13))  # Number of world 2 levels beaten
            return "2-%s.txt" % (beaten + 1) if beaten < 6 else None
        if world == 3:
            beaten3 = sum((1 if self.info_by_number(3, j) else 0) for j in range(1,7))  # Number of world 3 levels beaten
            return "3-%s.txt" % (beaten3 + 1) if beaten3 < 3 else None


    def level_status(self, (world, lev)):
        """Returns 0 for locked, 1 for unlocked, 2 for beaten"""
        beaten = sum((1 if self.info_by_number(2, j) else 0) for j in range(1,13))  # Number of world 2 levels beaten
        beaten3 = sum((1 if self.info_by_number(3, j) else 0) for j in range(1,7))  # Number of world 3 levels beaten
        if world and (settings.open_all_levels or self.info_by_number(world, lev)):
            return 2 if world > 0 else 1  # "special" zones should not be beaten
        if world < 0: return 1
        if (world,lev) == (1,1): return 1
        if world == 1 and lev > 1 and self.info_by_number(world, lev-1): return 1
        if world == 2:
            if lev < 5: return 1   # First 4 levels always open
            if lev < 9 and beaten >= 2: return 1  # Next tier open after you've beaten 2
            if beaten >= 4: return 1  # Last tier open after you've beaten 4

        if world == 3:
            return 1 if beaten >= 6 else 0  # World 3 open when you've beaten 6 levels in world 2

        if world == 0 and lev == 0: return 1
        if world == 0 and lev == 1: return 2 if self.info_by_number(1,8) else 1
        if world == 0 and lev == 2: return 2 if beaten >= 6 else (1 if self.info_by_number(1,8) else 0)
        if world == 0 and lev == 3: return 2 if beaten3 >= 3 else (1 if beaten >= 6 else 0)

        return 0

    def percent_complete(self):
        p = sum((2 if self.info_by_number(1, j) else 0) for j in range(1,9))  # 2 percent for Act 1 levels
        p += sum((4 if self.info_by_number(2, j) else 0) for j in range(1,13)) # 4 percent for Act 2 levels
        p += sum((6 if self.info_by_number(3, j) else 0) for j in range(1,7))  # 6 percent for Act 3 levels
        return p
            
    
    @property
    def names(self):
        """return an array of level names"""
        return self._names
    
    def print_all(self):
        for row in self.sorted():
            print row
    

def load(csv_file):
    """ Return collections: names, info_by_name, info_by_number.
    
    If the load fails, empty structures will be returned.
    
    names is a list of level names.
    info_by_name is a dict of world info keyed by level name.
    info_by_number is a dict of world info keyed by (world_num,level_num).
    """
    names = []
    info_by_name = {}
    info_by_number = {}
    try:
        csvreader = csv.DictReader(open(csv_file, 'rb'))
    except:
        if __debug__: print '%s: %s: %s' % (__name__, 'open failed', csv_file)
        return [], {}, {}
    for row in csvreader:
        if row['World Number'] == '#':
            # menu doesn't care to read comments
            continue
        wnum = int(row['World Number'])
        lnum = int(row['Level Number'])
        name = row['Level Name']
        tmx = row['TMX File']
        names.append(name)
        info_by_name[name] = (wnum,lnum,name,tmx)
        info_by_number[wnum,lnum] = (wnum,lnum,name,tmx)
    if __debug__:
        world_num = -1
        level_num = 0
        for recordfile in os.listdir(data.filepath('record', "")):
            if not recordfile.endswith("pickle"):
                continue
            name = "PLAY BACK %s" % recordfile.partition(".")[0]
            names.append(name)
            info_by_name[name] = (world_num,level_num,name,recordfile)
            info_by_number[world_num,level_num] = (world_num,level_num,name,recordfile)
            level_num += 1
    return (names, info_by_name, info_by_number)

def save(csv_file, rows, append=True, fieldnames=OFFICIAL_FIELDNAMES):
    """Write sequences to a text file using CSV format.
    
    csv_file is the file name.
    
    rows is a sequence of sequences. The data must be strings or numbers.
    
    append=True will save rows onto the end of the existing file. Otherwise,
    the file will be truncated. Data should not contain a header row.
    
    fieldnames can be used to override the official fieldnames.
    """
    
    # If file doesn't exist or is zero size, force append=False in order to
    # write header row.
    if not os.access(csv_file, os.F_OK):
        # doesn't exist: truncate
        append = False
    else:
        statinfo = os.stat(csv_file)
        if statinfo.st_size == 0:
            # zero length: truncate
            append = False
    
    if append is True:
        mode = 'ab' # append
    else:
        mode = 'wb' # truncate
    
    fh = open(csv_file, mode)
    csvwriter = csv.writer(fh)
    
    if not append:
        csvwriter.writerow(fieldnames)
    for row in rows:
        csvwriter.writerow([str(col) for col in row])

if __name__ == '__main__':
    import settings
    if False:
        # write test
        r = [
            [1,1,'Sweating Pigs','sweatpigs.tmx'],
            [1,2,'Flying Pigs','flypigs.tmx'],
            [1,3,'Greasing Pigs','greasepigs.tmx'],
        ]
        save(settings.save_file, r)
        save(settings.save_file, [(1,4,'Routing Pigs','routpigs.tmx')])
    elif False:
        # class test
        db = LevelInfo(settings.save_file)
        for n in db.names:
            assert n == db.info_by_name(n)[2]
        info = db.info_by_number(1,1)
        lvid = info[0:2]
        assert lvid == (1,1)
        db.add([99,99,'Gumms Wild Ride','wildride.tmx'])
        lvnext = db.next(lvid)
        assert lvnext == (99,99)
        lvnext = db.next(*lvid)
        assert lvnext == (99,99)
        print db.info_by_number(*lvnext)
        print db.info_by_number(lvnext)
        print db.info_by_number(99,99)
        print db.info_by_name('Gumms Wild Ride')
        print db.info_by_number(db.high())
        db.save()
    elif True:
        db = LevelInfo(settings.level_info_file)
        db.print_all()
        print db.info_by_number(1,5)
        print db.next((1,5))
