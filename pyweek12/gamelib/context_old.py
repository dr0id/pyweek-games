import pygame
from pygame.locals import *
import settings

class Context(object):
    def __init__(self):
        pass
    def think(self, dt):
        """Called once per frame"""
        if any(e.type == KEYDOWN and e.key in settings.keys.quit for e in pygame.event.get()):
            pop()
    def suspend(self):
        """Called when another context is pushed on top of this one."""
        pass
    def resume(self):
        """Called when another context is popped off the top of this one."""
        pass
    def enter(self):
        """Called when this context is pushed onto the stack."""
        pass
    def exit(self):
        """Called when this context is popped off the stack."""
        pass
    def draw(self, screen):
        """Refresh the screen"""
        pass
        

cstack = []

def push(c, do_enter=True):
    if cstack:
        if __debug__: print "CONTEXT: suspending", cstack[-1].__class__.__name__
        cstack[-1].suspend()
    if __debug__: print "CONTEXT: pushing", c.__class__.__name__
    cstack.append(c)
    if do_enter:
        if __debug__: print "CONTEXT: enter", c.__class__.__name__
        c.enter()

def pop(n = 1):
    for j in range(n):
        if cstack:
            if __debug__: print "CONTEXT: pop/exit", cstack[-1].__class__.__name__
            c = cstack[-1]
            del cstack[-1]
            c.exit()
        if cstack:
            if __debug__: print "CONTEXT: resume", cstack[-1].__class__.__name__
            cstack[-1].resume()

def top():
    return cstack[-1] if cstack else None





