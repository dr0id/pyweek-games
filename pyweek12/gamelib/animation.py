# -*- coding: utf-8 -*-


import pygame
from gummworld2 import data
import spritesheet, settings


class ImageCache(object):
    def __init__(self):
        self.spritesheets = {}  # Spritesheet cache, auto-populated when requested
        self.frames = {}  # Cache of frames of animation
        self.images = {}  # Cache of still images TODO: implement

    def getspritesheet(self, filename, convert_alpha=True):
        key = filename, convert_alpha
        if key not in self.spritesheets:
            self.spritesheets[key] = spritesheet.SpriteSheet(data.filepath('image', filename), convert_alpha)
        return self.spritesheets[key]

    def getframe(self, filename, fnumber, image_count, size=None, mirror=False, convert_alpha=True):
        key = filename, convert_alpha, size, image_count, fnumber, mirror
        if key not in self.frames:
            sheet = self.getspritesheet(filename, convert_alpha)
            if size is None:
                x,y = sheet.sheet.get_size()
                size = x/image_count,y
            self.frames[key] = sheet.image_at((size[0]*fnumber, 0, size[0], size[1]))
            if mirror:
                self.frames[key] = pygame.transform.flip(self.frames[key], True, False)
        return self.frames[key]
    
    def getimage(self, filename):
        print filename
        key = filename
        return None

imagecache = ImageCache()

class Animation(object):

    def __init__(self, filename, nframes=None, frames=None, size=None, fps=8, loop=True, mirror=False):
        self.loop = loop  # TODO: loop?
        self.playing = True

        self.frame_duration = 1.0 / fps
        self._time = 0.0
        self.index = 0

        if frames is None and nframes is None:
            frames, nframes = (0,), 1
        elif frames is None:
            frames = range(nframes)
        elif nframes is None:
            nframes = len(frames)
        self.nframes = nframes
        self.framekeys = [(filename, frame, nframes, size, mirror) for frame in frames]
        self.frames = None

    def loadframes(self):
        self.frames = [imagecache.getframe(*key) for key in self.framekeys]

    def play(self):
        self.playing = True

    def pause(self):
        self.playing = False

    def image(self):
        if self.frames: return self.frames[self.index % self.nframes]
        key = self.framekeys[self.index % self.nframes]
        return imagecache.getframe(*key)

    def think(self, dt):
        if not self.playing: return
        if not self.frames and settings.cacheframes: self.loadframes()
        self._time += dt
        while self._time > 0:
            self._time -= self.frame_duration
            self.index += 1

    def draw(self, surface, position):
        surface.blit(self.image(), position)

# __init__(self, filename, nframes=None, frames=None, size=None, fps=8, loop=True, mirror=False):
playerstandright = Animation("cartoon-human.png", frames=(8,), size=(64,96))
playerstandleft = Animation("cartoon-human.png", frames=(8,), size=(64,96), mirror=True)
playerjumpright = Animation("cartoon-human.png", frames=(9,), size=(64,96))
playerjumpleft = Animation("cartoon-human.png", frames=(9,), size=(64,96), mirror=True)
playerwalkright = Animation("cartoon-human.png", 6, fps=10, size=(64,96))
playerwalkleft = Animation("cartoon-human.png", 6, fps=10, size=(64,96), mirror=True)
playerfallright = Animation("cartoon-human.png", frames=(11,), fps=15, size=(64,96))
playerfallleft = Animation("cartoon-human.png", frames=(11,), fps=15, size=(64,96), mirror=True)
playerdieright = Animation("cartoon-human.png", frames=(13,), fps=15, size=(64,96))
playerdieleft = Animation("cartoon-human.png", frames=(13,), fps=15, size=(64,96), mirror=True)
userblock = Animation("cartoon-human.png", frames=(12,), size=(64,96))

if __name__ == '__main__':

    import spritesheet
    import pygame
    import pickle
    pygame.init()
    screen = pygame.display.set_mode((400,400))
    clock = pygame.time.Clock()
    anim = Animation(PLAYER_STANDING_ANIM_ID)
    f = open('anim.pkl', 'w')
    pickle.dump(anim, f)
    f.close()
    ff = open('anim.pkl', 'r')
    animation = pickle.load(ff)
    ff.close()
    
    while 1:
        dt = clock.tick(32) / 1000.0
        screen.fill((0,0,0))
        animation.think(dt)
        animation.draw(screen, (0,0))
        pygame.display.flip()
