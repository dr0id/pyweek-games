import sys
import os
import platform
import getpass
import keysyms
from pygame.locals import *
from gummworld2 import data

## Game appearance

game_title = "Fractured Soul"
resolution = 900, 700
fullscreen = False

if "-mini" in sys.argv:
    resolution = 600, 400

if "-f" in sys.argv:
    resolution = 640, 480
    fullscreen = True

show_hud = True

fade_duration = 0.75    # fade duration between menus, seconds
text_speed = .40        # cut scene reader speed, recommend .40 to .60

## Quick start

quickstart = False
quickstart_map = 'staircase.tmx'

## Sound

play_sound = True
play_music = False
music_loud = 0.7
music_soft = 0.45
sfx_volume = 1.0
master_volume = 1.0
walk_volume = 0.3       # Valid 0.0 through 1.0
jump_volume = 0.3       # Valid 0.0 through 1.0

## PVE aspects

align_on_walk = True    # Player moves to grid-aligned position after walking
align_on_jump = False   # Player moves to grid-aligned position after jumping
do_overshoot = True     # Compensate for the player jumping from the edge of the cliff
can_double_jump = False

player_enter_done = 0.5 # seconds the entering state lasts
player_speed = 220.0    #TODO tweak!
player_jump_start_speed = 640.
gravity = 1200.
terminal_velocity = 800.
support_edge = 16   # Number of pixels you have to be from the next grid square to land on a platform
collideafter = True

pit_margin = 40

player_lives = 8    # How many stones you can place per level

## Misc control

mouse_pan_threshold = 30    # pan when mouse is this far from screen center
mouse_pan_speed = 5         # speed multiplier, 5 is about right?
mouse_pan_color = Color('orange')
allow_camera_stealing = True

## Recording and playback

get_record = False       # Should I record a playback for playtesting?
playback_factor = 2.    # How many times real time should the playbacks go?
playback_max_fps = 5.

cacheframes = False  # Should animations cache their own frames outside the image cache? (May speed things up, will make recording impossible)

## Logging

printstatechanges = True  # Print a log message every time a FSM changes state
report_draw_tiles = True    # Hack to squelch toolkit.draw_tiles_of_layer
open_all_levels = False  # CHEAT!
beat_any_level_by_pushing_F8 = False  # VERY MUCH SO CHEAT!

## Game data

level_info_file = data.filepath('map','level_info.csv')
save_file = os.path.join(data.data_dir,'savegame.txt')

keys = keysyms.KeySyms([
    # name     [value1, ...]            description
    'left',    [K_LEFT, K_a],           'Move left',
    'right',   [K_RIGHT, K_d, K_e],     'Move right',
    'jump',    [K_UP, K_w, K_COMMA],    'Jump',
    'stone',   [K_SPACE],               'Leave statue',
    'scan',    [K_F1, K_LSHIFT],        'Scan level',
    'pause',   [K_ESCAPE, K_p],         'Pause',
    'resume',  [K_SPACE],               'Resume playing',
    'restart', [K_r],                   'Restart level',
    'quit',    [K_ESCAPE],              'Exit level',
    'up',      [K_UP, K_w, K_COMMA],    'Navigate menu',
    'down',    [K_DOWN, K_o, K_s],      'Navigate menu',
    'select',  [K_RETURN, K_SPACE],     'Select menu choice',
    'squish',  [K_BACKSPACE],           'Return to level entrance',
])

## Personal Settings ##

_hostname = platform.uname()[1]
_username = getpass.getuser()
_whoami = '%s@%s' % (_username,_hostname)

# Add your user@host case here if you want settings that won't affect others.
if _whoami == 'bw@bw-PC':
    os.environ['SDL_VIDEO_WINDOW_POS'] = '7,30'
    #os.environ['SDL_VIDEO_CENTERED'] = '1'
    quickstart = False
    quickstart_map = 'hop-skip.tmx'
    playback_factor = 1.
    report_draw_tiles = False
