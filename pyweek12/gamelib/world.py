import pygame
from pygame.locals import *

import gummworld2

class World(gummworld2.model.WorldQuadTree):
    """
        Inherits:
            collisions_dict(): items are {object:[other1,other2], ...}
            entities_in(rect): return list of entities that collide with rect
            add(*entities): add varags entities
            add_list(entities): add a list of entities
            remove(*entities): remove varags entities
            remove_list(entities): remove a list of entities
    """
    
    def __init__(self, rect):
        super(World, self).__init__(rect, worst_case=99)
    
    def collisions_other(self, other):
        return [right for left,right in self.collisions if left is entity]
