# -*- coding: utf-8 -*-

import context
from context import Context

"""
The Transitions.

"""


# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------

class Transition(Context):
    """
    A special context class that represents a transition between two contexts.
    It represents a transition from the topmost context on the stack to the
    new_context. The topmost context will be exchanges with the new_context at 
    the end.
    
    Transition B -> C
    
                +---+
                | T |
    +---+       +---+       +---+
    | B |       | B |       | C |
    +---+       +---+       +---+
    | A |  -->  | A |  -->  | A |
    +---+       +---+       +---+
    |   |  push |   |  pop  |   |
                                 
    
    T: Transition
    A-C: Context
    """
    def __init__(self, new_context, transition_effect, think_old=False, think_new=False):
        """
        Constructor.

        :Parameters:
            new_context : Context
                The new context to transition to.
        """
        # super(Transition, self).__init__() # TODO: why is self needed in __init__ ???
        Context.__init__(self) # TODO: why is self needed in __init__ ???
        self.new_context = new_context
        self.old_context = None
        self.effect = transition_effect
        self.think_old = think_old
        self.think_new = think_new

    def think(self, dt):
        """
        Called once per frame. The default implementation just updates the
        effect and checks if the effect desires to end the transition. If
        the transition should react on keypresses, this method may be 
        overriden, but keep in mind to call this base class method if 
        the default behavior still should work.
        
        :Parameters:
            dt : float
                time passed since last frame
        
        """
        if self.think_old:
            self.old_context.think(dt)
        if self.think_new:
            self.new_context.think(dt)
        if self.effect.think(dt):
            context.pop()
            return True
        return False

    def suspend(self):
        """Called when another context is pushed on top of this one."""
        self.old_context.suspend()
        if __debug__: print "CONTEXT Trans: suspended", self.old_context.__class__.__name__
        self.new_context.suspend()
        if __debug__: print "CONTEXT Trans: suspended", self.new_context.__class__.__name__
        self.effect.suspend()

    def resume(self):
        """Called when another context is popped off the top of this one."""
        self.old_context.resume()
        if __debug__: print "CONTEXT Trans: resumed", self.old_context.__class__.__name__
        self.new_context.resume()
        if __debug__: print "CONTEXT Trans: resumed", self.new_context.__class__.__name__
        self.effect.resume()

    def enter(self):
        """Called when this context is pushed onto the stack."""
        self.new_context.enter()
        if __debug__: print "CONTEXT Trans: entered", self.new_context.__class__.__name__
        self.old_context = context.top()
        self.effect.enter()

    def exit(self):
        self.effect.exit()

    def draw(self, screen):
        """
        Refresh the screen.
        
        :Parameters:
            screen : pygame.Surface
                the screen surface to draw on the final image.
        
        """
        self.effect.draw(screen, self.old_context, self.new_context)
        
    def _process_stack_push(self, context_stack, cont):
        cont.enter() # cont should be self
        if __debug__: print "CONTEXT Trans: entered", cont.__class__.__name__
        context_stack.append(cont)
        if __debug__: print "CONTEXT Trans: pushed", cont.__class__.__name__

    def _process_stack_pop(self, context_stack):
        self.exit()
        if __debug__: print "CONTEXT Trans: exited", self.__class__.__name__
        self.old_context.exit()
        if __debug__: print "CONTEXT Trans: exited", self.old_context.__class__.__name__
        # exchange
        cont = context_stack.pop(-1)
        context_stack.append(self.new_context)
        if __debug__: print "CONTEXT Trans: replaced", cont.__class__.__name__, "with", self.new_context.__class__.__name__
        

# -----------------------------------------------------------------------------
# class TransitionFollow(Transition):
    # """
    # Transition B -> C -> D
    
                # +---+
                # | T |
    # +---+       +---+       +---+       +---+
    # | B |       | B |       | TF|       | D |
    # +---+       +---+       +---+       +---+
    # | A |  -->  | A |  -->  | A |  -->  | A |
    # +---+       +---+       +---+       +---+
    # |   |  push |   |  pop  |   | think |   |
                                    # pop
    
    # T: Transition
    # TF: TransitionFollow
    # A-C: Context
    # """    
    
    # def think(self, dt):
        # super(TransitionFollow, self).think(dt)
        # if top() is self:
            # pop()

    # def exit(self):
        # """Called when this context is poped off the stack."""
        # exchange(self.new_context)

# -----------------------------------------------------------------------------
class PopTransition(Transition):
    """
    Transition B -> A
    
                +---+
                | T |
    +---+       +---+
    | B |       | B |
    +---+       +---+       +---+
    | A |  -->  | A |  -->  | A |
    +---+       +---+       +---+
    |   |  push |   |  pop  |   |
                                 
    T: Transition
    A-B: Context
    """
    
    def __init__(self, transition_effect, think_old=False, think_new=False):
        """
        Constructor.
        
        :Note: it needs at least two context instance on the stack to work.

        """
        Transition.__init__(self, context.top(1), transition_effect, think_old, think_new)

    def enter(self):
        """Called when this context is pushed onto the stack."""
        assert context.length() >= 2
        self.old_context = context.top()
        # TODO: make it handle None if there is only one context on the stack? -> use a dummy context
        self.new_context = context.top(1)
        self.effect.enter()

    def _process_stack_push(self, context_stack, cont):
        context_stack[-2].resume()
        if __debug__: print "CONTEXT PopTrans: resumed", context_stack[-2].__class__.__name__
        cont.enter()
        if __debug__: print "CONTEXT PopTrans: entered", cont.__class__.__name__
        context_stack.append(cont)
        if __debug__: print "CONTEXT PopTrans: pushed", cont.__class__.__name__

    def _process_stack_pop(self, context_stack):
        self.exit()
        if __debug__: print "CONTEXT PopTrans: exited", self.__class__.__name__
        cont = context_stack.pop(-1)
        if __debug__: print "CONTEXT PopTrans: poped", cont.__class__.__name__
        cont.exit()
        if __debug__: print "CONTEXT PopTrans: exited", cont.__class__.__name__

# -----------------------------------------------------------------------------
class PushTransition(Transition):
    """
    
    Transition B -> C
    
                +---+       +---+
                | T |       | C |
    +---+       +---+       +---+
    | B |       | B |       | B |
    +---+       +---+       +---+
    | A |  -->  | A |  -->  | A |
    +---+       +---+       +---+
    |   |       |   |       |   |
    
    T: Transition
    A-C: Context
    """
    
    def __init__(self, new_context, transition_effect, think_old=False, think_new=False):
        """
        Constructor.

        :Parameters:
            new_context : Context
                The new context to transition to.
        """
        Transition.__init__(self, new_context, transition_effect, think_old, think_new)
        self.old_context = None

    def enter(self):
        """Called when this context is pushed onto the stack."""
        self.new_context.enter()
        if __debug__: print "CONTEXT PushTrans: entered", self.new_context.__class__.__name__
        self.old_context = context.top()
        self.effect.enter()

    def _process_stack_pop(self, context_stack):
        self.exit()
        if __debug__: print "CONTEXT PushTrans: exited", self.__class__.__name__
        if self.old_context:
            self.old_context.suspend()
            if __debug__: print "CONTEXT PushTrans: suspended", self.old_context.__class__.__name__
        context_stack.append(self.new_context)
        if __debug__: print "CONTEXT PushTrans: pushed", self.new_context.__class__.__name__

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

class TransitionEffect(object):
    """
    Abstract effect class. Inherit from it to implement a graphical transition effect.
    """

    def enter(self):
        pass
        
    def exit(self):
        pass
        
    def suspend(self):
        pass
        
    def resume(self):
        pass

    def think(self, dt):
        """
        Any logic that needs to be updated for the effect.
        
        :Parameters:
            dt : float
                delta time passed since last frame
        
        :returns: True if it is done (finished, time is up or whatever), otherwise False.
        """
        raise NotImplementedError()
        
    def draw(self, screen, old_context, new_context):
        """
        Draws the effect on the screen surface.
        
        :Parameters:
            screen : pygame.Surface
                the screen surface, the finished drawing
            old_context : Context
                the old context, the one that where already here
            new_context : Context
                the new context, may eventually replace the old_context
            
        """
        raise NotImplementedError()

# -----------------------------------------------------------------------------
