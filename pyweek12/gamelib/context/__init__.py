# -*- coding: utf-8 -*-

"""
Context managment.

Limitations:
    pop can be called from anywhere, also from within a think method of an
    active (topmost) context. After poping it the active context is not the
    topmost context anymore, but can still modify the context stack (since
    it is still in the think method).


"""


# -----------------------------------------------------------------------------

class Context(object):
    """
    The context class.
    """
    def enter(self):
        """Called when this context is pushed onto the stack."""
        pass
    def exit(self):
        """Called when this context is popped off the stack."""
        pass
    def suspend(self):
        """Called when another context is pushed on top of this one."""
        pass
    def resume(self):
        """Called when another context is popped off the top of this one."""
        pass
    def think(self, dt):
        """Called once per frame"""
        pass
    def draw(self, screen):
        """Refresh the screen"""
        pass

    def _process_stack_push(self, context_stack, cont):
        if context_stack:
            context_stack[-1].suspend()
            if __debug__: print "CONTEXT: suspended", context_stack[-1].__class__.__name__
        cont.enter()
        if __debug__: print "CONTEXT: entered", cont.__class__.__name__
        context_stack.append(cont)
        if __debug__: print "CONTEXT: pushed", cont.__class__.__name__

    def _process_stack_pop(self, context_stack):
        self.exit()
        if __debug__: print "CONTEXT: exited", self.__class__.__name__
        if context_stack:
            context_stack[-1].resume()
            if __debug__: print "CONTEXT: resumed", context_stack[-1].__class__.__name__
        

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

_context_stack = []

def push(cont):
    """
    Pushes a new context on the stack. The enter method of the new context
    will be called when pushing it. If the stack is not empty, suspend will
    be called on the topmost context on the stack before pushing the new
    context on top of it.
    """
    cont._process_stack_push(_context_stack, cont)
    if __debug__: print "CONTEXT: pushed", cont.__class__.__name__


def pop(n = 1):
    """
    Pops a context from the stack. The exit method of the removed context will be
    called. If a there is another context on the stack, the resume method of it will
    be called.

    :Parameters:
        n : int
            defaults to 1, number of contextes to pop
    """
    for j in range(n):
        cont = None
        if _context_stack:
            cont = _context_stack.pop(-1)
            cont._process_stack_pop(_context_stack)
            if __debug__: print "CONTEXT: poped", cont.__class__.__name__

def top(n=0):
    """
    The active context.
    
    :Parameters:
        n : int
            the n'th item of the stack

    :returns:
        default: the topmost context of the stack or None, otherwise the n'th item or None
    """
    n += 1
    if n > len(_context_stack):
        return None
    return _context_stack[-1*n]

def length():
    """
    The number of items in the stack.
    
    :returns:
        number of items, integer >= 0
    """
    return len(_context_stack)

def print_stack():
    """
    Prints the current stack to the console. Should only be used for debugging purposes.
    """
    if __debug__:
        print "CONTEXT STACK:"
        for idx in range(0, length()):
            print idx, "    ", top(idx)
        print "    _________"