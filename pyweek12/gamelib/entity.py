# -*- coding: utf-8 -*-

import pygame, settings, animation
from pygame.locals import *

from gummworld2 import State, geometry, Vec2d

# Entities include pretty much everything that appears onscreen that could
#   interact with anything else in some way.
# Entities can be "attached" to other entities, meaning that their motion
#   is determined by the thing they're attached to.
# entity.parent = who I'm attached to (None or parent)
# entity.children = who's attached to me (list)

class EntityTree(object):
    def __init__(self):
        self.children = set()

    def think(self, dt):
        for child in list(self.children):
            child.think(dt)

    def handle_collisions(self):
        for child in list(self.children):
            child.handle_collisions()

    def absolute_position(self):
        return Vec2d(0,0)
        
    def supporting(self, rect):  # I can hold anything and everything
        return True

    def will_release(self):  # Always gonna give you up.
        """Is someone allowed to jump off me?"""
        return True

class Entity(geometry.RectGeometry):
    color = "#000000"
    weight = 0
    layer = 2.5

    def __init__(self, x, y, w, h, pos=None):
        geometry.RectGeometry.__init__(self, x, y, w, h, pos)
        self.parent = None  # Who I'm attached to: must be set before I can think
        self.children = set()  # Who all is attached to me
        self.moved = Vec2d(0, 0) # How much I've moved this last frame
        self.colliders = []
        self.sprites = []

    def set_tree(self, entity_tree):
        self.entity_tree = entity_tree  # Who I attach to when I detach
        self.parent = entity_tree  # Who I'm attached to
        self.parent.children.add(self)

    def set_colliders(self, colliders):
        """Pass in a list of colliders every frame"""
        self.colliders = colliders

    def think(self, dt, scootch = Vec2d(0,0)):
        for child in list(self.children):
            child.think(dt, scootch)

    def handle_collisions(self):
        if settings.collideafter:
            for child in list(self.children):
                child.handle_collisions()
    
    def constrain(self, rect, dp):
        """If this rect collided with me, how would I move it?"""
        return 0,0

    def absolute_position(self):
        return self.position - Vec2d(self.rect.width/2, self.rect.height/2)

    def relative_position(self):
        """Position relative either to parent or to the world, for grid alignment purposes.
        This returns the UPPER-LEFT CORNER, not the CENTER. Centers don't necessarily get aligned."""
        return self.absolute_position() - self.parent.absolute_position()

    def attach_to(self, parent = None):  # Can call with no argument to detach
        self.parent.children.remove(self)
        self.parent = parent if parent is not None else self.entity_tree
        self.parent.children.add(self)

    def draw(self, screen, world_to_screen):
        self.draw_image(screen, world_to_screen)
        if __debug__: self.draw_rect(screen, world_to_screen)
        
    def draw_image(self, screen, world_to_screen):
        pass

    def draw_rect(self, screen, world_to_screen):
        r = pygame.Rect(self.rect)
        r.topleft = world_to_screen(r.topleft)
        pygame.draw.rect(screen.surface, pygame.Color(str(self.color)), r, 1)

    def current_image(self):
        return animation.imagecache.getimage(self.imagename)

    def set_position(self, pos):
        self.position = pos
        self.rect.center = pos

    def load(self):  # How much weight is there pressing down on me?
        return sum(child.force() for child in self.children)

    def force(self):  # How much force am I pushing down with (my load + my weight)
        return self.weight + self.load()

    def will_release(self):
        return True

    def want_camera(self):  # I'm doing something interesting, look at me!
        return False

class MovingEntity(Entity):
    """An entity whose position changes over time, obv. Does not collide with other objects"""
    thinker = True

    def __init__(self, x, y, w, h, pos=None):
        Entity.__init__(self, x, y, w, h, pos)
        self.vx = 0
        self.vy = 0

    def think(self, dt, scootch = Vec2d(0,0)):
        """Should be called every frame"""
        self.moved = Vec2d(0, 0)  # For computing my motion this frame
        self.move(scootch)  # Move along with parent
        self.think_move(dt)
        self.position = self.rect.center # TODO: is this an error?
        if not settings.collideafter:
            self.handle_collisions()
        for child in set(self.children):
            child.think(dt, self.moved)  # Children must now be updated

    def think_move(self, dt):
        """The part of the thinking that controls motion. Should be overwritten in most subclasses"""
        self.move((self.vx * dt, self.vy * dt))

    def interp_position(self):
        """The current interpolated position based on time elapsed since last frame"""
        interp = State.camera.interp
        if interp > 1.0:
            return self.position
        x,y = self.moved * (1. - interp)  # TODO: should lastmove be change in rect.center rather than change in position?
        return self.position - (round(x), round(y))

    def interp_step(self):
        """The current interpolated step based on time elapsed since last frame"""
        interp = State.camera.interp
        if interp > 1.0:
            return self.position # TODO: is this return value correct??
        x,y = self.moved * (1. - interp)  # TODO: should lastmove be change in rect.center rather than change in position?
        return Vec2d(round(x), round(y))  
    
    def move(self, (dx, dy)):
        """Don't set position except through this function if you want smooth interpolation"""
        self.position += (dx,dy)
        self.moved += (dx, dy)
#        if self.sprites:
#            for s in self.sprites:
#                s.rect.center += Vec2d(round(dx),round(dy))

    def draw_image(self, screen, world_to_screen):
        phys_rect = Rect(self.rect)
        phys_rect.center = world_to_screen(self.interp_position())
        img = self.current_image()
        blit_rect = img.get_rect()
        blit_rect.bottomleft = phys_rect.bottomleft
        screen.blit(img, blit_rect)

    def draw_rect(self, screen, world_to_screen):
        r = Rect(self.rect)
        r.center = world_to_screen(self.interp_position())
        pygame.draw.rect(screen.surface, pygame.Color(str(self.color)), r, 1)


class AttachableEntity(MovingEntity):
    """Should be a FSM like player or enemies that move around. These entities attach to
    other entities when they fall onto them."""
    weight = 1

    def set_state(self, new_state):
        if __debug__ and settings.printstatechanges:
            if self.current_state:
                print self.__class__.__name__ + str(id(self)), self.current_state.__name__, " => ", new_state.__name__, self.position
            else:
                print self.__class__.__name__ + str(id(self)), "None", " => ", new_state.__name__, self.position
        if self.current_state:
            self.current_state.exit(self)
        new_state.enter(self)
        self.current_state = new_state

    def think_move(self, dt):
        state = self.current_state.think_move(self, dt)
        if state: self.set_state(state)

    def handle_collisions(self):
        if not self.alive: return
        if not self.parent.supporting(self.rect):  # No longer standing on the thing, so detach
            self.attach_to()
        if not self.colliders: return
        nhits = 0
        moved = True
        while moved:
            moved = False
            for hit in self.rect.collidelistall(self.colliders):
                if self.colliders[hit] is self or self.colliders[hit] in self.children: continue
                dx, dy = self.colliders[hit].constrain(self.rect, self.moved)
                if not dx and not dy: continue
                if dy < 0 and self.vy >= 0:  # Only want to collide with the top of something if we're moving down
                    self.move((dx, dy))
                    self.stop_falling()
                    self.attach_to(self.colliders[hit])
                    moved = True
                elif dy >= 0:  # Not moving down
                    self.move((dx, dy))
                    moved = True
                    if dy > 0 and self.vy < -50: self.vy = -50. # But maybe I should be.
                nhits += 1
                if nhits > 3:
                    self.squish()
                    return

        if self.parent is self.entity_tree:
            for platform in self.colliders:
                if platform.supporting(self.rect):
                    # TODO: what's the right behavior when you move from one platform to another?
                    state = self.current_state.fall(self)
                    if state: self.set_state(state)
                    self.attach_to(platform)
                    self.stop_falling()
                    break
            if self.parent is self.entity_tree:
                state = self.current_state.fall(self)
                if state: self.set_state(state)

        if settings.collideafter:
            for child in list(self.children):
                child.handle_collisions()

    def stop_falling(self):
        state = self.current_state.stop_falling(self)
        if state:
            self.set_state(state)

    
    def block_at(self, (tx, ty)):
        r = Rect(self.rect)
        x, y = r.center
        r.width -= 4
        r.height -= 4
        r.centerx = int(x / 64. + tx) * 64 + 32  # TODO: don't assume I'm exactly 1 tile big
        r.centery = int(y / 64. + ty) * 64 + 32
        return [self.colliders[j] for j in r.collidelistall(self.colliders)]

    def standing_on(self, platforms = None):
        if platforms is None: platforms = self.colliders
        if self.vy < 0: return None # going up
        for platform in platforms:
            if platform.supporting(self.rect):
                return platform

    def will_release(self):
        return self.parent.will_release()



