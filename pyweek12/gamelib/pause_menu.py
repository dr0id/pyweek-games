import pygame
from pygame.locals import *

from gummworld2 import State, data

import menu, context, game, settings, keysyms

INFO_COLOR = Color('skyblue')

class PauseMenuContext(menu.MenuContext):
    
    def __init__(self):
        super(PauseMenuContext, self).__init__(hilight=False)
        
        self.headers = []
        self.keysyms = []
        self.vals = []
        self.descs = []
        
        self.hpad = 40
        self.vpad = 3
        
        ## FILTER: these keys are not displayed.
        self.ignore_keysyms = (
            'up', 'down', 'select',
        )
        ## FILTER: only the first N values are displayed.
        self.nvals = 2
        
        self.add_header(State.level.name)
        self.add_header('')
        self.add_header('= PAUSED =')
        self.add_header('')
        self.add_key_info('ACTION', 'KEYS', 'DESCRIPTION')
        for keysym in settings.keys.ordered:
            if keysym not in self.ignore_keysyms:
                vals = settings.keys.values(keysym)[0:self.nvals]
                desc = settings.keys.description(keysym)
                self.add_key_info(keysym, vals, desc)
        
        self.pack()
    
    def add_header(self, line):
        s = pygame.sprite.Sprite()
        s.image,s.rect = self.render(line)
        self.headers.append(s)
        self.items.append(s)
    
    def add_key_info(self, keysym, vals, desc):
        s = pygame.sprite.Sprite()
        if isinstance(vals, str):
            color = menu.TEXT_COLOR
            vals_text = vals
        else:
            def quote(s):
                special = {
                    ',' : 'COMMA',
                    '.' : 'PERIOD',
                    ':' : 'COLON',
                    ';' : 'SEMICOLON',
                    "'" : 'SINGLE QUOTE',
                    '"' : 'DOUBLE QUOTE',
                    '`' : 'BACKTICK',
                    '-' : 'DASH',
                    '_' : 'UNDERSCORE',
                }
                if s in special:
                    return special[s]
                if len(s) == 1 and not (s.isalpha() or s.isdigit()):
                    return '"' + s + '"'
                return s.upper()
            color = INFO_COLOR
            vals_text = ', '.join([quote(pygame.key.name(v)) for v in vals])
        s.image,s.rect = self.render(vals_text, color)
        self.vals.append(s)
        self.items.append(s)
        
        s = pygame.sprite.Sprite()
        s.image,s.rect = self.render(keysym.capitalize(), color)
        self.keysyms.append(s)
        self.items.append(s)
        
        s = pygame.sprite.Sprite()
        s.image,s.rect = self.render(desc, color)
        self.descs.append(s)
        self.items.append(s)
    
    def pack(self):
        fonth = self.headers[0].rect.h
        kw = reduce(max, [s.rect.w for s in self.keysyms], 0)
        vw = reduce(max, [s.rect.w for s in self.vals], 0)
        dw = reduce(max, [s.rect.w for s in self.descs], 0)
        
        screen_rect = State.screen.rect
        num_rows = len(self.headers+self.keysyms)
        text_width = kw + self.hpad + vw + self.hpad + dw
        text_height = num_rows * fonth + (num_rows * self.vpad)
        
        keysyms_left = screen_rect.centerx - text_width / 2
        vals_left = keysyms_left + kw + self.hpad
        descs_left = vals_left + vw + self.hpad
        text_top = screen_rect.centery - text_height / 2
        
        # Set top and center for headers.
        y = text_top
        for sprite in self.headers:
            r = sprite.rect
            r.center = screen_rect.center
            r.top = y
            y += fonth + self.vpad
        # Set top and left for three columns of data.
        for i in range(len(self.keysyms)):
            for left,sprite in (
                (keysyms_left, self.keysyms[i]),
                (vals_left, self.vals[i]),
                (descs_left,self.descs[i]),
            ):
                r = sprite.rect
                r.left = left
                r.top = y
            y += fonth + self.vpad
    
    def render(self, text, color=menu.TEXT_COLOR):
        img = self.font.render(text, True, color)
        rect = img.get_rect()
        return img,rect
    
    def think(self, dt):
        pygame.time.wait(40)
        lvid = State.level.level_id
        for e in pygame.event.get():
            quit_event = False
            if e.type == KEYDOWN:
                if e.key in settings.keys.quit:
                    quit_event = True
                elif e.key in settings.keys.resume:
                    context.pop()
                    # Try a workaround for Cosmo
                    State.clock.reset()
                elif e.key == K_r:
                    context.pop(2)
                    context.push(game.GameContext(lvid))
            elif e.type == QUIT:
                quit_event = True
            
            if quit_event:
                context.pop(2)
                if lvid[0] < 0:
                    # test game
                    if __debug__:
                        print '%s: %s: %s' % (self.__class__.__name__,'exit to desktop', lvid)
                    return
                elif lvid == (0,0):
                    # exit game from main lobby
                    if __debug__:
                        print '%s: %s: %s' % (self.__class__.__name__,'exit to desktop', lvid)
                    return
                elif lvid[0]:
                    # return to lobby for my act
                    if __debug__:
                        print '%s: %s: %s' % (self.__class__.__name__,'exit to act lobby', lvid)
#                    lvid = 0,lvid[1]
                    lvid = 0,lvid[0]
                else:
                    # return to main lobby
                    if __debug__:
                        print '%s: %s: %s' % (self.__class__.__name__,'exit to main lobby', lvid)
                    lvid = 0,0
                context.push(game.GameContext(lvid))

if __name__ == '__main__':
    import gummworld2
    pygame.init()
    scr = State.screen = gummworld2.Screen((600,400))
    context.push(PauseMenuContext())
    while context.top():
        pygame.time.wait(40)
        scr.clear()
        context.top().think(0)
        if context.top():
            context.top().draw(scr)
        scr.flip()
