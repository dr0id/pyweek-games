# -*- coding: utf-8 -*-

import pygame
from pygame.locals import *

from gummworld2 import State, Vec2d

import entity, effect
import context
import game
import settings
import animation
from userblock import UserBlock
import sound
# ----------------------------------------------------------------------------
class _StateBase(object):

    @staticmethod
    def enter(self):
        pass

    @staticmethod
    def exit(self):
        pass

    @staticmethod
    def move_left(self):
        pass

    @staticmethod
    def move_right(self):
        pass

    @staticmethod
    def stop_moving(self):
        self.vx = 0.0

    @staticmethod
    def jump(self):
        pass

    @staticmethod
    def land(self):
        pass

    @staticmethod
    def fall(self):
        pass

    @staticmethod
    def stop_falling(self):
        pass

    @staticmethod
    def turn_to_block(self):
        pass

    @staticmethod
    def think(self, dt):
        pass

    # TODO: do we ever need a think method?
    ## See _StateWalk. think() would run sound and animation loops.
    @staticmethod
    def think_move(self, dt):
        self.move((self.vx * dt, self.vy * dt))

# ----------------------------------------------------------------------------
class _StateEntering(_StateBase):

    @staticmethod
    def enter(self):
        self.current_animation = animation.playerstandright
        self.vx, self.vy = 0, 0
        self.facingright = True
        self._time = 0.0
        self.alive = True

    @staticmethod
    def think_move(self, dt):
        self._time += dt
        if self._time > settings.player_enter_done:
            return _StateFall
    
    @staticmethod
    def exit(self):
        self.attach_to()

# ----------------------------------------------------------------------------
class _StateStanding(_StateBase):

    @staticmethod
    def enter(self):
        self.current_animation = animation.playerstandright if self.facingright else animation.playerstandleft
        self.vx = 0.0
        self.vy = 0.0

    @staticmethod
    def move_left(self):
        self.vx = settings.player_speed * -1
        return _StateWalk

    @staticmethod
    def move_right(self):
        self.vx = settings.player_speed
        return _StateWalk
        
    @staticmethod
    def jump(self):
        if not self.parent.will_release(): return
        self.attach_to()
        return _StateJump
    
    @staticmethod
    def fall(self):
        self.attach_to()
        return _StateFall
    
    @staticmethod
    def turn_to_block(self):
        if not self.rect.collidepoint(State.level.spawnpoint):
            return _StateAlignToBlock

# ----------------------------------------------------------------------------
class _StateWalk(_StateBase):

    @staticmethod
    def enter(self):
        self.facingright = True if self.vx > 0 else False 
        self.current_animation = animation.playerwalkright if self.facingright else animation.playerwalkleft
    
    @staticmethod
    def move_left(self):
        if self.vx > 0:
            self.current_animation = animation.playerwalkleft
            self.vx *= -1

    @staticmethod
    def move_right(self):
        if self.vx < 0.0:
            self.current_animation = animation.playerwalkright
            self.vx *= -1

    @staticmethod
    def stop_moving(self):
        return _StateAlign if settings.align_on_walk else _StateStanding

    @staticmethod
    def fall(self):
        self.attach_to()
        return _StateFall

    @staticmethod
    def jump(self):
        if not self.parent.will_release(): return
        self.attach_to()
        return _StateJump
    
    @staticmethod
    def think(self, dt):
        sound.play_next_sfx("walk")  # TODO: figure out what this should do, because it doesn't sound good
        pass 

# ----------------------------------------------------------------------------

class _StateAlign(_StateStanding):  # Inherits from Standing because it reacts to key presses the same
    """Continue walking until you hit the next grid point"""

    @staticmethod
    def enter(self):
        self.tilex = int(self.relative_position().x / 64.)  # TODO: more airtight logic here
        if abs(self.vx) < 140.:  # TODO: tweak this minimum speed
            self.vx = 140. if self.facingright else -140.
#        self.current_animation = animation.playerwalkright if self.facingright else animation.playerwalkleft

    @staticmethod
    def stop_moving(self):
        pass

    @staticmethod
    def think_move(self, dt):
        self.move((self.vx * dt, 0))
        px, py = self.relative_position()
        tilex, dx = int(px / 64.), px % 64
        py = (py + 32) % 64 - 32
        if self.facingright and (dx < 16 or tilex != self.tilex):
            self.move((-dx, -py))
            self.current_state.stop_moving(self)
            return _StateStanding
        if not self.facingright and (dx > 48 or tilex != self.tilex):
            self.move((64-dx, -py))
            self.current_state.stop_moving(self)
            return _StateStanding

# ----------------------------------------------------------------------------
class _StateAlignToBlock(_StateAlign):

    @staticmethod
    def stop_moving(self):
        '''
        Restart level and add block.
        '''
        if State.level.player_stones < State.level.player_lives:
            State.level.player_stones += 1
            sound.play_sfx("magicrock")
            newblock = UserBlock(self.position.x - 32, self.position.y -32, 64, 64)
            State.level.add_object(newblock)
            newblock.set_tree(self.entity_tree)
            newblock.attach_to(self.parent)
            self.attach_to()
            self.position = State.level.spawnpoint
            self.onscreen = False
            self.vx, self.vy = 0., 0.
        else:
            self.show_glowy = False
            ## TODO: SFX and some visual cue here to indicate you have no more
            ## lives to throw away!
            pass

    @staticmethod
    def think_move(self, dt):
        if _StateAlign.think_move(self, dt):
            if self.show_glowy:
                return _StateEntering
            else:
                return _StateFall

# ----------------------------------------------------------------------------
class _StatePush(_StateBase):
    pass

# ----------------------------------------------------------------------------
class _StateFall(_StateBase):

    @staticmethod
    def enter(self):
        self.current_animation = animation.playerjumpright if self.facingright else animation.playerjumpleft
        self.lovershoot, self.rovershoot = 0., 0.
        self.attach_to()

    @staticmethod
    def exit(self):
        self.vy = 0.0
        if __debug__: print "leaving jump, setting vy to 0", self.vy

    @staticmethod
    def think_move(self, dt):
        da = settings.gravity * dt * dt * 0.5
        self.vy = min(self.vy + settings.gravity * dt, settings.terminal_velocity)
        self.move((self.vx * dt, self.vy * dt + da))
        if self.vy < 0:
            self.current_animation = animation.playerjumpright if self.facingright else animation.playerjumpleft
        else:
            self.current_animation = animation.playerfallright if self.facingright else animation.playerfallleft
        # hold you back by up to 65% of your motion
        if self.rovershoot > 0 and self.vx > 0:
            dx = min(self.vx * dt * 0.65, self.rovershoot)
            if __debug__:
                print "overshoot info: ", dx, self.rovershoot, self.vx * dt, self.vy * dt
            self.move((-dx, 0))
            self.rovershoot -= dx
        elif self.lovershoot < 0 and self.vx < 0:
            dx = max(self.vx * dt * 0.65, self.lovershoot)
            self.move((-dx, 0))
            self.lovershoot -= dx

    @staticmethod
    def move_left(self):
        self.current_animation = animation.playerjumpleft
        self.vx = settings.player_speed * -1

    @staticmethod
    def move_right(self):
        self.current_animation = animation.playerjumpright
        self.vx = settings.player_speed

    @staticmethod
    def stop_falling(self):
        keys = pygame.key.get_pressed()
        mx = any(keys[k] for k in settings.keys.right) - any(keys[k] for k in settings.keys.left)
        if self.vx > 0 and mx == 1 or self.vx < 0 and mx == -1: return _StateWalk

        if self.vy == 0.: return _StateAlign  # This is what happens when you step from one block to the next
        self.vy = 0.
        self.grinding = False
        if settings.align_on_jump: return _StateAlign
        return _StateStanding  # TODO: check this logic

    @staticmethod
    def jump(self):
        if not settings.can_double_jump:
            return
        print "Grinding", self.grinding
        if self.grinding:
            return _StateDoubleJump

# ----------------------------------------------------------------------------
class _StateJump(_StateFall):
    """The only real difference between jumping and falling is that you
    start out with an initial upward velocity."""

    @staticmethod
    def enter(self):
        self.current_animation = animation.playerjumpright if self.facingright else animation.playerjumpleft
        sound.play_sfx('jump')
        self.vy = -settings.player_jump_start_speed
        # This is a hack to make sure you can't make what's supposed to be an impossible
        #   jump by edging out past the edge of a cliff and jumping at the last second.
        #   This is the number of pixels past the edge you are, and the idea is to undo
        #   them before you reach the apex of your jump.
        # TODO: make it so this only applies if you're actually stepping off the edge
        #   of a cliff.
        self.rovershoot = (self.position.x - 32) % 64
        self.lovershoot = self.rovershoot - 64 if self.rovershoot else 0
        if not settings.do_overshoot:
            self.lovershoot, self.rovershoot = 0, 0
    
# ----------------------------------------------------------------------------
class _StateDoubleJump(_StateJump):
    
    @staticmethod
    def enter(self):
        self.attach_to()
    
    @staticmethod
    def jump(self):
        pass

# ----------------------------------------------------------------------------
class _StateBlock(_StateBase):
    #TODO implement same behavior as a block

    @staticmethod
    def enter(self):
        # TODO: change animation
        pass

    @staticmethod
    def move_left(self):
        pass

    @staticmethod
    def move_right(self):
        pass

# ----------------------------------------------------------------------------

class _StateSquish(_StateBase):

    @staticmethod
    def enter(self):
        self.alive = False
        self.vy = -1000.
        self.vx = -200. if self.facingright else 200.
        self.current_animation = animation.playerdieright if self.facingright else animation.playerdieleft

    @staticmethod
    def think_move(self, dt):
        g = 2400.  # Fall fast
        da = g * dt * dt * 0.5
        self.vy = self.vy + g * dt
        self.move((self.vx * dt, self.vy * dt + da))

# ----------------------------------------------------------------------------

class InputEntityMixin(object):
    def __init__(self, *args, **kw):
        super(InputEntityMixin, self).__init__(*args, **kw)
    
    def handle_event(self, event):
        pass

class Player(entity.AttachableEntity, InputEntityMixin):
    layer = 2.7

    animations = None # {name: Animation}

    collision_types = [
        'blocks', 'userblocks',
        'platforms', 'movingplatforms', 'weightedplatforms',
        'counterweightedplatforms',
    ]

    def __init__(self, pos):
        x, y = pos
        w, h = 64, 64 #TODO go into settings?
        entity.AttachableEntity.__init__(self, x, y, w, h, pos)
        self._time = 0.0
        self.current_state = _StateEntering
        self.current_state.enter(self)
        self.colliders = [] # list of entities to collide with
            
        self.current_animation = animation.playerstandright
#        self.image = self.current_animation.image
        self.rect = Rect(0,0,64,64)
        self.rect.center = self.position
        self.facingright = True
        self.onscreen = False
        self.alive = True
        self.show_glowy = True

    def __getstate__(self):
        d = dict(self.__dict__)
        d['position'] = self.position * 1.
        return d
        
    def handle_event(self, event):
        if event.type == KEYDOWN:
            if event.key in settings.keys.jump:
                self.jump()
            if event.key in settings.keys.stone:
                self.turn_to_block()
            if event.key in settings.keys.squish:
                self.squish()

    # --- state methods ---#
    def move_left(self):
        state = self.current_state.move_left(self)
        if state:
            self.set_state(state)

    def move_right(self):
        state = self.current_state.move_right(self)
        if state:
            self.set_state(state)

    def stop_moving(self):
        state = self.current_state.stop_moving(self)
        if state:
            self.set_state(state)

    def jump(self):
        state = self.current_state.jump(self)
        if state:
            self.set_state(state)

    def fall(self):
        """Call every frame that motion down is not blocked"""
        state = self.current_state.fall(self)
        if state:
            self.set_state(state)

    def stop_falling(self):
        """Call every frame that motion down is blocked"""
        state = self.current_state.stop_falling(self)
        if state:
            sound.play_sfx("land")
            self.set_state(state)

    def squish(self):
        sound.play_sfx("die")
        self.set_state(_StateSquish)

    def turn_to_block(self):
        state = self.current_state.turn_to_block(self)
        if state:
            self.set_state(state)
    
    def is_onscreen(self):
        x, y = self.position.x, self.position.y
        cam = State.camera.rect
        return (x >= cam.left - settings.pit_margin and
                x <= cam.right + settings.pit_margin and
                y >= cam.top - settings.pit_margin and
                y <= cam.bottom + settings.pit_margin)
    
    def is_entering(self):
        return self.current_state == _StateEntering
    
    def is_standing(self):
        return self.current_state == _StateStanding
    
    def think(self, dt, scootch = Vec2d(0,0)):
        state = self.current_state.think(self, dt)
        if state:
            self.set_state(state)

        entity.AttachableEntity.think(self, dt, scootch)

        if not self.onscreen and self.is_onscreen():
            self.onscreen = True

        if self.vx > 0.: self.facingright = True
        if self.vx < 0.: self.facingright = False

        # update animation
        self.current_animation.think(dt)

    def draw(self, screen, world_to_screen):
        if self.current_state is _StateEntering and self.show_glowy:
            effect.draw_glowy_bit(screen, world_to_screen(self.interp_position()))
        entity.AttachableEntity.draw(self, screen, world_to_screen)
        

    def current_image(self):
        return self.current_animation.image()
        
    def fallen_to_death(self):
        self.position = State.level.spawnpoint
        self.onscreen = False
        self.vx, self.vy = 0., 0.
        self.set_state(_StateEntering)
    


    # --- other methods ---#

    def look_target(self):
        """Where the player is "looking", for the camera to follow."""
        look_time = 0.8
        x0, y0 = self.position
        return x0 + (128 if self.facingright else -128), y0 + self.vy * 0.2


