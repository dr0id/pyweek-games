'''Game main module.

Contains the entry point used by the run_game.py script.

Feel free to put all your game code here, or in other modules in this "gamelib"
package.
'''

import pygame
from pygame.locals import *

import gummworld2

import context, settings, main_menu, sound, jukebox, game, transition, block

BG_COLOR = Color('#372A70')

def main():
    pygame.display.set_caption(settings.game_title, settings.game_title)
    icon_surf = pygame.image.load(gummworld2.data.filepath('icon', '32x32.png'))
    pygame.display.set_icon(icon_surf)
    State = gummworld2.State
    flags = HWSURFACE | FULLSCREEN if settings.fullscreen else 0
    screen = State.screen = gummworld2.Screen(settings.resolution, flags)
    screen.eraser.fill(BG_COLOR)
    screen.flip()
    
    block.init()
    
    pygame_get_ticks = pygame.time.get_ticks
    pygame_time_source = lambda:pygame_get_ticks() / 1000.0
    clock = State.clock = gummworld2.GameClock(
        ticks_per_second=25, max_fps=0, time_source=pygame_time_source)
    
    sound.load_sfx()
    sound.load_songs()
    jukebox.jukebox.set_master_volume(settings.master_volume)
    jukebox.jukebox.set_type_volume('sfx', settings.sfx_volume)
    for name in 'walk1','walk2','walk3','walk4':
        sound.sfx_player[name].set_unit_volume(settings.walk_volume)
    for name in 'jump', 'land':
        sound.sfx_player[name].set_unit_volume(settings.jump_volume)
    
    if settings.quickstart:
        context.push(game.GameContext(settings.quickstart_map))
    else:
        # the initial setup is a bit tricky:
        # push first the context that is the main game
        # then push a black context
        # and use a pop transition so that at the end only the main 
        # game context is left on the stack
        # # context.push(game.GameContext((0, 0)))
        # # context.push(transition.UniColorContext((0, 0, 0)))
        # # context.push(transition.EscapePopTransition(transition.FadeInEffect(settings.fade_duration)))
        context.push(transition.EscapePushTransition(game.GameContext((0, 0)), transition.ColorFadeInEffect((0, 0, 0), settings.fade_duration)))
    
    elapsed = 0.0
    while context.top():
        elapsed += clock.tick()
        if clock.update_ready():
            context.top().think(elapsed)
            elapsed = 0.0
        if not context.top():
            break
        if clock.frame_ready():
            context.top().draw(screen)
            screen.flip()
