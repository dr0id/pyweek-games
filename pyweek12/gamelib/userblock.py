import entity, block, spritesheet, gummworld2, settings, animation

class _StateStanding(object):

    @staticmethod
    def enter(self):
        # TODO: change animation
        self.vx = 0.0
        self.vy = 0.0

    @staticmethod
    def exit(self):
        pass

    @staticmethod
    def fall(self):
        self.attach_to()
        return _StateFall

    @staticmethod
    def think_move(self, dt):
        pass

    @staticmethod
    def stop_falling(self):
        pass

class _StateFall(object):

    @staticmethod
    def enter(self):
        self.attach_to()

    @staticmethod
    def exit(self):
        pass

    @staticmethod
    def think_move(self, dt):
        da = settings.gravity * dt * dt * 0.5
        self.vy += settings.gravity * dt
        self.move((self.vx * dt, self.vy * dt + da))

    @staticmethod
    def fall(self):
        pass

    @staticmethod
    def stop_falling(self):
        return _StateStanding

class _StateSquish(_StateFall):

    @staticmethod
    def enter(self):
        self.alive = False
        self.vy = -1000.
        self.vx = 200.

    @staticmethod
    def think_move(self, dt):
        g = 2400.  # Fall fast
        da = g * dt * dt * 0.5
        self.vy = self.vy + g * dt
        self.move((self.vx * dt, self.vy * dt + da))

# ----------------------------------------------------------------------------


class UserBlock(entity.AttachableEntity, block.Block):
    tiled_group = 'userblocks'
    thinker = True
    layer = 2.8
    def __init__(self, *arg, **kw):
        self.current_state = _StateStanding
        entity.AttachableEntity.__init__(self, *arg, **kw)
        self.current_animation = animation.userblock
        self.alive = True

    def draw_image(self, screen, world_to_screen):
        entity.AttachableEntity.draw_image(self, screen, world_to_screen)

    def current_image(self):
        return self.current_animation.image()

    def squish(self):
        self.set_state(_StateSquish)


