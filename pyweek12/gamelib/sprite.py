import pygame

import gummworld2
from gummworld2 import Vec2d

VALID_KWARGS = ('position', 'image', 'rect')

class Sprite(pygame.sprite.Sprite):
    
    def __init__(self, *groups, **kw):
        for name in kw:
            if name not in VALID_KWARGS:
                raise pygame.error, 'unexpected keyword arg: "%s"' % (name,)
        super(Sprite, self).__init__(*groups)
        
        self.image = kw.get('image', pygame.surface.Surface((64,96)))
        self.rect = kw.get('rect', self.image.get_rect())
        self._position = Vec2d(kw.get('position', (0,0)))
    
    @property
    def position(self):
        return Vec2d(self._position)
    @position.setter
    def position(self, val):
        self_pos = self._position
        x,y = self_pos.x,self_pos.y = var
        self.rect.center = round(x), round(y)
