import pygame, math, os
from pygame.locals import *

import gummworld2
from gummworld2 import data, state, State, Vec2d

import context, settings, level_router, level_menu, cutscene
from level_info import LevelInfo
import pause_menu, transition, credits
import world, camera_target, level, player
import win, loose
import sound

# preload credits context :(
credits_context = credits.CreditsContext()

class GameContext(context.Context):
    
    def __init__(self, world_source):
        if isinstance(world_source, str) and world_source.endswith("pickle"):
            if __debug__:
                print '%s: %s: %s' % (self.__class__.__name__,'loading world source pickle', world_source)
            self.loadrecord(world_source)
            self.recording = False
            self.record = []
            world_source = self.tmx_file
        else:
            if isinstance(world_source, str):
                if __debug__:
                    print '%s: %s: %s' % (self.__class__.__name__,'loading world source string', world_source)
                self.tmx_file = world_source
            else:  # must be tuple
                if __debug__:
                    print '%s: %s: %s' % (self.__class__.__name__,'loading world source lvid', world_source)
                lvdb = LevelInfo(settings.level_info_file)
                lvinfo = lvdb.info_by_number(world_source)
                self.tmx_file = lvinfo[3]
            self.recording = settings.get_record
            self.recordt = 0.
            self.record = []  # For playing back playtesters
            self.playback = None
        
        self.level = level.Level(world_source, skip_intro = False)
        
        self.speed = 3
        self.move = Vec2d(0,0)
        self.key_look = False
        self.mouse_look = False
        
        if self.level.world_num > 0:
            self.preferred_song = 'back2.ogg'
        else:
            self.preferred_song = 'celest.ogg'
        
        State.game = self
    
    def enter(self):
        self.set_track_player(True)
        
        if hasattr(State, 'now_playing'):
            if State.now_playing != self.preferred_song:
                pygame.mixer.music.fadeout(1000)
        State.now_playing = self.preferred_song
#        sound.song_volume(settings.music_loud)
    
    def exit(self):
#        sound.song_volume(settings.music_soft)
        
        if self.level.state_name_orig in state.states:
            del state.states[self.level.state_name_orig]
            del state.states[self.level.state_name_collapsed]
        
        if self.record:
            try:
                self.saverecord()
            except Exception, e:
                if __debug__:
                    print "ERROR SAVING RECORDING!", e
                    print self.record[-1]

    def suspend(self):
#        sound.song_volume(settings.music_soft)
        pass
    
    def resume(self):
#        sound.song_volume(settings.music_loud)
        if hasattr(State, 'now_playing'):
            if State.now_playing != self.preferred_song:
                pygame.mixer.music.fadeout(1000)
        State.now_playing = self.preferred_song
        
        self.set_track_player(True)
        if self.level.state_name_orig in state.states:
            State.restore(self.level.state_name_orig)
            State.restore(self.level.state_name_collapsed)
            ## TODO: This needs to change since level numbering scheme has changed.
            if self.level.level_id == (1,0):
                State.level.player.position = State.level.spawnpoint
    
    def think(self, dt):
        if not dt: return
        #sound.next_song(dt)
        if not sound.song_is_playing():
            pygame.mixer.music.load(data.filepath('music', self.preferred_song))
            pygame.mixer.music.play()
        
        if self.playback:
            self.think_playback(dt)
            return
        if State.level.is_ready():
            self.get_input()
            State.level.think(dt)
            self.think_camera(dt)  #TODO: I'm moving some stuff from here to level.think.... 
            State.camera.update()
            State.hud.update()
            self.check_end_conditions()
        else:
            State.level.think(dt)  # TODO: ...but they weren't in this block. Is that bad?
            test_rect = State.level.test_rect
            test_rect.center = State.level.intro_data.position
            test_rect.clamp_ip(State.world.rect)
            State.camera.position = test_rect.center
            State.camera.update()
            State.hud.update()
            pygame.event.get() # prevent input to non ready states of level (?)
        if self.recording:
            self.recordt += dt
            if self.recordt >= 1. / settings.playback_max_fps:
                if len(self.record) < 500:
                    self.record.append((self.recordt, self.getstate()))
                self.recordt = 0.

    def think_playback(self, dt):
        pygame.event.get()
        m = pygame.key.get_pressed()
        if m[K_ESCAPE]: context.pop()
        sfactor = 0.5 if m[K_SPACE] else 1.
        self.playbacktimer += dt * settings.playback_factor * sfactor
        state = None
        while self.playbacktimer > 0:
            odt, state = self.playback[0]
            del self.playback[0]
            if not self.playback:
                context.pop()
                return
            self.playbacktimer -= odt
        if state:
            self.setstate(state)

    def think_camera(self, dt):
        test_rect = State.level.test_rect
        if self.camera_tracks_player():
            tx, ty = State.level.look_target()
            cx, cy = State.camera.position
            fac = math.exp(-4. * dt)
            test_rect.center = fac * cx + (1.-fac) * tx, fac * cy + (1.-fac) * ty
        else:
            test_rect.center += self.move * 20
        world_rect = State.world.rect
        if test_rect.left < world_rect.left: test_rect.left = world_rect.left
        if test_rect.top < world_rect.top: test_rect.top = world_rect.top
        if test_rect.right > world_rect.right: test_rect.right = world_rect.right
        if test_rect.bottom > world_rect.bottom: test_rect.bottom = world_rect.bottom
        State.camera.position = test_rect.center

    def getstate(self):
        return Vec2d(State.camera.position), State.level.getstate()

    def setstate(self, state):
        camera = State.camera
        camera.position, levelstate = state
        camera.rect.center = camera.position
        State.level.setstate(levelstate)
        
    
    def saverecord(self):
        import cPickle
        levelname = os.path.basename(self.tmx_file).partition(".")[0]
        fname = gummworld2.data.filepath('record', "playtest-%s.pickle" % levelname)
        f = open(fname, "wb")
        obj = self.tmx_file, tuple(self.record)
        cPickle.dump(obj, f)

    def loadrecord(self, pfile):
        import cPickle
        f = open(gummworld2.data.filepath('record', pfile), "rb")
        obj = cPickle.load(f)
        self.tmx_file, playback = obj
        if __debug__: print self.tmx_file
        f = self.tmx_file.split("/")[-1].split("\\")[-1]  # Hack!
        self.tmx_file = gummworld2.data.filepath('map', f)
        if __debug__: print self.tmx_file
        self.playback = list(playback)
        self.playbacktimer = 0.

    def draw(self, screen):
        if self.playback:
            screen.clear()
            State.level.draw(screen)
            screen.flip()
            return
            

        camera = State.camera
        ## The following block resolves Camera jitter during Transition.
        if self == context.top():
            camera.interpolate()
        screen.clear()
        State.level.draw(screen)
        State.hud.draw()
        
        self.draw_signs(screen)
        
        draw_grid = gummworld2.toolkit.draw_grid
        if State.show_grid:
            State.restore(self.level.state_name_orig)
            draw_grid()
            State.restore(self.level.state_name_collapsed)
            if self.mouse_look:
                pygame.draw.aaline(screen.surface, settings.mouse_pan_color,
                    State.screen.center, pygame.mouse.get_pos(), 2)
        
        # screen.flip()


    def draw_signs(self, screen):
        if 'signs' in State.level.objects:
            for sign in State.level.objects['signs']:
                if sign.rect.colliderect(State.level.player.rect):
                    posx, posy = settings.resolution
                    img = self.level.text_cache[sign.message]
                    blit_rect = img.get_rect()
                    blit_rect.midtop = (posx/2, posy/8)
                    screen.surface.blit(img, blit_rect)

    def get_input(self):  # TODO: I don't keep track of player yet. Should this logic be elsewhere?
        for e in pygame.event.get():
            if self.camera_tracks_player():
                State.level.player.handle_event(e)
            if e.type == KEYDOWN:
                if e.key in settings.keys.pause:
                    if self.level.level_id == (0,0):
                        self.pop()
                    else:
                        context.push(level_router.LevelRouter(
                            pause_menu.PauseMenuContext))
                elif e.key == K_F12 and State.level.level_id == (0,0):
                    context.push(level_menu.LevelMenuContext())
            elif e.type == QUIT:
                if self.level.level_id == (0,0):
                    self.pop()
                else:
                    self.push(pause_menu.PauseMenuContext())
        
        #TODO: maybe change that to use events KEYDOWN and KEYUP
        # The problem is if the window loses focus it can miss the KEYUP event and
        #  the player can get stuck running in one direction
        keys = pygame.key.get_pressed()
        mx = any(keys[k] for k in settings.keys.right) - any(keys[k] for k in settings.keys.left)
        my = any(keys[k] for k in settings.keys.down) - any(keys[k] for k in settings.keys.up)
        
        self.key_look = any(keys[k] for k in settings.keys.scan)
        self.mouse_look = any(pygame.mouse.get_pressed())
        if self.mouse_look:
            g = gummworld2.geometry
            mag = g.distance(State.screen.center, pygame.mouse.get_pos())
            radius = mag / State.screen.height * settings.mouse_pan_speed
            if mag > settings.mouse_pan_threshold:
                angle = g.angle_of(State.screen.center, pygame.mouse.get_pos())
                move_to = g.point_on_circumference(State.screen.center, radius, angle)
                mx,my = Vec2d(move_to) - State.screen.center
            self.set_track_player(False)
        elif self.key_look:
            self.set_track_player(False)
        elif not (self.key_look or self.mouse_look):
            self.set_track_player(True)
        
        if self.camera_tracks_player():
            if mx != self.move.x:
                if mx == 0:
                    State.level.player.stop_moving()
                elif mx > 0:
                    State.level.player.move_right()
                elif mx < 0:
                    State.level.player.move_left()
        
        self.move.x = mx
        self.move.y = my
        # TODO: -------------

    def camera_tracks_player(self):
        return State.camera.target.follow_player is True

    def set_track_player(self, bool):
        State.show_grid = not bool
        State.camera.target.follow_player = bool
    
    def check_end_conditions(self):
        cheatwin = __debug__ and settings.beat_any_level_by_pushing_F8 and pygame.key.get_pressed()[K_F8]
        if State.level.at_endpoint(State.level.player.rect) or cheatwin:
            sound.play_sfx("win")

            # Cutscene that shows after this level is beaten
            beatlvdb = LevelInfo(settings.save_file)
            cutscene_name = beatlvdb.cutscene_on_win(self.level.level_id)

            # Next level to load up
            lvdb = LevelInfo(settings.level_info_file)
            lvnext = lvdb.next(self.level.level_id)

            State.level.won()
            context.pop()
            if cutscene_name in ("1-8.txt", "2-6.txt", "3-3.txt"):
                lvnext = 0,0
            lrouter = level_router.LevelRouter(GameContext, lvnext)
            if cutscene_name:
                context.push(cutscene.CutScene(cutscene_name, lrouter))
            else:
                context.push(lrouter)
#            context.push(win.Win(GameContext, lvnext))
        elif State.level.player.rect.top >= State.world.rect.bottom + 40:
            State.level.lost()
        elif 'doors' in State.level.objects:
            in_a_door = False
            for door in State.level.objects['doors']:
#                if door.rect.colliderect(State.level.player.rect):
                if (door.position - State.level.player.position).get_length() < 5:
                    if self.disabled_door is door:
                        return
                    in_a_door = True
                    lvnext = door.tiled_properties['next']
                    if lvnext[0] == -3:
                        self.do_special_door(door)
                        return
                    lvdb = LevelInfo(settings.save_file)
                    ##lvdb.print_all()
                    lvhigh = lvdb.high()
                    if lvnext is None: return
                    level_status = lvdb.level_status(lvnext)
                    if __debug__:
                        print '<> Door: next=%s level_status=%d (%s)' % (
                            str(lvnext),
                            level_status,
                            ('LOCKED','UNLOCKED','BEATEN')[level_status]
                        )
                    if level_status == 0:
                        return
                    # Save level completion so we don't repeat cut scenes.
                    State.level.won()
#                    if (self.level.level_id) != (0,0):
                    context.pop()
                    context.push(level_router.LevelRouter(
                        GameContext, lvnext))
            if not in_a_door:
                self.disabled_door = None
    
    def do_special_door(self, door):
        lvnext = door.tiled_properties['next']
        if lvnext[1] == 0:
            ## Credits
            self.disabled_door = door
            self.push(transition.EscapePushTransition(credits_context, transition.FadeOutEffect(settings.fade_duration)))
#            context.push(credits.CreditsContext())
        elif lvnext[1] == 1:
            ## Exit game
            self.disabled_door = door
            self.pop()
        elif lvnext[1] == 2:
            ## New Game
            self.disabled_door = door
            try:
                os.unlink(settings.save_file)
            except:
                pass
            context.pop()
            context.push(GameContext((0,0)) )
    
    def push(self, new_con):
            context.push(new_con)
#        context.push(transition.FadeInTranition(
#            self, new_con, settings.fade_duration, False))
    
    def pop(self):
        context.pop()
#        context.push(transition.FadeOutTranition(
#            self, context.top(), settings.fade_duration, True))
