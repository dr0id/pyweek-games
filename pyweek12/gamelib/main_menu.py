import pygame
from pygame.locals import *

import menu, context, level_router, level_menu, game, credits, settings
from level_info import LevelInfo
import transition
import cutscene

class MainMenuContext(menu.MenuContext):
    
    def __init__(self):
        super(MainMenuContext, self).__init__()
        self.init()
    
    def init(self):
        del self.items[:]
        del self.text_list[:]
        self.my_text = list([
            'New Game',
            'Continue',
            'Select Level',
            'Credits',
            'Quit',
        ])
        if __debug__:
            self.my_text.append('Test Lobby')
        if __debug__: 
            self.my_text.append('Test CutScene')
        # Debug mode allows level selection.
        if not __debug__:
            self.my_text.remove('Select Level')
        # If there is not saved game, remove the option to Continue.
        lvdb = LevelInfo(settings.save_file)
        if not lvdb.names:
            self.my_text.remove('Continue')
        for text in self.my_text:
            self.add(text)
    
    def resume(self):
        self.init()
    
    def think(self, dt):
        pygame.time.wait(40)
        for e in pygame.event.get():
            if e.type == KEYDOWN:
                if e.key in settings.keys.up:
                    self.move_up()
                elif e.key in settings.keys.down:
                    self.move_down()
                elif e.key in settings.keys.select:
                    selected = self.get_selected()
                    if selected == 'New Game':
                        self.push(level_router.LevelRouter(
                            game.GameContext, (0,0)))
                    elif selected == 'Continue':
                        # Get the highest completed level id.
                        lvdb = LevelInfo(settings.save_file)
                        high = lvdb.high()
                        # Get the next id in sequence.
                        lvdb = LevelInfo(settings.level_info_file)
                        next = lvdb.next(high)
                        if next is None:
                            next = high
                        ## End-game to be handled in level router.
                        self.push(level_router.LevelRouter(
                            game.GameContext, next))
                    elif selected == 'Test Lobby':
                        self.push(level_router.LevelRouter(
                            game.GameContext, (0,0)))
                    elif selected == 'Select Level':
                        self.push(level_menu.LevelMenuContext())
                    elif selected == 'Credits':
                        self.push(credits.CreditsContext())
                    elif selected == 'Quit':
                        print context.cstack
                        self.pop()
                    if __debug__:
                        if selected == 'Test CutScene':
                            self.push(cutscene.CutScene('gummtest.txt'))
                elif e.key in settings.keys.quit:
                    self.pop()
            elif e.type == QUIT:
                self.pop()
    
    def push(self, new_con):
        # context.push(transition.FadeInTranition(self, new_con, settings.fade_duration, False))
        context.push(transition.EscapePushTranition(new_con, transition.FadeInEffect(settings.fade_duration)))
        
    def pop(self):
        # context.pop()
        # context.push(transition.FadeOutTranition(self, context.top(), settings.fade_duration, True))
        context.push(transition.EscapePopTransition(transition.FadeInEffect(settings.fade_duration)))

if __name__ == '__main__':
    import gummworld2
    pygame.init()
    scr = gummworld2.State.screen = gummworld2.Screen((400,400))
    context.push(MainMenuContext())
    while context.top():
        pygame.time.wait(40)
        scr.clear()
        context.top().think(0)
        if context.top():
            context.top().draw(scr)
        scr.flip()
