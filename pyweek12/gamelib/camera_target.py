import pygame
from pygame.locals import *

import gummworld2
from gummworld2 import Vec2d

class CameraTarget(gummworld2.model.QuadTreeObject):
    
    def __init__(self, rect, position=(0,0)):
        self.rect = Rect(rect)
        self._position = Vec2d(position)
        self.follow_player = True
