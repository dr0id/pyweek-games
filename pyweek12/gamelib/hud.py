import pygame

from gummworld2 import State, data
from gummworld2.ui import Statf, Stat, HUD as _HUD

import level_info, settings

class HUD(_HUD):

    def __init__(self):
        super(HUD, self).__init__()
        
        screen_size = State.screen.size
        if screen_size.y <= 480:
            big_font_size = 28
            med_font_size = 22
        else:
            big_font_size = 18
            med_font_size = 13
        self.big_font = pygame.font.Font(data.filepath('font','nvvzs.ttf'), big_font_size)
        self.med_font = pygame.font.Font(data.filepath('font','nvvzs.ttf'), med_font_size)
        
        if State.level.world_num != 0:
            self.add('Lives', Stat(
                self.next_pos(),
                '', callback=self.get_lives, interval=1000,
                font=self.big_font,
            ))
        
        self.add('Level', Stat(
            self.next_pos(),
            '', callback=self.get_level_name,
            font=self.med_font,
        ))
        
        if State.level.world_num == 0:
            self.lvdb = level_info.LevelInfo(settings.save_file)
            self.add('Progress', Statf(
                self.next_pos(),
                'Progress %s%%', callback=self.get_progress,
                font=self.med_font,
            ))
        
        if __debug__:
            self.add('FPS', Statf(
                self.next_pos(),
                '%d fps', callback=self.get_fps,
            ))
            self.add('UPS', Statf(
                self.next_pos(),
                '%d ups', callback=self.get_ups,
            ))
    
    def get_fps(self):
        fps = State.clock.get_fps()
        return fps
    
    def get_ups(self):
        ups = State.clock.get_ups()
        return ups
    
    def get_lives(self):
        stones = State.level.player_stones
        lives = State.level.player_lives
        return 'Lives: %s / %s' % (stones+1,lives+1)

    def get_level_name(self):
        level = State.level
        if level.world_num > 0:
            return "Act %s scene %s: %s" % (level.world_num, level.level_num, level.name)
        return level.name

    def get_progress(self):
        return str(self.lvdb.percent_complete())

