# This module is a simple wrapper for the jukebox module.
#
# sfx_player is the jukebox object. Advanced features not provided by this
# module may be accessed via the sfx_player, or the jukebox.jukebox object.
#
# Music is handled as a rotating queue. Play songs with next_song(). You can
# use the pygame.mixer.music module to access advanced features not provide here.

import os

import pygame

from gummworld2 import data

import settings, jukebox

sfx_player = jukebox.jukebox
carousels = {}
_songs = []
_now_playing = 0
_buffering = 3.0

def play_sfx(name):
    if settings.play_sound and pygame.mixer.get_init():
        sfx = sfx_player[name]
        sfx.play()
    return sfx

def play_next_sfx(name):
    if settings.play_sound and pygame.mixer.get_init():
        sfx = carousels[name]
        sfx.play_next()
    return sfx


def next_song(dt):
    global _now_playing, _buffering
    if not (_songs and settings.play_music):
        return
    if _buffering > 0:
        #if __debug__: print 'buff'
        _buffering -= dt
        if _buffering <= 0:
            #if __debug__: print 'play'
            pygame.mixer.music.play()
    elif not song_is_playing():
        #if __debug__: print 'next'
        if _now_playing >= len(_songs)-1:
            _now_playing = -1
        _now_playing += 1
        
        #if __debug__: print 'PLAY',_now_playing,_songs[_now_playing]
        pygame.mixer.music.load(_songs[_now_playing])
        _buffering = 4.0
#        pygame.mixer.music.play()

def sfx_names():
    return sfx_player.keys()

def song_names():
    return list(_songs)

def sfx_is_playing(sfx):
    return sfx.is_playing()

def song_is_playing():
    return pygame.mixer.music.get_busy()

def now_playing():
    return _songs[_now_playing]

def song_volume(level):
    pygame.mixer.music.set_volume(level)

def song_fadeout(ms):
    pygame.mixer.music.fadeout(ms)

def load_sfx():
    if __debug__: print 'loading sound effects assets:'
    sound_dir = data.paths['sound']
    for name in os.listdir(sound_dir):
        full_name = os.path.join(sound_dir,name)
        tag = os.path.splitext(name)[0]
        sfx_player.load_sound(full_name, name=tag)
        if __debug__: print '    %s:\t%s' % (tag,name)

    carousels['walk'] = SFXCarousel([n for n in sfx_names() if n.startswith('walk')])
    carousels['stone'] = SFXCarousel([n for n in sfx_names() if n.startswith('stone')])

class SFXCarousel(object):
    
    def __init__(self, *sfx_names):
        self.names = []
        self.i = 0
        self.sfx = None
        for arg in sfx_names:
            if isinstance(arg, str):
                self.add(arg)
            else:
                self.add(*arg)
    
    def add(self, *names):
        for name in names:
            self.names.append(name)
    
    def is_playing(self):
        c = self.get_current()
        return c and c.is_playing()
    
    def get_current(self):
        return self.sfx
    
    def play_next(self):
        if not self.is_playing():
            self.i += 1
            if self.i >= len(self.names):
                self.i = 0
            self.sfx = sfx_player[self.names[self.i]]
            if self.sfx:
                self.sfx.play()

def load_songs():
    if __debug__: print 'loading music assets:'
    song_dir = data.paths['music']
    for name in os.listdir(song_dir):
        full_name = os.path.join(song_dir,name)
        tag = os.path.splitext(name)[0]
        _songs.append(full_name)
        if __debug__: print '    %s' % name
    if _songs:
        pygame.mixer.music.load(_songs[_now_playing])

if __name__ == '__main__':
    from pygame.locals import *
    pygame.init()
    # Must have display open for sounds to work. Dunno why. You just do.
    pygame.display.set_mode((400,400))
    
    load_sfx()
    if __debug__: print 'Sound effects:', sfx_names()
    
    load_songs()
    if __debug__: print 'Songs:', song_names()
    
    # Play a carousel.
    sfx_carousel = SFXCarousel([n for n in sfx_names() if n.startswith('walk')])
    for i in range(12):
        print 'Playing carousel',i
        sfx_carousel.play_next()
        while sfx_carousel.is_playing():
            pygame.time.wait(40)
    
    # Play each of the sound effects.
    for name in sfx_player:
        if __debug__: print 'Playing SFX',name
        sfx = play_sfx(name)
        while sfx_is_playing(sfx):
            pygame.time.wait(40)
        pygame.time.wait(200)
    
    # Play the songs. Space skips to the next song. Escape exits.
    while 1:
        next_song(40)
        if __debug__: print 'Playing Song',now_playing()
        while song_is_playing():
            for e in pygame.event.get():
                if e.type == KEYDOWN:
                    if e.key == K_ESCAPE: quit()
                    elif e.key == K_SPACE: song_fadeout(1000)
            pygame.time.wait(200)
