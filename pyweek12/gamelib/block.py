# Terrain and objects

import time

import pygame
from pygame.locals import *

import gummworld2
from gummworld2 import data, Vec2d

import entity, spritesheet, settings

__all__ = ['Block', 'UserBlock', 'Platform']

class Block(entity.MovingEntity):
    """A solid block you can't move through in any direction"""
    tiled_group = 'blocks'
    color = "#ffffff"
    solid = True

    def __init__(self, *args, **kw):
        super(Block, self).__init__(*args, **kw)
        
        # Set this to True in subclasses if Level should create sprites from the
        # gummworld2 Map tiles at the starting location. The tiles will be
        # removed from the map.
        self.take_map_tiles = False
#        self.sprites = []
    
    def move_dists(self, rect):
        """How far would the given rect have to move in each direction
        (left, right, up, down) to not collide with me?"""
        return (rect.right - self.rect.left, self.rect.right - rect.left,
          rect.bottom - self.rect.top, self.rect.bottom - rect.top)

    def constrain(self, rect, (dx, dy)):
        """Return the distance you should move. (dx,dy) is the amount the
        rect has moved this frame."""
        mleft, mright, mup, mdown = self.move_dists(rect)
        if self.solid:
            m = min(mleft, mright)
            if m < settings.support_edge:  # Prefer sliding left/right when possible.
                if m == mleft: return -m, 0
                elif m == mright: return m, 0
            m = min(mleft, mright, mup, mdown)
            if m <= 0: return 0, 0
            if m == mleft: return -m, 0
            elif m == mright: return m, 0
            elif m == mup: return 0, -m
            elif m == mdown: return 0, m

        else:
            mleft, mright, mup, mdown = self.move_dists(rect)
            m = min(mleft, mright, mup, mdown)
            if m <= 0: return 0, 0
            if m == mup and dy > 0 and m < dy + self.vmargin() and mleft > settings.support_edge and mright > settings.support_edge: return 0, -m
            return 0, 0

    def vmargin(self):
        return 2
        
    def supporting(self, rect):
        """Return True if the rect could be walking on me"""
        # TODO: we need a more sophisticated way to tell if you're standing on a block
        # Idea: maybe pass dy and use that to discern proximity to block top.
        drop_rect = pygame.Rect(rect.left + settings.support_edge, rect.top + 1, rect.width - 2*settings.support_edge, rect.height)
        return self.rect.colliderect(drop_rect) and not self.rect.colliderect(rect)

    @classmethod
    def from_tiled(cls, obj, color="#ffffff"):
        rect = cls(obj.x, obj.y, obj.width, obj.height)
        rect.color = color
        rect.original_obj = obj
        rect.tiled_properties = {}
        rect.from_tiled_properties(obj.properties)
        return rect
    
    def from_tiled_properties(self, props):
        """Override to parse tiled map obj.properties.
        """
        pass
    
#    def make_sprites_from_tiles(self):
#        ## manipulating map directly, this is kinda dirty - Gumm
#        map = gummworld2.State.map
#        tiles = map.get_tiles_in_rect(self.rect, layer=0)
#        layer = map.layers[0]
#        for tile in tiles:
#            tile.parent = self
#            idx = layer.index(tile)
#            layer[idx] = None
#            self.sprites.append(tile)

    def draw_image(self, screen, world_to_screen):
        pass
#        for s in self.sprites:
#            rect = pygame.Rect(s.rect)
#            screen.blit(s.image, world_to_screen(self.rect.topleft))


class Platform(Block):
    """A platform you can walk on but also move through"""
    tiled_group = 'platforms'
    solid = False


door_img = [None, None, None]

class Door(Block):
    """A door you can walk into"""
    tiled_group = 'doors'
    solid = False

    def from_tiled_properties(self, props):
        next = props['next']
        next = [int(s) for s in next.split('.')]
        self.next = self.tiled_properties['next'] = next
        if not hasattr(self, "status"): self.status = 1

    def constrain(self, rect, moved):  # Don't constrain anything
        return 0,0
        
    def supporting(self, rect):  # Don't support anything
        return False

    def draw_image(self, screen, world_to_screen):
        # Status = 1: unbeaten   Status = 2: beaten
        if door_img[self.status] is None:
            f = gummworld2.data.filepath('image', "portal-%s.png" % ("beat" if self.status == 2 else "unbeat"))
            door_img[self.status] = pygame.image.load(f).convert_alpha()
        r = door_img[self.status].get_rect()
        r.center = world_to_screen(self.rect.center)
        screen.blit(door_img[self.status], r)
        

class MovingBlock(Block):
    """A platform you can jump on, and it takes you for a ride"""
    tiled_group = 'movingblocks'
    
    def __init__(self, *args, **kw):
        """MovingPlatform(x,y,w,h,kw)
        MovingPlatform(rect,kw)
        
        Keyword args:
            position: (x,y), the starting and home position
            xrange: (left,right), the range of motion on X-axis
            yrange: (top,bottom), the range of motion on the Y-axis
            speed: (dx,dy), the speed of motion in pixels per second
        """
        # Discern which usage...
        if isinstance(args[0], pygame.Rect):
            x,y,w,h = args[0]
        else:
            x,y,w,h = args
        # TODO: resolve multiple inheritance without brain asplodey
        Block.__init__(self,x,y,w,h)
        
        # take my sprites from map
#        self.take_map_tiles = True

        # TODO: get these from Tiled properties
        ## These are for procedural construction. The Level loader uses
        ## Block.from_tiled as its constructor, which calls in turn calls
        ## self.from_tiled_properties to set these attributes from the tiled
        ## properties. See overriding method later in this class.
        self.targets = [Vec2d(self.position)]
        self.next_target = 0

    def think_move(self, dt):
        if self.approach(self.get_target(), dt):
            self.next_target = (self.next_target + 1) % len(self.targets)

    def get_target(self):
        return self.targets[self.next_target]

    def approach(self, target, dt):
        """Returns True if I reach the target"""
        dist = self.v * dt
        dmove = target - self.position
        dnorm = dmove.get_length()
        if dnorm < dist:
            self.move(dmove)
            return True
        else:
            self.move(dmove * dist / dnorm)
            return False

    def from_tiled_properties(self, props):
        dxs = [int(a) for a in props.get('dx', '0').split(',')]
        dys = [int(a) for a in props.get('dy', '0').split(',')]
        for dx, dy in zip(dxs, dys):
            self.targets.append(self.position + (64*dx, 64*dy))
        self.v = float(props.get('v', '120.'))

    def vmargin(self):
        return 2 - self.moved.y        


    def draw_image(self, screen, world_to_screen):
        if self.solid:
            # I'm a block
            draw_block_helper(self, screen, world_to_screen)
        else:
            # I'm a platform
            draw_platform_helper(self, screen, world_to_screen)

class MovingPlatform(MovingBlock):
    tiled_group = 'movingplatforms'
    solid = False

class WeightedBlock(MovingBlock):
    """A platform you can jump on, and it sinks"""
    tiled_group = 'weightedblocks'
    
    def __init__(self, x, y, width, height):
        super(WeightedBlock, self).__init__(x, y, width, height)
        self.p0 = Vec2d(self.position)

    def think_move(self, dt):
        self.approach(self.get_target(), dt)

    def get_target(self):
        return self.p0 + (0, min(self.load(), self.sink_max) * 64)

    def from_tiled_properties(self, props):
        self.sink_max = int(props.get('sink_max', '1'))
        self.v = float(props.get('v', '400.'))

    def will_release(self):
        return (self.position - self.get_target()).get_length() < 5.

    def want_camera(self):
        return (self.position - self.get_target()).get_length() >= 5.

class WeightedPlatform(WeightedBlock):
    tiled_group = 'weightedplatforms'
    solid = False

class BalanceBlock(WeightedBlock):
    """A platform that's connected to a partner platform"""
    tiled_group = 'balanceblocks'
    
    def __init__(self, x, y, width, height):
        super(BalanceBlock, self).__init__(x, y, width, height)
        self.p0 = Vec2d(self.position)

    def find_partner(self, others):
        ps = [p for p in others if p is not self and hasattr(p, 'code') and p.code == self.code]
        assert len(ps) == 1, "Did not find exactly one partner %s %s" % (self.code, [p.code for p in others])
        self.partner = ps[0]

    def get_target(self):
        sinkage = max(min(self.load() - self.partner.load(), self.sink_max), -self.partner.sink_max)
        return self.p0 + (0, sinkage * 64)

    def from_tiled_properties(self, props):
        self.sink_max = int(props.get('sink_max', '1'))
        self.v = float(props.get('v', '400.'))
        self.code = props.get('code', "only")  # must have matching names so we can find partner

    
class BalancePlatform(BalanceBlock):
    tiled_group = 'balanceplatforms'
    solid = False


"""
Switches and Controlled Blocks

*   Switches are Blocks. They can be any size.
*   They have a value which is equal to the number of player/userblocks on top
    of them.
*   They control ControlledBlocks and ControlledPlatforms.
*   Every person that gets on the Switch will move the corresponding
    ControlledBlock by dx,dy, where dx and dy are properties of the
    ControlledBlock.
*   Switches and the blocks they control have to be associated by giving them
    both the same "code" property.
*   If you don't give a code, it will default to "only", so if you only have one
    switch in a level, you don't have to specify it.
*   Multiple switches can be associated with a ControlledBlock.
*   There are also NegativeSwitches, which move the block the opposite direction.
*   ControlledBlocks can also have max_move and min_move properties.
*   The v attribute is the speed it moves. If there's a platform whose motion
    you think people will miss, you can slow it down.
"""

class ControlledBlock(MovingBlock):
    """A block whose position is controlled by a switch"""
    tiled_group = "controlledblock"

    def __init__(self, x, y, width, height):
        super(ControlledBlock, self).__init__(x, y, width, height)
        self.p0 = Vec2d(self.position)

    def find_switch(self, switches):
        self.switches = [s for s in switches if hasattr(s, 'code') and s.code == self.code]
        assert len(self.switches) >= 1, "Did not find a controller %s %s" % (self.code, [s.code for s in switches])
        for switch in self.switches:
            switch.blocks.add(self)

    def think_move(self, dt):
        self.approach(self.get_target(), dt)

    def get_target(self):
        dist = max(min(sum(switch.value() for switch in self.switches), self.max_move), self.min_move)
        return self.p0 + (self.dx * dist * 64, self.dy * dist * 64)

    def from_tiled_properties(self, props):
        self.dx = int(props.get('dx', 0))
        self.dy = int(props.get('dy', 1))
        self.v = float(props.get('v', '240.'))
        self.max_move = int(props.get('max_move', 100))
        self.min_move = int(props.get('min_move', -100))
        self.code = props.get('code', "only")  # must have matching names so we can find switch

    def want_camera(self):
        return (self.position - self.get_target()).get_length() >= 5.

    def draw_image(self, screen, world_to_screen):
        MovingBlock.draw_image(self, screen, world_to_screen)
        if self.switches:
            color = self.switches[0].color
            key = (color, id(self))
            if key not in _image_cache:
                surf = pygame.Surface((24, 24))
                surf.fill(pygame.Color(color))
                _image_cache[key] = surf.convert()
            image = _image_cache[key]
            blit_rect = image.get_rect()
            blit_rect.center = world_to_screen(self.interp_position()) - (0,5)
            screen.blit(image, blit_rect)
    
class ControlledPlatform(ControlledBlock):
    tiled_group = 'controlledplatforms'
    solid = False

#    def draw_image(self, screen, world_to_screen):
#        if len(self.switches):
#            color = self.switches[0]
#        else:
#            color = Color('red')
#        image = self._get_image(color)
#        blit_rect = Rect(self.rect)
#        blit_rect.center = world_to_screen(self.rect.center)
#    
#    def _get_image(self, color):
#        w,h = 24,24
#        cache_key = (self.__class__.__name__, w, h, color)
#        if cache_key in _image_cache:
#            return image
#        image = pygame.surface.Surface((w,h))
#        image.fill(color)
#        _image_cache[cache_key] = image
#        return image

class Switch(Block):
    """A switch that can control block's position"""
    tiled_group = "switch"

    def from_tiled_properties(self, props):
#        self.dx = int(props.get('dx', 0))  # These should never be used
#        self.dy = int(props.get('dy', 1))
#        self.v = float(props.get('v', '240.'))
        self.code = props.get('code', "only")
        self.direction = str(props.get('direction', "right"))
        self.color = str(props.get('color', "#ff0000")).strip()
        self.blocks = set()  # Don't need to keep track of the switches I control, this is just in case.

    def value(self):
        return self.load()
        
    def draw_image(self, screen, world_to_screen):
        draw_switch_image(self, screen, world_to_screen, make_switch_image)

class NegativeSwitch(Switch):
    """A switch sends a block in the opposite direction"""
    tiled_group = "negativeswitch"

    def value(self):
        return -self.load()


def load_images(name, filename, tile_size, alpha=True):
    tiles = []
    fn = data.filepath('map', filename)
    ss = spritesheet.SpriteSheet(fn, convert_alpha=alpha)
    
    img_size = Vec2d(ss.sheet.get_rect().size)
    tile_size = Vec2d(tile_size)
    colorkey = ss.sheet.get_at((0,0))
    clip_rect = Rect(0,0,*tile_size)
    
    for y in range(0, img_size.y, tile_size.y):
        row = []
        tiles.append(row)
        for x in range(0, img_size.x, tile_size.x):
            clip_rect.topleft = (x,y)
            #print (y,x),clip_rect
            img = ss.image_at(clip_rect) #, colorkey=colorkey)
#            print img.get_flags(),img.get_flags()&SRCALPHA; quit()
            row.append(img)
            #assert tiles[y/64][x/64] is img
    
    _spritesheet_cache[name] = tiles

class Struct(object):
    def __init__(self, **kw):
        self.__dict__.update(kw)

def get_image(name, row, col):
    """Get image by name and position on the tilesheet. Tilesheet position
    starts at (col,row)=(1,1) in the upper left corner.
    """
    return _spritesheet_cache[name][row-1][col-1]

def build_tilesets():
    """tile positions start at 1,1 in topleft corner"""
    name = 'grassy'
    _tilesets['grassy_block'] = Struct(
        topleft     = get_image(name, 1,1),
        top         = get_image(name, 1,2),
        topright    = get_image(name, 1,6),
        lefttop     = get_image(name, 2,1),
        left        = get_image(name, 3,1),
        centertop   = get_image(name, 2,2),
        center      = get_image(name, 3,7),
        righttop    = get_image(name, 2,6),
        right       = get_image(name, 3,6),
        bottomleft  = get_image(name, 3,1),
        bottom      = get_image(name, 3,2),
        bottomright = get_image(name, 3,6),
    )
    _tilesets['grassy_platform'] = Struct(
        topleft     = get_image(name, 4,5),
        top         = get_image(name, 4,6),
        topright    = get_image(name, 4,7),
        bottomleft  = get_image(name, 5,5),
        bottom      = get_image(name, 5,6),
        bottomright = get_image(name, 5,7),
    )
    _tilesets['switch'] = Struct(
        down        = get_image(name, 1,8),
        topright    = get_image(name, 2,8),
        up          = get_image(name, 3,8),
        topleft     = get_image(name, 4,8),
        left        = get_image(name, 5,8),
        bottomleft  = get_image(name, 6,8),
        right       = get_image(name, 7,8),
        bottomright = get_image(name, 8,8),
    )
    

def make_block_image(size, tileset_name):
    w,h = size
    key = (tileset_name,w,h)
    #print 'size,key',size,key
    if key in _tilesets:
        #print 'cache hit'
        return _tilesets[key].copy()
    #print 'cache miss'
    
    fullsize = Vec2d(size) + (2,2)
    img = pygame.surface.Surface(fullsize*64, SRCALPHA)
    fullsize.y -= 1
    #print 'fullsize,img',fullsize,img
    
    corners = {
        (0,0) : 'topleft',
        (0,fullsize.x-1) : 'topright',
        (fullsize.y-1,0) : 'bottomleft',
        (fullsize.y-1,fullsize.x-1) : 'bottomright',
    }
    ysides = {
        0 : 'top',
        fullsize.y-1 : 'bottom',
    }
    xsides = {
        0 : 'left',
        fullsize.x-1 : 'right',
    }
    tileset = _tilesets[tileset_name]
    for y in range(fullsize.y):
        for x in range(fullsize.x):
            pos = (y,x)
            if pos in corners:
                attr = corners[pos]
            elif y in ysides:
                attr = ysides[y]
            elif x in xsides:
                attr = xsides[x]
            elif pos == (1,0):
                attr = 'lefttop'
            elif pos == (1,fullsize.x-1):
                attr == 'righttop'
            elif y == 1:
                attr = 'centertop'
            else:
                attr = 'center'
            #print 'row,col',(y,x),attr
            src = getattr(tileset, attr)
            #print 'blit',(x,y),(x*64,y*64)
            img.blit(src, (x*64,y*64))
    _tilesets[key] = img
    return img

def make_platform_image(size, tileset_name):
    w,h = size
    key = (tileset_name,w,h)
    #print 'size,key',size,key
    if key in _tilesets:
        #print 'cache hit'
        return _tilesets[key].copy()
    #print 'cache miss'
    
    fullsize = Vec2d(size) + (2,2)
    img = pygame.surface.Surface(fullsize*64, SRCALPHA)
    fullsize.y -= 1
    #print 'fullsize,img',fullsize,img
    
    corners = {
        (0,0) : 'topleft',
        (0,fullsize.x-1) : 'topright',
        (fullsize.y-1,0) : 'bottomleft',
        (fullsize.y-1,fullsize.x-1) : 'bottomright',
    }
    ysides = {
        0 : 'top',
        fullsize.y-1 : 'bottom',
    }
    tileset = _tilesets[tileset_name]
    for y in range(fullsize.y):
        for x in range(fullsize.x):
            pos = (y,x)
            if pos in corners:
                attr = corners[pos]
            elif y in ysides:
                attr = ysides[y]
            #print 'row,col',(y,x),attr
            src = getattr(tileset, attr)
            #print 'blit',(x,y),(x*64,y*64)
            img.blit(src, (x*64,y*64))
    _tilesets[key] = img
    return img
    
def make_switch_image(switch, tileset_name):
    # img = pygame.Surface((64, 64))
    # img.fill(pygame.Color(switch.color))
    # return img
    if __debug__: print "making image for switch", switch.code, switch.color, switch.direction
    images = _tilesets[tileset_name]
    if hasattr(images, switch.direction):
        img = getattr(images, switch.direction).copy()
        surf = pygame.Surface(img.get_size(), SRCALPHA)
        surf.fill(pygame.Color(switch.color))
        surf.blit(img, (0,0))
        surf.set_colorkey(None)
        surf.set_alpha(None)
        surf.set_colorkey(surf.get_at((1,1)))
        if __debug__: print "??", surf.get_at((1,1))
        return surf.convert()
    if __debug__: assert False, "ERROR: switch image not found %s" % (switch.code, switch.direction, switch.color)
    
def draw_switch_image(switch, screen, world_to_screen, make_switch_image, cache={}):
    direction = switch.direction
    color = switch.color
    tileset_name = 'switch'
    key = (tileset_name, direction, color)
    if key not in cache:
        cache[key] = make_switch_image(switch, tileset_name)
    # if __debug__: print 'switch draw', switch.code, switch.color, switch.direction, switch.rect, len(switch.color)#, id(cache[key])
    rect = switch.rect
    rect.center = world_to_screen(switch.position)
    screen.blit(cache[key], rect.move(0,-64))

def draw_block_helper(self, screen, world_to_screen, cache = {}):
    draw_helper(self, screen, world_to_screen,
        make_block_image, 'grassy_block', cache = {})

def draw_platform_helper(self, screen, world_to_screen, cache = {}):
    draw_helper(self, screen, world_to_screen,
        make_platform_image, 'grassy_platform', cache = {})

def draw_helper(self, screen, world_to_screen, make, name, cache = {}):
    key = self.rect.size
    if key not in cache:
        cache[key] = make(Vec2d(self.rect.size)/64, name)
    image = cache[key]
    blit_rect = image.get_rect()
    rect = blit_rect
    rect.center = world_to_screen(self.interp_position()) - (0,5)
    screen.blit(image, rect)

_spritesheet_cache = {} # {name:[surf,]}
_tilesets = {}          # key: name
_image_cache = {}       # key: (name,w,h)

def init():
    ## step 1: auto-load each tilesheet with load_images()
    ## step 2: add your struct to build_tileset()
    load_images('grassy', 'tileset.png', (64,64))

    ## this builds the structs
    build_tilesets()

    ## To build a platform image, get the struct:
    ##      tileset_name = 'grassy_block'
    ##      width = 2
    ##      height = 1
    ##      image = make_block_image((width,height), tileset_name)


if __name__ == '__main__':
    from pygame.locals import *
    from gummworld2 import State, Screen, Camera, model, GameClock
    pygame.init()
    scr = State.screen = Screen((400,400))
    cam = State.camera = Camera(model.Object())
    State.clock = GameClock(25,0)
    init()
    
#    # This works. _spritesheet_cache is blessed.
#    cache = _spritesheet_cache['grassy']
#    while 1:
#        pygame.time.wait(1000)
#        pygame.event.get()
#        scr.clear()
#        for row in range(512/64):
#            for col in range(512/64):
#                img = cache[row][col]
#                y = row * 64
#                x = col * 64
#                print (row,col),(y,x)
#                scr.surface.blit(img, (x,y))
#        scr.flip()

#    # This works. get_image() is blessed.
#    cache = _spritesheet_cache['grassy']
#    for row in range(512/64):
#        for col in range(512/64):
#            img1 = cache[row][col]
#            img2 = get_image('grassy', row+1,col+1)
#            assert img1 is img2

#    # This works. _tilesets is blessed.
#    cache = _spritesheet_cache['grassy']
#    ts = _tilesets['grassy_block']
#    assert ts.topleft is cache[0][2]
#    assert ts.topleft is get_image('grassy', 1,3)
#    assert ts.bottomleft is cache[5][0]
#    assert ts.bottomleft is get_image('grassy', 6,1)
    
    img = make_block_image((2,1), 'grassy_block')
    img = make_block_image((2,1), 'grassy_block')
    sizes = (
        (1,1), (2,1), (3,1), (2,1), (2,2), (2,3),
    )
    for size in sizes:
        print '---',size
        pygame.time.wait(1000)
        pygame.event.get()
        pygame.display.set_caption(str(size))
        img = make_block_image(size, 'grassy_block')
        scr.clear()
        scr.surface.blit(img, (100,100))
        pygame.draw.rect(scr.surface, (255,0,0), img.get_rect(topleft=(100,100)), 1)
        scr.flip()
