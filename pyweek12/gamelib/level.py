import os

import pygame
from pygame.locals import *

import gummworld2
from gummworld2.tiledtmxloader import TileMapParser, ImageLoaderPygame
from gummworld2 import State, geometry, Vec2d, data

import settings, world, camera_target, block, entity, player, fonts, hud, sound
from level_info import LevelInfo

# ----------------------------------------------------------------------------
class Struct(dict):
    def __init__(self, **kw):
        self.__dict__.update(**kw)

# ----------------------------------------------------------------------------
class _StateBase(object):

    @staticmethod
    def enter(self):
        pass

    @staticmethod
    def exit(self):
        pass
    
    @staticmethod
    def intro_start(self):
        pass
    
    @staticmethod
    def intro_conclude(self):
        pass
    
    @staticmethod
    def ready(self):
        pass

    @staticmethod
    def think(self, dt):
        pass

# ----------------------------------------------------------------------------
class _StateEntering(_StateBase):

    @staticmethod
    def enter(self):
        sound.play_sfx("magicdoor")
        print "MAGICDOOR"
        elapsed = 0.0
        pause = 0.
        speed = gummworld2.State.clock.ticks_per_second * 25.0
        spawn = gummworld2.State.camera.rect.center
        exit = self.endpoint
        self.intro_data = Struct(
            elapsed=elapsed,
            pause=pause,
            speed=speed,
            spawn=spawn,
            exit=exit,
            position=Vec2d(spawn),
        )
        
        # Steal tiles from the map for objects that require it.
        for objects in self.objects.values():
            for obj in objects:
                ## the trigger...
                if hasattr(obj, 'take_map_tiles') and obj.take_map_tiles:
                    obj.make_sprites_from_tiles()


    @staticmethod
    def think(self, dt):
        intro_data = self.intro_data
        intro_data.elapsed += dt
        if intro_data.elapsed >= intro_data.pause:
            return _StateIntroStart if self.endpoint else _StateReady

# ----------------------------------------------------------------------------
class _StateIntroStart(_StateBase):
    @staticmethod
    def enter(self):
        intro_data = self.intro_data
        intro_data.elapsed = 0.0
    
    @staticmethod
    def think(self, dt):
        intro_data = self.intro_data
        intro_data.elapsed += dt
        position = intro_data.position
        exit = intro_data.exit
        if position == exit:
            return _StateIntroConclude
        else:
            radius = intro_data.speed * dt
            if geometry.distance(position, exit) < radius:
                new_pos = exit
            else:
                angle = geometry.angle_of(position, exit)
                new_pos = geometry.point_on_circumference(position, radius, angle)
            position.x,position.y = new_pos

# ----------------------------------------------------------------------------
class _StateIntroConclude(_StateBase):
    @staticmethod
    def enter(self):
        intro_data = self.intro_data
        intro_data.elapsed = 0.0
        intro_data.pause = 0.25
        intro_data.speed *= 2.5
    
    @staticmethod
    def think(self, dt):
        intro_data = self.intro_data
        intro_data.elapsed += dt
        position = intro_data.position
        spawn = intro_data.spawn
        if intro_data.elapsed < intro_data.pause:
            return
        elif position == spawn:
            return _StateReady
        else:
            radius = intro_data.speed * dt
            if geometry.distance(position, spawn) < radius:
                new_pos = spawn
            else:
                angle = geometry.angle_of(position, spawn)
                new_pos = geometry.point_on_circumference(position, radius, angle)
            position.x,position.y = new_pos

# ----------------------------------------------------------------------------
class _StateReady(_StateBase):
    @staticmethod
    def enter(self):
        if self.world_num == 0:
            level_data = (self.world_num,self.level_num,self.name,
                os.path.split(self.tmx_file)[1])
            lvdb = LevelInfo(settings.save_file)
            lvdb.add(level_data)
            lvdb.save()    

    @staticmethod
    def think(self, dt):
        pass
# ----------------------------------------------------------------------------

class _StateWon(_StateBase):
    @staticmethod
    def enter(self):
        # Save completion data for lobbies and real levels only.
        if self.world_num > 0:
            level_data = (self.world_num,self.level_num,self.name,
                os.path.split(self.tmx_file)[1])
            # Load save file. Check if save data is already saved.
            lvdb = LevelInfo(settings.save_file)
            lvdb.add(level_data)
            lvdb.save()
    
    @staticmethod
    def think(self, dt):
        pass

# ----------------------------------------------------------------------------
# class _StateLost(_StateBase):
    # @staticmethod
    # def enter(self):
        # pass
    
    # @staticmethod
    # def think(self, dt):
        # pass

# ----------------------------------------------------------------------------
class Level(object):
    
    def __init__(self, world_source, skip_intro=False):
        """world_source is either a str(tmx_file), or (world_num,level_num).
        """
        State.level = self
        
        screen_size = State.screen.size
        if screen_size.y <= 480:
            font_size = 14
        else:
            font_size = 22
        self.font = pygame.font.Font(data.filepath('font','nvvzs.ttf'), font_size)
        self.text_cache = {}
        
        wnum = lnum = 0
        name = tmx = ''
        lvdb = LevelInfo(settings.level_info_file)
        if isinstance(world_source, str):
            tmx = os.path.split(world_source)[1]
            for name in lvdb.names:
                ent = lvdb.info_by_name(name)
                if ent[3] == tmx:
                    wnum,lnum = ent[0:2]
                    break
        else:
            wnum,lnum,name,tmx = lvdb.info_by_number(world_source)
        self.name = name
        self.level_id = wnum,lnum
        self.world_num = wnum
        self.level_num = lnum
        self.tmx_file = gummworld2.data.filepath('map',tmx)
        
        def calc_lives(wnum):
            lvdb = LevelInfo(settings.save_file)
            lvhigh = lvdb.high()
            #print lvhigh
            if wnum < 0:
                # test level, full life
                return settings.player_lives
            elif wnum == 0:
                # in the lobby, don't need lives
                #print 'lobby'
                return 0
            elif lvhigh is None or lvhigh[0] == 0:
                # haven't played through tuts, start at 0
                #print 'nub'
                return 0
            elif lvhigh[0] == 1:
                # still in tuts, start at highest level
                #print 'novice'
                return lvhigh[1]
            else:
                # veteran, full number of lives
                #print 'vet'
                return settings.player_lives
        self.player_lives = calc_lives(self.world_num)
        #print self.player_lives
        self.player_stones = 0
        
        State.map = gummworld2.toolkit.load_tiled_tmx_map(self.tmx_file)
        State.world = world.World(State.map.rect)

        self.entity_tree = entity.EntityTree()
        self.objects = {}  # {name: [objs]}
        self.load_objects()
        
        target = camera_target.CameraTarget(Rect(0,0,10,10))
        State.camera = gummworld2.Camera(target)
        # Start camera at bottom left of map. Knock camera in bounds...
        State.camera.rect.center = self.spawnpoint
        State.camera.rect.clamp_ip(State.world.rect)
        target.position = State.camera.rect.center
        self.camera_wanter = None  # Any objects I know of that want the camera.
        
        self.test_rect = Rect(State.camera.rect)
        
        # This will save the camera, world, world_type, and map. They can be
        # reloaded by gummworld2.State.restore(self.tmx_file).
        gummworld2.State.save(self.tmx_file)
        
        self.current_state = _StateReady if skip_intro else _StateEntering
        self.current_state.enter(self)
        
        State.hud = hud.HUD()
        State.show_hud = settings.show_hud
        
        # Save original and collapsed state objects. We'll need to swap in the
        # original map with grid lines for Level Scan.
        for attr in 'level','hud':
            if attr not in gummworld2.state._default_attrs:
                gummworld2.state._default_attrs.append(attr)
        self.state_name_orig = 'orig '+self.name
        State.save(self.state_name_orig)
        # State.map = gummworld2.toolkit.collapse_map(State.map, (10,10))
        self.state_name_collapsed = 'collapsed '+self.name
        State.save(self.state_name_collapsed)
        
        # Control toolkit spam.
        State.report_draw_tiles = settings.report_draw_tiles

    def set_state(self, new_state):
        if __debug__:
            if self.current_state:
                print self.__class__.__name__ + str(id(self)), self.current_state.__name__, " => ", new_state.__name__
            else:
                print self.__class__.__name__ + str(id(self)), "None", " => ", new_state.__name__
        if self.current_state:
            self.current_state.exit(self)
        new_state.enter(self)
        self.current_state = new_state

    def getstate(self):
        objsets = dict((name, list(objs)) for name, objs in self.objects.items())
        poslog = [(obj, Vec2d(obj.position)) for objs in objsets.values() for obj in objs]
        return objsets, poslog, self.current_state
    
    def setstate(self, state):
        self.objects, poslog, self.current_state = state
        for obj, pos in poslog:
            obj.set_position(pos)
        self.player = self.objects['player'][0]
    
    def is_ready(self):
        return self.current_state is _StateReady
    
    def won(self):
        self.set_state(_StateWon)
    
    def lost(self):
        self.player.fallen_to_death()
    
    def think(self, dt):
        state = self.current_state.think(self, dt)
        if state:
            self.set_state(state)
        self.set_colliders()  # TODO: does this need to be called every frame?
        self.entity_tree.think(dt)
        if settings.collideafter:
            self.entity_tree.handle_collisions()
#        print self.player.current_state, self.player.parent

    def draw(self, screen):
        # gummworld2.toolkit.draw_tiles()
        camera = State.camera
        world_to_screen = camera.world_to_screen
        blit = screen.blit
        class WorldLayer:
            def __init__(self, layer):
                self.layer = layer
                self.parallax = 1 if self.layer > 0 else 0.5
            def draw(self, screen, world_to_screen):
                gummworld2.toolkit.draw_tiles_of_layer(self.layer, self.parallax, self.parallax)

        allobjs = [obj for objects in self.objects.values() for obj in objects]
        allobjs += [WorldLayer(j) for j in (0,1,2,3)]
        allobjs.sort(key = lambda o: o.layer)

        for obj in allobjs:
            obj.draw(screen, world_to_screen)

    def add_object(self, object):
        group_name = object.tiled_group
        object.name = group_name
        group = self.objects.get(group_name, [])
        group.append(object)
        self.objects[group_name] = group
        if isinstance(object, entity.Entity):
            object.set_tree(self.entity_tree)

    def load_objects(self):

        # Start with left and right walls
        x,y,w,h = gummworld2.State.map.rect
        self.add_object(block.Block(x-64,y,64,h))
        self.add_object(block.Block(x+w,y,64,h))

        # Get saved game state so I know which doors to show
        lvdb = LevelInfo(settings.save_file)

        groups = gummworld2.State.map.tiled_map.object_groups
        for group in gummworld2.State.map.tiled_map.object_groups:
            color = getattr(group, 'color', '#ffffff')
            if '.' in group.name: # if name is a module path
                module, clsname = group.name.split('.')
                ##module = '.'.join(['gamelib', module])
                cls = getattr(__import__(module, globals(), locals(), [clsname]), clsname)
                for obj in group.objects:
                    # Create object instance
                    rect = cls.from_tiled(obj, color)
                    # Add it to level
                    if "next" in obj.properties:
                        world,level = obj.properties["next"].split(".")
                        status = lvdb.level_status((int(world),int(level)))
                        if not status:  # Don't show doors/indicators to places we can't go
                            continue
                        rect.status = status
                    self.add_object(rect)
            else: # Handle generic rect types
                for obj in group.objects:
                    if __debug__:
                        print "Loading generic", group.name, obj.x, obj.y, obj.width, obj.height
                    rect = entity.Entity(obj.x, obj.y, obj.width, obj.height)
                    for prop in obj.properties:
                        setattr(rect, prop, obj.properties[prop])
                    if "next" in obj.properties:
                        world,level = obj.properties["next"].split(".")
                        status = lvdb.level_status((int(world),int(level)))
                        if not status:  # Don't show doors/indicators to places we can't go
                            continue
                    rect.color = color
                    rect.tiled_group = group.name
                    self.add_object(rect)

        for key, obj in self.objects.items():
            gummworld2.State.world.add_list(obj)

        allobjs = [obj for group in self.objects.values() for obj in group]

        balances = [obj for obj in allobjs if isinstance(obj, block.BalanceBlock)]
        switches = [obj for obj in allobjs if isinstance(obj, block.Switch)]
        cons = [obj for obj in allobjs if isinstance(obj, block.ControlledBlock)]
        for b in balances: b.find_partner(balances)
        for c in cons: c.find_switch(switches)
        
        lvdb = LevelInfo(settings.level_info_file)
        if 'signs' in self.objects:
            for sign in self.objects['signs']:
                if hasattr(sign, 'next'):
                    wn,ln = [int(s) for s in sign.next.split('.')]
                    lvent = lvdb.info_by_number(wn,ln)
                    sign.message = lvent[2]
                elif not hasattr(sign, "message"):
                    sign.message = "..."
                self.text_cache[sign.message] = self.render_sign(sign.message)

        self.spawnpoint = self.objects['start'][0].rect.center
        self.endpoint = self.objects['end'][0].rect.center if 'end' in self.objects else None
        if __debug__:
            print "startpoint", self.objects['start'][0].rect

        self.player = player.Player(self.spawnpoint)  # Two references for easy... reference
        self.objects["player"] = [self.player]
        self.player.set_tree(self.entity_tree)

    def render_sign(self, message):
        texts = []
        while message:
            r = message.find(" ", 50)
            if r == -1:
                texts.append(message)
                message = ''
            else:
                texts.append(message[:r])
                message = message[r:].strip()
        imgs = [self.font.render(text.strip(), True, Color('yellow')).convert_alpha() for text in texts]
        w = max(img.get_width() for img in imgs)
        h = 0
        img = pygame.surface.Surface((w, 32*len(imgs))).convert_alpha()
        img.fill((0,0,0,0))
        for i in imgs:
            r = i.get_rect()
            r.midtop = w/2,h
            img.blit(i,r)
            h += 32
        return img

    def set_colliders(self):
        # TODO: only do the nearby entities
#        collision_types = self.player.collision_types
#        collision_groups = [self.objects[coltype] for coltype in collision_types if coltype in self.objects]
        collision_objects = [obj for group in self.objects.values() for obj in group if isinstance(obj, block.Block)]
        for objset in self.objects.values():
            for obj in objset:
                if isinstance(obj, entity.AttachableEntity):
                    obj.set_colliders(list(collision_objects))

    def at_endpoint(self, rect):
        return rect.collidepoint(self.endpoint) if self.endpoint else False
        
    def look_target(self):
        if self.camera_wanter:
            if self.camera_wanter.want_camera():
                return self.camera_wanter.position
            else:
                self.camera_wanter = None

        for group in self.objects.values():
            for obj in group:
                if obj.want_camera():
                    if self.camera_wanter is None:
                        sound.play_sfx("stone2")
                    self.camera_wanter = obj
                    if settings.allow_camera_stealing and not gummworld2.State.camera.rect.collidepoint(obj.position):
                        return obj.position

        return self.player.look_target()

