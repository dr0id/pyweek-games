
import math
from math import floor
from math import sin

import pygame
from pygame import *

import context, settings
import fonts

import data,os

import gummworld2
import transition


class CreditsContext(context.Context):

    

    # members = ["Cosmologicon", "DR0ID", "Stabbingfinger", "ldlework", "Nitrofurano", "Kiddo"]
    members = [     "PRODUCER & LEVELDESIGN;Christopher Night (Cosmologicon);<cosmologicon@gmail.com>", 
                    "LEAD CODER;DR0ID",
                    "LEAD CODER;Gummbum <stabbingfinger@gmail.com>",
                    "CODING SUPPORT;ldlework <dlacewell@gmail.com>",
                    "FONTS & ART;Nitrofurano <nitrofurano@gmail.com>",
                    "ART;Kiddo",
                    "MUSIC & SOUND;JDruid",
                    "TOOLS;python, pygame, Notepad++;Tiled, Paint.NET, Inkscape;DrPython, Gummworld2",
                    "PYWEEK 12, April 2011;(C) Multiverse Factory team",
                    ]

    def __init__(self):
        context.Context.__init__(self)

        screen_size = settings.resolution
        self.sprites = []
        if screen_size[1] <= 480:
            font_size = 18
            self.dddd = -0.350
        else:
            font_size = 38
            self.dddd = -0.175

        font_file = data.filepath(os.path.join('font','nvvzs.ttf'))
        font = pygame.font.Font(font_file, font_size)

        #font = pygame.font.Font(None, font_size) <- please avoid 'None' as typeface

        screen_rect = pygame.Rect((0, 0), screen_size)
        posx = screen_rect.centerx
        dy = screen_rect.height / (len(self.members) + 1)
        dy = 360.0 / (len(self.members) + 1)
        posy = dy + 190 # startoffset
        for member in reversed(self.members): # yeah, it rotates the other way around 
            texts = member.split(";")
            for idx, text in enumerate(texts):
                spr = pygame.sprite.Sprite()
                img = fonts.render_with_border(text, font,
#                    pygame.Color('#F6BB21'), pygame.Color('#000000'), pygame.Color('#B53841'), 2)
                    pygame.Color('yellow'), pygame.Color('black'), pygame.Color('red'))
                spr.image = img
                spr.rect = spr.image.get_rect(center=(posx, posy))
                spr.x = spr.rect.centerx
                spr.y = spr.rect.top
                spr.prev_pos = spr.y # save old position
                spr.offset = posy + font_size * self.dddd * idx
                self.sprites.append(spr)
                spr.scale = 1.0
                spr.angle = posy
            posy += dy
        self.time = 0.0
        self.speed = 1.0

    def think(self, dt):
        ## This causes a little bit of pixel skip. Removing it *really* smooths
        ## out the motion.
        ##pygame.time.wait(40)
        for e in pygame.event.get():
            if e.type == KEYDOWN:
                if e.key in settings.keys.quit:
                    self.do_leave()
            elif e.type == QUIT:
                self.do_leave()
        self.time += self.speed * dt
        screen_rect = pygame.Rect((0,0), settings.resolution)
        radius = (screen_rect.height - 100) / 2.0
        speed = 1.0
        # print self.speed
        for sprite in self.sprites:
            sprite.prev_pos = sprite.y # save old position
            angle = (self.time * 25.0 + sprite.offset) % 360.0 
            # print angle
            if angle > 175 and angle < 182: # slow down window
                speed = 0.2
            val = sin(math.radians(angle))
            posy = screen_rect.height / 2.0 + radius * val
            sprite.y = posy
            sprite.angle = angle
            sprite.scale = val
            sprite.layer = -1 if sprite.angle < 90 or sprite.angle > 270 else 1
        self.sprites.sort(key=lambda spr: spr.layer)
        self.speed = speed

    def do_leave(self):
        context.push(transition.EscapePopTransition(transition.FadeInEffect(settings.fade_duration)))
        
    def draw(self, gummworld_screen):
        """Refresh the screen"""
        
        screen = gummworld_screen.surface
        # screen.fill(0x66859E)
        screen.fill(0x000000)

        screen_rect = screen.get_rect()
        interp = gummworld2.State.clock.interpolate() 
        ## The following block resolves Camera jitter during Transition.
        if self != context.top():
            interp = 1.0
        interp = min(interp, 1.0)
        depth_factor = 0.75
        for spr in self.sprites:
            scale = (1.0 - abs(spr.scale))
            w, h = spr.rect.size
            h *= scale
            img = spr.image
            alpha = 255
            if spr.angle < 90.0 or spr.angle > 270.0: # behind part
                img = pygame.transform.flip(img, False, True)
                h *= depth_factor
                w *= depth_factor
                alpha = 128
            h = h if h > 1 else 1 # at least one pixel should be  visible
            img = pygame.transform.smoothscale(img, (int(w+0.5), int(h+0.5)))
            rect = img.get_rect()
            rect.centerx = screen_rect.centerx
            y = spr.prev_pos + (spr.y - spr.prev_pos) * interp
            img = self._generate(img, 0, (y - floor(y)))
            img.set_alpha(alpha)
            img.set_colorkey((0,0,0))
            screen.blit(img, (rect.left, y))
        # pygame.display.flip()
    
    def suspend(self):
        """Called when another context is pushed on top of this one."""
        pass
    def resume(self):
        """Called when another context is popped off the top of this one."""
        pass
    def enter(self):
        """Called when this context is pushed onto the stack."""
        self.think(0.0) # to set all values for rendering
    def exit(self):
        """Called when this context is popped off the stack."""
        pass
        
    @staticmethod
    def _generate(s, frac_x, frac_y):
        # frac_x, frac_y = frac_y, frac_x
        frac_x = 1. - frac_x        
        
        
        # sa = int( (1.-frac_x) * (1.-frac_y) * 255. ) 
        # sb = int( (1.-frac_x) * frac_y * 255. )
        sc = int( frac_x * (1.-frac_y) * 255. )
        sd = int( (frac_x * frac_y) * 255. )
        
        w, h = s.get_size()
        surf = pygame.Surface((w, h+1))#, pygame.SRCALPHA)
        # surf.fill((0,0,0,0))
        
        s.set_alpha(sc)
        surf.blit(s, (0, 0))
        surf.blit(s, (0, 0))
        # surf.blit(s, (0, 0))
        
        # s.set_alpha(sa)
        # surf.blit(s, (1, 0))
        # surf.blit(s, (1, 0))
        # # surf.blit(s, (1, 0))
        
        s.set_alpha(sd)
        surf.blit(s, (0, 1))
        surf.blit(s, (0, 1))
        # surf.blit(s, (0, 1))
        
        # s.set_alpha(sb)
        # surf.blit(s, (1, 1))
        # surf.blit(s, (1, 1))
        # # surf.blit(s, (1, 1))
        
        
        return surf
        
        
