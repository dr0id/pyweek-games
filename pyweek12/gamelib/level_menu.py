import pygame
from pygame.locals import *

import menu, context, level_router, game, settings, transition
from level_info import LevelInfo

class LevelMenuContext(menu.MenuContext):
    
    def __init__(self):
        super(LevelMenuContext, self).__init__(align='center')
        self.init()
    
    def init(self):
        del self.items[:]
        del self.text_list[:]
        self.max_width = 0
        lvdb = LevelInfo(settings.level_info_file)
        for name in lvdb.names:
            self.add(name)
    
    def resume(self):
        self.init()
    
    def think(self, dt):
        pygame.time.wait(40)
        for e in pygame.event.get():
            if e.type == KEYDOWN:
                if e.key in settings.keys.quit:
                    self.pop()
                elif e.key in settings.keys.up:
                    self.move_up()
                elif e.key in settings.keys.down:
                    self.move_down()
                elif e.key in settings.keys.select:
                    selected = self.get_selected()
                    lvdb = LevelInfo(settings.level_info_file)
                    wnum,lnum,name,tmx_file = lvdb.info_by_name(selected)
                    if tmx_file.endswith('.pickle'):
                        self.push(game.GameContext(tmx_file))
                    else:
                        self.push(level_router.LevelRouter(
                            game.GameContext, (wnum,lnum)))
            elif e.type == QUIT:
                self.pop()

    def push(self, new_con):
        # context.push(transition.FadeInTranition(self, new_con, settings.fade_duration, False))
        context.push(transition.EscapePushTranition(new_con, transition.FadeInEffect(settings.fade_duration)))
        
    def pop(self):
        # context.pop()
        # context.push(transition.FadeOutTranition(self, context.top(), settings.fade_duration))
        context.push(transition.EscapePopTranition(transition.FadeInEffect(settings.fade_duration)))



if __name__ == '__main__':
    import gummworld2
    pygame.init()
    scr = gummworld2.State.screen = gummworld2.Screen((900,700))
    context.push(LevelMenuContext())
    while context.top():
        pygame.time.wait(40)
        scr.clear()
        context.top().think(0)
        if context.top():
            context.top().draw(scr)
        scr.flip()
