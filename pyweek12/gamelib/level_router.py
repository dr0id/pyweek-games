import os

from gummworld2 import State, data

import context, level_info, settings, game, cutscene, pause_menu

class LevelRouter(context.Context):
    
    def __init__(self, cls, *args):
        context.Context.__init__(self)
        self.cls = cls
        self.args = args
        
    def think(self, dt):
        cls = self.cls
        args = self.args
        
        # Pop this context.
        context.pop()
        
        # Route the next context.
        if cls is game.GameContext:
            end_game = False
            if end_game:
                if __debug__: print '%s: %s' % (self.__class__.__name__,'END GAME')
                ## TODO: End game sequence
                context.push(CutScene('99-99.txt'))
            else:
                arg = args[0]
                new_game_context = game.GameContext(*args)
                if isinstance(arg, str):
                    if __debug__: print '%s: %s: %s' % (self.__class__.__name__,'STRING CASE', arg)
                    context.push(new_game_context)
                else:
                    wn,ln = arg
                    lvdb = level_info.LevelInfo(settings.save_file)
                    if wn < 0:
                        wn,ln = 0,0
                    filename = '%d-%d.txt' % (wn,ln)
#                    if os.access(data.filepath('cutscene', filename), os.R_OK) and not lvdb.info_by_number(wn,ln):
                    if wn==0 and not lvdb.info_by_number(wn,ln):
                        if __debug__: print '%s: %s: %s' % (self.__class__.__name__,'CUT SCENE', filename)
                        context.push(cutscene.CutScene(filename, new_game_context))
                    else:
                        if __debug__: print '%s: %s: %s' % (self.__class__.__name__,'DEFAULT', str(arg))
                        context.push(new_game_context)
        elif cls is pause_menu.PauseMenuContext:
            # Act lobbies don't post pause menu.
            worldn,leveln = State.level.level_id
            is_act_lobby = worldn == 0 and leveln != 0
            if is_act_lobby:
                context.pop()
                context.push(game.GameContext((0,0)))
            else:
                context.push(pause_menu.PauseMenuContext()) 
