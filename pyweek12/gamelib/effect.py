import pygame, gummworld2, random

imageCache = {}

def draw_glowy_bit(screen, pos):
    if "glowy_bit" not in imageCache:
        img = pygame.image.load(gummworld2.data.filepath('image', "glowy-bit.png")).convert_alpha()
        img = pygame.transform.scale2x(img)
        imageCache["glowy_bit"] = [pygame.transform.rotate(img, 90*j) for j in range(4)]
    imgs = imageCache["glowy_bit"]
    img = random.choice(imgs)
    r = img.get_rect()
    r.center = pos
    screen.blit(img, r)


