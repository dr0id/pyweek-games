# -*- coding: utf-8 -*-

import os

import pygame


from gummworld2 import data, State, View
import context
import settings
import transition

class CutScene(context.Context):
    """load a file from data.cutscene
    file format
    
    """
    def __init__(self, filename, new_context=None):
        self.filename = data.filepath('cutscene', filename)
        self.content = [] # [[text,]]
        
        self.new_context = new_context
        
        with open(self.filename, 'r') as file:
            for line in file.readlines():
                words = line.split(' ')
                self.content.append(words)
        
        self.lindex = 0
        self.windex = 0
        
        screen_size = State.screen.size
        rect = State.screen.rect.inflate(screen_size*-.2)
        self.view = View(State.screen.surface, rect)
        if screen_size.y <= 480:
            font_size = 18
        else:
            font_size = 28
        font_file = data.filepath('font','nvvzs.ttf')
        self.font = pygame.font.Font(font_file, font_size)
        self.sprites = []
        self.old_sprites = []
        self.time = 0.0
        self.duration = 0.0

        self.space = self.font.size(' ')[0]

        self._next()
        
    def _next(self):
        self.time = self.duration
        #if __debug__: print 'c', self.lindex, len(self.content)
        if self.lindex >= len(self.content):
            self.do_continue()
            return
        texts = self.content[self.lindex]
        #if __debug__: print 't', self.windex, len(texts)
        if self.windex >= len(texts):
            self.lindex += 1
            self.windex = 0
            return
        self.time = 0.0
        self.old_sprites = self.sprites
        self.sprites = []
        self.duration = 1.5 # initial duration
        w,h = self.view.size
        posx,posy = 0,0
        for text in texts[self.windex:]:
            # .render(text, antialias, color, background=None)
            text = text.strip()
            if text == '':
                self.windex += 1
                continue
            spr = pygame.sprite.Sprite()
            spr.image = self.font.render(text, False, (0,0,0))
            # spr.image.set_colorkey(None)
            # spr.image.set_alpha(255)
            spr.rect = spr.image.get_rect(topleft=(posx,posy))
            if spr.rect.right > w:
                posy += spr.rect.h + 1
                posx = 0
                spr.rect.topleft = posx,posy
            if spr.rect.bottom > h:
                return
            self.sprites.append(spr)
            posx += spr.rect.w + self.space
            self.windex += 1
            self.duration += settings.text_speed
    
    def do_continue(self):
        context.pop() # pop cutscene
        if self.new_context:
            new_context = self.new_context
        else:
            new_context = context.top()
        # context.push(transition.FadeInTranition((255, 255, 255), new_context, settings.fade_duration, False))
        context.push(transition.EscapePushTransition(new_context, transition.ColorFadeInEffect((255, 255, 255), settings.fade_duration)))
        context.print_stack()
        
    def think(self, dt):
        """Called once per frame"""
        self.time += dt
        
        if self.time >= self.duration:
            self._next()
            
        for e in pygame.event.get():
            if e.type == pygame.KEYDOWN:
                if e.key in settings.keys.quit:
                    self.do_continue()
                elif e.key in settings.keys.select:
                    self._next()

    def enter(self):
        """Called when this context is pushed onto the stack."""
        self.time = 0.0
        
    def exit(self):
        """Called when this context is popped off the stack."""
        pygame.event.clear()
        
    def draw(self, screen):
        """Refresh the screen"""
        screen.surface.fill((255, 255, 255))
        alpha = int(self.time / settings.fade_duration * 255.0)
        alpha = alpha if alpha < 255 else 255
        blit = self.view.blit
        for spr in self.sprites:
            spr.image.set_alpha(alpha)
            blit(spr.image, spr.rect)
        if 255-alpha == 0:
            del self.old_sprites[:]
        for spr in self.old_sprites:
            spr.image.set_alpha(255-alpha)
            blit(spr.image, spr.rect)
        end = self.view.width
        start = end * (self.time/self.duration)
        h = self.view.height - 7
        # pygame.draw.line(self.view.surface, (0,255,0), (start,h), (end,h), 7)
        

if __name__ == '__main__':
    import pygame
    from pygame.locals import *
    from gummworld2 import Screen
    pygame.init()
    scr = State.screen = Screen((600,400))
    context.push(CutScene('gummtest.txt'))
    while context.top():
        pygame.time.wait(10)
        scr.clear()
        context.top().think(10/1000.)
        if not context.top():
            break
        context.top().draw(scr)
        scr.flip()
