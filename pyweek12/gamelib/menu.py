import pygame
from pygame.locals import *

from gummworld2 import State, data

import context
import settings

FONT_NAME = 'nvvzs.ttf'
FONT_SIZE = 28
TEXT_COLOR = Color('white')
BG_COLOR = Color('#4a003e')
MINI_FONT_SIZE = 18

class MenuContext(context.Context):
    
    def __init__(self, text_list=[], align='center', hilight=True):
        super(MenuContext, self).__init__()
        self.text_list = []
        self.align = align
        self.hilight = hilight
        self.items = []
        
        screen_size = State.screen.size
        if screen_size.y <= 480:
            font_size = MINI_FONT_SIZE
        else:
            font_size = FONT_SIZE
        
        font_file = data.filepath('font',FONT_NAME)
        self.font = pygame.font.Font(font_file, font_size)
        
        self.max_width = 0
        for text in text_list:
            self.add(text)
        self.selected = 0
    
    def add(self, text):
        self.text_list.append(text)
        item = pygame.sprite.Sprite()
        item.image = self.font.render(text, True, TEXT_COLOR)
        item.rect = item.image.get_rect()
        if item.rect.w > self.max_width:
            self.max_width = item.rect.w
        self.items.append(item)
        
        screen_rect = State.screen.rect
        middle_item = len(self.items) // 2
        item_left = screen_rect.centerx - self.max_width // 2
        item_top = screen_rect.centery - item.rect.h * middle_item
        item_top = max(0, item_top)
        for i,item in enumerate(self.items):
            #item_top = screen_rect.centery + item.rect.h * (i-middle_item)
            if self.align == 'left':
                item.rect.topleft = item_left,item_top
            elif self.align == 'center':
                item.rect.centerx = screen_rect.centerx
                item.rect.top = item_top
            else:
                raise pygame.error, 'menu alignment must be "left" or "center"; got "%s"' % self.align
            item_top += item.rect.h
        
        self.hilighter = pygame.surface.Surface((self.max_width,item.rect.height))
        self.hilighter.set_colorkey((0,0,0))
        pygame.draw.rect(self.hilighter, Color('darkgrey'), self.hilighter.get_rect(), 1)

    def think(self, dt):
        pass
    
    def get_selected(self):
        return self.text_list[self.selected]
    
    def move_up(self):
        if self.selected > 0:
            self.selected -= 1
    
    def move_down(self):
        if self.selected < len(self.items)-1:
            self.selected += 1
    
    def draw(self, screen):
        screen.surface.fill(BG_COLOR)
        selected_item = self.items[self.selected]
        top_offset = 0
        if selected_item.rect.bottom > screen.rect.h:
            top_offset = screen.rect.h - selected_item.rect.bottom
        for i,item in enumerate(self.items):
            pos = item.rect.x, item.rect.y+top_offset
            screen.blit(item.image, pos)
            if i == self.selected:
                self.draw_hilighter(screen, item, top_offset)
    
    def draw_hilighter(self, screen, item, top_offset):
        if not self.hilight:
            return
        pos = item.rect.x, item.rect.y+top_offset
        if self.align == 'left':
            screen.blit(self.hilighter, pos)
        elif self.align == 'center':
            blit_rect = item.image.get_rect(topleft=pos)
            pygame.draw.rect(screen.surface, Color('darkgrey'), blit_rect, 1)
        
if __name__ == '__main__':
    import pygame
    from gummworld2 import Screen
    pygame.init()
    scr = State.screen = gummworld2.Screen((400,400))
    MENU_ITEMS_TEXT = (
        'Play',
        'Quit',
    )
    m = MenuContext(MENU_ITEMS_TEXT)
    while 1:
        pygame.time.wait(40)
        scr.clear()
        m.think(0)
        m.draw(scr)
        scr.flip()
