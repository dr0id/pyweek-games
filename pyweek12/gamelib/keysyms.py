class KeySyms(object):
    """A container that makes attributes from keywords, and saves key order.
    Use:
        k = Key('jump', (K_UP,K_SPACE), 'Jump', ...])
        values = k.jump
        pygame_event.key in (k.jump)
        for key in k.ordered: print k.description(key)
        values = k.values('jump')
    """
    
    def __init__(self, kw=[]):
        """kw is a flat list. Each key set required three arguments:
            
            1. A keysym. This is a string. It will be the key
        """
        self.ordered = []
        self.descs = {}
        for key,vals,desc in [kw[i:i+3] for i in range(0,len(kw),3)]:
            self.append(key, vals, desc)
    
    def insert(self, pos, key, vals, desc=''):
        """insert a key and values at pos. values for key will be overwritten."""
        self.__dict__[key] = vals
        self.descs[key] = desc
        if key in self.ordered:
            self.ordered.remove(key)
        self.ordered[pos:pos] = [key]
    
    def append(self, key, vals, desc=''):
        """append a key to the the ordered list. values for key will be overwritten."""
        pos = len(self.ordered)
        self.insert(pos, key, vals, desc)
    
    def names(self, val):
        """return list keys (str) that are associated with value."""
        n = []
        for key in self.ordered:
            if val in getattr(self, key):
                n.append(key)
        return n
    
    def values(self, key):
        """wrapper for getattr()."""
        return getattr(self, key)
    
    def description(self, key):
        """return the description for key."""
        return self.descs[key]

if __name__ == '__main__':
    def run_test(keys):
        assert keys.ordered == ['k2','k1','k99']
        assert keys.k1 == [1,11,111]
        assert keys.k2 == [2,22]
        assert keys.k99 == [1]
        assert keys.names(1) == ['k1','k99']
    
    # Test 1: build via constructor
    keys = KeySyms([
        'k2',  [2,22],     'Key 2',
        'k1',  [1,11,111], 'Key 1',
        'k99', [1],        'Key 99',
    ])
    run_test(keys)
    
    # Test 2: build via methods
    keys = KeySyms()
    keys.append(   'k1', [1,11])
    keys.insert(0, 'k2', [2,22])
    keys.k1.append(111)
    keys.append('k99', [1])
    run_test(keys)
