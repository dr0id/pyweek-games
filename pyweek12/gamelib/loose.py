import pygame
from pygame.locals import *

import gummworld2

import menu, context, game, settings

MENU_ITEMS_TEXT = (
    '= You have died! =',
    '',
    'To try again, press Space',
    '',
    'To quit the level, press Escape or "Q"',
    'or click the Close Button',
)

class Loose(menu.MenuContext):
    
    def __init__(self):
        super(Loose, self).__init__(
            MENU_ITEMS_TEXT, hilight=False)
    
    def think(self, dt):
        pygame.time.wait(40)
        for e in pygame.event.get():
            if e.type == KEYDOWN:
                if e.key in settings.keys.quit:
                    context.pop()
                elif e.key in (K_SPACE,):
                    lvid = gummworld2.State.level.level_id
                    context.pop()
                    context.push(game.GameContext(lvid))
            elif e.type == QUIT:
                context.pop()

if __name__ == '__main__':
    import gummworld2
    pygame.init()
    scr = gummworld2.State.screen = gummworld2.Screen((700,900))
    context.push(Loose())
    while context.top():
        pygame.time.wait(40)
        scr.clear()
        context.top().think(0)
        if context.top():
            context.top().draw(scr)
        scr.flip()
