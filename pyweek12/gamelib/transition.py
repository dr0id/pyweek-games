# -*- coding: utf-8 -*-

import pygame

import context
import context.transitions
import gummworld2
import settings


# ----------------------------------------------------------------------------

class UniColorContext(context.Context):

    def __init__(self, color):
        super(UniColorContext, self).__init__()
        self.color = color

    def draw(self, screen):
        """Refresh the screen"""
        screen.surface.fill(self.color)


# ----------------------------------------------------------------------------

class EscapeTransition(context.transitions.Transition):

    # def __init__(self, new_context, effect):
        # super(EscapeTransition, self).__init__(new_context, effect)

    def enter(self):
        super(EscapeTransition, self).enter()
        pygame.event.clear()

    def exit(self):
        super(EscapeTransition, self).exit()
        pygame.event.clear()

    def think(self, dt):
        """Called once per frame"""
        if super(EscapeTransition, self).think(dt):
            return
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key in settings.keys.quit:
                    context.pop()
        


# ----------------------------------------------------------------------------
class EscapePopTransition(context.transitions.PopTransition):

    def __init__(self, effect, do_think_old=True, do_think_new=False):
        super(EscapePopTransition, self).__init__(effect)
        self.do_think_old = do_think_old
        self.do_think_new = do_think_new

    def enter(self):
        super(EscapePopTransition, self).enter()
        pygame.event.clear()

    def exit(self):
        super(EscapePopTransition, self).exit()
        pygame.event.clear()

    def think(self, dt):
        """Called once per frame"""
        if self.do_think_new:
            self.new_context.think(dt)
        if self.do_think_old:
            self.old_context.think(dt)
        if super(EscapePopTransition, self).think(dt):
            return
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key in settings.keys.quit:
                    context.pop()
        
# ----------------------------------------------------------------------------
class EscapePushTransition(context.transitions.PushTransition):

    # def __init__(self, new_context, effect):
        # super(EscapePushTransition, self).__init__(new_context, effect)

    def enter(self):
        super(EscapePushTransition, self).enter()
        pygame.event.clear()

    def exit(self):
        super(EscapePushTransition, self).exit()
        pygame.event.clear()

    def think(self, dt):
        """Called once per frame"""
        if super(EscapePushTransition, self).think(dt):
            return
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key in settings.keys.quit:
                    context.pop()
# ----------------------------------------------------------------------------

class FadeInEffect(context.transitions.TransitionEffect):

    def __init__(self, duration):
        self.duration = duration
        self.alpha = 255.0
        self.time = 0.0

    def think(self, dt):
        self.time += dt
        if self.time < self.duration:
            self.alpha = int(255.0 - self.time / self.duration * 255.0)
            if self.alpha < 0:
                return True
        else:
            return True
        return False
        
    def draw(self, screen, old_context, new_context):
        screen.clear()
        new_context.draw(screen)
        size = screen.surface.get_size()
        s = gummworld2.screen.Screen(size, 0, pygame.Surface(size))
        old_context.draw(s)
        s.surface.set_alpha(self.alpha)
        screen.surface.blit(s.surface, (0, 0))
    
# ----------------------------------------------------------------------------
class FadeOutEffect(context.transitions.TransitionEffect):

    def __init__(self, duration):
        self.duration = duration
        self.alpha = 0
        self.time = 0.0

    def think(self, dt):
        self.time += dt
        if self.time < self.duration:
            self.alpha = int(self.time / self.duration * 255.0)
            if self.alpha > 255.0:
                return True
        else:
            return True
        return False
        
    def draw(self, screen, old_context, new_context):
        # this shoule have the same effect
        # super(FadeOutEffect, self).draw(screen, new_context, old_context)
        screen.clear()
        old_context.draw(screen)
        size = screen.surface.get_size()
        s = gummworld2.screen.Screen(size, 0, pygame.Surface(size))
        new_context.draw(s)
        s.surface.set_alpha(self.alpha)
        screen.surface.blit(s.surface, (0, 0))
# ----------------------------------------------------------------------------

class ColorFadeInEffect(context.transitions.TransitionEffect):

    def __init__(self, color, duration):
        self.duration = duration
        self.time = 0.0
        self.alpha = 0
        self.color = color

    def think(self, dt):
        self.time += dt
        if self.time < self.duration:
            self.alpha = int(255.0 - self.time / self.duration * 255.0)
            if self.alpha < 0:
                return True
        else:
            return True
        return False
        
    def draw(self, screen, old_context, new_context):
        screen.clear()
        new_context.draw(screen)
        size = screen.surface.get_size()
        s = gummworld2.screen.Screen(size, 0, pygame.Surface(size))
        s.surface.fill(self.color)
        s.surface.set_alpha(self.alpha)
        screen.surface.blit(s.surface, (0, 0))
    
    
# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------

