import pygame
from pygame.locals import *

import gummworld2

import menu, context, level_router, game, settings

MENU_ITEMS_TEXT = (
    "= You've won! =",
    '',
    '',
    '',
    'Press "space" key to continue...',
    '',
    '"Esc" or "q" for exit to main menu.'
)

class Win(menu.MenuContext):
    
    def __init__(self, following_level_class = None, *args):
        super(Win, self).__init__(
            MENU_ITEMS_TEXT, hilight=False)
        self.following_level_class = following_level_class
        self.args = args
            
    def enter(self):
        """Called when this context is pushed onto the stack."""
        pygame.event.clear()

    def think(self, dt):
        pygame.time.wait(40)
        back_out = False
        for e in pygame.event.get():
            if e.type == KEYDOWN:
                context.pop()
                if e.key in settings.keys.resume:
                    if self.following_level_class:
                        context.push(level_router.LevelRouter(
                            self.following_level_class, *self.args))
                        break
                    else:
                        back_out = True
                        break
                else:
                    back_out = True
            elif e.type == QUIT:
                context.pop()
                back_out = True
                break
        
        # We're in the WON screen, so we just won a level. Only way back out
        # is the Act Lobby.
        if back_out:
            wn,ln = gummworld2.State.level.level_id
            ln = wn
            wn = 0
            context.push(level_router.LevelRouter(
                        game.GameContext, (wn,ln)))


if __name__ == '__main__':
    import gummworld2
    pygame.init()
    scr = gummworld2.State.screen = gummworld2.Screen((700,900))
    context.push(Win(Win))
    while context.top():
        pygame.time.wait(40)
        scr.clear()
        context.top().think(0)
        if context.top():
            context.top().draw(scr)
        scr.flip()
