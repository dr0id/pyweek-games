
import context
from dialogue import Dialogue

# cut scene dialogue
cutscenes = {
    # Level 1:
    "whereami": ["CAPT BIEZ: Where am I?", "The last thing I remember is being sentenced to death.", "Is this hell?"],
    "whatwasthat": ["CAPT BIEZ: What was that thing?", "Good thing I have my trusty baseball bat", "I can use by left-clicking, and my pistol,", "I can use by right-clicking"],
    "battrick": ["CAPT BIEZ: This place is dangerous.", "I'd better remember my training.", "I can do a 360-attack with my bat", "by holding down left-click for a second."],
    "weaponselect": ["CAPT BIEZ: I've also got a chainsaw and a","shotgun. I shouldn't forget I can select", "weapons with the scroll wheel or Tab/Left Shift.", "Better watch my ammo, though."],

    # Level 2:
    "hiimzoe": ["DR BIM: Mo? Mo Biez? Is that you?",
                "Thank god you're still alive.",
                "CAPT BIEZ: Who are you? What is this place?",
                "DR BIM: My name is Zoe Bim. You're in",
                "Fort Izembo. It's a top secret facility.",
                "There's been a disaster. You've got to get up to",
                "the ground level. I can talk to you through",
                "these consoles. Now go!",],
    "whoareyou": ["CAPT BIEZ: Where do I know you from?",
                  "DR BIM: I'm the doctor who examined you after the",
                  "incident on the USS Moibez.",
                  "CAPT BIEZ: The Moibez... I wish I could forget....",
                  "DR BIM: This is the second time you've cheated death.",
                  "You're a lucky man, Biez.",
                  "CAPT BIEZ: Yeah, this is just my lucky day...."],
    "thenews": ["CAPT BIEZ: Look, Doc, I don't know if you've",
                "been watching the news....",
                "DR BIM: I know what you've been accused of, Biez.",
                "I also know you didn't kill Gen. M'Biezo.",
                "CAPT BIEZ: How do you know that?",
                "DR BIM: Let's just say that working here, you come to",
                "recognize injustice."],
    "whatarethey": ["CAPT BIEZ: What are these creatures anyway?",
                    "Are they human?",
                    "DR BIM: Unfortunately yes. There are some labs",
                    "ahead. They may hold some answers if you want them.",
                    "Just remember, Mo, some things are better left",
                    "unknown."],
    "location": ["FORT IZEMBO INFORMATION",
                 "Location: Mebizo Island",
                 "Notes: The location is secure, as the island is",
                 "considered to be cursed by the native Omezbi people",
                 "who do not approach it. This is because it's the site",
                 "of an ancient burial ground.",
                 "CAPT BIEZ: Mebizo Island? I've been there once before.",
                 "That was the last place we ever visited in the Moibez....",
                 "I thought it was uninhabited."], 
    "projectinfo": ["PROJECT BIO-MEZ INFORMATION",
                    "Classification: biochemical warfare",
                    "Project Lead: Dr. O'Mibez",
                    "Test subject authorization: Project Bio-Mez has",
                    "authorization to commandeer as test subjects any",
                    "prisoners in US military custody who have been",
                    "sentenced to death by court martial.",
                    "CAPT BIEZ: That's me... I'm a test subject?"],
    "biezinfo": ["INFORMATION ON SUBJECT #203818",
                 "Name: Mo Biez",
                 "Rank: Captain",
                 "Crimes: Assassination, Treason",
                 "Sentence: Death",
                 "CAPT BIEZ: Tell me something I don't know."],
    "systeminfo": ["SYSTEM INTEGRITY INFORMATION",
                   "Quarantine system has detected pathogens in air.",
                   "Life support system has detected elevated levels of",
                   "space radiation throughout the facility.",
                   "Evacuation order in place!",
                   "Elevators out of order, please use stairs.",
                   "CAPT BIEZ: Thanks for the tip."],
    "hiagain": ["DR BIM: Well, Biez, did you find your answers?",
                "CAPT BIEZ: All but one, Doc. Did this project of yours",
                "have anything to do with the deaths of my friends on",
                "the Moibez?",
                "DR BIM: I'm afraid so, Mo. I'm sorry. I want you to know",
                "I'm doing everything in my power to put an end to",
                "Project Bio-Mez once and for all.",
                "You have to believe me. Now please, we have to get",
                "you out of there!"],
    "imsorry": ["DR BIM: The next room is going to be very intense.",
                "Keep your head about you and just make it to the exit.",
                "And if anything happens, Mo, just know.... I'm sorry.",
                "Things were never supposed to get this bad.",
                "CAPT BIEZ: What's that supposed to mean?"],
    "goaway": ["WHAT THE HELL ARE YOU DOING OVER HERE!",
                "GET TO THE DAMN EXIT!"],

    # Level 3
    "youmadeit": ["DR BIM: Mo! You made it out alive! Let's get you to the",
                  "chopper.",
                  "CAPT BIEZ: Music to my ears. Let's go!",
                  "DR BIM JOINED YOUR PARTY!"],
#    "meetomibez": ["DR O'MIBEZ: Is that you, test subject #203818?",
#                   "How wonderful that you've survived!",
#                   "How do you feel?",
#                   "CAPT BIEZ: Just when I was starting to get over the",
#                   "mysterious deaths of my crewmates, I get accused of",
#                   "killing the President of Zemobi, and the next thing",
#                   "I know I wake up on an operating table and get chased",
#                   "by god knows what. How do you think I feel?",
#                   "DR O'MIBEZ: Fascinating! No signs of infection!",
#                   "DR O'MIBEZ JOINED YOUR PARTY!"],
#    "youreimmune": ["DR O'MIBEZ: You must have a natural immunity to the",
#                    "weapon. That won't do at all. It's back to the drawing",
#                    "board.",
#                    "CAPT BIEZ: Doc, your drawing board is currently crawling",
#                    "with monsters. Can we please get out of here?",
#                    "DR O'MIBEZ: Don't you think it's an amazing coincidence",
#                    "that you of all people wound up here, 203818?",
#                    "DR BIM: Shut up!",
#                    "CAPT BIEZ: Is there something you want to tell me?"],
    "confess": ["DR BIM: I'm so sorry, Mo, it was the only way to",
                "shut down Project Bio-Mez, you have to believe me!",
                "CAPT BIEZ: What are you saying?",
                "DR BIM: The assassination... I was the one who framed you.",
                "After I examined you, I knew you were immune. I had to",
                "get you here, this was the only way.",
                "CAPT BIEZ: You killed M'biezo?",
                "DR BIM: No, he's alive. At least, he was when we brought",
                "him here to Izembo yesterday...."],
    "theend": ["DR BIM: Go on without me. I'll stay behind to trigger the",
                "self-destruct. It's the only way to make sure the infection",
                "doesn't spread beyond the island.",
                "CAPT BIEZ: You're going to sacrifice yourself?",
                "DR BIM: It's all I deserve. Just promise me you'll tell",
                "them everything and finally shut this project down!",
                "CAPT BIEZ: Zoe.... good luck.",
                "DR BIM HAS LEFT YOUR PARTY!"],
    

}

seen = set()  # triggers we've already seen

# Call this when the player hits a trigger.
def trigger(name):
    if name in seen: return  # Only do triggers once
    seen.add(name)
    
    if name in cutscenes:
        context.push(Dialogue(cutscenes[name]))
    else:
        print "Unrecognized trigger: %s" % name



