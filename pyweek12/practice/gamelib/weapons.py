import pygame

import weapon

##
##  Only Pistol and BaseballBat work so far.
##
##  Weapons should probably have a max_ammo attribute, and refill only loads up
##  to that amount.
##
## Do we want an ammo belt/pouch? Or more like Doom, where excess ammo is lost
## when picked up.
##

"""
weapon.Attack class constructor docstring.

Note:
    - speed, cooldown, dot interval work on elapsed time (dt), which is
      a fraction of a second. For example, a quarter of a second in
      real-time is 0.25.
    - dot duration is number of repetitions (int).

Arguments:
    speed (time it takes to stab/swing/fire, after which direct damage
        is applied)
    cooldown (time it takes to recover, until next attack is ready)
    ammo used per attack (i.e. bullets, gasoline rev)
    ammo used per tick (i.e. gasoline idle)
    direct damage (applied once after waiting speed)
    damage over time (applied once per tick when cooldown starts)
    DoT interval (time between applications of damage over time)
    DoT duration (number of repetitions to apply damage over time)

Some cut-n-paste for convenience...
ammo = 
speed = 
cooldown = 
use_per_attack = 
use_per_tick = 
direct_damage = 
damage_over_time = 
dot_interval = 
dot_duration = 

"""

class BaseballBat(weapon.Weapon):
    
    name = 'Baseball Bat'
    type = 'melee'
    max_range = 60      # measured in pixels
    default_arc = 60    # measured in pixels
    special_arc = 360   # measured in pixels
    
    def __init__(self):
        ammo = None
        speed = [.25,.5] # default,special
        cooldown = [0.1,1.] # default,special
        use_per_attack = 0
        use_per_tick = 0
        direct_damage = [1,2] # default,special
        damage_over_time = 0
        dot_interval = 0
        dot_duration = 0
        super(BaseballBat, self).__init__(
            ammo,
            default=weapon.Attack(
                speed[0], cooldown[0],
                use_per_attack, use_per_tick,
                direct_damage[0],
                damage_over_time, dot_interval, dot_duration,
                hit_multiple=True,
            ),
            special=weapon.Attack(
                speed[1], cooldown[1],
                use_per_attack, use_per_tick,
                direct_damage[1],
                damage_over_time, dot_interval, dot_duration,
                hit_multiple=True,
            )
        )

class Chainsaw(weapon.Weapon):
    
    name = 'Chainsaw'
    type = 'melee'
    max_range = 60      # measured in pixels
    default_arc = 60    # measured in pixels
    special_arc = 360   # measured in pixels
    
    def __init__(self):
        ammo = 40
        speed = .5
        cooldown = [.5,1.]      # default,special
        use_per_attack = [1,2]  # default,special
        use_per_tick = 0
        direct_damage = [3,5]   # default,special
        damage_over_time = 0
        dot_interval = 0
        dot_duration = 0
        super(Chainsaw, self).__init__(
            ammo,
            default=weapon.Attack(
                speed, cooldown[0],
                use_per_attack[0], use_per_tick,
                direct_damage[0],
                damage_over_time, dot_interval, dot_duration,
                hit_multiple=True,
            ),
            special=weapon.Attack(
                speed, cooldown[1],
                use_per_attack[1], use_per_tick,
                direct_damage[1],
                damage_over_time, dot_interval, dot_duration,
                hit_multiple=True,
            )
        )

class Pistol(weapon.Weapon):
    
    name = 'Pistol'
    type = 'ranged'
    max_range = 200     # measured in pixels
    default_arc = 20    # measured in pixels
    special_arc = 20    # measured in pixels
    
    def __init__(self):
        ammo = 80
        speed = [0,.25] # default,special
        cooldown = 0.1
        use_per_attack = 1
        use_per_tick = 0
        direct_damage = [3,5] # default,special
        damage_over_time = 0
        dot_interval = 0
        dot_duration = 0
        super(Pistol, self).__init__(
            ammo,
            default=weapon.Attack(
                speed[0], cooldown,
                use_per_attack, use_per_tick,
                direct_damage[0],
                damage_over_time, dot_interval, dot_duration,
                hit_multiple=False,
            ),
            special=weapon.Attack(
                speed[1], cooldown,
                use_per_attack, use_per_tick,
                direct_damage[1],
                damage_over_time, dot_interval, dot_duration,
                hit_multiple=False,
            )
        )
        
        def refill(self, magazine):
            """magazine must have an int attribute named ammo."""
            super(Pistol, self).refill(magazine.ammo)

class Shotgun(weapon.Weapon):
    
    name = 'Shotgun'
    type = 'ranged'
    max_range = 125     # measured in pixels
    default_arc = 40    # measured in pixels
    special_arc = 40    # measured in pixels
    
    def __init__(self):
        ammo = 20
        speed = .0
        cooldown = 1.
        use_per_attack = 1
        use_per_tick = 0
        direct_damage = 5
        damage_over_time = 0
        dot_interval = 0
        dot_duration = 0
        super(Shotgun, self).__init__(
            ammo,
            default=weapon.Attack(
                speed, cooldown,
                use_per_attack, use_per_tick,
                direct_damage,
                damage_over_time, dot_interval, dot_duration,
                hit_multiple=True,
            ),
        )
        self.add_attack(self.attacks['default'], 'special')

class Grenade(weapon.Weapon):
    
    name = 'Grenade'
    
    def __init__(self):
        super(Grenade, self).__init__(
            default=weapon.Attack(
            ),
            special=weapon.Attack(
            )
        )
