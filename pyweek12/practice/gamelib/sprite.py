import pygame

import geometry

class Sprite(pygame.sprite.Sprite):
    
    def __init__(self, *groups, **kw):
        """Create a sprite.
        
        Sprite(*groups, source=FILENAME_OR_IMAGE, colorkey=None, position=(0,0))
        """
        super(Sprite, self).__init__(*groups)
        
        if 'source' not in kw:
            raise pygame.error,'missing required keyword argument source'
        
        valid_names = ('source','colorkey','position','name')
        for name in kw:
            if name not in valid_names:
                raise pygame.error,'unknown keyword argument '+name
        
        source = kw.get('source', None)
        self.colorkey = kw.get('colorkey', None)
        self.name = kw.get('name', '')
        
        if isinstance(source, pygame.surface.Surface):
            self.image = source
        elif isinstance(source, str):
            self.image = pygame.image.load(source)
            if colorkey is None:
                pass
            elif self.colorkey == -1:
                self.image.set_colorkey(self.image.get_at((0,0)))
            elif isinstance(self.colorkey, (tuple,list)):
                self.image.set_colorkey(self.image.get_at(self.colorkey))
        else:
            raise pygame.error, 'source must be type str or pygame.surface.Surface'
        
        self.rect = self.image.get_rect()
        #self.source_rect = kw.get('source_rect', self.rect.copy())
        self.source_rect = kw.get('source_rect', pygame.Rect(self.rect))

        self.flags = kw.get('flags', 0)

        self.position = kw.get('position', (0,0))
        self.rect.center = self.position
    
    ## entity's collided, static method used by QuadTree callback
    collided = staticmethod(geometry.rect_collided_other)
