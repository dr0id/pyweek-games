import gummworld2

class CameraTarget(gummworld2.model.QuadTreeObject):
    """rect is reserved for world placement and collision in quadtree.
    position property is inherited.
    """
    
    def __init__(self, rect, position=(0,0)):
        super(CameraTarget, self).__init__(rect, position)
        self.name = 'camera_target'
