# This file taken from www.pygame.org/wiki/SpriteSheet

"""
This class handles sprite sheets
This was taken from www.scriptefun.com/transcript-2-using
sprite-sheets-and-drawing-the-background
I've added some code to fail if the file wasn't found..
Note: When calling images_at the rect is the format:
(x, y, x + offset, y + offset)
"""

import pygame
from pygame.locals import *
 
class SpriteSheet(object):
    def __init__(self, filename, convert_alpha=False):
        try:
            image = pygame.image.load(filename)
            if convert_alpha:
                self.sheet = image.convert_alpha()
            else:
                self.sheet = pygame.surface.Surface(image.get_size())
                self.sheet.blit(image, (0,0))
        except pygame.error, message:
            print 'Unable to load spritesheet image:', filename
            raise SystemExit, message
    # Load a specific image from a specific rectangle
    def image_at(self, rectangle, colorkey = None):
        "Loads image from x,y,x+offset,y+offset"
        rect = pygame.Rect(rectangle)
#        image = pygame.Surface(rect.size).convert()
#        image.blit(self.sheet, (0,0), rect)
#        image = self.sheet.subsurface(rect).copy()
        flags = self.sheet.get_flags()
        image = pygame.surface.Surface(rect.size, flags)
        image.blit(self.sheet, (0,0), rect)
        if colorkey is not None:
            if colorkey is -1:
                colorkey = image.get_at((0,0))
            elif type(colorkey) not in (pygame.Color,tuple,list):
                colorkey = image.get_at((colorkey,colorkey))
            image.set_colorkey(colorkey, pygame.RLEACCEL)
        return image
    # Load a whole bunch of images and return them as a list
    def images_at(self, rects, colorkey = None):
        "Loads multiple images, supply a list of coordinates" 
        imgs = []
        for rect in rects:
            imgs.append(self.image_at(rect, colorkey))
        return imgs
    # Load a whole strip of images
    def load_strip(self, size, image_count, margin=(0,0), padding=(0,0), colorkey=None, orient='h'):
        "Loads a strip of images and returns them as a list"
        tups = []
        for n in range(image_count):
            if orient == 'h':
                x = n; y = 0
            else:
                x = 0; y = n
            left = margin[0] + x*size[0] + x*padding[0]
            top = margin[1] + y*size[1] + y*padding[1]
            tups.append((left, top, size[0], size[1]))
        return self.images_at(tups, colorkey)

if __name__ == '__main__':
    import pygame
    pygame.init()
    screen = pygame.display.set_mode((400,400))
    sheet = SpriteSheet('../data/image/blood0.png')
    images = sheet.load_strip((64,64), 6, margin=(0,0), padding=(0,0), colorkey=-1, orient='v')
    frames = (8,0,1,2,3,4,5,6,7)
    #frames = (0,1,2,3,4,5,6,7,8)
    frames = (0,1,2,3,4,5)
    clock = pygame.time.Clock()
    while 1:
        for i,frame in enumerate(frames):
            pygame.display.set_caption('Frame: '+str(i))
            clock.tick(2)
            image = images[frame]
            screen.fill((0,0,0))
            screen.blit(pygame.transform.rotozoom(image, 0, 4), (0,0))
            for y in range(0,400,8):
                pygame.draw.line(screen, (199,199,199), (0,y), (399,y), 1)
            rect = image.get_rect()
            rect.w *= 4
            rect.h *= 4
            pygame.draw.rect(screen, (255,0,0), rect, 1)
            pygame.display.flip()
