import context, fonts

import pygame,data,os

class CreditsContext(context.Context):
    def __init__(self):
        self.screen = pygame.display.get_surface()

#        help needed: i can't load zxumbi.ttf from here        

#        font_file = data.filepath(os.path.join('font','./data/font/zxumbi.ttf'))
#        font = pygame.font.Font('../data/font/zxumbi.ttf', 24)

        font_file = data.filepath(os.path.join('font','zxumbi.ttf'))
        font = pygame.font.Font(font_file, 10)

        #font = pygame.font.Font(None, 30)

        members = ["Christopher Night (Cosmologicon) <cosmologicon@gmail.com>",
                    "DR0ID <dr0iddr0id@googlemail.com>",
                    "Gummbum <stabbingfinger@gmail.com>",
                    "ldlework <dlacewell@gmail.com>",
                    "Nitrofurano <nitrofurano@gmail.com>" ]
        self.sprites = []
        posx = self.screen.get_rect().centerx
        dy = self.screen.get_rect().height / (len(members) + 1)
        posy = dy
        for member in members:
            spr = pygame.sprite.Sprite()
#            spr.image = font.render(member , True, pygame.Color('yellow'), pygame.Color('black'))
            spr.image = fonts.render_with_border(member, font,
                pygame.Color('yellow'), pygame.Color('black'), pygame.Color('red'))
            spr.rect = spr.image.get_rect(center=(posx, posy))
            posy += dy
            self.sprites.append(spr)
    
    def think(self, dt):
        """Called once per frame"""
        self.get_input()
        self.update()

    def resume(self):
        """Called when an already-existing context is again at the top of the stack"""
        pass

    def get_input(self):
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                context.pop()
            elif e.type == pygame.KEYDOWN:
                context.pop()
                
    def update(self):
        self.screen.fill((0,0,0))
        for spr in self.sprites:
            self.screen.blit(spr.image, spr.rect)
        pygame.display.flip()
