

import pygame

import context, fonts, data, os

class Loose(context.Context):
    
    def __init__(self):
        self.screen = pygame.display.get_surface()

        font_file = data.filepath(os.path.join('font','zxumbi.ttf'))
        font = pygame.font.Font(font_file, 24)
        
        #font = pygame.font.Font(None, 60)
        lines = ["You DIED", "press return to continue..."]
        self.sprites = []
        posx = self.screen.get_rect().centerx
        dy = self.screen.get_rect().height / (len(lines) + 2)
        posy = dy
        for member in lines:
            spr = pygame.sprite.Sprite()
#            spr.image = font.render(member , True, pygame.Color('red'), pygame.Color('black'))
            spr.image = fonts.render_with_border(member, font,
                pygame.Color('red'), pygame.Color(138,0,0), pygame.Color(139,0,0),
                border_width=3)
            spr.rect = spr.image.get_rect(center=(posx, posy))
            posy += dy
            self.sprites.append(spr)
        
    def think(self, dt):
        """Called once per frame"""
        self.get_input()
        self.update()

    def resume(self):
        """Called when an already-existing context is again at the top of the stack"""
        pass

    def get_input(self):
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                context.pop()
            elif e.type == pygame.KEYDOWN:
                if e.key == pygame.K_RETURN:
                    context.pop()
                
    def update(self):
        for spr in self.sprites:
            self.screen.blit(spr.image, spr.rect)
        pygame.display.flip()
