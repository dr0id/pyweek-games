# The context stack is a private member of this module
# To access the current (top) context on the context stack: context.top()
# To push a new context onto the stack: context.push(con)
# To pop the top context off the stack: context.pop()

class Context(object):
    
    def think(self, dt):
        """Called once per frame"""
        pass

    def suspend(self):
        """Called when another context is pushed onto it"""
        pass
        
    def resume(self):
        """Called when an already-existing context is again at the top of the stack"""
        pass
        
    def enter(self):
        """Called once when the context is pushed for the first time."""
        pass
        
    def exit(self):
        """Called once when the context is poped."""
        pass
        
    def draw(self, screen_surface):
        """Called when the screen needs to be refreshed"""
        pass

_cstack = list()

def top():
    return _cstack[-1] if _cstack else None

def push(con):
    if _cstack:
        if __debug__: print "context suspended: ", _cstack[-1].__class__.__name__
        _cstack[-1].suspend()
    if __debug__: print "context pushed: ", con.__class__.__name__
    con.enter()
    _cstack.append(con)

def pop(n = 1):
    for j in range(n):
        if _cstack:
            if __debug__: print "context exit: ", _cstack[-1].__class__.__name__
            _cstack[-1].exit()
            if __debug__: print "context popping: ", _cstack[-1].__class__.__name__
            del _cstack[-1]
        if _cstack: 
            if __debug__: print "context resumed: ", _cstack[-1].__class__.__name__
            _cstack[-1].resume()

