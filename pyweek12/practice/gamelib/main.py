'''Game main module.

Contains the entry point used by the run_game.py script.
'''

import pygame
from pygame.locals import *
import menus, context, settings, game
import gummworld2


def main():
    pygame.display.set_mode(settings.resolution, (FULLSCREEN if settings.fullscreen else 0))
    pygame.display.set_caption("Fort Izembo")
    if settings.quickstart:
        context.push(game.GameContext(settings.quickmap))
    else:
        context.push(menus.MainMenu())

#    clock = pygame.time.Clock()  
    gummworld2.State.clock = gummworld2.GameClock(
        ticks_per_second=25, max_fps=settings.fps)
  
    clock = gummworld2.State.clock
    elapsed = 0.0
    screen = pygame.display.get_surface()
    pygame_display_flip = pygame.display.flip
    
    while context.top():
        # clock.tick(settings.fps)
        
        elapsed += clock.tick()
        if clock.update_ready():
            context.top().think(elapsed)
            elapsed = 0.0
        if context.top() and clock.frame_ready():
            context.top().draw(screen)
            pygame_display_flip()
    pygame.quit()

