import sys

resolution = 800, 600
fullscreen = False
fps = 0
printfps = True
quickstart = False
quickmap = 'level-1.tmx'
double_click = .25 # fraction of a second

# zombie
zombie_wander_speed = 20.0
zombie_follow_speed = 40.0
zombie_hit_points = 10
zombie_attack_distance = 20
zombie_num_frames_to_backoff = 3
zombie_backoff_dist = 30.0
zombie_initial_health = 4
zombie_wander_see_dist = 250.0

# player
player_inital_health = 100
player_speed = 125

# QuadTree constructor argument: collide_entities
# Faster rect collision tested first, then if this is True more precise geometry
# collision is tested. If there are many tightly grouped entities in the tree,
# there could also be many collisions and turning this on could be expensive.
precise_collisions = False


if "-f" in sys.argv:
    fullscreen = True
if "-r" in sys.argv:
    rx, _, ry = sys.argv[sys.argv.index("-r")+1].partition("x")
    resolution = int(rx), int(ry)
if "-p" in sys.argv:
    printfps = True
if "-fps" in sys.argv:
    fps = int(sys.argv[sys.argv.index("-fps")+1])
if "-q" in sys.argv:
    quickstart = True

