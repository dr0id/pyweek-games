# Create a wedge based on player position and mouse position

import math

# x0, y0: center of wedge
# x1, y1: direction wedge is facing
# r: radius of wedge
# a: angular size of wedge in degrees
def wedge((x0, y0), (x1, y1), r, a = 90., ndiv = 4):
    dx, dy = x1 - x0, y1 - y0
    d = math.sqrt(dx ** 2 + dy ** 2)
    if not d:
        dx, dy, d = 0., 1., 1.
    if a >= 360.:
        ps = []
        a = 360.
    else:
        ps = [(x0, y0)]
    for jdiv in range(ndiv + 1):
        angle = (float(jdiv) / ndiv - 0.5) * a / 57.3
        S, C = math.sin(angle), math.cos(angle)
        wx, wy = r * (C * dx + S * dy) / d, r * (-S * dx + C * dy) / d
        ps.append((x0+wx, y0+wy))
    return ps


if __name__ == "__main__":
    import pygame, sys
    screen = pygame.display.set_mode((800, 600))
    px, py = 200, 200
    clock = pygame.time.Clock()
    while True:
        clock.tick(60)
        screen.fill((0,0,0))
        for e in pygame.event.get():
            if e.type == pygame.QUIT or (e.type == pygame.KEYDOWN and e.key == pygame.K_ESCAPE):
                sys.exit()
        m = pygame.key.get_pressed()
        px += 3 * (m[pygame.K_RIGHT] - m[pygame.K_LEFT])
        py += 3 * (m[pygame.K_DOWN] - m[pygame.K_UP])
        pygame.draw.circle(screen, (255, 128, 0), (px, py), 4, 0)
        if pygame.mouse.get_pressed()[0]:  # left-click for baseball bat attack
            ps = wedge((px, py), pygame.mouse.get_pos(), 50, 90.)
            pygame.draw.aalines(screen, (255, 255, 255), True, ps)
        if pygame.mouse.get_pressed()[1]:  # middle-click for shotgun attack
            ps = wedge((px, py), pygame.mouse.get_pos(), 500, 15.)
            pygame.draw.aalines(screen, (255, 255, 255), True, ps)
        if pygame.mouse.get_pressed()[2]:  # right-click for 360-degree attack
            ps = wedge((px, py), pygame.mouse.get_pos(), 50, 360., 12)
            pygame.draw.aalines(screen, (255, 255, 255), True, ps)
        pygame.display.flip()



