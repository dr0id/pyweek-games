

import pygame

import context

class SlideTransition(context.Context):
    
    def __init__(self, old_context, new_context):
        self.old_context = old_context
        self.new_context = new_context
        self.rect = None
        self.pos = 0
        self.old_surf = None
        self.new_surf = None
        self.new_context.think(0.)
        
    def think(self, dt):
        """Called once per frame"""
#        self.new_context.think(dt) # run the game during transition
        pygame.event.pump() # just pump the event loop, maybe check for keypresses to skip transition?
        if self.rect:
            self.pos -= 800.0 * dt
            if self.pos < -self.rect.width:
                self.change_context()
                
    # maybe call this too to skip on key press
    def change_context(self):
        context.pop()
        context.push(self.new_context)

    def draw(self, screen):
        self.rect = screen.get_rect()
        
        if not self.old_surf:
            self.old_surf = pygame.Surface(self.rect.size)
        self.old_context.draw(self.old_surf)
        screen.blit(self.old_surf, (self.pos, 0))
        
        if not self.new_surf:
            self.new_surf = pygame.Surface(self.rect.size)
        self.new_context.draw(self.new_surf)
        screen.blit(self.new_surf, (self.pos + self.rect.width, 0))
        
        

class ExplosionTransition(context.Context):
    
    def __init__(self, old_context, new_context, screen_size):
        self.old_context = old_context
        self.new_context = new_context
        self.rect = pygame.Rect((0, 0), screen_size)
        self.surf = pygame.Surface(self.rect.size)
        self.old_surf = pygame.Surface(self.rect.size)
        self.sprites = []
        area_width = 25
        area_height = 25
        area = pygame.Rect(0, 0, area_width, area_height)
        cx, cy = self.rect.center
        self.seen = False
        for x in range(0, self.rect.width, area_width):
            for y in range(0, self.rect.height, area_height):
                sprite = pygame.sprite.Sprite()
                area.x = x
                area.y = y
                # sprite.rect = pygame.Rect(area)
                sprite.image = self.old_surf.subsurface(area)
                dx = area.centerx - cx * 1.0
                dy = area.centery - cy * 1.0
                dist2 = (dx * dx + dy * dy) * 0.000025
                dist2 = dist2 if dist2 else 1.0
                sprite.vx = dx / dist2
                sprite.vy = dy / dist2
                sprite.x = x
                sprite.y = y
                sprite.layer = 1.0 / dist2
                self.sprites.append(sprite)
        self.sprites.sort(key=lambda spr: spr.layer)
        self.time = 0
        
    def think(self, dt):
        self.new_context.think(dt)
        self.time += dt
        pygame.event.pump()
        if self.time > 0.5 and self.seen:
            for sprite in self.sprites: 
                sprite.x += sprite.vx * dt
                sprite.y += sprite.vy * dt
        if self.time > 2:
            context.pop()
            context.push(self.new_context)
            
        
    def draw(self, screen):
        self.old_context.draw(self.old_surf)
        self.new_context.draw(self.surf)
        for sprite in self.sprites:
            self.surf.blit(sprite.image, (sprite.x, sprite.y))
            
        screen.blit(self.surf, (0, 0))
        self.seen = True
    
import random
class DisapearingRectsTransition(context.Context):
    
    def __init__(self, old_context, new_context, screen_size):
        self.old_context = old_context
        self.new_context = new_context
        self.rect = pygame.Rect((0, 0), screen_size)
        self.surf = pygame.Surface(self.rect.size)
        self.old_surf = pygame.Surface(self.rect.size)
        self.sprites = []
        area_width = 25
        area_height = 25
        area = pygame.Rect(0, 0, area_width, area_height)
        self.seen = False
        for x in range(0, self.rect.width, area_width):
            for y in range(0, self.rect.height, area_height):
                area.x = x
                area.y = y
                sprite = pygame.sprite.Sprite()
                sprite.image = self.old_surf.subsurface(area)
                sprite.rect = sprite.image.get_rect(topleft=area.topleft)
                self.sprites.append(sprite)
        self.time = 0
        
    def think(self, dt):
        self.new_context.think(dt)
        self.time += dt
        pygame.event.pump()
        if self.time > 0.2 and self.seen:
            for i in range(40):
                if self.sprites:
                    sprite = random.choice(self.sprites)
                    self.sprites.remove(sprite)
        
        if not self.sprites:
            context.pop()
            context.push(self.new_context)
            
        
    def draw(self, screen):
        self.old_context.draw(self.old_surf)
        self.new_context.draw(self.surf)
        for sprite in self.sprites:
            self.surf.blit(sprite.image, sprite.rect)
            
        screen.blit(self.surf, (0, 0))
        self.seen = True
    
