 # I pretty much just copied this from the win module. -Cosmo

import pygame

import context, fonts

class Dialogue(context.Context):
    
    def __init__(self, lines):
        self.screen = pygame.display.get_surface()
        
        font = pygame.font.Font(None, 40)
        self.sprites = []
        posx = self.screen.get_rect().centerx
        dy = 54
        posy = dy
        for member in lines:
            spr = pygame.sprite.Sprite()
#            spr.image = font.render(member , True, pygame.Color('yellow'), pygame.Color('lightblue'))
            spr.image = fonts.render_with_border(member, font,
                pygame.Color(0,150,0), pygame.Color(0,66,0), pygame.Color(0,77,0),
                border_width=3)
            spr.rect = spr.image.get_rect(center=(posx, posy))
            posy += dy
            self.sprites.append(spr)
        
    def think(self, dt):
        """Called once per frame"""
        self.get_input()
        self.update()

    def resume(self):
        """Called when an already-existing context is again at the top of the stack"""
        pass

    def get_input(self):
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                context.pop()
            elif e.type == pygame.KEYDOWN:
                if e.key in (pygame.K_RETURN, pygame.K_ESCAPE, pygame.K_SPACE):
                    context.pop()
                
    def update(self):
        for spr in self.sprites:
            self.screen.blit(spr.image, spr.rect)
        pygame.display.flip()


