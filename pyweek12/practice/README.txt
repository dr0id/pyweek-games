Fort Izembo
===========

Practice entry for Pyweek #12  <http://www.pyweek.org/12/>
Team: Multiverse Factory
Members:
Christopher Night (Cosmologicon) <cosmologicon@gmail.com>
DR0ID <dr0iddr0id@googlemail.com>
Gummbum <stabbingfinger@gmail.com>
ldlework <dlacewell@gmail.com>
Paulo Silva <nitrofurano@gmail.com>
JDruid <jhkarjal@gmail.com>

Project page:
http://code.google.com/p/multifac/

DEPENDENCIES:

You might need to install some of these before running the game:

  Python 2.6+:     http://www.python.org/
  PyGame 1.8+:     http://www.pygame.org/


RUNNING THE GAME:

On Windows or Mac OS X, locate the "run_game.pyw" file and double-click it.

Otherwise open a terminal / console and "cd" to the game directory and run:

  python run_game.py



HOW TO PLAY THE GAME:

Move the guy with the arrow keys or WASD.

Melee attack with the left mouse button.
Hold left mouse button for a second for a roundhouse attack.

Range attack with the right mouse button.

Tab: switch melee weapon
Left Shift: switch range weapon
Scroll wheel up/down: switch weapons


LICENSE:

The Gummworld library is LGPL.

The pgu library is LGPL.

The game code is GPL.

The typeface zxumbi has OFL licence - and is available also from http://nitrofurano.altervista.org/typefaces/zxumbi

The bitstream-vera fonts are copyright Bitstream, Inc. Their copyright notice is embedded in the fonts themselves.
