# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek19-2014-10
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function


__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2014"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module


class DirtyEntity(object):
    def __init__(self):
        self.dirty = False

    def set_dirty(self):
        self.dirty = True


class DirtyListEntity(object):
    dirty = []
    dirty_append = dirty.append

    def __ini__(self):
        pass

    def set_dirty(self):
        DirtyListEntity.dirty_append(self)

class DirtyListEntity2(object):
    dirty = []

    def __init__(self, dirty_list=None):
        self.dirty = dirty_list
        if dirty_list is None:
            self.dirty = DirtyListEntity2.dirty





total_count = 10000
all_de = [DirtyEntity() for i in range(total_count)]
all_dle = [DirtyListEntity() for i in range(total_count)]

dirty_percentage = 1
dirty_count = int(dirty_percentage * total_count)

import timeit
#
# elapsed_de = timeit.timeit("[ent.set_dirty() for ent in all_de]", setup="from __main__ import all_de", number=1000)
# elapsed_dle = timeit.timeit("[ent.set_dirty() for ent in all_dle]", setup="from __main__ import all_dle", number=1000)
#
# print("dirty flag:", elapsed_de, elapsed_de / total_count)
# print("dirty list:", elapsed_dle, elapsed_dle / total_count)

def de_loop():
    # set some dirty
    for i in range(dirty_count):
        all_de[i].set_dirty()

    # clear dirty
    for e in all_de:
        if e.dirty:
            # do stuff
            e.dirty = False


def de_loop_filter():
    # set some dirty
    for i in range(dirty_count):
        all_de[i].set_dirty()

    # clear dirty
    for e in filter(lambda x: x.dirty, all_de):
        # do stuff
        e.dirty = False


def de_loop_list_comprehension():
    # set some dirty
    for i in range(dirty_count):
        all_de[i].set_dirty()

    # clear dirty
    for e in [ent for ent in all_de if ent.dirty is True]:
        # do stuff
        e.dirty = False


def dle_loop():
    # set some dirty
    for i in range(dirty_count):
        all_dle[i].set_dirty()

    # clear dirty
    for d in DirtyListEntity.dirty:
        # do stuff
        pass
    DirtyListEntity.dirty[:] = []


elapsed = timeit.timeit("de_loop()", setup="from __main__ import de_loop", number=1000)
print("dirty flag  :", elapsed, elapsed / total_count)

elapsed = timeit.timeit("de_loop_filter()", setup="from __main__ import de_loop_filter", number=1000)
print("dirty filter:", elapsed, elapsed / total_count)

elapsed = timeit.timeit("dle_loop()", setup="from __main__ import dle_loop", number=1000)
print("dirty list  :", elapsed, elapsed / total_count)

elapsed = timeit.timeit("de_loop_list_comprehension()", setup="from __main__ import de_loop_list_comprehension", number=1000)
print("dirty compre:", elapsed, elapsed / total_count)

# py -3 filter_vs_listcomprehension_vs_append.py
# dirty flag  : 4.0219702866135805 0.00040219702866135803
# dirty filter: 5.260019928667345 0.0005260019928667345
# dirty list  : 3.0341549988453345 0.0003034154998845334
#
# py -2 filter_vs_listcomprehension_vs_append.py
# dirty flag  : 3.97934089995 0.000397934089995
# dirty filter: 4.76420452116 0.000476420452116
# dirty list  : 2.61366867094 0.000261366867094

