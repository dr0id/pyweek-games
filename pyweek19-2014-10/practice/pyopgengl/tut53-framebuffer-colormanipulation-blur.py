# -*- coding: utf-8 -*-
"""
A 'modern' OpenGL example using pygame and pyopengl

Based on

https://open.gl/framebuffers
"""
from __future__ import print_function
import glpy

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 

import os

from math import radians

from ctypes import c_float, c_void_p, sizeof

import pygame

from OpenGL.GL import glGenBuffers, glBindBuffer, GL_ARRAY_BUFFER, glBufferData, GL_STATIC_DRAW, glCreateShader, \
    GL_VERTEX_SHADER, glShaderSource, glCompileShader, glGetShaderiv, GL_COMPILE_STATUS, GL_TRUE, glGetShaderInfoLog, \
    GL_FRAGMENT_SHADER, glCreateProgram, glAttachShader, glBindFragDataLocation, glLinkProgram, \
    glUseProgram, GL_FLOAT, glGetAttribLocation, glVertexAttribPointer, glEnableVertexAttribArray, \
    glGenVertexArrays, glBindVertexArray, glClearColor, \
    glClear, glDeleteProgram, glDeleteShader, glDeleteBuffers, \
    glDeleteVertexArrays, GL_FALSE, GL_TRIANGLES, GL_COLOR_BUFFER_BIT, GL_DEPTH_BUFFER_BIT, \
    GL_ELEMENT_ARRAY_BUFFER, GL_UNSIGNED_INT, glDrawElements, glTexImage2D, GL_TEXTURE_2D, GL_RGB, \
    GL_UNSIGNED_BYTE, glDeleteTextures, glGenTextures, glTexParameteri, GL_TEXTURE_WRAP_S, \
    GL_CLAMP_TO_EDGE, GL_TEXTURE_WRAP_T, GL_TEXTURE_MIN_FILTER, GL_LINEAR, GL_TEXTURE_MAG_FILTER, \
    glActiveTexture, GL_TEXTURE0, GL_TEXTURE1, glBindTexture, glUniform1i, glGetUniformLocation, \
    glUniformMatrix4fv, glDrawArrays, glEnable, GL_DEPTH_TEST, glDepthMask, GL_STENCIL_TEST, glDisable, \
    glStencilFunc, GL_ALWAYS, glStencilOp, GL_KEEP, GL_KEEP, GL_REPLACE, glStencilMask, GL_STENCIL_BUFFER_BIT, \
    GL_EQUAL, glUniform3f, glGenFramebuffers, glBindFramebuffer, GL_FRAMEBUFFER, \
    glFramebufferTexture2D, GL_COLOR_ATTACHMENT0, glCheckFramebufferStatus, GL_FRAMEBUFFER_COMPLETE, \
    glGenRenderbuffers, glBindRenderbuffer, GL_RENDERBUFFER, glRenderbufferStorage, GL_RENDERBUFFER, \
    GL_DEPTH24_STENCIL8, glFramebufferRenderbuffer, GL_DEPTH_STENCIL_ATTACHMENT, \
    glDeleteRenderbuffers, glDeleteFramebuffers, GL_LINK_STATUS, glGetProgramiv, glGetProgramInfoLog, \
    glReadPixels, GLuint, glDrawBuffers


# enable error when array data is copied
from OpenGL.arrays import numpymodule

numpymodule.NumpyHandler.ERROR_ON_COPY = True

import glmath

SIZE_OF_FLOAT = sizeof(c_float)
SIZE_OF_GL_FLOAT = sizeof(c_float)

# shader sources

scene_vertex_source = """
#version 150 core

in vec3 position;
in vec3 color;
in vec2 texcoord;

out vec3 Color;
out vec2 Texcoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;
uniform vec3 overrideColor;

void main()
{
    Color = color * overrideColor;
    Texcoord = texcoord;
    gl_Position = proj * view * model * vec4(position, 1.0);
}
"""

scene_fragment_source = """
#version 150 core

in vec3 Color;
in vec2 Texcoord;

out vec4 outColor;

uniform sampler2D texKitten;
uniform sampler2D texPuppy;

void main()
{
    vec4 colKitten = texture(texKitten, Texcoord);
    vec4 colPuppy = texture(texPuppy, Texcoord);
    outColor = vec4(Color, 1.0) * mix(colKitten, colPuppy, 0.5);
}
"""

screen_vertex_source = """
# version 150 core

in vec2 position;
in vec2 texcoord;

out vec2 Texcoord;

void main()
{
    Texcoord = texcoord;
    gl_Position = vec4(position, 0.0, 1.0);
}
"""

screen_fragment_source = """
# version 150 core
in vec2 Texcoord;
out vec4 outColor;
uniform sampler2D texFrameBuffer;

const float blurSizeH = 1.0 / 300.0;
const float blurSizeV = 1.0 / 200.0;

void main(){
    vec4 sum = vec4(0.0);
    for(int x = -4; x <= 4; x++){
        for(int y = -4; y <=4; y++){
            sum += texture(texFrameBuffer, vec2(Texcoord.x + x * blurSizeH, Texcoord.y + y * blurSizeV)) / 81.0;
        }
    }
    outColor = sum;
};
"""


# vertices
# position   color         tex coord
# x     y    z    r     g    b   u    v
cube_vertices = [
    -0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 0.0,
    0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
    0.5, 0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
    0.5, 0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
    -0.5, 0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
    -0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 0.0,

    -0.5, -0.5, 0.5, 1.0, 1.0, 1.0, 0.0, 0.0,
    0.5, -0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
    0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
    0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
    -0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
    -0.5, -0.5, 0.5, 1.0, 1.0, 1.0, 0.0, 0.0,

    -0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
    -0.5, 0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
    -0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
    -0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
    -0.5, -0.5, 0.5, 1.0, 1.0, 1.0, 0.0, 0.0,
    -0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 0.0,

    0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
    0.5, 0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
    0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
    0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
    0.5, -0.5, 0.5, 1.0, 1.0, 1.0, 0.0, 0.0,
    0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 0.0,

    -0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
    0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
    0.5, -0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
    0.5, -0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
    -0.5, -0.5, 0.5, 1.0, 1.0, 1.0, 0.0, 0.0,
    -0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,

    -0.5, 0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
    0.5, 0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
    0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
    0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
    -0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 0.0, 0.0,
    -0.5, 0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,

    # plane
    -1.0, -1.0, -0.5, 0.0, 0.0, 0.0, 0.0, 0.0,
    1.0, -1.0, -0.5, 0.0, 0.0, 0.0, 1.0, 0.0,
    1.0, 1.0, -0.5, 0.0, 0.0, 0.0, 1.0, 1.0,
    1.0, 1.0, -0.5, 0.0, 0.0, 0.0, 1.0, 1.0,
    -1.0, 1.0, -0.5, 0.0, 0.0, 0.0, 0.0, 1.0,
    -1.0, -1.0, -0.5, 0.0, 0.0, 0.0, 0.0, 0.0,
]

quad_vertices = [
    -1.0, 1.0, 0.0, 1.0,
    1.0, 1.0, 1.0, 1.0,
    1.0, -1.0, 1.0, 0.0,

    1.0, -1.0, 1.0, 0.0,
    -1.0, -1.0, 0.0, 0.0,
    -1.0, 1.0, 0.0, 1.0
]


# quad_vertices = list(map(lambda x: 0.5 * x, quad_vertices))


def load_texture(path):
    texture = glGenTextures(1)

    glBindTexture(GL_TEXTURE_2D, texture)
    _image = pygame.image.load(path)
    _image = pygame.transform.flip(_image, False, True)
    width, height = _image.get_size()
    image = pygame.image.tostring(_image, "RGB", 1)  # get image data as array

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image)

    del _image
    del image

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)

    return texture


def create_shader_program(vertex_source, fragment_source):
    # create and compile the vertex shader
    vertex_shader = glCreateShader(GL_VERTEX_SHADER)
    glShaderSource(vertex_shader, vertex_source)
    glCompileShader(vertex_shader)

    if glGetShaderiv(vertex_shader, GL_COMPILE_STATUS) != GL_TRUE:
        raise RuntimeError(glGetShaderInfoLog(vertex_shader))

    # create and compile the fragment shader
    fragment_shader = glCreateShader(GL_FRAGMENT_SHADER)
    glShaderSource(fragment_shader, fragment_source)
    glCompileShader(fragment_shader)

    if glGetShaderiv(fragment_shader, GL_COMPILE_STATUS) != GL_TRUE:
        raise RuntimeError(glGetShaderInfoLog(fragment_shader))

    # link the vertex and fragment shader into a shader program
    shader_program = glCreateProgram()
    glAttachShader(shader_program, vertex_shader)
    glAttachShader(shader_program, fragment_shader)
    glBindFragDataLocation(shader_program, 0, "outColor")
    glLinkProgram(shader_program)

    if glGetProgramiv(shader_program, GL_LINK_STATUS) != GL_TRUE:
        raise RuntimeError(glGetProgramInfoLog(shader_program))

    return vertex_shader, fragment_shader, shader_program


def specify_scene_vertex_attributes(shader_program):
    pos_attrib = glGetAttribLocation(shader_program, "position")
    glEnableVertexAttribArray(pos_attrib)
    glVertexAttribPointer(pos_attrib, 3, GL_FLOAT, GL_FALSE, 8 * SIZE_OF_GL_FLOAT, c_void_p(0))

    color_attrib = glGetAttribLocation(shader_program, "color")
    glEnableVertexAttribArray(color_attrib)
    glVertexAttribPointer(color_attrib, 3, GL_FLOAT, GL_FALSE, 8 * SIZE_OF_GL_FLOAT, c_void_p(3 * SIZE_OF_GL_FLOAT))

    tex_attrib = glGetAttribLocation(shader_program, "texcoord")
    glEnableVertexAttribArray(tex_attrib)
    glVertexAttribPointer(tex_attrib, 2, GL_FLOAT, GL_FALSE, 8 * SIZE_OF_GL_FLOAT, c_void_p(6 * SIZE_OF_GL_FLOAT))


def specify_screen_vertex_attributes(shader_program):
    pos_attrib = glGetAttribLocation(shader_program, "position")
    glEnableVertexAttribArray(pos_attrib)
    glVertexAttribPointer(pos_attrib, 2, GL_FLOAT, GL_FALSE, 4 * SIZE_OF_GL_FLOAT, c_void_p(0))

    tex_attrib = glGetAttribLocation(shader_program, "texcoord")
    glEnableVertexAttribArray(tex_attrib)
    glVertexAttribPointer(tex_attrib, 2, GL_FLOAT, GL_FALSE, 4 * SIZE_OF_GL_FLOAT, c_void_p(2 * SIZE_OF_GL_FLOAT))


def main():
    """
    Main method. In this simple program it contains all the code.

    :raise RuntimeError:
    """

    # init window and context
    screen_width = 800
    screen_height = 600

    pygame.init()
    os.environ['SDL_VIDEO_CENTERED'] = '1'

    pygame.display.gl_set_attribute(pygame.GL_STENCIL_SIZE, 8)
    pygame.display.gl_set_attribute(pygame.GL_DEPTH_SIZE, 24)
    # pygame.display.gl_set_attribute(pygame.GL_MULTISAMPLEBUFFERS, 1)
    # pygame.display.gl_set_attribute(pygame.GL_MULTISAMPLESAMPLES, 2)
    # pygame.display.gl_set_attribute(pygame.GL_SWAP_CONTROL, 0)

    pygame.display.set_caption("OpenGL - framebuffer")

    pygame.display.set_mode((screen_width, screen_height), pygame.OPENGL | pygame.DOUBLEBUF)
    # pygame.display.set_mode((screen_width, screen_height), pygame.OPENGL)

    # these would be nice to configure in the openGL context, but I don't know how (if its
    # even possible using pygame)
    #
    # SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    # SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    # SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);

    # create VAO
    vao_cube = glGenVertexArrays(1)
    vao_quad = glGenVertexArrays(1)

    # load vertex data
    vbo_cube = glGenBuffers(1)
    vbo_quad = glGenBuffers(1)

    glBindBuffer(GL_ARRAY_BUFFER, vbo_cube)
    glBufferData(GL_ARRAY_BUFFER, glpy.get_float_ptr(cube_vertices), GL_STATIC_DRAW)

    glBindBuffer(GL_ARRAY_BUFFER, vbo_quad)
    glBufferData(GL_ARRAY_BUFFER, glpy.get_float_ptr(quad_vertices), GL_STATIC_DRAW)

    # create shader programs
    scene_vertex_shader, scene_fragment_shader, scene_shader_program = create_shader_program(scene_vertex_source,
                                                                                             scene_fragment_source)
    screen_vertex_shader, screen_fragment_shader, screen_shader_program = create_shader_program(screen_vertex_source,
                                                                                                screen_fragment_source)

    # specify the layout of the vertex data
    glBindVertexArray(vao_cube)
    glBindBuffer(GL_ARRAY_BUFFER, vbo_cube)
    specify_scene_vertex_attributes(scene_shader_program)

    glBindVertexArray(vao_quad)
    glBindBuffer(GL_ARRAY_BUFFER, vbo_quad)
    specify_screen_vertex_attributes(screen_shader_program)

    # load textures
    tex_kitten = load_texture("sample.png")
    tex_puppy = load_texture("sample2.png")

    glUseProgram(scene_shader_program)
    glUniform1i(glGetUniformLocation(scene_shader_program, "texKitten"), 0)
    glUniform1i(glGetUniformLocation(scene_shader_program, "texPuppy"), 1)

    glUseProgram(screen_shader_program)
    glUniform1i(glGetUniformLocation(screen_shader_program, "texFramebuffer"), 0)

    uni_model = glGetUniformLocation(scene_shader_program, "model")

    # create framge buffer
    frame_buffer = glGenFramebuffers(1)
    glBindFramebuffer(GL_FRAMEBUFFER, frame_buffer)

    # create texture to hold color buffer
    tex_color_buffer = glGenTextures(1)
    glBindTexture(GL_TEXTURE_2D, tex_color_buffer)

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, screen_width, screen_height, 0, GL_RGB, GL_UNSIGNED_BYTE, None)

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex_color_buffer, 0)

    # create render buffer
    rbo_depth_stencil = glGenRenderbuffers(1)
    glBindRenderbuffer(GL_RENDERBUFFER, rbo_depth_stencil)
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, screen_width, screen_height)
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo_depth_stencil)

    # setup projection
    glUseProgram(scene_shader_program)  # this deviates from original, seems it must be set to use th euniforms
    view = glmath.look_at([2.5, 2.5, 2.5],
                       [0.0, 0.0, 0.0],
                       [0.0, 0.0, 1.0])
    uni_view = glGetUniformLocation(scene_shader_program, "view")
    glUniformMatrix4fv(uni_view, 1, GL_FALSE, glpy.get_float_ptr(view))

    proj = glmath.perspective(radians(45.0), float(screen_width)/ screen_height, 1.0, 10.0)
    uni_proj = glGetUniformLocation(scene_shader_program, "proj")
    glUniformMatrix4fv(uni_proj, 1, GL_FALSE, glpy.get_float_ptr(proj))

    uni_color = glGetUniformLocation(scene_shader_program, "overrideColor")


    # check frame buffer creation
    frame_buffer_status = glCheckFramebufferStatus(GL_FRAMEBUFFER)
    if (GL_FRAMEBUFFER_COMPLETE != frame_buffer_status):
        raise RuntimeError('framebuffer not complete!')

    # main loop
    clock = pygame.time.Clock()
    running = True

    while running:
        clock.tick(60)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False

        # bind our framebuffer and draw 3D scene (spinning cube)
        glBindFramebuffer(GL_FRAMEBUFFER, frame_buffer)
        glBindVertexArray(vao_cube)
        glEnable(GL_DEPTH_TEST)
        glUseProgram(scene_shader_program)

        glActiveTexture(GL_TEXTURE0)
        glBindTexture(GL_TEXTURE_2D, tex_kitten)
        glActiveTexture(GL_TEXTURE1)
        glBindTexture(GL_TEXTURE_2D, tex_puppy)

        # clear screen to white
        glClearColor(1.0, 1.0, 1.0, 1.0)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        # calculate transform
        duration = pygame.time.get_ticks() / 1000.0  # convert to seconds
        model = glmath.rotation4(radians(180.0) * duration * 0.1, [0.0, 0.0, 1.0])

        glUniformMatrix4fv(uni_model, 1, GL_FALSE, glpy.get_float_ptr(model))
        glUniformMatrix4fv(uni_view, 1, GL_FALSE, glpy.get_float_ptr(view))
        glUniformMatrix4fv(uni_proj, 1, GL_FALSE, glpy.get_float_ptr(proj))

        # draw cube
        glDrawArrays(GL_TRIANGLES, 0, 36)

        glEnable(GL_STENCIL_TEST)

        # draw floor
        glStencilFunc(GL_ALWAYS, 1, 0xFF)
        glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE)
        glStencilMask(0xFF)
        glDepthMask(GL_FALSE)
        glClear(GL_STENCIL_BUFFER_BIT)

        glDrawArrays(GL_TRIANGLES, 36, 6)

        # draw cube reflection
        glStencilFunc(GL_EQUAL, 1, 0xFF)
        glStencilMask(0x00)
        glDepthMask(GL_TRUE)

        model = glmath.scale4(glmath.translate4(model, 0.0, 0.0, -1.0), 1.0, 1.0, -1.0)
        glUniformMatrix4fv(uni_model, 1, GL_FALSE, glpy.get_float_ptr(model))

        glUniform3f(uni_color, 0.3, 0.3, 0.3)
        glDrawArrays(GL_TRIANGLES, 0, 36)
        glUniform3f(uni_color, 1.0, 1.0, 1.0)

        glDisable(GL_STENCIL_TEST)

        # bind default framebuffer and draw contents of our framebuffer
        glBindFramebuffer(GL_FRAMEBUFFER, 0)
        glBindVertexArray(vao_quad)
        glDisable(GL_DEPTH_TEST)
        glUseProgram(screen_shader_program)

        glActiveTexture(GL_TEXTURE0)
        glBindTexture(GL_TEXTURE_2D, tex_color_buffer)

        glDrawArrays(GL_TRIANGLES, 0, 6)

        pygame.display.flip()

    # delete
    glDeleteRenderbuffers(1, [rbo_depth_stencil])
    glDeleteTextures(tex_color_buffer)
    glDeleteFramebuffers([frame_buffer])

    glDeleteTextures([tex_kitten, tex_puppy])

    glDeleteProgram(screen_shader_program)
    glDeleteShader(scene_fragment_shader)
    glDeleteShader(scene_vertex_shader)

    glDeleteProgram(screen_shader_program)
    glDeleteShader(screen_fragment_shader)
    glDeleteShader(screen_vertex_shader)

    glDeleteBuffers(2, [vbo_cube, vbo_quad])

    glDeleteVertexArrays(2, [vao_cube, vao_quad])


if __name__ == '__main__':
    main()
