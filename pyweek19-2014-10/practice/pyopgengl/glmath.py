# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek19-2014-10
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description

glm - openGL math (not related to the GLM class in C).


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function
import sys
from math import cos, sin, radians, tan

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["look_at", "orthographic"]  # list of public visible parts of this module


_3D = 3
_X, _Y, _Z = range(_3D)


def m4x4(d=1.0):
    return diagonal4(d)


def identity4():
    return diagonal4(1.0)


def diagonal4(v4):
    return [
        v4, 0.0, 0.0, 0.0,
        0.0, v4, 0.0, 0.0,
        0.0, 0.0, v4, 0.0,
        0.0, 0.0, 0.0, v4,
    ]


def flip_y_m4():
    v4 = 1.0
    return [
        v4,  0.0, 0.0, 0.0,
        0.0, -v4, 0.0, 0.0,
        0.0, 0.0, v4, 0.0,
        0.0, 0.0, 0.0, v4,
    ]


def flip_z_m4():
    v4 = 1.0
    return [
        v4,  0.0, 0.0, 0.0,
        0.0,  v4, 0.0, 0.0,
        0.0, 0.0, -v4, 0.0,
        0.0, 0.0, 0.0, v4,
    ]


def multsv3(s, v3):
    assert len(v3) == 3
    return [s * v3[0], s * v3[1], s * v3[2]]


def rotation_z4(angle):
    c = cos(angle)
    s = sin(angle)
    return [
        c, s, 0.0, 0.0,
        -s, c, 0.0, 0.0,
        0.0, 0.0, 1.0, 0.0,
        0.0, 0.0, 0.0, 1.0
    ]


def rotate_z4(m4, a):
    assert len(m4) == 16
    c = cos(a)
    s = sin(a)

    return [
        m4[0] * c + m4[4] * s,
        m4[1] * c + m4[5] * s,
        m4[2] * c + m4[6] * s,
        m4[3] * c + m4[7] * s,

        m4[0] * -s + m4[4] * c,
        m4[1] * -s + m4[5] * c,
        m4[2] * -s + m4[6] * c,
        m4[3] * -s + m4[7] * c,

        m4[8],
        m4[9],
        m4[10],
        m4[11],

        m4[12],
        m4[13],
        m4[14],
        m4[15],
    ]


def rotation4(angle, v):
    return rotate4(identity4(), angle, v)


def rotate4(m, angle, v):
    assert len(v) == 3
    assert len(m) == 16

    c = cos(angle)
    s = sin(angle)

    axis = normalize3(v)

    _x = 1.0 - c
    temp = multsv3(_x, axis)

    rotate_00 = c + temp[0] * axis[0]
    rotate_01 = 0 + temp[0] * axis[1] + s * axis[2]
    rotate_02 = 0 + temp[0] * axis[2] - s * axis[1]

    rotate_10 = 0 + temp[1] * axis[0] - s * axis[2]
    rotate_11 = c + temp[1] * axis[1]
    rotate_12 = 0 + temp[1] * axis[2] + s * axis[0]

    rotate_20 = 0 + temp[2] * axis[0] + s * axis[1]
    rotate_21 = 0 + temp[2] * axis[1] - s * axis[0]
    rotate_22 = c + temp[2] * axis[2]

    return [
        m[0] * rotate_00 + m[4] * rotate_01 + m[8] * rotate_02,
        m[1] * rotate_00 + m[5] * rotate_01 + m[9] * rotate_02,
        m[2] * rotate_00 + m[6] * rotate_01 + m[10] * rotate_02,
        0.0,

        m[0] * rotate_10 + m[4] * rotate_11 + m[8] * rotate_12,
        m[1] * rotate_10 + m[5] * rotate_11 + m[9] * rotate_12,
        m[2] * rotate_10 + m[6] * rotate_11 + m[10] * rotate_12,
        0.0,

        m[0] * rotate_20 + m[4] * rotate_21 + m[8] * rotate_22,
        m[1] * rotate_20 + m[5] * rotate_21 + m[9] * rotate_22,
        m[2] * rotate_20 + m[6] * rotate_21 + m[10] * rotate_22,
        0.0,

        m[12],
        m[13],
        m[14],
        m[15]
    ]


def transpose4(m4):
    # TODO: check length?
    assert len(m4) == 16, "passed in matrix should have 16 elements: " + str(m4)
    return [
        m4[0], m4[4], m4[8], m4[12],
        m4[1], m4[5], m4[9], m4[13],
        m4[2], m4[6], m4[10], m4[14],
        m4[3], m4[7], m4[11], m4[15],
    ]


def get_axis(m4):
    """
    :param m4: ortho-normal matrix
    :return: x-, y-, z- axis and position as v3 vectors
    """
    # TODO: assert axis are unit length
    # TODO: assert axis are orthogonal
    return m4[0:3], m4[4:7], m4[8:11], m4[12:15]


def invert_orthonormal(m4):
    """

    :param m4: ortho-normal matrix
    :return: inverted matrix
    """
    # TODO: assert axis are unit length
    # TODO: assert axis are orthogonal
    return [
        m4[0], m4[4], m4[8], -m4[3],
        m4[1], m4[5], m4[9], -m4[7],
        m4[2], m4[6], m4[10], -m4[11],
        # m4[12], m4[13], m4[14], m4[15],
        0.0, 0.0, 0.0, 1.0,
    ]


def negatev3(v3):
    return [-v3[0], -v3[1], -v3[2]]


def scale4(m, sx, sy, sz):
    return [
        m[0] * sx, m[1] * sx, m[2] * sx, m[3] * sx,
        m[4] * sy, m[5] * sy, m[6] * sy, m[7] * sy,
        m[8] * sz, m[9] * sz, m[10] * sz, m[11] * sz,
        m[12], m[13], m[14], m[15]
    ]


def translate4(m, tx, ty, tz):
    result = list(m)
    result[12] = m[0] * tx + m[4] * ty + m[8] * tz + m[12]
    result[13] = m[1] * tx + m[5] * ty + m[9] * tz + m[13]
    result[14] = m[2] * tx + m[6] * ty + m[10] * tz + m[14]
    result[15] = m[3] * tx + m[7] * ty + m[11] * tz + m[15]
    return result


def normalize3(v3, precision=10**-8, return_zero_vector=True):
    # TODO: precision parameter
    # TODO: potential zero division!
    assert len(v3) == 3
    length = (v3[0] ** 2 + v3[1] ** 2 + v3[2] ** 2) ** 0.5
    if return_zero_vector and length < precision:
        return v3
    return v3[0] / length, v3[1] / length, v3[2] / length


def addv3(v3, w3):
    assert len(v3) == 3, "1st vector should have 3 elements: " + str(v3)
    assert len(w3) == 3, "2nd vector should have 3 elements: " + str(w3)
    return v3[0] + w3[0], v3[1] + w3[1], v3[2] + w3[2]


def subv3(v3, w3):
    assert len(v3) == 3, "1st vector should have 3 elements: " + str(v3)
    assert len(w3) == 3, "2nd vector should have 3 elements: " + str(w3)
    return v3[0] - w3[0], v3[1] - w3[1], v3[2] - w3[2]


def cross3(v3, w3):
    assert len(v3) == len(w3), "vector dimensions do not match"
    assert len(v3) == 3, "vectors should have dimension 3"
    return v3[1] * w3[2] - v3[2] * w3[1], v3[2] * w3[0] - v3[0] * w3[2], v3[0] * w3[1] - v3[1] * w3[0]

# def _cross3(v3, w3):
#     assert len(v3) == len(w3), "vector dimensions do not match"
#     assert len(v3) == 3, "vectors should have dimension 3"
#     vx, vy, vz = v3
#     wx, wy, wz = w3
#
#     return vy * wz - vz * wy, vz * wx - vx * wz, vx * wy - vy * wx


def dot3(v3, w3):
    assert len(v3) == len(w3), "vector dimensions do not match"
    assert len(v3) == 3, "vectors should have dimension 3"
    return v3[0] * w3[0] + v3[1] * w3[1] + v3[2] * w3[2]


# def _dot3(v3, w3):
#     assert len(v3) == len(w3), "vector dimensions do not match"
#     assert len(v3) == 3, "vectors should have dimension 3"
#     vx, vy, vz = v3
#     wx, wy, wz = w3
#     return vx * wx + vy * wy + vz * wz

#
# def v3(x, y, z):
#     return x, y, z


def look_at(eye3, center3, up3):
    assert len(eye3) == 3
    assert len(center3) == 3
    assert len(up3) == 3
    f = normalize3(subv3(center3, eye3))
    s = normalize3(cross3(f, up3))
    u = cross3(s, f)  # normalize should not be needed since s and f are already unit vectors

    return [
        s[0], u[0], -f[0], 0.0,
        s[1], u[1], -f[1], 0.0,
        s[2], u[2], -f[2], 0.0,
        -dot3(s, eye3), -dot3(u, eye3), dot3(f, eye3), 1.0,  # translation, dot3() is row * col of matrix multiplication
    ]


def perspective(fov_y, aspect, z_near, z_far, epsilon=0.00001):
    """
    Creates the projection matrix.
    See: https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/gluPerspective.xml

    :param fov_y: Specifies the field of view angle, in degrees, in the y direction
    :param aspect: Specifies the aspect ratio that determines the field of view in the x direction.
                    The aspect ratio is the ratio of x (width) to y (height).
    :param z_near: Specifies the distance from the viewer to the near clipping plane (always positive).
    :param z_far:Specifies the distance from the viewer to the far clipping plane (always positive).
    :param epsilon: epsilon to use to check for 0, numbers <= epsilon will be rounded to 0
    :return: perspective 4x4 matrix as a list of 16 floats
    """
    assert abs(aspect - epsilon) > 0.0, "aspect should be > 0"
    assert z_far > z_near, "z_far should be bigger than z_near"
    assert z_near > epsilon, "z_near should be bigger than > epsilon"
    tan_half_fov_y = tan(radians(fov_y) / 2.0)

    return [
        1.0 / (1.0 * aspect * tan_half_fov_y),  0.0,                    0.0,                                        0.0,
        0.0,                                    1.0 / tan_half_fov_y,   0.0,                                        0.0,
        0.0,                                    0.0,                    -1.0 * (z_far + z_near) / (z_far - z_near), -1.0,
        0.0,                                    0.0,                     -(2.0 * z_far * z_near) / (z_far - z_near), 0.0
    ]


def perspective_frustum(z_near, z_far, x_left, x_right, y_top, y_bottom, epsilon=0.00001):
    """
    Creates the projection matrix according to:
    http://www.songho.ca/opengl/gl_projectionmatrix.html#perspective

    :param z_near: near plane coordinate
    :param z_far: far plane coordinate
    :param x_left: left plane coordinate
    :param x_right: right plane coordinate
    :param y_top: top plane coordinate
    :param y_bottom: bottom plane coordinate
    :param epsilon: epsilon to use to check for 0, numbers <= epsilon will be rounded to 0
    :return: perspective 4x4 matrix as a list of 16 floats
    """
    assert z_far > z_near, "z_far should be bigger than z_near"
    assert abs(z_near) > epsilon, "z_near should be bigger than > epsilon"
    assert x_left < x_right, "should be x_left < x_right"
    assert y_top > y_bottom, "should be y_top > y_bottom"
    _n = z_near
    _f = z_far
    _l = x_left
    _r = x_right
    _t = y_top
    _b = y_bottom
    r_minus_l = _r - _l
    t_minus_b = _t - _b
    two_n = 2 * _n
    return [
        two_n / r_minus_l,      0.0,                    0.0,                        0.0,
        0.0,                    two_n / t_minus_b,      0.0,                        0.0,
        (_r + _l) / r_minus_l,  (_t + _b) / t_minus_b,  -(_f + _n) / (_f - _n),     -1.0,
        0.0,                    0.0,                    (-_f * two_n) / (_f - _n),  0.0
    ]


def perspective_frustum_symmetric(z_near, z_far, width, height, epsilon=0.00001):
    """
    Creates a projection matrix according to:
    http://www.songho.ca/opengl/gl_projectionmatrix.html#perspective

    :param z_near: near plane coordinate
    :param z_far: far plane coordinate
    :param width: width of the frustum
    :param height: height of the frustum
    :param epsilon: epsilon to use to check for 0, numbers <= epsilon will be rounded to 0
    :return: perspective 4x4 matrix as a list of 16 floats
    """
    _r = width / 2.0
    _t = height / 2.0
    return perspective_frustum(z_near, z_far, -_r, _r, _t, -_t, epsilon)


def orthographic(z_near, z_far, x_left, x_right, y_top, y_bottom, epsilon=0.00001):
    """
    Creates the orthographic projection matrix according to:
    http://www.songho.ca/opengl/gl_projectionmatrix.html#perspective

    :param z_near: near plane coordinate, from position the cam
    :param z_far: far plane coordinate, from the position of the cam
    :param x_left: left plane coordinate
    :param x_right: right plane coordinate
    :param y_top: top plane coordinate
    :param y_bottom: bottom plane coordinate
    :param epsilon: epsilon to use to check for 0, numbers <= epsilon will be rounded to 0
    :return: perspective 4x4 matrix as a list of 16 floats
    """
    assert z_far > z_near, "z_far should be bigger than z_near"
    assert abs(z_near) > epsilon, "z_near should be bigger than > epsilon"
    assert x_left < x_right, "should be x_left < x_right"
    assert y_top > y_bottom, "should be y_top > y_bottom"
    _n = z_near
    _f = z_far
    _l = x_left
    _r = x_right
    _t = y_top
    _b = y_bottom
    return [
        2.0/(_r - _l),          0.0,                    0.0,                    0.0,
        0.0,                    2.0/(_t - _b),          0.0,                    0.0,
        0.0,                    0.0,                    -2.0/(_f - _n),         0.0,
        -(_r + _l)/(_r - _l),   -(_t + _b)/(_t - _b),   -(_f + _n)/(_f - _n),   1.0
    ]


def orthographic_symmetric(z_near, z_far, width, height, epsilon=0.00001):
    _r = width // 2
    _h = height // 2
    return orthographic(z_near, z_far, -_r, _r, _h, -_h, epsilon)


def print_m4(m, title="", p=5, fp=sys.stdout):
    import os

    if title:
        fp.write(title + os.linesep)

    t = transpose4(m)
    c1 = t[:4]
    c2 = t[4:8]
    c3 = t[8:12]
    c4 = t[12:]
    col_format = "{{0:+.{0}e}}, {{1:+.{0}e}}, {{2:+.{0}e}}, {{3:+.{0}e}}".format(p)
    fp.write(os.linesep.join([col_format.format(*c1),
                              col_format.format(*c2),
                              col_format.format(*c3),
                              col_format.format(*c4),
                              os.linesep
                              ]))


# # pretty slow
# from itertools import izip
# def subv(a, b):
#     assert len(a) == len(b)
#     return [ae - be for ae, be in izip(a, b)]
#
#
# # pretty slow
# def dot(v, w):
#     assert len(v) == len(w), "vector dimensions do not match"
#     return sum([x * y for x, y in izip(v, w)])


# this method is slower than the other
def __transpose(m, dim=4):
    # return m[0:13:4] + m[1:14:4] + m[2:15:4] + m[3:16:4]
    result = []
    for i in range(dim):
        result += m[i: dim * dim - dim + i + 1: dim]
    return result


def rotation_x4(angle):
    c = cos(angle)
    s = sin(angle)
    return [
        1.0, 0.0, 0.0, 0.0,
        0.0, c, s, 0.0,
        0.0, -s, c, 0.0,
        0.0, 0.0, 0.0, 1.0
    ]


def rotate_x4(m, angle):
    assert len(m) == 16
    c = cos(angle)
    s = sin(angle)

    return [
        m[0],
        m[1],
        m[2],
        m[3],

        m[4] * c + m[8] * s,
        m[5] * c + m[9] * s,
        m[6] * c + m[10] * s,
        m[7] * c + m[11] * s,

        m[4] * -s + m[8] * c,
        m[5] * -s + m[9] * c,
        m[6] * -s + m[10] * c,
        m[7] * -s + m[11] * c,

        m[12],
        m[13],
        m[14],
        m[15]
    ]


def rotation_y4(angle):
    c = cos(angle)
    s = sin(angle)

    return [
        c, 0.0, -s, 0.0,
        0.0, 1.0, 0.0, 0.0,
        s, 0.0, c, 0.0,
        0.0, 0.0, 0.0, 1.0
    ]


def rotate_y4(m, angle):
    c = cos(angle)
    s = sin(angle)

    return [
        m[0] * c - m[8] * s,
        m[1] * c - m[9] * s,
        m[2] * c - m[10] * s,
        m[3] * c - m[11] * s,

        m[4],
        m[5],
        m[6],
        m[7],

        m[0] * s + m[8] * c,
        m[1] * s + m[9] * c,
        m[2] * s + m[10] * c,
        m[3] * s + m[11] * c,

        m[12],
        m[13],
        m[14],
        m[15],
    ]


def mult_mat4(a, b):
    """
    Matrix multiplication for 4x4 matrices.
    :param a: 1st matrix
    :param b: 2d matrix
    :return: resulting 4x4 matrix
    """
    return [
        a[0] * b[0] + a[4] * b[1] + a[8] * b[2] + a[12] * b[3],
        a[1] * b[0] + a[5] * b[1] + a[9] * b[2] + a[13] * b[3],
        a[2] * b[0] + a[6] * b[1] + a[10] * b[2] + a[14] * b[3],
        a[3] * b[0] + a[7] * b[1] + a[11] * b[2] + a[15] * b[3],
        a[0] * b[4] + a[4] * b[5] + a[8] * b[6] + a[12] * b[7],
        a[1] * b[4] + a[5] * b[5] + a[9] * b[6] + a[13] * b[7],
        a[2] * b[4] + a[6] * b[5] + a[10] * b[6] + a[14] * b[7],
        a[3] * b[4] + a[7] * b[5] + a[11] * b[6] + a[15] * b[7],
        a[0] * b[8] + a[4] * b[9] + a[8] * b[10] + a[12] * b[11],
        a[1] * b[8] + a[5] * b[9] + a[9] * b[10] + a[13] * b[11],
        a[2] * b[8] + a[6] * b[9] + a[10] * b[10] + a[14] * b[11],
        a[3] * b[8] + a[7] * b[9] + a[11] * b[10] + a[15] * b[11],
        a[0] * b[12] + a[4] * b[13] + a[8] * b[14] + a[12] * b[15],
        a[1] * b[12] + a[5] * b[13] + a[9] * b[14] + a[13] * b[15],
        a[2] * b[12] + a[6] * b[13] + a[10] * b[14] + a[14] * b[15],
        a[3] * b[12] + a[7] * b[13] + a[11] * b[14] + a[15] * b[15]
    ]


def mult_addv3(v1, v2, scalar):
    # print('mult_addv3', v1, v2, scalar)
    r = [v1[0] + v2[0] * scalar, v1[1] + v2[1] * scalar, v1[2] + v2[2] * scalar]
    # print('mult_addv3', r)
    return r


if __name__ == '__main__':

    trans = range(16)
    print_m4(trans)
    print_m4(transpose4(trans))
    print_m4(__transpose(trans))
    trans = identity4()
    print_m4(rotate4(trans, radians(90), [0.0, 0.0, 1.0]))
    print_m4(rotate4(identity4(), radians(90), [1.0, 0.0, 0.0]))
    print_m4(rotate4(identity4(), radians(90), [0.0, 1.0, 0.0]))

    print(normalize3([3, 0, 0]))
    print(normalize3([3, 3, 0]))

    print_m4(look_at([0.0, 0.0, 0.0], [1.0, 0.0, 0.0], [0.0, 0.0, 1.0]), p=10)
    print_m4(look_at([1.0, 1.0, 1.0], [0.0, 0.0, 0.0], [0.0, 0.0, 1.0]))
    print_m4(perspective(radians(45), 800.0 / 600.0, 1.0, 10.0))

    def wrapper(func, *args, **kwargs):
        def wrapped():
            return func(*args, **kwargs)

        return wrapped
    # print(timeit.timeit(wrapper(transpose4, range(16))))
    # print(timeit.timeit(wrapper(__transpose, range(16))))
    # print(timeit.timeit(wrapper(sub3, range(3), range(3))))
    # print(timeit.timeit(wrapper(sub, range(3), range(3))))
    # print(timeit.timeit(wrapper(dot, range(3), range(3))))
    # dot3(range(3), range(3))
    # _dot3(range(3), range(3))
    # print(timeit.timeit(wrapper(_dot3, range(3), range(3))), "_dot3")
    # print(timeit.timeit(wrapper(dot3, range(3), range(3))), "dot3")
    # assert dot3(range(3), range(3)) == _dot3(range(3), range(3))
    # cross3(range(3), range(3))
    # _cross3(range(3), range(3))
    # print(timeit.timeit(wrapper(_cross3, range(3), range(3))), "_cross3")
    # print(timeit.timeit(wrapper(cross3, range(3), range(3))), "cross3")
    # assert cross3(range(3), range(3)) == _cross3(range(3), range(3))
    matrix = m4x4()
    angle_in_rad = radians(45)
    # print(timeit.timeit(wrapper(rotate_z, m, angle)), "rotate_z")
    # print(timeit.timeit(wrapper(_rotate_z, m, angle)), "_rotate_z")
    # assert _rotate_z(m, angle) == rotate_z(m, angle)
    print('???')
    assert rotation_x4(angle_in_rad) == rotate4(matrix, angle_in_rad, [1.0, 0.0, 0.0])
    assert rotation_x4(angle_in_rad) == rotate_x4(matrix, angle_in_rad)
    assert rotation_y4(angle_in_rad) == rotate4(matrix, angle_in_rad, [0.0, 1.0, 0.0])
    assert rotation_y4(angle_in_rad) == rotate_y4(matrix, angle_in_rad)
    assert rotation_z4(angle_in_rad) == rotate4(matrix, angle_in_rad, [0.0, 0.0, 1.0])
    assert rotation_z4(angle_in_rad) == rotate_z4(matrix, angle_in_rad)
    m = m4x4()
    n = m4x4()

    assert m4x4() == mult_mat4(m, n)
    t = translate4(m, 1, 1, 1)
    assert [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1] == mult_mat4(t, n)
    assert [4] * 16 == mult_mat4([1] * 16, [1]*16)


