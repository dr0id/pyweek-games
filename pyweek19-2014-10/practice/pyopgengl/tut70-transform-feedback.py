# -*- coding: utf-8 -*-
"""
A 'modern' OpenGL example using pygame and pyopengl

Based on

https://open.gl/feedback
"""
from __future__ import print_function
from OpenGL.raw.GL._types import GLfloat, GLvoidp
from OpenGL.GL import shaders
import glpy

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 

import os

from ctypes import c_float, c_void_p
import ctypes
sizeof = ctypes.sizeof

import pygame

from OpenGL.GL import glGenBuffers, glBindBuffer, GL_ARRAY_BUFFER, glBufferData, GL_STATIC_DRAW, glCreateShader, \
    GL_VERTEX_SHADER, glShaderSource, glCompileShader, glGetShaderiv, GL_COMPILE_STATUS, GL_TRUE, glGetShaderInfoLog, \
    GL_FRAGMENT_SHADER, glCreateProgram, glAttachShader, glBindFragDataLocation, glLinkProgram, \
    glUseProgram, GL_FLOAT, glGetAttribLocation, glVertexAttribPointer, glEnableVertexAttribArray, \
    glGenVertexArrays, glBindVertexArray, glClearColor, \
    glClear, glDeleteProgram, glDeleteShader, glDeleteBuffers, \
    glDeleteVertexArrays, GL_FALSE, GL_COLOR_BUFFER_BIT, glDrawArrays, GL_LINK_STATUS, glGetProgramiv, glGetProgramInfoLog, \
    GL_POINTS, GL_GEOMETRY_SHADER, glTransformFeedbackVaryings, GL_INTERLEAVED_ATTRIBS, GL_STATIC_READ, glEnable, \
    GL_RASTERIZER_DISCARD, glBindBufferBase, GL_TRANSFORM_FEEDBACK_BUFFER, glBeginTransformFeedback, glDrawArrays, \
    glEndTransformFeedback, glFlush, glGetBufferSubData


# enable error when array data is copied
from OpenGL.arrays import numpymodule

numpymodule.NumpyHandler.ERROR_ON_COPY = True

import glmath

SIZE_OF_FLOAT = sizeof(c_float)
SIZE_OF_GL_FLOAT = sizeof(c_float)

# shader sources

vertex_source = """
#version 150 core

in float inValue;

out float outValue;

void main()
{
    outValue = sqrt(inValue);
}
"""


geometry_source = """
#version 150 core


"""


def _create_and_compile_shader(shader_type, shader_source):
    shader = glCreateShader(shader_type)
    glShaderSource(shader, shader_source)
    glCompileShader(shader)

    if glGetShaderiv(shader, GL_COMPILE_STATUS) != GL_TRUE:
        raise RuntimeError(glGetShaderInfoLog(shader))

    return shader


def create_shader_program(vertex_source, geometry_source=None, fragment_source=None):

    # link the vertex and fragment shader into a shader program
    shader_program = glCreateProgram()

    # create and compile the vertex shader
    vertex_shader = _create_and_compile_shader(GL_VERTEX_SHADER, vertex_source)
    glAttachShader(shader_program, vertex_shader)

    geometry_shader = None
    fragment_shader = None
    if geometry_source:
        # create and compile the geometry shader
        geometry_shader = _create_and_compile_shader(GL_GEOMETRY_SHADER, geometry_source)
        glAttachShader(shader_program, geometry_shader)

    if fragment_source:
        # create and compile the fragment shader
        fragment_shader = _create_and_compile_shader(GL_FRAGMENT_SHADER, fragment_source)
        glAttachShader(shader_program, fragment_shader)

    # glBindFragDataLocation(shader_program, 0, "outColor")
    glLinkProgram(shader_program)

    if glGetProgramiv(shader_program, GL_LINK_STATUS) != GL_TRUE:
        raise RuntimeError(glGetProgramInfoLog(shader_program))

    return vertex_shader, geometry_shader, fragment_shader, shader_program


def main():
    """
    Main method. In this simple program it contains all the code.

    :raise RuntimeError:
    """

    # init window and context
    screen_width = 800
    screen_height = 600

    pygame.init()
    os.environ['SDL_VIDEO_CENTERED'] = '1'

    pygame.display.gl_set_attribute(pygame.GL_STENCIL_SIZE, 8)
    pygame.display.gl_set_attribute(pygame.GL_DEPTH_SIZE, 24)
    # pygame.display.gl_set_attribute(pygame.GL_MULTISAMPLEBUFFERS, 1)
    # pygame.display.gl_set_attribute(pygame.GL_MULTISAMPLESAMPLES, 2)
    # pygame.display.gl_set_attribute(pygame.GL_SWAP_CONTROL, 0)

    pygame.display.set_caption("OpenGL - transform feedback")

    pygame.display.set_mode((screen_width, screen_height), pygame.OPENGL | pygame.DOUBLEBUF)
    # pygame.display.set_mode((screen_width, screen_height), pygame.OPENGL)

    # these would be nice to configure in the openGL context, but I don't know how (if its
    # even possible using pygame)
    #
    # SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    # SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    # SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);

    vertex_shader = shaders.compileShader(vertex_source, GL_VERTEX_SHADER)
    shader_program = shaders.compileProgram(vertex_shader)

    # tell opengl witch data has to be written back
    glTransformFeedbackVaryings(shader_program, 1, glpy.get_char_ptr("outValue"), GL_INTERLEAVED_ATTRIBS)

    # re-link since glTransformFeedbackVaryings modified state in the linking of the program
    glLinkProgram(shader_program)
    glUseProgram(shader_program)  # maybe use with shader_program: ...

    # create vao
    vao = glGenVertexArrays(1)
    glBindVertexArray(vao)

    data = [1.0, 2.0, 3.0, 4.0, 5.0]
    data_ptr = glpy.get_float_ptr(data)
    # data_ptr = (GLfloat * 5)(1.0, 2.0, 3.0, 4.0, 5.0)

    # create vbo
    vbo = glGenBuffers(1)
    glBindBuffer(GL_ARRAY_BUFFER, vbo)
    glBufferData(GL_ARRAY_BUFFER, sizeof(data_ptr), data_ptr, GL_STATIC_DRAW)

    # specify layout of point data
    input_attrib = glGetAttribLocation(shader_program, "inValue")
    glEnableVertexAttribArray(input_attrib)
    glVertexAttribPointer(input_attrib, 1, GL_FLOAT, GL_FALSE, 0, glpy.get_stride(0))


    tbo = glGenBuffers(1)
    glBindBuffer(GL_ARRAY_BUFFER, tbo)
    glBufferData(GL_ARRAY_BUFFER, sizeof(data_ptr), None, GL_STATIC_READ)

    # main loop
    clock = pygame.time.Clock()
    running = True

    while running:
        clock.tick(60)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False

        # render frame
        glEnable(GL_RASTERIZER_DISCARD)
        glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, tbo)
        glBeginTransformFeedback(GL_POINTS)
        glDrawArrays(GL_POINTS, 0, 5)
        glEndTransformFeedback()
        glFlush()

        # @_p.types(None,_cs.GLenum,_cs.GLintptr,_cs.GLsizeiptr,ctypes.c_void_p)
        # glGetBufferSubData(target,offset,size,data):pass
        feedback = (GLfloat * 5)()
        glGetBufferSubData(GL_TRANSFORM_FEEDBACK_BUFFER, 0, sizeof(feedback), feedback)

        print(list(feedback))


    # delete
    glDeleteProgram(shader_program)
    glDeleteShader(vertex_shader)

    glDeleteVertexArrays(1, [vao])
    glDeleteBuffers(1, [vbo])


if __name__ == '__main__':
    main()
