# -*- coding: utf-8 -*-
"""
A 'modern' OpenGL example using pygame and pyopengl

Based on

http://www.gamedev.net/page/resources/_/technical/opengl/opengl-instancing-demystified-r3226
"""
from __future__ import print_function
from OpenGL.GL import GLfloat
from OpenGL.GL import GL_STATIC_DRAW
import glpy

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 

import os

from math import radians

from ctypes import c_float, c_void_p, sizeof

SIZE_OF_FLOAT = sizeof(c_float)

import pygame

# enable error when array data is copied
from OpenGL.arrays import numpymodule
numpymodule.NumpyHandler.ERROR_ON_COPY = True

from OpenGL.GL import shaders, glBufferData

from OpenGL.GL import glGenBuffers, glBindBuffer, GL_ARRAY_BUFFER, GL_VERTEX_SHADER, GL_FRAGMENT_SHADER, \
    glBindFragDataLocation, glLinkProgram, \
    glUseProgram, GL_FLOAT, glGetAttribLocation, glVertexAttribPointer, glEnableVertexAttribArray, \
    glGenVertexArrays, glBindVertexArray, glClearColor, \
    glClear, glDeleteProgram, glDeleteBuffers, \
    glDeleteVertexArrays, GL_FALSE, GL_TRIANGLES, GL_COLOR_BUFFER_BIT, GL_DEPTH_BUFFER_BIT, \
    glTexImage2D, GL_TEXTURE_2D, GL_RGB, \
    GL_UNSIGNED_BYTE, glDeleteTextures, glGenTextures, glTexParameteri, GL_TEXTURE_WRAP_S, \
    GL_CLAMP_TO_EDGE, GL_TEXTURE_WRAP_T, GL_TEXTURE_MIN_FILTER, GL_LINEAR, GL_TEXTURE_MAG_FILTER, \
    glActiveTexture, GL_TEXTURE0, GL_TEXTURE1, glBindTexture, glUniform1i, glGetUniformLocation, \
    glUniformMatrix4fv, glDrawArrays, glEnable, GL_DEPTH_TEST, glVertexAttribDivisor, glDrawArraysInstanced

import glmath

# vertices
# position   color         tex coord
#    x     y    z    r     g    b   u    v
vertices = [
    -0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 0.0,
    0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
    0.5, 0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
    0.5, 0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
    -0.5, 0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
    -0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 0.0,

    -0.5, -0.5, 0.5, 1.0, 1.0, 1.0, 0.0, 0.0,
    0.5, -0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
    0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
    0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
    -0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
    -0.5, -0.5, 0.5, 1.0, 1.0, 1.0, 0.0, 0.0,

    -0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
    -0.5, 0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
    -0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
    -0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
    -0.5, -0.5, 0.5, 1.0, 1.0, 1.0, 0.0, 0.0,
    -0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 0.0,

    0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
    0.5, 0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
    0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
    0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
    0.5, -0.5, 0.5, 1.0, 1.0, 1.0, 0.0, 0.0,
    0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 0.0,

    -0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
    0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
    0.5, -0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
    0.5, -0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
    -0.5, -0.5, 0.5, 1.0, 1.0, 1.0, 0.0, 0.0,
    -0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,

    -0.5, 0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
    0.5, 0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
    0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
    0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
    -0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 0.0, 0.0,
    -0.5, 0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0

]


vertex_source = """
#version 330 core

// define layout data -> glBindAtribLocation(...) -> glGetAttribLocation(...)
layout(location=0)in vec3 color;
layout(location=1)in vec2 texcoord;
layout(location=2)in vec3 position;
layout(location=3) in vec3 offset;

out vec3 Color;
out vec2 Texcoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

void main()
{
    Color = color;
    mat4 t = mat4(1.0);  // translation matrix, init to identity
    t[3] = vec4(offset.xyz, 1.0);  // translation by offset
    mat4 s = mat4(0.3); // scale matrix, diag(0.3)
    s[3][3] = 1.0;  // reset last component to 1.0
    Texcoord = texcoord;
    gl_Position = proj * view * t * model * s * vec4(position, 1.0);
}
"""

fragment_source = """
#version 330 core

in vec3 Color;
in vec2 Texcoord;

out vec4 outColor;

uniform sampler2D texKitten;
uniform sampler2D texPuppy;

void main()
{
    vec4 colKitten = texture(texKitten, Texcoord);
    vec4 colPuppy =texture(texPuppy, Texcoord);
    outColor = vec4(Color, 1.0) * mix(colKitten, colPuppy, 0.5);
}
"""

def main():
    """
    Main method. In this simple program it contains all the code.

    :raise RuntimeError:
    """
    screen_width = 800
    screen_height = 600

    pygame.init()
    os.environ['SDL_VIDEO_CENTERED'] = '1'
    pygame.display.set_mode((screen_width, screen_height), pygame.OPENGL | pygame.DOUBLEBUF)

    # these would be nice to configure in the openGL context, but I don't know how (if its
    # even possible using pygame)
    #
    # SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    # SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    # SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);

    # crating Vertex Array Object so save shader attributes
    vao_box = glGenVertexArrays(1)
    glBindVertexArray(vao_box)

    count = 20
    distance = 0.5
    positions = []
    for x in range(count):
        for y in range(count):
            for z in range(count):
                positions.extend([x * distance, y * distance, z * distance])
    # positions = [0.0, 0.0, 0.0]
    for x in range(1, 2):
        positions.extend([x * distance, 0.0, 0.0])
    for y in range(1, 3):
        positions.extend([0.0, y * distance, 0.0])
    for z in range(1, 4):
        positions.extend([0.0, 0.0, z * distance])


    # setting up shader program
    vertex_shader = shaders.compileShader(vertex_source, GL_VERTEX_SHADER)
    fragment_shader = shaders.compileShader(fragment_source, GL_FRAGMENT_SHADER)
    shader_program = shaders.compileProgram(vertex_shader, fragment_shader)

    # define which output is written to which buffer from the fragment shader
    glBindFragDataLocation(shader_program, 0, "outColor")
    glLinkProgram(shader_program)  # re-link here for the bind * location calls
    glUseProgram(shader_program)

    # link between vertex data and attributes
    location_pos_attrib = 3
    # location_pos_attrib = glGetAttribLocation(shader_program, "offset")
    components = 3
    attrib_type = GL_FLOAT
    normalized = GL_FALSE
    data_size = 3 * glpy.SIZE_OF_GL_FLOAT  #4 * sizeof(GLfloat)
    pointer = 0  # no other components
    divisor = 1  # instanced, 0 off

    vbo_pos = glGenBuffers(1)
    glBindBuffer(GL_ARRAY_BUFFER, vbo_pos)
    glBufferData(GL_ARRAY_BUFFER, glpy.get_float_ptr(positions), GL_STATIC_DRAW)

    glEnableVertexAttribArray(location_pos_attrib)
    glVertexAttribPointer(location_pos_attrib, components, attrib_type, normalized, data_size, c_void_p(pointer))
    glVertexAttribDivisor(location_pos_attrib, divisor)

    vbo = glGenBuffers(1)
    glBindBuffer(GL_ARRAY_BUFFER, vbo)
    glBufferData(GL_ARRAY_BUFFER, glpy.get_float_ptr(vertices), GL_STATIC_DRAW)

    pos_attrib = 2
    glEnableVertexAttribArray(pos_attrib)
    glVertexAttribPointer(pos_attrib, 3, GL_FLOAT, GL_FALSE, 8 * SIZE_OF_FLOAT, c_void_p(0))

    color_attrib = 0  #glGetAttribLocation(shader_program, "color")  # 0
    glEnableVertexAttribArray(color_attrib)
    glVertexAttribPointer(color_attrib, 3, GL_FLOAT, GL_FALSE, 8 * SIZE_OF_FLOAT, c_void_p(3 * SIZE_OF_FLOAT))

    tex_attrib = 1  #glGetAttribLocation(shader_program, "texcoord")  # 1
    glEnableVertexAttribArray(tex_attrib)
    glVertexAttribPointer(tex_attrib, 2, GL_FLOAT, GL_FALSE, 8 * SIZE_OF_FLOAT, c_void_p(6 * SIZE_OF_FLOAT))

    # texture loading (texture uniform)
    textures = glGenTextures(2)

    img = pygame.image.load("sample.png")
    img = pygame.transform.flip(img, False, True)  # invert
    texture_data = pygame.image.tostring(img, "RGB", 1)  # get image data as array
    width, height = img.get_size()

    glActiveTexture(GL_TEXTURE0)
    glBindTexture(GL_TEXTURE_2D, textures[0])
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, texture_data)
    glUniform1i(glGetUniformLocation(shader_program, "texKitten"), 0)

    # defines how the texture is sampled
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)

    img = pygame.image.load("sample2.png")
    img = pygame.transform.flip(img, False, True)  # invert
    texture_data = pygame.image.tostring(img, "RGB", 1)  # get image data as array
    width, height = img.get_size()

    glActiveTexture(GL_TEXTURE1)
    glBindTexture(GL_TEXTURE_2D, textures[1])
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, texture_data)
    glUniform1i(glGetUniformLocation(shader_program, "texPuppy"), 1)

    # defines how the texture is sampled
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)

    a = radians(180.0)
    mat = glmath.m4x4()
    mat = glmath.rotate4(mat, a, [0.0, 0.0, 1.0])
    glmath.print_m4(mat, "model")
    uni_model = glGetUniformLocation(shader_program, "model")
    glUniformMatrix4fv(uni_model, 1, GL_FALSE, glpy.get_float_ptr(mat))

    look_at = [0.0, 0.0, 0.0]
    up = [0.0, 1.0, 0.0]
    cam_pos = [1.0, 1.0, 1.0]
    set_view_at(shader_program, cam_pos, look_at, up)

    proj = glmath.perspective(radians(45.0), float(screen_width) / screen_height, 0.5, 10000.0)
    glmath.print_m4(proj, "proj")
    uni_proj = glGetUniformLocation(shader_program, "proj")
    glUniformMatrix4fv(uni_proj, 1, GL_FALSE, glpy.get_float_ptr(proj))

    print('count: ', count ** 3)

    glEnable(GL_DEPTH_TEST)

    # main loop
    clock = pygame.time.Clock()

    running = True
    while running:
        clock.tick(60)

        glEnableVertexAttribArray(vao_box)

        # active shader program
        glUseProgram(shader_program)
        d = 1.0
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
                elif event.key == pygame.K_UP:
                    cam_pos[0] += d
                elif event.key == pygame.K_DOWN:
                    cam_pos[0] -= d
                elif event.key == pygame.K_LEFT:
                    cam_pos[1] += d
                elif event.key == pygame.K_RIGHT:
                    cam_pos[1] -= d
                elif event.key == pygame.K_RCTRL:
                    cam_pos[2] += d
                elif event.key == pygame.K_RALT:
                    cam_pos[2] -= d
                set_view_at(shader_program, cam_pos, look_at, up)
                print('cam_pos', cam_pos)

        duration = pygame.time.get_ticks() / 1000.0  # convert to seconds
        # duration = 0.0

        # Clear the screen to black
        glClearColor(0.0, 0.0, 0.0, 1.0)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        a = radians(180.0) * duration * 0.5

        mat = glmath.m4x4()
        mat = glmath.rotate4(mat, a, [0.0, 0.0, 1.0])
        glUniformMatrix4fv(uni_model, 1, GL_FALSE, glpy.get_float_ptr(mat))

        try:
            glBindVertexArray(vao_box)

            # draw triangles
            # glDrawArrays(GL_TRIANGLES, 0, 36)
            glDrawArraysInstanced(GL_TRIANGLES, 0, 36, count ** 3)
        finally:
            glBindVertexArray(0)
            glUseProgram(0)

        pygame.display.flip()

    glDeleteProgram(shader_program)

    glDeleteTextures(textures)

    glDeleteBuffers(1, [vbo])
    # glDeleteBuffers(1, [ebo])

    glDeleteVertexArrays(1, [vao_box])


def set_view_at(shader_program, position, look_at, up):
    # view = glm.look_at([1.5, 1.5, 1.5], [0.0, 0.0, 0.0], [0.0, 0.0, 1.0])
    view = glmath.look_at(position, look_at, up)
    glmath.print_m4(view, "view1")
    uni_view = glGetUniformLocation(shader_program, "view")
    glUniformMatrix4fv(uni_view, 1, GL_FALSE, glpy.get_float_ptr(view))


if __name__ == '__main__':
    main()
