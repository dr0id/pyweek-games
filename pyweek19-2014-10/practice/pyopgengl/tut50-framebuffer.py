# -*- coding: utf-8 -*-
"""
A 'modern' OpenGL example using pygame and pyopengl

Based on

https://open.gl/framebuffers
"""
from __future__ import print_function
import glpy

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 

import os

from math import radians

from ctypes import c_float, c_void_p, sizeof

SIZE_OF_FLOAT = sizeof(c_float)

import pygame

from OpenGL.GL import glGenBuffers, glBindBuffer, GL_ARRAY_BUFFER, glBufferData, GL_STATIC_DRAW, glCreateShader, \
    GL_VERTEX_SHADER, glShaderSource, glCompileShader, glGetShaderiv, GL_COMPILE_STATUS, GL_TRUE, glGetShaderInfoLog, \
    GL_FRAGMENT_SHADER, glCreateProgram, glAttachShader, glBindFragDataLocation, glLinkProgram, \
    glUseProgram, GL_FLOAT, glGetAttribLocation, glVertexAttribPointer, glEnableVertexAttribArray, \
    glGenVertexArrays, glBindVertexArray, glClearColor, \
    glClear, glDeleteProgram, glDeleteShader, glDeleteBuffers, \
    glDeleteVertexArrays, GL_FALSE, GL_TRIANGLES, GL_COLOR_BUFFER_BIT, GL_DEPTH_BUFFER_BIT, \
    GL_ELEMENT_ARRAY_BUFFER, GL_UNSIGNED_INT, glDrawElements, glTexImage2D, GL_TEXTURE_2D, GL_RGB, \
    GL_UNSIGNED_BYTE, glDeleteTextures, glGenTextures, glTexParameteri, GL_TEXTURE_WRAP_S, \
    GL_CLAMP_TO_EDGE, GL_TEXTURE_WRAP_T, GL_TEXTURE_MIN_FILTER, GL_LINEAR, GL_TEXTURE_MAG_FILTER, \
    glActiveTexture, GL_TEXTURE0, GL_TEXTURE1, glBindTexture, glUniform1i, glGetUniformLocation, \
    glUniformMatrix4fv, glDrawArrays, glEnable, GL_DEPTH_TEST, glDepthMask, GL_STENCIL_TEST, glDisable, \
    glStencilFunc, GL_ALWAYS, glStencilOp, GL_KEEP, GL_KEEP, GL_REPLACE, glStencilMask, GL_STENCIL_BUFFER_BIT, \
    GL_EQUAL, glUniform3f, glGenFramebuffers, glBindFramebuffer, GL_FRAMEBUFFER, \
    glFramebufferTexture2D, GL_COLOR_ATTACHMENT0, glCheckFramebufferStatus, GL_FRAMEBUFFER_COMPLETE,\
    glGenRenderbuffers, glBindRenderbuffer, GL_RENDERBUFFER, glRenderbufferStorage, GL_RENDERBUFFER, \
    GL_DEPTH24_STENCIL8, glFramebufferRenderbuffer, GL_DEPTH_STENCIL_ATTACHMENT, \
    glDeleteRenderbuffers, glDeleteFramebuffers, GL_LINK_STATUS, glGetProgramiv, glGetProgramInfoLog, \
    glReadPixels, GLuint, glDrawBuffers


# enable error when array data is copied
from OpenGL.arrays import numpymodule

numpymodule.NumpyHandler.ERROR_ON_COPY = True

import glmath


def main():
    """
    Main method. In this simple program it contains all the code.

    :raise RuntimeError:
    """
    screen_width = 800
    screen_height = 600

    pygame.init()
    os.environ['SDL_VIDEO_CENTERED'] = '1'

    pygame.display.gl_set_attribute(pygame.GL_STENCIL_SIZE, 8)
    pygame.display.gl_set_attribute(pygame.GL_DEPTH_SIZE, 24)
    # pygame.display.gl_set_attribute(pygame.GL_MULTISAMPLEBUFFERS, 1)
    # pygame.display.gl_set_attribute(pygame.GL_MULTISAMPLESAMPLES, 2)
    # pygame.display.gl_set_attribute(pygame.GL_SWAP_CONTROL, 0)

    pygame.display.set_mode((screen_width, screen_height), pygame.OPENGL | pygame.DOUBLEBUF)
    # pygame.display.set_mode((screen_width, screen_height), pygame.OPENGL)

    # these would be nice to configure in the openGL context, but I don't know how (if its
    # even possible using pygame)
    #
    # SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    # SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    # SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);

    # crating Vertex Array Object so save shader attributes
    vao_cube = glGenVertexArrays(1)
    glBindVertexArray(vao_cube)

    elements = [
        0, 1, 2,
        2, 3, 0
    ]

    # vertices
    # position   color         tex coord
    #    x     y    z    r     g    b   u    v
    vertices = [
        -0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 0.0,
        0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
        0.5, 0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
        0.5, 0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
        -0.5, 0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
        -0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 0.0,

        -0.5, -0.5, 0.5, 1.0, 1.0, 1.0, 0.0, 0.0,
        0.5, -0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
        0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
        0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
        -0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
        -0.5, -0.5, 0.5, 1.0, 1.0, 1.0, 0.0, 0.0,

        -0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
        -0.5, 0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
        -0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
        -0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
        -0.5, -0.5, 0.5, 1.0, 1.0, 1.0, 0.0, 0.0,
        -0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 0.0,

        0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
        0.5, 0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
        0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
        0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
        0.5, -0.5, 0.5, 1.0, 1.0, 1.0, 0.0, 0.0,
        0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 0.0,

        -0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
        0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
        0.5, -0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
        0.5, -0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
        -0.5, -0.5, 0.5, 1.0, 1.0, 1.0, 0.0, 0.0,
        -0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,

        -0.5, 0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
        0.5, 0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
        0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
        0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
        -0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 0.0, 0.0,
        -0.5, 0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,

        # plane
    -1.0, -1.0, -0.5, 0.0, 0.0, 0.0, 0.0, 0.0,
     1.0, -1.0, -0.5, 0.0, 0.0, 0.0, 1.0, 0.0,
     1.0,  1.0, -0.5, 0.0, 0.0, 0.0, 1.0, 1.0,
     1.0,  1.0, -0.5, 0.0, 0.0, 0.0, 1.0, 1.0,
    -1.0,  1.0, -0.5, 0.0, 0.0, 0.0, 0.0, 1.0,
    -1.0, -1.0, -0.5, 0.0, 0.0, 0.0, 0.0, 0.0,
    ]

    vbo = glGenBuffers(1)
    glBindBuffer(GL_ARRAY_BUFFER, vbo)
    glBufferData(GL_ARRAY_BUFFER, glpy.get_float_ptr(vertices), GL_STATIC_DRAW)

    # ebo = glGenBuffers(1)
    # glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo)
    # glBufferData(GL_ELEMENT_ARRAY_BUFFER, glm.int_ptr(elements), GL_STATIC_DRAW)

    # shader sources

    vertex_source = """
#version 150 core

in vec3 position;
in vec3 color;
in vec2 texcoord;

out vec3 Color;
out vec2 Texcoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;
uniform vec3 overrideColor;

void main()
{
    Color = color * overrideColor;
    Texcoord = texcoord;
    gl_Position = proj * view * model * vec4(position, 1.0);
}
"""

    fragment_source = """
#version 150 core

in vec3 Color;
in vec2 Texcoord;

out vec4 outColor;

uniform sampler2D texKitten;
uniform sampler2D texPuppy;

void main()
{
    vec4 colKitten = texture(texKitten, Texcoord);
    vec4 colPuppy =texture(texPuppy, Texcoord);
    outColor = vec4(Color, 1.0) * mix(colKitten, colPuppy, 0.5);
}
"""
    vertex_shader = glCreateShader(GL_VERTEX_SHADER)
    glShaderSource(vertex_shader, vertex_source)
    glCompileShader(vertex_shader)
    if glGetShaderiv(vertex_shader, GL_COMPILE_STATUS) != GL_TRUE:
        raise RuntimeError(glGetShaderInfoLog(vertex_shader))

    fragment_shader = glCreateShader(GL_FRAGMENT_SHADER)
    glShaderSource(fragment_shader, fragment_source)
    glCompileShader(fragment_shader)
    if glGetShaderiv(fragment_shader, GL_COMPILE_STATUS) != GL_TRUE:
        raise RuntimeError(glGetShaderInfoLog(fragment_shader))

    # setting up shader program
    shader_program = glCreateProgram()
    glAttachShader(shader_program, vertex_shader)
    glAttachShader(shader_program, fragment_shader)

    # define which output is written to which buffer from the fragment shader
    glBindFragDataLocation(shader_program, 0, "outColor")

    glLinkProgram(shader_program)
    if glGetProgramiv(shader_program, GL_LINK_STATUS) != GL_TRUE:
        raise RuntimeError(glGetProgramInfoLog(shader_program))

    glUseProgram(shader_program)

    # link between vertex data and attributes
    pos_attrib = glGetAttribLocation(shader_program, "position")
    glEnableVertexAttribArray(pos_attrib)
    glVertexAttribPointer(pos_attrib, 3, GL_FLOAT, GL_FALSE, 8 * SIZE_OF_FLOAT, c_void_p(0))

    color_attrib = glGetAttribLocation(shader_program, "color")
    glEnableVertexAttribArray(color_attrib)
    glVertexAttribPointer(color_attrib, 3, GL_FLOAT, GL_FALSE, 8 * SIZE_OF_FLOAT, c_void_p(3 * SIZE_OF_FLOAT))

    tex_attrib = glGetAttribLocation(shader_program, "texcoord")
    glEnableVertexAttribArray(tex_attrib)
    glVertexAttribPointer(tex_attrib, 2, GL_FLOAT, GL_FALSE, 8 * SIZE_OF_FLOAT, c_void_p(6 * SIZE_OF_FLOAT))

    # texture loading (texture uniform)
    textures = glGenTextures(2)

    img = pygame.image.load("sample.png")
    img = pygame.transform.flip(img, False, True)  # invert
    texture_data = pygame.image.tostring(img, "RGB", 1)  # get image data as array
    width, height = img.get_size()

    glActiveTexture(GL_TEXTURE0)
    glBindTexture(GL_TEXTURE_2D, textures[0])
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, texture_data)
    glUniform1i(glGetUniformLocation(shader_program, "texKitten"), 0)

    # defines how the texture is sampled
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)

    glBindTexture(GL_TEXTURE_2D, 0)

    img = pygame.image.load("sample2.png")
    img = pygame.transform.flip(img, False, True)  # invert
    texture_data = pygame.image.tostring(img, "RGB", 1)  # get image data as array
    width, height = img.get_size()

    glActiveTexture(GL_TEXTURE1)
    glBindTexture(GL_TEXTURE_2D, textures[1])
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, texture_data)
    glUniform1i(glGetUniformLocation(shader_program, "texPuppy"), 1)

    # defines how the texture is sampled
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)

    glBindTexture(GL_TEXTURE_2D, 0)

    # model = glm.m4x4()
    # model = glm.rotation_z4(a)
    model = glmath.rotation4(radians(180.0), [0.0, 0.0, 1.0])
    glmath.print_m4(model, "model")
    uni_model = glGetUniformLocation(shader_program, "model")
    glUniformMatrix4fv(uni_model, 1, GL_FALSE, glpy.get_float_ptr(model))

    view = glmath.look_at([0.0, 0.0, 2.5], [0.0, 0.0, 0.0], [0.0, 1.0, 0.0])
    view = glmath.look_at([2.5, 2.5, 2.5], [0.0, 0.0, 0.0], [0.0, 0.0, 1.0])
    glmath.print_m4(view, "view")
    uni_view = glGetUniformLocation(shader_program, "view")
    glUniformMatrix4fv(uni_view, 1, GL_FALSE, glpy.get_float_ptr(view))

    proj = glmath.perspective(radians(45.0), float(screen_width) / screen_height, 1.0, 10.0)
    glmath.print_m4(proj, "proj")
    uni_proj = glGetUniformLocation(shader_program, "proj")
    glUniformMatrix4fv(uni_proj, 1, GL_FALSE, glpy.get_float_ptr(proj))

    uni_override_color = glGetUniformLocation(shader_program, "overrideColor")
    glUniform3f(uni_override_color, 1.0, 1.0, 1.0)

    glBindVertexArray(0)

    # 2D quad

    quad_vertices = [
        -1.0, 1.0, 0.0, 1.0,
        1.0, 1.0, 1.0, 1.0,
        1.0, -1.0, 1.0, 0.0,

        1.0, -1.0, 1.0, 0.0,
        -1.0, -1.0, 0.0, 0.0,
        -1.0, 1.0, 0.0, 1.0
    ]

    quad_vertices = map(lambda x: 0.5 * x, quad_vertices)

    vao_quad = glGenVertexArrays(1)
    glBindVertexArray(vao_quad)

    quad_vbo = glGenBuffers(1)
    glBindBuffer(GL_ARRAY_BUFFER, quad_vbo)
    glBufferData(GL_ARRAY_BUFFER, glpy.get_float_ptr(quad_vertices), GL_STATIC_DRAW)

    quad_vertex_source = """
# version 150 core

in vec2 position;
in vec2 texcoord;

out vec2 Texcoord;

void main()
{
    Texcoord = texcoord;
    gl_Position = vec4(position, 0.0, 1.0);
}
"""

    quad_frag_source="""
# version 150 core
in vec2 Texcoord;
out vec4 outColor;
uniform sampler2D texFrameBuffer;

void main(){
    outColor = texture(texFrameBuffer, Texcoord);
};
"""

    quad_vertex_shader = glCreateShader(GL_VERTEX_SHADER)
    glShaderSource(quad_vertex_shader, quad_vertex_source)
    glCompileShader(quad_vertex_shader)
    if glGetShaderiv(quad_vertex_shader, GL_COMPILE_STATUS) != GL_TRUE:
        raise RuntimeError(glGetShaderInfoLog(quad_vertex_shader))

    quad_frag_shader = glCreateShader(GL_FRAGMENT_SHADER)
    glShaderSource(quad_frag_shader, quad_frag_source)
    glCompileShader(quad_frag_shader)
    if glGetShaderiv(quad_frag_shader, GL_COMPILE_STATUS) != GL_TRUE:
        raise RuntimeError(glGetShaderInfoLog(quad_frag_shader))

    quad_shader_program = glCreateProgram()
    glAttachShader(quad_shader_program, quad_vertex_shader)
    glAttachShader(quad_shader_program, quad_frag_shader)
    glBindFragDataLocation(quad_shader_program, 0, "outColor")

    glLinkProgram(quad_shader_program)
    if glGetProgramiv(quad_shader_program, GL_LINK_STATUS) != GL_TRUE:
        raise RuntimeError(glGetProgramInfoLog(quad_shader_program))

    glUseProgram(quad_shader_program)

    # map vertices to shader input
    q_pos_attrib = glGetAttribLocation(quad_shader_program, "position")
    glEnableVertexAttribArray(q_pos_attrib)
    glVertexAttribPointer(q_pos_attrib, 2, GL_FLOAT, GL_FALSE, 4 * SIZE_OF_FLOAT, c_void_p(0))

    q_tex_attrib = glGetAttribLocation(quad_shader_program, "texcoord")
    glEnableVertexAttribArray(q_tex_attrib)
    glVertexAttribPointer(q_tex_attrib, 2, GL_FLOAT, GL_FALSE, 4 * SIZE_OF_FLOAT, c_void_p(2))

    q_tex_frame_buffer_attrib = glGetUniformLocation(quad_shader_program, "texFrameBuffer")
    glUniform1i(q_tex_frame_buffer_attrib, 0)

    # framebuffer
    frame_buffer = glGenFramebuffers(1)
    glBindFramebuffer(GL_FRAMEBUFFER, frame_buffer)

    # render buffer
    rbo_depth_stencil = glGenRenderbuffers(1)
    glBindRenderbuffer(GL_RENDERBUFFER, rbo_depth_stencil)
    # width and height must be smaller than GL_MAX_RENDERBUFFER_SIZE
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, screen_width, screen_height)
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo_depth_stencil)

    frame_buffer_status = glCheckFramebufferStatus(GL_FRAMEBUFFER)
    if(GL_FRAMEBUFFER_COMPLETE != frame_buffer_status):
        raise RuntimeError('framebuffer2 not complete!')



    tex_color_buffer = glGenTextures(1)
    # glActiveTexture(GL_TEXTURE0)
    glBindTexture(GL_TEXTURE_2D, tex_color_buffer)

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, screen_width, screen_height, 0, GL_RGB, GL_UNSIGNED_BYTE, None)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex_color_buffer, 0)

    frame_buffer_status = glCheckFramebufferStatus(GL_FRAMEBUFFER)
    if(GL_FRAMEBUFFER_COMPLETE != frame_buffer_status):
        raise RuntimeError('framebuffer not complete!')

    # glBindFramebuffer(GL_FRAMEBUFFER, 0)



    # main loop
    clock = pygame.time.Clock()

    running = True
    while running:
        clock.tick(60)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False

        duration = pygame.time.get_ticks() / 1000.0  # convert to seconds

        model = glmath.rotation4(radians(180.0) * duration * 0.1, [0.0, 0.0, 1.0])

        try:

            glBindFramebuffer(GL_FRAMEBUFFER, frame_buffer)
            glBindVertexArray(vao_cube)
            glEnable(GL_DEPTH_TEST)
            glUseProgram(shader_program)
            glActiveTexture(GL_TEXTURE0)
            glBindTexture(GL_TEXTURE_2D, textures[0])  # kitty
            glActiveTexture(GL_TEXTURE1)
            glBindTexture(GL_TEXTURE_2D, textures[1])  # puppy
            # glBindRenderbuffer(GL_RENDERBUFFER, rbo_depth_stencil)
            # glBindFramebuffer(GL_FRAMEBUFFER, frame_buffer)
            # Clear the screen to white
            glClearColor(1.0, 1.0, 1.0, 1.0)
            # glClearColor(0.0, 0.0, 0.0, 0.0)
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)


            # glBindFramebuffer(GL_FRAMEBUFFER, 0)



            glUniformMatrix4fv(uni_model, 1, GL_FALSE, glpy.get_float_ptr(model))
            glUniformMatrix4fv(uni_view, 1, GL_FALSE, glpy.get_float_ptr(view))
            glUniformMatrix4fv(uni_proj, 1, GL_FALSE, glpy.get_float_ptr(proj))

            # draw cube
            glDrawArrays(GL_TRIANGLES, 0, 36)

            glEnable(GL_STENCIL_TEST)

            # draw floor
            glStencilFunc(GL_ALWAYS, 1, 0xFF)  # set any stencil to 1
            glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE)
            glStencilMask(0xFF)  # write to stencil buffer
            glDepthMask(GL_FALSE)  # don't write to depth buffer
            glClear(GL_STENCIL_BUFFER_BIT)  # clear stencil buffer (0 by default)

            glDrawArrays(GL_TRIANGLES, 36, 6)

            # draw inverted cube (reflection)
            glStencilFunc(GL_EQUAL, 1, 0xFF)  # pass test if stencil value is 1
            glStencilMask(0x00)  # don't write anything to stencil buffer
            glDepthMask(GL_TRUE)  # write to depth buffer

            model = glmath.scale4(glmath.translate4(model, 0.0, 0.0, -1.0), 1.0, 1.0, -1.0)
            glUniformMatrix4fv(uni_model, 1, GL_FALSE, glpy.get_float_ptr(model))

            glUniform3f(uni_override_color, 0.3, 0.3, 0.3)
            glDrawArrays(GL_TRIANGLES, 0, 36)
            glUniform3f(uni_override_color, 1.0, 1.0, 1.0)

            glDisable(GL_STENCIL_TEST)

            # glDrawBuffers([GL_COLOR_ATTACHMENT0, GL_DEPTH_STENCIL_ATTACHMENT])

            # bind default framebuffer and draw contents of our framebuffer
            # glBindRenderbuffer(GL_RENDERBUFFER, 0)
            glBindFramebuffer(GL_FRAMEBUFFER, 0)

            # a = (GLuint * screen_width * screen_height)(0)
            # w, h = screen_width, screen_height
            # aa = [0] * screen_height * screen_width
            # data = glReadPixels(0, 0, screen_width, screen_height, GL_RGB, GL_UNSIGNED_BYTE)
            # import pygame
            # pygame.image.from_string()
            # print('data', data, aa)

            glBindVertexArray(vao_quad)
            glDisable(GL_DEPTH_TEST)
            glUseProgram(quad_shader_program)

            glActiveTexture(GL_TEXTURE0)
            glBindTexture(GL_TEXTURE_2D, tex_color_buffer)

            glDrawArrays(GL_TRIANGLES, 0, 6)
            pygame.display.flip()

        except Exception as ex:
            print("FAILED", ex)

        finally:
            pass
            # glBindVertexArray(0)
            # glUseProgram(0)
            # glBindFramebuffer(GL_FRAMEBUFFER, 0)


    glDeleteProgram(shader_program)
    glDeleteShader(fragment_shader)
    glDeleteShader(vertex_shader)

    glDeleteTextures(textures)

    glDeleteBuffers(1, [vbo])

    glDeleteFramebuffers(1, [frame_buffer])
    glDeleteRenderbuffers(1, [rbo_depth_stencil])

    glDeleteVertexArrays(1, [vao_cube, vao_quad])


if __name__ == '__main__':
    main()
