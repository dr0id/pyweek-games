# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'tut91-acceleratedsprites.py' is part of HG_pyweek_games
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]                    # For instance:
                   |                                  * 1.2.0.1 instead of 1.2-a
                   +-|* 0 for alpha (status)          * 1.2.1.2 instead of 1.2-b2 (beta with some bug fixes)
                     |* 1 for beta (status)           * 1.2.2.3 instead of 1.2-rc (release candidate)
                     |* 2 for release candidate       * 1.2.3.0 instead of 1.2-r (commercial distribution)
                     |* 3 for (public) release        * 1.2.3.5 instead of 1.2-r5 (commercial dist with many bug fixes)


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division

try:
    import logging
    logging.basicConfig()

    import sys
    import traceback
    import os
    import random
    import time

    # # enable following lines for extensive logging
    # import OpenGL
    # OpenGL.FULL_LOGGING = True  # very slow

    import pygame

    from OpenGL.GL import GLfloat, sizeof, glBufferSubData, GL_ARRAY_BUFFER, glEnable, GL_BLEND, glBlendFunc, \
        GL_SRC_ALPHA, glDepthMask, glDisable, GL_RASTERIZER_DISCARD, GL_ONE, glUseProgram, glGetUniformLocation, \
        glUniformMatrix4fv, GL_FALSE, glUniform3fv, glUniform1i, glBindVertexArray, glDisableVertexAttribArray, \
        glDrawArrays, GL_POINTS, glBindBuffer, glGenVertexArrays, glGenBuffers, glBufferData, GL_DYNAMIC_DRAW, \
        glVertexAttribPointer, GL_FLOAT, glGenTextures, glActiveTexture, GL_TEXTURE0, GL_TEXTURE_2D, glBindTexture, \
        glTexImage2D, GL_RGBA, GL_UNSIGNED_BYTE, glTexParameteri, GL_TEXTURE_WRAP_S, GL_TEXTURE_WRAP_T, \
        GL_TEXTURE_MIN_FILTER, GL_TEXTURE_MAG_FILTER, GL_CLAMP_TO_EDGE, GL_LINEAR, shaders, GL_VERTEX_SHADER, \
        GL_FRAGMENT_SHADER, GL_GEOMETRY_SHADER, GL_RGB, glFlush, glFinish, glEnableVertexAttribArray, GL_CULL_FACE, \
        glClearColor, GL_DEPTH_TEST, glUniform2fv, glBlendEquationSeparate, GL_FUNC_ADD, glBlendFuncSeparate, \
        GL_ONE_MINUS_SRC_ALPHA, GL_ZERO, glEnablei, glViewport, glClear, GL_COLOR_BUFFER_BIT, GL_DEPTH_BUFFER_BIT

    import glpy
    import glmath

    logger = logging.getLogger(__name__)

    __version__ = '1.0.0.0'

    # for easy comparison as in sys.version_info but digits only
    __version_info__ = tuple([int(_d) for _d in __version__.split('.')])

    __author__ = "DR0ID"
    __email__ = "dr0iddr0id {at} gmail [dot] com"
    __copyright__ = "DR0ID @ 2018"
    __credits__ = ["DR0ID"]  # list of contributors
    __maintainer__ = "DR0ID"
    __license__ = "New BSD license"

    __all__ = []  # list of public visible parts of this module 

    SCREENSIZE = 800, 600


    #
    # class _SpriteFacade(object):
    #
    #     _idx00_v1 = 0
    #     _idx01_v2 = _idx00_v1 + 3
    #     _idx02_v3 = _idx01_v2 + 3
    #     _idx03_v4 = _idx02_v3 + 3
    #     _idx04_scale = _idx03_v4 + 3
    #     _idx05_offset = _idx04_scale + 3
    #     _idx06_rotation = _idx05_offset + 3
    #     _idx07_position = _idx06_rotation + 3
    #     _idx08_alpha = _idx07_position + 3
    #     _idx09_tex_coord = _idx08_alpha + 1
    #     _idx10_tex_size = _idx09_tex_coord + 2
    #     _idx11_frame_count = _idx10_tex_size + 2
    #     _idx12_curr_frame = _idx11_frame_count + 1
    #     _idx13_tpf = _idx12_curr_frame + 1
    #     _idx14_time = _idx13_tpf + 1
    #     _idx15_direction = _idx14_time + 1
    #     _idx16_velocity = _idx15_direction + 3
    #     _idx17_acceleration = _idx16_velocity + 3
    #     _idx18_rot_velocity = _idx17_acceleration + 3
    #     _idx19_rot_acceleration = _idx18_rot_velocity + 3
    #
    #     data_size = sizeof((GLfloat * (_idx19_rot_acceleration + 3))())  # last entry + size of last entry
    #
    #     def __init__(self, idx, dirty_sprites):
    #
    #         # self._data = [
    #         #     1.0, 1.0, 0.0,  # vertex1
    #         self._set_data((1.0, 1.0, 0.0), self._idx00_v1, self._idx01_v2)
    #         #     1.0, -1.0, 0.0,  # vertex2
    #         self._set_data((1.0, -1.0, 0.0), self._idx01_v2, self._idx02_v3)
    #         #     -1.0, -1.0, 0.0,  # vertex3
    #         self._set_data((-1.0, -1.0, 0.0), self._idx00_v3, self._idx03_v4)
    #         #     -1.0, 1.0, 0.0,  # vertex4
    #         self._set_data((-1.0, 1.0, 0.0), self._idx00_v4, self._idx05_offset)
    #         #
    #         #     # transformation
    #         #     1.0, 1.0, 1.0,  # scale
    #         #     0.0, 0.0, 0.0,  # offset
    #         #     0.0, 0.0, 0.0,  # rotation
    #         #     0.0, 0.0, 0.0,  # position
    #         #
    #         #     # image
    #         #     1.0,  # alpha
    #         #     0.0, 0.0,  # texture coord
    #         #     1.0, 1.0,  # texture size w, h
    #         #
    #         #     # animation
    #         #     1.0,   # frame count for animation, int?
    #         #     0.0,  # current frame of the animation, int?
    #         #     1.0 / 10.0,  # time per frame
    #         #
    #         #     0.0,  # individual time
    #         #
    #         #     # physics for boids
    #         #     1.0, 0.0, 0.0,  # direction
    #         #     0.0, 0.0, 0.0,  # velocity
    #         #     0.0, 0.0, 0.0,  # acceleration
    #         #     0.0, 0.0, 0.0,  # omega, direction is axis of rotation, length is rotational speed
    #         #     0.0, 0.0, 0.0,  # direction is axis of rotation, length is rotational acceleration
    #         #
    #         # ]
    #
    #         self.idx = idx
    #         self.dirty_sprites = dirty_sprites
    #         self._dirty_bytes = set()
    #
    #     def set_scale(self, sx, sy, sz):
    #         self._set_data((sx, sy, sz), self._idx04_scale, self._idx05_offset)
    #
    #     def _set_data(self, data, start_idx, end_idx):
    #         # self._data[start_idx: end_idx] = data
    #         self._dirty_bytes.add((start_idx, data))
    #         _SpriteFacade.dirty_sprites.add(self)
    #
    # class SpriteSystem(object):
    #
    #     def __init__(self):
    #         self._dirty_sprites = set()
    #         self._unused_slots = []  # add unused slots
    #         self._next_idx = 0
    #
    #     def create_sprite(self):
    #         idx = self._next_idx
    #         if self._unused_slots:
    #             self._unused_slots.sort()
    #             idx = self._unused_slots.pop()
    #         spr = _SpriteFacade(idx, self._dirty_sprites)
    #         self._dirty_sprites.add(spr)
    #         return spr
    #
    #     def remove_sprite(self, spr):
    #         self._unused_slots.append(spr.idx)
    #
    #
    #     def initialize(self):
    #         pass
    #
    #     def draw(self):
    #         size_of_particle = _SpriteFacade.data_size
    #         for spr in _SpriteFacade.dirty_sprites:
    #             idx = spr.idx
    #             for start_idx, particle in spr._dirty_bytes:
    #                 data_ptr = get_ptr(particle)
    #                 glBufferSubData(GL_ARRAY_BUFFER, idx * size_of_particle + start_idx, data_ptr)
    #
    #         pass

    class Atlas(object):

        def __init__(self, size, sampler_id):
            self.size = size
            self.sampler_id = sampler_id

            if not (self.is_power_of_two(size[0]) and self.is_power_of_two(size[1])):
                raise Exception("Texture size should be power of two, provided: " + str(size))
            self._tex_surf = pygame.Surface(size, pygame.SRCALPHA, 32)
            self._tex_surf.fill((0, 0, 0, 0), None, pygame.BLEND_RGBA_MULT)

            self._last_rect = pygame.Rect(1, 1, 0, 0)  # leaving a 1 x 1 border
            self._max_of_line = 0

            self._cache = {}  # {id: (rect, img)}
            self.is_dirty = True
            self._tex_id = -1

        @staticmethod
        def is_power_of_two(n):
            return n and not (n & (n - 1))

        def add(self, image):
            if image is None:
                raise Exception("image should not be None!")

            _img_id = id(image)
            if _img_id in self._cache:
                return self._cache[_img_id]

            self.is_dirty = True
            img_rect = image.get_rect(topleft=self._last_rect.topright)
            img_rect.move_ip(1, 0)

            _tex_w, _tex_h = self._tex_surf.get_size()
            if img_rect.right + 1 > _tex_w:
                img_rect.topleft = (1, self._max_of_line + 1)
                if img_rect.bottom + 1 > _tex_h:
                    raise Exception("image does not fit: tex size {0} img_rect {1}".format((_tex_w, _tex_h), img_rect))

            if img_rect.bottom > self._max_of_line:
                self._max_of_line = img_rect.bottom

            self._last_rect = img_rect
            self._tex_surf.blit(image, img_rect, None, pygame.BLEND_RGBA_ADD)

            result = img_rect.topleft, image.get_size()
            self._cache[_img_id] = result
            return result

        def update_texture(self):
            if self.is_dirty:
                if self._tex_id == -1:
                    self._tex_id = glGenTextures(1)

                t_w, t_h = self._tex_surf.get_size()
                t_data = pygame.image.tostring(self._tex_surf, "RGBA_PREMULT", 0)

                glActiveTexture(GL_TEXTURE0 + self.sampler_id)  # texture bind location for texture unit 0
                glBindTexture(GL_TEXTURE_2D, self._tex_id)
                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, t_w, t_h, 0, GL_RGBA, GL_UNSIGNED_BYTE, t_data)

                # defines how the texture is sampled
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)

                self.is_dirty = False

        def save(self, file_name):
            # TODO: count mechanism to prevent overrides?
            pygame.image.save(self._tex_surf, file_name)


    class GlSprite(object):

        def __init__(self, image):
            # sprite management, don't use them
            self.dirty_list = None
            self.draw_idx = -1
            self.data_offset = -1  # self.draw_index * size_of_data
            self.data = None

            # transformation
            self.scale = [1.0, 1.0, 1.0]  # sx, sy, sz
            self.offset = [0.0, 0.0, 0.0]  # ox, oy, oz
            self.rotation = [0.0, 0.0, 0.0]  # rx, ry, rz
            self.position = [0.0, 0.0, 0.0]  # px, py, pz

            self.vel = [0.0, 0.0, 0.0]

            # image
            self.alpha = 1.0
            self.image = image

            # texture atlas info
            self.tex_coord = [0.0, 0.0]
            self.tex_size = list(image.get_size())

            # # animation
            # self.frame_count = 1
            # self.current_frame = 0
            # self.tpf = 1.0 / 10.0  # time per frame, 1 / fps
            # self.time = 0.0

            # self._data = [
            #     1.0, 1.0, 0.0,  # vertex1
            #     1.0, -1.0, 0.0,  # vertex2
            #     -1.0, -1.0, 0.0,  # vertex3
            #     -1.0, 1.0, 0.0,  # vertex4
            #
            #     # transformation
            #     1.0, 1.0, 1.0,  # scale
            #     0.0, 0.0, 0.0,  # offset
            #     0.0, 0.0, 0.0,  # rotation
            #     0.0, 0.0, 0.0,  # position
            #
            #     # image
            #     1.0,  # alpha
            #     0.0, 0.0,  # texture coord
            #     1.0, 1.0,  # texture size w, h
            #
            #     # animation
            #     1.0,   # frame count for animation, int?
            #     0.0,  # current frame of the animation, int?
            #     1.0 / 10.0,  # time per frame
            #
            #     0.0,  # individual time
            #
            #     # physics for boids
            #     1.0, 0.0, 0.0,  # direction
            #     0.0, 0.0, 0.0,  # velocity
            #     0.0, 0.0, 0.0,  # acceleration
            #     0.0, 0.0, 0.0,  # omega, direction is axis of rotation, length is rotational speed
            #     0.0, 0.0, 0.0,  # direction is axis of rotation, length is rotational acceleration
            #
            # ]


    class SpriteSystem(object):

        _MAX_SPRITES = 10000
        _SPRITE_FLOAT_COUNT = 20

        vert_shader_source = """
#version 420 core

layout (location = 0) in vec3 vScale;
layout (location = 1) in vec3 vOffset;
layout (location = 2) in vec3 vRotation;
layout (location = 3) in vec3 vPosition;
layout (location = 4) in float fAlpha;
layout (location = 5) in vec2 vTexCoord;
layout (location = 6) in vec2 vTexSize;
        
out vec3 vScalePass;
out vec3 vOffsetPass;
out vec3 vRotationPass;
out float fAlphaPass;
out vec2 vTexCoordPass;
out vec2 vTexSizePass;

void main()
{
    gl_Position = vec4(vPosition, 1.0);

    vScalePass = vScale;
    vOffsetPass = vOffset;
    vRotationPass = vRotation;
    fAlphaPass = fAlpha;
    vTexCoordPass = vTexCoord;
    vTexSizePass = vTexSize;
}
        """

        geo_shader_source = """
#version 420 core

uniform struct Matrices
{
    mat4 mProj;
    mat4 mView;
} matrices;

uniform vec2 textureAtlasSize;

layout(points) in;
layout(triangle_strip) out;
layout(max_vertices = 4) out;

in vec3 vScalePass[];
in vec3 vOffsetPass[];
in vec3 vRotationPass[];
in float fAlphaPass[];
in vec2 vTexCoordPass[];
in vec2 vTexSizePass[];

smooth out vec2 vTexCoordinates;
flat out float fAlphaOut;

float PI = 3.14159265359;

mat3 rotate(vec3 rotation, float angle){
    float c = cos(angle);
    float s = sin(angle);
    
    vec3 axis = normalize(rotation);
    vec3 temp = (1 - c) * axis;

    mat3 Rotate = mat3(1.0);

    Rotate[0][0] = c + temp[0] * axis[0];
    Rotate[0][1] = temp[0] * axis[1] + s * axis[2];
    Rotate[0][2] = temp[0] * axis[2] - s * axis[1];
    
    Rotate[1][0] = temp[1] * axis[0] - s * axis[2];
    Rotate[1][1] = c + temp[1] * axis[1];
    Rotate[1][2] = temp[1] * axis[2] + s * axis[0];

    Rotate[2][0] = temp[2] * axis[0] + s * axis[1];
    Rotate[2][1] = temp[2] * axis[1] - s * axis[0];
    Rotate[2][2] = c + temp[2] * axis[2];
    
    return Rotate;
}

vec4 applyTransformations(vec3 position, vec3 vertex, vec3 scale, vec3 offset, mat3 m3Rotation, float angle){

    vertex = vec3(vertex.x * scale.x, vertex.y * scale.y, vertex.z * scale.z);
    vertex = vertex - offset;
    if(angle > 0.0){
        vertex = m3Rotation * vertex;
    }
    return vec4(position + vertex, 1.0);
}

void main()
{
    // texture
    float fAtlTexW = textureAtlasSize.x;
    float fAtlTexH = textureAtlasSize.y;
    vec2 vTexCoordOrigin = vec2(vTexCoordPass[0].x, vTexCoordPass[0].y);
    
    float fTexW = vTexSizePass[0].x;
    float fTexH = vTexSizePass[0].y;
    
    // alpha
    fAlphaOut = fAlphaPass[0];
    
    // position transformation
    vec3 vOldPos = gl_in[0].gl_Position.xyz;
    mat4 mVP = matrices.mProj * matrices.mView;
    vec3 vHalfW = vec3(fTexW / 2.0, 0.0, 0.0);
    vec3 vHalfH = vec3(0.0, fTexH / 2.0, 0.0);
    vec3 vScale = vScalePass[0];
    vec3 vOffset = vOffsetPass[0];
    vec3 vRotation = vRotationPass[0];
    
    // calculate those values only once for all vertices
    float degAngle = length(vRotation);
    mat3 mRot = mat3(1.0);
    if(degAngle > 0.0){
        float radAngle = degAngle * PI / 180.0;  // convert to radians
        mRot = rotate(vRotation, -radAngle);  // accommodate for positive values rotate ccw (like pygame)
    }
    
    vTexCoordinates = vec2(vTexCoordOrigin.x / fAtlTexW, vTexCoordOrigin.y / fAtlTexH);
    vec4 vTransformed = applyTransformations(vOldPos, -vHalfW - vHalfH, vScale, vOffset, mRot, degAngle);
    gl_Position = mVP * vTransformed;
    EmitVertex();

    vTexCoordinates = vec2(vTexCoordOrigin.x / fAtlTexW, (vTexCoordOrigin.y + fTexH) / fAtlTexH);
    vTransformed = applyTransformations(vOldPos, -vHalfW + vHalfH, vScale, vOffset, mRot, degAngle);
    gl_Position = mVP * vTransformed;
    EmitVertex();

    vTexCoordinates = vec2((vTexCoordOrigin.x + fTexW) / fAtlTexW, vTexCoordOrigin.y / fAtlTexH);
    vTransformed = applyTransformations(vOldPos, vHalfW - vHalfH, vScale, vOffset, mRot, degAngle);
    gl_Position = mVP * vTransformed;
    EmitVertex();

    vTexCoordinates = vec2((vTexCoordOrigin.x + fTexW) / fAtlTexW, (vTexCoordOrigin.y + fTexH) / fAtlTexH);
    vTransformed = applyTransformations(vOldPos, vHalfW + vHalfH, vScale, vOffset, mRot, degAngle);
    gl_Position = mVP * vTransformed;
    EmitVertex();

    EndPrimitive();
}
        """

        fag_shader_source = """
#version 420 core

uniform sampler2D gSampler;

smooth in vec2 vTexCoordinates;
flat in float fAlphaOut;

out vec4 FragColor;

void main()
{
    vec2 vTCoord = vTexCoordinates;
    vec4 vTexColor = texture2D(gSampler, vTCoord);
    FragColor = vTexColor * fAlphaOut;
}
        """

        def __init__(self, atlas):
            self._atlas = atlas

            self.buffer_id = None
            self.vao_id = None
            self.tex_id = None
            self.shader_prog = None

            self._initialized = False
            self._unused_slots_to_append = set()
            self._dirty_sprites = set()
            self._sprite_count = 0
            self._last_free_idx = 0
            self._unused_slots = []

            self._data = [0.0] * self._MAX_SPRITES * self._SPRITE_FLOAT_COUNT

        def create_sprite(self, img):
            spr = GlSprite(img)
            self._add_sprite(spr)
            return spr

        def _check_max_sprites(self):
            if self._sprite_count > self._MAX_SPRITES:
                raise Exception("Max number of sprites {0} reached: {1}".format(self._MAX_SPRITES, self._sprite_count))

        def add_sprite(self, spr):
            self._add_sprite(spr)

        def _add_sprite(self, spr):
            self._sprite_count += 1
            self._check_max_sprites()

            if spr.dirty_list is not None:
                raise Exception("sprite should not have dirty_list already set! Avoid adding same sprite twice!")

            spr.dirty_list = self._dirty_sprites
            spr.draw_idx = self._get_next_draw_index()

            self._dirty_sprites.add(spr)
            spr.tex_coord, spr.tex_size = self._atlas.add(spr.image)

        def _get_next_draw_index(self):
            draw_idx = self._last_free_idx
            if self._unused_slots:
                self._unused_slots.sort()
                draw_idx = self._unused_slots.pop()
            else:
                self._last_free_idx += 1
            return draw_idx

        def remove_sprite(self, spr):
            self._sprite_count -= 1
            self._unused_slots_to_append.add(spr.draw_idx)

            self._dirty_sprites.add(spr)
            spr.dirty_list = None  # set sprite dirty list to None to prevent further drawing of this sprite

        def initialize(self):
            # shader
            vertex_shader = shaders.compileShader(self.vert_shader_source, GL_VERTEX_SHADER)
            geo_shader = shaders.compileShader(self.geo_shader_source, GL_GEOMETRY_SHADER)
            frag_shader = shaders.compileShader(self.fag_shader_source, GL_FRAGMENT_SHADER)
            self.shader_prog = shaders.compileProgram(vertex_shader, geo_shader, frag_shader)

            self.shader_prog.check_validate()
            self.shader_prog.check_linked()

            # buffers
            self.vao_id = glGenVertexArrays(1)
            self.buffer_id = glGenBuffers(1)

            glBindVertexArray(self.vao_id)
            glBindBuffer(GL_ARRAY_BUFFER, self.buffer_id)
            data_ptr = (GLfloat * self._SPRITE_FLOAT_COUNT)()
            size_of_sprite = sizeof(data_ptr)
            print("glBufferData size:", size_of_sprite * self._MAX_SPRITES)
            glBufferData(GL_ARRAY_BUFFER, size_of_sprite * self._MAX_SPRITES, glpy.NULL, GL_DYNAMIC_DRAW)

            for j in range(8):
                glEnableVertexAttribArray(j)

            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, size_of_sprite, glpy.get_offset_from_index(0))  # scale
            glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, size_of_sprite, glpy.get_offset_from_index(3))  # offset
            glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, size_of_sprite, glpy.get_offset_from_index(6))  # rotation
            glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, size_of_sprite, glpy.get_offset_from_index(9))  # position
            glVertexAttribPointer(4, 1, GL_FLOAT, GL_FALSE, size_of_sprite, glpy.get_offset_from_index(12))  # alpha
            glVertexAttribPointer(5, 2, GL_FLOAT, GL_FALSE, size_of_sprite, glpy.get_offset_from_index(13))  # tex coord
            glVertexAttribPointer(6, 2, GL_FLOAT, GL_FALSE, size_of_sprite, glpy.get_offset_from_index(15))  # tex size
            glVertexAttribPointer(7, 3, GL_FLOAT, GL_FALSE, size_of_sprite, glpy.get_offset_from_index(17))  # vel

            self._initialized = True

        def draw(self, cam):
            if not self._initialized:
                return

            if self._atlas.is_dirty:
                self._atlas.update_texture()

            # blending
            glEnable(GL_BLEND)

            # # pre multiplied alpha
            glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_ADD)
            glBlendFuncSeparate(GL_ONE, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_ALPHA)

            # # traditional alpha blending
            # glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_ADD)
            # glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ZERO)

            # # source alpha blended with destination alpha
            # glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_ADD)
            # glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_ALPHA)

            # # using simple blend function
            # glEnable(GL_BLEND)
            # glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

            # enable things
            glEnable(GL_CULL_FACE)
            glEnable(GL_DEPTH_TEST)

            glDisable(GL_RASTERIZER_DISCARD)
            glUseProgram(self.shader_prog)

            # set uniforms (shader global vars)
            u_loc = glGetUniformLocation(self.shader_prog, "matrices.mProj")
            glUniformMatrix4fv(u_loc, 1, GL_FALSE, cam.projection)

            u_loc = glGetUniformLocation(self.shader_prog, "matrices.mView")
            glUniformMatrix4fv(u_loc, 1, GL_FALSE, cam.view)

            a_size = tuple([float(_v) for _v in self._atlas.size])
            u_loc = glGetUniformLocation(self.shader_prog, "textureAtlasSize")
            glUniform2fv(u_loc, 1, a_size)

            u_loc = glGetUniformLocation(self.shader_prog, "gSampler")
            glUniform1i(u_loc, self._atlas.sampler_id)

            # update data before draw
            self._unused_slots.extend(self._unused_slots_to_append)
            self._unused_slots_to_append.clear()

            glBindBuffer(GL_ARRAY_BUFFER, self.buffer_id)
            self._update_vbo_for_dirty_sprites()

            # draw
            glBindVertexArray(self.vao_id)
            glDrawArrays(GL_POINTS, 0, self._last_free_idx)

            glUseProgram(0)
            glBindVertexArray(0)  # prevent that vao is changed outside
            glBindBuffer(GL_ARRAY_BUFFER, 0)
            glDisable(GL_BLEND)

        def _update_vbo_for_dirty_sprites(self):
            # TODO: only update buffer if there is something to update??
            for spr in self._dirty_sprites:
                # data = [
                #     spr.scale[0], spr.scale[1], spr.scale[2],
                #     spr.offset[0], spr.offset[1], spr.offset[2],
                #     spr.rotation[0], spr.rotation[1], spr.rotation[2],
                #     spr.position[0], spr.position[1], spr.position[2],
                #     spr.alpha,
                #     spr.tex_coord[0], spr.tex_coord[1],
                #     spr.tex_size[0], spr.tex_size[1],
                #     spr.vel[0], spr.vel[1], spr.vel[2]
                # ]
                # if __debug__:
                #     if len(data) != self._SPRITE_FLOAT_COUNT:
                #         raise Exception("initialize and draw should use the same number of bytes!")
                #
                start = spr.draw_idx * self._SPRITE_FLOAT_COUNT
                self._data[start:start + 3] = spr.scale
                start += 3
                self._data[start:start + 3] = spr.offset
                start += 3
                self._data[start:start + 3] = spr.rotation
                start += 3
                self._data[start:start + 3] = spr.position
                start += 3
                self._data[start] = spr.alpha
                start += 1
                self._data[start:start + 2] = spr.tex_coord
                start += 2
                self._data[start:start + 2] = spr.tex_size
                start += 2
                self._data[start:start + 3] = spr.vel

            data_ptr = glpy.get_ptr(self._data)
            glBufferSubData(GL_ARRAY_BUFFER, 0, data_ptr)

            self._dirty_sprites.clear()

    def do_timing(func, *args):
        start = time.time()
        result = func(*args)
        end = time.time()
        duration = end - start
        print("duration {0}: {1}s".format(func.__name__, duration))
        return result


    def main():
        # init window and context
        screen_width = 800
        screen_height = 600

        pygame.init()
        os.environ['SDL_VIDEO_CENTERED'] = '1'

        pygame.display.set_caption("OpenGL - tut93 - spritesystem")

        pygame.display.set_mode((screen_width, screen_height), pygame.OPENGL | pygame.DOUBLEBUF)
        glViewport(0, 0, screen_width, screen_height)

        _atlas = Atlas((128, 128), 0)
        spritesystem = SpriteSystem(_atlas)

        spritesystem.initialize()

        img = pygame.image.load("particle2.bmp").convert_alpha()

        sprites = []

        bounds = 300
        for i in range(30):  # range(SpriteSystem._MAX_SPRITES):
            spr = GlSprite(img)
            spr.position = [random.randint(-bounds, bounds), random.randint(-bounds, bounds), 0.0]
            spritesystem.add_sprite(spr)
            sprites.append(spr)

        speed = 100
        for spr in sprites:
            spr.vel = [speed * random.random() - speed / 2, speed * random.random() - speed / 2, 0.0]

        create_sprite(img, sprites, spritesystem, [400, 300, 0.0])
        create_sprite(img, sprites, spritesystem, [40, 40, 0.0])

        create_sprite(img, sprites, spritesystem, [-400, 300, 0.0])
        create_sprite(img, sprites, spritesystem, [-40, 40, 0.0])

        create_sprite(img, sprites, spritesystem, [400, -300, 0.0])
        create_sprite(img, sprites, spritesystem, [40, -40, 0.0])

        create_sprite(img, sprites, spritesystem, [-400, -300, 0.0])
        create_sprite(img, sprites, spritesystem, [-40, -40, 0.0])

        create_sprite(img, sprites, spritesystem, [0, 0, 0.0])  # origin
        create_sprite(img, sprites, spritesystem, [40, 0, 0.0])  # x-axis
        create_sprite(img, sprites, spritesystem, [0, 50, 0.0])  # y-axis

        x_img = pygame.image.load("x-axis.png").convert_alpha()
        z_img = pygame.image.load("z-axis.png").convert_alpha()
        spr = GlSprite(x_img)
        spr.position = [16.0, 0.0, 110.0]
        spritesystem.add_sprite(spr)
        sprites.append(spr)

        y_img = pygame.image.load("y-axis.png").convert_alpha()
        spr = GlSprite(y_img)
        spr.position = [0.0, 16.0, 10.0]
        spr.rotation = [0.0, 0.0, -90.0]
        spritesystem.add_sprite(spr)
        sprites.append(spr)

        # spr = GlSprite(img)
        # spr.position = [0.0, 0.0, 0.40]
        # spr.rotation = [45.0, 0.0, 0.0]
        # spritesystem.add_sprite(spr)
        # sprites.append(spr)

        spr = GlSprite(img)
        spr.position = [80.0, -80.0, 0.1]
        spritesystem.add_sprite(spr)
        sprites.append(spr)

        spr = GlSprite(y_img)
        spr.scale = [5.0, 2.0, 1.0]
        spr.offset = [-5.0 * 16, 2 * 16, 0.2]
        spr.position = [80, -80, 0.0]
        spritesystem.add_sprite(spr)
        sprites.append(spr)

        t_sprite = spr

        spr = GlSprite(img)
        spr.position = [-80.0, -80.0, 0.0]
        spr.offset = [-16, -16, 0]
        spr.rotation = [0.0, 0.0, 45.0]
        spritesystem.add_sprite(spr)
        sprites.append(spr)

        spr = GlSprite(img)
        spr.position = [-80.0, -80.0, 0.0]
        spr.rotation = [0.0, 0.0, 45.0]
        spritesystem.add_sprite(spr)
        sprites.append(spr)
        z_sprite = spr

        random.shuffle(sprites)

        # aspect = screen_width / (1.0 * screen_height)
        # diam = bounds
        camera_types = [
            # glpy._Camera(glmath.perspective(45, aspect, 0.5, diam)),
            # # glpy._Camera(glmath.perspective_frustum(-1.0, 1.0, -screen_width/2, screen_width/2, screen_height/2, -screen_height/2)),
            # # glpy._Camera(glmath.perspective_frustum_symmetric(0.5, diam, screen_width, screen_height)),
            # # glpy._Camera(glmath.orthographic_symmetric(0.5, diam, screen_width, screen_height)),
            # glpy._Camera(glmath.orthographic(-1.0, 1.0 + diam, -1.0 * aspect, 1.0 * aspect, 1.0, -1.0)),
            # glpy._Camera(glmath.orthographic(-1.0, 1.0 + diam, -diam * aspect, diam * aspect, diam, -diam)),
            # # center object
            # # glpy._Camera(glmath.orthographic(-1.0, 1.0, 0.0, screen_width, 0.0, -screen_height)),  # origin; topleft
            glpy.PygameCamera(screen_width, screen_height),
            # glpy._Camera(glmath.orthographic(-1.0, 1.0, 0.0, screen_width, screen_height, 0.0)),  # origin; bottomleft
            # glpy._Camera(glmath.orthographic_symmetric(-1.0, 1.0, screen_width, screen_height)),  # origin: center
        ]
        cam_idx = 0
        cam = camera_types[cam_idx]

        clock = pygame.time.Clock()
        next_update = 0

        _atlas.save("atlas.png")

        stepping = False
        running = True
        while running:
            dt = clock.tick() / 1000.0  # delta in seconds
            # events
            events = [pygame.event.wait()] if stepping else pygame.event.get()
            for event in events:
                if event.type == pygame.QUIT:
                    running = False
                elif event.type == pygame.KEYDOWN:
                    dz = 0.2
                    dx = 10
                    dy = 10
                    da = 0.1
                    if event.key == pygame.K_ESCAPE:
                        running = False
                    elif event.key == pygame.K_s:
                        stepping = not stepping
                        print("stepping:", stepping)
                    elif event.key == pygame.K_PAGEUP:
                        cam.move(glmath.multsv3(dz, cam.look_direction))
                    elif event.key == pygame.K_PAGEDOWN:
                        cam.move(glmath.multsv3(-dz, cam.look_direction))
                    elif event.key == pygame.K_TAB:
                        cam_idx += 1
                        cam_idx %= len(camera_types)
                        camera_types[cam_idx].position = cam.position
                        camera_types[cam_idx].look_at = cam.look_at
                        cam = camera_types[cam_idx]
                    elif event.key == pygame.K_RIGHT:
                        cam.move([dx, 0.0, 0.0])
                    elif event.key == pygame.K_LEFT:
                        cam.move([-dx, 0.0, 0.0])
                    elif event.key == pygame.K_UP:
                        cam.move([0.0, -dy, 0.0])
                    elif event.key == pygame.K_DOWN:
                        cam.move([0.0, dy, 0.0])
                    elif event.key == pygame.K_o:
                        x, y, z = z_sprite.position
                        z_sprite.position = [x, y, z + dz]
                        z_sprite.dirty_list.add(z_sprite)
                        print("spr pos: ", z_sprite.position)
                    elif event.key == pygame.K_l:
                        x, y, z = z_sprite.position
                        z_sprite.position = [x, y, z - dz]
                        z_sprite.dirty_list.add(z_sprite)
                        print("spr pos: ", z_sprite.position)
                    elif event.key == pygame.K_k:
                        t_sprite.alpha -= da
                        if t_sprite.alpha < 0:
                            t_sprite.alpha = 0.0
                        t_sprite.dirty_list.add(t_sprite)
                    elif event.key == pygame.K_i:
                        t_sprite.alpha += da
                        if t_sprite.alpha > 1.0:
                            t_sprite.alpha = 1.0
                        t_sprite.dirty_list.add(t_sprite)
                    elif event.key == pygame.K_HOME:
                        # flip the look at about the z axis
                        cam.look_at = glmath.subv3(cam.position, cam.look_direction)
                    print("using cam with idx:", cam_idx, "at position", cam.position, "looking at:", cam.look_at, "dir:", cam.look_direction)

            if pygame.time.get_ticks() > next_update:
                print("dt:", dt, "fps:", 1.0 / (dt if dt > 0 else 1.0), clock.get_fps())
                next_update += 5 * 1000

            update_sprites_data(bounds, dt, spritesystem)

            # draw
            glClearColor(0.1, 0.5, 0.5, 1.0)
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

            # do_timing(spritesystem.draw, cam)
            spritesystem.draw(cam)

            pygame.display.flip()

        pygame.quit()


    def create_sprite(img, sprites, spritesystem, position):
        spr = GlSprite(img)
        spr.position = position
        spritesystem.add_sprite(spr)
        sprites.append(spr)

    def update_sprites_data(bounds, dt, spritesystem):
        data = spritesystem._data
        for start in range(9, spritesystem._last_free_idx * spritesystem._SPRITE_FLOAT_COUNT,
                           spritesystem._SPRITE_FLOAT_COUNT):
            position = data[start:start + 3]
            vel = data[start + 8:start + 8 + 3]

            position = [position[0] + vel[0] * dt, position[1] + vel[1] * dt, position[2] + vel[2] * dt]
            if position[0] > bounds or position[0] < -bounds:
                vel[0] = -vel[0]
            if position[1] > bounds or position[1] < -bounds:
                vel[1] = -vel[1]

            data[start:start + 3] = position
            data[start + 8:start + 8 + 3] = vel

    if __name__ == '__main__':
        main()

except Exception as ex:
    # trying to catch any exceptions and print them out in a way that the user will see them!
    sys.stderr.write("Error:")
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    sys.stderr.write(str(ex))
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback.print_tb(exc_traceback, file=sys.stderr)
    sys.stderr.write('\n')
    sys.stderr.write('Press any key... ')
    sys.stderr.flush()
    sys.stdin.readline()
    logging.shutdown()
