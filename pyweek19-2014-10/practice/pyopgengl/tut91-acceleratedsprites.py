# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'tut91-acceleratedsprites.py' is part of HG_pyweek_games
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]                    # For instance:
                   |                                  * 1.2.0.1 instead of 1.2-a
                   +-|* 0 for alpha (status)          * 1.2.1.2 instead of 1.2-b2 (beta with some bug fixes)
                     |* 1 for beta (status)           * 1.2.2.3 instead of 1.2-rc (release candidate)
                     |* 2 for release candidate       * 1.2.3.0 instead of 1.2-r (commercial distribution)
                     |* 3 for (public) release        * 1.2.3.5 instead of 1.2-r5 (commercial dist with many bug fixes)


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division

import os
import time

# import OpenGL
# OpenGL.FULL_LOGGING = True  # very slow
import random

from OpenGL.raw.GL.VERSION.GL_1_0 import glViewport, glClear
from OpenGL.raw.GL.VERSION.GL_1_1 import GL_COLOR_BUFFER_BIT, GL_DEPTH_BUFFER_BIT

try:
    import sys
    import traceback
    import logging

    logging.basicConfig()
    import pygame

    from OpenGL.GL import GLfloat, sizeof, glBufferSubData, GL_ARRAY_BUFFER, glEnable, GL_BLEND, glBlendFunc, \
    GL_SRC_ALPHA, glDepthMask, glDisable, GL_RASTERIZER_DISCARD, GL_ONE, glUseProgram, glGetUniformLocation, \
    glUniformMatrix4fv, GL_FALSE, glUniform3fv, glUniform1i, glBindVertexArray, glDisableVertexAttribArray, \
    glDrawArrays, GL_POINTS, glBindBuffer, glGenVertexArrays, glGenBuffers, glBufferData, GL_DYNAMIC_DRAW, \
    glVertexAttribPointer, GL_FLOAT, glGenTextures, glActiveTexture, GL_TEXTURE0, GL_TEXTURE_2D, glBindTexture, \
    glTexImage2D, GL_RGBA, GL_UNSIGNED_BYTE, glTexParameteri, GL_TEXTURE_WRAP_S, GL_TEXTURE_WRAP_T, \
    GL_TEXTURE_MIN_FILTER, GL_TEXTURE_MAG_FILTER, GL_CLAMP_TO_EDGE, GL_LINEAR, shaders, GL_VERTEX_SHADER, \
    GL_FRAGMENT_SHADER, GL_GEOMETRY_SHADER, GL_RGB, glFlush, glFinish, glEnableVertexAttribArray

    import glpy
    from glpy import get_ptr, NULL, get_offset_from_index, OrthogonalCamera

    import glmath

    logger = logging.getLogger(__name__)

    __version__ = '1.0.0.0'

    # for easy comparison as in sys.version_info but digits only
    __version_info__ = tuple([int(_d) for _d in __version__.split('.')])

    __author__ = "DR0ID"
    __email__ = "dr0iddr0id {at} gmail [dot] com"
    __copyright__ = "DR0ID @ 2018"
    __credits__ = ["DR0ID"]  # list of contributors
    __maintainer__ = "DR0ID"
    __license__ = "New BSD license"

    __all__ = []  # list of public visible parts of this module 

    SCREENSIZE = 800, 600

    #
    # class _SpriteFacade(object):
    #
    #     _idx00_v1 = 0
    #     _idx01_v2 = _idx00_v1 + 3
    #     _idx02_v3 = _idx01_v2 + 3
    #     _idx03_v4 = _idx02_v3 + 3
    #     _idx04_scale = _idx03_v4 + 3
    #     _idx05_offset = _idx04_scale + 3
    #     _idx06_rotation = _idx05_offset + 3
    #     _idx07_position = _idx06_rotation + 3
    #     _idx08_alpha = _idx07_position + 3
    #     _idx09_tex_coord = _idx08_alpha + 1
    #     _idx10_tex_size = _idx09_tex_coord + 2
    #     _idx11_frame_count = _idx10_tex_size + 2
    #     _idx12_curr_frame = _idx11_frame_count + 1
    #     _idx13_tpf = _idx12_curr_frame + 1
    #     _idx14_time = _idx13_tpf + 1
    #     _idx15_direction = _idx14_time + 1
    #     _idx16_velocity = _idx15_direction + 3
    #     _idx17_acceleration = _idx16_velocity + 3
    #     _idx18_rot_velocity = _idx17_acceleration + 3
    #     _idx19_rot_acceleration = _idx18_rot_velocity + 3
    #
    #     data_size = sizeof((GLfloat * (_idx19_rot_acceleration + 3))())  # last entry + size of last entry
    #
    #     def __init__(self, idx, dirty_sprites):
    #
    #         # self._data = [
    #         #     1.0, 1.0, 0.0,  # vertex1
    #         self._set_data((1.0, 1.0, 0.0), self._idx00_v1, self._idx01_v2)
    #         #     1.0, -1.0, 0.0,  # vertex2
    #         self._set_data((1.0, -1.0, 0.0), self._idx01_v2, self._idx02_v3)
    #         #     -1.0, -1.0, 0.0,  # vertex3
    #         self._set_data((-1.0, -1.0, 0.0), self._idx00_v3, self._idx03_v4)
    #         #     -1.0, 1.0, 0.0,  # vertex4
    #         self._set_data((-1.0, 1.0, 0.0), self._idx00_v4, self._idx05_offset)
    #         #
    #         #     # transformation
    #         #     1.0, 1.0, 1.0,  # scale
    #         #     0.0, 0.0, 0.0,  # offset
    #         #     0.0, 0.0, 0.0,  # rotation
    #         #     0.0, 0.0, 0.0,  # position
    #         #
    #         #     # image
    #         #     1.0,  # alpha
    #         #     0.0, 0.0,  # texture coord
    #         #     1.0, 1.0,  # texture size w, h
    #         #
    #         #     # animation
    #         #     1.0,   # frame count for animation, int?
    #         #     0.0,  # current frame of the animation, int?
    #         #     1.0 / 10.0,  # time per frame
    #         #
    #         #     0.0,  # individual time
    #         #
    #         #     # physics for boids
    #         #     1.0, 0.0, 0.0,  # direction
    #         #     0.0, 0.0, 0.0,  # velocity
    #         #     0.0, 0.0, 0.0,  # acceleration
    #         #     0.0, 0.0, 0.0,  # omega, direction is axis of rotation, length is rotational speed
    #         #     0.0, 0.0, 0.0,  # direction is axis of rotation, length is rotational acceleration
    #         #
    #         # ]
    #
    #         self.idx = idx
    #         self.dirty_sprites = dirty_sprites
    #         self._dirty_bytes = set()
    #
    #     def set_scale(self, sx, sy, sz):
    #         self._set_data((sx, sy, sz), self._idx04_scale, self._idx05_offset)
    #
    #     def _set_data(self, data, start_idx, end_idx):
    #         # self._data[start_idx: end_idx] = data
    #         self._dirty_bytes.add((start_idx, data))
    #         _SpriteFacade.dirty_sprites.add(self)
    #
    # class SpriteSystem(object):
    #
    #     def __init__(self):
    #         self._dirty_sprites = set()
    #         self._unused_slots = []  # add unused slots
    #         self._next_idx = 0
    #
    #     def create_sprite(self):
    #         idx = self._next_idx
    #         if self._unused_slots:
    #             self._unused_slots.sort()
    #             idx = self._unused_slots.pop()
    #         spr = _SpriteFacade(idx, self._dirty_sprites)
    #         self._dirty_sprites.add(spr)
    #         return spr
    #
    #     def remove_sprite(self, spr):
    #         self._unused_slots.append(spr.idx)
    #
    #
    #     def initialize(self):
    #         pass
    #
    #     def draw(self):
    #         size_of_particle = _SpriteFacade.data_size
    #         for spr in _SpriteFacade.dirty_sprites:
    #             idx = spr.idx
    #             for start_idx, particle in spr._dirty_bytes:
    #                 data_ptr = get_ptr(particle)
    #                 glBufferSubData(GL_ARRAY_BUFFER, idx * size_of_particle + start_idx, data_ptr)
    #
    #         pass

    def is_power_of_two(n):
        return n and not (n & (n - 1))

    class Atlas(object):

        def __init__(self, size):
            if not (is_power_of_two(size[0]) and is_power_of_two(size[1])):
                raise Exception("Texture size should be power of two, provided: " + str(size))
            self._tex_surf = pygame.Surface(size, pygame.SRCALPHA, 32)
            self._tex_surf.fill((0, 0, 0, 0), None, pygame.BLEND_RGBA_MULT)
            self._last_rect = pygame.Rect(0, 0, 0, 0)
            self._max_per_line = 0
            self._cache = {}  # {id: (rect, img)}
            self.is_dirty = True

        def add(self, image):
            if image is None:
                raise Exception("image should not be None!")

            _img_id = id(image)
            if _img_id in self._cache:
                return self._cache[_img_id]

            self.is_dirty = True
            img_rect = image.get_rect(topleft=self._last_rect.topright)

            _tex_w, _tex_h = self._tex_surf.get_size()
            if img_rect.right > _tex_w:
                img_rect.topleft = (0, self._max_per_line)
                if img_rect.bottom > _tex_h:
                    raise Exception("image does not fit: tex size {0} img_rect {1}".format((_tex_w, _tex_h), img_rect))

            if img_rect.bottom > self._max_per_line:
                self._max_per_line = img_rect.bottom

            self._last_rect = img_rect
            self._tex_surf.blit(image, img_rect, None, pygame.BLEND_RGBA_ADD)

            result = img_rect.topleft, image.get_size()
            self._cache[_img_id] = result
            return result

        def get_texture_data(self, tex_format="RGBA"):
            w, h = self._tex_surf.get_size()
            return pygame.image.tostring(self._tex_surf, tex_format, 0), w, h

    class GlSprite(object):

        def __init__(self, image):

            # sprite management, don't use them
            self.dirty_list = None
            self.draw_idx = -1
            self.tex_coord = [0.0, 0.0]
            self.text_size = list(image.get_size())

            # transformation
            self.scale = [1.0, 1.0, 1.0]  # sx, sy, sz
            self.offset = [0.0, 0.0, 0.0]  # ox, oy, oz
            self.rotation = [0.0, 0.0, 0.0]  # rx, ry, rz
            self.position = [0.0, 0.0, 0.0]  # px, py, pz

            # image
            self.alpha = 1.0
            self.image = image

            self.vel = [0.0, 0.0, 0.0]

            # # animation
            # self.frame_count = 1
            # self.current_frame = 0
            # self.tpf = 1.0 / 10.0  # time per frame, 1 / fps
            # self.time = 0.0

            # self._data = [
            #     1.0, 1.0, 0.0,  # vertex1
            #     1.0, -1.0, 0.0,  # vertex2
            #     -1.0, -1.0, 0.0,  # vertex3
            #     -1.0, 1.0, 0.0,  # vertex4
            #
            #     # transformation
            #     1.0, 1.0, 1.0,  # scale
            #     0.0, 0.0, 0.0,  # offset
            #     0.0, 0.0, 0.0,  # rotation
            #     0.0, 0.0, 0.0,  # position
            #
            #     # image
            #     1.0,  # alpha
            #     0.0, 0.0,  # texture coord
            #     1.0, 1.0,  # texture size w, h
            #
            #     # animation
            #     1.0,   # frame count for animation, int?
            #     0.0,  # current frame of the animation, int?
            #     1.0 / 10.0,  # time per frame
            #
            #     0.0,  # individual time
            #
            #     # physics for boids
            #     1.0, 0.0, 0.0,  # direction
            #     0.0, 0.0, 0.0,  # velocity
            #     0.0, 0.0, 0.0,  # acceleration
            #     0.0, 0.0, 0.0,  # omega, direction is axis of rotation, length is rotational speed
            #     0.0, 0.0, 0.0,  # direction is axis of rotation, length is rotational acceleration
            #
            # ]


    class SpriteSystem(object):

        _MAX_SPRITES = 10001
        _SPRITE_FLOAT_COUNT = 17

        vert_shader_source = """
#version 420 core

layout (location = 0) in vec3 vScale;
layout (location = 1) in vec3 vOffset;
layout (location = 2) in vec3 vRotation;
layout (location = 3) in vec3 vPosition;
layout (location = 4) in float fAlpha;
layout (location = 5) in vec2 vTexCoord;
layout (location = 6) in vec2 vTexSize;
        
// out vec3 vScalePass;
// out vec3 vOffsetPass;
// out vec3 vRotationPass;
// flat out float fAlphaPass;
// smooth out vec2 vTexCoordPass;
// flat out vec2 vTexSizePass;

void main()
{
    gl_Position = vec4(vPosition, 1.0);
    // gl_Position = vec4(0.0, 0.0, 0.0, 1.0);

//    vScalePass = vScale;
//    vOffsetPass = vOffset;
//    vRotationPass = vRotation;
//    fAlphaPass = fAlpha;
//    vTexCoordPass = vTexCoord;
//    vTexSizePass = vTexSize;
}
        
        """

        geo_shader_source = """
#version 420 core

uniform struct Matrices
{
    mat4 mProj;
    mat4 mView;
} matrices;

layout(points) in;
layout(triangle_strip) out;
layout(max_vertices = 4) out;

// in vec3 vScalePass[];
// in vec3 vOffsetPass[];
// in vec3 vRotationPass[];
// in float vAlphaPass[];
// in vec2 vTexCoordPass[];
// in vec2 vTexSizePass[];

smooth out vec2 vTexCoordinates;
// flat out float vAlpha;

//vec4 applyTransformations(vec3 centerPos, vec3 vertexCoord, vec3 scale, vec3 offset, vec3 rotation){
//
//    vec3 pos = centerPos + vertexCoord;
//    pos = vec3(pos.x * scale.x, pos.y * scale.y, pos.z * scale.z);
//    pos = pos + offset;
//    float angle = length(rotation);
//    // todo: rotation transformation
//    //vec3 axis = rotation.normalize();
//    float s = sin(angle);
//    float c = cos(angle);
//    pos.x = pos.x * c + pos.y * s;
//    pos.y = pos.x * -s + pos.y * c;
//
//    return vec4(pos, 1.0);
//}

void main()
{
    vec3 oldPos = gl_in[0].gl_Position.xyz;
    mat4 mVP = matrices.mProj * matrices.mView;
    vec2 vTc = vec2(0.0, 0.0);
    
    vTexCoordinates = vTc + vec2(0.0, 0.0);
    vec4 vTransformed = vec4(oldPos + vec3(-1.0, -1.0, 0.0), 1.0);
    gl_Position = mVP * vTransformed;
    EmitVertex();

    vTexCoordinates = vTc + vec2(0.0, 1.0);
    vTransformed = vec4(oldPos + vec3(-1.0, 1.0, 0.0), 1.0);
    gl_Position = mVP * vTransformed;
    EmitVertex();

    vTexCoordinates = vTc + vec2(1.0, 0.0);
    vTransformed = vec4(oldPos + vec3(1.0, -1.0, 0.0), 1.0);
    gl_Position = mVP * vTransformed;
    EmitVertex();

    vTexCoordinates = vTc + vec2(1.0, 1.0);
    vTransformed = vec4(oldPos + vec3(1.0, 1.0, 0.0), 1.0);
    gl_Position = mVP * vTransformed;
    EmitVertex();

    EndPrimitive();
}

/*
void main_()
{
    vAlpha = vAlphaPass[0];
    
    vec3 oldPos = gl_in[0].gl_Position.xyz;
    mat4 mVP = matrices.mProj * matrices.mView;
    vec2 vTc = vTexCoordPass[0];
    vec2 vTs = vTexSizePass[0];
    
    vTexCoordinates = vTc + vec2(0.0, 0.0);
    vec4 vTransformed = applyTransformations(oldPos, vec3(-1.0, -1.0, 0.0), vScalePass, vOffsetPass, vRotationPass);
    gl_Position = mVP * vTransformed;
    EmitVertex();

    vTexCoordinates = vTc + vec2(0.0, 1.0);
    vTransformed = applyTransformations(oldPos, vec3(-1.0, 1.0, 0.0), vScalePass, vOffsetPass, vRotationPass);
    gl_Position = mVP * vTransformed;
    EmitVertex();

    vTexCoordinates = vTc + vec2(1.0, 0.0);
    vTransformed = applyTransformations(oldPos, vec3(1.0, -1.0, 0.0), vScalePass, vOffsetPass, vRotationPass);
    gl_Position = mVP * vTransformed;
    EmitVertex();

    vTexCoordinates = vTc + vec2(1.0, 1.0);
    vTransformed = applyTransformations(oldPos, vec3(1.0, 1.0, 0.0), vScalePass, vOffsetPass, vRotationPass);
    gl_Position = mVP * vTransformed;
    EmitVertex();
    
    EndPrimitive();
}
*/
        """

        fag_shader_source = """
#version 420 core

uniform sampler2D gSampler;

smooth in vec2 vTexCoordinates;
// flat in float vAlpha;

out vec4 FragColor;

void main()
{
    vec4 vTexColor = texture2D(gSampler, vTexCoordinates);
//    FragColor = vec4(vTexColor.xyz, vAlpha * vTexColor.w);
    FragColor = vec4(vTexColor.xyz, vTexColor.w);
//    FragColor = vec4(1.0, 1.0, 1.0, 1.0);
}
        
        """

        def __init__(self, atlas):
            self.buffer_id = None
            self.vao_id = None
            self.tex_id = None
            self.sampler_id = 0
            self.shader_prog = None

            self._initialized = False
            self._unused_slots_to_append = set()
            self._dirty_sprites = set()
            self._sprite_count = 0
            self._last_free_idx = 0
            self._unused_slots = []

            self._atlas = atlas

        def create_sprite(self):
            self._check_max_sprites()

            self._sprite_count += 1

            spr = GlSprite()
            self._add_sprite(spr)
            return spr

        def _check_max_sprites(self):
            if self._sprite_count >= self._MAX_SPRITES:
                raise Exception("Max number of sprites reached: " + str(self._MAX_SPRITES))

        def add_sprite(self, spr):
            self._check_max_sprites()
            if spr.dirty_list is not None:
                raise Exception("sprite should not have dirty_list already set! Avoid adding same sprite twice!")
            self._add_sprite(spr)

        def _add_sprite(self, spr):
            spr.dirty_list = self._dirty_sprites
            spr.draw_idx = self._get_next_draw_index()
            self._dirty_sprites.add(spr)
            spr.tex_coord, spr.tex_size = self._atlas.add(spr.image)

        def _get_next_draw_index(self):
            draw_idx = self._last_free_idx
            if self._unused_slots:
                self._unused_slots.sort()
                draw_idx = self._unused_slots.pop()
            else:
                self._last_free_idx += 1
            return draw_idx

        def remove_sprite(self, spr):
            self._sprite_count -= 1
            self._unused_slots_to_append.append(spr.draw_idx)

            self._dirty_sprites.add(spr)
            spr.dirty_list = None  # set sprite dirty list to None to prevent further drawing of this sprite

        def initialize(self):
            # shader
            vertex_shader = shaders.compileShader(self.vert_shader_source, GL_VERTEX_SHADER)
            geo_shader = shaders.compileShader(self.geo_shader_source, GL_GEOMETRY_SHADER)
            frag_shader = shaders.compileShader(self.fag_shader_source, GL_FRAGMENT_SHADER)
            self.shader_prog = shaders.compileProgram(vertex_shader, geo_shader, frag_shader)
            # self.shader_prog = shaders.compileProgram(vertex_shader, frag_shader)

            self.shader_prog.check_validate()
            self.shader_prog.check_linked()

            # buffers
            self.vao_id = glGenVertexArrays(1)
            self.buffer_id = glGenBuffers(1)

            glBindVertexArray(self.vao_id)
            glBindBuffer(GL_ARRAY_BUFFER, self.buffer_id)
            data_ptr = (GLfloat * self._SPRITE_FLOAT_COUNT)()
            size_of_sprite = sizeof(data_ptr)
            glBufferData(GL_ARRAY_BUFFER, size_of_sprite * self._MAX_SPRITES, NULL, GL_DYNAMIC_DRAW)

            for j in range(7):
                glEnableVertexAttribArray(j)

            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, size_of_sprite, get_offset_from_index(0))  # scale
            glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, size_of_sprite, get_offset_from_index(3))  # offset
            glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, size_of_sprite, get_offset_from_index(6))  # rotation
            glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, size_of_sprite, get_offset_from_index(9))  # position
            glVertexAttribPointer(4, 1, GL_FLOAT, GL_FALSE, size_of_sprite, get_offset_from_index(12))  # alpha
            glVertexAttribPointer(5, 2, GL_FLOAT, GL_FALSE, size_of_sprite, get_offset_from_index(13))  # tex coord
            glVertexAttribPointer(6, 2, GL_FLOAT, GL_FALSE, size_of_sprite, get_offset_from_index(15))  # tex size
            # glVertexAttribPointer(7, 1, GL_FLOAT, GL_FALSE, size_of_sprite, get_offset_from_index(17))  # current frame

            # texture atlas
            self.tex_id = glGenTextures(1)

            self._initialized = True

        def draw(self, cam):
            if not self._initialized:
                return

            if self._atlas.is_dirty:
                # update texture
                t_data, t_w, t_h = self._atlas.get_texture_data("RGBA")
                self._update_texture(t_data, t_w, t_h, self.sampler_id, self.tex_id)
                self._atlas.is_dirty = False  # todo: hack!

            # bind buffer first
            glEnable(GL_BLEND)
            glBlendFunc(GL_SRC_ALPHA, GL_ONE)
            # glDisable(GL_BLEND)

            glDepthMask(1)

            glDisable(GL_RASTERIZER_DISCARD)
            glUseProgram(self.shader_prog)

            u_loc = glGetUniformLocation(self.shader_prog, "matrices.mProj")
            glUniformMatrix4fv(u_loc, 1, GL_FALSE, cam.projection)

            u_loc = glGetUniformLocation(self.shader_prog, "matrices.mView")
            glUniformMatrix4fv(u_loc, 1, GL_FALSE, cam.view)

            # u_loc = glGetUniformLocation(self.render_shader_program, "vQuad1")
            # glUniform3fv(u_loc, 1, self.v3_quad1)
            #
            # u_loc = glGetUniformLocation(self.render_shader_program, "vQuad2")
            # glUniform3fv(u_loc, 1, self.v3_quad2)

            u_loc = glGetUniformLocation(self.shader_prog, "gSampler")
            glUniform1i(u_loc, self.sampler_id)

            # glDisableVertexAttribArray(1)
            glBindBuffer(GL_ARRAY_BUFFER, self.buffer_id)

            # update data before draw
            self._unused_slots.extend(self._unused_slots_to_append)
            self._unused_slots_to_append.clear()

            do_timing(self._update_vbo_for_dirty_sprites)

            # draw
            glBindVertexArray(self.vao_id)
            glDrawArrays(GL_POINTS, 0, self._last_free_idx)

            # glDepthMask(1)
            # glDisable(GL_BLEND)
            glUseProgram(0)
            glBindVertexArray(0)  # prevent that vao is changed outside
            glBindBuffer(GL_ARRAY_BUFFER, 0)
            glDisable(GL_BLEND)

        def _update_vbo_for_dirty_sprites(self):
            for spr in self._dirty_sprites:
                data = [
                    spr.scale[0], spr.scale[1], spr.scale[2],
                    spr.offset[0], spr.offset[1], spr.offset[2],
                    spr.rotation[0], spr.rotation[1], spr.rotation[2],
                    spr.position[0], spr.position[1], spr.position[2],
                    spr.alpha,
                    spr.tex_coord[0], spr.tex_coord[1],
                    spr.tex_size[0], spr.tex_size[1],
                ]
                if __debug__:
                    if len(data) != self._SPRITE_FLOAT_COUNT:
                        raise Exception("initialize and draw should use the same number of bytes!")

                # glBindBuffer(GL_ARRAY_BUFFER, self.buffer_id)
                data_ptr = get_ptr(data)
                size_of_particle = sizeof(data_ptr)
                glBufferSubData(GL_ARRAY_BUFFER, spr.draw_idx * size_of_particle, data_ptr)
                # print("updating dirty sprite {1} at: {0} \n{2} \nsize {3}".format(spr.position, spr.draw_idx, data,
                #                                                                   size_of_particle))
            self._dirty_sprites.clear()

        def _update_texture(self, texture_data, width, height, sampler_id, tex_id):
            glActiveTexture(GL_TEXTURE0 + sampler_id)  # texture bind location for texture unit 0
            glBindTexture(GL_TEXTURE_2D, tex_id)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_data)
            # glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_data)

            # defines how the texture is sampled
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)


    def do_timing(func, *args):
        start = time.time()
        result = func(*args)
        end = time.time()
        duration = end - start
        print("duration {0}: {1}s".format(func.__name__, duration))
        return result

    def main():
        # init window and context
        screen_width = 800
        screen_height = 600

        pygame.init()
        os.environ['SDL_VIDEO_CENTERED'] = '1'

        pygame.display.set_caption("OpenGL - particles")

        pygame.display.set_mode((screen_width, screen_height), pygame.OPENGL | pygame.DOUBLEBUF)
        glViewport(0, 0, screen_width, screen_height)

        _atlas = Atlas((64, 64))
        spritesystem = SpriteSystem(_atlas)

        spritesystem.initialize()

        img = pygame.image.load("particle.bmp").convert_alpha()
        spr1 = GlSprite(img)
        spritesystem.add_sprite(spr1)

        sprites = []

        bounds = 10
        for i in range(10000):
            spr = GlSprite(img)
            # spr.position = [random.randint(-screen_width, screen_width), random.randint(-screen_height, screen_height), 0.0]
            spr.position = [random.randint(-bounds, bounds), random.randint(-bounds, bounds), 0.0]
            spritesystem.add_sprite(spr)
            sprites.append(spr)

        random.shuffle(sprites)

        for spr in sprites:
            spr.vel = [4 * random.random() - 2, 4 * random.random() - 2, 0.0]

        cam = glpy._Camera(glmath.perspective(45, screen_width / (1.0 * screen_height), 0.5, 1000.0))
        # cam = glpy.OrthogonalCamera()
        cam.position = [0.0, 0.0, 15.0]
        # cam.position = [-71.41778489984125, 0.0, -71.4177848998413]

        direction = 0.1
        clock = pygame.time.Clock()

        running = True
        while running:
            dt = clock.tick() / 1000.0  # delta in seconds
            # events
            for event in pygame.event.get():

                if event.type == pygame.QUIT:
                    running = False
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        running = False
                    elif event.key == pygame.K_UP:
                        cam.move(glmath.multsv3(10, cam.look_direction))
                    elif event.key == pygame.K_DOWN:
                        cam.move(glmath.multsv3(-10, cam.look_direction))
                    print("cam at pos: " + str(cam.position))

            # update
            # if spr.position[0] > 10 or spr.position[0] < -10:
            #     direction = -direction
            # dirv3 = [direction * dt] * 3
            # spr.position = glmath.addv3(dirv3, spr.position)
            # spr.dirty_list.add(spr)  # todo: hack
            print("dt:", dt, 1.0/(dt if dt > 0 else 1.0))
            do_timing(update_sprites, bounds, dt, sprites)


            # draw
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

            do_timing(spritesystem.draw, cam)

            pygame.display.flip()
            # screen.fill((255, 0, 255))

        pygame.quit()


    def update_sprites(bounds, dt, sprites):
        for spr in sprites:
            spr.position = glmath.mult_addv3(spr.position, spr.vel, dt)
            if spr.position[0] > bounds or spr.position[0] < -bounds:
                spr.vel[0] = -spr.vel[0]
            if spr.position[1] > bounds or spr.position[1] < -bounds:
                spr.vel[1] = -spr.vel[1]
            spr.dirty_list.add(spr)


    if __name__ == '__main__':
        main()

except Exception as ex:
    # trying to catch any exceptions and print them out in a way that the user will see them!
    sys.stderr.write("Error:")
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    sys.stderr.write(str(ex))
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback.print_tb(exc_traceback, file=sys.stderr)
    sys.stderr.write('\n')
    sys.stderr.write('Press any key... ')
    sys.stderr.flush()
    sys.stdin.readline()
    logging.shutdown()
