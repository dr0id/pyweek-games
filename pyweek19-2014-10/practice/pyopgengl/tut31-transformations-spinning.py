# -*- coding: utf-8 -*-
"""
A 'modern' OpenGL example using pygame and pyopengl

Based on

https://open.gl/transformations
"""
from __future__ import print_function

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 

import os

from math import sin, cos, radians
from ctypes import c_float, c_void_p, sizeof, c_int
import pygame
from OpenGL.GL import glGenBuffers, glBindBuffer, GL_ARRAY_BUFFER, glBufferData, GL_STATIC_DRAW, glCreateShader, \
    GL_VERTEX_SHADER, glShaderSource, glCompileShader, glGetShaderiv, GL_COMPILE_STATUS, GL_TRUE, glGetShaderInfoLog, \
    GL_FRAGMENT_SHADER, glCreateProgram, glAttachShader, glBindFragDataLocation, glLinkProgram, \
    glUseProgram, GL_FLOAT, glGetAttribLocation, glVertexAttribPointer, glEnableVertexAttribArray, \
    glGenVertexArrays, glBindVertexArray, glClearColor, \
    glClear, glDeleteProgram, glDeleteShader, glDeleteBuffers, \
    glDeleteVertexArrays, GL_FALSE, GL_TRIANGLES, GL_COLOR_BUFFER_BIT, GL_DEPTH_BUFFER_BIT, \
    GL_ELEMENT_ARRAY_BUFFER, GL_UNSIGNED_INT, glDrawElements, glTexImage2D, GL_TEXTURE_2D, GL_RGB, \
    GL_UNSIGNED_BYTE, glDeleteTextures, glGenTextures, glTexParameteri, GL_TEXTURE_WRAP_S, \
    GL_CLAMP_TO_EDGE, GL_TEXTURE_WRAP_T, GL_TEXTURE_MIN_FILTER, GL_LINEAR, GL_TEXTURE_MAG_FILTER, \
    glActiveTexture, GL_TEXTURE0, GL_TEXTURE1, glBindTexture, glUniform1i, glGetUniformLocation, \
    glUniformMatrix4fv
# enable error when array data is copied
from OpenGL.arrays import numpymodule
numpymodule.NumpyHandler.ERROR_ON_COPY = True

import glmath



def main():
    """
    Main method. In this simple program it contains all the code.

    :raise RuntimeError:
    """
    width = 800
    height = 600

    pygame.init()
    os.environ['SDL_VIDEO_CENTERED'] = '1'
    pygame.display.set_mode((width, height), pygame.OPENGL | pygame.DOUBLEBUF)

    # these would be nice to configure in the openGL context, but I don't know how (if its
    # even possible using pygame)
    #
    # SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    # SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    # SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);

    # crating Vertex Array Object so save shader attributes
    vao = glGenVertexArrays(1)
    glBindVertexArray(vao)

    elements = [
        0, 1, 2,
        2, 3, 0
    ]

    # vertices
    #   position   color         tex coord
    vertices = [
        -0.5, 0.5, 1.0, 0.0, 0.0, 0.0, 0.0,  # topleft
        0.5, 0.5, 0.0, 1.0, 0.0, 1.0, 0.0,  # topright
        0.5, -0.5, 0.0, 0.0, 1.0, 1.0, 1.0,  # bottomright
        -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,  # bottomleft
    ]

    vbo = glGenBuffers(1)
    glBindBuffer(GL_ARRAY_BUFFER, vbo)
    # noinspection PyCallingNonCallable
    vertices_buffer = (c_float * len(vertices))(*vertices)
    glBufferData(GL_ARRAY_BUFFER, vertices_buffer, GL_STATIC_DRAW)

    ebo = glGenBuffers(1)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo)
    # noinspection PyCallingNonCallable
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, (c_int * len(elements))(*elements), GL_STATIC_DRAW)

    # shader sources

    vertex_source = """
#version 150

in vec2 position;
in vec3 color;
in vec2 texcoord;

out vec3 Color;
out vec2 Texcoord;

uniform mat4 trans;

void main()
{
    Color = color;
    Texcoord = texcoord;
    gl_Position = trans * vec4(position, 0.0, 1.0);
}
"""

    fragment_source = """
#version 150

in vec3 Color;
in vec2 Texcoord;

out vec4 outColor;

uniform sampler2D texKitten;
uniform sampler2D texPuppy;

void main()
{
    vec4 colKitten = texture(texKitten, Texcoord);
    vec4 colPuppy =texture(texPuppy, Texcoord);
    outColor = mix(colKitten, colPuppy, 0.5);
}
"""
    vertex_shader = glCreateShader(GL_VERTEX_SHADER)
    glShaderSource(vertex_shader, vertex_source)
    glCompileShader(vertex_shader)
    if glGetShaderiv(vertex_shader, GL_COMPILE_STATUS) != GL_TRUE:
        raise RuntimeError(glGetShaderInfoLog(vertex_shader))

    fragment_shader = glCreateShader(GL_FRAGMENT_SHADER)
    glShaderSource(fragment_shader, fragment_source)
    glCompileShader(fragment_shader)
    if glGetShaderiv(fragment_shader, GL_COMPILE_STATUS) != GL_TRUE:
        raise RuntimeError(glGetShaderInfoLog(fragment_shader))

    # setting up shader program
    shader_program = glCreateProgram()
    glAttachShader(shader_program, vertex_shader)
    glAttachShader(shader_program, fragment_shader)

    # define which output is written to which buffer from the fragment shader
    glBindFragDataLocation(shader_program, 0, "outColor")

    glLinkProgram(shader_program)
    glUseProgram(shader_program)

    # link between vertex data and attributes
    pos_attrib = glGetAttribLocation(shader_program, "position")
    glEnableVertexAttribArray(pos_attrib)
    glVertexAttribPointer(pos_attrib, 2, GL_FLOAT, GL_FALSE, 7 * sizeof(c_float), c_void_p(0))

    color_attrib = glGetAttribLocation(shader_program, "color")
    glEnableVertexAttribArray(color_attrib)
    glVertexAttribPointer(color_attrib, 3, GL_FLOAT, GL_FALSE, 7 * sizeof(c_float), c_void_p(2 * sizeof(c_float)))

    tex_attrib = glGetAttribLocation(shader_program, "texcoord")
    glEnableVertexAttribArray(tex_attrib)
    glVertexAttribPointer(tex_attrib, 2, GL_FLOAT, GL_FALSE, 7 * sizeof(c_float), c_void_p(5 * sizeof(c_float)))

    # texture loading (texture uniform)
    textures = glGenTextures(2)

    img = pygame.image.load("sample.png")
    img = pygame.transform.flip(img, False, True)  # invert
    texture_data = pygame.image.tostring(img, "RGB", 1)  # get image data as array
    width, height = img.get_size()

    glActiveTexture(GL_TEXTURE0)
    glBindTexture(GL_TEXTURE_2D, textures[0])
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, texture_data)
    glUniform1i(glGetUniformLocation(shader_program, "texKitten"), 0)

    # defines how the texture is sampled
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)

    img = pygame.image.load("sample2.png")
    img = pygame.transform.flip(img, False, True)  # invert
    texture_data = pygame.image.tostring(img, "RGB", 1)  # get image data as array
    width, height = img.get_size()

    glActiveTexture(GL_TEXTURE1)
    glBindTexture(GL_TEXTURE_2D, textures[1])
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, texture_data)
    glUniform1i(glGetUniformLocation(shader_program, "texPuppy"), 1)

    # defines how the texture is sampled
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)

    uni_transform = glGetUniformLocation(shader_program, "trans")
    a = radians(180.0)
    mat = glmath.m4x4()
    mat = glmath.rotate4(mat, a, [0.0, 0.0, 1.0])
    glUniformMatrix4fv(uni_transform, 1, GL_FALSE, mat)

    # main loop
    clock = pygame.time.Clock()

    running = True
    while running:
        clock.tick(60)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False

        duration = pygame.time.get_ticks() / 1000.0  # convert to seconds

        # Clear the screen to black
        glClearColor(0, 0, 0, 1)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        # active shader program
        glUseProgram(shader_program)

        a = radians(180.0) * duration
        mat = glmath.m4x4()
        mat = glmath.rotate4(mat, a, [0.0, 0.0, 1.0])
        glUniformMatrix4fv(uni_transform, 1, GL_FALSE, mat)

        try:
            glBindVertexArray(vao)

            # draw triangle
            # 6 = len(elements) = number of indices
            glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, c_void_p(0))
        finally:
            glBindVertexArray(0)
            glUseProgram(0)

        pygame.display.flip()

    glDeleteProgram(shader_program)
    glDeleteShader(fragment_shader)
    glDeleteShader(vertex_shader)

    glDeleteTextures(textures)

    glDeleteBuffers(1, [vbo])
    glDeleteBuffers(1, [ebo])

    glDeleteVertexArrays(1, [vao])


if __name__ == '__main__':
    main()
