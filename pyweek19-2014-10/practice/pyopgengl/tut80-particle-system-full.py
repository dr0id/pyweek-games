# -*- coding: utf-8 -*-
"""
A 'modern' OpenGL example using pygame and pyopengl

Based on

http://www.mbsoftworks.sk/index.php?page=tutorials&series=1&tutorial=26
"""
from __future__ import print_function

import ctypes
import os
import random
from math import radians, cos, sin, tan
import time

import pygame
# import OpenGL
# OpenGL.FULL_LOGGING = True  # very slow
from OpenGL.GL import glGenBuffers, glBindBuffer, GL_ARRAY_BUFFER, glBufferData, GL_VERTEX_SHADER, \
    glLinkProgram, \
    glUseProgram, GL_FLOAT, glVertexAttribPointer, glEnableVertexAttribArray, \
    glGenVertexArrays, glBindVertexArray, GL_FALSE, GL_POINTS, GL_GEOMETRY_SHADER, \
    GL_INTERLEAVED_ATTRIBS, \
    glEnable, \
    GL_RASTERIZER_DISCARD, glBindBufferBase, GL_TRANSFORM_FEEDBACK_BUFFER, glBeginTransformFeedback, glDrawArrays, \
    glEndTransformFeedback, GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, shaders, glGenQueries, \
    glBeginQuery, \
    glEndQuery, GL_QUERY_RESULT, \
    GL_FRAGMENT_SHADER, glGenTransformFeedbacks, GL_DYNAMIC_DRAW, sizeof, glGetUniformLocation, glUniform3fv, \
    glUniform1f, glBindTransformFeedback, GL_TRANSFORM_FEEDBACK, glUniform1i, GL_BLEND, \
    GL_SRC_ALPHA, GL_ONE, glBlendFunc, glDepthMask, glDisable, glGenTextures, glTexImage2D, GL_TEXTURE_2D, \
    GL_RGB, GL_UNSIGNED_BYTE, GL_CLAMP_TO_EDGE, GL_TEXTURE_WRAP_S, GL_TEXTURE_WRAP_T, GL_TEXTURE_MIN_FILTER, \
    GL_TEXTURE_MAG_FILTER, GL_LINEAR, glTexParameteri, glUniformMatrix4fv, glDisableVertexAttribArray, glBindTexture, \
    glActiveTexture, GL_TEXTURE0, glViewport, c_char_p, GLchar, GLfloat, GLvoidp, glBufferSubData, \
    glClear, GL_COLOR_BUFFER_BIT, GL_DEPTH_BUFFER_BIT, glTransformFeedbackVaryings, \
    glBindBufferRange, glGetQueryObjectiv, glGetBufferSubData, glGetQueryObjectuiv

# enable error when array data is copied
# from OpenGL.arrays import numpymodule

from glpy import get_ptr, get_char_ptr_ptr

from glpy import NULL

from glpy import get_offset_from_index

from glmath import look_at

from glmath import subv3

from glmath import normalize3

from glmath import cross3

from glmath import perspective

# numpymodule.NumpyHandler.ERROR_ON_COPY = True

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"


# __all__ = []  # list of public visible parts of this module

# to profile: py -2 -m cProfile -s pcalls tut80-particle-system.py


# random helper method
def g_rand_f(f_min, f_add):
    return f_min + random.random() * f_add


class ParticleSystem(object):
    # shader sources
    vertex_update_source = """
#version 330 core

layout (location = 0) in vec3 vPosition;
layout (location = 1) in vec3 vVelocity;
layout (location = 2) in vec3 vColor;
layout (location = 3) in float fLifeTime;
layout (location = 4) in float fSize;
//layout (location = 5) in int iType;
layout (location = 5) in float iType;

out vec3 vPositionPass;
out vec3 vVelocityPass;
out vec3 vColorPass;
out float fLifeTimePass;
out float fSizePass;
//out int iTypePass;
out float iTypePass;

void main()
{
  vPositionPass = vPosition;
  vVelocityPass = vVelocity;
  vColorPass = vColor;
  fLifeTimePass = fLifeTime;
  fSizePass = fSize;
  iTypePass = iType;
}

"""

    geometry_update_source = """
#version 330 core

layout(points) in;
layout(points) out;
layout(max_vertices = 200) out;

// All that we get from vertex shader

in vec3 vPositionPass[];
in vec3 vVelocityPass[];
in vec3 vColorPass[];
in float fLifeTimePass[];
in float fSizePass[];
//in int iTypePass[];
in float iTypePass[];

// All that we send further

out vec3 vPositionOut;
out vec3 vVelocityOut;
out vec3 vColorOut;
out float fLifeTimeOut;
out float fSizeOut;
//out int iTypeOut;
out float iTypeOut;

uniform vec3 vGenPosition; // Position where new particles are spawned
uniform vec3 vGenGravityVector; // Gravity vector for particles - updates velocity of particles
uniform vec3 vGenVelocityMin; // Velocity of new particle - from min to (min+range)
uniform vec3 vGenVelocityRange;

uniform vec3 vGenColor;
uniform float fGenSize;

uniform float fGenLifeMin, fGenLifeRange; // Life of new particle - from min to (min+range)
uniform float fTimePassed; // Time passed since last frame

uniform vec3 vRandomSeed; // Seed number for our random number function

uniform int iNumToGenerate; // How many particles will be generated next time, if greater than zero, particles are generated

vec3 vLocalSeed;

// This function returns random number from zero to one
float randZeroOne()
{
    //return 1.0;
    uint n = floatBitsToUint(vLocalSeed.y * 214013.0 + vLocalSeed.x * 2531011.0 + vLocalSeed.z * 141251.0);
    n = n * (n * n * 15731u + 789221u);
    n = (n >> 9u) | 0x3F800000u;

    float fRes =  2.0 - uintBitsToFloat(n);
    vLocalSeed = vec3(vLocalSeed.x + 147158.0 * fRes, vLocalSeed.y*fRes  + 415161.0 * fRes, vLocalSeed.z + 324154.0*fRes);
    return fRes;
}

void main()
{
  vLocalSeed = vRandomSeed;

  // gl_Position doesn't matter now, as rendering is discarded, so I don't set it at all

  vPositionOut = vPositionPass[0];
  vVelocityOut = vVelocityPass[0];
  vColorOut = vColorPass[0];
  fLifeTimeOut = fLifeTimePass[0] - fTimePassed;
  fSizeOut = fSizePass[0];
  iTypeOut = iTypePass[0];


  if(fLifeTimeOut > 0.0)
  {
      if(iTypeOut > 0)
      {
          if(fLifeTimeOut > 0.0)
          {
              vPositionOut += vVelocityOut * fTimePassed;
              vVelocityOut += vGenGravityVector * fTimePassed;
              iTypeOut = 1.0;
              EmitVertex();
              EndPrimitive();
          }
      }
      else if(iTypeOut <= 0)
      {
        // emit particle (its the generator particle), assuming all vars to have default values?
        EmitVertex();
        EndPrimitive();

        // emit additional particles
        for(int i = 0; i < iNumToGenerate; i++)
        {
          vPositionOut += vGenPosition;
          vVelocityOut = vGenVelocityMin+vec3(vGenVelocityRange.x * randZeroOne(), vGenVelocityRange.y * randZeroOne(), vGenVelocityRange.z * randZeroOne());
          vColorOut = vGenColor * vColorPass[0];
          fLifeTimeOut = fGenLifeMin + fGenLifeRange * randZeroOne();
          fSizeOut *= fGenSize;
          iTypeOut = 1.0;
          EmitVertex();
          EndPrimitive();
        }
      }
  }
}

"""

    fragment_update_source = """
#version 330 core

out vec4 vColor;

void main()
{
    //vColor = vec4(1.0, 1.0, 1.0, 1.0);
}

"""

    vertex_render_source = """
#version 330 core

layout (location = 0) in vec3 vPosition;
layout (location = 2) in vec3 vColor;
layout (location = 3) in float fLifeTime;
layout (location = 4) in float fSize;
layout (location = 5) in int iType;

out vec3 vColorPass;
out float fLifeTimePass;
out float fSizePass;
out int iTypePass;

void main()
{
   gl_Position = vec4(vPosition, 1.0);
   vColorPass = vColor;
   fSizePass = fSize;
   fLifeTimePass = fLifeTime;
   iTypePass = iType;
}

    """

    geometry_render_source = """
    #version 330 core

    uniform struct Matrices
    {
        mat4 mProj;
        mat4 mView;
    } matrices;

    uniform vec3 vQuad1, vQuad2;

    layout(points) in;
    layout(triangle_strip) out;
    layout(max_vertices = 4) out;

    in vec3 vColorPass[];
    in float fLifeTimePass[];
    in float fSizePass[];
    in int iTypePass[];

    smooth out vec2 vTexCoord;
    flat out vec4 vColorPart;

    void main()
    {
      if(iTypePass[0] != 0)
      {
        vec3 vPosOld = gl_in[0].gl_Position.xyz;
        float fSize = fSizePass[0];
        mat4 mVP = matrices.mProj*matrices.mView;

        vColorPart = vec4(vColorPass[0], fLifeTimePass[0]);
        vec3 vQ1 = vQuad1;
        vec3 vQ2 = vQuad2;

        vec3 vPos = vPosOld+(-vQ1-vQ2)*fSize;
        vTexCoord = vec2(0.0, 0.0);
        gl_Position = mVP*vec4(vPos, 1.0);
        EmitVertex();

        vPos = vPosOld+(-vQ1+vQ2)*fSize;
        vTexCoord = vec2(0.0, 1.0);
        gl_Position = mVP*vec4(vPos, 1.0);
        EmitVertex();

        vPos = vPosOld+(vQ1-vQ2)*fSize;
        vTexCoord = vec2(1.0, 0.0);
        gl_Position = mVP*vec4(vPos, 1.0);
        EmitVertex();

        vPos = vPosOld+(vQ1+vQ2)*fSize;
        vTexCoord = vec2(1.0, 1.0);
        gl_Position = mVP*vec4(vPos, 1.0);
        EmitVertex();

        EndPrimitive();
      }
    }

    """

    fragment_render_source = """
#version 330 core

uniform sampler2D gSampler;

smooth in vec2 vTexCoord;
flat in vec4 vColorPart;

out vec4 FragColor;

void main()
{
  vec4 vTexColor = texture2D(gSampler, vTexCoord);
  FragColor = vec4(vTexColor.xyz, 1.0) * vColorPart;
  //FragColor = vec4(1.0, 0.0, 0.0, 1.0) * vColorPart;
}

"""

    NUM_PARTICLE_ATTRIBUTES = 6
    MAX_PARTICLES_ON_SCENE = 100000  # if too low then new particles will start to overwrite existing ones
    PARTICLE_TYPE_GENERATOR = -1.0
    PARTICLE_TYPE_NORMAL = 1.0

    def __init__(self, v3_position):
        self._initialized = False
        self.transform_feedback_buffer = 0
        self.ui_particle_buffers = [0, 0]
        self.ui_vaos = [0, 0]
        self.ui_query = 0
        self.ui_texture = 0

        self._cur_buf_idx = 0
        self.num_particles = 1

        self.mat4_projection = None
        self.mat4_view = None

        self.v3_quad1 = None
        self.v3_quad2 = None

        self.elapsed_time = 0

        self.r_color = 0.5
        self.g_color = 0.5
        self.b_color = 0.5
        self._color_change_time = 0

        # tuning values
        self.next_generation_time = 0.001
        self.v3_gen_position = v3_position
        self.v3_vel_min = [-5.0, 0.0, -5.0]
        self.v3_vel_range = [10.0, 10.0, 10.0]
        self.v3_vel_range = [5.0, 10.0, 5.0]
        # self.v3_gravity_vector = [0.0, -9.81, 0.0]  # realistic value
        self.v3_gravity_vector = [0.0, -1.0, 0.0]  # this is a nicer effect
        self.v3_gen_color = [1.0, 1.0, 1.0]
        self.gen_life_min = 0.5
        self.gen_life_range = 20.0
        self.gen_size = 0.5
        self.num_to_generate = 400

        # shaders
        self.shader_vertex_render = None
        self.shader_geom_render = None
        self.shader_frag_render = None
        self.shader_vertex_update = None
        self.shader_geom_update = None
        self.shader_frag_update = None

        # updating
        self.update_shader_program = None
        self.render_shader_program = None

    def initialize(self):
        if self._initialized:
            return False

        varyings = [
            "vPositionOut",
            "vVelocityOut",
            "vColorOut",
            "fLifeTimeOut",
            "fSizeOut",
            "iTypeOut",
        ]

        # update shader
        self.shader_vertex_update = shaders.compileShader(self.vertex_update_source, GL_VERTEX_SHADER)
        self.shader_geom_update = shaders.compileShader(self.geometry_update_source, GL_GEOMETRY_SHADER)
        self.shader_frag_update = shaders.compileShader(self.fragment_update_source, GL_FRAGMENT_SHADER)
        self.update_shader_program = shaders.compileProgram(self.shader_vertex_update, self.shader_geom_update,
                                                            self.shader_frag_update)

        c_var = get_char_ptr_ptr(varyings)
        glTransformFeedbackVaryings(self.update_shader_program, len(varyings), c_var, GL_INTERLEAVED_ATTRIBS)

        # relink because of varyings
        glLinkProgram(self.update_shader_program)
        self.update_shader_program.check_validate()
        self.update_shader_program.check_linked()

        # render shader
        self.shader_vertex_render = shaders.compileShader(self.vertex_render_source, GL_VERTEX_SHADER)
        self.shader_geom_render = shaders.compileShader(self.geometry_render_source, GL_GEOMETRY_SHADER)
        self.shader_frag_render = shaders.compileShader(self.fragment_render_source, GL_FRAGMENT_SHADER)
        self.render_shader_program = shaders.compileProgram(self.shader_vertex_render, self.shader_geom_render,
                                                            self.shader_frag_render)
        self.render_shader_program.check_validate()
        self.render_shader_program.check_linked()

        # buffers for transform feedback
        self.transform_feedback_buffer = glGenTransformFeedbacks(1)
        self.ui_query = glGenQueries(1)
        self.ui_vaos = glGenVertexArrays(2)
        self.ui_particle_buffers = glGenBuffers(2)

        for idx in range(len(self.ui_vaos)):
            glBindVertexArray(self.ui_vaos[idx])
            glBindBuffer(GL_ARRAY_BUFFER, self.ui_particle_buffers[idx])

            particle = [0.0, 0.0, 0.0,  # pos
                        0.0, 0.0, 0.0,  # vel
                        1.0, 0.0, 0.0,  # col
                        1000.0,  # lifetime
                        1.0,  # size
                        self.PARTICLE_TYPE_GENERATOR,  # particle type == 0
                        ]

            data_ptr = get_ptr(particle)
            size_of_particle = sizeof(data_ptr)
            glBufferData(GL_ARRAY_BUFFER, size_of_particle * self.MAX_PARTICLES_ON_SCENE, NULL, GL_DYNAMIC_DRAW)

            glBufferSubData(GL_ARRAY_BUFFER, 0 * size_of_particle, data_ptr)

            # # second particle
            # particle = [-25.0, 0.0, 25.0,  # pos
            #             0.0, 0.0, 0.0,  # vel
            #             0.0, 1.0, 0.0,  # col
            #             2000.0,  # lifetime
            #             3.5,  # size
            #             self.PARTICLE_TYPE_GENERATOR,  # particle type == 0
            #             ]
            # data_ptr = get_ptr(particle)
            # size_of_particle = sizeof(data_ptr)
            # glBufferSubData(GL_ARRAY_BUFFER, 1 * size_of_particle, data_ptr)

            # other attributes
            for j in range(self.NUM_PARTICLE_ATTRIBUTES):
                glEnableVertexAttribArray(j)

            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, size_of_particle, get_offset_from_index(0))  # position
            glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, size_of_particle, get_offset_from_index(3))  # velocity
            glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, size_of_particle, get_offset_from_index(6))  # color
            glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, size_of_particle, get_offset_from_index(9))  # lifetime
            glVertexAttribPointer(4, 1, GL_FLOAT, GL_FALSE, size_of_particle, get_offset_from_index(10))  # size
            glVertexAttribPointer(5, 1, GL_FLOAT, GL_FALSE, size_of_particle, get_offset_from_index(11))  # type

        self._cur_buf_idx = 0
        self.num_particles = 2

        self._initialized = True

        return True

    def update_colors(self, delta_time):
        self._color_change_time += delta_time
        duration = 0.1
        if self._color_change_time > duration:
            self._color_change_time -= duration
        else:
            return

        # for idx in range(len(self.ui_vaos)):
        glBindVertexArray(self.ui_vaos[self._cur_buf_idx])
        glBindBuffer(GL_ARRAY_BUFFER, self.ui_particle_buffers[self._cur_buf_idx])

        self.r_color = (self.r_color * 100 + 2) % 100 / 100.0
        if self.r_color <= 0.2:
            self.g_color = (self.g_color * 100 + 3) % 100 / 100.0
        if self.g_color <= 0.3:
            self.b_color = (self.b_color * 100 + 5) % 100 / 100.0

        for idx in range(1):

            particle = [0.0, 0.0, 0.0,  # pos
                        0.0, 0.0, 0.0,  # vel
                        self.r_color, self.g_color, self.b_color,  # col
                        1000.0,  # lifetime
                        1.0,  # size
                        self.PARTICLE_TYPE_GENERATOR,  # particle type == 0
                        ]

            data_ptr = get_ptr(particle)
            size_of_particle = sizeof(data_ptr)
            # glBufferData(GL_ARRAY_BUFFER, size_of_particle * self.MAX_PARTICLES_ON_SCENE, NULL, GL_DYNAMIC_DRAW)

            glBufferSubData(GL_ARRAY_BUFFER, idx * size_of_particle, data_ptr)

        glBindVertexArray(0)

        # print("changed color to ", self.r_color, self.g_color, self.b_color)

    def update(self, delta_time):
        if not self._initialized:
            return

        glUseProgram(self.update_shader_program)

        u_loc = glGetUniformLocation(self.update_shader_program, "fTimePassed")
        glUniform1f(u_loc, delta_time)

        u_loc = glGetUniformLocation(self.update_shader_program, "vGenPosition")
        glUniform3fv(u_loc, 1, self.v3_gen_position)

        u_loc = glGetUniformLocation(self.update_shader_program, "vGenVelocityMin")
        glUniform3fv(u_loc, 1, self.v3_vel_min)

        u_loc = glGetUniformLocation(self.update_shader_program, "vGenVelocityRange")
        glUniform3fv(u_loc, 1, self.v3_vel_range)

        u_loc = glGetUniformLocation(self.update_shader_program, "vGenColor")
        glUniform3fv(u_loc, 1, self.v3_gen_color)

        u_loc = glGetUniformLocation(self.update_shader_program, "vGenGravityVector")
        glUniform3fv(u_loc, 1, self.v3_gravity_vector)

        u_loc = glGetUniformLocation(self.update_shader_program, "fGenLifeMin")
        glUniform1f(u_loc, self.gen_life_min)

        u_loc = glGetUniformLocation(self.update_shader_program, "fGenLifeRange")
        glUniform1f(u_loc, self.gen_life_range)

        u_loc = glGetUniformLocation(self.update_shader_program, "fGenSize")
        glUniform1f(u_loc, self.gen_size)

        self.elapsed_time += delta_time

        if self.elapsed_time > self.next_generation_time:

            actual_num_to_generate = self.MAX_PARTICLES_ON_SCENE - self.num_particles
            actual_num_to_generate = self.num_to_generate if self.num_to_generate < actual_num_to_generate else actual_num_to_generate
            # actual_num_to_generate = 0 if actual_num_to_generate < 0 else actual_num_to_generate  # sanity check, not needed if self.num_particles is guaranteed < MAX_PARTICLES_ON_SCENE
            # actual_num_to_generate //= 2
            # print("!! generating ", actual_num_to_generate)

            u_loc_num_to_gen = glGetUniformLocation(self.update_shader_program, "iNumToGenerate")
            glUniform1i(u_loc_num_to_gen, actual_num_to_generate)

            self.elapsed_time -= self.next_generation_time

            random_seed = [g_rand_f(-10.0, 20.0), g_rand_f(-10.0, 20.0), g_rand_f(-10.0, 20.0)]

            u_loc = glGetUniformLocation(self.update_shader_program, "vRandomSeed")
            glUniform3fv(u_loc, 1, random_seed)
        else:

            u_loc_num_to_gen = glGetUniformLocation(self.update_shader_program, "iNumToGenerate")
            glUniform1i(u_loc_num_to_gen, 0)

            u_loc = glGetUniformLocation(self.update_shader_program, "vRandomSeed")
            glUniform3fv(u_loc, 1, [0.0, 0.0, 0.0])

        glEnable(GL_RASTERIZER_DISCARD)
        glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, self.transform_feedback_buffer)
        glBindVertexArray(self.ui_vaos[self._cur_buf_idx])
        glEnableVertexAttribArray(1)  # re-enable velocity

        floats_per_particle = 12
        size_of_float = sizeof(GLfloat)

        glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, self.ui_particle_buffers[1 - self._cur_buf_idx])
        glBindBufferRange(GL_TRANSFORM_FEEDBACK_BUFFER, 0, self.ui_particle_buffers[1 - self._cur_buf_idx], 0, self.MAX_PARTICLES_ON_SCENE * floats_per_particle * size_of_float)

        glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, self.ui_query)
        glBeginTransformFeedback(GL_POINTS)

        # glDrawArrays(GL_POINTS, 0, self.num_particles)
        glDrawArrays(GL_POINTS, 0, self.MAX_PARTICLES_ON_SCENE)

        glEndTransformFeedback()

        glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN)

        self.num_particles = glGetQueryObjectuiv(self.ui_query, GL_QUERY_RESULT)
        # self.num_particles = do_timing(glGetQueryObjectuiv, self.ui_query, GL_QUERY_RESULT)

        self._cur_buf_idx = 1 - self._cur_buf_idx
        glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, 0)

        glBindVertexArray(0)  # prevent that this vao ist changed outside

        self.update_colors(delta_time)
        # print("! num particles: ", self.num_particles)

        # glBindVertexArray(self.ui_vaos[self._cur_buf_idx])
        # raw_uint8_data = glGetBufferSubData(GL_ARRAY_BUFFER, 0, 48 * ParticleSystem.MAX_PARTICLES_ON_SCENE)
        # data = raw_uint8_data.view('<f4')
        # _generators = [_idx for _idx, _d in enumerate(data) if (_idx + 1) % 12 == 0 and _d == self.PARTICLE_TYPE_GENERATOR]
        # for _idx in _generators:
        #     print("!!!!", self._cur_buf_idx, _idx, ", ".join(["{0:.2f}".format(_x) for _x in data[_idx - 11: _idx + 1]]))
        # print("!!!!---------------------------")


        # glBindVertexArray(0)


    def draw(self):
        if not self._initialized:
            return

        glEnable(GL_BLEND)
        glBlendFunc(GL_SRC_ALPHA, GL_ONE)
        glDepthMask(0)

        glDisable(GL_RASTERIZER_DISCARD)
        glUseProgram(self.render_shader_program)

        u_loc = glGetUniformLocation(self.render_shader_program, "matrices.mProj")
        glUniformMatrix4fv(u_loc, 1, GL_FALSE, self.mat4_projection)

        u_loc = glGetUniformLocation(self.render_shader_program, "matrices.mView")
        glUniformMatrix4fv(u_loc, 1, GL_FALSE, self.mat4_view)

        u_loc = glGetUniformLocation(self.render_shader_program, "vQuad1")
        glUniform3fv(u_loc, 1, self.v3_quad1)

        u_loc = glGetUniformLocation(self.render_shader_program, "vQuad2")
        glUniform3fv(u_loc, 1, self.v3_quad2)

        u_loc = glGetUniformLocation(self.render_shader_program, "gSampler")
        glUniform1i(u_loc, 0)  # using texture unit 0 because of GL_TEXTURE0 usage

        glBindVertexArray(self.ui_vaos[self._cur_buf_idx])
        glDisableVertexAttribArray(1)

        glDrawArrays(GL_POINTS, 0, self.num_particles)

        glDepthMask(1)
        glDisable(GL_BLEND)
        glBindVertexArray(0)  # prevent that vao is changed outside

    def set_matrices(self, projection, eye3, view3, up3):
        self.mat4_projection = projection
        self.mat4_view = look_at(eye3, view3, up3)
        view3 = subv3(view3, eye3)
        view3 = normalize3(view3)
        self.v3_quad1 = cross3(view3, up3)
        self.v3_quad1 = normalize3(self.v3_quad1)
        self.v3_quad2 = cross3(view3, self.v3_quad1)
        self.v3_quad2 = normalize3(self.v3_quad2)


def do_timing(func, *args):
    start = time.time()
    result = func(*args)
    end = time.time()
    duration = end - start
    print("duration {0}: {1}s".format(func.__name__, duration))
    return result

def main():
    """
    Main method. In this simple program it contains all the code.

    :raise RuntimeError:
    """

    # init window and context
    screen_width = 800
    screen_height = 600

    pygame.init()
    os.environ['SDL_VIDEO_CENTERED'] = '1'

    pygame.display.set_caption("OpenGL - particles")

    pygame.display.set_mode((screen_width, screen_height), pygame.OPENGL | pygame.DOUBLEBUF)
    glViewport(0, 0, screen_width, screen_height)

    # texture loading (texture uniform)
    tex_id = glGenTextures(1)

    img = pygame.image.load("particle.bmp")
    img = pygame.transform.flip(img, False, True)  # invert
    texture_data = pygame.image.tostring(img, "RGB", 1)  # get image data as array
    width, height = img.get_size()

    glActiveTexture(GL_TEXTURE0)  # texture bind location for texture unit 0
    glBindTexture(GL_TEXTURE_2D, tex_id)
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, texture_data)

    # defines how the texture is sampled
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)

    pss = []
    for i in range(1):
        ps = ParticleSystem([i * 20.0, 0.0, 0.0])
        ps.initialize()
        pss.append(ps)

    projection = perspective(45, screen_width / (1.0 * screen_height), 0.5, 1000.0)
    eye3 = [-71.41778489984125, 0.0, -71.4177848998413]  # this is just a nicer view
    view3 = [0.0, 0.0, 0.0]
    up3 = [0.0, 1.0, 0.0]

    glViewport(0, 0, screen_width, screen_height)

    # main loop
    clock = pygame.time.Clock()
    running = True
    angle_change = 45.0 / 4.0
    step_forward = 5.0
    print_fps = True
    frame_count = 0

    while running:
        dt_in_ms = clock.tick(60)
        dt_in_s = dt_in_ms / 1000.0
        frame_count += 1
        _angle_step = 0
        _step_forward = 0
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
                elif event.key == pygame.K_RIGHT:
                    _angle_step = angle_change
                elif event.key == pygame.K_LEFT:
                    _angle_step = -angle_change
                elif event.key == pygame.K_UP:
                    _step_forward = step_forward
                elif event.key == pygame.K_DOWN:
                    _step_forward = -step_forward
                elif event.key == pygame.K_F1:
                    print_fps = not print_fps

        if _angle_step != 0:
            rad_alpha = radians(_angle_step)
            c = cos(rad_alpha)
            s = sin(rad_alpha)
            if pygame.key.get_mods() & pygame.KMOD_SHIFT:
                # rotate around where the cam stands
                v3_dir = subv3(view3, eye3)
                view3 = [
                    eye3[0] + c * v3_dir[0] - s * v3_dir[2],
                    eye3[1] + v3_dir[1],
                    eye3[2] + s * v3_dir[0] + c * v3_dir[2]
                ]
            else:
                # rotate around the point were it is looked at
                eye3 = [
                    c * eye3[0] - s * eye3[2],
                    eye3[1],
                    s * eye3[0] + c * eye3[2]
                ]
            print("looking at", view3, "from ", eye3)

            ps.set_matrices(projection, eye3, view3, up3)
            print("quad1", ps.v3_quad1, "quad2", ps.v3_quad2, "pos", ps.v3_gen_position)

        if _step_forward != 0:
            v3_dir = subv3(view3, eye3)
            v3_dir = normalize3(v3_dir)
            eye3 = [
                eye3[0] + v3_dir[0] * _step_forward,
                eye3[1] + v3_dir[1] * _step_forward,
                eye3[2] + v3_dir[2] * _step_forward
                     ]
            print("moved to: ", eye3)

        # render frame
        # glClearColor(1.0, 1.0, 0.0, 1.0)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        for ps in pss:
            ps.set_matrices(projection, eye3, view3, up3)
            # ps.update(dt_in_s)
            do_timing(ps.update, dt_in_s)
            do_timing(ps.draw)
        pygame.display.flip()

        if print_fps:
            print("---", dt_in_s, 1.0/dt_in_s, "#particles:", ps.num_particles, "frame:", frame_count)

        # debug help
        if frame_count > 20:
            # break
            pass


if __name__ == '__main__':
    main()
