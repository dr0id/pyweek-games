# -*- coding: utf-8 -*-
"""
A 'modern' OpenGL example using pygame and pyopengl

Based on

https://open.gl/geometry
"""
from __future__ import print_function
import glpy

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 

import os

from ctypes import c_float, c_void_p, sizeof

import pygame

from OpenGL.GL import glGenBuffers, glBindBuffer, GL_ARRAY_BUFFER, glBufferData, GL_STATIC_DRAW, glCreateShader, \
    GL_VERTEX_SHADER, glShaderSource, glCompileShader, glGetShaderiv, GL_COMPILE_STATUS, GL_TRUE, glGetShaderInfoLog, \
    GL_FRAGMENT_SHADER, glCreateProgram, glAttachShader, glBindFragDataLocation, glLinkProgram, \
    glUseProgram, GL_FLOAT, glGetAttribLocation, glVertexAttribPointer, glEnableVertexAttribArray, \
    glGenVertexArrays, glBindVertexArray, glClearColor, \
    glClear, glDeleteProgram, glDeleteShader, glDeleteBuffers, \
    glDeleteVertexArrays, GL_FALSE, GL_COLOR_BUFFER_BIT, glDrawArrays, GL_LINK_STATUS, glGetProgramiv, glGetProgramInfoLog, \
    GL_POINTS, GL_GEOMETRY_SHADER


# enable error when array data is copied
from OpenGL.arrays import numpymodule

numpymodule.NumpyHandler.ERROR_ON_COPY = True

import glmath

SIZE_OF_FLOAT = sizeof(c_float)
SIZE_OF_GL_FLOAT = sizeof(c_float)

# shader sources

vertex_source = """
#version 150 core

in vec2 pos;
void main() {
    gl_Position = vec4(pos, 0.0, 1.0);
};

"""

fragment_source = """
#version 150 core

out vec4 outColor;

void main() {
    outColor = vec4(1.0, 0.0, 0.0, 1.0);
};

"""

geometry_source = """
#version 150 core

layout(points) in;
layout(points, max_vertices = 1) out;

void main() {
    gl_Position = gl_in[0].gl_Position;
    EmitVertex();
    EndPrimitive();
}
"""


def _create_and_compile_shader(shader_type, shader_source):
    shader = glCreateShader(shader_type)
    glShaderSource(shader, shader_source)
    glCompileShader(shader)

    if glGetShaderiv(shader, GL_COMPILE_STATUS) != GL_TRUE:
        raise RuntimeError(glGetShaderInfoLog(shader))

    return shader


def create_shader_program(vertex_source, geometry_source, fragment_source):
    # create and compile the geometry shader
    geometry_shader = _create_and_compile_shader(GL_GEOMETRY_SHADER, geometry_source)

    # create and compile the vertex shader
    vertex_shader = _create_and_compile_shader(GL_VERTEX_SHADER, vertex_source)

    # create and compile the fragment shader
    fragment_shader = _create_and_compile_shader(GL_FRAGMENT_SHADER, fragment_source)

    # link the vertex and fragment shader into a shader program
    shader_program = glCreateProgram()
    glAttachShader(shader_program, vertex_shader)
    glAttachShader(shader_program, geometry_shader)
    glAttachShader(shader_program, fragment_shader)
    glBindFragDataLocation(shader_program, 0, "outColor")
    glLinkProgram(shader_program)

    if glGetProgramiv(shader_program, GL_LINK_STATUS) != GL_TRUE:
        raise RuntimeError(glGetProgramInfoLog(shader_program))

    return vertex_shader, fragment_shader, shader_program


def main():
    """
    Main method. In this simple program it contains all the code.

    :raise RuntimeError:
    """

    # init window and context
    screen_width = 800
    screen_height = 600

    pygame.init()
    os.environ['SDL_VIDEO_CENTERED'] = '1'

    pygame.display.gl_set_attribute(pygame.GL_STENCIL_SIZE, 8)
    pygame.display.gl_set_attribute(pygame.GL_DEPTH_SIZE, 24)
    # pygame.display.gl_set_attribute(pygame.GL_MULTISAMPLEBUFFERS, 1)
    # pygame.display.gl_set_attribute(pygame.GL_MULTISAMPLESAMPLES, 2)
    # pygame.display.gl_set_attribute(pygame.GL_SWAP_CONTROL, 0)

    pygame.display.set_caption("OpenGL - geometry shaders")

    pygame.display.set_mode((screen_width, screen_height), pygame.OPENGL | pygame.DOUBLEBUF)
    # pygame.display.set_mode((screen_width, screen_height), pygame.OPENGL)

    # these would be nice to configure in the openGL context, but I don't know how (if its
    # even possible using pygame)
    #
    # SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    # SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    # SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);

    vertex_shader, fragment_shader, shader_program = create_shader_program(vertex_source, geometry_source,
                                                                           fragment_source)
    glUseProgram(shader_program)

    vbo = glGenBuffers(1)
    points = [
        -0.45, 0.45,
        0.45, 0.45,
        0.45, -0.45,
        -0.45, -0.45,
    ]

    # create vbo
    glBindBuffer(GL_ARRAY_BUFFER, vbo)
    glBufferData(GL_ARRAY_BUFFER, glpy.get_float_ptr(points), GL_STATIC_DRAW)

    # create vao
    vao = glGenVertexArrays(1)
    glBindVertexArray(vao)

    # specify layout of point data
    pos_attrib = glGetAttribLocation(shader_program, "pos")
    glEnableVertexAttribArray(pos_attrib)
    glVertexAttribPointer(pos_attrib, 2, GL_FLOAT, GL_FALSE, 0, c_void_p(0))

    # main loop
    clock = pygame.time.Clock()
    running = True

    while running:
        clock.tick(60)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False

        # render frame
        glClearColor(0.0, 0.0, 0.0, 0.0)
        glClear(GL_COLOR_BUFFER_BIT)

        glDrawArrays(GL_POINTS, 0, 4)

        pygame.display.flip()

    # delete
    glDeleteProgram(shader_program)
    glDeleteShader(fragment_shader)
    glDeleteShader(vertex_shader)

    glDeleteVertexArrays(1, [vao])
    glDeleteBuffers(1, [vbo])


if __name__ == '__main__':
    main()
