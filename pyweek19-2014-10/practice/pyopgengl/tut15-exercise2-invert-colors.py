# -*- coding: utf-8 -*-
"""
A 'modern' OpenGL example using pygame and pyopengl

Based on

https://open.gl/drawing
"""
from __future__ import print_function

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 

import os
import pygame

from OpenGL.GL import glGenBuffers, glBindBuffer, GL_ARRAY_BUFFER, glBufferData, GL_STATIC_DRAW, glCreateShader, \
    GL_VERTEX_SHADER, glShaderSource, glCompileShader, glGetShaderiv, GL_COMPILE_STATUS, GL_TRUE, glGetShaderInfoLog, \
    GL_FRAGMENT_SHADER, glCreateProgram, glAttachShader, glBindFragDataLocation, glLinkProgram, \
    glUseProgram, GL_FLOAT, glGetAttribLocation, glVertexAttribPointer, glEnableVertexAttribArray, \
    glGenVertexArrays, glBindVertexArray, glClearColor, \
    glClear, glDeleteProgram, glDeleteShader, glDeleteBuffers, \
    glDeleteVertexArrays, GL_FALSE, glDrawArrays, GL_TRIANGLES, GL_COLOR_BUFFER_BIT, GL_DEPTH_BUFFER_BIT

# enable error when array data is copied
from OpenGL.arrays import numpymodule

numpymodule.NumpyHandler.ERROR_ON_COPY = True

from ctypes import c_float, c_void_p, sizeof


def main():
    """
    Main method. In this simple program it contains all the code.

    :raise RuntimeError:
    """
    width = 800
    height = 600

    pygame.init()
    os.environ['SDL_VIDEO_CENTERED'] = '1'
    pygame.display.set_mode((width, height), pygame.OPENGL | pygame.DOUBLEBUF)

    # these would be nice to configure in the openGL context, but I don't know how (if its
    # even possible using pygame)
    #
    # SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    # SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    # SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);

    # crating Vertex Array Object so save shader attributes
    vao = glGenVertexArrays(1)
    glBindVertexArray(vao)

    # vertices
    vertices = [0.0, 0.5, 1.0, 0.0, 0.0,
                0.5, -0.5, 0.0, 1.0, 0.0,
                -0.5, -0.5, 0.0, 0.0, 1.0
                ]

    vbo = glGenBuffers(1)
    glBindBuffer(GL_ARRAY_BUFFER, vbo)
    # noinspection PyCallingNonCallable
    vertices_buffer = (c_float * len(vertices))(*vertices)
    glBufferData(GL_ARRAY_BUFFER, vertices_buffer, GL_STATIC_DRAW)

    # shader sources

    vertex_source = """
#version 150

in vec2 position;
in vec3 color;

out vec3 Color;

void main()
{
    Color = color;
    gl_Position = vec4(position, 0.0, 1.0);
}
"""

    fragment_source = """
#version 150

in vec3 Color;

out vec4 outColor;

void main()
{
    outColor = vec4(1.0 - Color, 1.0);
}
"""
    vertex_shader = glCreateShader(GL_VERTEX_SHADER)
    glShaderSource(vertex_shader, vertex_source)
    glCompileShader(vertex_shader)
    if glGetShaderiv(vertex_shader, GL_COMPILE_STATUS) != GL_TRUE:
        raise RuntimeError(glGetShaderInfoLog(vertex_shader))

    fragment_shader = glCreateShader(GL_FRAGMENT_SHADER)
    glShaderSource(fragment_shader, fragment_source)
    glCompileShader(fragment_shader)
    if glGetShaderiv(fragment_shader, GL_COMPILE_STATUS) != GL_TRUE:
        raise RuntimeError(glGetShaderInfoLog(fragment_shader))

    # setting up shader program
    shader_program = glCreateProgram()
    glAttachShader(shader_program, vertex_shader)
    glAttachShader(shader_program, fragment_shader)

    # define which output is written to which buffer from the fragment shader
    glBindFragDataLocation(shader_program, 0, "outColor")

    glLinkProgram(shader_program)
    glUseProgram(shader_program)

    # link between vertex data and attributes
    pos_attrib = glGetAttribLocation(shader_program, "position")
    glEnableVertexAttribArray(pos_attrib)
    glVertexAttribPointer(pos_attrib, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(c_float), c_void_p(0))

    color_attrib = glGetAttribLocation(shader_program, "color")
    glEnableVertexAttribArray(color_attrib)
    glVertexAttribPointer(color_attrib, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(c_float), c_void_p(2 * sizeof(c_float)))

    # main loop
    clock = pygame.time.Clock()

    running = True
    while running:
        clock.tick()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False

        # Clear the screen to black
        glClearColor(0, 0, 0, 1)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        # active shader program
        glUseProgram(shader_program)

        try:
            glBindVertexArray(vao)

            # draw triangle
            glDrawArrays(GL_TRIANGLES, 0, 3)
        finally:
            glBindVertexArray(0)
            glUseProgram(0)

        pygame.display.flip()

    glDeleteProgram(shader_program)
    glDeleteShader(fragment_shader)
    glDeleteShader(vertex_shader)

    glDeleteBuffers(1, [vbo])

    glDeleteVertexArrays(1, [vao])


if __name__ == '__main__':
    main()
