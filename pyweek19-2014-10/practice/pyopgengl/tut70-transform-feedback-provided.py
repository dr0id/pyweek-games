# -*- coding: utf-8 -*-
"""
A 'modern' OpenGL example using pygame and pyopengl

Based on

https://open.gl/feedback
"""
from __future__ import print_function

import ctypes
from OpenGL.raw.GL._types import GLfloat, GLvoidp, GLchar, sizeof
from OpenGL.GL import shaders

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 

import os

from ctypes import c_float, c_void_p

import pygame

from OpenGL.GL import glGenBuffers, glBindBuffer, GL_ARRAY_BUFFER, glBufferData, GL_STATIC_DRAW, glCreateShader, \
    GL_VERTEX_SHADER, glShaderSource, glCompileShader, glGetShaderiv, GL_COMPILE_STATUS, GL_TRUE, glGetShaderInfoLog, \
    GL_FRAGMENT_SHADER, glCreateProgram, glAttachShader, glBindFragDataLocation, glLinkProgram, \
    glUseProgram, GL_FLOAT, glGetAttribLocation, glVertexAttribPointer, glEnableVertexAttribArray, \
    glGenVertexArrays, glBindVertexArray, glClearColor, \
    glClear, glDeleteProgram, glDeleteShader, glDeleteBuffers, \
    glDeleteVertexArrays, GL_FALSE, GL_COLOR_BUFFER_BIT, glDrawArrays, GL_LINK_STATUS, glGetProgramiv, \
    glGetProgramInfoLog, \
    GL_POINTS, GL_GEOMETRY_SHADER, glTransformFeedbackVaryings, GL_INTERLEAVED_ATTRIBS, GL_STATIC_READ, glEnable, \
    GL_RASTERIZER_DISCARD, glBindBufferBase, GL_TRANSFORM_FEEDBACK_BUFFER, glBeginTransformFeedback, glDrawArrays, \
    glEndTransformFeedback, glFlush, glGetBufferSubData, glDisable


# enable error when array data is copied
from OpenGL.arrays import numpymodule

numpymodule.NumpyHandler.ERROR_ON_COPY = True

import glmath

SIZE_OF_FLOAT = sizeof(c_float)
SIZE_OF_GL_FLOAT = sizeof(c_float)

# shader sources

vertex_shader = """
#version 150 core

in float inValue;

out float outValue;

void main()
{
    outValue = sqrt(inValue);
}
"""

geometry_source = """
#version 150 core


"""


def main():
    """
    Main method. In this simple program it contains all the code.

    :raise RuntimeError:
    """

    # init window and context
    screen_width = 800
    screen_height = 600

    pygame.init()
    os.environ['SDL_VIDEO_CENTERED'] = '1'

    pygame.display.gl_set_attribute(pygame.GL_STENCIL_SIZE, 8)
    pygame.display.gl_set_attribute(pygame.GL_DEPTH_SIZE, 24)
    # pygame.display.gl_set_attribute(pygame.GL_MULTISAMPLEBUFFERS, 1)
    # pygame.display.gl_set_attribute(pygame.GL_MULTISAMPLESAMPLES, 2)
    # pygame.display.gl_set_attribute(pygame.GL_SWAP_CONTROL, 0)

    pygame.display.set_caption("OpenGL - transform feedback")

    pygame.display.set_mode((screen_width, screen_height), pygame.OPENGL | pygame.DOUBLEBUF)
    # pygame.display.set_mode((screen_width, screen_height), pygame.OPENGL)

    # these would be nice to configure in the openGL context, but I don't know how (if its
    # even possible using pygame)
    #
    # SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    # SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    # SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);

    # vertex_shader, geometry_shader, fragment_shader, shader_program = create_shader_program(vertex_source)

    program = shaders.compileProgram(
        shaders.compileShader([vertex_shader], GL_VERTEX_SHADER),
    )

    buff = ctypes.c_char_p("outValue")
    c_text = ctypes.cast(ctypes.pointer(buff), ctypes.POINTER(ctypes.POINTER(GLchar)))
    # modifies the state in the linking of the program
    glTransformFeedbackVaryings(program, 1, c_text, GL_INTERLEAVED_ATTRIBS)

    # so have to re-link
    glLinkProgram(program)
    glUseProgram(program)

    vao = glGenVertexArrays(1)
    glBindVertexArray(vao)

    data = (GLfloat * 5)(1.0, 2.0, 3.0, 4.0, 5.0)

    vbo = glGenBuffers(1)
    glBindBuffer(GL_ARRAY_BUFFER, vbo)
    glBufferData(GL_ARRAY_BUFFER, ctypes.sizeof(data), data, GL_STATIC_DRAW)

    inputAttrib = glGetAttribLocation(program, "inValue")
    glEnableVertexAttribArray(inputAttrib)
    # Note the need to cast 0 to a GLvoidp here!
    glVertexAttribPointer(inputAttrib, 1, GL_FLOAT, GL_FALSE, 0, GLvoidp(0))

    tbo = glGenBuffers(1)
    glBindBuffer(GL_ARRAY_BUFFER, tbo)
    glBufferData(GL_ARRAY_BUFFER, ctypes.sizeof(data), None, GL_STATIC_READ)


    # main loop
    clock = pygame.time.Clock()
    running = True

    while running:
        clock.tick(60)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False

        # render frame
        glEnable(GL_RASTERIZER_DISCARD)

        glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, tbo)

        glBeginTransformFeedback(GL_POINTS)
        glDrawArrays(GL_POINTS, 0, len(data))
        glEndTransformFeedback()

        glDisable(GL_RASTERIZER_DISCARD)

        glFlush()

        feedback = (GLfloat * len(data))()
        glGetBufferSubData(GL_TRANSFORM_FEEDBACK_BUFFER, 0, sizeof(feedback), feedback)

        for value in feedback:
            print(value)


    # delete
    glDeleteProgram(program)

    glDeleteVertexArrays(1, [vao])
    glDeleteBuffers(1, [vbo])


if __name__ == '__main__':
    main()
