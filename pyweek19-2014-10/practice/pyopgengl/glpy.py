# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek19-2014-10
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import ctypes
from ctypes import c_char_p, cast, pointer, POINTER

from OpenGL.GL import GLfloat, GLint, GLvoidp, GLchar, glGetUniformLocation, glUniform1f, glViewport

import glmath
from glmath import look_at, orthographic, addv3, subv3, normalize3

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"


# use ctypes.sizeof() for older compatibility
SIZE_OF_GL_FLOAT = ctypes.sizeof(GLfloat)
SIZE_OF_GL_INT = ctypes.sizeof(GLint)

NULL = None


def get_ptr(mat, type=GLfloat):
    # this is much faster than (type * len(mat))(*mat)
    # see here: https://stackoverflow.com/questions/39225263/why-is-ctypes-so-slow-to-convert-a-python-list-to-a-c-array
    arr = (type * len(mat))()
    arr[:] = mat
    return arr
    # return (type * len(mat))(*mat)


def get_float_ptr(value):
    return get_ptr(value, GLfloat)
    # return (GLfloat * len(value))(*value)


def get_int_ptr(value):
    return get_ptr(value, GLint)
    # return (GLint * len(value))(*value)


def get_stride(count, number_type=GLfloat):
    return GLvoidp(count * ctypes.sizeof(number_type))


def get_offset_from_index(count, number_type=GLfloat):
    # return ctypes.c_void_p(count * ctypes.sizeof(number_type))
    return get_stride(count, number_type)


def get_char_ptr(text):
    buff = c_char_p(text)
    c_text = cast(pointer(buff), POINTER(POINTER(GLchar)))
    return c_text


def get_char_ptr_ptr(varyings):
    """
    Returns a char** pointer from a list of strings. Does probably not work with unicode strings.
    :param varyings: list of strings
    :return: ctypes pointer for char**
    """
    c_varyings = (c_char_p * len(varyings))()  # create an array, pycharm gets confused here
    c_varyings[:] = varyings
    c_var = ctypes.cast(ctypes.pointer(c_varyings), ctypes.POINTER(ctypes.POINTER(ctypes.c_char)))
    return c_var


def set_uniform_1f(shader_program, name, value):
    u_loc = glGetUniformLocation(shader_program, name)
    glUniform1f(u_loc, value)


# def glUniformMatrix4fv(location, count, transpose, value):
#     _glUniformMatrix4fv(location, count, transpose, get_float_ptr(value))
#
#
# def glVertexAttribPointer(index, size, type, normalized, stride, pointer_to_buffer):
#     _type = GLfloat
#     if type == GL_INT:
#         _type = GLint
#     _glVertexAttribPointer(index, size, type, normalized, stride * ctypes.sizeof(_type),
#                            c_void_p(pointer_to_buffer * ctypes.sizeof(_type)))
#
# def glDrawElements(mode, count, type, indices):
#     _glDrawElements(mode, count, type, c_void_p(indices))


def print_array(elements, row_element_count):
    """
    Prints an array cropped lines to length n
    :param elements: the list to print
    :param row_element_count: the number of elements to place on a row.
    """
    for i in range(0, len(elements), row_element_count):
        print(", ".join(map(str, elements[i: i + row_element_count])))
    # for sub in [s[i: i + n] for i in range(0, len(s), n)]:
    #     print(", ".join(sub))


class Viewport(object):

    def __init__(self, top, left, width, height):
        self.top = int(top)
        self.left = int(left)
        self.width = int(width)
        self.height = int(height)
        self.camera = None

    def __enter__(self):
        glViewport(*self.as_tuple)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        return False  # re-raise any exceptions

    @property
    def as_tuple(self):
        """
        Creates a tuple.
        :return: a tuple like (self.left, self.top, self.width, self.height)
        """
        return self.left, self.top, self.width, self.height


class _Camera(object):

    def __init__(self, projection):
        self._pos3 = (0.0, 0.0, -1.0)
        self._up3 = [0.0, 1.0, 0.0]
        self._at3 = [0.0, 0.0, 0.0]
        self._look_dir3 = [0.0, 0.0, 1.0]
        self._view = look_at([0.0, 0.0, -1.0], [0.0, 0.0, 0.0], [0.0, -1.0, 0.0])
        self._view = None
        self._projection = projection
        self.fix_look_at = False

    @property
    def view(self):
        if self._view is None:
            loo_at_pos = glmath.addv3(self._pos3, self._look_dir3)
            self._view = look_at(self._pos3, loo_at_pos, self._up3)
        return self._view

    @property
    def projection(self):
        return self._projection

    @projection.setter
    def projection(self, new_projection):
        self._projection = new_projection

    @property
    def look_at(self):
        return glmath.addv3(self._pos3, self._look_dir3)

    @look_at.setter
    def look_at(self, at3):
        self._view = None
        self._look_dir3 = normalize3(glmath.subv3(at3, self._pos3))

    @property
    def position(self):
        return self._pos3

    @position.setter
    def position(self, position3):
        if self.fix_look_at:
            at = self.look_at

        self._pos3 = position3
        self._view = None

        if self.fix_look_at:
            self.look_at = at

    @property
    def up(self):
        return self._up3

    @up.setter
    def up(self, up3):
        self._up3 = up3
        self._view = None

    def move(self, delta):
        self.position = addv3(self._pos3, delta)

    @property
    def look_direction(self):
        return self._look_dir3


class PygameCamera(_Camera):
    """
    This is a camera that has the same view as pygame. Origin is at topleft of the screen (
    """
    _z_position = 1002

    def __init__(self, screen_width, screen_height):
        projection = glmath.orthographic_symmetric(1.0, PygameCamera._z_position + 1, screen_width, screen_height)
        projection = glmath.mult_mat4(projection, glmath.flip_y_m4())
        _Camera.__init__(self, projection)
        half_sw = screen_width // 2
        half_sh = screen_height // 2
        self.position = [half_sw, half_sh, PygameCamera._z_position]
        self.look_at = [half_sw, half_sh, 0.0]

    @property
    def view(self):
        if self._view is None:
            self._pos3 = (self._pos3[0], self._pos3[1], PygameCamera._z_position)
            loo_at_pos = glmath.addv3(self._pos3, self._look_dir3)
            self._view = look_at(self._pos3, loo_at_pos, self._up3)
        return self._view


class OrthogonalCamera(_Camera):

    def __init__(self):
        _Camera.__init__(self, orthographic(0.5, 100, -1, 1, 1, -1))  # todo: parameters?



__all__ = [get_ptr.__name__, "NULL", get_offset_from_index.__name__, OrthogonalCamera.__name__]  # list of public visible parts of this module
