# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek19-2014-10
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function


__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2014"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module


import timeit

class Obj(object):
    def __init__(self, i):
        object.__init__(self)
        self.i = i

num_passes = 10000

objects = [Obj(i) for i in range(100)]
count = 0

def test0(seq):
    # pop
    global count
    out = []
    out_append = out.append
    while seq:
        o1 = seq.pop(-1)
        o1.i
        for o2 in seq:
            o2.i
            count += 1
            out_append(o1)
    return out

def test1(seq):
    # while
    global count
    n = len(seq)
    i1 = 0
    while i1 < n:
        o1 = seq[i1]
        o1.i
        i2 = i1 + 1
        while i2 < n:
            o2 = seq[i2]
            o2.i
            i2 += 1
            count += 1
        i1 += 1
    return seq

def test2(seq):
    # for
    global count
    n = len(seq)
    for i1 in xrange(n):
        o1 = seq[i1]
        o1.i
        for i2 in xrange(i1 + 1, n):
            o2 = seq[i2]
            o2.i
            count += 1
    return seq

def test3(seq):
    # slice
    global count
    n = len(seq)
    for i1, o1 in enumerate(seq):
        o1.i
        for o2 in seq[i1 + 1:]:
            o2.i
            count += 1
    return seq

count = 0
print('test0     pop: {0:0.5f}'.format(timeit.timeit(
    'test0(list(objects))', setup="from __main__ import test0, objects", number=num_passes)))
print('count: {0}'.format(count))

count = 0
print('test1   while: {0:0.5f}'.format(timeit.timeit(
    'test1(list(objects))', setup="from __main__ import test1, objects", number=num_passes)))
print('count: {0}'.format(count))

count = 0
print('test2   for: {0:0.5f}'.format(timeit.timeit(
    'test2(list(objects))', setup="from __main__ import test2, objects", number=num_passes)))
print('count: {0}'.format(count))

count = 0
print('test3 slice: {0:0.5f}'.format(timeit.timeit(
    'test3(list(objects))', setup="from __main__ import test3, objects", number=num_passes)))
print('count: {0}'.format(count))

"""
Results:
  pop: 4.54134
count: 49500000
while: 6.64925
count: 49500000
  for: 5.51049
count: 49500000
slice: 4.68893
count: 49500000
"""
