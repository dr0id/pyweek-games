# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek19-2014-10
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
# * Neither the name of the organization> nor the
# names of its contributors may be used to endorse or promote products
# derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Unittests for the dirty rectangle split method. The tests are divided into following classes:


PerformanceTests:
    performance comparing tests, compares speed dirty_rectangle_split against optimized_dirty_rects

TestDirtyRectSplit:
    this contains the tests that assure correct splitting

TestOtherOptimizedDirtyRects:
    this executes the same tests as in TestDirtyRectSplit using the optimized_dirty_rects algorithm


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function
import random

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id [at] gmail [dot] com"
__copyright__ = "DR0ID @ 2014"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

import unittest
import time

import optimized_dirty_rects

import dirty_rectangle_split as mut  # module under test
import rectangle_test_data as test_data
import datadriventestingdecorators as ddtd
from pygame import Rect

if mut.__version_info__ != __version_info__:
    raise Exception("version numbers do not match!")


class TestTestData(unittest.TestCase):
    def test_overlapping_rects_do_overlap(self):
        # arrange
        rects = test_data.get_overlapping_rects_from_combinations(10)

        # act / verify
        for r1, r2, overlap in rects:
            self.assertTrue(r1.colliderect(r2))

    def test_overlapping_rects_overlap_is_same_as_provided(self):
        # arrange
        rects = test_data.get_all_rects(10, 10)

        # act / verify
        for r1, r2, overlap in rects:
            common = r1.clip(r2).clip(r1)
            self.assertEqual(overlap, common)


def rect_test_cases_generator():
    """
    This is a generator function to generate the different test cases.

    It will yield following data structure: (input, expected, additional test case parameters)
    where:

    input: list of rectangles
    expected: list of rectangles with no overlap
    additional test case parameters: test_name parameter to easier find a failing test case
    """
    test_cases = [
        # these are the 81 cases
        # blue                     yellow                 expected split rectangles
        (([Rect(0, 0, 10, 10), Rect(-10, -10, 30, 30)],
          [Rect(-10, -10, 30, 30)]), {'test_name': '00_10-0_(10)'}),
        (([Rect(0, 0, 10, 10), Rect(0, -10, 20, 30)],
          [Rect(0, -10, 20, 30)]), {'test_name': '01_10-1_(26)'}),
        (([Rect(-10, 0, 20, 10), Rect(0, -10, 20, 30)],
          [Rect(0, -10, 20, 10), Rect(-10, 0, 30, 10), Rect(0, 10, 20, 10)]), {'test_name': '02_11-1_(27)'}),
        (([Rect(-10, -10, 20, 20), Rect(0, 0, 20, 20)],
          [Rect(-10, -10, 20, 10), Rect(-10, 0, 30, 10), Rect(0, 10, 20, 10)]), {'test_name': '03_08-2_(40)'}),
        (([Rect(0, -10, 10, 20), Rect(0, 0, 20, 20)],
          [Rect(0, -10, 10, 10), Rect(0, 0, 20, 20)]), {'test_name': '04_09-2_(41)'}),
        (([Rect(0, 0, 10, 10), Rect(0, 0, 20, 20)],
          [Rect(0, 0, 20, 20)]), {'test_name': '05_10-2_(42)'}),
        (([Rect(-10, 0, 20, 10), Rect(0, 0, 20, 20)],
          [Rect(-10, 0, 30, 10), Rect(0, 10, 20, 10)]), {'test_name': '06_11-2_(43)'}),
        (([Rect(0, -10, 10, 20), Rect(-10, 0, 30, 20)],
          [Rect(0, -10, 10, 10), Rect(-10, 0, 30, 20)]), {'test_name': '07_09-3_(57)'}),
        (([Rect(0, 0, 10, 10), Rect(-10, 0, 30, 20)],
          [Rect(-10, 0, 30, 20)]), {'test_name': '08_10-3_(58)'}),
        (([Rect(0, -10, 10, 20), Rect(-10, 0, 30, 10)],
          [Rect(0, -10, 10, 10), Rect(-10, 0, 30, 10)]), {'test_name': '09_09-4_(73)'}),
        (([Rect(0, 0, 10, 10), Rect(-10, 0, 30, 10)],
          [Rect(-10, 0, 30, 10)]), {'test_name': '10_10-4_(74)'}),
        (([Rect(0, 0, 10, 20), Rect(-10, 0, 30, 10)],
          [Rect(-10, 0, 30, 10), Rect(0, 10, 10, 10)]), {'test_name': '11_13-4_(77)'}),
        (([Rect(0, -10, 10, 30), Rect(-10, 0, 30, 10)],
          [Rect(0, -10, 10, 10), Rect(-10, 0, 30, 10), Rect(0, 10, 10, 10)]), {'test_name': '12_14-4_(78)'}),
        (([Rect(-10, -10, 20, 20), Rect(0, 0, 20, 10)],
          [Rect(-10, -10, 20, 10), Rect(-10, 0, 30, 10)]), {'test_name': '13_08-5_(88)'}),
        (([Rect(0, -10, 10, 20), Rect(0, 0, 20, 10)],
          [Rect(0, -10, 10, 10), Rect(0, 0, 20, 10)]), {'test_name': '14_09-5_(89)'}),
        (([Rect(0, 0, 10, 10), Rect(0, 0, 20, 10)],
          [Rect(0, 0, 20, 10)]), {'test_name': '15_10-5_(90)'}),
        (([Rect(-10, 0, 20, 10), Rect(0, 0, 20, 10)],
          [Rect(-10, 0, 30, 10)]), {'test_name': '16_11-5_(91)'}),
        (([Rect(-10, 0, 20, 20), Rect(0, 0, 20, 10)],
          [Rect(-10, 0, 30, 10), Rect(-10, 10, 20, 10)]), {'test_name': '17_12-5_(92)'}),
        (([Rect(0, 0, 10, 20), Rect(0, 0, 20, 10)],
          [Rect(0, 0, 20, 10), Rect(0, 10, 10, 10)]), {'test_name': '18_13-5_(93)'}),
        (([Rect(0, -10, 10, 30), Rect(0, 0, 20, 10)],
          [Rect(0, -10, 10, 10), Rect(0, 0, 20, 10), Rect(0, 10, 10, 10)]), {'test_name': '19_14-5_(94)'}),
        (([Rect(-10, -10, 20, 30), Rect(0, 0, 20, 10)],
          [Rect(-10, -10, 20, 10), Rect(-10, 0, 30, 10), Rect(-10, 10, 20, 10)]), {'test_name': '20_15-5_(95)'}),
        (([Rect(0, 0, 10, 10), Rect(0, -10, 20, 20)],
          [Rect(0, -10, 20, 20)]), {'test_name': '21_10-6_(106)'}),
        (([Rect(-10, 0, 20, 10), Rect(0, -10, 20, 20)],
          [Rect(0, -10, 20, 10), Rect(-10, 0, 30, 10)]), {'test_name': '22_11-6_(107)'}),
        (([Rect(-10, 0, 20, 20), Rect(0, -10, 20, 20)],
          [Rect(0, -10, 20, 10), Rect(-10, 0, 30, 10), Rect(-10, 10, 20, 10)]), {'test_name': '23_12-6_(108)'}),
        (([Rect(0, 0, 10, 20), Rect(0, -10, 20, 20)],
          [Rect(0, -10, 20, 20), Rect(0, 10, 10, 10)]), {'test_name': '24_13-6_(109)'}),
        (([Rect(0, 0, 10, 10), Rect(-10, -10, 30, 20)],
          [Rect(-10, -10, 30, 20)]), {'test_name': '25_10-7_(122)'}),
        (([Rect(0, 0, 10, 20), Rect(-10, -10, 30, 20)],
          [Rect(-10, -10, 30, 20), Rect(0, 10, 10, 10)]), {'test_name': '26_13-7_(125)'}),
        (([Rect(0, 0, 20, 20), Rect(-10, -10, 20, 20)],
          [Rect(-10, -10, 20, 10), Rect(-10, 0, 30, 10), Rect(0, 10, 20, 10)]), {'test_name': '27_02-8_(130)'}),
        (([Rect(0, 0, 20, 10), Rect(-10, -10, 20, 20)],
          [Rect(-10, -10, 20, 10), Rect(-10, 0, 30, 10)]), {'test_name': '28_05-8_(133)'}),
        (([Rect(0, 0, 10, 10), Rect(-10, -10, 20, 20)],
          [Rect(-10, -10, 20, 20)]), {'test_name': '29_10-8_(138)'}),
        (([Rect(0, 0, 10, 20), Rect(-10, -10, 20, 20)],
          [Rect(-10, -10, 20, 20), Rect(0, 10, 10, 10)]), {'test_name': '30_13-8_(141)'}),
        (([Rect(0, 0, 20, 20), Rect(0, -10, 10, 20)],
          [Rect(0, -10, 10, 10), Rect(0, 0, 20, 20)]), {'test_name': '31_02-9_(146)'}),
        (([Rect(-10, 0, 30, 20), Rect(0, -10, 10, 20)],
          [Rect(0, -10, 10, 10), Rect(-10, 0, 30, 20)]), {'test_name': '32_03-9_(147)'}),
        (([Rect(-10, 0, 30, 10), Rect(0, -10, 10, 20)],
          [Rect(0, -10, 10, 10), Rect(-10, 0, 30, 10)]), {'test_name': '33_04-9_(148)'}),
        (([Rect(0, 0, 20, 10), Rect(0, -10, 10, 20)],
          [Rect(0, -10, 10, 10), Rect(0, 0, 20, 10)]), {'test_name': '34_05-9_(149)'}),
        (([Rect(0, 0, 10, 10), Rect(0, -10, 10, 20)],
          [Rect(0, -10, 10, 20)]), {'test_name': '35_10-9_(154)'}),
        (([Rect(-10, 0, 20, 10), Rect(0, -10, 10, 20)],
          [Rect(0, -10, 10, 10), Rect(-10, 0, 20, 10)]), {'test_name': '36_11-9_(155)'}),
        (([Rect(-10, 0, 20, 20), Rect(0, -10, 10, 20)],
          [Rect(0, -10, 10, 10), Rect(-10, 0, 20, 20)]), {'test_name': '37_12-9_(156)'}),
        (([Rect(0, 0, 10, 20), Rect(0, -10, 10, 20)],
          [Rect(0, -10, 10, 30)]), {'test_name': '38_13-9_(157)'}),
        (([Rect(-10, -10, 30, 30), Rect(0, 0, 10, 10)],
          [Rect(-10, -10, 30, 30)]), {'test_name': '39_00-10_(160)'}),
        (([Rect(0, -10, 20, 30), Rect(0, 0, 10, 10)],
          [Rect(0, -10, 20, 30)]), {'test_name': '40_01-10_(161)'}),
        (([Rect(0, 0, 20, 20), Rect(0, 0, 10, 10)],
          [Rect(0, 0, 20, 20)]), {'test_name': '41_02-10_(162)'}),
        (([Rect(-10, 0, 30, 20), Rect(0, 0, 10, 10)],
          [Rect(-10, 0, 30, 20)]), {'test_name': '42_03-10_(163)'}),
        (([Rect(-10, 0, 30, 10), Rect(0, 0, 10, 10)],
          [Rect(-10, 0, 30, 10)]), {'test_name': '43_04-10_(164)'}),
        (([Rect(0, 0, 20, 10), Rect(0, 0, 10, 10)],
          [Rect(0, 0, 20, 10)]), {'test_name': '44_05-10_(165)'}),
        (([Rect(0, -10, 20, 20), Rect(0, 0, 10, 10)],
          [Rect(0, -10, 20, 20)]), {'test_name': '45_06-10_(166)'}),
        (([Rect(-10, -10, 30, 20), Rect(0, 0, 10, 10)],
          [Rect(-10, -10, 30, 20)]), {'test_name': '46_07-10_(167)'}),
        (([Rect(-10, -10, 20, 20), Rect(0, 0, 10, 10)],
          [Rect(-10, -10, 20, 20)]), {'test_name': '47_08-10_(168)'}),
        (([Rect(0, -10, 10, 20), Rect(0, 0, 10, 10)],
          [Rect(0, -10, 10, 20)]), {'test_name': '48_09-10_(169)'}),
        (([Rect(0, 0, 10, 10), Rect(0, 0, 10, 10)],
          [Rect(0, 0, 10, 10)]), {'test_name': '49_10-10_(170)'}),
        (([Rect(-10, 0, 20, 10), Rect(0, 0, 10, 10)],
          [Rect(-10, 0, 20, 10)]), {'test_name': '50_11-10_(171)'}),
        (([Rect(-10, 0, 20, 20), Rect(0, 0, 10, 10)],
          [Rect(-10, 0, 20, 20)]), {'test_name': '51_12-10_(172)'}),
        (([Rect(0, 0, 10, 20), Rect(0, 0, 10, 10)],
          [Rect(0, 0, 10, 20)]), {'test_name': '52_13-10_(173)'}),
        (([Rect(0, -10, 10, 30), Rect(0, 0, 10, 10)],
          [Rect(0, -10, 10, 30)]), {'test_name': '53_14-10_(174)'}),
        (([Rect(-10, -10, 20, 30), Rect(0, 0, 10, 10)],
          [Rect(-10, -10, 20, 30)]), {'test_name': '54_15-10_(175)'}),
        (([Rect(0, -10, 20, 30), Rect(-10, 0, 20, 10)],
          [Rect(0, -10, 20, 10), Rect(-10, 0, 30, 10), Rect(0, 10, 20, 10)]), {'test_name': '55_01-11_(177)'}),
        (([Rect(0, 0, 20, 20), Rect(-10, 0, 20, 10)],
          [Rect(-10, 0, 30, 10), Rect(0, 10, 20, 10)]), {'test_name': '56_02-11_(178)'}),
        (([Rect(0, 0, 20, 10), Rect(-10, 0, 20, 10)],
          [Rect(-10, 0, 30, 10)]), {'test_name': '57_05-11_(181)'}),
        (([Rect(0, -10, 20, 20), Rect(-10, 0, 20, 10)],
          [Rect(0, -10, 20, 10), Rect(-10, 0, 30, 10)]), {'test_name': '58_06-11_(182)'}),
        (([Rect(0, -10, 10, 20), Rect(-10, 0, 20, 10)],
          [Rect(0, -10, 10, 10), Rect(-10, 0, 20, 10)]), {'test_name': '59_09-11_(185)'}),
        (([Rect(0, 0, 10, 10), Rect(-10, 0, 20, 10)],
          [Rect(-10, 0, 20, 10)]), {'test_name': '60_10-11_(186)'}),
        (([Rect(0, 0, 10, 20), Rect(-10, 0, 20, 10)],
          [Rect(-10, 0, 20, 10), Rect(0, 10, 10, 10)]), {'test_name': '61_13-11_(189)'}),
        (([Rect(0, -10, 10, 30), Rect(-10, 0, 20, 10)],
          [Rect(0, -10, 10, 10), Rect(-10, 0, 20, 10), Rect(0, 10, 10, 10)]), {'test_name': '62_14-11_(190)'}),
        (([Rect(0, 0, 20, 10), Rect(-10, 0, 20, 20)],
          [Rect(-10, 0, 30, 10), Rect(-10, 10, 20, 10)]), {'test_name': '63_05-12_(197)'}),
        (([Rect(0, -10, 20, 20), Rect(-10, 0, 20, 20)],
          [Rect(0, -10, 20, 10), Rect(-10, 0, 30, 10), Rect(-10, 10, 20, 10)]), {'test_name': '64_06-12_(198)'}),
        (([Rect(0, -10, 10, 20), Rect(-10, 0, 20, 20)],
          [Rect(0, -10, 10, 10), Rect(-10, 0, 20, 20)]), {'test_name': '65_09-12_(201)'}),
        (([Rect(0, 0, 10, 10), Rect(-10, 0, 20, 20)],
          [Rect(-10, 0, 20, 20)]), {'test_name': '66_10-12_(202)'}),
        (([Rect(-10, 0, 30, 10), Rect(0, 0, 10, 20)],
          [Rect(-10, 0, 30, 10), Rect(0, 10, 10, 10)]), {'test_name': '67_04-13_(212)'}),
        (([Rect(0, 0, 20, 10), Rect(0, 0, 10, 20)],
          [Rect(0, 0, 20, 10), Rect(0, 10, 10, 10)]), {'test_name': '68_05-13_(213)'}),
        (([Rect(0, -10, 20, 20), Rect(0, 0, 10, 20)],
          [Rect(0, -10, 20, 20), Rect(0, 10, 10, 10)]), {'test_name': '69_06-13_(214)'}),
        (([Rect(-10, -10, 30, 20), Rect(0, 0, 10, 20)],
          [Rect(-10, -10, 30, 20), Rect(0, 10, 10, 10)]), {'test_name': '70_07-13_(215)'}),
        (([Rect(-10, -10, 20, 20), Rect(0, 0, 10, 20)],
          [Rect(-10, -10, 20, 20), Rect(0, 10, 10, 10)]), {'test_name': '71_08-13_(216)'}),
        (([Rect(0, -10, 10, 20), Rect(0, 0, 10, 20)],
          [Rect(0, -10, 10, 30)]), {'test_name': '72_09-13_(217)'}),
        (([Rect(0, 0, 10, 10), Rect(0, 0, 10, 20)],
          [Rect(0, 0, 10, 20)]), {'test_name': '73_10-13_(218)'}),
        (([Rect(-10, 0, 20, 10), Rect(0, 0, 10, 20)],
          [Rect(-10, 0, 20, 10), Rect(0, 10, 10, 10)]), {'test_name': '74_11-13_(219)'}),
        (([Rect(-10, 0, 30, 10), Rect(0, -10, 10, 30)],
          [Rect(0, -10, 10, 10), Rect(-10, 0, 30, 10), Rect(0, 10, 10, 10)]), {'test_name': '75_04-14_(228)'}),
        (([Rect(0, 0, 20, 10), Rect(0, -10, 10, 30)],
          [Rect(0, -10, 10, 10), Rect(0, 0, 20, 10), Rect(0, 10, 10, 10)]), {'test_name': '76_05-14_(229)'}),
        (([Rect(0, 0, 10, 10), Rect(0, -10, 10, 30)],
          [Rect(0, -10, 10, 30)]), {'test_name': '77_10-14_(234)'}),
        (([Rect(-10, 0, 20, 10), Rect(0, -10, 10, 30)],
          [Rect(0, -10, 10, 10), Rect(-10, 0, 20, 10), Rect(0, 10, 10, 10)]), {'test_name': '78_11-14_(235)'}),
        (([Rect(0, 0, 20, 10), Rect(-10, -10, 20, 30)],
          [Rect(-10, -10, 20, 10), Rect(-10, 0, 30, 10), Rect(-10, 10, 20, 10)]), {'test_name': '79_05-15_(245)'}),
        (([Rect(0, 0, 10, 10), Rect(-10, -10, 20, 30)],
          [Rect(-10, -10, 20, 30)]), {'test_name': '80_10-15_(250)'}),

        # non overlapping cases
        (([Rect(0, 0, 10, 10), Rect(-10, 0, 10, 10)], [Rect(-10, 0, 20, 10)]),
         {'test_name': '81_00-16_(256)'}),
        (([Rect(0, 0, 10, 10), Rect(-10, -10, 10, 10)], [Rect(0, 0, 10, 10), Rect(-10, -10, 10, 10)]),
         {'test_name': '82_01-16_(257)'}),
        (([Rect(0, 0, 10, 10), Rect(0, -10, 10, 10)], [Rect(0, -10, 10, 20)]),
         {'test_name': '83_02-16_(258)'}),
        (([Rect(0, 0, 10, 10), Rect(10, -10, 10, 10)], [Rect(0, 0, 10, 10), Rect(10, -10, 10, 10)]),
         {'test_name': '84_03-16_(259)'}),
        (([Rect(0, 0, 10, 10), Rect(10, 0, 10, 10)], [Rect(0, 0, 20, 10)]),
         {'test_name': '85_04-16_(260)'}),
        (([Rect(0, 0, 10, 10), Rect(10, 10, 10, 10)], [Rect(0, 0, 10, 10), Rect(10, 10, 10, 10)]),
         {'test_name': '86_05-16_(261)'}),
        (([Rect(0, 0, 10, 10), Rect(0, 10, 10, 10)], [Rect(0, 0, 10, 20)]),
         {'test_name': '87_06-16_(262)'}),
        (([Rect(0, 0, 10, 10), Rect(-10, 10, 10, 10)], [Rect(0, 0, 10, 10), Rect(-10, 10, 10, 10)]),
         {'test_name': '88_07-16_(263)'}),
        (([Rect(0, 0, 10, 10), Rect(-15, 0, 10, 10)], [Rect(0, 0, 10, 10), Rect(-15, 0, 10, 10)]),
         {'test_name': '89_08-16_(264)'}),
        (([Rect(0, 0, 10, 10), Rect(-15, -15, 10, 10)], [Rect(0, 0, 10, 10), Rect(-15, -15, 10, 10)]),
         {'test_name': '90_09-16_(265)'}),
        (([Rect(0, 0, 10, 10), Rect(0, -15, 10, 10)], [Rect(0, 0, 10, 10), Rect(0, -15, 10, 10)]),
         {'test_name': '91_10-16_(266)'}),
        (([Rect(0, 0, 10, 10), Rect(15, -15, 10, 10)], [Rect(0, 0, 10, 10), Rect(15, -15, 10, 10)]),
         {'test_name': '92_11-16_(267)'}),
        (([Rect(0, 0, 10, 10), Rect(15, 0, 10, 10)], [Rect(0, 0, 10, 10), Rect(15, 0, 10, 10)]),
         {'test_name': '93_12-16_(268)'}),
        (([Rect(0, 0, 10, 10), Rect(15, 15, 10, 10)], [Rect(0, 0, 10, 10), Rect(15, 15, 10, 10)]),
         {'test_name': '94_13-16_(269)'}),
        (([Rect(0, 0, 10, 10), Rect(0, 15, 10, 10)], [Rect(0, 0, 10, 10), Rect(0, 15, 10, 10)]),
         {'test_name': '95_14-16_(270)'}),
        (([Rect(0, 0, 10, 10), Rect(-15, 15, 10, 10)], [Rect(0, 0, 10, 10), Rect(-15, 15, 10, 10)]),
         {'test_name': '96_15-16_(271)'}),
        (([Rect(0, 0, 10, 10), Rect(-15, -5, 10, 10)], [Rect(0, 0, 10, 10), Rect(-15, -5, 10, 10)]),
         {'test_name': '97_00-17_(272)'}),
        (([Rect(0, 0, 10, 10), Rect(-5, -15, 10, 10)], [Rect(0, 0, 10, 10), Rect(-5, -15, 10, 10)]),
         {'test_name': '98_01-17_(273)'}),
        (([Rect(0, 0, 10, 10), Rect(5, -15, 10, 10)], [Rect(0, 0, 10, 10), Rect(5, -15, 10, 10)]),
         {'test_name': '99_02-17_(274)'}),
        (([Rect(0, 0, 10, 10), Rect(15, -5, 10, 10)], [Rect(0, 0, 10, 10), Rect(15, -5, 10, 10)]),
         {'test_name': '100_03-17_(275)'}),
        (([Rect(0, 0, 10, 10), Rect(15, 5, 10, 10)], [Rect(0, 0, 10, 10), Rect(15, 5, 10, 10)]),
         {'test_name': '101_04-17_(276)'}),
        (([Rect(0, 0, 10, 10), Rect(5, 15, 10, 10)], [Rect(0, 0, 10, 10), Rect(5, 15, 10, 10)]),
         {'test_name': '102_05-17_(277)'}),
        (([Rect(0, 0, 10, 10), Rect(-5, 15, 10, 10)], [Rect(0, 0, 10, 10), Rect(-5, 15, 10, 10)]),
         {'test_name': '103_06-17_(278)'}),
        (([Rect(0, 0, 10, 10), Rect(-15, 5, 10, 10)], [Rect(0, 0, 10, 10), Rect(-15, 5, 10, 10)]),
         {'test_name': '104_07-17_(279)'}),

        # special test, same top after split
        (([Rect(0, 0, 10, 10), Rect(0, 0, 10, 10), Rect(0, 0, 10, 10)], [Rect(0, 0, 10, 10)]),
         {'test_name': '105_exact_overlapping'}),
        (([Rect(0, 0, 10, 10), Rect(5, 0, 10, 10), Rect(-5, 0, 10, 10)], [Rect(-5, 0, 20, 10)]),
         {'test_name': '106_x_shifted_overlapping'}),
        (([Rect(0, 0, 10, 10), Rect(0, 5, 10, 10), Rect(0, -5, 10, 10)], [Rect(0, -5, 10, 20)]),
         {'test_name': '107_y_shifted_overlapping'}),
        (([Rect(-20, 0, 40, 0), Rect(0, -20, 0, 40), Rect(-10, -10, 20, 20)], [Rect(-10, -10, 20, 20)]),
         {'test_name': '108_y_0_width_0_height'}),
        (([Rect(-10, -10, 20, 20)], [Rect(-10, -10, 20, 20)]),
         {'test_name': '109_y_single_rect'}),
        (([Rect(15, -15, 10, 5), Rect(15, 20, 10, 5), Rect(-15, -15, 30, 5), Rect(-15, 20, 30, 5),
           Rect(-15, -10, 40, 30)],
          [Rect(-15, -15, 40, 40)]), {"test_name": "boundaries1"}),

    ]

    for idx, test_case in enumerate(test_cases):
        yield test_case


def triple_rect_generator():
    """
    Generator function providing test cases. This function generates 6561 test cases. It does combine
    each of the 81 cases with the 81 interesting combinations.

    Provides following data structure: (input, additional test case parameters)
    where:

    input: list of rectangles to split
    additional test case parameters: test name parameter to easier identify failing tests
    """
    combs, indices = test_data.get_overlapping_combinations()
    combinations = test_data.get_overlapping_rects_from_combinations(10, 10, combs)
    comb_tuples = [[yellow, blue] for yellow, blue, magenta in combinations]
    for c in comb_tuples:
        c.sort(key=lambda rec: (-rec.y, -rec.x, rec.w, rec.h))

    test_cases = []
    for idx, comb in enumerate(comb_tuples):
        table_index = indices[idx]
        for sub_idx, sub_comb in enumerate(comb_tuples):
            sub_table_index = indices[sub_idx]
            r2, r1 = comb
            s2, s1 = sub_comb
            dx = r2.left - s1.left
            dy = r2.top - s1.top

            s2 = s2.move(dx, dy)

            test_name = "{0:02}_{1:02}-{2:02}_({3})--{4:02}_{5:02}-{6:02}_({7})".format(idx, table_index % 16,
                                                                                        table_index // 16, table_index,
                                                                                        sub_idx, sub_table_index % 16,
                                                                                        sub_table_index // 16,
                                                                                        sub_table_index)

            test_cases.append((([r1, r2, s2],), {'test_name': test_name}))

    for count, test_case in enumerate(test_cases):
        if count > 50000:
            break
        yield test_case


@ddtd.use_data_driven_testing_decorators
class TestDirtyRectSplit(unittest.TestCase):
    def setUp(self):
        self.func = mut.split_dirty_rects

    def get_rects(self, overlapping_rects):
        rects = [rec[0].copy() for rec in overlapping_rects]
        rects.extend([rec[1].copy() for rec in overlapping_rects])
        return rects

    @ddtd.test_case_from_generator(rect_test_cases_generator)
    def test_overlapping_rects(self, dirty_rects, expected_overlaps):
        # arrange
        # make a copy because the list will be modified
        _dirty_rects_copy = [dr.copy() for dr in dirty_rects]

        # act
        actual_dirty_areas = self.func(_dirty_rects_copy)

        # sort to be able to compare the lists
        expected_overlaps.sort(key=lambda r_o: (r_o[0], r_o[1], r_o[2], r_o[3]))
        actual_dirty_areas.sort(key=lambda r_a: (r_a[0], r_a[1], r_a[2], r_a[3]))

        # verify
        self.maxDiff = None
        self.assertEqual(expected_overlaps, actual_dirty_areas)

    @ddtd.test_case_from_generator(triple_rect_generator)
    def __test_overlapping_of_3_rects(self, orig_dirty_rects):
        # arrange
        dirty_rects = [rec.copy() for rec in orig_dirty_rects]

        # act
        actual = self.func(dirty_rects)

        # verify
        for rr in actual:
            indices = rr.collidelistall(actual)
            self.assertEqual(1, len(indices), "cur: {0} \nin \n{1} \nfrom \n{2}".format(rr, actual, orig_dirty_rects))
            self.assertEqual(actual.index(rr), indices[0],
                             "cur: {0} \nin \n{1} \nfrom \n{2}".format(rr, actual, orig_dirty_rects))

    def test_special_arrangement(self):
        # arrange
        # orig_dirty_rects = [Rect(0, -10, 20, 30), Rect(-10, 0, 20, 10), Rect(-20, 10, 20, 10)]
        # orig_dirty_rects = [Rect(-10, 0, 20, 10), Rect(0, 0, 20, 20), Rect(0, 0, 10, 10)]
        #
        # orig_dirty_rects = [Rect(0, 0, 10, 10), Rect(-10, -10, 30, 30)]
        # orig_dirty_rects = [Rect(-10, 0, 20, 10), Rect(0, -10, 20, 30)]

        orig_dirty_rects = [Rect(0, 0, 10, 10), Rect(-10, -10, 10, 10)]  # {'test_name': '82_01-16_(257)'}),

        dirty_rects = [rec.copy() for rec in orig_dirty_rects]

        # act
        actual = self.func(dirty_rects)

        # verify
        actual.sort(key=lambda rec: (rec.topleft, rec.size))
        # self.assertEqual([pr(-10, -10, 30, 30)], actual)
        self.assertEqual([Rect(-10, -10, 10, 10), Rect(0, 0, 10, 10)], actual)  # {'test_name': '82_01-16_(257)'}),
        # self.assertEqual([pr(0, -10, 20, 10), pr(-10, 0, 30, 10), pr(0, 10, 20, 10)], actual)
        for rr in actual:
            indices = rr.collidelistall(actual)
            self.assertEqual(1, len(indices),
                             "cur: {0} \nin \n{1} \nfrom \n{2}\n\nindices: {3}".format(rr, actual, orig_dirty_rects,
                                                                                       indices))
            self.assertEqual(actual.index(rr), indices[0],
                             "cur: {0} \nin \n{1} \nfrom \n{2}\n\nindices: {3}".format(rr, actual, orig_dirty_rects,
                                                                                       indices))

    def test_random_rects(self):
        # arrange
        ri = random.randint
        dirty_rects = []
        w = 20
        h = 20
        step = 20
        for i in range(1000):
            rec = Rect(ri(-w, w), i * step * 0 + ri(-h, h), ri(0, w), ri(0, h))
            rec.normalize()
            dirty_rects.append(rec)



            # dirty_rects = [pr(-20, -20, 57, 6), pr(-20, -14, 57, 1), pr(-20, -13, 57, 1), pr(-20, -12, 57, 3), pr(-20, -9, 57, 0), pr(-20, -9, 57, 2), pr(-20, -7, 57, 1), pr(-20, -6, 59, 2), pr(-20, -4, 55, 0), pr(-20, -4, 59, 3), pr(-20, -1, 59, 1), pr(-20, 0, 59, 1), pr(-20, 1, 59, 1), pr(-20, 2, 59, 1), pr(-20, 3, 50, 0), pr(-20, 3, 59, 1), pr(-20, 4, 59, 1), pr(-20, 5, 59, 1), pr(-20, 6, 59, 1), pr(-20, 7, 59, 2), pr(-20, 9, 34, 0), pr(-20, 9, 59, 2), pr(-20, 11, 59, 1), pr(-20, 12, 59, 1), pr(-20, 13, 59, 1), pr(-20, 14, 43, 1), pr(-20, 14, 0, 13), pr(-19, 14, 0, 7), pr(-14, 14, 17, 0), pr(-12, 14, 1, 11), pr(-11, 14, 50, 1), pr(-20, 15, 57, 1), pr(-20, 16, 57, 1), pr(-20, 17, 57, 2), pr(-20, 19, 56, 2), pr(-20, 21, 56, 1), pr(-20, 22, 56, 2), pr(-20, 24, 56, 2), pr(-20, 26, 56, 1), pr(-19, 27, 55, 1), pr(-19, 28, 55, 1), pr(-19, 29, 55, 1), pr(-19, 30, 55, 1), pr(-19, 31, 54, 3), pr(-16, 34, 51, 1), pr(-16, 35, 51, 1), pr(-16, 36, 43, 1), pr(-12, 37, 5, 3), pr(12, 37, 9, 1), pr(12, 38, 5, 1)]
            # dirty_rects = dirty_rects[len(dirty_rects)/2:len(dirty_rects)/3*4]
            # dirty_rects = dirty_rects[:len(dirty_rects)/2]

        # AssertionError: current rect(-20, 14, 43, 1)>
        # [0, 1, 2, 3, 4, 5] in
        # [rect(-20, 14, 43, 1), rect(-20, 14, 0, 13), rect(-19, 14, 0, 7), rect(-14, 14, 17, 0), rect(-12, 14, 1, 11), rect(-11, 14, 50, 1), rect(-20, 15, 57, 1), rect(-20, 16, 57, 1), rect(-20, 17, 57, 2), rect(-20, 19, 56, 2), rect(-20, 21, 56, 1), rect(-20, 22, 56, 2)>]
        # original
        # [rect(-20, 22, 56, 2), rect(-20, 21, 56, 1), rect(-20, 19, 56, 2), rect(-20, 17, 57, 2), rect(-20, 16, 57, 1), rect(-20, 15, 57, 1), rect(-11, 14, 50, 1), rect(-12, 14, 1, 11), rect(-14, 14, 17, 0), rect(-19, 14, 0, 7), rect(-20, 14, 0, 13), rect(-20, 14, 43, 1)>]

        # dirty_rects = [pr(-20, 22, 56, 2), pr(-20, 21, 56, 1), pr(-20, 19, 56, 2), pr(-20, 17, 57, 2),
        # pr(-20, 16, 57, 1), pr(-20, 15, 57, 1), pr(-11, 14, 50, 1), pr(-12, 14, 1, 11),
        # pr(-14, 14, 17, 0), pr(-19, 14, 0, 7), pr(-20, 14, 0, 13), pr(-20, 14, 43, 1)]
        # dirty_rects = dirty_rects[len(dirty_rects) / 2:]
        # dirty_rects = dirty_rects[len(dirty_rects) / 2:]
        #
        # print(len(dirty_rects))
        #
        # dirty_rects.sort(key=lambda x: (-x.y, -x.x, x.w, x.h))

        c = [rec.copy() for rec in dirty_rects]

        # act
        actual_dirty_areas = self.func(c)
        # print(dirty_rects)
        # print(actual_dirty_areas)

        # verify
        for current in actual_dirty_areas:
            indices = current.collidelistall(actual_dirty_areas)
            self.assertEqual(1, len(indices),
                             "current {3}\n{0} in \n{1} \noriginal \n{2}".format(indices, actual_dirty_areas,
                                                                                 dirty_rects, current))
            self.assertEqual(actual_dirty_areas.index(current), indices[0],
                             "current {0}\noriginal {1}".format(current, actual_dirty_areas))

    def __test_collide_list_all(self):
        # arrange
        ri = random.randint
        dirty_rects = []
        w = 20
        h = 20
        step = 50
        for i in range(1000):
            r = Rect(ri(-w, w), i * step + ri(-h, h), ri(0, w), ri(0, h))
            r.normalize()
            dirty_rects.append(r)

        # act / verify
        ada = dirty_rects
        for current in dirty_rects:
            indices = []
            for r in dirty_rects:
                if current == r:
                    continue
                if current.colliderect(r):
                    indices.append(dirty_rects.index(current))
                else:
                    if current == r:
                        # self.fail("same should have collided \n{0} \n== \n{1}".format(current, r))
                        indices.append(dirty_rects.index(current))
            self.assertEqual(0, len(indices),
                             "current {3}\n{0} in \n{1} \noriginal \n{2}".format(indices, ada, dirty_rects, current))
            # self.assertEqual(ada.index(current), indices[0], "current {0}\noriginal {1}".format(current, ada))

            # AssertionError: current rect(-20, 14, 43, 1)>
            # [21, 22, 23, 24, 25, 26] in
            # [rect(-20, -20, 57, 6), rect(-20, -14, 57, 1), rect(-20, -13, 57, 1), rect(-20, -12, 57, 3), rect(-20, -9, 57, 2), rect(-20, -7, 57, 1), rect(-20, -6, 59, 2), rect(-20, -4, 59, 3), rect(-20, -1, 59, 1), rect(-20, 0, 59, 1), rect(-20, 1, 59, 1), rect(-20, 2, 59, 1), rect(-20, 3, 59, 1), rect(-20, 4, 59, 1), rect(-20, 5, 59, 1), rect(-20, 6, 59, 1), rect(-20, 7, 59, 2), rect(-20, 9, 59, 2), rect(-20, 11, 59, 1), rect(-20, 12, 59, 1), rect(-20, 13, 59, 1), rect(-20, 14, 43, 1), rect(-20, 14, 0, 13), rect(-19, 14, 0, 7), rect(-14, 14, 17, 0), rect(-12, 14, 1, 11), rect(-11, 14, 50, 1), rect(-20, 15, 57, 1), rect(-20, 16, 57, 1), rect(-20, 17, 57, 2), rect(-20, 19, 56, 2), rect(-20, 21, 56, 1), rect(-20, 22, 56, 2), rect(-20, 24, 56, 2), rect(-20, 26, 56, 1), rect(-19, 27, 55, 1), rect(-19, 28, 55, 1), rect(-19, 29, 55, 1), rect(-19, 30, 55, 1), rect(-19, 31, 54, 3), rect(-16, 34, 51, 1), rect(-16, 35, 51, 1), rect(-16, 36, 43, 1), rect(-12, 37, 5, 3), rect(12, 37, 9, 1), rect(12, 38, 5, 1)>]
            # original
            # [rect(12, 38, 5, 1), rect(12, 37, 9, 1), rect(-12, 37, 5, 3), rect(-16, 36, 43, 1), rect(-16, 35, 51, 1), rect(-16, 34, 51, 1), rect(-19, 31, 54, 3), rect(-19, 30, 55, 1), rect(-19, 29, 55, 1), rect(-19, 28, 55, 1), rect(-19, 27, 55, 1), rect(-20, 26, 56, 1), rect(-20, 24, 56, 2), rect(-20, 22, 56, 2), rect(-20, 21, 56, 1), rect(-20, 19, 56, 2), rect(-20, 17, 57, 2), rect(-20, 16, 57, 1), rect(-20, 15, 57, 1), rect(-11, 14, 50, 1), rect(-12, 14, 1, 11), rect(-14, 14, 17, 0), rect(-19, 14, 0, 7), rect(-20, 14, 0, 13), rect(-20, 14, 43, 1), rect(-20, 13, 59, 1), rect(-20, 12, 59, 1), rect(-20, 11, 59, 1), rect(-20, 9, 34, 0), rect(-20, 9, 59, 2), rect(-20, 7, 59, 2), rect(-20, 6, 59, 1), rect(-20, 5, 59, 1), rect(-20, 4, 59, 1), rect(-20, 3, 50, 0), rect(-20, 3, 59, 1), rect(-20, 2, 59, 1), rect(-20, 1, 59, 1), rect(-20, 0, 59, 1), rect(-20, -1, 59, 1), rect(-20, -4, 55, 0), rect(-20, -4, 59, 3), rect(-20, -6, 59, 2), rect(-20, -7, 57, 1), rect(-20, -9, 57, 0), rect(-20, -9, 57, 2), rect(-20, -12, 57, 3), rect(-20, -13, 57, 1), rect(-20, -14, 57, 1), rect(-20, -20, 57, 6)>]

            #
            # AssertionError: current rect(5, 144764, 1, 1)>
            # [275, 275] in
            # [rect(-2, 199992, 9, 8), rect(-13, 199639, 11, 7), rect(-39, 199449, 20, 9), rect(13, 199104, 18, 3), rect(6, 198911, 10, 16), rect(-17, 198646, 17, 5), rect(-9, 198535, 4, 19), rect(-13, 198458, 16, 1), rect(-12, 198189, 14, 9), rect(-14, 198168, 7, 14), rect(-6, 197771, 10, 10), rect(5, 197540, 5, 6), rect(-18, 197245, 7, 15), rect(5, 197192, 0, 18), rect(6, 197142, 10, 8), rect(6, 196849, 9, 4), rect(5, 196436, 3, 20), rect(-38, 196293, 19, 5), rect(3, 196287, 9, 9), rect(11, 195910, 16, 7), rect(-7, 195811, 13, 8), rect(-13, 195554, 14, 20), rect(-15, 195506, 20, 20), rect(-16, 195248, 15, 13), rect(-36, 194922, 16, 2), rect(7, 194860, 5, 16), rect(-15, 194571, 2, 2), rect(13, 194548, 4, 17), rect(5, 194168, 19, 20), rect(6, 193866, 2, 9), rect(10, 193859, 12, 4), rect(-30, 193471, 12, 18), rect(-9, 193270, 15, 14), rect(-7, 193129, 13, 3), rect(-4, 192893, 1, 5), rect(-22, 192837, 3, 20), rect(11, 192634, 9, 15), rect(-1, 192351, 20, 19), rect(-15, 192275, 1, 9), rect(-15, 192175, 7, 17), rect(-14, 191800, 11, 15), rect(-15, 191750, 2, 2), rect(-13, 191288, 15, 5), rect(5, 191282, 8, 10), rect(12, 190935, 3, 16), rect(8, 190808, 4, 9), rect(-11, 190598, 8, 16), rect(15, 190424, 8, 19), rect(-4, 190126, 4, 11), rect(-20, 189978, 2, 1), rect(-5, 189842, 10, 8), rect(6, 189750, 3, 9), rect(3, 189464, 1, 8), rect(-17, 189021, 17, 14), rect(-25, 188997, 9, 16), rect(5, 188814, 14, 2), rect(-21, 188480, 20, 9), rect(-8, 188311, 17, 15), rect(17, 188270, 20, 12), rect(-8, 188032, 20, 12), rect(0, 187861, 4, 11), rect(-10, 187605, 18, 13), rect(7, 187549, 0, 4), rect(-10, 187151, 6, 11), rect(7, 187057, 1, 4), rect(-18, 186623, 15, 18), rect(-11, 186484, 10, 9), rect(-26, 186443, 20, 19), rect(10, 186355, 0, 19), rect(-31, 185899, 12, 18), rect(19, 185784, 1, 5), rect(-2, 185558, 10, 3), rect(-3, 185452, 15, 9), rect(-13, 185334, 11, 7), rect(5, 185062, 19, 4), rect(2, 184637, 8, 11), rect(12, 184518, 15, 17), rect(-9, 184441, 16, 3), rect(12, 184062, 9, 5), rect(-15, 183863, 15, 11), rect(-19, 183655, 9, 11), rect(-31, 183549, 11, 19), rect(4, 183486, 14, 8), rect(-19, 183203, 14, 7), rect(4, 182972, 8, 16), rect(-21, 182943, 11, 6), rect(-18, 182581, 7, 16), rect(-17, 182327, 4, 8), rect(5, 182173, 11, 11), rect(-1, 182099, 14, 11), rect(-7, 181600, 15, 7), rect(1, 181552, 8, 7), rect(-7, 181413, 13, 16), rect(-17, 181013, 0, 11), rect(-18, 180846, 8, 13), rect(16, 180814, 4, 7), rect(-25, 180441, 18, 15), rect(-17, 180316, 6, 20), rect(-26, 180117, 17, 16), rect(-15, 180034, 15, 10), rect(-3, 179902, 19, 5), rect(5, 179693, 11, 3), rect(-36, 179368, 19, 14), rect(4, 179280, 20, 1), rect(13, 178857, 4, 8), rect(-9, 178714, 12, 3), rect(-12, 178632, 18, 2), rect(-4, 178325, 9, 12), rect(17, 178246, 7, 7), rect(10, 178174, 8, 3), rect(0, 177758, 17, 2), rect(-15, 177439, 6, 10), rect(20, 177400, 7, 4), rect(-18, 177097, 12, 10), rect(-13, 176888, 10, 19), rect(-20, 176737, 11, 10), rect(-10, 176612, 13, 13), rect(-7, 176306, 5, 20), rect(2, 176214, 10, 10), rect(-7, 175954, 16, 15), rect(15, 175931, 1, 0), rect(-22, 175453, 2, 13), rect(9, 175381, 0, 17), rect(1, 175361, 6, 1), rect(-8, 175173, 8, 12), rect(0, 174755, 5, 19), rect(-4, 174478, 16, 10), rect(-10, 174451, 14, 13), rect(14, 174344, 7, 14), rect(-20, 173821, 1, 5), rect(-18, 173724, 5, 20), rect(-1, 173718, 13, 8), rect(-17, 173343, 3, 10), rect(-1, 173181, 8, 14), rect(-12, 172952, 9, 9), rect(6, 172791, 16, 1), rect(-3, 172700, 11, 5), rect(-29, 172238, 16, 9), rect(-11, 172159, 15, 11), rect(-3, 172089, 11, 20), rect(9, 171734, 3, 12), rect(-5, 171717, 4, 20), rect(12, 171419, 5, 1), rect(-2, 171384, 14, 9), rect(-13, 170805, 12, 18), rect(10, 170629, 17, 13), rect(-13, 170601, 11, 16), rect(-27, 170340, 19, 15), rect(-28, 170244, 11, 10), rect(15, 170001, 1, 1), rect(-17, 169725, 7, 13), rect(15, 169574, 3, 15), rect(-16, 169431, 15, 9), rect(19, 169224, 6, 6), rect(4, 168909, 20, 4), rect(-15, 168754, 7, 14), rect(-7, 168588, 20, 4), rect(-30, 168523, 20, 19), rect(-3, 168099, 0, 17), rect(-10, 168039, 9, 16), rect(-38, 167826, 20, 11), rect(-27, 167632, 10, 1), rect(4, 167310, 4, 9), rect(-19, 167213, 15, 17), rect(-3, 166960, 9, 1), rect(-20, 166922, 11, 5), rect(3, 166488, 7, 7), rect(-3, 166370, 18, 9), rect(-19, 166015, 9, 7), rect(-10, 165898, 13, 18), rect(-2, 165820, 9, 18), rect(-17, 165639, 15, 14), rect(-9, 165580, 4, 7), rect(3, 165292, 7, 14), rect(-10, 164858, 15, 10), rect(9, 164742, 11, 16), rect(-6, 164725, 10, 4), rect(-12, 164517, 3, 2), rect(-1, 164235, 4, 19), rect(-17, 164167, 7, 13), rect(-9, 163790, 17, 17), rect(-7, 163638, 20, 10), rect(4, 163296, 9, 10), rect(14, 163188, 0, 18), rect(-7, 162830, 18, 10), rect(1, 162789, 13, 20), rect(-25, 162568, 6, 1), rect(-15, 162307, 10, 8), rect(3, 162192, 5, 15), rect(-6, 162023, 20, 16), rect(-18, 161602, 11, 16), rect(-22, 161443, 15, 16), rect(9, 161227, 18, 5), rect(6, 161059, 9, 5), rect(19, 161021, 11, 14), rect(-15, 160758, 16, 3), rect(-1, 160597, 14, 12), rect(-3, 160463, 11, 14), rect(16, 160303, 1, 14), rect(12, 160158, 6, 17), rect(17, 159853, 0, 13), rect(-16, 159780, 10, 15), rect(11, 159211, 17, 3), rect(17, 159145, 1, 5), rect(-32, 159014, 15, 11), rect(-17, 158781, 20, 10), rect(-1, 158726, 5, 3), rect(13, 158498, 5, 10), rect(-3, 158079, 10, 12), rect(-3, 158037, 6, 17), rect(-3, 157713, 5, 9), rect(-9, 157452, 13, 6), rect(-1, 157349, 13, 12), rect(10, 157310, 1, 18), rect(8, 156956, 2, 3), rect(-10, 156743, 16, 3), rect(-33, 156689, 18, 9), rect(-4, 156349, 2, 18), rect(-1, 156238, 1, 14), rect(9, 156178, 12, 8), rect(-15, 155806, 7, 14), rect(-15, 155632, 0, 20), rect(7, 155519, 14, 1), rect(-9, 155176, 2, 8), rect(-3, 155096, 12, 14), rect(-16, 154784, 16, 3), rect(8, 154561, 8, 1), rect(-1, 154373, 2, 16), rect(-27, 154275, 19, 7), rect(-5, 154058, 9, 19), rect(-28, 153962, 19, 11), rect(-10, 153601, 9, 1), rect(-34, 153363, 15, 11), rect(-17, 153303, 15, 20), rect(-32, 153045, 12, 10), rect(-17, 152610, 5, 3), rect(-12, 152607, 15, 10), rect(4, 152545, 12, 13), rect(-26, 152316, 11, 13), rect(1, 152197, 19, 2), rect(5, 151804, 15, 9), rect(-6, 151514, 2, 0), rect(12, 151466, 20, 15), rect(-13, 151021, 15, 8), rect(-2, 150927, 15, 3), rect(-33, 150732, 13, 9), rect(-13, 150681, 14, 11), rect(3, 150361, 13, 6), rect(19, 150325, 20, 17), rect(-19, 150093, 20, 12), rect(18, 149806, 13, 10), rect(4, 149525, 5, 4), rect(-2, 149302, 4, 8), rect(12, 149124, 6, 14), rect(-5, 149037, 11, 4), rect(-1, 148784, 20, 20), rect(7, 148515, 20, 19), rect(11, 148297, 0, 5), rect(14, 148089, 6, 10), rect(-13, 148080, 14, 9), rect(-14, 147636, 13, 11), rect(17, 147443, 15, 10), rect(12, 147435, 4, 5), rect(-30, 147133, 19, 18), rect(-13, 146892, 9, 18), rect(11, 146791, 19, 15), rect(10, 146580, 12, 15), rect(-11, 146522, 9, 1), rect(-18, 146273, 13, 11), rect(-13, 145995, 10, 5), rect(-15, 145679, 5, 14), rect(-7, 145652, 7, 16), rect(-13, 145259, 3, 14), rect(-5, 145252, 2, 12), rect(-9, 144923, 5, 18), rect(5, 144764, 1, 1), rect(5, 144762, 1, 9), rect(-20, 144241, 14, 19), rect(-20, 144199, 19, 19), rect(19, 144073, 16, 18), rect(-3, 143827, 1, 18), rect(-10, 143770, 3, 12), rect(0, 143366, 13, 12), rect(-3, 143218, 8, 4), rect(-16, 142994, 11, 10), rect(0, 142863, 16, 10), rect(9, 142554, 5, 7), rect(8, 142448, 16, 0), rect(-18, 142386, 2, 7), rect(-4, 141963, 17, 2), rect(-3, 141895, 1, 1), rect(16, 141771, 6, 18), rect(-17, 141337, 18, 20), rect(1, 141040, 11, 10), rect(19, 141019, 5, 3), rect(-21, 140958, 3, 20), rect(-16, 140643, 2, 0), rect(-26, 140271, 10, 6), rect(-32, 140216, 14, 14), rect(-4, 139984, 8, 12), rect(3, 139770, 16, 17), rect(-14, 139672, 7, 8), rect(16, 139377, 11, 6), rect(-18, 139111, 10, 15), rect(19, 139030, 3, 10), rect(-11, 138688, 13, 2), rect(-9, 138661, 7, 18), rect(-13, 138243, 11, 11), rect(-3, 138170, 2, 17), rect(0, 138115, 9, 2), rect(-6, 137868, 7, 11), rect(5, 137689, 6, 4), rect(11, 137324, 20, 5), rect(18, 137190, 16, 18), rect(15, 136957, 2, 12), rect(-6, 136925, 7, 6), rect(-17, 136668, 12, 5), rect(-35, 136437, 15, 0), rect(3, 136265, 11, 11), rect(5, 136183, 10, 11), rect(-21, 135744, 4, 17), rect(-7, 135560, 17, 5), rect(12, 135514, 9, 10), rect(4, 135209, 4, 9), rect(-6, 135149, 3, 20), rect(12, 134706, 20, 16), rect(2, 134476, 15, 20), rect(-19, 134443, 8, 18), rect(-17, 134394, 12, 5), rect(-6, 133874, 16, 8), rect(-29, 133847, 10, 1), rect(-16, 133589, 3, 17), rect(-9, 133273, 15, 14), rect(-10, 133191, 7, 13), rect(10, 132985, 8, 10), rect(-39, 132711, 19, 14), rect(-38, 132703, 19, 9), rect(-37, 132436, 17, 6), rect(-7, 132318, 0, 4), rect(-22, 131840, 14, 19), rect(6, 131600, 18, 12), rect(-8, 131547, 19, 11), rect(-9, 131503, 16, 9), rect(6, 131085, 3, 5), rect(-14, 131010, 3, 14), rect(6, 130816, 6, 14), rect(9, 130624, 15, 10), rect(19, 130560, 0, 12), rect(5, 130104, 8, 7), rect(-2, 129900, 7, 4), rect(6, 129817, 1, 18), rect(-7, 129380, 2, 20), rect(-15, 129260, 16, 15), rect(-20, 129102, 20, 13), rect(-20, 129083, 10, 12), rect(4, 128687, 14, 17), rect(13, 128678, 8, 20), rect(-16, 128344, 14, 19), rect(-2, 128161, 6, 20), rect(-17, 128003, 6, 11), rect(10, 127613, 8, 17), rect(7, 127595, 0, 13), rect(20, 127484, 4, 3), rect(-29, 127097, 16, 13), rect(-9, 127065, 11, 12), rect(4, 126614, 9, 9), rect(-7, 126560, 14, 17), rect(-8, 126410, 14, 2), rect(-16, 126300, 3, 10), rect(6, 125879, 4, 13), rect(-14, 125845, 10, 18), rect(9, 125568, 12, 14), rect(-17, 125489, 6, 6), rect(13, 125165, 4, 11), rect(-4, 125059, 17, 20), rect(-31, 124782, 18, 1), rect(-12, 124729, 9, 10), rect(13, 124557, 12, 5), rect(-6, 124263, 12, 7), rect(-5, 123980, 3, 12), rect(-17, 123624, 15, 4), rect(7, 123541, 10, 19), rect(-14, 123517, 3, 15), rect(3, 123210, 7, 15), rect(-39, 122993, 20, 19), rect(-25, 122915, 11, 20), rect(-2, 122610, 8, 16), rect(-1, 122290, 14, 13), rect(-20, 122192, 11, 11), rect(1, 122087, 6, 13), rect(10, 121683, 12, 8), rect(12, 121526, 4, 15), rect(-10, 121284, 5, 19), rect(-23, 121247, 6, 0), rect(-16, 121092, 14, 2), rect(-23, 120809, 7, 15), rect(-4, 120628, 1, 12), rect(16, 120556, 2, 5), rect(-4, 120375, 17, 12), rect(-28, 119924, 12, 15), rect(1, 119903, 16, 1), rect(-5, 119529, 1, 6), rect(9, 119347, 6, 9), rect(-10, 119342, 3, 16), rect(-5, 118822, 14, 12), rect(-11, 118705, 8, 8), rect(7, 118690, 8, 16), rect(-5, 118279, 15, 9), rect(17, 118138, 0, 19), rect(-13, 117898, 4, 10), rect(-10, 117805, 5, 12), rect(-20, 117696, 6, 9), rect(13, 117523, 15, 2), rect(-34, 117150, 18, 3), rect(10, 116984, 2, 6), rect(-5, 116873, 19, 2), rect(-2, 116568, 1, 11), rect(-19, 116426, 0, 0), rect(19, 116146, 12, 3), rect(-7, 116067, 18, 13), rect(13, 115810, 20, 14), rect(4, 115553, 7, 20), rect(-26, 115543, 14, 13), rect(9, 115040, 2, 13), rect(-15, 114937, 19, 20), rect(5, 114870, 20, 3), rect(15, 114691, 0, 17), rect(-17, 114511, 10, 17), rect(8, 114193, 10, 2), rect(-15, 114049, 3, 10), rect(-4, 113979, 2, 8), rect(13, 113571, 5, 11), rect(-14, 113430, 3, 6), rect(-14, 113271, 8, 13), rect(1, 113013, 11, 20), rect(1, 112943, 4, 20), rect(3, 112485, 3, 2), rect(-21, 112461, 1, 18), rect(-6, 112330, 6, 14), rect(-9, 112115, 17, 18), rect(-3, 111638, 2, 8), rect(2, 111600, 16, 3), rect(-1, 111577, 6, 6), rect(-19, 111070, 7, 5), rect(-1, 110928, 17, 11), rect(-14, 110917, 2, 7), rect(11, 110520, 8, 9), rect(-24, 110429, 14, 15), rect(7, 110241, 14, 8), rect(-10, 110015, 20, 4), rect(13, 109784, 13, 13), rect(-5, 109747, 10, 20), rect(-7, 109203, 10, 11), rect(-5, 109038, 14, 4), rect(11, 108955, 6, 2), rect(19, 108825, 10, 3), rect(-11, 108485, 15, 7), rect(-3, 108446, 17, 7), rect(-3, 108045, 18, 1), rect(-8, 107866, 7, 15), rect(-6, 107610, 15, 14), rect(-1, 107602, 0, 14), rect(14, 107286, 4, 15), rect(6, 107169, 17, 5), rect(-16, 106940, 17, 8), rect(-14, 106757, 3, 19), rect(-34, 106704, 14, 12), rect(17, 106439, 20, 20), rect(-7, 106068, 20, 8), rect(2, 105933, 2, 8), rect(-6, 105729, 17, 3), rect(8, 105687, 10, 0), rect(17, 105372, 3, 11), rect(10, 105180, 10, 20), rect(15, 104998, 0, 14), rect(-26, 104779, 9, 12), rect(-12, 104752, 14, 20), rect(4, 104486, 14, 4), rect(-7, 104068, 2, 5), rect(10, 103857, 10, 18), rect(-25, 103822, 18, 18), rect(1, 103574, 1, 1), rect(1, 103218, 4, 15), rect(-8, 103199, 6, 20), rect(-24, 102971, 16, 18), rect(1, 102739, 18, 9), rect(2, 102681, 5, 14), rect(13, 102322, 10, 4), rect(-4, 102298, 12, 16), rect(5, 102117, 6, 15), rect(-19, 101827, 7, 15), rect(-2, 101734, 5, 0), rect(-3, 101514, 0, 2), rect(-27, 101161, 13, 6), rect(12, 101022, 8, 10), rect(-9, 100627, 11, 19), rect(4, 100497, 7, 7), rect(13, 100415, 18, 17), rect(8, 100216, 15, 3), rect(-19, 100072, 3, 11), rect(-17, 99682, 17, 4), rect(9, 99610, 11, 4), rect(4, 99353, 17, 8), rect(18, 99247, 10, 3), rect(10, 98927, 13, 19), rect(-18, 98802, 9, 2), rect(-2, 98450, 19, 2), rect(-3, 98323, 5, 7), rect(-13, 98255, 4, 16), rect(-4, 97933, 14, 8), rect(-6, 97873, 10, 2), rect(-6, 97630, 12, 5), rect(-1, 97369, 1, 13), rect(-5, 97219, 10, 15), rect(-18, 97012, 20, 2), rect(-6, 96672, 7, 15), rect(12, 96444, 9, 12), rect(4, 96400, 11, 3), rect(3, 96310, 7, 1), rect(2, 96157, 11, 19), rect(11, 95824, 4, 5), rect(-24, 95457, 5, 12), rect(7, 95210, 6, 9), rect(-9, 95066, 20, 10), rect(-15, 94926, 2, 3), rect(-13, 94763, 7, 0), rect(-12, 94606, 6, 19), rect(18, 94202, 5, 1), rect(-19, 94082, 3, 19), rect(19, 94014, 16, 19), rect(-12, 93976, 4, 13), rect(-19, 93775, 18, 3), rect(12, 93399, 1, 19), rect(-1, 93203, 3, 20), rect(7, 93115, 10, 19), rect(-16, 92690, 16, 18), rect(20, 92519, 14, 2), rect(-9, 92425, 3, 10), rect(6, 92235, 4, 5), rect(16, 92113, 19, 12), rect(-29, 91701, 12, 3), rect(-13, 91647, 17, 20), rect(-35, 91534, 16, 8), rect(10, 91249, 16, 14), rect(9, 91142, 5, 17), rect(-26, 90908, 14, 3), rect(-12, 90680, 5, 20), rect(-31, 90308, 12, 12), rect(-6, 90222, 8, 0), rect(-2, 89988, 11, 5), rect(-13, 89940, 5, 15), rect(13, 89700, 5, 17), rect(-12, 89480, 1, 17), rect(-8, 89265, 1, 4), rect(-1, 89129, 19, 10), rect(0, 88784, 3, 19), rect(-11, 88507, 15, 13), rect(-14, 88282, 6, 9), rect(-19, 88087, 12, 10), rect(-6, 87873, 18, 3), rect(3, 87807, 7, 7), rect(-18, 87720, 6, 12), rect(13, 87524, 6, 17), rect(4, 87201, 0, 11), rect(12, 86819, 4, 1), rect(-20, 86797, 12, 19), rect(-28, 86761, 10, 9), rect(-6, 86405, 0, 20), rect(-28, 86143, 18, 13), rect(7, 85884, 20, 2), rect(-17, 85800, 11, 1), rect(-8, 85386, 10, 14), rect(-4, 85378, 11, 5), rect(-15, 85351, 18, 9), rect(19, 84918, 20, 5), rect(-15, 84596, 9, 17), rect(-36, 84577, 19, 16), rect(-1, 84347, 15, 19), rect(-27, 84250, 11, 19), rect(-23, 83797, 15, 16), rect(-6, 83669, 14, 15), rect(-14, 83482, 13, 10), rect(-15, 83327, 20, 18), rect(8, 83317, 11, 3), rect(-9, 83113, 12, 14), rect(-18, 82920, 7, 18), rect(-18, 82694, 14, 1), rect(4, 82546, 13, 17), rect(-33, 82101, 19, 15), rect(12, 82020, 2, 5), rect(-16, 81642, 11, 10), rect(18, 81538, 20, 14), rect(0, 81360, 12, 5), rect(-4, 81221, 3, 15), rect(16, 80938, 17, 19), rect(-9, 80703, 11, 2), rect(-8, 80428, 10, 6), rect(-6, 80363, 20, 13), rect(-1, 80341, 5, 14), rect(-14, 79962, 15, 8), rect(0, 79891, 18, 5), rect(-9, 79758, 3, 7), rect(-16, 79399, 20, 5), rect(7, 78993, 9, 8), rect(-15, 78974, 9, 18), rect(-13, 78673, 1, 8), rect(-9, 78466, 13, 10), rect(-11, 78244, 8, 10), rect(-7, 78009, 12, 1), rect(-21, 77827, 1, 5), rect(-11, 77681, 5, 14), rect(18, 77573, 13, 1), rect(-3, 77478, 5, 14), rect(-14, 77297, 9, 6), rect(-5, 77107, 15, 0), rect(-7, 76681, 10, 8), rect(-38, 76508, 20, 13), rect(-14, 76303, 6, 12), rect(-2, 76275, 17, 18), rect(0, 75832, 14, 9), rect(-11, 75594, 6, 8), rect(-15, 75478, 2, 18), rect(5, 75436, 19, 6), rect(-22, 75164, 4, 5), rect(4, 75061, 11, 5), rect(14, 74949, 8, 2), rect(-14, 74759, 12, 5), rect(-20, 74521, 13, 15), rect(-9, 74349, 3, 10), rect(-30, 73911, 18, 1), rect(14, 73828, 2, 7), rect(2, 73551, 9, 10), rect(-14, 73480, 11, 20), rect(-13, 73119, 0, 8), rect(-13, 73074, 4, 12), rect(-8, 72691, 20, 3), rect(-10, 72663, 4, 5), rect(16, 72497, 2, 7), rect(-16, 72083, 2, 5), rect(-6, 72035, 7, 1), rect(-9, 71882, 5, 8), rect(10, 71751, 9, 15), rect(17, 71323, 15, 12), rect(-4, 71124, 13, 9), rect(-10, 71051, 13, 18), rect(-18, 70971, 4, 5), rect(-17, 70514, 3, 16), rect(8, 70298, 5, 3), rect(4, 70013, 4, 13), rect(-1, 69940, 10, 19), rect(5, 69665, 2, 3), rect(-20, 69639, 8, 16), rect(-11, 69368, 10, 10), rect(14, 69323, 2, 18), rect(3, 68940, 1, 20), rect(-6, 68938, 8, 12), rect(-5, 68739, 1, 14), rect(-10, 68234, 13, 10), rect(-2, 68173, 13, 7), rect(-38, 67958, 19, 17), rect(-23, 67800, 9, 9), rect(4, 67650, 6, 1), rect(-4, 67556, 13, 11), rect(-9, 67201, 6, 9), rect(-17, 66939, 8, 16), rect(2, 66849, 7, 7), rect(-16, 66687, 17, 15), rect(15, 66244, 19, 7), rect(-10, 66199, 5, 18), rect(-3, 65882, 6, 18), rect(-29, 65847, 9, 11), rect(-28, 65554, 19, 10), rect(-13, 65454, 18, 6), rect(-19, 65281, 10, 11), rect(-9, 65100, 13, 2), rect(14, 64658, 18, 13), rect(8, 64409, 9, 5), rect(-12, 64263, 16, 18), rect(-11, 64227, 15, 5), rect(8, 63932, 7, 17), rect(-30, 63695, 12, 20), rect(-20, 63397, 10, 11), rect(-5, 63245, 5, 2), rect(-2, 63148, 7, 20), rect(4, 63127, 13, 1), rect(6, 62601, 8, 19), rect(-15, 62489, 9, 9), rect(-5, 62411, 5, 14), rect(-1, 62209, 3, 14), rect(16, 61965, 6, 4), rect(-1, 61923, 5, 13), rect(-9, 61555, 7, 17), rect(20, 61283, 10, 13), rect(-31, 61106, 16, 4), rect(-11, 60893, 6, 20), rect(-19, 60758, 3, 15), rect(-16, 60448, 9, 18), rect(-13, 60206, 18, 16), rect(12, 60147, 1, 11), rect(-32, 60049, 12, 3), rect(7, 59963, 1, 9), rect(6, 59533, 7, 8), rect(14, 59496, 16, 8), rect(-4, 59203, 14, 8), rect(-9, 59178, 0, 8), rect(14, 58925, 15, 11), rect(17, 58457, 12, 5), rect(10, 58415, 18, 13), rect(-8, 58030, 9, 4), rect(-24, 57964, 14, 16), rect(-9, 57862, 20, 8), rect(-10, 57450, 3, 6), rect(-9, 57284, 6, 6), rect(-6, 57118, 9, 7), rect(-9, 57069, 8, 19), rect(-1, 56694, 20, 19), rect(8, 56660, 17, 20), rect(-9, 56337, 0, 11), rect(2, 56214, 17, 16), rect(-18, 55854, 5, 13), rect(20, 55716, 12, 18), rect(-11, 55605, 20, 3), rect(-10, 55411, 14, 4), rect(-4, 55170, 20, 6), rect(-10, 55164, 5, 18), rect(-19, 54943, 8, 7), rect(5, 54797, 5, 3), rect(-20, 54557, 1, 15), rect(16, 54183, 1, 20), rect(4, 53953, 3, 7), rect(-11, 53628, 1, 15), rect(1, 53462, 5, 20), rect(-28, 53363, 13, 16), rect(-14, 53080, 6, 14), rect(-12, 52931, 0, 9), rect(-8, 52812, 15, 9), rect(-29, 52642, 10, 9), rect(-14, 52433, 8, 14), rect(-25, 52175, 11, 7), rect(-1, 51969, 9, 17), rect(0, 51821, 18, 11), rect(0, 51595, 13, 8), rect(-17, 51586, 11, 2), rect(13, 51186, 0, 6), rect(-6, 51120, 18, 7), rect(0, 50749, 3, 18), rect(-30, 50687, 13, 2), rect(6, 50561, 2, 10), rect(-4, 50227, 8, 18), rect(18, 49959, 3, 16), rect(-10, 49725, 18, 19), rect(-12, 49659, 18, 5), rect(3, 49256, 3, 2), rect(10, 49105, 3, 12), rect(2, 48889, 8, 17), rect(9, 48836, 0, 17), rect(-14, 48458, 17, 20), rect(-10, 48395, 15, 9), rect(-25, 48355, 19, 4), rect(10, 47914, 8, 9), rect(-11, 47794, 7, 20), rect(19, 47724, 20, 18), rect(17, 47538, 1, 5), rect(-8, 47248, 6, 10), rect(1, 46868, 6, 5), rect(-2, 46857, 15, 8), rect(-26, 46613, 19, 2), rect(-22, 46555, 12, 13), rect(-20, 46060, 13, 16), rect(2, 45886, 15, 10), rect(-5, 45600, 18, 12), rect(15, 45573, 10, 19), rect(-15, 45258, 1, 15), rect(-8, 45240, 17, 10), rect(12, 45048, 10, 19), rect(-13, 44720, 12, 13), rect(-21, 44689, 2, 3), rect(-10, 44188, 19, 14), rect(7, 44147, 18, 2), rect(-4, 44145, 4, 10), rect(-4, 43618, 9, 7), rect(-3, 43481, 5, 20), rect(-17, 43361, 1, 4), rect(-12, 43078, 14, 3), rect(5, 43030, 3, 5), rect(1, 42637, 15, 8), rect(12, 42450, 5, 0), rect(-3, 42399, 10, 12), rect(14, 42155, 16, 8), rect(5, 42150, 13, 10), rect(-18, 41955, 13, 8), rect(-16, 41694, 1, 5), rect(-16, 41217, 19, 19), rect(-1, 41088, 7, 8), rect(13, 40943, 4, 11), rect(-10, 40618, 20, 12), rect(-4, 40472, 11, 10), rect(-17, 40431, 7, 10), rect(18, 40089, 4, 1), rect(11, 39934, 7, 14), rect(12, 39597, 13, 12), rect(-1, 39525, 2, 1), rect(-2, 39403, 2, 17), rect(-5, 39274, 20, 7), rect(-28, 39056, 20, 19), rect(-9, 38611, 20, 2), rect(-13, 38511, 2, 13), rect(-22, 38442, 14, 20), rect(19, 38081, 8, 15), rect(1, 37916, 9, 3), rect(-21, 37888, 12, 6), rect(14, 37778, 2, 8), rect(15, 37281, 18, 15), rect(12, 37086, 8, 14), rect(1, 37044, 4, 2), rect(15, 36750, 4, 6), rect(-6, 36410, 14, 10), rect(9, 36328, 1, 3), rect(1, 36116, 1, 13), rect(6, 35949, 3, 2), rect(-31, 35847, 16, 8), rect(-4, 35688, 10, 8), rect(-20, 35410, 9, 11), rect(6, 35049, 11, 9), rect(11, 34939, 14, 2), rect(-1, 34858, 14, 7), rect(-28, 34598, 16, 17), rect(-10, 34287, 17, 8), rect(5, 34261, 6, 8), rect(12, 33970, 7, 8), rect(-21, 33661, 9, 19), rect(-2, 33656, 19, 20), rect(-18, 33378, 13, 7), rect(18, 33275, 12, 2), rect(9, 33149, 8, 20), rect(11, 32742, 3, 1), rect(-11, 32717, 4, 7), rect(15, 32246, 19, 4), rect(-10, 32241, 8, 11), rect(-14, 31904, 10, 14), rect(-3, 31725, 12, 14), rect(-14, 31556, 5, 12), rect(-20, 31506, 1, 18), rect(-11, 31183, 16, 1), rect(-6, 30840, 2, 15), rect(4, 30649, 9, 9), rect(-36, 30576, 19, 13), rect(20, 30383, 17, 14), rect(-2, 30234, 2, 19), rect(-3, 30008, 13, 14), rect(-16, 29680, 14, 18), rect(19, 29412, 10, 1), rect(-9, 29361, 5, 13), rect(-20, 29262, 9, 2), rect(11, 29145, 9, 19), rect(-5, 28866, 6, 11), rect(-14, 28602, 6, 3), rect(13, 28353, 10, 1), rect(1, 28266, 2, 3), rect(6, 28177, 0, 0), rect(-33, 27872, 20, 5), rect(-11, 27422, 7, 13), rect(-22, 27285, 6, 9), rect(16, 27248, 10, 1), rect(10, 27121, 4, 1), rect(7, 26693, 11, 18), rect(5, 26659, 2, 14), rect(-4, 26551, 12, 3), rect(-11, 26332, 3, 17), rect(-25, 25925, 11, 10), rect(15, 25813, 6, 11), rect(-16, 25503, 20, 11), rect(-17, 25494, 4, 6), rect(6, 25191, 9, 8), rect(5, 25080, 11, 10), rect(14, 24836, 16, 19), rect(2, 24768, 13, 11), rect(-11, 24387, 2, 1), rect(-18, 24188, 8, 7), rect(2, 24116, 12, 6), rect(-5, 23972, 7, 2), rect(-22, 23506, 18, 18), rect(-5, 23473, 20, 14), rect(-10, 23035, 5, 20), rect(16, 22885, 3, 20), rect(-6, 22820, 13, 5), rect(-19, 22440, 12, 10), rect(-12, 22405, 14, 20), rect(-23, 22314, 9, 5), rect(-17, 22110, 0, 15), rect(2, 21957, 18, 12), rect(-10, 21485, 3, 20), rect(-1, 21371, 19, 5), rect(-13, 21096, 13, 2), rect(-36, 20923, 20, 20), rect(-31, 20859, 16, 20), rect(2, 20430, 5, 13), rect(-26, 20349, 17, 14), rect(-13, 20268, 13, 6), rect(-27, 20130, 15, 5), rect(-24, 19953, 10, 20), rect(-16, 19677, 1, 9), rect(-7, 19455, 2, 19), rect(-14, 19025, 14, 1), rect(-18, 18827, 11, 12), rect(-3, 18736, 6, 15), rect(-6, 18654, 11, 19), rect(-9, 18237, 10, 2), rect(-17, 18002, 8, 1), rect(12, 17811, 1, 18), rect(16, 17622, 2, 8), rect(3, 17543, 20, 18), rect(8, 17331, 15, 4), rect(-11, 17224, 7, 11), rect(-15, 17178, 3, 2), rect(-11, 16768, 17, 3), rect(-15, 16469, 18, 3), rect(-14, 16326, 12, 8), rect(-9, 16270, 20, 17), rect(-8, 16115, 19, 11), rect(-10, 15923, 1, 19), rect(-10, 15719, 12, 9), rect(0, 15324, 16, 3), rect(-20, 15092, 1, 15), rect(-19, 15086, 15, 14), rect(-32, 14701, 18, 18), rect(9, 14642, 16, 5), rect(-14, 14278, 13, 14), rect(-18, 14141, 14, 4), rect(-20, 13852, 16, 18), rect(-8, 13674, 7, 15), rect(9, 13666, 11, 3), rect(18, 13502, 18, 2), rect(-12, 13147, 13, 13), rect(-14, 13146, 17, 3), rect(-4, 12795, 13, 3), rect(-16, 12751, 7, 2), rect(0, 12427, 15, 18), rect(-6, 12233, 10, 9), rect(7, 12033, 12, 13), rect(12, 11833, 6, 10), rect(-30, 11691, 16, 3), rect(-7, 11224, 14, 18), rect(-34, 11223, 17, 15), rect(-5, 11063, 9, 20), rect(-2, 10785, 15, 12), rect(-16, 10639, 14, 2), rect(-10, 10398, 14, 16), rect(5, 10070, 12, 17), rect(3, 9784, 12, 16), rect(-39, 9713, 19, 2), rect(14, 9540, 1, 2), rect(-10, 9302, 19, 11), rect(17, 9210, 10, 9), rect(12, 9177, 14, 4), rect(-27, 8589, 17, 19), rect(-35, 8438, 15, 6), rect(-1, 8416, 19, 2), rect(-5, 8133, 12, 18), rect(-12, 7959, 2, 19), rect(10, 7849, 0, 13), rect(8, 7544, 1, 20), rect(-18, 7403, 13, 15), rect(5, 7369, 8, 20), rect(-17, 6876, 6, 15), rect(5, 6645, 12, 12), rect(-29, 6467, 19, 18), rect(-40, 6383, 20, 19), rect(-9, 6172, 12, 20), rect(-6, 6038, 2, 18), rect(-11, 5662, 9, 16), rect(11, 5543, 10, 20), rect(-6, 5503, 20, 5), rect(16, 5222, 3, 5), rect(-22, 4941, 12, 8), rect(-12, 4842, 7, 1), rect(-3, 4508, 12, 10), rect(6, 4273, 7, 8), rect(10, 4000, 6, 19), rect(-31, 3961, 16, 14), rect(20, 3801, 11, 7), rect(-9, 3457, 9, 19), rect(-9, 3354, 12, 17), rect(-6, 3225, 8, 14), rect(18, 3086, 2, 2), rect(-12, 2610, 7, 12), rect(-21, 2506, 3, 10), rect(16, 2471, 12, 5), rect(3, 2068, 6, 15), rect(-12, 2024, 1, 10), rect(1, 1937, 10, 14), rect(-11, 1527, 10, 19), rect(-23, 1320, 17, 8), rect(-9, 1128, 4, 1), rect(-10, 1036, 15, 16), rect(-11, 956, 20, 13), rect(-20, 517, 4, 16), rect(-30, 491, 18, 11), rect(-31, 182, 18, 6), rect(14, -182, 9, 8)>]
            # original
            # [rect(-2, 199992, 9, 8), rect(-13, 199639, 11, 7), rect(-39, 199449, 20, 9), rect(13, 199104, 18, 3), rect(6, 198911, 10, 16), rect(-17, 198646, 17, 5), rect(-9, 198535, 4, 19), rect(-13, 198458, 16, 1), rect(-12, 198189, 14, 9), rect(-14, 198168, 7, 14), rect(-6, 197771, 10, 10), rect(5, 197540, 5, 6), rect(-18, 197245, 7, 15), rect(5, 197192, 0, 18), rect(6, 197142, 10, 8), rect(6, 196849, 9, 4), rect(5, 196436, 3, 20), rect(-38, 196293, 19, 5), rect(3, 196287, 9, 9), rect(11, 195910, 16, 7), rect(-7, 195811, 13, 8), rect(-13, 195554, 14, 20), rect(-15, 195506, 20, 20), rect(-16, 195248, 15, 13), rect(-36, 194922, 16, 2), rect(7, 194860, 5, 16), rect(-15, 194571, 2, 2), rect(13, 194548, 4, 17), rect(5, 194168, 19, 20), rect(6, 193866, 2, 9), rect(10, 193859, 12, 4), rect(-30, 193471, 12, 18), rect(-9, 193270, 15, 14), rect(-7, 193129, 13, 3), rect(-4, 192893, 1, 5), rect(-22, 192837, 3, 20), rect(11, 192634, 9, 15), rect(-1, 192351, 20, 19), rect(-15, 192275, 1, 9), rect(-15, 192175, 7, 17), rect(-14, 191800, 11, 15), rect(-15, 191750, 2, 2), rect(-13, 191288, 15, 5), rect(5, 191282, 8, 10), rect(12, 190935, 3, 16), rect(8, 190808, 4, 9), rect(-11, 190598, 8, 16), rect(15, 190424, 8, 19), rect(-4, 190126, 4, 11), rect(-20, 189978, 2, 1), rect(-5, 189842, 10, 8), rect(6, 189750, 3, 9), rect(3, 189464, 1, 8), rect(-17, 189021, 17, 14), rect(-25, 188997, 9, 16), rect(5, 188814, 14, 2), rect(-21, 188480, 20, 9), rect(-8, 188311, 17, 15), rect(17, 188270, 20, 12), rect(-8, 188032, 20, 12), rect(0, 187861, 4, 11), rect(-10, 187605, 18, 13), rect(7, 187549, 0, 4), rect(-10, 187151, 6, 11), rect(7, 187057, 1, 4), rect(-18, 186623, 15, 18), rect(-11, 186484, 10, 9), rect(-26, 186443, 20, 19), rect(10, 186355, 0, 19), rect(-31, 185899, 12, 18), rect(19, 185784, 1, 5), rect(-2, 185558, 10, 3), rect(-3, 185452, 15, 9), rect(-13, 185334, 11, 7), rect(5, 185062, 19, 4), rect(2, 184637, 8, 11), rect(12, 184518, 15, 17), rect(-9, 184441, 16, 3), rect(12, 184062, 9, 5), rect(-15, 183863, 15, 11), rect(-19, 183655, 9, 11), rect(-31, 183549, 11, 19), rect(4, 183486, 14, 8), rect(-19, 183203, 14, 7), rect(4, 182972, 8, 16), rect(-21, 182943, 11, 6), rect(-18, 182581, 7, 16), rect(-17, 182327, 4, 8), rect(5, 182173, 11, 11), rect(-1, 182099, 14, 11), rect(-7, 181600, 15, 7), rect(1, 181552, 8, 7), rect(-7, 181413, 13, 16), rect(-17, 181013, 0, 11), rect(-18, 180846, 8, 13), rect(16, 180814, 4, 7), rect(-25, 180441, 18, 15), rect(-17, 180316, 6, 20), rect(-26, 180117, 17, 16), rect(-15, 180034, 15, 10), rect(-3, 179902, 19, 5), rect(5, 179693, 11, 3), rect(-36, 179368, 19, 14), rect(4, 179280, 20, 1), rect(13, 178857, 4, 8), rect(-9, 178714, 12, 3), rect(-12, 178632, 18, 2), rect(-4, 178325, 9, 12), rect(17, 178246, 7, 7), rect(10, 178174, 8, 3), rect(0, 177758, 17, 2), rect(-15, 177439, 6, 10), rect(20, 177400, 7, 4), rect(-18, 177097, 12, 10), rect(-13, 176888, 10, 19), rect(-20, 176737, 11, 10), rect(-10, 176612, 13, 13), rect(-7, 176306, 5, 20), rect(2, 176214, 10, 10), rect(-7, 175954, 16, 15), rect(15, 175931, 1, 0), rect(-22, 175453, 2, 13), rect(9, 175381, 0, 17), rect(1, 175361, 6, 1), rect(-8, 175173, 8, 12), rect(0, 174755, 5, 19), rect(-4, 174478, 16, 10), rect(-10, 174451, 14, 13), rect(14, 174344, 7, 14), rect(-20, 173821, 1, 5), rect(-18, 173724, 5, 20), rect(-1, 173718, 13, 8), rect(-17, 173343, 3, 10), rect(-1, 173181, 8, 14), rect(-12, 172952, 9, 9), rect(6, 172791, 16, 1), rect(-3, 172700, 11, 5), rect(-29, 172238, 16, 9), rect(-11, 172159, 15, 11), rect(-3, 172089, 11, 20), rect(9, 171734, 3, 12), rect(-5, 171717, 4, 20), rect(12, 171419, 5, 1), rect(-2, 171384, 14, 9), rect(-13, 170805, 12, 18), rect(10, 170629, 17, 13), rect(-13, 170601, 11, 16), rect(-27, 170340, 19, 15), rect(-28, 170244, 11, 10), rect(15, 170001, 1, 1), rect(-17, 169725, 7, 13), rect(15, 169574, 3, 15), rect(-16, 169431, 15, 9), rect(19, 169224, 6, 6), rect(4, 168909, 20, 4), rect(-15, 168754, 7, 14), rect(-7, 168588, 20, 4), rect(-30, 168523, 20, 19), rect(-3, 168099, 0, 17), rect(-10, 168039, 9, 16), rect(-38, 167826, 20, 11), rect(-27, 167632, 10, 1), rect(4, 167310, 4, 9), rect(-19, 167213, 15, 17), rect(-3, 166960, 9, 1), rect(-20, 166922, 11, 5), rect(3, 166488, 7, 7), rect(-3, 166370, 18, 9), rect(-19, 166015, 9, 7), rect(-10, 165898, 13, 18), rect(-2, 165820, 9, 18), rect(-17, 165639, 15, 14), rect(-9, 165580, 4, 7), rect(3, 165292, 7, 14), rect(-10, 164858, 15, 10), rect(9, 164742, 11, 16), rect(-6, 164725, 10, 4), rect(-12, 164517, 3, 2), rect(-1, 164235, 4, 19), rect(-17, 164167, 7, 13), rect(-9, 163790, 17, 17), rect(-7, 163638, 20, 10), rect(4, 163296, 9, 10), rect(14, 163188, 0, 18), rect(-7, 162830, 18, 10), rect(1, 162789, 13, 20), rect(-25, 162568, 6, 1), rect(-15, 162307, 10, 8), rect(3, 162192, 5, 15), rect(-6, 162023, 20, 16), rect(-18, 161602, 11, 16), rect(-22, 161443, 15, 16), rect(9, 161227, 18, 5), rect(6, 161059, 9, 5), rect(19, 161021, 11, 14), rect(-15, 160758, 16, 3), rect(-1, 160597, 14, 12), rect(-3, 160463, 11, 14), rect(16, 160303, 1, 14), rect(12, 160158, 6, 17), rect(17, 159853, 0, 13), rect(-16, 159780, 10, 15), rect(11, 159211, 17, 3), rect(17, 159145, 1, 5), rect(-32, 159014, 15, 11), rect(-17, 158781, 20, 10), rect(-1, 158726, 5, 3), rect(13, 158498, 5, 10), rect(-3, 158079, 10, 12), rect(-3, 158037, 6, 17), rect(-3, 157713, 5, 9), rect(-9, 157452, 13, 6), rect(-1, 157349, 13, 12), rect(10, 157310, 1, 18), rect(8, 156956, 2, 3), rect(-10, 156743, 16, 3), rect(-33, 156689, 18, 9), rect(-4, 156349, 2, 18), rect(-1, 156238, 1, 14), rect(9, 156178, 12, 8), rect(-15, 155806, 7, 14), rect(-15, 155632, 0, 20), rect(7, 155519, 14, 1), rect(-9, 155176, 2, 8), rect(-3, 155096, 12, 14), rect(-16, 154784, 16, 3), rect(8, 154561, 8, 1), rect(-1, 154373, 2, 16), rect(-27, 154275, 19, 7), rect(-5, 154058, 9, 19), rect(-28, 153962, 19, 11), rect(-10, 153601, 9, 1), rect(-34, 153363, 15, 11), rect(-17, 153303, 15, 20), rect(-32, 153045, 12, 10), rect(-17, 152610, 5, 3), rect(-12, 152607, 15, 10), rect(4, 152545, 12, 13), rect(-26, 152316, 11, 13), rect(1, 152197, 19, 2), rect(5, 151804, 15, 9), rect(-6, 151514, 2, 0), rect(12, 151466, 20, 15), rect(-13, 151021, 15, 8), rect(-2, 150927, 15, 3), rect(-33, 150732, 13, 9), rect(-13, 150681, 14, 11), rect(3, 150361, 13, 6), rect(19, 150325, 20, 17), rect(-19, 150093, 20, 12), rect(18, 149806, 13, 10), rect(4, 149525, 5, 4), rect(-2, 149302, 4, 8), rect(12, 149124, 6, 14), rect(-5, 149037, 11, 4), rect(-1, 148784, 20, 20), rect(7, 148515, 20, 19), rect(11, 148297, 0, 5), rect(14, 148089, 6, 10), rect(-13, 148080, 14, 9), rect(-14, 147636, 13, 11), rect(17, 147443, 15, 10), rect(12, 147435, 4, 5), rect(-30, 147133, 19, 18), rect(-13, 146892, 9, 18), rect(11, 146791, 19, 15), rect(10, 146580, 12, 15), rect(-11, 146522, 9, 1), rect(-18, 146273, 13, 11), rect(-13, 145995, 10, 5), rect(-15, 145679, 5, 14), rect(-7, 145652, 7, 16), rect(-13, 145259, 3, 14), rect(-5, 145252, 2, 12), rect(-9, 144923, 5, 18), rect(5, 144764, 1, 1), rect(5, 144762, 1, 9), rect(-20, 144241, 14, 19), rect(-20, 144199, 19, 19), rect(19, 144073, 16, 18), rect(-3, 143827, 1, 18), rect(-10, 143770, 3, 12), rect(0, 143366, 13, 12), rect(-3, 143218, 8, 4), rect(-16, 142994, 11, 10), rect(0, 142863, 16, 10), rect(9, 142554, 5, 7), rect(8, 142448, 16, 0), rect(-18, 142386, 2, 7), rect(-4, 141963, 17, 2), rect(-3, 141895, 1, 1), rect(16, 141771, 6, 18), rect(-17, 141337, 18, 20), rect(1, 141040, 11, 10), rect(19, 141019, 5, 3), rect(-21, 140958, 3, 20), rect(-16, 140643, 2, 0), rect(-26, 140271, 10, 6), rect(-32, 140216, 14, 14), rect(-4, 139984, 8, 12), rect(3, 139770, 16, 17), rect(-14, 139672, 7, 8), rect(16, 139377, 11, 6), rect(-18, 139111, 10, 15), rect(19, 139030, 3, 10), rect(-11, 138688, 13, 2), rect(-9, 138661, 7, 18), rect(-13, 138243, 11, 11), rect(-3, 138170, 2, 17), rect(0, 138115, 9, 2), rect(-6, 137868, 7, 11), rect(5, 137689, 6, 4), rect(11, 137324, 20, 5), rect(18, 137190, 16, 18), rect(15, 136957, 2, 12), rect(-6, 136925, 7, 6), rect(-17, 136668, 12, 5), rect(-35, 136437, 15, 0), rect(3, 136265, 11, 11), rect(5, 136183, 10, 11), rect(-21, 135744, 4, 17), rect(-7, 135560, 17, 5), rect(12, 135514, 9, 10), rect(4, 135209, 4, 9), rect(-6, 135149, 3, 20), rect(12, 134706, 20, 16), rect(2, 134476, 15, 20), rect(-19, 134443, 8, 18), rect(-17, 134394, 12, 5), rect(-6, 133874, 16, 8), rect(-29, 133847, 10, 1), rect(-16, 133589, 3, 17), rect(-9, 133273, 15, 14), rect(-10, 133191, 7, 13), rect(10, 132985, 8, 10), rect(-39, 132711, 19, 14), rect(-38, 132703, 19, 9), rect(-37, 132436, 17, 6), rect(-7, 132318, 0, 4), rect(-22, 131840, 14, 19), rect(6, 131600, 18, 12), rect(-8, 131547, 19, 11), rect(-9, 131503, 16, 9), rect(6, 131085, 3, 5), rect(-14, 131010, 3, 14), rect(6, 130816, 6, 14), rect(9, 130624, 15, 10), rect(19, 130560, 0, 12), rect(5, 130104, 8, 7), rect(-2, 129900, 7, 4), rect(6, 129817, 1, 18), rect(-7, 129380, 2, 20), rect(-15, 129260, 16, 15), rect(-20, 129102, 20, 13), rect(-20, 129083, 10, 12), rect(4, 128687, 14, 17), rect(13, 128678, 8, 20), rect(-16, 128344, 14, 19), rect(-2, 128161, 6, 20), rect(-17, 128003, 6, 11), rect(10, 127613, 8, 17), rect(7, 127595, 0, 13), rect(20, 127484, 4, 3), rect(-29, 127097, 16, 13), rect(-9, 127065, 11, 12), rect(4, 126614, 9, 9), rect(-7, 126560, 14, 17), rect(-8, 126410, 14, 2), rect(-16, 126300, 3, 10), rect(6, 125879, 4, 13), rect(-14, 125845, 10, 18), rect(9, 125568, 12, 14), rect(-17, 125489, 6, 6), rect(13, 125165, 4, 11), rect(-4, 125059, 17, 20), rect(-31, 124782, 18, 1), rect(-12, 124729, 9, 10), rect(13, 124557, 12, 5), rect(-6, 124263, 12, 7), rect(-5, 123980, 3, 12), rect(-17, 123624, 15, 4), rect(7, 123541, 10, 19), rect(-14, 123517, 3, 15), rect(3, 123210, 7, 15), rect(-39, 122993, 20, 19), rect(-25, 122915, 11, 20), rect(-2, 122610, 8, 16), rect(-1, 122290, 14, 13), rect(-20, 122192, 11, 11), rect(1, 122087, 6, 13), rect(10, 121683, 12, 8), rect(12, 121526, 4, 15), rect(-10, 121284, 5, 19), rect(-23, 121247, 6, 0), rect(-16, 121092, 14, 2), rect(-23, 120809, 7, 15), rect(-4, 120628, 1, 12), rect(16, 120556, 2, 5), rect(-4, 120375, 17, 12), rect(-28, 119924, 12, 15), rect(1, 119903, 16, 1), rect(-5, 119529, 1, 6), rect(9, 119347, 6, 9), rect(-10, 119342, 3, 16), rect(-5, 118822, 14, 12), rect(-11, 118705, 8, 8), rect(7, 118690, 8, 16), rect(-5, 118279, 15, 9), rect(17, 118138, 0, 19), rect(-13, 117898, 4, 10), rect(-10, 117805, 5, 12), rect(-20, 117696, 6, 9), rect(13, 117523, 15, 2), rect(-34, 117150, 18, 3), rect(10, 116984, 2, 6), rect(-5, 116873, 19, 2), rect(-2, 116568, 1, 11), rect(-19, 116426, 0, 0), rect(19, 116146, 12, 3), rect(-7, 116067, 18, 13), rect(13, 115810, 20, 14), rect(4, 115553, 7, 20), rect(-26, 115543, 14, 13), rect(9, 115040, 2, 13), rect(-15, 114937, 19, 20), rect(5, 114870, 20, 3), rect(15, 114691, 0, 17), rect(-17, 114511, 10, 17), rect(8, 114193, 10, 2), rect(-15, 114049, 3, 10), rect(-4, 113979, 2, 8), rect(13, 113571, 5, 11), rect(-14, 113430, 3, 6), rect(-14, 113271, 8, 13), rect(1, 113013, 11, 20), rect(1, 112943, 4, 20), rect(3, 112485, 3, 2), rect(-21, 112461, 1, 18), rect(-6, 112330, 6, 14), rect(-9, 112115, 17, 18), rect(-3, 111638, 2, 8), rect(2, 111600, 16, 3), rect(-1, 111577, 6, 6), rect(-19, 111070, 7, 5), rect(-1, 110928, 17, 11), rect(-14, 110917, 2, 7), rect(11, 110520, 8, 9), rect(-24, 110429, 14, 15), rect(7, 110241, 14, 8), rect(-10, 110015, 20, 4), rect(13, 109784, 13, 13), rect(-5, 109747, 10, 20), rect(-7, 109203, 10, 11), rect(-5, 109038, 14, 4), rect(11, 108955, 6, 2), rect(19, 108825, 10, 3), rect(-11, 108485, 15, 7), rect(-3, 108446, 17, 7), rect(-3, 108045, 18, 1), rect(-8, 107866, 7, 15), rect(-6, 107610, 15, 14), rect(-1, 107602, 0, 14), rect(14, 107286, 4, 15), rect(6, 107169, 17, 5), rect(-16, 106940, 17, 8), rect(-14, 106757, 3, 19), rect(-34, 106704, 14, 12), rect(17, 106439, 20, 20), rect(-7, 106068, 20, 8), rect(2, 105933, 2, 8), rect(-6, 105729, 17, 3), rect(8, 105687, 10, 0), rect(17, 105372, 3, 11), rect(10, 105180, 10, 20), rect(15, 104998, 0, 14), rect(-26, 104779, 9, 12), rect(-12, 104752, 14, 20), rect(4, 104486, 14, 4), rect(-7, 104068, 2, 5), rect(10, 103857, 10, 18), rect(-25, 103822, 18, 18), rect(1, 103574, 1, 1), rect(1, 103218, 4, 15), rect(-8, 103199, 6, 20), rect(-24, 102971, 16, 18), rect(1, 102739, 18, 9), rect(2, 102681, 5, 14), rect(13, 102322, 10, 4), rect(-4, 102298, 12, 16), rect(5, 102117, 6, 15), rect(-19, 101827, 7, 15), rect(-2, 101734, 5, 0), rect(-3, 101514, 0, 2), rect(-27, 101161, 13, 6), rect(12, 101022, 8, 10), rect(-9, 100627, 11, 19), rect(4, 100497, 7, 7), rect(13, 100415, 18, 17), rect(8, 100216, 15, 3), rect(-19, 100072, 3, 11), rect(-17, 99682, 17, 4), rect(9, 99610, 11, 4), rect(4, 99353, 17, 8), rect(18, 99247, 10, 3), rect(10, 98927, 13, 19), rect(-18, 98802, 9, 2), rect(-2, 98450, 19, 2), rect(-3, 98323, 5, 7), rect(-13, 98255, 4, 16), rect(-4, 97933, 14, 8), rect(-6, 97873, 10, 2), rect(-6, 97630, 12, 5), rect(-1, 97369, 1, 13), rect(-5, 97219, 10, 15), rect(-18, 97012, 20, 2), rect(-6, 96672, 7, 15), rect(12, 96444, 9, 12), rect(4, 96400, 11, 3), rect(3, 96310, 7, 1), rect(2, 96157, 11, 19), rect(11, 95824, 4, 5), rect(-24, 95457, 5, 12), rect(7, 95210, 6, 9), rect(-9, 95066, 20, 10), rect(-15, 94926, 2, 3), rect(-13, 94763, 7, 0), rect(-12, 94606, 6, 19), rect(18, 94202, 5, 1), rect(-19, 94082, 3, 19), rect(19, 94014, 16, 19), rect(-12, 93976, 4, 13), rect(-19, 93775, 18, 3), rect(12, 93399, 1, 19), rect(-1, 93203, 3, 20), rect(7, 93115, 10, 19), rect(-16, 92690, 16, 18), rect(20, 92519, 14, 2), rect(-9, 92425, 3, 10), rect(6, 92235, 4, 5), rect(16, 92113, 19, 12), rect(-29, 91701, 12, 3), rect(-13, 91647, 17, 20), rect(-35, 91534, 16, 8), rect(10, 91249, 16, 14), rect(9, 91142, 5, 17), rect(-26, 90908, 14, 3), rect(-12, 90680, 5, 20), rect(-31, 90308, 12, 12), rect(-6, 90222, 8, 0), rect(-2, 89988, 11, 5), rect(-13, 89940, 5, 15), rect(13, 89700, 5, 17), rect(-12, 89480, 1, 17), rect(-8, 89265, 1, 4), rect(-1, 89129, 19, 10), rect(0, 88784, 3, 19), rect(-11, 88507, 15, 13), rect(-14, 88282, 6, 9), rect(-19, 88087, 12, 10), rect(-6, 87873, 18, 3), rect(3, 87807, 7, 7), rect(-18, 87720, 6, 12), rect(13, 87524, 6, 17), rect(4, 87201, 0, 11), rect(12, 86819, 4, 1), rect(-20, 86797, 12, 19), rect(-28, 86761, 10, 9), rect(-6, 86405, 0, 20), rect(-28, 86143, 18, 13), rect(7, 85884, 20, 2), rect(-17, 85800, 11, 1), rect(-8, 85386, 10, 14), rect(-4, 85378, 11, 5), rect(-15, 85351, 18, 9), rect(19, 84918, 20, 5), rect(-15, 84596, 9, 17), rect(-36, 84577, 19, 16), rect(-1, 84347, 15, 19), rect(-27, 84250, 11, 19), rect(-23, 83797, 15, 16), rect(-6, 83669, 14, 15), rect(-14, 83482, 13, 10), rect(-15, 83327, 20, 18), rect(8, 83317, 11, 3), rect(-9, 83113, 12, 14), rect(-18, 82920, 7, 18), rect(-18, 82694, 14, 1), rect(4, 82546, 13, 17), rect(-33, 82101, 19, 15), rect(12, 82020, 2, 5), rect(-16, 81642, 11, 10), rect(18, 81538, 20, 14), rect(0, 81360, 12, 5), rect(-4, 81221, 3, 15), rect(16, 80938, 17, 19), rect(-9, 80703, 11, 2), rect(-8, 80428, 10, 6), rect(-6, 80363, 20, 13), rect(-1, 80341, 5, 14), rect(-14, 79962, 15, 8), rect(0, 79891, 18, 5), rect(-9, 79758, 3, 7), rect(-16, 79399, 20, 5), rect(7, 78993, 9, 8), rect(-15, 78974, 9, 18), rect(-13, 78673, 1, 8), rect(-9, 78466, 13, 10), rect(-11, 78244, 8, 10), rect(-7, 78009, 12, 1), rect(-21, 77827, 1, 5), rect(-11, 77681, 5, 14), rect(18, 77573, 13, 1), rect(-3, 77478, 5, 14), rect(-14, 77297, 9, 6), rect(-5, 77107, 15, 0), rect(-7, 76681, 10, 8), rect(-38, 76508, 20, 13), rect(-14, 76303, 6, 12), rect(-2, 76275, 17, 18), rect(0, 75832, 14, 9), rect(-11, 75594, 6, 8), rect(-15, 75478, 2, 18), rect(5, 75436, 19, 6), rect(-22, 75164, 4, 5), rect(4, 75061, 11, 5), rect(14, 74949, 8, 2), rect(-14, 74759, 12, 5), rect(-20, 74521, 13, 15), rect(-9, 74349, 3, 10), rect(-30, 73911, 18, 1), rect(14, 73828, 2, 7), rect(2, 73551, 9, 10), rect(-14, 73480, 11, 20), rect(-13, 73119, 0, 8), rect(-13, 73074, 4, 12), rect(-8, 72691, 20, 3), rect(-10, 72663, 4, 5), rect(16, 72497, 2, 7), rect(-16, 72083, 2, 5), rect(-6, 72035, 7, 1), rect(-9, 71882, 5, 8), rect(10, 71751, 9, 15), rect(17, 71323, 15, 12), rect(-4, 71124, 13, 9), rect(-10, 71051, 13, 18), rect(-18, 70971, 4, 5), rect(-17, 70514, 3, 16), rect(8, 70298, 5, 3), rect(4, 70013, 4, 13), rect(-1, 69940, 10, 19), rect(5, 69665, 2, 3), rect(-20, 69639, 8, 16), rect(-11, 69368, 10, 10), rect(14, 69323, 2, 18), rect(3, 68940, 1, 20), rect(-6, 68938, 8, 12), rect(-5, 68739, 1, 14), rect(-10, 68234, 13, 10), rect(-2, 68173, 13, 7), rect(-38, 67958, 19, 17), rect(-23, 67800, 9, 9), rect(4, 67650, 6, 1), rect(-4, 67556, 13, 11), rect(-9, 67201, 6, 9), rect(-17, 66939, 8, 16), rect(2, 66849, 7, 7), rect(-16, 66687, 17, 15), rect(15, 66244, 19, 7), rect(-10, 66199, 5, 18), rect(-3, 65882, 6, 18), rect(-29, 65847, 9, 11), rect(-28, 65554, 19, 10), rect(-13, 65454, 18, 6), rect(-19, 65281, 10, 11), rect(-9, 65100, 13, 2), rect(14, 64658, 18, 13), rect(8, 64409, 9, 5), rect(-12, 64263, 16, 18), rect(-11, 64227, 15, 5), rect(8, 63932, 7, 17), rect(-30, 63695, 12, 20), rect(-20, 63397, 10, 11), rect(-5, 63245, 5, 2), rect(-2, 63148, 7, 20), rect(4, 63127, 13, 1), rect(6, 62601, 8, 19), rect(-15, 62489, 9, 9), rect(-5, 62411, 5, 14), rect(-1, 62209, 3, 14), rect(16, 61965, 6, 4), rect(-1, 61923, 5, 13), rect(-9, 61555, 7, 17), rect(20, 61283, 10, 13), rect(-31, 61106, 16, 4), rect(-11, 60893, 6, 20), rect(-19, 60758, 3, 15), rect(-16, 60448, 9, 18), rect(-13, 60206, 18, 16), rect(12, 60147, 1, 11), rect(-32, 60049, 12, 3), rect(7, 59963, 1, 9), rect(6, 59533, 7, 8), rect(14, 59496, 16, 8), rect(-4, 59203, 14, 8), rect(-9, 59178, 0, 8), rect(14, 58925, 15, 11), rect(17, 58457, 12, 5), rect(10, 58415, 18, 13), rect(-8, 58030, 9, 4), rect(-24, 57964, 14, 16), rect(-9, 57862, 20, 8), rect(-10, 57450, 3, 6), rect(-9, 57284, 6, 6), rect(-6, 57118, 9, 7), rect(-9, 57069, 8, 19), rect(-1, 56694, 20, 19), rect(8, 56660, 17, 20), rect(-9, 56337, 0, 11), rect(2, 56214, 17, 16), rect(-18, 55854, 5, 13), rect(20, 55716, 12, 18), rect(-11, 55605, 20, 3), rect(-10, 55411, 14, 4), rect(-4, 55170, 20, 6), rect(-10, 55164, 5, 18), rect(-19, 54943, 8, 7), rect(5, 54797, 5, 3), rect(-20, 54557, 1, 15), rect(16, 54183, 1, 20), rect(4, 53953, 3, 7), rect(-11, 53628, 1, 15), rect(1, 53462, 5, 20), rect(-28, 53363, 13, 16), rect(-14, 53080, 6, 14), rect(-12, 52931, 0, 9), rect(-8, 52812, 15, 9), rect(-29, 52642, 10, 9), rect(-14, 52433, 8, 14), rect(-25, 52175, 11, 7), rect(-1, 51969, 9, 17), rect(0, 51821, 18, 11), rect(0, 51595, 13, 8), rect(-17, 51586, 11, 2), rect(13, 51186, 0, 6), rect(-6, 51120, 18, 7), rect(0, 50749, 3, 18), rect(-30, 50687, 13, 2), rect(6, 50561, 2, 10), rect(-4, 50227, 8, 18), rect(18, 49959, 3, 16), rect(-10, 49725, 18, 19), rect(-12, 49659, 18, 5), rect(3, 49256, 3, 2), rect(10, 49105, 3, 12), rect(2, 48889, 8, 17), rect(9, 48836, 0, 17), rect(-14, 48458, 17, 20), rect(-10, 48395, 15, 9), rect(-25, 48355, 19, 4), rect(10, 47914, 8, 9), rect(-11, 47794, 7, 20), rect(19, 47724, 20, 18), rect(17, 47538, 1, 5), rect(-8, 47248, 6, 10), rect(1, 46868, 6, 5), rect(-2, 46857, 15, 8), rect(-26, 46613, 19, 2), rect(-22, 46555, 12, 13), rect(-20, 46060, 13, 16), rect(2, 45886, 15, 10), rect(-5, 45600, 18, 12), rect(15, 45573, 10, 19), rect(-15, 45258, 1, 15), rect(-8, 45240, 17, 10), rect(12, 45048, 10, 19), rect(-13, 44720, 12, 13), rect(-21, 44689, 2, 3), rect(-10, 44188, 19, 14), rect(7, 44147, 18, 2), rect(-4, 44145, 4, 10), rect(-4, 43618, 9, 7), rect(-3, 43481, 5, 20), rect(-17, 43361, 1, 4), rect(-12, 43078, 14, 3), rect(5, 43030, 3, 5), rect(1, 42637, 15, 8), rect(12, 42450, 5, 0), rect(-3, 42399, 10, 12), rect(14, 42155, 16, 8), rect(5, 42150, 13, 10), rect(-18, 41955, 13, 8), rect(-16, 41694, 1, 5), rect(-16, 41217, 19, 19), rect(-1, 41088, 7, 8), rect(13, 40943, 4, 11), rect(-10, 40618, 20, 12), rect(-4, 40472, 11, 10), rect(-17, 40431, 7, 10), rect(18, 40089, 4, 1), rect(11, 39934, 7, 14), rect(12, 39597, 13, 12), rect(-1, 39525, 2, 1), rect(-2, 39403, 2, 17), rect(-5, 39274, 20, 7), rect(-28, 39056, 20, 19), rect(-9, 38611, 20, 2), rect(-13, 38511, 2, 13), rect(-22, 38442, 14, 20), rect(19, 38081, 8, 15), rect(1, 37916, 9, 3), rect(-21, 37888, 12, 6), rect(14, 37778, 2, 8), rect(15, 37281, 18, 15), rect(12, 37086, 8, 14), rect(1, 37044, 4, 2), rect(15, 36750, 4, 6), rect(-6, 36410, 14, 10), rect(9, 36328, 1, 3), rect(1, 36116, 1, 13), rect(6, 35949, 3, 2), rect(-31, 35847, 16, 8), rect(-4, 35688, 10, 8), rect(-20, 35410, 9, 11), rect(6, 35049, 11, 9), rect(11, 34939, 14, 2), rect(-1, 34858, 14, 7), rect(-28, 34598, 16, 17), rect(-10, 34287, 17, 8), rect(5, 34261, 6, 8), rect(12, 33970, 7, 8), rect(-21, 33661, 9, 19), rect(-2, 33656, 19, 20), rect(-18, 33378, 13, 7), rect(18, 33275, 12, 2), rect(9, 33149, 8, 20), rect(11, 32742, 3, 1), rect(-11, 32717, 4, 7), rect(15, 32246, 19, 4), rect(-10, 32241, 8, 11), rect(-14, 31904, 10, 14), rect(-3, 31725, 12, 14), rect(-14, 31556, 5, 12), rect(-20, 31506, 1, 18), rect(-11, 31183, 16, 1), rect(-6, 30840, 2, 15), rect(4, 30649, 9, 9), rect(-36, 30576, 19, 13), rect(20, 30383, 17, 14), rect(-2, 30234, 2, 19), rect(-3, 30008, 13, 14), rect(-16, 29680, 14, 18), rect(19, 29412, 10, 1), rect(-9, 29361, 5, 13), rect(-20, 29262, 9, 2), rect(11, 29145, 9, 19), rect(-5, 28866, 6, 11), rect(-14, 28602, 6, 3), rect(13, 28353, 10, 1), rect(1, 28266, 2, 3), rect(6, 28177, 0, 0), rect(-33, 27872, 20, 5), rect(-11, 27422, 7, 13), rect(-22, 27285, 6, 9), rect(16, 27248, 10, 1), rect(10, 27121, 4, 1), rect(7, 26693, 11, 18), rect(5, 26659, 2, 14), rect(-4, 26551, 12, 3), rect(-11, 26332, 3, 17), rect(-25, 25925, 11, 10), rect(15, 25813, 6, 11), rect(-16, 25503, 20, 11), rect(-17, 25494, 4, 6), rect(6, 25191, 9, 8), rect(5, 25080, 11, 10), rect(14, 24836, 16, 19), rect(2, 24768, 13, 11), rect(-11, 24387, 2, 1), rect(-18, 24188, 8, 7), rect(2, 24116, 12, 6), rect(-5, 23972, 7, 2), rect(-22, 23506, 18, 18), rect(-5, 23473, 20, 14), rect(-10, 23035, 5, 20), rect(16, 22885, 3, 20), rect(-6, 22820, 13, 5), rect(-19, 22440, 12, 10), rect(-12, 22405, 14, 20), rect(-23, 22314, 9, 5), rect(-17, 22110, 0, 15), rect(2, 21957, 18, 12), rect(-10, 21485, 3, 20), rect(-1, 21371, 19, 5), rect(-13, 21096, 13, 2), rect(-36, 20923, 20, 20), rect(-31, 20859, 16, 20), rect(2, 20430, 5, 13), rect(-26, 20349, 17, 14), rect(-13, 20268, 13, 6), rect(-27, 20130, 15, 5), rect(-24, 19953, 10, 20), rect(-16, 19677, 1, 9), rect(-7, 19455, 2, 19), rect(-14, 19025, 14, 1), rect(-18, 18827, 11, 12), rect(-3, 18736, 6, 15), rect(-6, 18654, 11, 19), rect(-9, 18237, 10, 2), rect(-17, 18002, 8, 1), rect(12, 17811, 1, 18), rect(16, 17622, 2, 8), rect(3, 17543, 20, 18), rect(8, 17331, 15, 4), rect(-11, 17224, 7, 11), rect(-15, 17178, 3, 2), rect(-11, 16768, 17, 3), rect(-15, 16469, 18, 3), rect(-14, 16326, 12, 8), rect(-9, 16270, 20, 17), rect(-8, 16115, 19, 11), rect(-10, 15923, 1, 19), rect(-10, 15719, 12, 9), rect(0, 15324, 16, 3), rect(-20, 15092, 1, 15), rect(-19, 15086, 15, 14), rect(-32, 14701, 18, 18), rect(9, 14642, 16, 5), rect(-14, 14278, 13, 14), rect(-18, 14141, 14, 4), rect(-20, 13852, 16, 18), rect(-8, 13674, 7, 15), rect(9, 13666, 11, 3), rect(18, 13502, 18, 2), rect(-12, 13147, 13, 13), rect(-14, 13146, 17, 3), rect(-4, 12795, 13, 3), rect(-16, 12751, 7, 2), rect(0, 12427, 15, 18), rect(-6, 12233, 10, 9), rect(7, 12033, 12, 13), rect(12, 11833, 6, 10), rect(-30, 11691, 16, 3), rect(-7, 11224, 14, 18), rect(-34, 11223, 17, 15), rect(-5, 11063, 9, 20), rect(-2, 10785, 15, 12), rect(-16, 10639, 14, 2), rect(-10, 10398, 14, 16), rect(5, 10070, 12, 17), rect(3, 9784, 12, 16), rect(-39, 9713, 19, 2), rect(14, 9540, 1, 2), rect(-10, 9302, 19, 11), rect(17, 9210, 10, 9), rect(12, 9177, 14, 4), rect(-27, 8589, 17, 19), rect(-35, 8438, 15, 6), rect(-1, 8416, 19, 2), rect(-5, 8133, 12, 18), rect(-12, 7959, 2, 19), rect(10, 7849, 0, 13), rect(8, 7544, 1, 20), rect(-18, 7403, 13, 15), rect(5, 7369, 8, 20), rect(-17, 6876, 6, 15), rect(5, 6645, 12, 12), rect(-29, 6467, 19, 18), rect(-40, 6383, 20, 19), rect(-9, 6172, 12, 20), rect(-6, 6038, 2, 18), rect(-11, 5662, 9, 16), rect(11, 5543, 10, 20), rect(-6, 5503, 20, 5), rect(16, 5222, 3, 5), rect(-22, 4941, 12, 8), rect(-12, 4842, 7, 1), rect(-3, 4508, 12, 10), rect(6, 4273, 7, 8), rect(10, 4000, 6, 19), rect(-31, 3961, 16, 14), rect(20, 3801, 11, 7), rect(-9, 3457, 9, 19), rect(-9, 3354, 12, 17), rect(-6, 3225, 8, 14), rect(18, 3086, 2, 2), rect(-12, 2610, 7, 12), rect(-21, 2506, 3, 10), rect(16, 2471, 12, 5), rect(3, 2068, 6, 15), rect(-12, 2024, 1, 10), rect(1, 1937, 10, 14), rect(-11, 1527, 10, 19), rect(-23, 1320, 17, 8), rect(-9, 1128, 4, 1), rect(-10, 1036, 15, 16), rect(-11, 956, 20, 13), rect(-20, 517, 4, 16), rect(-30, 491, 18, 11), rect(-31, 182, 18, 6), rect(14, -182, 9, 8)>]


            # AssertionError: current rect(-15, 182, 17, 4)>
            # [9, 10] in
            # [rect(8, -25, 11, 10), rect(-7, 18, 6, 1), rect(-16, 44, 1, 7), rect(-3, 44, 6, 10), rect(-23, 62, 6, 10), rect(0, 89, 2, 13), rect(5, 129, 8, 6), rect(2, 135, 14, 6), rect(-15, 170, 12, 12), rect(-15, 182, 17, 4), rect(-13, 183, 15, 4)>]
            # original
            # [rect(-13, 182, 15, 5), rect(-15, 170, 12, 13), rect(2, 135, 14, 6), rect(5, 129, 8, 6), rect(0, 89, 2, 13), rect(-23, 62, 6, 10), rect(-3, 44, 6, 10), rect(-16, 44, 1, 7), rect(-7, 18, 6, 1), rect(8, -25, 11, 10)>]

            # AssertionError: current rect(-3, 34, 12, 13)>
            # [1, 3] in
            # [rect(14, 19, 10, 16), rect(-3, 34, 12, 13), rect(12, 37, 14, 9), rect(-18, 41, 18, 15), rect(17, 86, 7, 6), rect(-21, 113, 4, 4), rect(-35, 120, 15, 17), rect(-7, 157, 13, 8), rect(-27, 162, 7, 17), rect(-14, 194, 9, 7)>]
            # original
            # [rect(-14, 194, 9, 7), rect(-27, 162, 7, 17), rect(-7, 157, 13, 8), rect(-35, 120, 15, 17), rect(-21, 113, 4, 4), rect(17, 86, 7, 6), rect(-18, 41, 18, 15), rect(12, 37, 14, 9), rect(-3, 34, 12, 13), rect(14, 19, 10, 16)>]

            # AssertionError: current rect(-8, 10, 0, 19)>
            # [] in
            # [rect(12, -30, 1, 11), rect(-8, 10, 0, 19), rect(-38, 30, 20, 3), rect(-38, 33, 28, 13), rect(-24, 46, 14, 4), rect(-19, 55, 3, 8), rect(-17, 78, 17, 12), rect(-4, 104, 9, 2), rect(20, 115, 17, 9), rect(2, 171, 7, 3), rect(-20, 199, 14, 15)>]
            # original
            # [rect(-20, 199, 14, 15), rect(2, 171, 7, 3), rect(20, 115, 17, 9), rect(-4, 104, 9, 2), rect(-17, 78, 17, 12), rect(-19, 55, 3, 8), rect(-24, 33, 14, 17), rect(-38, 30, 20, 7), rect(-8, 10, 0, 19), rect(12, -30, 1, 11)>]

            # AssertionError: current rect(-20, -19, 15, 0)>
            # [] in
            # [rect(-20, -19, 15, 0), rect(-19, 3, 16, 2), rect(1, 29, 7, 6), rect(-24, 47, 18, 4), rect(6, 79, 2, 15), rect(-21, 83, 20, 9), rect(-2, 116, 6, 6), rect(-23, 130, 19, 16), rect(-23, 146, 25, 17)>]
            # original
            # [rect(20, 174, 5, 3), rect(-16, 146, 18, 17), rect(-23, 130, 19, 16), rect(-2, 116, 6, 6), rect(-21, 83, 20, 9), rect(6, 79, 2, 15), rect(-24, 47, 18, 4), rect(1, 29, 7, 6), rect(-19, 3, 16, 2), rect(-20, -19, 15, 0)>]

            # AssertionError: current rect(-31, 62, 38, 9)>
            # [4, 5] in
            # [rect(17, 12, 15, 10), rect(-7, 38, 9, 17), rect(-14, 56, 12, 3), rect(-31, 59, 29, 3), rect(-31, 62, 38, 9), rect(-4, 68, 11, 9), rect(-31, 104, 11, 11), rect(-11, 110, 10, 14), rect(-25, 129, 16, 3), rect(-9, 166, 12, 6), rect(-13, 175, 19, 5)>]
            # original
            # [rect(-13, 175, 19, 5), rect(-9, 166, 12, 6), rect(-25, 129, 16, 3), rect(-11, 110, 10, 14), rect(-31, 104, 11, 11), rect(-4, 62, 11, 15), rect(-31, 59, 18, 10), rect(-14, 56, 12, 12), rect(-7, 38, 9, 17), rect(17, 12, 15, 10)>]

    def test_many_overlap(self):
        # arrange
        dirty_rects = []
        rect_gen = rect_test_cases_generator()
        for data in rect_gen:
            d, e = data
            dirty_rects.extend(d[0])
        for i in range(3):
            dirty_rects.extend(dirty_rects)

        # act
        actual = self.func(dirty_rects)

        # verify
        expected = [Rect(-15, -15, 40, 40)]
        self.assertEqual(expected, actual)

    def test_special_sequence(self):
        # arrange
        rect = Rect
        dirty_rects = [rect(15, -15, 10, 5), rect(-15, 20, 10, 5), rect(-15, -15, 30, 5), rect(-15, -10, 40, 30),
                       rect(-5, 20, 30, 5)]

        # act
        actual = self.func(dirty_rects)

        # verify
        expected = [Rect(-15, -15, 40, 40)]
        self.assertEqual(expected, actual)

    def test_special_sequence2(self):
        # arrange
        rect = Rect
        dirty_rects = [rect(15, -15, 10, 5), rect(-15, -15, 30, 5), rect(-15, -10, 40, 35)]

        # act
        actual = self.func(dirty_rects)

        # verify
        expected = [Rect(-15, -15, 40, 40)]
        self.assertEqual(expected, actual)

    def test_special_sequence3(self):
        # arrange
        rect = Rect
        dirty_rects = [rect(10, -17, 16, 10), rect(7, -15, 14, 7), rect(-5, -12, 19, 6), rect(-18, 15, 4, 20),
                       rect(-1, 6, 2, 4), rect(-7, 19, 11, 20), rect(11, 14, 20, 9), rect(-8, 2, 1, 16),
                       rect(13, -17, 18, 14), rect(-3, 8, 4, 19), rect(-1, -9, 10, 20), rect(8, -20, 15, 4),
                       rect(-10, -20, 11, 13), rect(-18, 15, 4, 7), rect(8, -8, 9, 3), rect(-16, 14, 2, 17),
                       rect(14, -3, 4, 16), rect(-18, 13, 3, 14), rect(-7, 3, 1, 2), rect(11, -6, 4, 18),
                       rect(19, -15, 1, 4), rect(3, 15, 19, 16), rect(-3, 3, 15, 5), rect(-18, -13, 6, 7),
                       rect(8, -17, 16, 1), rect(3, 20, 4, 18), rect(2, -2, 16, 3), rect(9, 10, 4, 2),
                       rect(-15, -13, 15, 13), rect(1, -20, 15, 3), rect(1, 6, 15, 18), rect(16, -20, 14, 14),
                       rect(-1, -16, 14, 2), rect(-14, 20, 6, 11), rect(-20, -16, 10, 14), rect(11, 6, 9, 7),
                       rect(5, 19, 13, 16), rect(16, 20, 14, 9), rect(-20, 3, 10, 7), rect(-5, -12, 6, 18),
                       rect(-7, 9, 7, 18), rect(-1, 11, 19, 9), rect(7, -15, 20, 1), rect(-16, -1, 12, 7),
                       rect(1, -2, 16, 9), rect(1, 8, 6, 4), rect(4, 0, 15, 16), rect(17, 2, 4, 10), rect(3, 17, 1, 6),
                       rect(0, -18, 15, 7), rect(5, 0, 18, 15), rect(18, 19, 12, 4), rect(-9, -1, 10, 6),
                       rect(-5, 11, 13, 11), rect(7, -2, 4, 20), rect(5, 19, 15, 19), rect(5, 14, 8, 13),
                       rect(-11, 19, 16, 4), rect(10, 19, 18, 19), rect(17, -12, 0, 9), rect(-5, 5, 12, 12),
                       rect(20, -14, 3, 1), rect(19, 7, 6, 9), rect(8, 17, 12, 6), rect(-17, -9, 14, 17),
                       rect(16, -1, 1, 8), rect(-17, 6, 14, 5), rect(5, -5, 16, 12), rect(-18, 19, 12, 2),
                       rect(-4, -6, 1, 18), rect(-2, -5, 4, 14), rect(-17, -4, 9, 14), rect(15, -3, 12, 12),
                       rect(19, -14, 19, 4), rect(13, -4, 15, 2), rect(18, 4, 20, 13), rect(-15, -11, 11, 4),
                       rect(-13, 7, 3, 15), rect(-9, 0, 14, 4), rect(16, 6, 19, 15), rect(-7, 0, 13, 19),
                       rect(-20, 3, 7, 8), rect(-12, 5, 14, 15), rect(18, -14, 6, 17), rect(11, 9, 8, 9),
                       rect(16, -18, 17, 16), rect(-8, -17, 4, 16), rect(20, 16, 13, 6), rect(-9, 18, 10, 5),
                       rect(-1, 2, 2, 9), rect(11, -5, 19, 20), rect(16, 20, 12, 10), rect(-9, 1, 0, 13),
                       rect(5, 18, 4, 12), rect(-20, -20, 10, 4), rect(-5, -19, 17, 5), rect(7, 10, 18, 8),
                       rect(10, 19, 9, 11), rect(-19, -1, 6, 5), rect(-1, 9, 6, 5), rect(4, -20, 18, 12),
                       rect(-12, -19, 12, 9), rect(-5, 1, 3, 18), rect(3, 15, 6, 8), rect(18, -15, 0, 17),
                       rect(20, -13, 7, 2), rect(9, -9, 15, 10), rect(-6, -5, 8, 6), rect(-16, 3, 15, 18),
                       rect(6, 2, 2, 1), rect(-11, 15, 7, 12), rect(-6, -14, 5, 18), rect(-17, -19, 10, 20),
                       rect(-14, 11, 7, 3), rect(-5, -11, 13, 11), rect(4, 4, 13, 6), rect(-12, -11, 19, 2),
                       rect(-9, -4, 1, 10), rect(-6, 2, 18, 18), rect(-20, -13, 17, 1), rect(-9, 10, 20, 10),
                       rect(7, -4, 13, 17), rect(14, 9, 0, 11), rect(-20, -15, 0, 15), rect(-15, -14, 18, 1),
                       rect(-9, -1, 19, 19), rect(-20, 6, 9, 8), rect(6, 1, 20, 3), rect(-18, -6, 13, 1),
                       rect(10, 4, 18, 13), rect(-20, -15, 14, 13), rect(5, -14, 8, 17), rect(-8, -11, 5, 2),
                       rect(-1, 11, 0, 14), rect(-11, 19, 2, 10), rect(7, 6, 0, 13), rect(15, 7, 19, 12),
                       rect(16, -12, 18, 8), rect(-14, -1, 20, 17), rect(-20, -15, 9, 12), rect(-18, 5, 7, 5),
                       rect(-5, 2, 0, 1), rect(20, 2, 10, 4), rect(0, 3, 17, 16), rect(4, -1, 17, 14),
                       rect(-9, -8, 8, 18), rect(7, -10, 5, 9), rect(-6, -6, 16, 5), rect(-7, 19, 12, 5),
                       rect(-10, 10, 0, 15), rect(-18, -14, 10, 13), rect(-18, -20, 1, 15), rect(15, 16, 7, 9),
                       rect(13, -3, 15, 4), rect(9, 5, 15, 7), rect(-12, 6, 15, 9), rect(10, 0, 9, 9),
                       rect(5, -4, 11, 20), rect(4, -4, 1, 18), rect(-14, -19, 3, 6), rect(12, 6, 15, 20),
                       rect(-8, 10, 15, 19), rect(19, 14, 19, 19), rect(-5, -1, 13, 20), rect(-3, -7, 19, 1),
                       rect(0, -1, 15, 0), rect(-17, -20, 1, 4), rect(1, -6, 15, 20), rect(4, 5, 7, 7),
                       rect(-1, 17, 4, 9), rect(-8, 18, 18, 16), rect(8, -8, 8, 11), rect(15, 5, 7, 7),
                       rect(5, -6, 2, 17), rect(-15, 8, 6, 17), rect(10, -14, 16, 13), rect(14, -5, 8, 3),
                       rect(-20, -8, 18, 18), rect(16, -19, 12, 18), rect(3, -13, 7, 7), rect(11, -13, 20, 3),
                       rect(13, -6, 14, 12), rect(5, -20, 13, 16), rect(-5, 13, 9, 9), rect(18, -16, 1, 11),
                       rect(-15, -3, 15, 10), rect(17, 11, 16, 16), rect(0, 1, 1, 16), rect(13, 17, 8, 11),
                       rect(7, 10, 1, 3), rect(-9, 18, 14, 18), rect(-18, 16, 12, 19), rect(13, 15, 10, 14),
                       rect(-1, -10, 9, 19), rect(3, -5, 17, 10), rect(-8, 12, 14, 7), rect(8, 6, 20, 17),
                       rect(14, -12, 12, 20), rect(8, -18, 15, 13), rect(-2, -12, 15, 13), rect(-19, 2, 4, 15),
                       rect(-20, 14, 1, 7), rect(-6, 1, 20, 20), rect(-6, -16, 20, 19), rect(13, 15, 13, 1),
                       rect(-20, 18, 11, 11), rect(18, 5, 19, 15), rect(9, 6, 2, 19), rect(18, -14, 13, 15),
                       rect(-13, -18, 13, 10), rect(-10, 11, 4, 18), rect(7, -16, 18, 9), rect(20, 12, 6, 0),
                       rect(6, 0, 19, 5), rect(12, -6, 17, 19), rect(3, 11, 9, 3), rect(-15, 12, 19, 18),
                       rect(-15, -16, 12, 18), rect(3, 16, 6, 20), rect(12, -11, 8, 6), rect(-19, 1, 5, 10),
                       rect(-3, -20, 20, 16), rect(-6, 2, 15, 2), rect(-2, 10, 6, 13), rect(-7, 7, 15, 12),
                       rect(9, -20, 18, 0), rect(7, -14, 5, 19), rect(20, 12, 12, 17), rect(-8, -11, 3, 4),
                       rect(11, -17, 13, 19), rect(-2, 7, 1, 14), rect(5, 15, 9, 2), rect(2, -18, 12, 3),
                       rect(-5, 3, 1, 13), rect(15, -10, 10, 10), rect(-9, -14, 4, 16), rect(-7, -2, 20, 2),
                       rect(-4, -4, 12, 9), rect(-15, 16, 17, 13), rect(-9, 14, 18, 2), rect(11, 2, 17, 14),
                       rect(4, -5, 8, 3), rect(1, -3, 12, 8), rect(-4, 17, 10, 16), rect(-11, -20, 0, 0),
                       rect(6, -19, 2, 5), rect(1, -18, 5, 13), rect(5, 18, 12, 12), rect(2, 2, 17, 19),
                       rect(0, 7, 0, 10), rect(-6, 8, 5, 4), rect(-5, 14, 13, 13), rect(15, -19, 11, 19),
                       rect(-14, -3, 3, 18), rect(0, 2, 20, 4), rect(-3, 1, 5, 13), rect(12, 5, 12, 3),
                       rect(-8, -16, 14, 10), rect(-11, 7, 14, 0), rect(2, 14, 0, 8), rect(-4, 19, 16, 14),
                       rect(15, 2, 19, 4), rect(-16, -9, 12, 13), rect(-8, -12, 18, 2), rect(-20, 4, 4, 8),
                       rect(-14, -19, 13, 13), rect(-10, -5, 13, 20), rect(20, 8, 1, 19), rect(17, 16, 17, 18),
                       rect(-19, -11, 17, 10), rect(3, -9, 10, 19), rect(13, 0, 20, 13), rect(-6, -5, 2, 14),
                       rect(-18, 11, 17, 8), rect(-18, 8, 14, 11), rect(-14, -17, 11, 10), rect(-3, 1, 20, 11),
                       rect(16, 19, 14, 5), rect(2, -3, 3, 2), rect(15, -20, 9, 17), rect(-5, -11, 6, 5),
                       rect(-13, 4, 11, 20), rect(-1, -5, 4, 14), rect(-4, -4, 4, 10), rect(20, -12, 17, 3),
                       rect(5, -10, 4, 12), rect(19, -20, 0, 2), rect(17, 6, 13, 2), rect(6, -7, 10, 1),
                       rect(-6, -14, 7, 9), rect(-1, -7, 14, 15), rect(12, -9, 10, 16), rect(-12, 18, 3, 4),
                       rect(16, 14, 18, 5), rect(13, -4, 12, 10), rect(-2, -17, 6, 16), rect(-6, 16, 9, 16),
                       rect(16, 13, 17, 14), rect(-19, -12, 12, 4), rect(2, 6, 7, 16), rect(-4, -1, 17, 10),
                       rect(-17, 16, 17, 17), rect(15, 1, 5, 0), rect(12, 15, 20, 0), rect(7, 12, 6, 16),
                       rect(4, 3, 1, 10), rect(19, 9, 15, 5), rect(12, -19, 17, 18), rect(11, 13, 9, 1),
                       rect(16, 0, 18, 17), rect(7, 11, 3, 3), rect(-2, 15, 5, 1), rect(14, -17, 20, 17),
                       rect(8, -20, 15, 13), rect(7, -4, 14, 4), rect(4, 1, 19, 15), rect(4, -8, 18, 15),
                       rect(-3, -9, 9, 18), rect(-17, -12, 1, 4), rect(9, -13, 11, 9), rect(11, -1, 6, 16),
                       rect(20, -8, 8, 2), rect(6, 13, 8, 1), rect(17, 9, 16, 15), rect(9, -19, 4, 15),
                       rect(3, 3, 6, 10), rect(18, 6, 6, 7), rect(-13, -16, 14, 7), rect(-1, -12, 9, 13),
                       rect(2, -19, 14, 20), rect(14, -17, 16, 17), rect(-19, -15, 5, 13), rect(16, 17, 0, 2),
                       rect(-4, 9, 1, 18), rect(11, 6, 4, 1), rect(-13, 10, 18, 16), rect(-11, 6, 3, 2),
                       rect(19, 16, 11, 0), rect(4, -4, 15, 14), rect(18, 0, 1, 10), rect(-1, 15, 8, 11),
                       rect(-4, 3, 5, 14), rect(10, -12, 0, 2), rect(20, -15, 10, 19), rect(18, -20, 5, 7),
                       rect(-17, -16, 10, 13), rect(8, 7, 0, 6), rect(1, -13, 14, 7), rect(-15, 3, 10, 12),
                       rect(12, 4, 0, 2), rect(-13, -8, 7, 20), rect(-3, 17, 16, 18), rect(-12, 10, 5, 18),
                       rect(5, -12, 10, 12), rect(-11, 20, 13, 17), rect(-7, -6, 16, 9), rect(-20, -15, 2, 0),
                       rect(15, -17, 15, 0), rect(16, -14, 5, 6), rect(20, 8, 18, 13), rect(-14, 20, 7, 18),
                       rect(-13, -20, 2, 5), rect(11, 19, 18, 13), rect(-16, 5, 7, 3), rect(7, 5, 3, 18),
                       rect(-13, 8, 10, 13), rect(-18, 1, 3, 11), rect(9, -15, 19, 12), rect(-14, -9, 12, 4),
                       rect(-10, 17, 4, 3), rect(13, 17, 4, 15), rect(9, 7, 18, 16), rect(13, -18, 3, 3),
                       rect(-17, 7, 16, 14), rect(14, 6, 6, 17), rect(-6, 0, 13, 8), rect(-3, -4, 10, 6),
                       rect(0, -15, 1, 18), rect(8, 6, 4, 15), rect(0, -9, 18, 5), rect(3, -9, 1, 19),
                       rect(20, 14, 12, 6), rect(9, -20, 3, 1), rect(-16, 14, 14, 14), rect(8, 0, 12, 7),
                       rect(-16, -14, 4, 8), rect(-3, -18, 19, 18), rect(0, 16, 18, 9), rect(5, -8, 1, 0),
                       rect(-9, -14, 20, 8), rect(-18, -10, 18, 1), rect(0, -7, 5, 19), rect(13, 6, 6, 7),
                       rect(-9, -17, 19, 5), rect(13, 9, 12, 9), rect(2, 3, 15, 8), rect(2, 17, 3, 9),
                       rect(2, -5, 16, 13), rect(-5, -8, 20, 0), rect(-7, -3, 20, 20), rect(14, -2, 12, 19),
                       rect(1, 18, 11, 7), rect(-5, 16, 18, 14), rect(10, -3, 14, 10), rect(-11, 12, 2, 5),
                       rect(4, 17, 14, 10), rect(8, 0, 3, 2), rect(19, 4, 9, 6), rect(0, -5, 20, 6),
                       rect(-3, -15, 9, 1), rect(-9, -5, 9, 7), rect(17, -3, 20, 19), rect(-4, 20, 2, 11),
                       rect(-9, -3, 11, 2), rect(-11, -6, 0, 14), rect(-3, 3, 7, 2), rect(6, -4, 11, 19),
                       rect(4, 6, 12, 19), rect(-5, 5, 15, 20), rect(9, -11, 12, 10), rect(-14, -9, 17, 18),
                       rect(-4, 4, 9, 16), rect(18, -12, 19, 0), rect(-16, -11, 4, 2), rect(-7, 15, 14, 6),
                       rect(-20, -18, 18, 7), rect(-10, -13, 14, 5), rect(-17, -8, 10, 7), rect(-9, -5, 14, 12),
                       rect(6, 8, 5, 2), rect(7, -10, 6, 14), rect(12, 13, 10, 6), rect(18, -15, 11, 15),
                       rect(12, -11, 1, 1), rect(-16, -13, 14, 18), rect(-12, -19, 9, 7), rect(-7, -16, 15, 0),
                       rect(9, -16, 15, 19), rect(7, 9, 18, 0), rect(3, 10, 17, 20), rect(0, 4, 10, 6),
                       rect(15, -13, 8, 10), rect(9, 8, 14, 11), rect(10, -4, 8, 4), rect(-5, 5, 20, 1),
                       rect(10, -3, 8, 5), rect(-9, 4, 6, 10), rect(8, -15, 11, 16), rect(-11, 0, 17, 11),
                       rect(13, -2, 19, 0), rect(0, 18, 1, 17), rect(-14, -6, 3, 10), rect(-1, -7, 10, 13),
                       rect(16, -20, 16, 14), rect(18, 14, 10, 20), rect(-15, -10, 3, 4), rect(-15, 15, 16, 17),
                       rect(10, 7, 14, 5), rect(2, 3, 5, 8), rect(20, -9, 16, 9), rect(-8, -14, 9, 9),
                       rect(-12, -1, 10, 2), rect(17, -15, 1, 16), rect(8, 20, 10, 18), rect(14, -9, 11, 11),
                       rect(0, -16, 1, 19), rect(6, 2, 4, 13), rect(10, 4, 18, 4), rect(7, 12, 2, 0),
                       rect(-15, -12, 18, 12), rect(-12, 17, 14, 1), rect(14, -3, 2, 14), rect(10, -15, 1, 17),
                       rect(13, 11, 1, 1), rect(16, -11, 13, 7), rect(-12, -15, 3, 18), rect(-8, -15, 0, 18),
                       rect(11, -4, 14, 10), rect(2, -20, 15, 5), rect(-18, -17, 12, 2), rect(9, 10, 18, 14),
                       rect(5, 7, 9, 16), rect(11, 15, 18, 2), rect(-12, -18, 18, 12), rect(-14, -5, 10, 1),
                       rect(-12, -4, 19, 3), rect(0, -16, 3, 9), rect(5, -2, 7, 17), rect(10, 0, 17, 17),
                       rect(-12, -14, 13, 6), rect(-10, 1, 1, 9), rect(-1, -12, 10, 14), rect(-18, 1, 18, 3),
                       rect(-11, -14, 7, 5), rect(-4, 10, 11, 4), rect(20, -15, 11, 5), rect(19, 16, 0, 4),
                       rect(15, 16, 4, 5), rect(-5, 20, 7, 0), rect(18, -17, 9, 10), rect(-13, 4, 15, 19),
                       rect(18, -17, 13, 6), rect(11, -18, 19, 16), rect(-16, 6, 19, 14), rect(-10, -15, 7, 15),
                       rect(18, 6, 19, 19), rect(-19, 10, 0, 0), rect(12, 15, 17, 4), rect(7, 0, 19, 11),
                       rect(19, 16, 6, 20), rect(7, 19, 7, 4), rect(-17, -11, 9, 10), rect(7, 15, 2, 8),
                       rect(0, 3, 13, 15), rect(-17, -13, 15, 3), rect(1, -13, 9, 17), rect(-4, -16, 14, 19),
                       rect(-14, -8, 4, 18), rect(17, 17, 9, 5), rect(-4, 3, 16, 10), rect(19, -17, 2, 1),
                       rect(14, 20, 9, 12), rect(-16, -5, 4, 9), rect(-10, -3, 2, 15), rect(11, 2, 6, 6),
                       rect(-9, -18, 16, 19), rect(12, -13, 8, 19), rect(3, 4, 8, 20), rect(0, -16, 8, 17),
                       rect(-8, 7, 18, 11), rect(11, -3, 16, 18), rect(7, 9, 1, 11), rect(15, 8, 7, 3),
                       rect(7, 13, 19, 7), rect(19, 10, 2, 13), rect(20, -8, 15, 12), rect(-20, 17, 6, 16),
                       rect(16, 15, 13, 9), rect(20, -18, 11, 18), rect(11, 19, 9, 7), rect(18, -13, 9, 11),
                       rect(-1, 17, 14, 7), rect(13, 18, 10, 16), rect(2, 15, 17, 2), rect(5, 6, 4, 9),
                       rect(-12, -19, 6, 1), rect(-2, 9, 17, 14), rect(10, 9, 1, 0), rect(-20, -10, 9, 11),
                       rect(6, -20, 4, 12), rect(17, -18, 2, 11), rect(-9, 2, 18, 14), rect(-19, -13, 19, 3),
                       rect(16, 0, 14, 7), rect(6, 10, 5, 16), rect(11, 11, 6, 8), rect(13, 19, 6, 16),
                       rect(-7, -13, 8, 15), rect(10, -16, 0, 18), rect(-8, -8, 11, 14), rect(-6, 0, 0, 12),
                       rect(14, -20, 16, 2), rect(-15, -3, 16, 10), rect(20, -5, 11, 15), rect(7, 14, 9, 10),
                       rect(-15, -19, 9, 0), rect(5, 2, 20, 3), rect(-3, -17, 9, 18), rect(-18, -5, 3, 20),
                       rect(11, 6, 6, 3), rect(-8, -17, 6, 0), rect(-9, 6, 3, 8), rect(-2, -11, 20, 11),
                       rect(15, -16, 17, 1), rect(10, 2, 0, 20), rect(0, 0, 17, 1), rect(18, 7, 0, 18),
                       rect(-12, -4, 8, 3), rect(2, -20, 9, 4), rect(4, 15, 2, 3), rect(20, -3, 18, 6),
                       rect(-2, 16, 8, 4), rect(2, 10, 0, 4), rect(5, -11, 3, 13), rect(-17, 5, 20, 9),
                       rect(-2, -12, 19, 14), rect(16, -6, 16, 9), rect(-15, -19, 5, 12), rect(7, -6, 3, 18),
                       rect(-19, 0, 20, 8), rect(15, 20, 19, 6), rect(-4, -1, 6, 12), rect(6, -14, 15, 10),
                       rect(-6, 18, 15, 10), rect(20, 12, 14, 18), rect(-12, 20, 20, 7), rect(7, 10, 12, 3),
                       rect(-15, -19, 0, 20), rect(-11, 1, 3, 8), rect(0, -15, 11, 13), rect(8, -3, 1, 3),
                       rect(-11, 2, 9, 5), rect(-1, -9, 3, 11), rect(19, -1, 15, 17), rect(20, -6, 1, 1),
                       rect(13, 12, 8, 19), rect(-9, 5, 18, 4), rect(-5, -8, 10, 3), rect(-16, -4, 4, 16),
                       rect(-18, 10, 12, 19), rect(-19, -9, 5, 8), rect(-16, -17, 7, 13), rect(19, -16, 13, 18),
                       rect(-17, 13, 5, 1), rect(18, 1, 2, 9), rect(9, -15, 5, 14), rect(3, -17, 16, 2),
                       rect(11, 11, 19, 14), rect(-14, 6, 5, 7), rect(-4, -13, 11, 0), rect(-7, 10, 3, 8),
                       rect(-13, 12, 12, 4), rect(0, 18, 15, 12), rect(18, -10, 19, 0), rect(6, -17, 10, 8),
                       rect(-4, 3, 13, 7), rect(11, -15, 19, 17), rect(-8, 7, 19, 17), rect(-3, -20, 14, 18),
                       rect(11, 4, 10, 20), rect(-1, -8, 9, 1), rect(-18, 13, 16, 10), rect(-1, -1, 13, 10),
                       rect(4, -2, 1, 3), rect(-6, 19, 10, 5), rect(-9, 4, 5, 4), rect(1, -16, 19, 7),
                       rect(-3, 11, 16, 9), rect(-16, 2, 11, 15), rect(12, 19, 20, 3), rect(-15, 12, 18, 10),
                       rect(-5, 18, 12, 8), rect(17, -19, 18, 9), rect(-3, 8, 20, 18), rect(-10, -7, 14, 4),
                       rect(5, 8, 18, 14), rect(18, 2, 6, 12), rect(3, -3, 1, 17), rect(13, -16, 6, 15),
                       rect(0, 7, 12, 14), rect(-9, -4, 8, 10), rect(13, -16, 17, 18), rect(12, -16, 1, 11),
                       rect(-16, -7, 7, 15), rect(7, 3, 6, 1), rect(-3, -1, 7, 12), rect(-16, -11, 9, 8),
                       rect(4, -10, 1, 16), rect(12, 20, 1, 10), rect(-16, -17, 11, 20), rect(-9, -15, 18, 15),
                       rect(-11, -14, 3, 6), rect(14, 7, 5, 1), rect(-18, 8, 8, 6), rect(-14, -15, 0, 16),
                       rect(20, -11, 0, 4), rect(19, 19, 4, 16), rect(3, -1, 14, 15), rect(18, -13, 15, 1),
                       rect(-2, 16, 17, 12), rect(-11, -14, 0, 9), rect(-3, -1, 15, 12), rect(6, -1, 10, 9),
                       rect(8, -3, 16, 6), rect(-2, -9, 14, 5), rect(-12, 20, 16, 2), rect(9, -8, 3, 3),
                       rect(16, -3, 3, 9), rect(6, -4, 9, 4), rect(-18, -8, 6, 15), rect(10, 0, 19, 4),
                       rect(-15, 20, 2, 8), rect(-18, 14, 11, 18), rect(-20, -7, 16, 17), rect(-14, 6, 13, 4),
                       rect(8, -17, 14, 11), rect(-9, -3, 16, 8), rect(-6, 5, 6, 7), rect(16, 10, 2, 20),
                       rect(4, 7, 18, 6), rect(-3, 2, 4, 6), rect(-4, -11, 14, 7), rect(10, 20, 5, 6),
                       rect(19, -8, 7, 11), rect(-6, -6, 5, 6), rect(11, -18, 13, 4), rect(-9, 19, 7, 12),
                       rect(13, 17, 2, 19), rect(20, -5, 17, 9), rect(11, 8, 11, 14), rect(-4, 10, 12, 19),
                       rect(10, 2, 10, 20), rect(-7, -8, 3, 2), rect(-11, -2, 3, 13), rect(18, -20, 7, 5),
                       rect(1, 7, 11, 12), rect(-2, -13, 1, 18), rect(-13, 11, 6, 13), rect(15, -19, 20, 1),
                       rect(-5, -11, 9, 13), rect(0, 9, 2, 13), rect(-1, 19, 2, 18), rect(-14, 20, 9, 17),
                       rect(-12, -4, 1, 3), rect(-11, 7, 7, 7), rect(18, -4, 1, 17), rect(-19, -14, 9, 11),
                       rect(-3, -5, 12, 15), rect(-14, -20, 6, 18), rect(17, -19, 19, 1), rect(6, -6, 16, 9),
                       rect(-20, 9, 6, 19), rect(-2, -20, 4, 13), rect(-20, 18, 10, 9), rect(5, -20, 10, 5),
                       rect(-11, -20, 19, 12), rect(-2, 16, 3, 16), rect(-6, 4, 5, 19), rect(-17, 19, 2, 8),
                       rect(9, 0, 8, 17), rect(6, 9, 13, 3), rect(-9, 20, 1, 5), rect(5, -4, 12, 6),
                       rect(9, -18, 18, 17), rect(20, -18, 2, 15), rect(-14, -9, 15, 5), rect(-8, 11, 7, 20),
                       rect(0, -6, 1, 8), rect(11, -1, 12, 5), rect(-12, -1, 0, 4), rect(0, 18, 6, 11),
                       rect(4, 6, 7, 4), rect(5, -3, 2, 2), rect(-11, -9, 6, 15), rect(16, 1, 8, 8),
                       rect(-10, -7, 14, 8), rect(-4, 4, 1, 16), rect(9, -2, 17, 8), rect(-3, -6, 5, 6),
                       rect(-19, 7, 7, 1), rect(-11, -2, 5, 2), rect(3, -20, 10, 19), rect(-16, -16, 9, 13),
                       rect(-8, -15, 19, 8), rect(-12, 0, 3, 4), rect(19, -3, 16, 13), rect(10, -17, 20, 11),
                       rect(15, 11, 14, 15), rect(-4, 13, 7, 19), rect(20, -15, 9, 1), rect(7, 16, 1, 9),
                       rect(10, -6, 18, 14), rect(1, -18, 17, 18), rect(-8, 7, 14, 5), rect(-7, 2, 17, 14),
                       rect(11, 7, 4, 20), rect(-6, -4, 20, 6), rect(14, 1, 12, 14), rect(-17, 17, 10, 10),
                       rect(13, 11, 15, 16), rect(4, 19, 16, 18), rect(-19, 20, 2, 6), rect(14, 10, 0, 15),
                       rect(-2, 8, 11, 10), rect(20, -19, 12, 15), rect(18, -16, 15, 15), rect(11, -15, 15, 4),
                       rect(-17, 2, 10, 8), rect(6, 11, 1, 16), rect(4, -20, 4, 9), rect(5, -11, 8, 3),
                       rect(13, 16, 11, 3), rect(14, -12, 10, 2), rect(-13, 4, 18, 12), rect(14, 0, 15, 4),
                       rect(-8, 12, 8, 16), rect(-19, 18, 5, 3), rect(14, -11, 7, 14), rect(-20, 15, 15, 13),
                       rect(15, 8, 12, 9), rect(-1, 0, 6, 1), rect(9, -17, 2, 0), rect(16, -13, 1, 1),
                       rect(-11, 15, 8, 6), rect(-19, 16, 4, 10), rect(-15, 12, 16, 11), rect(9, 14, 5, 6),
                       rect(-2, -18, 14, 2), rect(-15, 4, 19, 5), rect(-13, 14, 15, 17), rect(17, 9, 1, 19),
                       rect(19, 17, 20, 14), rect(-16, -12, 4, 7), rect(0, 15, 11, 5), rect(-18, 6, 3, 11),
                       rect(-8, 10, 6, 3), rect(-11, -11, 9, 14), rect(16, -12, 18, 15), rect(10, -8, 9, 9),
                       rect(0, 18, 12, 17), rect(-18, 18, 7, 18), rect(-7, 13, 6, 19), rect(-10, -17, 4, 15),
                       rect(-16, 8, 15, 20), rect(0, 4, 1, 20), rect(8, 3, 1, 10), rect(4, 0, 11, 11),
                       rect(9, 0, 15, 6), rect(8, -7, 0, 6), rect(18, -17, 19, 7), rect(2, -5, 1, 16),
                       rect(-4, 4, 4, 8), rect(5, 5, 2, 10), rect(8, -7, 2, 4), rect(17, 7, 7, 20),
                       rect(-18, -15, 9, 8), rect(6, 8, 3, 10), rect(-5, -11, 17, 11), rect(2, 5, 15, 12),
                       rect(-5, -5, 18, 4), rect(-15, -8, 4, 9), rect(-18, -1, 1, 15), rect(-15, 1, 0, 7),
                       rect(-7, 8, 6, 18), rect(9, 16, 20, 7), rect(15, -14, 12, 3), rect(-2, -1, 2, 16),
                       rect(-5, 3, 4, 18), rect(12, -18, 2, 12), rect(3, -11, 10, 12), rect(-17, -16, 2, 13),
                       rect(-7, 8, 6, 12), rect(-19, -7, 7, 20), rect(-6, 17, 6, 9), rect(-5, -14, 9, 11),
                       rect(-10, 6, 9, 15), rect(2, -5, 9, 6), rect(15, 1, 0, 5), rect(6, 19, 11, 17),
                       rect(-10, 0, 11, 8), rect(10, 20, 4, 10), rect(-18, -17, 1, 10), rect(-13, 14, 1, 1),
                       rect(-9, -14, 17, 17), rect(-18, 11, 11, 1), rect(-4, -4, 20, 8), rect(-11, -19, 5, 1),
                       rect(8, -7, 15, 7), rect(-16, -14, 15, 5), rect(-16, 19, 16, 18), rect(13, 10, 18, 11),
                       rect(-8, -6, 15, 20), rect(-11, 10, 1, 12), rect(15, 17, 13, 4), rect(4, 15, 3, 16),
                       rect(4, 15, 20, 7), rect(-6, -12, 11, 8), rect(2, 8, 5, 11), rect(8, -13, 0, 6),
                       rect(15, 2, 13, 7), rect(17, 5, 15, 20), rect(14, 9, 10, 16), rect(12, 13, 18, 16),
                       rect(-10, 17, 2, 12), rect(-10, 10, 14, 16), rect(-16, 15, 2, 8), rect(-12, -10, 1, 0),
                       rect(-13, 6, 18, 2), rect(7, 4, 3, 2), rect(-20, -16, 9, 1), rect(5, 13, 18, 12),
                       rect(7, -5, 6, 14), rect(-4, 16, 13, 5), rect(5, -8, 18, 10), rect(4, 17, 15, 12),
                       rect(-17, 7, 2, 8), rect(14, 13, 17, 3), rect(17, -14, 15, 15), rect(15, -15, 6, 15),
                       rect(-8, -4, 3, 10), rect(0, 0, 7, 15), rect(16, -8, 13, 4), rect(14, 20, 1, 2),
                       rect(-14, 3, 18, 13), rect(16, 14, 1, 12), rect(17, 16, 16, 19), rect(7, 14, 20, 3),
                       rect(9, -6, 13, 18), rect(10, 19, 4, 4), rect(0, 10, 0, 16), rect(9, -9, 10, 2),
                       rect(17, -9, 17, 20), rect(-4, -5, 13, 15), rect(-10, 6, 15, 5), rect(-18, 20, 17, 4),
                       rect(3, -4, 7, 14), rect(-12, -12, 14, 7), rect(0, 17, 14, 1), rect(-4, -19, 19, 4),
                       rect(1, 2, 11, 2), rect(17, 9, 4, 7), rect(-9, -4, 1, 2), rect(-4, 10, 18, 19),
                       rect(-14, 0, 18, 2), rect(5, 8, 7, 11), rect(16, -2, 12, 11), rect(-4, -20, 12, 13),
                       rect(2, -9, 12, 13), rect(15, 19, 5, 8), rect(7, -4, 17, 16), rect(-10, -6, 17, 15),
                       rect(-1, 6, 6, 18), rect(-8, 1, 9, 14), rect(10, -2, 1, 11), rect(10, -20, 4, 20),
                       rect(-9, -13, 14, 3), rect(-1, 8, 10, 20), rect(-16, 3, 3, 3), rect(13, -17, 18, 11),
                       rect(-11, 7, 1, 6), rect(-2, -16, 11, 7), rect(-10, 0, 8, 13), rect(4, 0, 3, 18),
                       rect(0, 8, 7, 19), rect(-20, 5, 17, 15), rect(9, 12, 15, 3), rect(-10, 7, 3, 3),
                       rect(-10, 8, 6, 5), rect(11, -20, 9, 15), rect(-13, 7, 18, 8), rect(6, 20, 18, 6),
                       rect(5, 1, 20, 10), rect(20, 9, 1, 2), rect(-20, -15, 16, 13), rect(15, -6, 5, 6),
                       rect(-18, 14, 13, 16), rect(2, 7, 8, 15), rect(-20, 12, 5, 18), rect(-4, 0, 7, 2),
                       rect(20, 17, 13, 14), rect(1, -6, 5, 13), rect(9, 13, 16, 16), rect(-16, 5, 20, 6),
                       rect(-20, 0, 12, 0), rect(-17, -9, 16, 5), rect(12, 11, 8, 17), rect(18, -14, 14, 1),
                       rect(-4, -11, 17, 5), rect(-18, 9, 14, 19), rect(-8, 2, 10, 14), rect(-6, -5, 0, 8),
                       rect(-1, 9, 2, 10), rect(2, -9, 0, 1), rect(-11, 13, 9, 17), rect(-9, -12, 17, 19),
                       rect(-19, 3, 14, 14), rect(-16, -9, 13, 16), rect(10, -5, 6, 18), rect(7, 9, 16, 15),
                       rect(16, 13, 16, 8), rect(-18, 3, 13, 14), rect(17, -13, 16, 2), rect(10, -11, 14, 2),
                       rect(14, -20, 12, 20), rect(4, 4, 10, 5), rect(-18, 4, 13, 18), rect(2, 16, 7, 15),
                       rect(11, 20, 4, 0), rect(-13, 10, 18, 11), rect(-9, -1, 19, 10), rect(1, -19, 13, 20),
                       rect(0, -15, 14, 4), rect(-5, 17, 16, 1), rect(-3, -2, 7, 7), rect(10, 14, 10, 4),
                       rect(10, -5, 3, 11), rect(6, 16, 3, 3), rect(8, -18, 18, 0), rect(-8, 20, 11, 6),
                       rect(10, 16, 12, 3), rect(2, 17, 12, 0), rect(-1, -6, 7, 11), rect(-17, -7, 8, 0),
                       rect(2, 2, 11, 6), rect(1, -9, 20, 3), rect(-7, 9, 14, 9), rect(0, 1, 3, 20), rect(-14, 2, 6, 4),
                       rect(16, -17, 15, 4), rect(6, -9, 1, 9)]

        # count = len(dirty_rects)
        # dirty_rects = dirty_rects[count/3:2*count/3]

        # act
        actual = self.func(dirty_rects)

        # verify
        for current in actual:
            indices = current.collidelistall(actual)
            self.assertEqual(1, len(indices),
                             "current {3}\n{0} in \n{1} \noriginal \n{2}".format(indices, actual,
                                                                                 dirty_rects, current))
            self.assertEqual(actual.index(current), indices[0],
                             "current {0}\noriginal {1}".format(current, actual))

        # verify
        self.maxDiff = None
        expected = [rect(-20, 17, 59, 14), rect(-18, 34, 51, 1), rect(-16, 36, 44, 1), rect(-20, 31, 58, 2),
                    rect(-20, 4, 58, 13), rect(-20, 3, 57, 1), rect(-20, -14, 58, 4), rect(-20, -10, 57, 1),
                    rect(-20, -3, 58, 6), rect(-14, 37, 42, 1), rect(-20, -5, 57, 2), rect(-18, 35, 46, 1),
                    rect(-20, -9, 56, 4), rect(-7, 38, 11, 1), rect(-20, -19, 56, 1), rect(-18, 33, 52, 1),
                    rect(-20, -20, 52, 1), rect(-20, -18, 55, 1), rect(-20, -17, 57, 3), ]
        # rect(3, 36, 25, 1)]

        sort_key = lambda r: tuple(r)
        expected.sort(key=sort_key)
        actual.sort(key=sort_key)

        self.assertEqual(expected, actual)

    def test_special_sequence4(self):
        # arrange
        rect = Rect
        dirty_rects = [
            # rect(0, 0, 10, 10), rect(-10, -10, 30, 30), rect(0, 0, 10, 10), rect(0, -10, 20, 30),
            # rect(-10, 0, 20, 10), rect(0, -10, 20, 30), rect(-10, -10, 20, 20), rect(0, 0, 20, 20),
            # rect(0, -10, 10, 20), rect(0, 0, 20, 20), rect(0, 0, 10, 10), rect(0, 0, 20, 20),
            # rect(-10, 0, 20, 10), rect(0, 0, 20, 20), rect(0, -10, 10, 20), rect(-10, 0, 30, 20),
            # rect(0, 0, 10, 10), rect(-10, 0, 30, 20), rect(0, -10, 10, 20), rect(-10, 0, 30, 10),
            # rect(0, 0, 10, 10), rect(-10, 0, 30, 10), rect(0, 0, 10, 20), rect(-10, 0, 30, 10),
            # rect(0, -10, 10, 30), rect(-10, 0, 30, 10), rect(-10, -10, 20, 20), rect(0, 0, 20, 10),
            # rect(0, -10, 10, 20), rect(0, 0, 20, 10), rect(0, 0, 10, 10), rect(0, 0, 20, 10),
            # rect(-10, 0, 20, 10), rect(0, 0, 20, 10), rect(-10, 0, 20, 20), rect(0, 0, 20, 10),
            # rect(0, 0, 10, 20), rect(0, 0, 20, 10), rect(0, -10, 10, 30), rect(0, 0, 20, 10),
            # rect(-10, -10, 20, 30), rect(0, 0, 20, 10), rect(0, 0, 10, 10), rect(0, -10, 20, 20),
            # rect(-10, 0, 20, 10), rect(0, -10, 20, 20), rect(-10, 0, 20, 20), rect(0, -10, 20, 20),
            # rect(0, 0, 10, 20), rect(0, -10, 20, 20), rect(0, 0, 10, 10), rect(-10, -10, 30, 20),
            # rect(0, 0, 10, 20), rect(-10, -10, 30, 20), rect(0, 0, 20, 20), rect(-10, -10, 20, 20),
            # rect(0, 0, 20, 10), rect(-10, -10, 20, 20), rect(0, 0, 10, 10), rect(-10, -10, 20, 20),
            # rect(0, 0, 10, 20), rect(-10, -10, 20, 20), rect(0, 0, 20, 20), rect(0, -10, 10, 20),
            # rect(-10, 0, 30, 20), rect(0, -10, 10, 20), rect(-10, 0, 30, 10), rect(0, -10, 10, 20),
            # rect(0, 0, 20, 10), rect(0, -10, 10, 20), rect(0, 0, 10, 10), rect(0, -10, 10, 20),
            # rect(-10, 0, 20, 10), rect(0, -10, 10, 20), rect(-10, 0, 20, 20), rect(0, -10, 10, 20),
            # rect(0, 0, 10, 20), rect(0, -10, 10, 20), rect(-10, -10, 30, 30), rect(0, 0, 10, 10),
            # rect(0, -10, 20, 30), rect(0, 0, 10, 10), rect(0, 0, 20, 20), rect(0, 0, 10, 10),
            # rect(-10, 0, 30, 20), rect(0, 0, 10, 10), rect(-10, 0, 30, 10), rect(0, 0, 10, 10),
            # rect(0, 0, 20, 10), rect(0, 0, 10, 10), rect(0, -10, 20, 20), rect(0, 0, 10, 10),
            # rect(-10, -10, 30, 20), rect(0, 0, 10, 10), rect(-10, -10, 20, 20), rect(0, 0, 10, 10),
            # rect(0, -10, 10, 20), rect(0, 0, 10, 10), rect(0, 0, 10, 10), rect(0, 0, 10, 10),
            # rect(-10, 0, 20, 10), rect(0, 0, 10, 10), rect(-10, 0, 20, 20), rect(0, 0, 10, 10),
            # rect(0, 0, 10, 20), rect(0, 0, 10, 10), rect(0, -10, 10, 30), rect(0, 0, 10, 10),
            # rect(-10, -10, 20, 30), rect(0, 0, 10, 10), rect(0, -10, 20, 30), rect(-10, 0, 20, 10),
            # rect(0, 0, 20, 20), rect(-10, 0, 20, 10), rect(0, 0, 20, 10), rect(-10, 0, 20, 10),
            # rect(0, -10, 20, 20), rect(-10, 0, 20, 10), rect(0, -10, 10, 20), rect(-10, 0, 20, 10),
            # rect(0, 0, 10, 10), rect(-10, 0, 20, 10), rect(0, 0, 10, 20), rect(-10, 0, 20, 10),

            rect(0, -10, 10, 30), rect(-10, 0, 20, 10), rect(0, 0, 20, 10), rect(-10, 0, 20, 20),
            rect(0, -10, 20, 20), rect(-10, 0, 20, 20), rect(0, -10, 10, 20), rect(-10, 0, 20, 20),
            rect(0, 0, 10, 10), rect(-10, 0, 20, 20), rect(-10, 0, 30, 10), rect(0, 0, 10, 20),
            rect(0, 0, 20, 10), rect(0, 0, 10, 20), rect(0, -10, 20, 20), rect(0, 0, 10, 20),
            rect(-10, -10, 30, 20), rect(0, 0, 10, 20), rect(-10, -10, 20, 20), rect(0, 0, 10, 20),
            rect(0, -10, 10, 20), rect(0, 0, 10, 20), rect(0, 0, 10, 10), rect(0, 0, 10, 20),
            rect(-10, 0, 20, 10), rect(0, 0, 10, 20), rect(-10, 0, 30, 10), rect(0, -10, 10, 30),
            rect(0, 0, 20, 10), rect(0, -10, 10, 30), rect(0, 0, 10, 10), rect(0, -10, 10, 30),
            rect(-10, 0, 20, 10), rect(0, -10, 10, 30), rect(0, 0, 20, 10), rect(-10, -10, 20, 30),
            rect(0, 0, 10, 10), rect(-10, -10, 20, 30), rect(0, 0, 10, 10), rect(-10, 0, 10, 10),
            rect(0, 0, 10, 10), rect(-10, -10, 10, 10), rect(0, 0, 10, 10), rect(0, -10, 10, 10),
            rect(0, 0, 10, 10), rect(10, -10, 10, 10), rect(0, 0, 10, 10), rect(10, 0, 10, 10),
            rect(0, 0, 10, 10), rect(10, 10, 10, 10), rect(0, 0, 10, 10), rect(0, 10, 10, 10),
            rect(0, 0, 10, 10), rect(-10, 10, 10, 10), rect(0, 0, 10, 10), rect(-15, 0, 10, 10),
            rect(0, 0, 10, 10), rect(-15, -15, 10, 10), rect(0, 0, 10, 10), rect(0, -15, 10, 10),
            rect(0, 0, 10, 10), rect(15, -15, 10, 10), rect(0, 0, 10, 10), rect(15, 0, 10, 10),
            rect(0, 0, 10, 10), rect(15, 15, 10, 10), rect(0, 0, 10, 10), rect(0, 15, 10, 10),
            rect(0, 0, 10, 10), rect(-15, 15, 10, 10), rect(0, 0, 10, 10), rect(-15, -5, 10, 10),
            rect(0, 0, 10, 10), rect(-5, -15, 10, 10), rect(0, 0, 10, 10), rect(5, -15, 10, 10),
            rect(0, 0, 10, 10), rect(15, -5, 10, 10), rect(0, 0, 10, 10), rect(15, 5, 10, 10),
            rect(0, 0, 10, 10), rect(5, 15, 10, 10), rect(0, 0, 10, 10), rect(-5, 15, 10, 10),
            rect(0, 0, 10, 10), rect(-15, 5, 10, 10), rect(0, 0, 10, 10), rect(0, 0, 10, 10),
            rect(0, 0, 10, 10), rect(0, 0, 10, 10), rect(5, 0, 10, 10), rect(-5, 0, 10, 10),
            rect(0, 0, 10, 10), rect(0, 5, 10, 10), rect(0, -5, 10, 10), rect(-20, 0, 40, 0),
            rect(0, -20, 0, 40), rect(-10, -10, 20, 20), rect(-10, -10, 20, 20)]

        # count = len(dirty_rects)
        # dirty_rects = dirty_rects[count/2:]

        # act
        actual = self.func(dirty_rects)

        # verify
        expected = [Rect(-15, -15, 40, 40)]
        self.assertEqual(expected, actual)

    def test_performance(self):
        dirty_rects = []
        gen = rect_test_cases_generator()
        for t in gen:
            d, e = t
            dirty_rects.extend(d[0])
        for i in range(3):
            dirty_rects.extend(dirty_rects)

        print("rect count:", len(dirty_rects))

        # dirty_rects = dirty_rects[:len(dirty_rects)//3 + 26]
        # dirty_rects = dirty_rects[:len(dirty_rects) // 3 + 26]

        start_time = time.time()
        actual = self.func(dirty_rects)
        delta = time.time() - start_time
        print(actual)

        print(self.func.__name__ + ": " + str(delta))

    def test_sequence(self):
        # arrange
        dirty_rects = [Rect(10, 0, 10, 10),
                       Rect(-10, -10, 10, 10),
                       Rect(0, 0, 10, 10),
                       Rect(10, 10, 10, 10),
                       Rect(10, -10, 10, 10),
                       Rect(0, -10, 10, 10),
                       Rect(-10, -10, 20, 30)
                       ]

        # act
        actual = self.func(dirty_rects)

        # verify
        self.fail("TODO")


class PerformanceTests(unittest.TestCase):
    def test_performance(self):
        # arrange
        dirty_rects = []
        gen = rect_test_cases_generator()
        for t in gen:
            d, e = t
            dirty_rects.extend(d[0])

        self.act(dirty_rects)

    def test_performance_repeated_sequence(self):
        # arrange
        dirty_rects = []
        gen = rect_test_cases_generator()
        for t in gen:
            d, e = t
            dirty_rects.extend(d[0])
        for i in range(3):
            dirty_rects.extend(dirty_rects)

        self.act(dirty_rects)

    def test_performance_repeated_copied_sequence(self):
        # arrange
        dirty_rects = []
        gen = rect_test_cases_generator()
        for t in gen:
            d, e = t
            dirty_rects.extend(d[0])
        for i in range(3):
            dirty_rects.extend([r.copy() for r in dirty_rects])

        self.act(dirty_rects)

    def test_performance_worst_case(self):
        # arrange
        dirty_rects = []

        # worst case
        for i in range(0, 12000, 6):
            dirty_rects.append(Rect(i, 0, 5, 5))

        self.act(dirty_rects)

    def act(self, dirty_rects):
        # act
        _opt_dirty_rects = [r.copy() for r in dirty_rects]
        print("rect count:", len(dirty_rects))

        start_time2 = time.time()
        actual2 = optimized_dirty_rects.optimize_dirty_rects(_opt_dirty_rects)
        delta2 = time.time() - start_time2
        print("optimize_dirty_rects: ", delta2, actual2)

        start_time1 = time.time()
        actual1 = mut.split_dirty_rects(dirty_rects)
        delta1 = time.time() - start_time1
        print("split_dirty_rects: ", delta1, actual1)

        factor = delta1 / float(delta2)
        if factor >= 1.0:
            print("factor (slower): ", factor)
        else:
            if factor == 0:
                print("factor (faster): ", factor)
            else:
                print("factor (faster): ", 1.0 / factor)

        # verify
        self.assertLessEqual(delta1, delta2)
        self.assertEqual(len(actual1), len(actual2))


@ddtd.use_data_driven_testing_decorators
class TestOtherOptimizedDirtyRects(TestDirtyRectSplit):
    def setUp(self):
        self.func = optimized_dirty_rects.optimize_dirty_rects


if __name__ == '__main__':

    import sys

    if len(sys.argv) == 0:
        unittest.main()
    else:

        import cProfile
        import pstats

        _dirty_rects = []
        gen = rect_test_cases_generator()
        for t in gen:
            d, e = t
            _dirty_rects.extend(d[0])
        for i in range(5):
            _dirty_rects.extend(_dirty_rects)

        _dirty_rects_opt = [r.copy() for r in _dirty_rects]
        print("num rects: ", len(_dirty_rects))

        def _perf_dirty_rects2():
            optimized_dirty_rects.optimize_dirty_rects(_dirty_rects_opt)

        stats_filename = "stats_optimized_dirty.dat"
        cProfile.run('_perf_dirty_rects2()', stats_filename)
        p = pstats.Stats(stats_filename)
        p.strip_dirs().sort_stats('time').print_stats()

        def _perf_dirty_rects():
            mut.split_dirty_rects(_dirty_rects)

        stats_filename = "stats_split_dirty.dat"
        cProfile.run('_perf_dirty_rects()', stats_filename)
        p = pstats.Stats(stats_filename)
        p.strip_dirs().sort_stats('time').print_stats()
