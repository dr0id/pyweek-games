# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) Gummbum
# This file is part of pyweek19-2014-10  TODO: move this to project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
# * Neither the name of the <organization --TODO: author? project? -- > nor the
# names of its contributors may be used to endorse or promote products
# derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Gumm wants to see! :)

This is a visualize program to see the splitting rectangle algorithm doing its work.

Left part is the input, right part represents the rectangles without overlapping.

Keys:

right, down: move to next
left, up: move to previous
space: switch algorithm
click to print the rect data where the cursor is (works only with the split rectangles on the right side)

"""
from __future__ import print_function

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "Gummbum"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "Gummbum @ 2014"
__credits__ = ["Gummbum, DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"



import pygame
from pygame.locals import *

from dirty_rectangle_split import split_dirty_rects
from optimized_dirty_rects import optimize_dirty_rects
from test_dirty_rectangle_split import rect_test_cases_generator


def main():
    """The main function."""
    resolution = 800, 600
    pygame.init()
    screen = pygame.display.set_mode(resolution)
    erase_color = Color('black')

    # Get our rects from somewhere. Put 'em in a flat list or list [[1a, 1b], [2a, 2b], ...].
    dirty_rects = []
    gen = rect_test_cases_generator()
    for t in gen:
        d, e = t
        dirty_rects.extend(d[0])
    for i in range(3):
        dirty_rects.extend(dirty_rects)
    # noinspection PyListCreation
    rects = [dirty_rects]

    rects.append([pygame.Rect(0, 0, 10, 10), pygame.Rect(-10, -10, 30, 30), pygame.Rect(0, 0, 10, 10),
                  pygame.Rect(0, -10, 20, 30), pygame.Rect(-10, 0, 20, 10), pygame.Rect(0, -10, 20, 30),
                  pygame.Rect(-10, -10, 20, 20), pygame.Rect(0, 0, 20, 20), pygame.Rect(0, -10, 10, 20),
                  pygame.Rect(0, 0, 20, 20), pygame.Rect(0, 0, 10, 10), pygame.Rect(0, 0, 20, 20),
                  pygame.Rect(-10, 0, 20, 10), pygame.Rect(0, 0, 20, 20), pygame.Rect(0, -10, 10, 20),
                  pygame.Rect(-10, 0, 30, 20), pygame.Rect(0, 0, 10, 10), pygame.Rect(-10, 0, 30, 20),
                  pygame.Rect(0, -10, 10, 20), pygame.Rect(-10, 0, 30, 10), pygame.Rect(0, 0, 10, 10),
                  pygame.Rect(-10, 0, 30, 10), pygame.Rect(0, 0, 10, 20), pygame.Rect(-10, 0, 30, 10),
                  pygame.Rect(0, -10, 10, 30), pygame.Rect(-10, 0, 30, 10), pygame.Rect(-10, -10, 20, 20),
                  pygame.Rect(0, 0, 20, 10), pygame.Rect(0, -10, 10, 20), pygame.Rect(0, 0, 20, 10),
                  pygame.Rect(0, 0, 10, 10), pygame.Rect(0, 0, 20, 10), pygame.Rect(-10, 0, 20, 10),
                  pygame.Rect(0, 0, 20, 10), pygame.Rect(-10, 0, 20, 20), pygame.Rect(0, 0, 20, 10),
                  pygame.Rect(0, 0, 10, 20), pygame.Rect(0, 0, 20, 10), pygame.Rect(0, -10, 10, 30),
                  pygame.Rect(0, 0, 20, 10), pygame.Rect(-10, -10, 20, 30), pygame.Rect(0, 0, 20, 10),
                  pygame.Rect(0, 0, 10, 10), pygame.Rect(0, -10, 20, 20), pygame.Rect(-10, 0, 20, 10),
                  pygame.Rect(0, -10, 20, 20), pygame.Rect(-10, 0, 20, 20), pygame.Rect(0, -10, 20, 20),
                  pygame.Rect(0, 0, 10, 20), pygame.Rect(0, -10, 20, 20), pygame.Rect(0, 0, 10, 10),
                  pygame.Rect(-10, -10, 30, 20), pygame.Rect(0, 0, 10, 20), pygame.Rect(-10, -10, 30, 20),
                  pygame.Rect(0, 0, 20, 20), pygame.Rect(-10, -10, 20, 20), pygame.Rect(0, 0, 20, 10),
                  pygame.Rect(-10, -10, 20, 20), pygame.Rect(0, 0, 10, 10), pygame.Rect(-10, -10, 20, 20),
                  pygame.Rect(0, 0, 10, 20), pygame.Rect(-10, -10, 20, 20), pygame.Rect(0, 0, 20, 20),
                  pygame.Rect(0, -10, 10, 20), pygame.Rect(-10, 0, 30, 20), pygame.Rect(0, -10, 10, 20),
                  pygame.Rect(-10, 0, 30, 10), pygame.Rect(0, -10, 10, 20), pygame.Rect(0, 0, 20, 10),
                  pygame.Rect(0, -10, 10, 20), pygame.Rect(0, 0, 10, 10), pygame.Rect(0, -10, 10, 20),
                  pygame.Rect(-10, 0, 20, 10), pygame.Rect(0, -10, 10, 20), pygame.Rect(-10, 0, 20, 20),
                  pygame.Rect(0, -10, 10, 20), pygame.Rect(0, 0, 10, 20), pygame.Rect(0, -10, 10, 20),
                  pygame.Rect(-10, -10, 30, 30), pygame.Rect(0, 0, 10, 10), pygame.Rect(0, -10, 20, 30),
                  pygame.Rect(0, 0, 10, 10), pygame.Rect(0, 0, 20, 20), pygame.Rect(0, 0, 10, 10),
                  pygame.Rect(-10, 0, 30, 20), pygame.Rect(0, 0, 10, 10), pygame.Rect(-10, 0, 30, 10),
                  pygame.Rect(0, 0, 10, 10), pygame.Rect(0, 0, 20, 10), pygame.Rect(0, 0, 10, 10),
                  pygame.Rect(0, -10, 20, 20), pygame.Rect(0, 0, 10, 10), pygame.Rect(-10, -10, 30, 20),
                  pygame.Rect(0, 0, 10, 10), pygame.Rect(-10, -10, 20, 20), pygame.Rect(0, 0, 10, 10),
                  pygame.Rect(0, -10, 10, 20), pygame.Rect(0, 0, 10, 10), pygame.Rect(0, 0, 10, 10),
                  pygame.Rect(0, 0, 10, 10), pygame.Rect(-10, 0, 20, 10), pygame.Rect(0, 0, 10, 10),
                  pygame.Rect(-10, 0, 20, 20), pygame.Rect(0, 0, 10, 10), pygame.Rect(0, 0, 10, 20),
                  pygame.Rect(0, 0, 10, 10), pygame.Rect(0, -10, 10, 30), pygame.Rect(0, 0, 10, 10),
                  pygame.Rect(-10, -10, 20, 30), pygame.Rect(0, 0, 10, 10), pygame.Rect(0, -10, 20, 30),
                  pygame.Rect(-10, 0, 20, 10), pygame.Rect(0, 0, 20, 20), pygame.Rect(-10, 0, 20, 10),
                  pygame.Rect(0, 0, 20, 10), pygame.Rect(-10, 0, 20, 10), pygame.Rect(0, -10, 20, 20),
                  pygame.Rect(-10, 0, 20, 10), pygame.Rect(0, -10, 10, 20), pygame.Rect(-10, 0, 20, 10),
                  pygame.Rect(0, 0, 10, 10), pygame.Rect(-10, 0, 20, 10), pygame.Rect(0, 0, 10, 20),
                  pygame.Rect(-10, 0, 20, 10), pygame.Rect(0, -10, 10, 30), pygame.Rect(-10, 0, 20, 10),
                  pygame.Rect(0, 0, 20, 10), pygame.Rect(-10, 0, 20, 20), pygame.Rect(0, -10, 20, 20),
                  pygame.Rect(-10, 0, 20, 20), pygame.Rect(0, -10, 10, 20), pygame.Rect(-10, 0, 20, 20),
                  pygame.Rect(0, 0, 10, 10), pygame.Rect(-10, 0, 20, 20), pygame.Rect(-10, 0, 30, 10),
                  pygame.Rect(0, 0, 10, 20), pygame.Rect(0, 0, 20, 10), pygame.Rect(0, 0, 10, 20),
                  pygame.Rect(0, -10, 20, 20), pygame.Rect(0, 0, 10, 20), pygame.Rect(-10, -10, 30, 20),
                  pygame.Rect(0, 0, 10, 20), pygame.Rect(-10, -10, 20, 20), pygame.Rect(0, 0, 10, 20),
                  pygame.Rect(0, -10, 10, 20), pygame.Rect(0, 0, 10, 20), pygame.Rect(0, 0, 10, 10),
                  pygame.Rect(0, 0, 10, 20), pygame.Rect(-10, 0, 20, 10), pygame.Rect(0, 0, 10, 20),
                  pygame.Rect(-10, 0, 30, 10), pygame.Rect(0, -10, 10, 30), pygame.Rect(0, 0, 20, 10),
                  pygame.Rect(0, -10, 10, 30), pygame.Rect(0, 0, 10, 10), pygame.Rect(0, -10, 10, 30),
                  pygame.Rect(-10, 0, 20, 10), pygame.Rect(0, -10, 10, 30), pygame.Rect(0, 0, 20, 10),
                  pygame.Rect(-10, -10, 20, 30), pygame.Rect(0, 0, 10, 10), pygame.Rect(-10, -10, 20, 30),
                  pygame.Rect(0, 0, 10, 10), pygame.Rect(-10, 0, 10, 10), pygame.Rect(0, 0, 10, 10),
                  pygame.Rect(-10, -10, 10, 10), pygame.Rect(0, 0, 10, 10), pygame.Rect(0, -10, 10, 10),
                  pygame.Rect(0, 0, 10, 10), pygame.Rect(10, -10, 10, 10), pygame.Rect(0, 0, 10, 10),
                  pygame.Rect(10, 0, 10, 10), pygame.Rect(0, 0, 10, 10), pygame.Rect(10, 10, 10, 10),
                  pygame.Rect(0, 0, 10, 10), pygame.Rect(0, 10, 10, 10), pygame.Rect(0, 0, 10, 10),
                  pygame.Rect(-10, 10, 10, 10), pygame.Rect(0, 0, 10, 10), pygame.Rect(-15, 0, 10, 10),
                  pygame.Rect(0, 0, 10, 10), pygame.Rect(-15, -15, 10, 10), pygame.Rect(0, 0, 10, 10),
                  pygame.Rect(0, -15, 10, 10), pygame.Rect(0, 0, 10, 10), pygame.Rect(15, -15, 10, 10),
                  pygame.Rect(0, 0, 10, 10), pygame.Rect(15, 0, 10, 10), pygame.Rect(0, 0, 10, 10),
                  pygame.Rect(15, 15, 10, 10), pygame.Rect(0, 0, 10, 10), pygame.Rect(0, 15, 10, 10),
                  pygame.Rect(0, 0, 10, 10), pygame.Rect(-15, 15, 10, 10), pygame.Rect(0, 0, 10, 10),
                  pygame.Rect(-15, -5, 10, 10), pygame.Rect(0, 0, 10, 10), pygame.Rect(-5, -15, 10, 10),
                  pygame.Rect(0, 0, 10, 10), pygame.Rect(5, -15, 10, 10), pygame.Rect(0, 0, 10, 10),
                  pygame.Rect(15, -5, 10, 10), pygame.Rect(0, 0, 10, 10), pygame.Rect(15, 5, 10, 10),
                  pygame.Rect(0, 0, 10, 10), pygame.Rect(5, 15, 10, 10), pygame.Rect(0, 0, 10, 10),
                  pygame.Rect(-5, 15, 10, 10), pygame.Rect(0, 0, 10, 10), pygame.Rect(-15, 5, 10, 10),
                  pygame.Rect(0, 0, 10, 10), pygame.Rect(0, 0, 10, 10), pygame.Rect(0, 0, 10, 10),
                  pygame.Rect(0, 0, 10, 10), pygame.Rect(5, 0, 10, 10), pygame.Rect(-5, 0, 10, 10),
                  pygame.Rect(0, 0, 10, 10), pygame.Rect(0, 5, 10, 10), pygame.Rect(0, -5, 10, 10),
                  pygame.Rect(-20, 0, 40, 0), pygame.Rect(0, -20, 0, 40), pygame.Rect(-10, -10, 20, 20),
                  pygame.Rect(-10, -10, 20, 20)])

    rect = pygame.Rect
    rects.append(
        [rect(10, -17, 16, 10), rect(7, -15, 14, 7), rect(-5, -12, 19, 6), rect(-18, 15, 4, 20), rect(-1, 6, 2, 4),
         rect(-7, 19, 11, 20), rect(11, 14, 20, 9), rect(-8, 2, 1, 16), rect(13, -17, 18, 14), rect(-3, 8, 4, 19),
         rect(-1, -9, 10, 20), rect(8, -20, 15, 4), rect(-10, -20, 11, 13), rect(-18, 15, 4, 7), rect(8, -8, 9, 3),
         rect(-16, 14, 2, 17), rect(14, -3, 4, 16), rect(-18, 13, 3, 14), rect(-7, 3, 1, 2), rect(11, -6, 4, 18),
         rect(19, -15, 1, 4), rect(3, 15, 19, 16), rect(-3, 3, 15, 5), rect(-18, -13, 6, 7), rect(8, -17, 16, 1),
         rect(3, 20, 4, 18), rect(2, -2, 16, 3), rect(9, 10, 4, 2), rect(-15, -13, 15, 13), rect(1, -20, 15, 3),
         rect(1, 6, 15, 18), rect(16, -20, 14, 14), rect(-1, -16, 14, 2), rect(-14, 20, 6, 11), rect(-20, -16, 10, 14),
         rect(11, 6, 9, 7), rect(5, 19, 13, 16), rect(16, 20, 14, 9), rect(-20, 3, 10, 7), rect(-5, -12, 6, 18),
         rect(-7, 9, 7, 18), rect(-1, 11, 19, 9), rect(7, -15, 20, 1), rect(-16, -1, 12, 7), rect(1, -2, 16, 9),
         rect(1, 8, 6, 4), rect(4, 0, 15, 16), rect(17, 2, 4, 10), rect(3, 17, 1, 6), rect(0, -18, 15, 7),
         rect(5, 0, 18, 15), rect(18, 19, 12, 4), rect(-9, -1, 10, 6), rect(-5, 11, 13, 11), rect(7, -2, 4, 20),
         rect(5, 19, 15, 19), rect(5, 14, 8, 13), rect(-11, 19, 16, 4), rect(10, 19, 18, 19), rect(17, -12, 0, 9),
         rect(-5, 5, 12, 12), rect(20, -14, 3, 1), rect(19, 7, 6, 9), rect(8, 17, 12, 6), rect(-17, -9, 14, 17),
         rect(16, -1, 1, 8), rect(-17, 6, 14, 5), rect(5, -5, 16, 12), rect(-18, 19, 12, 2), rect(-4, -6, 1, 18),
         rect(-2, -5, 4, 14), rect(-17, -4, 9, 14), rect(15, -3, 12, 12), rect(19, -14, 19, 4), rect(13, -4, 15, 2),
         rect(18, 4, 20, 13), rect(-15, -11, 11, 4), rect(-13, 7, 3, 15), rect(-9, 0, 14, 4), rect(16, 6, 19, 15),
         rect(-7, 0, 13, 19), rect(-20, 3, 7, 8), rect(-12, 5, 14, 15), rect(18, -14, 6, 17), rect(11, 9, 8, 9),
         rect(16, -18, 17, 16), rect(-8, -17, 4, 16), rect(20, 16, 13, 6), rect(-9, 18, 10, 5), rect(-1, 2, 2, 9),
         rect(11, -5, 19, 20), rect(16, 20, 12, 10), rect(-9, 1, 0, 13), rect(5, 18, 4, 12), rect(-20, -20, 10, 4),
         rect(-5, -19, 17, 5), rect(7, 10, 18, 8), rect(10, 19, 9, 11), rect(-19, -1, 6, 5), rect(-1, 9, 6, 5),
         rect(4, -20, 18, 12), rect(-12, -19, 12, 9), rect(-5, 1, 3, 18), rect(3, 15, 6, 8), rect(18, -15, 0, 17),
         rect(20, -13, 7, 2), rect(9, -9, 15, 10), rect(-6, -5, 8, 6), rect(-16, 3, 15, 18), rect(6, 2, 2, 1),
         rect(-11, 15, 7, 12), rect(-6, -14, 5, 18), rect(-17, -19, 10, 20), rect(-14, 11, 7, 3), rect(-5, -11, 13, 11),
         rect(4, 4, 13, 6), rect(-12, -11, 19, 2), rect(-9, -4, 1, 10), rect(-6, 2, 18, 18), rect(-20, -13, 17, 1),
         rect(-9, 10, 20, 10), rect(7, -4, 13, 17), rect(14, 9, 0, 11), rect(-20, -15, 0, 15), rect(-15, -14, 18, 1),
         rect(-9, -1, 19, 19), rect(-20, 6, 9, 8), rect(6, 1, 20, 3), rect(-18, -6, 13, 1), rect(10, 4, 18, 13),
         rect(-20, -15, 14, 13), rect(5, -14, 8, 17), rect(-8, -11, 5, 2), rect(-1, 11, 0, 14), rect(-11, 19, 2, 10),
         rect(7, 6, 0, 13), rect(15, 7, 19, 12), rect(16, -12, 18, 8), rect(-14, -1, 20, 17), rect(-20, -15, 9, 12),
         rect(-18, 5, 7, 5), rect(-5, 2, 0, 1), rect(20, 2, 10, 4), rect(0, 3, 17, 16), rect(4, -1, 17, 14),
         rect(-9, -8, 8, 18), rect(7, -10, 5, 9), rect(-6, -6, 16, 5), rect(-7, 19, 12, 5), rect(-10, 10, 0, 15),
         rect(-18, -14, 10, 13), rect(-18, -20, 1, 15), rect(15, 16, 7, 9), rect(13, -3, 15, 4), rect(9, 5, 15, 7),
         rect(-12, 6, 15, 9), rect(10, 0, 9, 9), rect(5, -4, 11, 20), rect(4, -4, 1, 18), rect(-14, -19, 3, 6),
         rect(12, 6, 15, 20), rect(-8, 10, 15, 19), rect(19, 14, 19, 19), rect(-5, -1, 13, 20), rect(-3, -7, 19, 1),
         rect(0, -1, 15, 0), rect(-17, -20, 1, 4), rect(1, -6, 15, 20), rect(4, 5, 7, 7), rect(-1, 17, 4, 9),
         rect(-8, 18, 18, 16), rect(8, -8, 8, 11), rect(15, 5, 7, 7), rect(5, -6, 2, 17), rect(-15, 8, 6, 17),
         rect(10, -14, 16, 13), rect(14, -5, 8, 3), rect(-20, -8, 18, 18), rect(16, -19, 12, 18), rect(3, -13, 7, 7),
         rect(11, -13, 20, 3), rect(13, -6, 14, 12), rect(5, -20, 13, 16), rect(-5, 13, 9, 9), rect(18, -16, 1, 11),
         rect(-15, -3, 15, 10), rect(17, 11, 16, 16), rect(0, 1, 1, 16), rect(13, 17, 8, 11), rect(7, 10, 1, 3),
         rect(-9, 18, 14, 18), rect(-18, 16, 12, 19), rect(13, 15, 10, 14), rect(-1, -10, 9, 19), rect(3, -5, 17, 10),
         rect(-8, 12, 14, 7), rect(8, 6, 20, 17), rect(14, -12, 12, 20), rect(8, -18, 15, 13), rect(-2, -12, 15, 13),
         rect(-19, 2, 4, 15), rect(-20, 14, 1, 7), rect(-6, 1, 20, 20), rect(-6, -16, 20, 19), rect(13, 15, 13, 1),
         rect(-20, 18, 11, 11), rect(18, 5, 19, 15), rect(9, 6, 2, 19), rect(18, -14, 13, 15), rect(-13, -18, 13, 10),
         rect(-10, 11, 4, 18), rect(7, -16, 18, 9), rect(20, 12, 6, 0), rect(6, 0, 19, 5), rect(12, -6, 17, 19),
         rect(3, 11, 9, 3), rect(-15, 12, 19, 18), rect(-15, -16, 12, 18), rect(3, 16, 6, 20), rect(12, -11, 8, 6),
         rect(-19, 1, 5, 10), rect(-3, -20, 20, 16), rect(-6, 2, 15, 2), rect(-2, 10, 6, 13), rect(-7, 7, 15, 12),
         rect(9, -20, 18, 0), rect(7, -14, 5, 19), rect(20, 12, 12, 17), rect(-8, -11, 3, 4), rect(11, -17, 13, 19),
         rect(-2, 7, 1, 14), rect(5, 15, 9, 2), rect(2, -18, 12, 3), rect(-5, 3, 1, 13), rect(15, -10, 10, 10),
         rect(-9, -14, 4, 16), rect(-7, -2, 20, 2), rect(-4, -4, 12, 9), rect(-15, 16, 17, 13), rect(-9, 14, 18, 2),
         rect(11, 2, 17, 14), rect(4, -5, 8, 3), rect(1, -3, 12, 8), rect(-4, 17, 10, 16), rect(-11, -20, 0, 0),
         rect(6, -19, 2, 5), rect(1, -18, 5, 13), rect(5, 18, 12, 12), rect(2, 2, 17, 19), rect(0, 7, 0, 10),
         rect(-6, 8, 5, 4), rect(-5, 14, 13, 13), rect(15, -19, 11, 19), rect(-14, -3, 3, 18), rect(0, 2, 20, 4),
         rect(-3, 1, 5, 13), rect(12, 5, 12, 3), rect(-8, -16, 14, 10), rect(-11, 7, 14, 0), rect(2, 14, 0, 8),
         rect(-4, 19, 16, 14), rect(15, 2, 19, 4), rect(-16, -9, 12, 13), rect(-8, -12, 18, 2), rect(-20, 4, 4, 8),
         rect(-14, -19, 13, 13), rect(-10, -5, 13, 20), rect(20, 8, 1, 19), rect(17, 16, 17, 18),
         rect(-19, -11, 17, 10), rect(3, -9, 10, 19), rect(13, 0, 20, 13), rect(-6, -5, 2, 14), rect(-18, 11, 17, 8),
         rect(-18, 8, 14, 11), rect(-14, -17, 11, 10), rect(-3, 1, 20, 11), rect(16, 19, 14, 5), rect(2, -3, 3, 2),
         rect(15, -20, 9, 17), rect(-5, -11, 6, 5), rect(-13, 4, 11, 20), rect(-1, -5, 4, 14), rect(-4, -4, 4, 10),
         rect(20, -12, 17, 3), rect(5, -10, 4, 12), rect(19, -20, 0, 2), rect(17, 6, 13, 2), rect(6, -7, 10, 1),
         rect(-6, -14, 7, 9), rect(-1, -7, 14, 15), rect(12, -9, 10, 16), rect(-12, 18, 3, 4), rect(16, 14, 18, 5),
         rect(13, -4, 12, 10), rect(-2, -17, 6, 16), rect(-6, 16, 9, 16), rect(16, 13, 17, 14), rect(-19, -12, 12, 4),
         rect(2, 6, 7, 16), rect(-4, -1, 17, 10), rect(-17, 16, 17, 17), rect(15, 1, 5, 0), rect(12, 15, 20, 0),
         rect(7, 12, 6, 16), rect(4, 3, 1, 10), rect(19, 9, 15, 5), rect(12, -19, 17, 18), rect(11, 13, 9, 1),
         rect(16, 0, 18, 17), rect(7, 11, 3, 3), rect(-2, 15, 5, 1), rect(14, -17, 20, 17), rect(8, -20, 15, 13),
         rect(7, -4, 14, 4), rect(4, 1, 19, 15), rect(4, -8, 18, 15), rect(-3, -9, 9, 18), rect(-17, -12, 1, 4),
         rect(9, -13, 11, 9), rect(11, -1, 6, 16), rect(20, -8, 8, 2), rect(6, 13, 8, 1), rect(17, 9, 16, 15),
         rect(9, -19, 4, 15), rect(3, 3, 6, 10), rect(18, 6, 6, 7), rect(-13, -16, 14, 7), rect(-1, -12, 9, 13),
         rect(2, -19, 14, 20), rect(14, -17, 16, 17), rect(-19, -15, 5, 13), rect(16, 17, 0, 2), rect(-4, 9, 1, 18),
         rect(11, 6, 4, 1), rect(-13, 10, 18, 16), rect(-11, 6, 3, 2), rect(19, 16, 11, 0), rect(4, -4, 15, 14),
         rect(18, 0, 1, 10), rect(-1, 15, 8, 11), rect(-4, 3, 5, 14), rect(10, -12, 0, 2), rect(20, -15, 10, 19),
         rect(18, -20, 5, 7), rect(-17, -16, 10, 13), rect(8, 7, 0, 6), rect(1, -13, 14, 7), rect(-15, 3, 10, 12),
         rect(12, 4, 0, 2), rect(-13, -8, 7, 20), rect(-3, 17, 16, 18), rect(-12, 10, 5, 18), rect(5, -12, 10, 12),
         rect(-11, 20, 13, 17), rect(-7, -6, 16, 9), rect(-20, -15, 2, 0), rect(15, -17, 15, 0), rect(16, -14, 5, 6),
         rect(20, 8, 18, 13), rect(-14, 20, 7, 18), rect(-13, -20, 2, 5), rect(11, 19, 18, 13), rect(-16, 5, 7, 3),
         rect(7, 5, 3, 18), rect(-13, 8, 10, 13), rect(-18, 1, 3, 11), rect(9, -15, 19, 12), rect(-14, -9, 12, 4),
         rect(-10, 17, 4, 3), rect(13, 17, 4, 15), rect(9, 7, 18, 16), rect(13, -18, 3, 3), rect(-17, 7, 16, 14),
         rect(14, 6, 6, 17), rect(-6, 0, 13, 8), rect(-3, -4, 10, 6), rect(0, -15, 1, 18), rect(8, 6, 4, 15),
         rect(0, -9, 18, 5), rect(3, -9, 1, 19), rect(20, 14, 12, 6), rect(9, -20, 3, 1), rect(-16, 14, 14, 14),
         rect(8, 0, 12, 7), rect(-16, -14, 4, 8), rect(-3, -18, 19, 18), rect(0, 16, 18, 9), rect(5, -8, 1, 0),
         rect(-9, -14, 20, 8), rect(-18, -10, 18, 1), rect(0, -7, 5, 19), rect(13, 6, 6, 7), rect(-9, -17, 19, 5),
         rect(13, 9, 12, 9), rect(2, 3, 15, 8), rect(2, 17, 3, 9), rect(2, -5, 16, 13), rect(-5, -8, 20, 0),
         rect(-7, -3, 20, 20), rect(14, -2, 12, 19), rect(1, 18, 11, 7), rect(-5, 16, 18, 14), rect(10, -3, 14, 10),
         rect(-11, 12, 2, 5), rect(4, 17, 14, 10), rect(8, 0, 3, 2), rect(19, 4, 9, 6), rect(0, -5, 20, 6),
         rect(-3, -15, 9, 1), rect(-9, -5, 9, 7), rect(17, -3, 20, 19), rect(-4, 20, 2, 11), rect(-9, -3, 11, 2),
         rect(-11, -6, 0, 14), rect(-3, 3, 7, 2), rect(6, -4, 11, 19), rect(4, 6, 12, 19), rect(-5, 5, 15, 20),
         rect(9, -11, 12, 10), rect(-14, -9, 17, 18), rect(-4, 4, 9, 16), rect(18, -12, 19, 0), rect(-16, -11, 4, 2),
         rect(-7, 15, 14, 6), rect(-20, -18, 18, 7), rect(-10, -13, 14, 5), rect(-17, -8, 10, 7), rect(-9, -5, 14, 12),
         rect(6, 8, 5, 2), rect(7, -10, 6, 14), rect(12, 13, 10, 6), rect(18, -15, 11, 15), rect(12, -11, 1, 1),
         rect(-16, -13, 14, 18), rect(-12, -19, 9, 7), rect(-7, -16, 15, 0), rect(9, -16, 15, 19), rect(7, 9, 18, 0),
         rect(3, 10, 17, 20), rect(0, 4, 10, 6), rect(15, -13, 8, 10), rect(9, 8, 14, 11), rect(10, -4, 8, 4),
         rect(-5, 5, 20, 1), rect(10, -3, 8, 5), rect(-9, 4, 6, 10), rect(8, -15, 11, 16), rect(-11, 0, 17, 11),
         rect(13, -2, 19, 0), rect(0, 18, 1, 17), rect(-14, -6, 3, 10), rect(-1, -7, 10, 13), rect(16, -20, 16, 14),
         rect(18, 14, 10, 20), rect(-15, -10, 3, 4), rect(-15, 15, 16, 17), rect(10, 7, 14, 5), rect(2, 3, 5, 8),
         rect(20, -9, 16, 9), rect(-8, -14, 9, 9), rect(-12, -1, 10, 2), rect(17, -15, 1, 16), rect(8, 20, 10, 18),
         rect(14, -9, 11, 11), rect(0, -16, 1, 19), rect(6, 2, 4, 13), rect(10, 4, 18, 4), rect(7, 12, 2, 0),
         rect(-15, -12, 18, 12), rect(-12, 17, 14, 1), rect(14, -3, 2, 14), rect(10, -15, 1, 17), rect(13, 11, 1, 1),
         rect(16, -11, 13, 7), rect(-12, -15, 3, 18), rect(-8, -15, 0, 18), rect(11, -4, 14, 10), rect(2, -20, 15, 5),
         rect(-18, -17, 12, 2), rect(9, 10, 18, 14), rect(5, 7, 9, 16), rect(11, 15, 18, 2), rect(-12, -18, 18, 12),
         rect(-14, -5, 10, 1), rect(-12, -4, 19, 3), rect(0, -16, 3, 9), rect(5, -2, 7, 17), rect(10, 0, 17, 17),
         rect(-12, -14, 13, 6), rect(-10, 1, 1, 9), rect(-1, -12, 10, 14), rect(-18, 1, 18, 3), rect(-11, -14, 7, 5),
         rect(-4, 10, 11, 4), rect(20, -15, 11, 5), rect(19, 16, 0, 4), rect(15, 16, 4, 5), rect(-5, 20, 7, 0),
         rect(18, -17, 9, 10), rect(-13, 4, 15, 19), rect(18, -17, 13, 6), rect(11, -18, 19, 16), rect(-16, 6, 19, 14),
         rect(-10, -15, 7, 15), rect(18, 6, 19, 19), rect(-19, 10, 0, 0), rect(12, 15, 17, 4), rect(7, 0, 19, 11),
         rect(19, 16, 6, 20), rect(7, 19, 7, 4), rect(-17, -11, 9, 10), rect(7, 15, 2, 8), rect(0, 3, 13, 15),
         rect(-17, -13, 15, 3), rect(1, -13, 9, 17), rect(-4, -16, 14, 19), rect(-14, -8, 4, 18), rect(17, 17, 9, 5),
         rect(-4, 3, 16, 10), rect(19, -17, 2, 1), rect(14, 20, 9, 12), rect(-16, -5, 4, 9), rect(-10, -3, 2, 15),
         rect(11, 2, 6, 6), rect(-9, -18, 16, 19), rect(12, -13, 8, 19), rect(3, 4, 8, 20), rect(0, -16, 8, 17),
         rect(-8, 7, 18, 11), rect(11, -3, 16, 18), rect(7, 9, 1, 11), rect(15, 8, 7, 3), rect(7, 13, 19, 7),
         rect(19, 10, 2, 13), rect(20, -8, 15, 12), rect(-20, 17, 6, 16), rect(16, 15, 13, 9), rect(20, -18, 11, 18),
         rect(11, 19, 9, 7), rect(18, -13, 9, 11), rect(-1, 17, 14, 7), rect(13, 18, 10, 16), rect(2, 15, 17, 2),
         rect(5, 6, 4, 9), rect(-12, -19, 6, 1), rect(-2, 9, 17, 14), rect(10, 9, 1, 0), rect(-20, -10, 9, 11),
         rect(6, -20, 4, 12), rect(17, -18, 2, 11), rect(-9, 2, 18, 14), rect(-19, -13, 19, 3), rect(16, 0, 14, 7),
         rect(6, 10, 5, 16), rect(11, 11, 6, 8), rect(13, 19, 6, 16), rect(-7, -13, 8, 15), rect(10, -16, 0, 18),
         rect(-8, -8, 11, 14), rect(-6, 0, 0, 12), rect(14, -20, 16, 2), rect(-15, -3, 16, 10), rect(20, -5, 11, 15),
         rect(7, 14, 9, 10), rect(-15, -19, 9, 0), rect(5, 2, 20, 3), rect(-3, -17, 9, 18), rect(-18, -5, 3, 20),
         rect(11, 6, 6, 3), rect(-8, -17, 6, 0), rect(-9, 6, 3, 8), rect(-2, -11, 20, 11), rect(15, -16, 17, 1),
         rect(10, 2, 0, 20), rect(0, 0, 17, 1), rect(18, 7, 0, 18), rect(-12, -4, 8, 3), rect(2, -20, 9, 4),
         rect(4, 15, 2, 3), rect(20, -3, 18, 6), rect(-2, 16, 8, 4), rect(2, 10, 0, 4), rect(5, -11, 3, 13),
         rect(-17, 5, 20, 9), rect(-2, -12, 19, 14), rect(16, -6, 16, 9), rect(-15, -19, 5, 12), rect(7, -6, 3, 18),
         rect(-19, 0, 20, 8), rect(15, 20, 19, 6), rect(-4, -1, 6, 12), rect(6, -14, 15, 10), rect(-6, 18, 15, 10),
         rect(20, 12, 14, 18), rect(-12, 20, 20, 7), rect(7, 10, 12, 3), rect(-15, -19, 0, 20), rect(-11, 1, 3, 8),
         rect(0, -15, 11, 13), rect(8, -3, 1, 3), rect(-11, 2, 9, 5), rect(-1, -9, 3, 11), rect(19, -1, 15, 17),
         rect(20, -6, 1, 1), rect(13, 12, 8, 19), rect(-9, 5, 18, 4), rect(-5, -8, 10, 3), rect(-16, -4, 4, 16),
         rect(-18, 10, 12, 19), rect(-19, -9, 5, 8), rect(-16, -17, 7, 13), rect(19, -16, 13, 18), rect(-17, 13, 5, 1),
         rect(18, 1, 2, 9), rect(9, -15, 5, 14), rect(3, -17, 16, 2), rect(11, 11, 19, 14), rect(-14, 6, 5, 7),
         rect(-4, -13, 11, 0), rect(-7, 10, 3, 8), rect(-13, 12, 12, 4), rect(0, 18, 15, 12), rect(18, -10, 19, 0),
         rect(6, -17, 10, 8), rect(-4, 3, 13, 7), rect(11, -15, 19, 17), rect(-8, 7, 19, 17), rect(-3, -20, 14, 18),
         rect(11, 4, 10, 20), rect(-1, -8, 9, 1), rect(-18, 13, 16, 10), rect(-1, -1, 13, 10), rect(4, -2, 1, 3),
         rect(-6, 19, 10, 5), rect(-9, 4, 5, 4), rect(1, -16, 19, 7), rect(-3, 11, 16, 9), rect(-16, 2, 11, 15),
         rect(12, 19, 20, 3), rect(-15, 12, 18, 10), rect(-5, 18, 12, 8), rect(17, -19, 18, 9), rect(-3, 8, 20, 18),
         rect(-10, -7, 14, 4), rect(5, 8, 18, 14), rect(18, 2, 6, 12), rect(3, -3, 1, 17), rect(13, -16, 6, 15),
         rect(0, 7, 12, 14), rect(-9, -4, 8, 10), rect(13, -16, 17, 18), rect(12, -16, 1, 11), rect(-16, -7, 7, 15),
         rect(7, 3, 6, 1), rect(-3, -1, 7, 12), rect(-16, -11, 9, 8), rect(4, -10, 1, 16), rect(12, 20, 1, 10),
         rect(-16, -17, 11, 20), rect(-9, -15, 18, 15), rect(-11, -14, 3, 6), rect(14, 7, 5, 1), rect(-18, 8, 8, 6),
         rect(-14, -15, 0, 16), rect(20, -11, 0, 4), rect(19, 19, 4, 16), rect(3, -1, 14, 15), rect(18, -13, 15, 1),
         rect(-2, 16, 17, 12), rect(-11, -14, 0, 9), rect(-3, -1, 15, 12), rect(6, -1, 10, 9), rect(8, -3, 16, 6),
         rect(-2, -9, 14, 5), rect(-12, 20, 16, 2), rect(9, -8, 3, 3), rect(16, -3, 3, 9), rect(6, -4, 9, 4),
         rect(-18, -8, 6, 15), rect(10, 0, 19, 4), rect(-15, 20, 2, 8), rect(-18, 14, 11, 18), rect(-20, -7, 16, 17),
         rect(-14, 6, 13, 4), rect(8, -17, 14, 11), rect(-9, -3, 16, 8), rect(-6, 5, 6, 7), rect(16, 10, 2, 20),
         rect(4, 7, 18, 6), rect(-3, 2, 4, 6), rect(-4, -11, 14, 7), rect(10, 20, 5, 6), rect(19, -8, 7, 11),
         rect(-6, -6, 5, 6), rect(11, -18, 13, 4), rect(-9, 19, 7, 12), rect(13, 17, 2, 19), rect(20, -5, 17, 9),
         rect(11, 8, 11, 14), rect(-4, 10, 12, 19), rect(10, 2, 10, 20), rect(-7, -8, 3, 2), rect(-11, -2, 3, 13),
         rect(18, -20, 7, 5), rect(1, 7, 11, 12), rect(-2, -13, 1, 18), rect(-13, 11, 6, 13), rect(15, -19, 20, 1),
         rect(-5, -11, 9, 13), rect(0, 9, 2, 13), rect(-1, 19, 2, 18), rect(-14, 20, 9, 17), rect(-12, -4, 1, 3),
         rect(-11, 7, 7, 7), rect(18, -4, 1, 17), rect(-19, -14, 9, 11), rect(-3, -5, 12, 15), rect(-14, -20, 6, 18),
         rect(17, -19, 19, 1), rect(6, -6, 16, 9), rect(-20, 9, 6, 19), rect(-2, -20, 4, 13), rect(-20, 18, 10, 9),
         rect(5, -20, 10, 5), rect(-11, -20, 19, 12), rect(-2, 16, 3, 16), rect(-6, 4, 5, 19), rect(-17, 19, 2, 8),
         rect(9, 0, 8, 17), rect(6, 9, 13, 3), rect(-9, 20, 1, 5), rect(5, -4, 12, 6), rect(9, -18, 18, 17),
         rect(20, -18, 2, 15), rect(-14, -9, 15, 5), rect(-8, 11, 7, 20), rect(0, -6, 1, 8), rect(11, -1, 12, 5),
         rect(-12, -1, 0, 4), rect(0, 18, 6, 11), rect(4, 6, 7, 4), rect(5, -3, 2, 2), rect(-11, -9, 6, 15),
         rect(16, 1, 8, 8), rect(-10, -7, 14, 8), rect(-4, 4, 1, 16), rect(9, -2, 17, 8), rect(-3, -6, 5, 6),
         rect(-19, 7, 7, 1), rect(-11, -2, 5, 2), rect(3, -20, 10, 19), rect(-16, -16, 9, 13), rect(-8, -15, 19, 8),
         rect(-12, 0, 3, 4), rect(19, -3, 16, 13), rect(10, -17, 20, 11), rect(15, 11, 14, 15), rect(-4, 13, 7, 19),
         rect(20, -15, 9, 1), rect(7, 16, 1, 9), rect(10, -6, 18, 14), rect(1, -18, 17, 18), rect(-8, 7, 14, 5),
         rect(-7, 2, 17, 14), rect(11, 7, 4, 20), rect(-6, -4, 20, 6), rect(14, 1, 12, 14), rect(-17, 17, 10, 10),
         rect(13, 11, 15, 16), rect(4, 19, 16, 18), rect(-19, 20, 2, 6), rect(14, 10, 0, 15), rect(-2, 8, 11, 10),
         rect(20, -19, 12, 15), rect(18, -16, 15, 15), rect(11, -15, 15, 4), rect(-17, 2, 10, 8), rect(6, 11, 1, 16),
         rect(4, -20, 4, 9), rect(5, -11, 8, 3), rect(13, 16, 11, 3), rect(14, -12, 10, 2), rect(-13, 4, 18, 12),
         rect(14, 0, 15, 4), rect(-8, 12, 8, 16), rect(-19, 18, 5, 3), rect(14, -11, 7, 14), rect(-20, 15, 15, 13),
         rect(15, 8, 12, 9), rect(-1, 0, 6, 1), rect(9, -17, 2, 0), rect(16, -13, 1, 1), rect(-11, 15, 8, 6),
         rect(-19, 16, 4, 10), rect(-15, 12, 16, 11), rect(9, 14, 5, 6), rect(-2, -18, 14, 2), rect(-15, 4, 19, 5),
         rect(-13, 14, 15, 17), rect(17, 9, 1, 19), rect(19, 17, 20, 14), rect(-16, -12, 4, 7), rect(0, 15, 11, 5),
         rect(-18, 6, 3, 11), rect(-8, 10, 6, 3), rect(-11, -11, 9, 14), rect(16, -12, 18, 15), rect(10, -8, 9, 9),
         rect(0, 18, 12, 17), rect(-18, 18, 7, 18), rect(-7, 13, 6, 19), rect(-10, -17, 4, 15), rect(-16, 8, 15, 20),
         rect(0, 4, 1, 20), rect(8, 3, 1, 10), rect(4, 0, 11, 11), rect(9, 0, 15, 6), rect(8, -7, 0, 6),
         rect(18, -17, 19, 7), rect(2, -5, 1, 16), rect(-4, 4, 4, 8), rect(5, 5, 2, 10), rect(8, -7, 2, 4),
         rect(17, 7, 7, 20), rect(-18, -15, 9, 8), rect(6, 8, 3, 10), rect(-5, -11, 17, 11), rect(2, 5, 15, 12),
         rect(-5, -5, 18, 4), rect(-15, -8, 4, 9), rect(-18, -1, 1, 15), rect(-15, 1, 0, 7), rect(-7, 8, 6, 18),
         rect(9, 16, 20, 7), rect(15, -14, 12, 3), rect(-2, -1, 2, 16), rect(-5, 3, 4, 18), rect(12, -18, 2, 12),
         rect(3, -11, 10, 12), rect(-17, -16, 2, 13), rect(-7, 8, 6, 12), rect(-19, -7, 7, 20), rect(-6, 17, 6, 9),
         rect(-5, -14, 9, 11), rect(-10, 6, 9, 15), rect(2, -5, 9, 6), rect(15, 1, 0, 5), rect(6, 19, 11, 17),
         rect(-10, 0, 11, 8), rect(10, 20, 4, 10), rect(-18, -17, 1, 10), rect(-13, 14, 1, 1), rect(-9, -14, 17, 17),
         rect(-18, 11, 11, 1), rect(-4, -4, 20, 8), rect(-11, -19, 5, 1), rect(8, -7, 15, 7), rect(-16, -14, 15, 5),
         rect(-16, 19, 16, 18), rect(13, 10, 18, 11), rect(-8, -6, 15, 20), rect(-11, 10, 1, 12), rect(15, 17, 13, 4),
         rect(4, 15, 3, 16), rect(4, 15, 20, 7), rect(-6, -12, 11, 8), rect(2, 8, 5, 11), rect(8, -13, 0, 6),
         rect(15, 2, 13, 7), rect(17, 5, 15, 20), rect(14, 9, 10, 16), rect(12, 13, 18, 16), rect(-10, 17, 2, 12),
         rect(-10, 10, 14, 16), rect(-16, 15, 2, 8), rect(-12, -10, 1, 0), rect(-13, 6, 18, 2), rect(7, 4, 3, 2),
         rect(-20, -16, 9, 1), rect(5, 13, 18, 12), rect(7, -5, 6, 14), rect(-4, 16, 13, 5), rect(5, -8, 18, 10),
         rect(4, 17, 15, 12), rect(-17, 7, 2, 8), rect(14, 13, 17, 3), rect(17, -14, 15, 15), rect(15, -15, 6, 15),
         rect(-8, -4, 3, 10), rect(0, 0, 7, 15), rect(16, -8, 13, 4), rect(14, 20, 1, 2), rect(-14, 3, 18, 13),
         rect(16, 14, 1, 12), rect(17, 16, 16, 19), rect(7, 14, 20, 3), rect(9, -6, 13, 18), rect(10, 19, 4, 4),
         rect(0, 10, 0, 16), rect(9, -9, 10, 2), rect(17, -9, 17, 20), rect(-4, -5, 13, 15), rect(-10, 6, 15, 5),
         rect(-18, 20, 17, 4), rect(3, -4, 7, 14), rect(-12, -12, 14, 7), rect(0, 17, 14, 1), rect(-4, -19, 19, 4),
         rect(1, 2, 11, 2), rect(17, 9, 4, 7), rect(-9, -4, 1, 2), rect(-4, 10, 18, 19), rect(-14, 0, 18, 2),
         rect(5, 8, 7, 11), rect(16, -2, 12, 11), rect(-4, -20, 12, 13), rect(2, -9, 12, 13), rect(15, 19, 5, 8),
         rect(7, -4, 17, 16), rect(-10, -6, 17, 15), rect(-1, 6, 6, 18), rect(-8, 1, 9, 14), rect(10, -2, 1, 11),
         rect(10, -20, 4, 20), rect(-9, -13, 14, 3), rect(-1, 8, 10, 20), rect(-16, 3, 3, 3), rect(13, -17, 18, 11),
         rect(-11, 7, 1, 6), rect(-2, -16, 11, 7), rect(-10, 0, 8, 13), rect(4, 0, 3, 18), rect(0, 8, 7, 19),
         rect(-20, 5, 17, 15), rect(9, 12, 15, 3), rect(-10, 7, 3, 3), rect(-10, 8, 6, 5), rect(11, -20, 9, 15),
         rect(-13, 7, 18, 8), rect(6, 20, 18, 6), rect(5, 1, 20, 10), rect(20, 9, 1, 2), rect(-20, -15, 16, 13),
         rect(15, -6, 5, 6), rect(-18, 14, 13, 16), rect(2, 7, 8, 15), rect(-20, 12, 5, 18), rect(-4, 0, 7, 2),
         rect(20, 17, 13, 14), rect(1, -6, 5, 13), rect(9, 13, 16, 16), rect(-16, 5, 20, 6), rect(-20, 0, 12, 0),
         rect(-17, -9, 16, 5), rect(12, 11, 8, 17), rect(18, -14, 14, 1), rect(-4, -11, 17, 5), rect(-18, 9, 14, 19),
         rect(-8, 2, 10, 14), rect(-6, -5, 0, 8), rect(-1, 9, 2, 10), rect(2, -9, 0, 1), rect(-11, 13, 9, 17),
         rect(-9, -12, 17, 19), rect(-19, 3, 14, 14), rect(-16, -9, 13, 16), rect(10, -5, 6, 18), rect(7, 9, 16, 15),
         rect(16, 13, 16, 8), rect(-18, 3, 13, 14), rect(17, -13, 16, 2), rect(10, -11, 14, 2), rect(14, -20, 12, 20),
         rect(4, 4, 10, 5), rect(-18, 4, 13, 18), rect(2, 16, 7, 15), rect(11, 20, 4, 0), rect(-13, 10, 18, 11),
         rect(-9, -1, 19, 10), rect(1, -19, 13, 20), rect(0, -15, 14, 4), rect(-5, 17, 16, 1), rect(-3, -2, 7, 7),
         rect(10, 14, 10, 4), rect(10, -5, 3, 11), rect(6, 16, 3, 3), rect(8, -18, 18, 0), rect(-8, 20, 11, 6),
         rect(10, 16, 12, 3), rect(2, 17, 12, 0), rect(-1, -6, 7, 11), rect(-17, -7, 8, 0), rect(2, 2, 11, 6),
         rect(1, -9, 20, 3), rect(-7, 9, 14, 9), rect(0, 1, 3, 20), rect(-14, 2, 6, 4), rect(16, -17, 15, 4),
         rect(6, -9, 1, 9)]
    )

    rects.append([rect(-15, -15, 30, 5), rect(15, -15, 10, 5), rect(-15, -10, 40, 35)])

    for info in rect_test_cases_generator():
        lst, blah = info
        tup = lst[0]
        rects.append(tup)

    # Some colors and translations for our rects.
    colors = [Color(c) for c in (
        # 'gray1',
        'red1', 'green1', 'blue1', 'orange1', 'purple1',
        'red3', 'green3', 'blue3', 'orange3', 'purple3',)]
    offset_before = 30, 30
    offset_after = 100, 30
    scale = 5

    # Get the first pair and split 'em.
    index = 0
    in_rects = rects[index]
    split_func = split_dirty_rects
    frags = split_func(in_rects)

    # Display what we got.
    running = True
    while running:
        # clock.tick(10)
        screen.fill(erase_color)
        # # events will select rects to frag and display
        for e in [pygame.event.wait()]:  # pygame.event.get():
            if e.type == pygame.QUIT:
                quit()
            if e.type == KEYDOWN:
                if e.key == K_ESCAPE:
                    quit()
                if e.key == K_SPACE:
                    if split_func == split_dirty_rects:
                        split_func = optimize_dirty_rects
                    else:
                        split_func = split_dirty_rects
                elif e.key == K_LEFT or e.key == K_UP:
                    index -= 1
                else:
                    index += 1
                index %= len(rects)
                in_rects = rects[index]
                frags = split_func([r.copy() for r in in_rects])
                print(index, in_rects, frags)
                pygame.display.set_caption(str(index) + " " + split_func.__name__)
            elif e.type == MOUSEBUTTONDOWN:
                x, y = e.pos
                colliding = pygame.Rect(x / scale - offset_after[0], y / scale - offset_after[1], 1, 1).collidelistall(
                    frags)
                if colliding:
                    for idx in colliding:
                        print("colliding", frags[idx])
                    print("-----")

        # # display rects before split
        for i, r in enumerate(in_rects):
            translated_rect = [n * scale for n in r.move(offset_before)]
            j = i % len(colors)
            pygame.draw.rect(screen, colors[j], translated_rect, 1)

        # display frags after split
        for i, f in enumerate(frags):
            j = (i + 4) % len(colors)
            translated_rect = f.move(offset_after)
            translated_rect = [n * scale for n in translated_rect]
            pygame.draw.rect(screen, colors[j], translated_rect, 1)

        pygame.display.flip()


if __name__ == '__main__':
    main()
