# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek19-2014-10  TODO: move this to project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
# * Neither the name of the <organization --TODO: author? project? -- > nor the
# names of its contributors may be used to endorse or promote products
# derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This module contains methods for splitting a rectangles into other rectangles such that there are no overlapping
areas. This module depends on pygame, it uses pygame.Rect and its collidedictall method. This code was inspired
by:

https://bitbucket.org/jmm0/optimize_dirty_rects


To see how many ways two rectangles can overlap see following url:

http://gandraxa.com/detect_overlapping_subrectangles.xml

The table show the possible overlapping of two rectangles. Each case has to be considered to split them
correctly into no overlapping rectangles.

                0     1     2     3     4     5     6     7     8     9    10    11    12    13    14    15
              ----  ---L  --TL  --T-  -BT-  -BTL  -B-L  -B--  RB--  RB-L  RBTL  RBT-  R-T-  R-TL  R--L  R---

              OOO   OOO   §§O   O§O   O§O   §OO   OOO   OOO   OOO   OOO   §§§   OO§   O§§   §§§   OOO   OOO
              O§O   §OO   §§O   OOO   O§O   §OO   §§O   OOO   O§§   OOO   §§§   OO§   O§§   OOO   §§§   OO§
              OOO   OOO   OOO   OOO   O§O   §OO   §§O   O§O   O§§   §§§   §§§   OO§   OOO   OOO   OOO   OOO
            +     +     +     +     +     +     +     +     +     +     +     +     +     +     +     +     +
        OOO                                                               @@@
 0 ---- O§O                                                               @#@
        OOO                                                               @@@
            +     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     +
        OOO                                                               @@@   @@
 1 ---L §OO                                                               #@@  O#@
        OOO                                                               @@@   @@
            +     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     +
        §§O                                                   OO    OO    ##@   O#@
 2 --TL §§O                                                   O#@   ##@   ##@   O#@
        OOO                                                    @@   @@@   @@@    @@
            +     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     +
        O§O                                                          O    @#@
 3 --T- OOO                                                         @#@   @@@
        OOO                                                         @@@   @@@
            +     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     +
        0§0                                                          O    @#@               @#@    O
 4 -BT- 0§0                                                          O    @#@                O    @#@
        0§0                                                         @#@   @#@                O     O
            +     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     +
        §OO                                                   OO    O     ##@   O#@   O#@   #@@   O     OO
 5 -BTL §OO                                                   O#@   O     ##@   O#@   O#@   O     #@@   O#@
        §OO                                                   O#@   #@@   ##@   O#@   OO    O     O     OO
            +     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     +
        OOO                                                               @@@    @@    @@   @@@
 6 -B-L §§O                                                               @@@   O#@   O#@   ##@
        §§O                                                               #@@   O#@   OO    OO
            +     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     +
        OOO                                                               @@@               @@@
 7 -B-- OOO                                                               @@@               @#@
        O§O                                                               @#@                O
            +     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     +
        OOO               @@                OO                            @@@               @@@
 8 RB-- O§§               @#O               O#@                           @##               @##
        O§§                OO               O#@                           @##                OO
            +     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     +
        OOO               @@     @    @     @                             @@@     @   @@    @@@
 9 RB-L OOO               ##O   O#O   @     @                             ###     @  O##    ###
        §§§               OOO   OOO  O#O    #OO                           ###   OO#  OOO    OOO
            +     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     +
        §§§   OOO   OOO   ##O   O#O   O#O   ##O   OOO   OOO   OOO   OOO   ###   O##   O##   ###   OOO   OOO
10 RBTL §§§   O#O   #OO   ##O   OOO   O#O   ##O   ##O   OOO   O##   OOO   ###   O##   O##   OOO   ###   OO#
        §§§   OOO   OOO   OOO   OOO   O#O   ##O   ##O   O#O   O##   ###   ###   O##   OOO   OOO   OOO   OOO
            +     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     +
        OO§          OO   @#O               @#O    OO                 O   @##               @@#     O
11 RBT- OO§         @#O   @#O               @#O   @#O                 O   @##                 O   @@#
        OO§          OO    OO               @#O   @#O               @@#   @##                 O     O
            +     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     +
        O§§                                 @#O    OO                OO   @##
12 R-T- O§§                                 @#O   @#O               @##   @##
        OOO                                 @@    @@                @@@   @@@
            +     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     +
        §§§                           O#O   #OO   OOO   OOO   OOO   OOO   ###   OO#
13 R-TL §§§                            @    @     ##O   O#O   O##   ###   ###     @
        OOO                            @    @     @@     @     @@   @@@   @@@     @
            +     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     +
        OOO                            @   @                              @@@     @
14 R--L §§§                           O#O  #OO                            ###   OO#
        OOO                            @   @                              @@@     @
            +     .     .     .     .     .     .     .     .     .     .     .     .     .     .     .     +
        OOO                                 @@                            @@@
15 R--- OO§                                 @#O                           @@#
        OOO                                 @@                            @@@
            +     +     +     +     +     +     +     +     +     +     +     +     +     +     +     +     +








Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2014"
__credits__ = ["DR0ID, Gummbum"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

from collections import deque

from pygame import Rect

_inflate_amount = 2


def split_dirty_rects(dirty_rects, do_copy_rects=True):
    """
    Splits a list of rectangles into a new list such that there are no overlapping areas.
    See module description for more details.

    :param dirty_rects: the list to check for overlapping rects.
    :param do_copy_rects: if true, a copy of the rects will be used, otherwise they might be modified. Default: True
    :return: list of non overlapping rects covering the same area as the passed in rects.
    """
    if len(dirty_rects) <= 1:
        if do_copy_rects:
            return [Rect(dirty_rects[0])]
        else:
            return dirty_rects

    queue = deque()
    queue_append = queue.append
    queue_pop = queue.pop
    if do_copy_rects:
        for r in dirty_rects:
            if r and r.w > 0 and r.h > 0:
                r.normalize()
                queue_append(r.copy())
    else:
        for r in dirty_rects:
            if r and r.w > 0 and r.h > 0:
                r.normalize()
                queue_append(r)

    r1 = queue_pop()
    all_r_union = r1.unionall(queue)
    if r1 == all_r_union:
        # shortcut
        return [r1]
    for r2 in queue:
        if r2 == all_r_union:
            # shortcut
            return [r2]

    optimized = {tuple(r1): r1}

    while queue:

        r1 = queue_pop()
        collisions = r1.collidedictall(optimized)

        while collisions:
            key, r2 = collisions.pop(-1)

            if r1[1] < r2[1]:
                if r1[0] > r2[0]:
                    if r1.right > r2.right:
                        if r1.bottom > r2.bottom:
                            # raise Exception("1-11 " + str(r1) + str(r2))
                            queue_append(Rect(r1.left, r2.bottom, r1.w, r1.bottom - r2.bottom))
                            r2.w = r1.right - r2[0]
                            r1.h = r2[1] - r1[1]
                            del optimized[key]
                            queue_append(r2)
                            collisions = r1.collidedictall(optimized)
                        elif r1[1] + r1[3] == r2[1] + r2[3]:
                            # raise Exception("11-6 " + str(r1) + str(r2))
                            r1.h = r2.top - r1.top
                            r2.w = r1.right - r2.left
                            queue_append(r2)  # got bigger, check for collisions again
                            del optimized[key]
                            collisions = r1.collidedictall(optimized)
                        else:  # r1b < r2b
                            # raise Exception("12-6 " + str(r1) + str(r2))
                            queue_append(Rect(r2.left, r1.bottom, r2.w, r2.bottom - r1.bottom))
                            r2.w = r1.right - r2.left
                            r2.h = r1.bottom - r2.top
                            queue_append(r2)  # got bigger, check for collisions again
                            r1.h = r2.top - r1.top
                            del optimized[key]
                            collisions = r1.collidedictall(optimized)
                    elif r1.right < r2.right:
                        if r1.bottom > r2.bottom:
                            # raise Exception("14-4 " + str(r1) + str(r2))
                            queue_append(Rect(r1.left, r2.bottom, r1.w, r1.bottom - r2.bottom))
                            r1.h = r2.top - r1.top
                            collisions = r1.collidedictall(optimized)
                        elif r1.bottom == r2.bottom:
                            # raise Exception("9-4 " + str(r1) + str(r2))
                            r1.h = r2.top - r1.top
                            collisions = r1.collidedictall(optimized)
                        else:  # r1b < r2b
                            # raise Exception("9-3 " + str(r1) + str(r2))
                            r1.h = r2.top - r1.top
                            collisions = r1.collidedictall(optimized)
                    else:  # r1r == r2r
                        if r1.bottom > r2.bottom:
                            # raise Exception("11-14 " + str(r1) + str(r2))
                            queue_append(Rect(r1[0], r2.bottom, r1[2], r1.bottom - r2.bottom))
                            r1.h = r2[1] - r1[1]
                            collisions = r1.collidedictall(optimized)
                        elif r1.bottom == r2.bottom:
                            # raise Exception("9-11 " + str(r1) + str(r2))
                            r1.h = r2.top - r1.top
                            collisions = r1.collidedictall(optimized)
                        else:  # r1b < r2b
                            # raise Exception("9-12 " + str(r1) + str(r2))
                            r1.h = r2.top - r1.top
                            collisions = r1.collidedictall(optimized)
                elif r1.left == r2.left:
                    if r1.right > r2.right:
                        if r1.bottom > r2.bottom:
                            # raise Exception("1-10 " + str(r1) + str(r2))
                            del optimized[key]
                        elif r1.bottom == r2.bottom:
                            # raise Exception("6-10 " + str(r1) + str(r2))
                            del optimized[key]
                        else:  # r1b < r2b
                            # raise Exception("6-13 " + str(r1) + str(r2))
                            r2.h = r2.bottom - r1.bottom
                            r2.top = r1.bottom
                            del optimized[key]
                            queue_append(r2)
                    elif r1.right < r2.right:
                        if r1.bottom > r2.bottom:
                            # raise Exception("14-5 " + str(r1) + str(r2))
                            queue_append(Rect(r2.left, r2.bottom, r1.w, r1.bottom - r2.bottom))
                            r1.h = r2.top - r1.top
                            collisions = r1.collidedictall(optimized)
                        elif r1.bottom == r2.bottom:
                            # raise Exception("9-5 " + str(r1) + str(r2))
                            r1.h = r2.top - r1.top
                            collisions = r1.collidedictall(optimized)
                        else:  # r1b < r2b
                            # raise Exception("9-2 " + str(r1) + str(r2))
                            r1.h = r2.top - r1.top
                            collisions = r1.collidedictall(optimized)
                    else:  # r1r == r2r
                        if r1.bottom > r2.bottom:
                            # raise Exception("14-10 " + str(r1) + str(r2))
                            del optimized[key]
                        elif r1.bottom == r2.bottom:
                            # raise Exception("9-10 " + str(r1) + str(r2))
                            del optimized[key]
                        else:  # r1b < r2b
                            # raise Exception("9-13 " + str(r1) + str(r2))
                            r1.h = r2.bottom - r1.top
                            # shortcut
                            if r1 == all_r_union:
                                return [r1]
                            del optimized[key]
                            collisions = r1.collidedictall(optimized)

                else:  # r1l < r2l
                    if r1.right > r2.right:
                        if r1.bottom > r2.bottom:
                            # raise Exception("0-10 " + str(r1) + str(r2))
                            del optimized[key]
                        elif r1.bottom == r2.bottom:
                            # raise Exception("7-10 " + str(r1) + str(r2))
                            del optimized[key]
                        else:  # r1b < r2b
                            # raise Exception("7-13 " + str(r1) + str(r2))
                            r2.h = r2.bottom - r1.bottom
                            r2.top = r1.bottom
                            del optimized[key]
                            queue_append(r2)

                    elif r1.right < r2.right:
                        if r1.bottom > r2.bottom:
                            # raise Exception("15-5 " + str(r1) + str(r2))
                            queue_append(Rect(r1.left, r2.bottom, r1.w, r1.bottom - r2.bottom))
                            r2.w = r2.right - r1.left
                            r2.left = r1.left
                            r1.h = r2.top - r1.top
                            del optimized[key]
                            queue_append(r2)  # got bigger, check again for collisions
                            collisions = r1.collidedictall(optimized)
                        elif r1.bottom == r2.bottom:
                            # raise Exception("8-5 " + str(r1) + str(r2))
                            r1.h = r2.top - r1.top
                            r2.w = r2.right - r1.left
                            r2.left = r1.left
                            del optimized[key]
                            queue_append(r2)  # got bigger, check again for collisions
                            collisions = r1.collidedictall(optimized)
                        else:  # r1b < r2b
                            # raise Exception("8-2 " + str(r1) + str(r2))
                            queue_append(Rect(r1.left, r2.top, r2.right - r1.left, r1.bottom - r2.top))
                            r2_top = r2.top
                            r2.h = r2.bottom - r1.bottom
                            r2.top = r1.bottom
                            r1.h = r2_top - r1.top
                            del optimized[key]
                            queue_append(r2)  # got bigger, check again for collisions
                            collisions = r1.collidedictall(optimized)
                    else:  # r1r == r2r
                        if r1.bottom > r2.bottom:
                            # raise Exception("15-10 " + str(r1) + str(r2))
                            del optimized[key]
                        elif r1.bottom == r2.bottom:
                            # raise Exception("8-10 " + str(r1) + str(r2))
                            del optimized[key]
                        else:  # r1b < r2b
                            # raise Exception("8-13 " + str(r1) + str(r2))
                            r2.h = r2.bottom - r1.bottom
                            r2.top = r1.bottom
                            del optimized[key]
                            queue_append(r2)

            elif r1.top == r2.top:
                if r1.left > r2.left:
                    if r1.right > r2.right:
                        if r1.bottom > r2.bottom:
                            # raise Exception("2-11 " + str(r1) + str(r2))
                            r2.w = r1.right - r2.left
                            r1.h = r1.bottom - r2.bottom
                            r1.top = r2.bottom
                            del optimized[key]
                            queue_append(r2)  # got bigger, check again for collisions
                            collisions = r1.collidedictall(optimized)
                        elif r1.bottom == r2.bottom:
                            # raise Exception("5-11 " + str(r1) + str(r2))
                            r1.w = r1.right - r2.left
                            r1.left = r2.left
                            if r1 == all_r_union:
                                return [r1]
                            del optimized[key]

                        else:  # r1b < r2b
                            # raise Exception("5-12 " + str(r1) + str(r2))
                            r1.w = r1.right - r2.left
                            r1.left = r2.left
                            r2.h = r2.bottom - r1.bottom
                            r2.top = r1.bottom
                            del optimized[key]
                            queue_append(r2)
                            collisions = r1.collidedictall(optimized)
                    elif r1.right < r2.right:
                        if r1.bottom > r2.bottom:
                            # raise Exception("13-4 " + str(r1) + str(r2))
                            r1.h = r1.bottom - r2.bottom
                            r1.top = r2.bottom
                            collisions = r1.collidedictall(optimized)
                        elif r1.bottom == r2.bottom:
                            # raise Exception("10-4 " + str(r1) + str(r2))
                            break  # forget about r1
                        else:  # r1b < r2b
                            # raise Exception("10-3 " + str(r1) + str(r2))
                            break  # forget about r1
                    else:  # r1r == r2r
                        if r1.bottom > r2.bottom:
                            # raise Exception("13-11 " + str(r1) + str(r2))
                            r1.h = r1.bottom - r2.bottom
                            r1.top = r2.bottom
                            collisions = r1.collidedictall(optimized)
                        elif r1.bottom == r2.bottom:
                            # raise Exception("10-11 " + str(r1) + str(r2))
                            break  # forget about r1
                        else:  # r1b < r2b
                            # raise Exception("10-12 " + str(r1) + str(r2))
                            break  # forget about r1
                elif r1.left == r2.left:
                    if r1.right > r2.right:
                        if r1.bottom > r2.bottom:
                            # raise Exception("2-10 " + str(r1) + str(r2))
                            del optimized[key]
                        elif r1.bottom == r2.bottom:
                            # raise Exception("5-10 " + str(r1) + str(r2))
                            del optimized[key]
                        else:  # r1b < r2b
                            # raise Exception("5-13 " + str(r1) + str(r2))
                            r2.h = r2.bottom - r1.bottom
                            r2.top = r1.bottom
                            del optimized[key]
                            queue_append(r2)  # got bigger, check again for collisions
                    elif r1.right < r2.right:
                        if r1.bottom > r2.bottom:
                            # raise Exception("13-5 " + str(r1) + str(r2))
                            r1.h = r1.bottom - r2.bottom
                            r1.top = r2.bottom
                            collisions = r1.collidedictall(optimized)
                        elif r1.bottom == r2.bottom:
                            # raise Exception("10-5 " + str(r1) + str(r2))
                            break  # forget about r1
                        else:  # r1b < r2b
                            # raise Exception("10-2 " + str(r1) + str(r2))
                            break  # forget about r1
                    else:  # r1r == r2r
                        if r1.bottom > r2.bottom:
                            # raise Exception("13-10 " + str(r1) + str(r2))
                            del optimized[key]
                        elif r1.bottom == r2.bottom:
                            # raise Exception("10-10 " + str(r1) + str(r2))
                            break  # forget about r1
                        else:  # r1b < r2b
                            # raise Exception("10-13 " + str(r1) + str(r2))
                            break  # roget about r1
                else:  # r1l < r2l
                    if r1.right > r2.right:
                        if r1.bottom > r2.bottom:
                            # raise Exception("3-10 " + str(r1) + str(r2))
                            del optimized[key]
                        elif r1.bottom == r2.bottom:
                            # raise Exception("4-10 " + str(r1) + str(r2))
                            del optimized[key]
                        else:  # r1b < r2b
                            # raise Exception("4-13 " + str(r1) + str(r2))
                            r2.h = r2.bottom - r1.bottom
                            r2.top = r1.bottom
                            del optimized[key]
                            queue_append(r2)
                    elif r1.right < r2.right:
                        if r1.bottom > r2.bottom:
                            # raise Exception("12-5 " + str(r1) + str(r2))
                            r2.w = r2.right - r1.left
                            r2.left = r1.left
                            r1.h = r1.bottom - r2.bottom
                            r1.top = r2.bottom
                            del optimized[key]
                            queue_append(r2)  # got bigger, check again for collisions
                            collisions = r1.collidedictall(optimized)
                        elif r1.bottom == r2.bottom:
                            # raise Exception("11-5" + str(r1) + str(r2))
                            r1.w = r2.right - r1.left
                            # shortcut
                            if r1 == all_r_union:
                                return [r1]
                            del optimized[key]
                            collisions = r1.collidedictall(optimized)
                        else:  # r1b < r2b
                            # raise Exception("11-2 " + str(r1) + str(r2))
                            r1.w = r2.right - r1.left
                            r2.h = r2.bottom - r1.bottom
                            r2.top = r1.bottom
                            del optimized[key]
                            queue_append(r2)
                            collisions = r1.collidedictall(optimized)
                    else:  # r1r == r2r
                        if r1.bottom > r2.bottom:
                            # raise Exception("12-10 " + str(r1) + str(r2))
                            del optimized[key]
                        elif r1.bottom == r2.bottom:
                            # raise Exception("11-10 " + str(r1) + str(r2))
                            del optimized[key]
                        else:  # r1b < r2b
                            # raise Exception("11-13 " + str(r1) + str(r2))
                            r2.h = r2.bottom - r1.bottom
                            r2.top = r1.bottom
                            del optimized[key]
                            queue_append(r2)

            else:  # r1t > r2t
                if r1.left > r2.left:
                    if r1.right > r2.right:
                        if r1.bottom > r2.bottom:
                            # raise Exception("2-8 " + str(r1) + str(r2))
                            queue_append(Rect(r2.left, r1.top, r1.right - r2.left, r2.bottom - r1.top))
                            r2.h = r1.top - r2.top
                            r1.h = r1.bottom - r2.bottom
                            r1.top = r2.bottom
                            del optimized[key]
                            queue_append(r2)
                            collisions = r1.collidedictall(optimized)
                        elif r1.bottom == r2.bottom:
                            # raise Exception("5-8 " + str(r1) + str(r2))
                            r2.h = r1.top - r2.top
                            r1.w = r1.right - r2.left
                            r1.left = r2.left
                            del optimized[key]
                            queue_append(r2)
                            collisions = r1.collidedictall(optimized)
                        else:  # r1b < r2b
                            # raise Exception("5-15 " + str(r1) + str(r2))
                            queue_append(Rect(r2.left, r1.bottom, r2.w, r2.bottom - r1.bottom))
                            r2.h = r1.top - r2.top
                            r1.w = r1.right - r2.left
                            r1.left = r2.left
                            del optimized[key]
                            queue_append(r2)
                            collisions = r1.collidedictall(optimized)
                    elif r1.right < r2.right:
                        if r1.bottom > r2.bottom:
                            # raise Exception("13-7 " + str(r1) + str(r2))
                            r1.h = r1.bottom - r2.bottom
                            r1.top = r2.bottom
                            collisions = r1.collidedictall(optimized)
                        elif r1.bottom == r2.bottom:
                            # raise Exception("10-7 " + str(r1) + str(r2))
                            break  # forget about r1
                        else:  # r1b < r2b
                            # raise Exception("10-0 " + str(r1) + str(r2))
                            break  # forget about r1
                    else:  # r1r == r2r
                        if r1.bottom > r2.bottom:
                            # raise Exception("13-8 " + str(r1) + str(r2))
                            r1.h = r1.bottom - r2.bottom
                            r1.top = r2.bottom
                            collisions = r1.collidedictall(optimized)
                        elif r1.bottom == r2.bottom:
                            # raise Exception("10-8 " + str(r1) + str(r2))
                            break  # forget about r1
                        else:  # r1b < r2b
                            # raise Exception("10-15 " + str(r1) + str(r2))
                            break  # forget about r1
                elif r1.left == r2.left:
                    if r1.right > r2.right:
                        if r1.bottom > r2.bottom:
                            # raise Exception("2-9 " + str(r1) + str(r2))
                            r2.h = r1.top - r2.top
                            del optimized[key]
                            queue_append(r2)
                        elif r1.bottom == r2.bottom:
                            # raise Exception("5-9 " + str(r1) + str(r2))
                            r2.h = r1.top - r2.top
                            del optimized[key]
                            queue_append(r2)
                        else:  # r1b < r2b
                            # raise Exception("5-14 " + str(r1) + str(r2))
                            queue_append(Rect(r1.left, r1.bottom, r2.w, r2.bottom - r1.bottom))
                            r2.h = r1.top - r2.top
                            del optimized[key]
                            queue_append(r2)
                    elif r1.right < r2.right:
                        if r1.bottom > r2.bottom:
                            # raise Exception("13-6" + str(r1) + str(r2))
                            r1.h = r1.bottom - r2.bottom
                            r1.top = r2.bottom
                            collisions = r1.collidedictall(optimized)
                        elif r1.bottom == r2.bottom:
                            # raise Exception("10-6 " + str(r1) + str(r2))
                            break  # forget about r1
                        else:  # r1b < r2b
                            # raise Exception("10-1 " + str(r1) + str(r2))
                            break  # forget about r1
                    else:  # r1r == r2r
                        if r1.bottom > r2.bottom:
                            # raise Exception("13-9 " + str(r1) + str(r2))
                            r1.h = r1.bottom - r2.top
                            r1.top = r2.top
                            # shortcut
                            if r1 == all_r_union:
                                return [r1]
                            del optimized[key]
                            collisions = r1.collidedictall(optimized)
                        elif r1.bottom == r2.bottom:
                            # raise Exception("10-9 " + str(r1) + str(r2))
                            break  # forget about r1
                        else:  # r1b < r2b
                            # raise Exception("10-14 " + str(r1) + str(r2))
                            break  # forget about r1

                else:  # r1l < r2l
                    if r1.right > r2.right:
                        if r1.bottom > r2.bottom:
                            # raise Exception("3-9 " + str(r1) + str(r2))
                            r2.h = r1.top - r2.top
                            del optimized[key]
                            queue_append(r2)
                        elif r1.bottom == r2.bottom:
                            # raise Exception("4-9 " + str(r1) + str(r2))
                            r2.h = r1.top - r2.top
                            del optimized[key]
                            queue_append(r2)
                        else:  # r1b < r2b
                            # raise Exception("4-14 " + str(r1) + str(r2))
                            queue_append(Rect(r2.left, r1.bottom, r2.w, r2.bottom - r1.bottom))
                            r2.h = r1.top - r2.top
                            del optimized[key]
                            queue_append(r2)
                    elif r1.right < r2.right:
                        if r1.bottom > r2.bottom:
                            # raise Exception("12-6 " + str(r1) + str(r2))
                            r2_bottom = r2.bottom
                            queue_append(Rect(r1.left, r2_bottom, r1.w, r1.bottom - r2_bottom))
                            r2.h = r1.top - r2.top
                            r1.w = r2.right - r1.left
                            r1.h = r2_bottom - r1.top
                            del optimized[key]
                            queue_append(r2)
                            collisions = r1.collidedictall(optimized)
                        elif r1.bottom == r2.bottom:
                            # raise Exception("11-6 " + str(r1) + str(r2))
                            r2.h = r1.top - r2.top
                            r1.w = r2.right - r1.left
                            del optimized[key]
                            queue_append(r2)
                            collisions = r1.collidedictall(optimized)
                        else:  # r1b < r2b
                            # raise Exception("11-1 " + str(r1) + str(r2))
                            queue_append(Rect(r2.left, r1.bottom, r2.w, r2.bottom - r1.bottom))
                            r2.h = r1.top - r2.top
                            r1.w = r2.right - r1.left
                            del optimized[key]
                            queue_append(r2)
                            collisions = r1.collidedictall(optimized)
                    else:  # r1r == r2r
                        if r1.bottom > r2.bottom:
                            # raise Exception("12-9 " + str(r1) + str(r2))
                            r2.h = r1.top - r2.top
                            del optimized[key]
                            queue_append(r2)
                        elif r1.bottom == r2.bottom:
                            # raise Exception("11-9 " + str(r1) + str(r2))
                            r2.h = r1.top - r2.top
                            del optimized[key]
                            queue_append(r2)
                        else:  # r1b < r2b
                            # raise Exception("11-14 " + str(r1) + str(r2))
                            queue_append(Rect(r2.left, r1.bottom, r2.w, r2.bottom - r1.bottom))
                            r2.h = r1.top - r2.top
                            del optimized[key]
                            queue_append(r2)
        else:
            # check to see if r1 has a common side with another one, union them if possible
            # the inflate is needed because a rect lying exactly on the right or bottom side
            # is not considered colliding, but should be considered to union it
            collisions = r1.inflate(_inflate_amount, _inflate_amount).collidedictall(optimized)

            while collisions:
                key, r2 = collisions.pop()

                if r1.topleft == r2.topright and r1.bottomleft == r2.bottomright:
                    del optimized[key]
                    r1.union_ip(r2)
                    collisions = r1.inflate(_inflate_amount, _inflate_amount).collidedictall(optimized)
                if r1.topright == r2.topleft and r1.bottomright == r2.bottomleft:
                    del optimized[key]
                    r1.union_ip(r2)
                    collisions = r1.inflate(_inflate_amount, _inflate_amount).collidedictall(optimized)
                if r1.topleft == r2.bottomleft and r1.topright == r2.bottomright:
                    del optimized[key]
                    r1.union_ip(r2)
                    collisions = r1.inflate(_inflate_amount, _inflate_amount).collidedictall(optimized)
                if r1.bottomleft == r2.topleft and r1.bottomright == r2.topright:
                    del optimized[key]
                    r1.union_ip(r2)
                    collisions = r1.inflate(_inflate_amount, _inflate_amount).collidedictall(optimized)

                # if r1.left == r2.left and r1.right == r2.right and (r1.top == r2.bottom or r1.bottom == r2.top):
                # del optimized[key]
                #     r1.union_ip(r2)
                #     collisions = r1.inflate(delta, delta).collidedictall(optimized)
                # if r1.top == r2.top and r1.bottom == r2.bottom and (r1.left == r2.right or r1.right == r2.left):
                #     del optimized[key]
                #     r1.union_ip(r2)
                #     collisions = r1.inflate(delta, delta).collidedictall(optimized)

                # shortcut
                if r1 == all_r_union:
                    return [r1]

            optimized[tuple(r1)] = r1

    return list(optimized.values())
