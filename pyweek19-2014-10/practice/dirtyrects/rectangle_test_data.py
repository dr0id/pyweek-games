# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek19-2014-10
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
# * Neither the name of the <organization> nor the
# names of its contributors may be used to endorse or promote products
# derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version


http://gandraxa.com/detect_overlapping_subrectangles.xml

https://bitbucket.org/jmm0/optimize_dirty_rects/src/c2affd5450b57fc142c919de10e530e367306222/optimize_dirty_rects.py?at=default





"""
from __future__ import print_function


__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(side_length) for side_length in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2014"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module


R = 0b1000  # right
B = 0b0100  # bottom
T = 0b0010  # top
L = 0b0001  # left

opposite_side = dict([(R, L), (B, T), (T, B), (L, R)])
opposite_vertex = dict([(T | R, B | L), (B | R, T | L), (T | L, B | R), (B | L, T | R)])

# header
# gray code sequence (does not need to be)
header = [
    0 | 0 | 0 | 0,
    0 | 0 | 0 | L,
    0 | 0 | T | L,
    0 | 0 | T | 0,
    0 | B | T | 0,
    0 | B | T | L,
    0 | B | 0 | L,
    0 | B | 0 | 0,
    R | B | 0 | 0,
    R | B | 0 | L,
    R | B | T | L,
    R | B | T | 0,
    R | 0 | T | 0,
    R | 0 | T | L,
    R | 0 | 0 | L,
    R | 0 | 0 | 0
]

import pygame


# make extension methods out of this? <= not possible:
# TypeError: can't set attributes of built-in/extension type 'pygame.Rect'
def expand_right(rect, distance):
    """
    Expands the right side of a pygame.Rect instance.
    :param rect: the rect which is expanded.
    :param distance: the distance to move, positive expands the rect, negative shrinks it.
    :return: returns a new expanded rect
    """
    rect = rect.copy()
    rect.width += distance
    return rect


def expand_bottom(rect, distance):
    """
    Expands the bottom side of a pygame.Rect instance.
    :param rect: the rect which is expanded.
    :param distance: the distance to move, positive expands the rect, negative shrinks it.
    :return: returns a new expanded rect
    """
    rect = rect.copy()
    rect.height += distance
    return rect


def expand_top(rect, distance):
    """
    Expands the top side of a pygame.Rect instance.
    :param rect: the rect which is expanded.
    :param distance: the distance to move, positive expands the rect, negative shrinks it.
    :return: returns a new expanded rect
    """
    rect = rect.copy()
    rect.top -= distance
    rect.height += distance
    return rect


def expand_left(rect, distance):
    """
    Expands the left side of a pygame.Rect instance.
    :param rect: the rect which is expanded.
    :param distance: the distance to move, positive expands the rect, negative shrinks it.
    :return: returns a new expanded rect
    """
    rect = rect.copy()
    rect.left -= distance
    rect.width += distance
    return rect


def get_overlapping_combinations():
    """
    Returns the combinations from the header list that have an overlapping area.
    :return: overlapping combinations, indices
            where overlapping combinations is a list of tuples containing tuples of two header entries
            indies is the index of the tuples from all the 256 possible combinations of the header entries,
            e.g. ([(c1, c2), ...], [int, ...])
    """
    combinations = []
    indexes = []
    idx = 0
    for c_y in header:
        for c_x in header:
            # overlapping combination
            if c_x | c_y == 0b1111:
                combinations.append((c_x, c_y))
                indexes.append(idx)
            idx += 1
    return combinations, indexes


def get_overlapping_rects_from_combinations(side_length, expand_distance=None, overlapping_combinations=None):
    """
    Returns rectangles (pygame.Rect instances) corresponding to the combinations.
    :param side_length: the rectangle side length
    :param expand_distance: the distance to expand the rectangle sides according to the combination
    :param overlapping_combinations: the list with the combinations containing tuples of two header entries.
    :return: list of tuples containing 3 rectangles: the two that overlap and a rectangle representing the
            overlapping area, e.g. [(r1, r2, overlap_rect), ...]
    """
    # :raise ValueError: if
    if overlapping_combinations is None:
        overlapping_combinations, indices = get_overlapping_combinations()

    if expand_distance is None:
        expand_distance = side_length / 2

    # if expand_distance > side_length:
    # raise ValueError("expand_distance should be smaller than the side_length")

    sides_to_move = [(R, expand_right), (B, expand_bottom), (T, expand_top), (L, expand_left)]

    rects = []
    # # variant where the rects a shrunk, but magenta area is unknown (different for every combination)
    # for yellow_def, cyan_def in overlapping_combinations:
    # magenta = pygame.Rect(0, 0, 0, 0)
    # yellow = pygame.Rect(0, 0, 3 * side_length, 3 * side_length)
    # cyan = yellow.copy()
    # for side, move_side in sides_to_move:
    # if side & yellow_def == 0:
    #             yellow = move_side(yellow, -shrink_dist)
    #         if side & cyan_def == 0:
    #             cyan = move_side(cyan, -shrink_dist)
    #
    #     areas.append((yellow, cyan, magenta))

    for yellow_def, cyan_def in overlapping_combinations:
        magenta = pygame.Rect(0, 0, side_length, side_length)
        yellow = pygame.Rect(0, 0, side_length, side_length)
        cyan = yellow.copy()
        for side, move_side in sides_to_move:
            if side & yellow_def == 0:
                yellow = move_side(yellow, expand_distance)
            if side & cyan_def == 0:
                cyan = move_side(cyan, expand_distance)

        rects.append((yellow, cyan, magenta))
    return rects


def get_non_overlapping_rects(side_length, off_set=None):
    """
    Returns a list of non overlapping rectangles (pygame.Rect instances).
    :param side_length: the length of a side of a rectangle
    :return: list of tuples containing the two rectangles and the overlapping area (which is always 0), e.g.
            [(r1, r2, overlapping_rect), ...]
    """
    if off_set is None:
        off_set = side_length + 5
    half_side = side_length // 2
    new_rect = pygame.Rect
    rectangles = [
        # touching edges and vertices
        (new_rect(0, 0, side_length, side_length), new_rect(-side_length, 0, side_length, side_length),
         new_rect(0, 0, 0, 0)),
        (new_rect(0, 0, side_length, side_length), new_rect(-side_length, -side_length, side_length, side_length),
         new_rect(0, 0, 0, 0)),
        (new_rect(0, 0, side_length, side_length), new_rect(0, -side_length, side_length, side_length),
         new_rect(0, 0, 0, 0)),
        (new_rect(0, 0, side_length, side_length), new_rect(side_length, -side_length, side_length, side_length),
         new_rect(0, 0, 0, 0)),
        (new_rect(0, 0, side_length, side_length), new_rect(side_length, 0, side_length, side_length),
         new_rect(0, 0, 0, 0)),
        (new_rect(0, 0, side_length, side_length), new_rect(side_length, side_length, side_length, side_length),
         new_rect(0, 0, 0, 0)),
        (new_rect(0, 0, side_length, side_length), new_rect(0, side_length, side_length, side_length),
         new_rect(0, 0, 0, 0)),
        (new_rect(0, 0, side_length, side_length), new_rect(-side_length, side_length, side_length, side_length),
         new_rect(0, 0, 0, 0)),
        # small distance between
        (
            new_rect(0, 0, side_length, side_length), new_rect(-off_set, 0, side_length, side_length),
            new_rect(0, 0, 0, 0)),
        (new_rect(0, 0, side_length, side_length), new_rect(-off_set, -off_set, side_length, side_length),
         new_rect(0, 0, 0, 0)),
        (
            new_rect(0, 0, side_length, side_length), new_rect(0, -off_set, side_length, side_length),
            new_rect(0, 0, 0, 0)),
        (new_rect(0, 0, side_length, side_length), new_rect(off_set, -off_set, side_length, side_length),
         new_rect(0, 0, 0, 0)),
        (
            new_rect(0, 0, side_length, side_length), new_rect(off_set, 0, side_length, side_length),
            new_rect(0, 0, 0, 0)),
        (new_rect(0, 0, side_length, side_length), new_rect(off_set, off_set, side_length, side_length),
         new_rect(0, 0, 0, 0)),
        (
            new_rect(0, 0, side_length, side_length), new_rect(0, off_set, side_length, side_length),
            new_rect(0, 0, 0, 0)),
        (new_rect(0, 0, side_length, side_length), new_rect(-off_set, off_set, side_length, side_length),
         new_rect(0, 0, 0, 0)),
        # around corners, small distance
        (new_rect(0, 0, side_length, side_length), new_rect(-off_set, -half_side, side_length, side_length),
         new_rect(0, 0, 0, 0)),
        (new_rect(0, 0, side_length, side_length), new_rect(-half_side, -off_set, side_length, side_length),
         new_rect(0, 0, 0, 0)),
        (new_rect(0, 0, side_length, side_length), new_rect(half_side, -off_set, side_length, side_length),
         new_rect(0, 0, 0, 0)),
        (new_rect(0, 0, side_length, side_length), new_rect(off_set, -half_side, side_length, side_length),
         new_rect(0, 0, 0, 0)),
        (new_rect(0, 0, side_length, side_length), new_rect(off_set, half_side, side_length, side_length),
         new_rect(0, 0, 0, 0)),
        (new_rect(0, 0, side_length, side_length), new_rect(half_side, off_set, side_length, side_length),
         new_rect(0, 0, 0, 0)),
        (new_rect(0, 0, side_length, side_length), new_rect(-half_side, off_set, side_length, side_length),
         new_rect(0, 0, 0, 0)),
        (new_rect(0, 0, side_length, side_length), new_rect(-off_set, half_side, side_length, side_length),
         new_rect(0, 0, 0, 0)),

    ]

    return rectangles


def get_all_rects(side_length, expand_distance):
    """
    Returns a list containing the overlapping and non overlapping rectangles (pygame.Rect instances).
    :param side_length: the length of a side of a base rectangle
    :param expand_distance: the distance to expand a side of a rectangle.
    :return: list of tuples containing the two rectangles and an overlapping area rectangle (might have size=(0, 0),
            e.g. [(r1, r2, overlapping_rect), ...]
    """
    areas = get_overlapping_rects_from_combinations(side_length, expand_distance)
    areas.extend(get_non_overlapping_rects(side_length))
    return areas


if __name__ == '__main__':

    side_length = 10
    shrink_dist = 10

    overlapping_combinations, indices = get_overlapping_combinations()
    areas = get_overlapping_rects_from_combinations(side_length, shrink_dist, overlapping_combinations)
    non_overlap = get_non_overlapping_rects(side_length)

    indices.extend(range(256, 256 + len(non_overlap), 1))
    areas.extend(non_overlap)

    # move rects to its position in grid
    dist = 4 * side_length
    # dist = 3 * side_length
    SCREEN_W = 1024
    SCREEN_H = 768
    num_tiles_x = 16  # SCREEN_W / dist
    offset = 2 * side_length
    rects = []
    for index, items in enumerate(areas):
        y_rect, c_rect, m_rect = items
        idx = indices[index]
        x_pos = idx % num_tiles_x * dist + offset
        y_pos = idx // num_tiles_x * dist + offset
        # print(x_pos, y_pos)
        rects.append((
            y_rect.move(x_pos, y_pos),
            c_rect.move(x_pos, y_pos),
            m_rect.move(x_pos, y_pos))
        )
        # print(idx, ye, cy, m)

    # render
    pygame.init()
    screen = pygame.display.set_mode((SCREEN_W, SCREEN_H), pygame.SRCALPHA)
    # screen.fill((0, 0, 0, 255))
    screen.fill((255, 255, 255, 255))
    fill_colors = [(255, 255, 0, 80), (0, 255, 255, 80), (255, 0, 255, 255)]
    border_colors = [(255, 0, 0), (0, 0, 255), (255, 0, 255)]
    screen_rect = screen.get_rect()
    for r in rects:
        for idx, rr in enumerate(r):
            color = fill_colors[idx]
            # pygame.draw.rect(screen, color, rr, 0)
            rr = rr.clip(screen_rect)
            # screen.fill(color, rr, pygame.BLEND_RGBA_MULT)
            if rr.width > 0 or rr.height > 0:
                screen.fill(color, rr)
                pygame.draw.rect(screen, border_colors[idx], rr, 1)

    for a, b, c in get_overlapping_rects_from_combinations(side_length, shrink_dist, overlapping_combinations):
        if not a.colliderect(b):
            print('r1 and r2 do not overlap, but they should', a, b, c)
        if a.clip(b).clip(a) != c:
            print("overlapping area differs from given overlapping area", a, b, c)

    non_rects = get_non_overlapping_rects(side_length)
    for a, b, c in non_rects:
        if a.colliderect(b):
            print("r1 and r2 have overlapping area, they should not", a, b, c)

    # print(len(get_all_rects(side_length, shrink_dist)), get_all_rects(side_length, shrink_dist))
    pygame.display.flip()

    top_equal = "01 top =="
    bottom_equal = "02 bottom =="
    left_equal = "03 left =="
    right_equal = "04 right =="
    top_lt = "05 top <"
    top_lt_l_eq = "06   top < l =="
    top_lt_l_lt = "07   top < l <"
    top_lt_l_gt = "08   top < l >"
    top_lt_r_eq = "09   top < r =="
    top_lt_r_lt = "10   top < r <"
    top_lt_r_gt = "11   top < r >"
    top_lt_b_eq = "12   top < b =="
    top_lt_b_lt = "13   top < b <"
    top_lt_b_gt = "14   top < b >"
    top_equal_left_eq = "15 top == left =="
    top_equal_left_lt = "16 top == left <"
    top_equal_left_gt = "17 top == left >"
    r1_contains = "18 r1 contains"
    r2_contains = "19 r2 contains"
    top_lt_r_lt_l_lt = "20      top < r < l <"
    top_lt_r_lt_l_gt = "21      top < r < l >"
    top_lt_r_lt_l_eq = "22      top < r < l =="
    top_lt_r_lt_b_lt = "23      top < r < b <"
    top_lt_r_lt_b_gt = "24      top < r < b >"
    top_lt_r_lt_b_eq = "25      top < r < b =="
    top_lt_r_lt_l_lt_b_gt = "26         top < r < l < b >"
    top_lt_r_lt_l_lt_b_lt = "27         top < r < l < b <"
    top_lt_r_lt_l_lt_b_eq = "28         top < r < l < b =="
    top_lt_r_lt_l_gt_b_lt = "29         top < r < l > b <"
    top_lt_r_lt_l_gt_b_gt = "30         top < r < l > b >"
    top_lt_r_lt_l_gt_b_eq = "31         top < r < l > b =="
    top_lt_r_lt_l_eq_b_eq = "32         top < r < l == b =="
    top_lt_r_lt_l_eq_b_lt = "33         top < r < l == b <"
    top_lt_r_lt_l_eq_b_gt = "34         top < r < l == b >"
    statistics = {
        top_equal: 0,
        bottom_equal: 0,
        left_equal: 0,
        right_equal: 0,
        top_lt: 0,
        top_equal_left_eq: 0,
        top_equal_left_lt: 0,
        top_equal_left_gt: 0,
        r1_contains: 0,
        r2_contains: 0,
        top_lt_l_eq: 0,
        top_lt_l_lt: 0,
        top_lt_l_gt: 0,
        top_lt_r_eq: 0,
        top_lt_r_lt: 0,
        top_lt_r_gt: 0,
        top_lt_b_eq: 0,
        top_lt_b_lt: 0,
        top_lt_b_gt: 0,
        top_lt_r_lt_l_eq: 0,
        top_lt_r_lt_l_gt: 0,
        top_lt_r_lt_l_lt: 0,
        top_lt_r_lt_b_lt: 0,
        top_lt_r_lt_b_gt: 0,
        top_lt_r_lt_b_eq: 0,
        top_lt_r_lt_l_lt_b_gt: 0,
        top_lt_r_lt_l_lt_b_lt: 0,
        top_lt_r_lt_l_lt_b_eq: 0,
        top_lt_r_lt_l_gt_b_lt: 0,
        top_lt_r_lt_l_gt_b_gt: 0,
        top_lt_r_lt_l_gt_b_eq: 0,
        top_lt_r_lt_l_eq_b_eq: 0,
        top_lt_r_lt_l_eq_b_lt: 0,
        top_lt_r_lt_l_eq_b_gt: 0,
    }

    rr = get_overlapping_rects_from_combinations(10, 10)  # (r1, r2, o)
    for idx, rrr in enumerate(rr):
        r1, r2, o = rrr
        rr = [r1, r2]
        rr.sort(key=lambda v: (v.y, v.x))
        rect, r = rr
        if rect.contains(r):
            statistics[r1_contains] += 1
        elif r.contains(rect):
            statistics[r2_contains] += 1
        elif rect.top == r.top:
            statistics[top_equal] += 1
            if rect.left == r.left:
                statistics[top_equal_left_eq] += 1
            elif rect.left < r.left:
                statistics[top_equal_left_lt] += 1
            else:
                statistics[top_equal_left_gt] += 1
        elif rect.top < r.top:
            statistics[top_lt] += 1
            if rect.left < r.left:
                statistics[top_lt_l_lt] += 1
            elif rect.left == r.left:
                statistics[top_lt_l_eq] += 1
            elif rect.left > r.left:
                statistics[top_lt_l_gt] += 1

            if rect.right < r.right:
                statistics[top_lt_r_lt] += 1
                if rect.left < r.left:
                    statistics[top_lt_r_lt_l_lt] += 1
                    if rect.bottom < r.bottom:
                        statistics[top_lt_r_lt_l_lt_b_lt] += 1
                    elif rect.bottom == r.bottom:
                        statistics[top_lt_r_lt_l_lt_b_eq] += 1
                    else:
                        statistics[top_lt_r_lt_l_lt_b_gt] += 1
                elif rect.left == r.left:
                    statistics[top_lt_r_lt_l_eq] += 1
                    if rect.bottom < r.bottom:
                        statistics[top_lt_r_lt_l_eq_b_lt] += 1
                    elif rect.bottom == r.bottom:
                        statistics[top_lt_r_lt_l_eq_b_eq] += 1
                    else:
                        statistics[top_lt_r_lt_l_eq_b_gt] += 1
                else:
                    statistics[top_lt_r_lt_l_gt] += 1
                    if rect.bottom < r.bottom:
                        statistics[top_lt_r_lt_l_gt_b_lt] += 1
                    elif rect.bottom == r.bottom:
                        statistics[top_lt_r_lt_l_gt_b_eq] += 1
                    else:
                        statistics[top_lt_r_lt_l_gt_b_gt] += 1

                if rect.bottom < r.bottom:
                    statistics[top_lt_r_lt_b_lt] += 1
                elif rect.bottom == r.bottom:
                    statistics[top_lt_r_lt_b_eq] += 1
                else:
                    statistics[top_lt_r_lt_b_gt] += 1
            elif rect.right == r.right:
                statistics[top_lt_r_eq] += 1
            elif rect.right > r.right:
                statistics[top_lt_r_gt] += 1

            if rect.bottom < r.bottom:
                statistics[top_lt_b_lt] += 1
            elif rect.bottom == r.bottom:
                statistics[top_lt_b_eq] += 1
            elif rect.bottom > r.bottom:
                statistics[top_lt_b_gt] += 1
        else:
            raise Exception("should not happen")

    for item in sorted(statistics.items(), key=lambda x: -x[1]):
        print(item)

    pygame.event.clear()
    running = True
    while running:
        event = pygame.event.wait()
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                running = False
            elif event.key == pygame.K_RETURN:
                pygame.image.save(screen, "screenshot.png")
