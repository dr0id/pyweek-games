﻿VROOM41
=======

A game written for pyweek 19 (www.pyweek.org)

The theme was One Room. The theme is incorporated into the game setting,
not the game mechanics. Some references will hit you like a sack of
ripe stockings. Others you will have to dig for with care. :)

Team: Multiverse Factory
Members: DR0ID, Gummbumm

CREDITS:
Coding: DR0ID, Gummbumm
Artistes: DR0ID, Gummbumm
Musicians: JDruid
SFX: courtesy of findsounds.com, salamisound.com
Story: Gummbumm
Special Effects: DR0ID
Technical Advisor: DR0ID
Casting and Wardrobe: DR0ID
Public Relations: Grumpy Cat


DEPENDENCIES:

python (tested with: 2.6.4, 2.7.2, 3.1, and 3.2.5)
pygame (tested with: 1.9.1)

You might need to install some of these before running the game:

  Python:     http://www.python.org/
  PyGame:     http://www.pygame.org/


GAMEPLAY:

A Poem by Gummbum, read by Gummbum

The life of a pizza delivery guy
is full of adventure.
Working for Rooman's Pizza is a dream.
Literally.
Having dozed off in your One Room flat,
life unexpectedly takes off in a new direction.

In your dream you are the hero of the day.
Mounting your trusty moped,
license plate

    VROOM41,

you find the streets in disarray.
Your once loyal customers run amok.
Mr. Doubleshrooms Oneanchovy looks right at your face
but doesn't seem to recognize you!
He, along with many others,
are menaced
by overgrown pizza fixin's!

Your heart goes out to them!

There is not a moment to lose!!

With a steely glint in your eye
you don your helmet
with new purpose.
No longer do you deliver pizzas
from your mentor Rooman's cozy chalet to your loyal customers.
You must deliver the endangered customers
to the last bastion of safety and sanity:

    Rooman's Pizza Haven!

As if wiggling customers
stacked ten-high on your pizza rack
were not enough of a challenge,
with 3.5 HP you must wend your way
through the horde of baddies
(whose only mischievous desire
is to knock your customers off
and make you chase them down again)
while also avoiding those
mocking puddles of gooey cheese!!!

There better be a big promotion in this...


RUNNING THE GAME:

On Windows or Mac OS X, locate the "run_game.pyw" file and double-click it.

Othewise open a terminal / console and "cd" to the game directory and run:

  python run_game.py



HOW TO PLAY THE GAME:

These instructions are repeated in-game. Cheaters can read more tips after this
section Cheat tips are only available in this readme.

You are a pizza delivery boy. You are having a nightmare where you have to save
your customers! Unfortunately you only have room for one on your moped!

Collect the customers with your moped by getting near them and drive them to
the pizza parlor.

Avoid colliding with anything or you loose collected customers.

Keys:

'Up' to accelerate
'Down' to brake, or step in reverse
'Left' to steer to left
'Right' to steer to right
'Space' to switch the lights on

THE BEST TIP: If night descends upon you and you can't play because it over-
works your poor little computer, you can press the "1" key to make it day. This
cheat is sanctioned because lighting is way cool, but we had no easy way make
it tunible once the game started and you might discover in level 1000 that
your deck can't handle lighting with 70,000,000 baddies.


HOW TO CHEAT:











Keep scrolling... cheater.












Look at you. Yeah, you know you're cheating. Don't ya, cheater?









Ok, here are some tips for struggling heroes.


1. You can carry any number of customers on your pizza rack. Stack them as high
as you like!

NEXT TIP...

2. You can even take zero customers. Boss is demanding. He won't let you off
the hook, he will just send you back out for more without even a bathroom
break. And you know there will be more. Because, I mean, look out there:
obviously nobody has enough sense to stay indoors.

3. You probably figured this out already. If you clear an area of tomatoes it
will often make rounding up those fleeing customers a lot easier.

4. The sky-high stack of customers does not make you top-heavy. It is just an
optical illusion.

5. When it gets dark turn on your headlamp, man!

6. Use those brakes to make tight turns and improve your driving skill.
Often it is easier to step in reverse than to bully through an obstacle (trees,
the side ditches, duh! and gooey cheese puddles).

7. I know it looks like those pizza cutters, with their keen edges and evil
intellect, have it in for you. But honestly, they just randomly frolic. :)

8. If you don't want to play through all the levels in order to see the ending,
customize settings.PICK_LEVELS. Or if you just like to play the hardest levels;
or if you just like to wimp out on the hard levels. This setting is a list of
level indices (0..N) that you can specify. Any levels not listed in there will
be skipped. To play all levels leave the list empty. The __debug__ internal is
True when starting the game via run_game_debug.py.

9. You can turn off music and the moped sounds in settings.py. But seriously,
why would you want that!? :)

10. There are many tweaks in settings.py. While they were meant for fine-tuning
and debugging by the developers, you may have fun with them at your own risk.
Management reserves the right to refuse service. :D

11. The game designers chose the end-game difficulty based on a wild guess at
what common low-end systems out there can handle. If the hardest level isn't
hard enough and your computer can handle it, you can add your own levels! In
level.py add a line of text for each custom level; or replace the whole shebang
with your own data. The data column meanings are documented right there for
convenience. Here's one for ya:
1000 4000 15000 3000   40   2  120  120 90

12. Don't drive in front of a moving pizza cutter. No, that tree did *not*
stick its root out in front of you.

Omega. Cheaters never win! Even though the screen says that you won. Screens
don't know any better. So who has the greater responsibility, hm?
