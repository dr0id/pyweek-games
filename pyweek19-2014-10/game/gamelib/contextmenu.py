# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek19-2014-10
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function
import pygame
from Button import Button
from contextcredits import ContextCredits
from contextinstructions import ContextInstructions
from gameplay import Gameplay
import pyknicpygame
from pyknicpygame.pyknic import context
from pyknicpygame.pyknic.context import Context
from pyknicpygame.pyknic.mathematics import Point2
from pyknicpygame.spritesystem import Sprite
from settings import SCREEN_SIZE, SCREEN_WIDTH
import tweening
import sound


__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2014"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module




class ContextMenu(Context):

    def __init__(self):
        self.tweener = tweening.Tweener()
        self.scheduler = pyknicpygame.pyknic.timing.Scheduler()
        sw, sh = SCREEN_SIZE
        self.renderer = pyknicpygame.spritesystem.DefaultRenderer()
        cam = pyknicpygame.spritesystem.Camera(pygame.Rect(0, 0, sw, sh), position=Point2(0, 0))
        self.cam = cam

        self.button_play = None
        self.button_credit = None
        self.button_instructions = None
        self.button_exit = None

        self.focused = None
        self.buttons = []
        self.is_handling_events = True

    def think(self, delta_time):
        """
        Update the game.
        :param delta_time: time passed in last time step (game time)
        :return:
        """

        self._handle_events()
        self.tweener.update(delta_time)
        self.scheduler.update(delta_time)
        if not pygame.mixer.music.get_busy():
            sound.songs['menu'].play()

    def execute_action(self, button):
        # action
        # self.renderer.remove_sprites(self.buttons)
        if button == self.button_exit:
            sound.sfx['rev_up'].play()

        if button is not self.button_instructions:
            pygame.mixer.music.fadeout(2000)
        self.is_handling_events = False
        button.do_action()

    def _handle_events(self):
        for event in pygame.event.get():
            if self.is_handling_events:
                if event.type == pygame.QUIT:
                    self.button_exit.on_focus()
                    self.execute_action(self.button_exit)
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        self.button_exit.on_focus()
                        self.execute_action(self.button_exit)
                    elif event.key == pygame.K_UP:
                        self.focus_next(-1)
                    elif event.key == pygame.K_LEFT:
                        self.focus_next(-1)
                    elif event.key == pygame.K_DOWN:
                        self.focus_next(1)
                    elif event.key == pygame.K_RIGHT:
                        self.focus_next(1)
                    elif event.key == pygame.K_RETURN:
                         self.execute_action(self.focused)
                elif event.type == pygame.MOUSEMOTION:
                    # hover
                    for b in self.buttons:
                        b.on_focus_lost()

                    spr = self.renderer.get_sprites_in_rect(pygame.Rect(event.pos, (1, 1)))
                    if spr:
                        button = spr[0]
                        button.on_focus()
                        self.focused = button

                elif event.type == pygame.MOUSEBUTTONDOWN:
                    self.execute_action(self.focused)

    def draw(self, screen, do_flip=True):

        self.renderer.draw(screen, self.cam, fill_color=(0, 0, 0), do_flip=False, interpolation_factor=1.0)

        if do_flip:
            pygame.display.flip()

    def enter(self):
        pygame.mouse.set_visible(True)
        self.renderer.clear()
        ld = self.load_image
        duration = 1.0
        self.button_play = Button(ld('menuplay.png'), ld('menuplayhover.png'), Point2(70, -240), None, 'menuplay.png')
        self.button_play.z_layer = 2
        self.button_play.action = self.tweener.create_tween
        self.button_play.action_args = (self.button_play, "zoom", 1.0, 3.01, duration, tweening.IN_BACK, None, self.next, lambda *args: context.push(Gameplay()))
        # self.button_play._zoom_precision = 10
        self.button_play.on_focus()

        self.button_credit = Button(ld("menucredits.png"), ld('menucreditshover.png'), Point2(-220, -80), None, "menucredits.png")
        self.button_credit.action = self.tweener.create_tween
        self.button_credit.action_args = (self.button_credit, "zoom", 1.0, 3.01, duration, tweening.OUT_CUBIC, None, self.next, lambda *args: context.push(ContextCredits()))

        self.button_instructions = Button(ld('menuinstr.png'), ld('menuinstrhover.png'), Point2(180, 60), None, 'menuinstr.png')
        self.button_instructions.action = self.dim

        self.button_exit = Button(ld('menuexit.png'), ld('menuexithover.png'), Point2(-130, 260), None, 'menuexit.png')
        self.button_exit.action = self.tweener.create_tween
        self.button_exit.action_args = (self.button_exit.position, "x", -150, SCREEN_WIDTH, duration, tweening.IN_CUBIC, None, self.next, lambda *args: context.pop())

        self.focused = self.button_play

        self.buttons = [self.button_play, self.button_credit, self.button_instructions, self.button_exit]
        self.renderer.add_sprites(self.buttons)

    def dim(self, *args):
        img = pygame.Surface(SCREEN_SIZE, pygame.SRCALPHA)
        img.fill((0, 0, 0, 255))
        spr = Sprite(img, Point2(0, 0), z_layer=100, name='context menu dim')
        spr.alpha = 0
        self.renderer.add_sprite(spr)
        self.tweener.create_tween(spr, 'alpha', 0, 255, 1.0, tweening.LINEAR, None, self.next, lambda *args: context.push(ContextInstructions()))

    def load_image(self, name):
        p = 'data/image/' + name
        return pygame.image.load(p).convert_alpha()

    def next(self, *cb_args):
        action = cb_args[-1][0]
        action()
        self.is_handling_events = True

    #def suspend(self):
    #    pygame.mixer.music.fadeout(1000)
    
    def resume(self):
        self.enter()

    def focus_next(self, param):
        idx = self.buttons.index(self.focused)
        idx += param
        idx %= len(self.buttons)

        for b in self.buttons:
            b.on_focus_lost()

        n = self.buttons[idx]
        self.focused = n
        n.on_focus()
