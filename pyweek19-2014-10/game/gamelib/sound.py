# -*- coding: utf-8 -*-
import os

import pygame


if __name__ == '__main__':
    os.chdir('..')
    os.environ['SDL_VIDEODRIVER'] = 'dummy'
    pygame.init()
    pygame.mixer.pre_init(frequency=0, size=-16, channels=2, buffer=128)
    pygame.mixer.init()
    pygame.display.set_mode((1, 1))


class Song(object):
    def __init__(self, name, file_name, volume=1.0):
        self.name = name
        self.file_name = file_name
        self.volume = volume

    def play(self):
        pygame.mixer.music.load(self.file_name)
        self.set_volume(self.volume)
        pygame.mixer.music.play()

    def queue(self):
        pygame.mixer.music.queue(self.file_name)

    @staticmethod
    def fadeout(time=1000):
        pygame.mixer.music.fadeout(time)

    @staticmethod
    def stop():
        pygame.mixer.music.stop()

    @staticmethod
    def pause():
        pygame.mixer.music.pause()

    @staticmethod
    def unpause():
        pygame.mixer.music.unpause()

    @staticmethod
    def stop():
        pygame.mixer.music.stop()

    @staticmethod
    def get_volume():
        return pygame.mixer.music.get_volume()

    @staticmethod
    def set_volume(value):
        pygame.mixer.music.set_volume(value)

    @staticmethod
    def get_busy():
        return pygame.mixer.music.get_busy()


sfx = {}
songs = {}

channels = {}
def playn(name, n=0):
    s = sfx[name]
    if name not in channels:
        channels[name] = set()
    if n > 0:
        chans = channels[name]
        nbusy = 0
        for ch in chans:
            if ch.get_sound() is s and ch.get_busy():
                nbusy += 1
        if nbusy < n:
            new_chan = s.play()
            channels[name].add(new_chan)
    else:
        new_chan = s.play()
        channels[name].add(new_chan)

def init():
    """call me *after* pygame is initialized"""

    # # SFX
    sfx_files = (
        ('bump1', 'data/sfx/bump1.ogg', 0.5),
        ('bump2', 'data/sfx/bump2.ogg', 0.2),
        ('crash_hard', 'data/sfx/crash_hard1.ogg', 1.0),
        ('crash_light1', 'data/sfx/crash_light1.ogg', 1.0),
        ('crash_light2', 'data/sfx/crash_light2.ogg', 1.0),
        ('rev_idle', 'data/sfx/rev_idle.ogg', 1.0),
        ('rev_up', 'data/sfx/rev_up.ogg', 1.0),
        ('rev_down_up', 'data/sfx/rev_down_up.ogg', 1.0),
        ('rev_sustained', 'data/sfx/rev_sustained.ogg', 1.0),
        ('rev_rev_rev', 'data/sfx/rev_rev_rev.ogg', 1.0),
        ('rev_down', 'data/sfx/rev_down.ogg', 1.0),
        ('skid', 'data/sfx/skid.ogg', 1.0),
        ('slice_long', 'data/sfx/slice_long.ogg', 1.0),
        ('slice_medium', 'data/sfx/slice_medium.ogg', 1.0),
        ('slice_short', 'data/sfx/slice_short.ogg', 1.0),
        ('splat_big', 'data/sfx/splat_big1.ogg', 0.7),
        ('splat_gloop', 'data/sfx/splat_gloop1.ogg', 0.7),
        ('splat_juicy', 'data/sfx/splat_juicy1.ogg', 0.7),
        ('splat_splatter1', 'data/sfx/splat_splatter1.ogg', 0.7),
        ('splat_splatter2', 'data/sfx/splat_splatter2.ogg', 0.7),
    )
    for name, file_name, volume in sfx_files:
        assert os.access(file_name, os.F_OK)
        sfx[name] = pygame.mixer.Sound(file_name)
        sfx[name].set_volume(volume)

    # # Music
    song_files = (
        ('gauntlet3', 'data/music/0pything2.ogg', 1.0),
        ('menu', 'data/music/back2.ogg', 1.0),
        ('gauntlet2', 'data/music/burn_up.ogg', 1.0),
        ('credits', 'data/music/celest.ogg', 1.0),
        ('gauntlet1', 'data/music/the_gauntlet.ogg', 1.0),
    )
    for name, file_name, volume in song_files:
        assert os.access(file_name, os.F_OK)
        songs[name] = Song(name, file_name, volume)


if __name__ == '__main__':
    init()
    case = ''
    if case == 'songs':
        def play_song(s):
            # play the song for 10 seconds...
            songs[s].play()
            n = 0
            while Song.get_busy() and n < 10:
                pygame.time.wait(1000)
                n += 1
            # then fade out
            Song.fadeout()
            pygame.time.wait(1000)

        for s in sorted(songs):
            play_song(s)
    else:
        def play_sfx(s):
            chan = sfx[s].play()
            while chan.get_busy():
                pygame.time.wait(1000)
        def test_playn(s, n=0):
            playn(s, n)
            print('nchan={0}'.format(sfx[s].get_num_channels()))
        for i in (1,1,1):
            print('play')
            test_playn('skid', 1)
            pygame.time.wait(100)
        while any([chan.get_busy() for chan in channels['skid']]):
            print('busy')
            pygame.time.wait(100)
        quit()
        for s in sorted(sfx):
            play_sfx(s)
