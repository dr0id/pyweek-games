import logging

import pygame
from pygame.locals import *

import pyknicpygame
from pyknicpygame.pyknic.context import Context

import sound
import contextcredits

class Struct(object):
    def __init__(self, **kwargs):
        self.__dict__.update(**kwargs)

class ContextWon(Context):
    def __init__(self, logger=None):
        Context.__init__(self)
        self.logger = logging.getLogger(self.__class__.__name__)
        if logger is not None:
            self.logger = logger
        
        self.scenes = []
        self.current_scene = None
        
        self.font = pygame.font.SysFont('sans', 24, True)

    def enter(self):
        screen = pygame.display.get_surface()
        screen_rect = screen.get_rect()
        
        # blank
        image = pygame.Surface((10, 10))
        image.fill((0, 0, 0))
        rect = image.get_rect()
        self.scenes.append(Struct(
            image=image, image_rect=rect, text=image, text_rect=rect, duration=4.0))
        
        # waking 1
        image = pygame.image.load('data/image/waking1.png').convert_alpha()
        text = self.font.render('Aaaa!', True, Color('white'))
        image_rect = image.get_rect(centerx=screen_rect.centerx, centery=screen_rect.centery - 30)
        text_rect = text.get_rect(centerx=screen_rect.centerx, top=image_rect.bottom + 50)
        self.scenes.append(Struct(
            image=image, image_rect=image_rect, text=text, text_rect=text_rect, duration=3.0))
        
        # waking 2
        image = pygame.image.load('data/image/waking2.png').convert_alpha()
        text = self.font.render('Whew. It was just a dream.', True, Color('white'))
        image_rect = image.get_rect(centerx=screen_rect.centerx, centery=screen_rect.centery - 30)
        text_rect = text.get_rect(centerx=screen_rect.centerx, top=image_rect.bottom + 50)
        self.scenes.append(Struct(
            image=image, image_rect=image_rect, text=text, text_rect=text_rect, duration=6.0))
        
        self.current_scene = self.scenes.pop(0)
    
    def think(self, delta_time):
        pygame.event.clear()
        self.current_scene.duration -= delta_time
        if self.current_scene.duration <= 0.0:
            try:
                self.current_scene = self.scenes.pop(0)
            except IndexError:
                pyknicpygame.pyknic.context.pop()
                pyknicpygame.pyknic.context.push(contextcredits.ContextCredits())
        if not pygame.mixer.music.get_busy():
            sound.songs['credits'].play()
    
    def draw(self, screen, do_flip=True):
        screen = pygame.display.get_surface()
        screen_rect = screen.get_rect()
        
        screen.fill((0, 0, 0))
        scene = self.current_scene
        screen.blit(scene.image, scene.image_rect)
        screen.blit(scene.text, scene.text_rect)
        pygame.display.flip()
