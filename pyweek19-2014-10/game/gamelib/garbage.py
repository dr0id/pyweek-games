import gc


class Garbage(object):
    
    THRESHOLDS = (100000, 10, 10)
    
    def __init__(self, thresholds=None):
        if thresholds is None:
            self.set_threshold(*self.THRESHOLDS)
        else:
            self.set_threshold(*thresholds)
        
        self.ignore = False
    
    def set_threshold(self, t0, t1, t2):
        gc.set_threshold(t0, t1, t2)
    
    def get_threshold(self):
        return gc.get_threshold()
    
    def enable(self):
        gc.enable()
    
    def disable(self):
        gc.disable()
    
    def isenabled(self):
        gc.isenabled()
    
    def get_count(self):
        return gc.get_count()
    
    def collect(self, generations=2):
        gc.collect(generations)
    
    def update(self, *args):
        if self.ignore:
            return
        count = self.get_count()
        for i,t in enumerate(self.get_threshold()):
            if count[i] > t:
                if __debug__:
                    print('{0}: collecting generation {1}'.format(
                        self.__class__.__name__, i))
                self.collect(i)
                break
