# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek19-2014-10
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
# * Neither the name of the <organization> nor the
#   names of its contributors may be used to endorse or promote products
#   derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import logging

import pygame
from pygame.locals import Rect

import settings


# Columns:
# 0: height of start zone
# 1: height of roundup zone
# 2: height of gauntlet zone
# 3: height of home zone
# 4: number of customers in roundup
# 5: max number of baddies onscreen in roundup
# 6: number of stationary baddies in gauntlet
# 7: max number of mobile baddies onscreen in gauntlet
# 8: objective
#  0    1     2    3    4   5   6   7   8
#--- ---- ----- ----  --- --- --- --- ---
data = """
1000 2000  7000 3000    3   2  10  10  1
1000 2000  7000 3000    4   3  15  15  3
1000 2000  7000 3000    5   4  20  20  5
1000 2000  7000 3000    7   5  25  25  7
1000 2000  7000 3000    9   6  30  30  9
1000 2000  8000 3000   11   7  40  40 11
1000 2000  9000 3000   13   7  45  45 13
1000 2000 10000 3000   15   7  50  50 15
1000 2000 11000 3000   17   7  55  55 17
1000 2000 12000 3000   19   7  60  60 19
""".split('\n')
data = [line for line in data if line]

## This level data was supposed to be the impossible boss level. Needs quite a bit of special
## case code. This will happen only if we have time to try it!
"""
1000 2000  7000 3000   50   0  10  10  0
"""

## PICK LEVELS FOR TESTING
if settings.PICK_LEVELS:
    data = [line for i,line in enumerate(data) if i in settings.PICK_LEVELS]


class Level(object):
    def __init__(self, n, logger=None):
        self.logger = logging.getLogger(self.__class__.__name__)
        if logger is not None:
            self.logger = logger
        #self.logger.debug("level_num: {0}".format(n))
        
        self.num_levels = len(data)
        
        parts = data[n].split()

        zone_rects = []
        y = 0
        for h in parts[0:4]:
            h = int(h)
            r = Rect(0, y, 600, -h)
            ### not sure why this works,
            ### but it gets the results I want :) - Gumm
            r.left = 0
            y -= h
            zone_rects.append(r)
        (self.start_rect, self.roundup_rect, self.gauntlet_rect,
         self.home_rect) = zone_rects
        self.start_rect.y += 1000
        self.start_rect.h -= 1000

        self.roundup_customers = int(parts[4])
        self.roundup_baddies = int(parts[5])
        self.gauntlet_stationary = int(parts[6])
        self.gauntlet_mobile = int(parts[7])
        self.objective = int(parts[8])

        self.level_length = self.start_rect.h + self.home_rect.h + self.roundup_rect.h + self.gauntlet_rect.h
        
        self.score = 0


if __name__ == '__main__':
    pygame.init()
    for i in range(len(data)):
        print('# Level({0})'.format(i))
        a = Level(i)
        print(a.start_rect)
        print(a.roundup_rect)
        print(a.gauntlet_rect)
        print(a.home_rect)
