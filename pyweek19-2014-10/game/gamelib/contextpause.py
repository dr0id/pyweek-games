# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek19-2014-10
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function
import pygame
import pyknicpygame
from pyknicpygame.pyknic.context import Context, top
from pyknicpygame.pyknic.mathematics import Point2
from pyknicpygame.spritesystem import Sprite
from settings import SCREEN_SIZE
import tweening


__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2014"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module



class ContextPause(Context):

    def __init__(self):
        self.tweener = tweening.Tweener()
        self.scheduler = pyknicpygame.pyknic.timing.Scheduler()
        sw, sh = SCREEN_SIZE
        self.renderer = pyknicpygame.spritesystem.DefaultRenderer()
        cam = pyknicpygame.spritesystem.Camera(pygame.Rect(0, 0, sw, sh), position=Point2(0, 0))
        self.cam = cam
        self.buttons = []

    def think(self, delta_time):
        """
        Update the game.
        :param delta_time: time passed in last time step (game time)
        :return:
        """

        self._handle_events()
        self.tweener.update(delta_time)
        self.scheduler.update(delta_time)

    def _handle_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pyknicpygame.pyknic.context.pop(2)
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pyknicpygame.pyknic.context.pop()
                elif event.key == pygame.K_UP:
                    pass

    def draw(self, screen, do_flip=True):

        c = top(1)
        if c:
            c.draw(screen, False)
        self.renderer.draw(screen, self.cam, do_flip=False, interpolation_factor=1.0)


        if do_flip:
            pygame.display.flip()

    def enter(self):
        img = pygame.image.load('data/image/paused.png')
        spr = Sprite(img, Point2(0, 0), name='context pause')
        spr._use_image_cache = False
        self.renderer.add_sprite(spr)
        self.on_up(spr, spr)

    def on_down(self, *args):
        self.tweener.create_tween(args[1], "alpha", 255, -225, 0.5, cb_end=self.on_up)

    def on_up(self, *args):
        self.tweener.create_tween(args[1], "alpha", 30, 225, 0.5, cb_end=self.on_down)

    def exit(self):
        pass
