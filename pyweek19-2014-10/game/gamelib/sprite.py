import logging

import pygame

import pyknicpygame
import pyknicpygame.spritesystem
from pyknicpygame.pyknic.mathematics import Point2
from settings import MOPED_LENGTH, TIRE_LAYER, MOPED_LAYER, MOPED_LIGHT_LAYER


class MopedSprite(pyknicpygame.spritesystem.Sprite):
    def __init__(self, obj, logger=None):
        self.logger = logging.getLogger(self.__class__.__name__)
        if logger is not None:
            self.logger = logger
        
        # Constant: angle at which front wheel image changes
        self.turn_threshold = 28.5

        # The model object
        self.obj = obj

        # Image data
        self.image_straight = pygame.image.load('data/image/moped0.png').convert_alpha()
        self.image_left = pygame.image.load('data/image/moped45.png').convert_alpha()
        self.image_right = pygame.image.load('data/image/moped-45.png').convert_alpha()
        self.front_tire = pygame.image.load('data/image/moped-tire.png').convert_alpha()

        self.front_light = pygame.image.load('data/image/frontlight.png').convert() #.convert_alpha()
        self.back_light_normal = pygame.image.load('data/image/backlight.png').convert()
        self.back_light_on = pygame.image.load('data/image/backlighton.png').convert_alpha()
        self.back_light_breaking = pygame.image.load('data/image/backlightbreaking.png').convert_alpha()

        # self.image_left = pyknicpygame.surface.SubPixelSurfacePygame(self.image_left)
        # self.image_right = pyknicpygame.surface.SubPixelSurfacePygame(self.image_right)
        # self.image_straight= pyknicpygame.surface.SubPixelSurfacePygame(self.image_straight)

        self.front_tire_sprite = pyknicpygame.spritesystem.Sprite(self.front_tire, Point2(0, MOPED_LENGTH * -11),
                                                                  z_layer=TIRE_LAYER,
                                                                  name='moped front tire')

        self.front_light_spr = pyknicpygame.spritesystem.Sprite(self.front_light, Point2(0, MOPED_LENGTH * -11),
                                                                anchor="center", z_layer=MOPED_LIGHT_LAYER,
                                                                name='moped front light')
        self.front_light_spr.is_light = 1
        self.back_light_on_spr = pyknicpygame.spritesystem.Sprite(self.back_light_on, Point2(0, MOPED_LENGTH * 11), anchor="center",
                                                                  z_layer=MOPED_LIGHT_LAYER,
                                                                  name=self.__class__.__name__)
        # self.back_light_on_spr.is_light = 1
        self.back_light_breaking_spr = pyknicpygame.spritesystem.Sprite(self.back_light_breaking,
                                                                        Point2(0, MOPED_LENGTH * 11), anchor="center",
                                                                        z_layer=MOPED_LIGHT_LAYER + 1,
                                                                        name=self.__class__.__name__)
        # self.back_light_breaking_spr.is_light = 1
        self.back_light_spr = pyknicpygame.spritesystem.Sprite(self.back_light_normal, Point2(0, MOPED_LENGTH * 11), anchor="center", z_layer=MOPED_LAYER, name=self.__class__.__name__)
        self.back_light_spr.is_light = 1
        # init_image = self.image_straight.at(0, 0)
        self.image_angle = 0
        init_image = self.image_straight
        pyknicpygame.spritesystem.Sprite.__init__(
            self, init_image, Point2(0, 0),
            anchor='center', z_layer=MOPED_LAYER, parallax_factors=None, do_init=True,
            name=self.__class__.__name__)
        self.anchor = pyknicpygame.pyknic.mathematics.Vec2(0, 30)

    def cache_key(self):
        return self._rot, self._flip_x, self._flip_y, self._zoom_factor, self._alpha, self.image_angle

    def update(self, delta_time=None):
        steering_angle = self.obj.steering_angle
        if steering_angle <= -self.turn_threshold:
            self.image_angle = -1
            img = self.image_left
        elif steering_angle >= self.turn_threshold:
            self.image_angle = 1
            img = self.image_right
        else:
            self.image_angle = 0
            img = self.image_straight
        # self.set_image(img.at(self.obj.position.x, self.obj.position.y))
        self.set_image(img)
        self.rotation = -self.obj.direction.angle - 90.0

        self.position = self.obj.position

        self.front_tire_sprite.position = self.obj.position + self.obj.direction * (MOPED_LENGTH + 25)
        self.front_tire_sprite.rotation = -self.obj.steering_angle - self.obj.direction.angle - 90.0

        self.front_light_spr.position = self.obj.position + self.obj.direction * (MOPED_LENGTH + 28) + self.obj.direction.rotated(self.obj.steering_angle) * 115
        self.front_light_spr.rotation = -self.obj.steering_angle - self.obj.direction.angle - 90.0

        back_light_d = 55
        self.back_light_on_spr.position = self.obj.position + self.obj.direction * (MOPED_LENGTH - back_light_d)
        self.back_light_on_spr.rotation = -self.obj.direction.angle - 90.0

        self.back_light_breaking_spr.position = self.obj.position + self.obj.direction * (MOPED_LENGTH - back_light_d - 4)
        self.back_light_breaking_spr.rotation = -self.obj.direction.angle - 90.0

        self.back_light_spr.position = self.obj.position + self.obj.direction * (MOPED_LENGTH - back_light_d)
        self.back_light_spr.rotation = -self.obj.direction.angle - 90.0
