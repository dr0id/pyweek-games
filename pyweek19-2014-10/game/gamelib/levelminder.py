# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek19-2014-10
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import logging
import random

import pygame
from pygame.locals import Color

import settings


class _StateBase(object):

    @staticmethod
    def enter(self):
        pass

    @staticmethod
    def exit(self):
        pass
    
    @staticmethod
    def think(self, dt):
        pass
# -----------------------------------------------------------------------------
class _StateReset(_StateBase):
    """reset level
    
    This waits for new gameplay objects to be configured.
    
    Progress when the gameplay object is populated (via configure() method).
    """
    
    @staticmethod
    def think(self, dt):
        if self.gameplay is not None:
            return _StateIntro
# -----------------------------------------------------------------------------
class _StateIntro(_StateBase):
    """new level, greet player, state objective
    
    This plays the intro_scene.
    
    Progress when scene is done playing.
    """
    #
    # If cinema is ever added, this should pick it up without any hassle.
    #
    @staticmethod
    def enter(self):
        gameplay = self.gameplay
        self.make_text('Level {0} - BOSS NEEDS {1}'.format(
            gameplay.level_num + 1, gameplay.level.objective))
    
    @staticmethod
    def think(self, dt):
        self.text_elapsed += dt
        if self.intro_scene:
            self.intro_scene.update(dt)
            if self.intro_scene.is_finished():
                return _StateGetReady
        elif self.text_elapsed >= 3.0:
            return _StateGetReady
    
    @staticmethod
    def exit(self):
        self.clear_text()
# -----------------------------------------------------------------------------
class _StateContinue(_StateBase):
    """loop branch to continue, restate objective
    
    This generates the next section of road ahead.
    
    Progress when scene is done playing.
    """
    @staticmethod
    def think(self, dt):
        if self.continue_scene:
            self.continue_scene.update(dt)
            if self.continue_scene.is_finished():
                return _StateGetReady
        else:
            return _StateGetReady
# -----------------------------------------------------------------------------
class _StateGetReady(_StateBase):
    """tell player to get ready for roundup
    
    Flash a message and arrows to move ahead when ready.
    
    Progress when player drives over the border into the roundup zone.
    """
    @staticmethod
    def enter(self):
        self.make_text('^ ROUND EM UP! ^')
    
    @staticmethod
    def think(self, dt):
        if self._is_roundup():
            return _StateRoundup
    
    @staticmethod
    def exit(self):
        self.clear_text()
# -----------------------------------------------------------------------------
class _StateRoundup(_StateBase):
    """avoid a few baddies while rounding up customers
    
    This is the phase for the roundup logic. Get as many customers as you can.
    The risk from baddies is low. But if you sit in one place baddies will
    steal all your customers.
    
    Progress when player drives over the border into the Gauntlet zone.
    """
    
    @staticmethod
    def think(self, dt):
        if self._is_gauntlet():
            return _StateGauntlet
# -----------------------------------------------------------------------------
class _StateGauntlet(_StateBase):
    """avoid baddies while racing to pizza parlor
    
    This phase is running the gauntlet. Risk from baddies is high. They will
    steal your customers: you must decide whether to risk the customers you
    have in order to retrieve a dropped customer, or forget him and keep going.
    
    Progress when player drives over the border into the pizza parlor zone.
    """
    
    @staticmethod
    def think(self, dt):
        if self._is_home():
            return _StateDelivery
# -----------------------------------------------------------------------------
class _StateDelivery(_StateBase):
    """arrived at pizza parlor
    
    Scene. Made it! Pause to process the new customers delivered in this
    section of road.
    
    Progress when scene is done playing.
    """
    #
    # If cinema is ever added, this should pick it up without any hassle.
    #
    @staticmethod
    def enter(self):
        gameplay = self.gameplay
        tally = len(gameplay.moped.customer_stack.stack)
        score = gameplay.level.score
        #self.logger.debug("=> Delivery: {0} + {1} = {2}".format(
        #    score, tally, score + tally))
        gameplay.level.score += tally
        level_num = self.gameplay.level_num
        # determine day/night
        if level_num > 0:
            if self.gameplay.is_day() and random.random() < 0.5:
                self.gameplay.set_night()
            else:
                self.gameplay.set_day()
    
    @staticmethod
    def think(self, dt):
        if self.delivery_scene:
            self.delivery_scene.update(dt)
            if self.delivery_scene.is_finished():
                return _StateNeedMore
        else:
            return _StateNeedMore
# -----------------------------------------------------------------------------
class _StateNeedMore(_StateBase):
    """coach player, restate objective
    
    Scene. Like intro, only "keep going!"
    
    Progress when scene is done playing.
    """
    #
    # If cinema is ever added, this should pick it up without any hassle.
    #
    @staticmethod
    def enter(self):
        amount_needed = self.amount_needed()
        if amount_needed:
            self.make_text('BOSS DEMANDS {0} MORE'.format(amount_needed))
    
    @staticmethod
    def think(self, dt):
        self.text_elapsed += dt
        if self.amount_needed() == 0:
            return _StateFinished
        else:
            if self.need_more_scene:
                self.need_more_scene.update(dt)
                if self.need_more_scene.is_finished():
                    return _StateContinue
            elif self.text_elapsed >= 3.0:
                return _StateContinue
    
    @staticmethod
    def exit(self):
        self.clear_text()
# -----------------------------------------------------------------------------
class _StateFinished(_StateBase):
    """congratulations, objective complete
    
    This plays the Level Complete cinematic, and clears out the gameplay and
    scene objects in preparation for the next level.
    
    Progress when scene is done playing.
    """
    #
    # If cinema is ever added, this should pick it up without any hassle.
    #
    @staticmethod
    def enter(self):
        self.gameplay.level_num += 1
        self.make_text("BOSS SMILES - (YOU SEE SPINACH IN HIS TEETH AGAIN)")
    
    @staticmethod
    def think(self, dt):
        gameplay = self.gameplay
        if gameplay.level_num >= gameplay.level.num_levels:
            return _StateGameWon_r1chardj0n3s
        self.text_elapsed += dt
        if self.finished_scene:
            self.finished_scene.update(dt)
            if self.finished_scene.is_finished():
                return _StateReset
        else:
            if self.text_elapsed >= 3.0:
                return _StateReset
    
    @staticmethod
    def exit(self):
        self.clear_text()
        self.gameplay = None
        self.intro_scene = None
        self.delivery_scene = None
        self.need_more_scene = None
        self.finished_scene = None
# -----------------------------------------------------------------------------
class _StateGameWon_r1chardj0n3s(_StateBase):
    """I can't believe you played the whole thing!
    This class is dedicated to r1chardj0n3s. :)
    """
    @staticmethod
    def enter(self):
        #self.logger.debug(' !!!!! GAME WON !!!!!')
        self.make_text("I can't believe you played it all! (Hi Richard! :))")
    
    @staticmethod
    def think(self, dt):
        self.text_elapsed += dt
        if self.game_won_scene:
            self.game_won_scene.update(dt)
            if self.game_won_scene.is_finished():
                return _StateReset
        else:
            if self.text_elapsed >= 5.0:
                return _StateEndGame
    
    @staticmethod
    def exit(self):
        self.clear_text()
# -----------------------------------------------------------------------------
class _StateEndGame(_StateBase):
    @staticmethod
    def think(self, dt):
        # loop forever, until the caller detects us
        pass
# -----------------------------------------------------------------------------


class LevelMinder(object):
    
    def __init__(self, logger=None):
        #self.logger = logging.getLogger(self.__class__.__name__)
        if logger is not None:
            self.logger = logger
        
        self.current_state = _StateReset
        self.set_state(_StateReset)
        
        self.gameplay = None
        
        # Placeholders for cinema / cut scenes.
        self.intro_scene = None
        self.continue_scene = None
        self.delivery_scene = None
        self.need_more_scene = None
        self.finished_scene = None
        self.game_won_scene = None
        
        self.font = pygame.font.SysFont('sans', 24, True)
        self.text = None
        self.text_rect = None
        self.text_elapsed = 0.0
    
    def configure(self, gameplay, intro_scene=None, continue_scene=None,
                  delivery_scene=None, need_more_scene=None,
                  finished_scene=None, game_won_scene=None):
        #self.logger.debug("=> Configure: {0}".format(gameplay))
        self.gameplay = gameplay
        self.intro_scene = intro_scene
        self.continue_scene = continue_scene
        self.delivery_scene = delivery_scene
        self.need_more_scene = need_more_scene
        self.finished_scene = finished_scene
        self.game_won_scene = game_won_scene
        self.set_state(_StateReset)
    
    # Detect states.
    def is_reset(self):
        return self.current_state is _StateReset
    def is_intro(self):
        return self.current_state is _StateIntro
    def is_continue(self):
        return self.current_state is _StateContinue
    def is_get_ready(self):
        return self.current_state is _StateGetReady
    def is_roundup(self):
        return self.current_state is _StateRoundup
    def is_gauntlet(self):
        return self.current_state is _StateGauntlet
    def is_delivery(self):
        return self.current_state is _StateDelivery
    def is_need_more(self):
        return self.current_state is _StateNeedMore
    def is_finished(self):
        return self.current_state is _StateFinished
    def is_game_won_r1chardj0n3s(self):
        return self.current_state is _StateGameWon_r1chardj0n3s
    def is_end_game(self):
        return self.current_state is _StateEndGame
    
    # For internal use, these test moped position on the roadway.
    def _is_start(self):
        moped = self.gameplay.moped
        roadway = self.gameplay.roadway
        return roadway.is_start(moped)
    def _is_roundup(self):
        moped = self.gameplay.moped
        roadway = self.gameplay.roadway
        return roadway.is_roundup(moped)
    def _is_gauntlet(self):
        moped = self.gameplay.moped
        roadway = self.gameplay.roadway
        return roadway.is_gauntlet(moped)
    def _is_home(self):
        moped = self.gameplay.moped
        roadway = self.gameplay.roadway
        return roadway.is_home(moped)
    
    def amount_needed(self):
        """return the amount of customers needed to complete objective or 0
        """
        level = self.gameplay.level
        goal = level.objective
        have = level.score
        needed = goal - have
        return needed if needed > 0 else 0
    
    def think(self, dt):
        state = self.current_state.think(self, dt)
        if state:
            self.set_state(state)
    
    def set_state(self, new_state):
        if self.current_state:
            #self.logger.debug("=> Exit State: {0}".format(self.current_state))
            self.current_state.exit(self)
        #self.logger.debug("=> Enter State: {0}".format(new_state))
        new_state.enter(self)
        self.current_state = new_state
    
    def make_text(self, text):
        self.text = self.font.render(text, True, Color('yellow'))
        self.text_rect = self.text.get_rect(centerx=settings.SCREEN_WIDTH // 2, y=100)
    
    def clear_text(self):
        self.text = None
        self.text_rect = None
        self.text_elapsed = 0.0
    
