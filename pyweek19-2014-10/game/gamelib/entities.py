# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek19-2014-10
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function
import logging
import random
import time

import pygame

from pyknicpygame import spritesystem
from pyknicpygame import pyknic
from pyknicpygame.pyknic.mathematics import sign
from settings import CUSTOMER_TYPE, C_BADDIE_TYPE, JUMPING_BADDIE_TYPE, OTHER_TYPE, \
    J_BADDIE_RADIUS, J_BADDIE_Y_DIST, J_BADDIE_X_DIST, \
    J_BADDIE_MIN_RND_DUR, J_BADDIE_MAX_RND_DUR, J_BADDIE_MIN_RND_X_DIR, J_BADDIE_MIN_RND_Y_DIR, J_BADDIE_MAX_RND_Y_DIR, \
    J_BADDIE_MAX_RND_X_DIR, S_BADDIE_MAX_RND_DUR, S_BADDIE_MIN_RND_DUR, S_BADDIE_MIN_RND_X_DIR, S_BADDIE_MIN_RND_Y_DIR, \
    S_BADDIE_MAX_RND_Y_DIR, S_BADDIE_MAX_RND_X_DIR, S_BADDIE_X_DIST, S_BADDIE_Y_DIST, J_BADDIE_LAYER, \
    S_BADDIE_LAYER, CUSTOMER_LAYER, TREE_TYPE, TREE_TRUNK_LAYER, TREE_LEAVES_LAYER, C_BADDIE_LAYER, HALF_STREET_WIDTH, NO_TYPE, S_BADDIE_TYPE, CUSTOMER_RADIUS, S_BADDIE_RESET_LETHAL_INTERVAL, STREET_MARKS
import tweening


__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2014"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module


def clamped_to_street_distance(pos, dist):
    x = pos + dist
    if x > HALF_STREET_WIDTH:
        return HALF_STREET_WIDTH - pos
    if x < -HALF_STREET_WIDTH:
        return -HALF_STREET_WIDTH - pos
    return dist

class BaseEntity(object):
    def __init__(self, entity_type, pos_x, pos_y, radius):
        self.type = entity_type
        self.position = pyknic.mathematics.Point2(pos_x, pos_y)
        self.radius = radius


class BaseSpriteEntity(BaseEntity):
    def __init__(self, name, entity_type, pos_x, pos_y, image, radius=1, parallax_factors=pyknic.mathematics.Vec2(1.0, 1.0)):
        BaseEntity.__init__(self, entity_type, pos_x, pos_y, radius)
        self.sprite = spritesystem.Sprite(
            image,
            pyknic.mathematics.Point2(self.position.x * parallax_factors.x,
                                        self.position.y * parallax_factors.y,),
            parallax_factors=parallax_factors,
            name=name)
        self.sprites = [self.sprite]


class PizzaParlorEntity(BaseSpriteEntity):
    
    image_right = None
    image_left = None
    
    def __init__(self, zone_rect, side='right'):
        if PizzaParlorEntity.image_right is None:
            image_right = pygame.image.load('data/image/pizza_parlor_daytime.png').convert()
            image_right.set_colorkey(image_right.get_at((0, 0)))
            image_left = pygame.transform.flip(image_right, True, False)
            PizzaParlorEntity.image_right = image_right
            PizzaParlorEntity.image_left = image_left

        image_rect = PizzaParlorEntity.image_right.get_rect()
        if side == 'right':
            image_rect.x = zone_rect.right
            image = PizzaParlorEntity.image_right
        else:
            image_rect.x = -zone_rect.w / 2 - image_rect.w
            image = PizzaParlorEntity.image_left
        image_rect.bottom = zone_rect.top
        x = image_rect.x
        y = image_rect.y
        #print('zone',zone_rect)
        #print('image',image_rect)
        #quit()
        BaseSpriteEntity.__init__(self, self.__class__.__name__, OTHER_TYPE, x, y, image)
        self.sprite.z_layer = STREET_MARKS


class CheeseEntity(BaseSpriteEntity):

    image = None

    def __init__(self, x, y, r):
        if CheeseEntity.image is None:
            CheeseEntity.image = pygame.image.load('data/image/cheese.png').convert_alpha()

        BaseSpriteEntity.__init__(self, self.__class__.__name__, C_BADDIE_TYPE, x, y, CheeseEntity.image, r)
        self.sprite.z_layer = C_BADDIE_LAYER
        self.sprite.rotation = random.randint(0, 259)
        
        self.time_last_hit = time.time()


class CustomerSprite(spritesystem.Sprite):

    image_state = 0

    def __init__(self, image, pos, index, parallax_factors=0.0, name=''):
        self.index = index
        spritesystem.Sprite.__init__(self, image, pos, parallax_factors=parallax_factors, name=name)

    def cache_key(self):
        return self._rot, self._flip_x, self._flip_y, self._zoom_factor, self._alpha, self.index, self.image_state


class CustomerSpriteEntity(BaseEntity):

    def __init__(self, name, entity_type, pos_x, pos_y, image, radius=1, parallax_factors=pyknic.mathematics.Vec2(1.0, 1.0)):
        BaseEntity.__init__(self, entity_type, pos_x, pos_y, radius)
        self.sprite = CustomerSprite(
            image,
            pyknic.mathematics.Point2(self.position.x * parallax_factors.x,
                                        self.position.y * parallax_factors.y,),
            self.index,
            parallax_factors=parallax_factors,
            name=name)
        self.sprites = [self.sprite]


class CustomerEntity(CustomerSpriteEntity):
    images_walk = None
    images_lie = None
    images_lie_waving = None

    name = None

    def __init__(self, x, y, tweener, logger=None):
        self.logger = logging.getLogger(self.__class__.__name__)
        if logger is not None:
            self.logger = logger

        # TODO: another image
        if CustomerEntity.images_walk is None:
            #CustomerEntity.image = pygame.image.load('data/image/tomato.png')
            CustomerEntity.images_walk = [
                pygame.image.load('data/image/c1.png').convert_alpha(),
                pygame.image.load('data/image/c2.png').convert_alpha(),
                pygame.image.load('data/image/c3.png').convert_alpha(),
            ]

        if CustomerEntity.images_lie is None:
            CustomerEntity.images_lie = [
                pygame.image.load('data/image/c1l.png').convert_alpha(),
                pygame.image.load('data/image/c2l.png').convert_alpha(),
                pygame.image.load('data/image/c3l.png').convert_alpha(),
            ]
        if CustomerEntity.images_lie_waving is None:
            CustomerEntity.images_lie_waving = [
                pygame.image.load('data/image/c1lw.png').convert_alpha(),
                pygame.image.load('data/image/c2lw.png').convert_alpha(),
                pygame.image.load('data/image/c3lw.png').convert_alpha(),
            ]

        #self.index = random.randint(0, len(CustomerEntity.images_walk) - 1)
        #self.index = random.randint(0, len(CustomerEntity.images_walk) - 1)
        self.stack_orientation = 1
        entity_image = CustomerEntity.images_walk[self.index]
        CustomerSpriteEntity.__init__(self, self.__class__.__name__, CUSTOMER_TYPE, x, y, entity_image, radius=CUSTOMER_RADIUS)
        self.tweener = tweener
        self.doing = None

        self.direction = pyknic.mathematics.Vec2(-1, 0)
        self.start_tween()
        self.sprite.z_layer = CUSTOMER_LAYER
        #self.sprite.rotation = random.randint(0, 259)

    def start_tween(self, from_position=None):
        """random"""
        distance_y = J_BADDIE_Y_DIST * random.randint(J_BADDIE_MIN_RND_Y_DIR, J_BADDIE_MAX_RND_Y_DIR)
        distance_x = J_BADDIE_X_DIST * random.randint(J_BADDIE_MIN_RND_X_DIR, J_BADDIE_MAX_RND_X_DIR)
        duration = random.randint(J_BADDIE_MIN_RND_DUR, J_BADDIE_MAX_RND_DUR)
        distance_x = clamped_to_street_distance(self.position.x, distance_x)
        self.direction.x = distance_x
        self.direction.y = distance_y
        self.sprite.rotation = -self.direction.angle - 90
        self.sprite.position = self.position  # same position for sprite as for the entity
        self.tweener.create_tween(self.position, "y", self.position.y, distance_y, duration, tweening.OUT_BOUNCE,
                                  cb_end=self.on_end_customer_tween)
        self.tweener.create_tween(self.position, "x", self.position.x, distance_x, duration, tweening.LINEAR)
        self.sprite.rotation = -pyknic.mathematics.Vec2(distance_x, distance_y).angle - 90
    
    def stop_tween(self):
        self.tweener.remove_tween(self.position, "x")
        self.tweener.remove_tween(self.position, "y")

    def run_away(self, position):
        if self.doing == 'run':
            # let current tween finish
            return
        self.doing = 'run'
        self.stop_tween()
        distance_x = 50 * -sign(position.x - self.position.x) * random.randint(1, 2)
        distance_x = clamped_to_street_distance(self.position.x, distance_x)
        distance_y = 50 * -sign(position.y - self.position.y) * random.randint(1, 2)
        duration = 2.0
        #self.logger.debug('RUNNING AWAY dx={0} dy={1} dur={2}'.format(distance_x, distance_y, duration))
        self.sprite.position = self.position  # same position for sprite as for the entity
        self.tweener.create_tween(self.position, "y", self.position.y, distance_y, duration, tweening.OUT_CUBIC,
                                  cb_end=self.on_end_customer_tween)
        self.tweener.create_tween(self.position, "x", self.position.x, distance_x, duration, tweening.LINEAR)

    def on_end_customer_tween(self, *args):
        self.doing = None
        self.start_tween()

    def on_collision(self, *args):
        self.stop_tween()


class Customer1(CustomerEntity):

    def __init__(self, x, y, tweener, logger=None):
        self.name = 'customer1'
        self.index = 0
        CustomerEntity.__init__(self, x, y, tweener, logger)


class Customer2(CustomerEntity):

    def __init__(self, x, y, tweener, logger=None):
        self.name = 'customer2'
        self.index = 1
        CustomerEntity.__init__(self, x, y, tweener, logger)


class Customer3(CustomerEntity):

    def __init__(self, x, y, tweener, logger=None):
        self.name = 'customer3'
        self.index = 2
        CustomerEntity.__init__(self, x, y, tweener, logger)


class JumpingSprite(spritesystem.Sprite):

    tomato_image = 0

    def cache_key(self):
        return self._rot, self._flip_x, self._flip_y, self._zoom_factor, self._alpha, self.tomato_image


class JumpingSpriteEntity(BaseEntity):

    def __init__(self, name, entity_type, pos_x, pos_y, image, radius=1, parallax_factors=pyknic.mathematics.Vec2(1.0, 1.0)):
        BaseEntity.__init__(self, entity_type, pos_x, pos_y, radius)
        self.sprite = JumpingSprite(
            image,
            pyknic.mathematics.Point2(self.position.x * parallax_factors.x,
                                        self.position.y * parallax_factors.y,),
            parallax_factors=parallax_factors,
            name=name)
        self.sprites = [self.sprite]


class JumpingBaddie(JumpingSpriteEntity):
    image = None
    image_squashed = None

    def __init__(self, x, y, tweener):
        # TODO: another image
        if JumpingBaddie.image is None:
            JumpingBaddie.image = pygame.image.load('data/image/tomato.png').convert_alpha()
        if JumpingBaddie.image_squashed is None:
            JumpingBaddie.image_squashed = pygame.image.load('data/image/tomato_squashed.png').convert_alpha()

        JumpingSpriteEntity.__init__(self, self.__class__.__name__, JUMPING_BADDIE_TYPE, x, y, JumpingBaddie.image, J_BADDIE_RADIUS)
        self.tweener = tweener

        self.on_end_jump_tween()
        self.sprite.z_layer = J_BADDIE_LAYER
        self.sprite.rotation = random.randint(0, 259)

    def on_end_jump_tween(self, *args):
        distance_y = J_BADDIE_Y_DIST * random.randint(J_BADDIE_MIN_RND_Y_DIR, J_BADDIE_MAX_RND_Y_DIR)
        distance_x = J_BADDIE_X_DIST * random.randint(J_BADDIE_MIN_RND_X_DIR, J_BADDIE_MAX_RND_X_DIR)
        distance_x = clamped_to_street_distance(self.position.x, distance_x)
        duration = random.randint(J_BADDIE_MIN_RND_DUR, J_BADDIE_MAX_RND_DUR)
        self.sprite.position = self.position  # same position for sprite as for the entity
        self.tweener.create_tween(self.position, "y", self.position.y, distance_y, duration, tweening.OUT_BOUNCE,
                                  cb_end=self.on_end_jump_tween)
        self.tweener.create_tween(self.position, "x", self.position.x, distance_x, duration, tweening.LINEAR)
        self.sprite.rotation = -pyknic.mathematics.Vec2(distance_x, distance_y).angle - 90

    def on_collision(self, *args):
        self.tweener.remove_tween(self.position, "x")
        self.tweener.remove_tween(self.position, "y")
        self.type = NO_TYPE
        self.sprite.tomato_image = 1
        self.sprite.set_image(self.image_squashed)
        self.sprite.z_layer = C_BADDIE_LAYER


class SpeedBaddie(BaseSpriteEntity):
    image = None

    def __init__(self, x, y, tweener, scheduler):
        if SpeedBaddie.image is None:
            SpeedBaddie.image = pygame.image.load('data/image/cutter.png').convert_alpha()

        BaseSpriteEntity.__init__(self, self.__class__.__name__, S_BADDIE_TYPE, x, y, SpeedBaddie.image, J_BADDIE_RADIUS)
        self.tweener = tweener
        self.scheduler = scheduler
        self.sprite.anchor = "midtop"

        self.on_end_jump_tween()
        self.sprite.z_layer = S_BADDIE_LAYER
        self.is_lethal = True
        self.move_id = 0
        self.rotate_id = 0

    def set_is_lethal(self, value):
        if value != self.is_lethal:
            self.is_lethal = value
            if value is False:
                self.scheduler.schedule(self.set_is_lethal, S_BADDIE_RESET_LETHAL_INTERVAL, 0, True)
        return 0


    def on_end_jump_tween(self, *args):
        self.rotate_id = self.scheduler.schedule(self.do_rotate, 1)

    def do_rotate(self):
        distance_y = S_BADDIE_Y_DIST * random.randint(S_BADDIE_MIN_RND_Y_DIR, S_BADDIE_MAX_RND_Y_DIR)
        distance_x = S_BADDIE_X_DIST * random.randint(S_BADDIE_MIN_RND_X_DIR, S_BADDIE_MAX_RND_X_DIR)
        distance_x = clamped_to_street_distance(self.position.x, distance_x)
        duration = random.randint(S_BADDIE_MIN_RND_DUR, S_BADDIE_MAX_RND_DUR)
        new_angle = (-pyknic.mathematics.Vec2(distance_x, distance_y).angle - 90) % 360
        self.tweener.create_tween(self.sprite, "rotation", self.sprite.rotation, new_angle - self.sprite.rotation, 1, tweening.IN_OUT_CUBIC)
        self.move_id = self.scheduler.schedule(self.start_move, 1, 0, distance_x, distance_y, duration)
        return 0

    def start_move(self, distance_x, distance_y, duration):
        self.sprite.position = self.position  # same position for sprite as for the entity
        self.tweener.create_tween(self.position, "y", self.position.y, distance_y, duration, tweening.OUT_EXPO,
                                  cb_end=self.on_end_jump_tween)
        distance_x = clamped_to_street_distance(self.position.x, distance_x)
        self.tweener.create_tween(self.position, "x", self.position.x, distance_x, duration, tweening.OUT_EXPO)


    def on_collision(self, *args):
        if self.is_lethal:
            self.tweener.remove_tween(self.position, "x")
            self.tweener.remove_tween(self.position, "y")
            self.scheduler.remove(self.rotate_id)
            self.scheduler.remove(self.move_id)
            self.on_end_jump_tween()


class Tree(BaseSpriteEntity):
    trunk_image = None
    leaves_image = None


    def __init__(self, x, y, r):
        if Tree.trunk_image is None:
            Tree.trunk_image = pygame.image.load('data/image/trunk.png').convert()
        if Tree.leaves_image is None:
            Tree.leaves_image = pygame.image.load('data/image/leaves.png').convert_alpha()

        # entity_type, pos_x, pos_y, image, radius=1, parallax_factors=pyknic.mathematics.Vec2(1.0, 1.0)
        BaseSpriteEntity.__init__(self, self.__class__.__name__, TREE_TYPE, x, y, Tree.trunk_image, r)
        self.sprite.z_layer = TREE_TRUNK_LAYER

        parallax_factors = pyknic.mathematics.Vec2(1.15, 1.15)
        self.leave_sprite = spritesystem.Sprite(Tree.leaves_image, pyknic.mathematics.Point2(x * parallax_factors.x, y * parallax_factors.y),
                                                z_layer=TREE_LEAVES_LAYER, parallax_factors=parallax_factors,
                                                name=self.__class__.__name__)

        self.leave_sprite.rotation = random.randint(0, 359)

        self.sprites.append(self.leave_sprite)