# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek19-2014-10
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
# * Neither the name of the <organization> nor the
# names of its contributors may be used to endorse or promote products
# derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function
import logging
import random
import time

from contextpause import ContextPause
import entities
import garbage
import sound
import contextwon


__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2014"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

import pygame
from pygame.locals import Color, BLEND_RGBA_ADD, Rect

from settings import (
    SCREEN_SIZE, OTHER_TYPE, NO_TYPE, C_BADDIE_TYPE, CUSTOMER_TYPE, SHOW_ZONES, MOPED_TYPE,
    JUMPING_BADDIE_TYPE, HALF_STREET_WIDTH, COLLISION_LOGGING, TREE_RADIUS, TREE_TYPE,
    C_BADDIE_RADIUS, TREE_COUNT_MAX,
    ACTIVE_ENTITIES_UPDATE_INTERVAL, ACTIVE_ENTITIES_Y_RANGE, S_BADDIE_TYPE, CRASH_JUMP_ANGLE,
    CRASH_JUMP_MIN_DIST, CRASH_JUMP_MAX_DIST,
    CUSTOMER_LAYER, TREE_TRUNK_LAYER, STREET_MARKS, NIGHT_MODE,
    PLAY_MUSIC, PLAY_MOPED_SOUNDS, TEST_END_GAME,
    USE_DAY_AND_NIGHT, SUN_RISE_AND_SET_DUR, SUN_RISE_AND_SET_MIN, SUN_RISE_AND_SET_MAX,
    SPRITE_CACHE_MAX_MEMORY, SPRITE_CACHE_DEFAULT_EXPIRATION, SPRITE_CACHE_AT_MOST,
    SPRITE_CACHE_AUTO_TUNE_AGING, SPRITE_CACHE_TUNE_MIN_PERCENT, SPRITE_CACHE_TUNE_MAX_PERCENT,
    SPRITE_CACHE_TUNE_STEP)

import tweening
import pyknicpygame
from pyknicpygame.pyknic.context import Context
from pyknicpygame.pyknic.mathematics import Point2, Vec2

import level
import levelminder
import moped
import roadway
import sprite

image_cache = pyknicpygame.spritesystem.Sprite._image_cache
megabyte = 2 ** 20


def get_objects_in_range(position, list_of_objects, y_range, key=lambda v: v.position.y):
    """
    Gets the range indices, list must be sorted by the key!
    :param position:
    :param list_of_objects:
    :param y_range:
    :param key:
    :return:
    """
    pos_limit = position.y + y_range  # bottom limit due to inverted y axis
    neg_limit = position.y - y_range  # top limit due to inverted y axis
    p_index = bisect_right(list_of_objects, pos_limit, key=key)
    n_index = bisect_left(list_of_objects, neg_limit, key=key)
    return n_index, p_index


class Gameplay(Context):
    def __init__(self, logger=None):
        Context.__init__(self)
        self.collision_functions = {}
        self.logger = logging.getLogger(self.__class__.__name__)
        if logger is not None:
            self.logger = logger
        self.event_no_break = pyknicpygame.pyknic.events.Signal()
        self.event_no_accelerate = pyknicpygame.pyknic.events.Signal()
        self.event_steer = pyknicpygame.pyknic.events.Signal()
        self.event_break = pyknicpygame.pyknic.events.Signal()
        self.event_accelerate = pyknicpygame.pyknic.events.Signal()
        self.event_toggle_light = pyknicpygame.pyknic.events.Signal(sort_order=pyknicpygame.pyknic.events.NEW_LAST)
        self.tweener = tweening.Tweener()
        self.scheduler = pyknicpygame.pyknic.timing.Scheduler()
        sw, sh = SCREEN_SIZE
        self.renderer = pyknicpygame.spritesystem.DefaultRenderer()
        cam = pyknicpygame.spritesystem.Camera(pygame.Rect(0, 0, sw, sh), position=Point2(0, 0))
        self.cams = [cam]

        # objects
        self.moped = moped.Moped()
        self.moped_sprite = sprite.MopedSprite(self.moped)
        self.moped.customer_stack.event_customer_dropped.add(self.on_customer_dropped)
        self.moped_sound = None

        self.active_entities = set()
        self.inactive_entities = []
        self.inactive_sprites = []

        # cam
        cam.track = self.moped

        self.renderer.add_sprite(self.moped_sprite)
        self.renderer.add_sprite(self.moped_sprite.front_tire_sprite)

        # actions
        self.event_accelerate.add(self.moped.on_accelerate)
        self.event_break.add(self.moped.on_break)
        self.event_break.add(self.on_break)
        self.event_steer.add(self.moped.on_steer)
        self.event_no_accelerate.add(self.moped.on_no_accelerate)
        self.event_no_break.add(self.moped.on_no_break)
        self.event_no_break.add(self.on_no_break)
        self.event_toggle_light.add(self.moped.on_toggle_light)
        self.event_toggle_light.add(self.on_toggle_light)

        self.event_toggle_light.fire()

        # level (progression rules) and roadway (world geometry)
        self.roadway = roadway.Roadway()
        self.levelminder = levelminder.LevelMinder()
        self.level_num = 0
        self.level = None

        self.cracks = []
        self.line = None
        
        self.garbage_man = garbage.Garbage()
        self.garbage_man.disable()

        self.keys_pressed = pygame.key.get_pressed()
        self.now_playing = None

        self.scheduler.schedule(self.print_cache_stats, 5.0)
        self.scheduler.schedule(self.collect_garbage, 10.0)

    def print_cache_stats(self):
        self.logger.debug(
            'ImageCache: auto={} expire={} count={} SIZE={}/{} HIT={}% PURGE=(old={} mem={})'.format(
            image_cache.auto_tune_aging,
            round(image_cache.default_expiration, 1),
            image_cache.count(),
            int(image_cache.size() / megabyte),
            int(image_cache.max_memory / megabyte),
            image_cache.hit_ratio(),
            image_cache.old_purged,
            image_cache.mem_purged))
        self.scheduler.schedule(self.print_cache_stats, 5.0)

    def collect_garbage(self):
        #self.logger.debug('Collecting garbage')
        self.garbage_man.collect()
        self.scheduler.schedule(self.collect_garbage, 10.0)

    def add_entity(self, entity):
        if entity in self.inactive_entities:
            return
        self.inactive_entities.append(entity)
        self.inactive_entities.sort(key=lambda ent: ent.position.y)
        if hasattr(entity, "sprites"):
            self.renderer.add_sprites(entity.sprites)
        self.update_active_entities_list()

    def remove_entity(self, entity):
        if entity is self.moped:
            return
        if entity in self.active_entities:
            self.active_entities.remove(entity)
        if entity in self.inactive_entities:
            self.inactive_entities.remove(entity)
        if hasattr(entity, "sprites"):
            self.renderer.remove_sprites(entity.sprites)
            for spr in entity.sprites:
                if spr in self.inactive_sprites:
                    self.inactive_sprites.remove(spr)

    def clear_entities(self):
        for ent in list(self.inactive_entities):
            self.remove_entity(ent)

    def exit(self):
        self.garbage_man.collect()
        pygame.mouse.set_visible(True)

    def suspend(self):
        pygame.mouse.set_visible(True)

    def resume(self):
        pygame.mouse.set_visible(False)

    def enter(self):

        pygame.mouse.set_visible(False)

        if NIGHT_MODE:
            self.cams[0].ambient_is_on = True
            self.cams[0].ambient_factor = 0.15

        # DON'T USE THE CACHE, IT EATS THE MEMORY
        #pyknicpygame.spritesystem.Sprite.configure_global_image_cache(True, 2, 2, SPRITE_MAX_CACHE_IMAGE_COUNT)
        pyknicpygame.spritesystem.Sprite.configure_global_image_cache(
            True, 0, 2,
            max_memory=SPRITE_CACHE_MAX_MEMORY,
            default_expiration=SPRITE_CACHE_DEFAULT_EXPIRATION,
            at_most=SPRITE_CACHE_AT_MOST,
            auto_tune_aging=SPRITE_CACHE_AUTO_TUNE_AGING,
            tune_min_percent=SPRITE_CACHE_TUNE_MIN_PERCENT,
            tune_max_percent=SPRITE_CACHE_TUNE_MAX_PERCENT,
            tune_step=SPRITE_CACHE_TUNE_STEP)
        image_cache.enable_age_cap(False)
        image_cache.enable_memory_cap(True)

        self.scheduler.schedule(self.update_active_entities_list, ACTIVE_ENTITIES_UPDATE_INTERVAL)
        self.scheduler.schedule(self.update_active_sprites_list, ACTIVE_ENTITIES_UPDATE_INTERVAL,
                                ACTIVE_ENTITIES_UPDATE_INTERVAL / 2)

        self.collision_functions[MOPED_TYPE | CUSTOMER_TYPE] = self.on_moped_customer_collision
        self.collision_functions[MOPED_TYPE | C_BADDIE_TYPE] = self.on_moped_cheese_baddie_collision
        self.collision_functions[MOPED_TYPE | JUMPING_BADDIE_TYPE] = self.on_moped_jumping_baddie_collision
        self.collision_functions[MOPED_TYPE | S_BADDIE_TYPE] = self.on_moped_speed_baddie_collision
        self.collision_functions[MOPED_TYPE | TREE_TYPE] = self.on_moped_tree_collision

        img = pygame.image.load('data/image/roadside.png').convert()
        pos = pyknicpygame.pyknic.mathematics.Point2(-HALF_STREET_WIDTH, 0)
        self.street_side_left = pyknicpygame.spritesystem.Sprite(img, pos, name='street1')
        self.street_side_left.anchor = "midleft"
        self.street_side_left.parallax_factors = pyknicpygame.pyknic.mathematics.Vec2(1.0, 0.0)
        self.street_side_left.rotation = 180

        pos = pyknicpygame.pyknic.mathematics.Point2(HALF_STREET_WIDTH, 0)
        self.street_side_right = pyknicpygame.spritesystem.Sprite(img, pos, name='street2')
        self.street_side_right.anchor = "midleft"
        self.street_side_right.parallax_factors = pyknicpygame.pyknic.mathematics.Vec2(1.0, 0.0)

        self.renderer.add_sprite(self.street_side_left)
        self.renderer.add_sprite(self.street_side_right)

        self.line = pygame.image.load('data/image/line.png').convert()
        self.cracks = [
            pygame.image.load('data/image/crack1.png').convert_alpha(),
            pygame.image.load('data/image/crack2.png').convert_alpha(),
            pygame.image.load('data/image/crack3.png').convert_alpha(),
            pygame.image.load('data/image/crack4.png').convert_alpha(),
            pygame.image.load('data/image/crack5.png').convert_alpha(),
            pygame.image.load('data/image/crack6.png').convert_alpha(),
            pygame.image.load('data/image/crack7.png').convert_alpha(),
        ]
        self.pepple = pygame.image.load('data/image/pebble.png').convert()


        # self.cams[0].ambient_factor = 0.2
        # img = pygame.Surface((255, 1), pygame.SRCALPHA)
        # for i in range(255):
        # img.set_at((i, 0), (i, i, i, 128))
        # img = pygame.transform.scale(img, (255, 255))
        # self.light = pyknicpygame.spritesystem.Sprite(img, pyknicpygame.pyknic.mathematics.Point2(0, 0))
        # self.light.is_light = 1
        # self.renderer.add_sprite(self.light)
        #
        # light = pyknicpygame.spritesystem.Sprite(img, pyknicpygame.pyknic.mathematics.Point2(0, -100))
        # light.is_light = 1
        # light.flags = pygame.BLEND_RGB_MULT
        # light.z_layer = 1000
        # self.renderer.add_sprite(light)

    def exit(self):
        pygame.mixer.music.fadeout(1000)

    def make_customers(self):
        """make customers and baddies in the roundup zone
        """
        current_level = self.level
        zone = current_level.roundup_rect

        customer_type, baddie_type = 0, 1
        for which_type, num_entities in (
                (customer_type, current_level.roundup_customers),
                (baddie_type, current_level.roundup_baddies),):
            for y in range(zone.top, zone.bottom, zone.height // num_entities):
                x = random.randint(-200, 200)
                z_layer = random.randint(0, 10)
                if which_type == customer_type:
                    # make customer
                    customer_i = random.randint(1, 3)
                    if customer_i == 1:
                        ent = entities.Customer1(x, y + random.randint(-100, 100), self.tweener)
                    elif customer_i == 2:
                        ent = entities.Customer2(x, y + random.randint(-100, 100), self.tweener)
                    elif customer_i == 3:
                        ent = entities.Customer3(x, y + random.randint(-100, 100), self.tweener)
                    self.add_entity(ent)
                elif which_type == baddie_type:
                    # make jumper
                    j = entities.JumpingBaddie(x, y, self.tweener)
                    self.add_entity(j)
                    # make speedy
                    s = entities.SpeedBaddie(x, y, self.tweener, self.scheduler)
                    self.add_entity(s)


    def make_baddies(self):
        """make baddies in the gauntlet zone
        """
        current_level = self.level
        zone = current_level.gauntlet_rect
        num_static = current_level.gauntlet_stationary
        num_mobile = current_level.gauntlet_mobile
        #self.logger.debug('make_baddies:static={0} mobile={1}'.format(num_static, num_mobile))

        static_type, mobile_type = 0, 1
        for which_type, num_baddies in (
                (static_type, current_level.gauntlet_stationary),
                (mobile_type, current_level.gauntlet_mobile)):
            for y in range(zone.top, zone.bottom, zone.height // num_baddies):
                # create entity
                x = random.randint(-200, 200)
                z_layer = random.randint(0, 10)
                parallax_factor = pyknicpygame.pyknic.mathematics.Vec2(1.0, 1.0)
                if random.randint(0, 100) >= 50:  # 50% chance
                    parallax_factor = pyknicpygame.pyknic.mathematics.Vec2(z_layer / 100.0 + 1, z_layer / 100.0 + 1)
                if which_type == static_type:
                    # make cheese
                    ent = entities.CheeseEntity(random.randint(-200, 200), y + random.randint(-100, 100),
                                                C_BADDIE_RADIUS)
                    self.add_entity(ent)
                elif which_type == mobile_type:
                    # make jumper
                    j = entities.JumpingBaddie(x, y, self.tweener)
                    self.add_entity(j)
                    # make speedy
                    s = entities.SpeedBaddie(x, y, self.tweener, self.scheduler)
                    self.add_entity(s)

        # make trees (use cap from settings.py for performance help)
        num_trees = min(num_static, TREE_COUNT_MAX)
        for y in range(zone.top, zone.bottom, zone.height // num_trees):
            # create entity
            tree = entities.Tree(random.choice((-1, -0.85, -0.9, 0.9, 0.95, 1)) * HALF_STREET_WIDTH, y, TREE_RADIUS)
            self.add_entity(tree)
            self.renderer.add_sprite(tree.leave_sprite)

        # add the pizza parlor - add both sides so player will see it
        zone = current_level.home_rect
        chalet = entities.PizzaParlorEntity(zone, 'right')
        self.add_entity(chalet)
        chalet = entities.PizzaParlorEntity(zone, 'left')
        self.add_entity(chalet)

        ri = random.randint
        for i in range(100):
            pos = pyknicpygame.pyknic.mathematics.Point2(ri(-HALF_STREET_WIDTH, HALF_STREET_WIDTH),
                                                         ri(self.level.level_length, 0))
            spr = pyknicpygame.spritesystem.Sprite(self.pepple, pos, name='street3')
            spr.z_layer = TREE_TRUNK_LAYER
            spr.rotation = ri(0, 360)
            self.inactive_sprites.append(spr)

        for y in range(self.level.level_length, 0, 300):
            pos = pyknicpygame.pyknic.mathematics.Point2(0, y)
            spr = pyknicpygame.spritesystem.Sprite(self.line, pos, name='street4')
            spr.z_layer = STREET_MARKS
            self.inactive_sprites.append(spr)

        for y in range(self.level.level_length, 0, 200):
            pos = pyknicpygame.pyknic.mathematics.Point2(HALF_STREET_WIDTH + 60, y + ri(-20, 20))
            spr = pyknicpygame.spritesystem.Sprite(self.cracks[ri(0, 6)], pos, name='street5')
            spr.z_layer = TREE_TRUNK_LAYER
            self.inactive_sprites.append(spr)

            pos = pyknicpygame.pyknic.mathematics.Point2(-HALF_STREET_WIDTH - 60, y + ri(-20, 20))
            spr = pyknicpygame.spritesystem.Sprite(self.cracks[ri(0, 6)], pos, name='street6')
            spr.rotation = 180
            spr.z_layer = TREE_TRUNK_LAYER
            self.inactive_sprites.append(spr)

    def is_day(self):
        #return self.cams[0].ambient_factor > SUN_RISE_AND_SET_MIN
        return not self.cams[0].ambient_is_on

    def set_day(self):
        if not USE_DAY_AND_NIGHT or self.is_day():
            return
        self.cams[0].ambient_is_on = True
        self.tweener.remove_tween(self.cams[0], "ambient_factor")
        change = SUN_RISE_AND_SET_MAX - SUN_RISE_AND_SET_MIN
        self.tweener.create_tween(
            self.cams[0], "ambient_factor",
            self.cams[0].ambient_factor, change, SUN_RISE_AND_SET_DUR, tweening.LINEAR,
            cb_end=self.set_ambient_off)
    
    def set_ambient_off(self, *args):
        self.cams[0].ambient_is_on = False

    def set_night(self):
        if not USE_DAY_AND_NIGHT or not self.is_day():
            return
        self.cams[0].ambient_is_on = True
        self.tweener.remove_tween(self.cams[0], "ambient_factor")
        change = SUN_RISE_AND_SET_MIN - SUN_RISE_AND_SET_MAX
        self.tweener.create_tween(
            self.cams[0], "ambient_factor",
            self.cams[0].ambient_factor, change, SUN_RISE_AND_SET_DUR, tweening.LINEAR)

    def think(self, delta_time):
        """
        Update the game.
        :param delta_time: time passed in last time step (game time)
        :return:
        """

        # TODO: We may need to dictate what LevelMinder states some parts of
        # this code runs or suspends in.

        self._handle_events()

        moped = self.moped
        moped.update(delta_time)
        self.moped_sprite.update(delta_time)

        moped_position = moped.position
        # street border collision, crashing makes moped to stop dead
        if moped_position.x > HALF_STREET_WIDTH or moped_position.x < -HALF_STREET_WIDTH:
            self.on_moped_road_border_collision()

        # collision checks, no border checks in x direction
        moped_position_get_distance_sq = moped_position.get_distance_sq
        moped_radius = moped.radius
        collision_functions_get = self.collision_functions
        moped_type = moped.type
        # default_coll_func = self.no_collision_function_found
        for ent in self.active_entities.copy():
            # self.logger.debug("{0}: {1}".format(ent, ent.position))
            # TODO: maybe use other shape for moped, or two circles
            if ent.type != NO_TYPE and ent.type != moped_type and moped_position_get_distance_sq(ent.position) < (
                        moped_radius + ent.radius) ** 2:
                # func = collision_functions_get(moped_type | ent.type, default_coll_func)
                func = collision_functions_get[moped_type | ent.type]
                if moped_type < ent.type:
                    func(moped, ent)
                else:
                    func(ent, moped)
                    # if ent.type == CUSTOMER_TYPE:
                    # if moped_position_get_distance_sq(ent.position) <= 200 * 200:
                    #         #logging.debug('=> Customer sees moped')
                    #         ent.run_away(moped_position)

        self.tweener.update(delta_time)
        self.scheduler.update(delta_time)
        self._check_end_condition()

        self.update_levelminder(delta_time)
        self.update_moped_sound(delta_time)

        image_cache.update()
        

    def update_active_entities_list(self):

        # move the ones in range from inactive to active
        self.inactive_entities.sort(key=lambda v: v.position.y)
        lo, hi = get_objects_in_range(self.moped.position, self.inactive_entities, ACTIVE_ENTITIES_Y_RANGE)
        self.active_entities.update(self.inactive_entities[lo:hi])
        # self.tweener.resume_tweens(activated)

        # move the ones out of range from active to inactive
        self.active_entities.difference_update(self.inactive_entities[hi:])
        self.active_entities.difference_update(self.inactive_entities[:lo])
        # self.tweener.pause_tweens(deactivated)

        # self.logger.debug(" active entities {0}/{1}".format(len(self.active_entities), len(self.inactive_entities)))
        return ACTIVE_ENTITIES_UPDATE_INTERVAL

    def update_active_sprites_list(self):

        self.inactive_sprites.sort(key=lambda v: v.position.y)
        lo, hi = get_objects_in_range(self.moped.position, self.inactive_sprites, ACTIVE_ENTITIES_Y_RANGE)

        self.renderer.add_sprites(self.inactive_sprites[lo:hi])
        self.renderer.remove_sprites(self.inactive_sprites[:lo])
        self.renderer.remove_sprites(self.inactive_sprites[hi:])

        # self.logger.debug(" active sprites {0}/{1}".format(len(self.renderer.get_sprites()), len(self.inactive_sprites)))
        return ACTIVE_ENTITIES_UPDATE_INTERVAL


    def update_levelminder(self, delta_time):
        """Update the level minder and respond to state changes"""
        self.levelminder.think(delta_time)

        # TODO: cinematic scenes. These can be placards or animations.
        # (Probably won't have time for anything fancy.)

        if self.levelminder.is_reset():
            self.level = level.Level(self.level_num)
            self.moped.position.y = 0.0
            self.moped.customer_stack.clear()
            self.clear_entities()
            self.add_entity(self.moped)
            self.make_customers()
            self.make_baddies()
            # Divide our roadway into zones.
            self.roadway.configure(
                self.level.start_rect,
                self.level.roundup_rect,
                self.level.gauntlet_rect,
                self.level.home_rect)
            self.levelminder.configure(
                self, intro_scene=None, continue_scene=None,
                delivery_scene=None, need_more_scene=None,
                finished_scene=None)
            if TEST_END_GAME:
                self.level_num = self.level.num_levels
                self.levelminder.set_state(levelminder._StateFinished)
        elif self.levelminder.is_intro():
            # No reponse needed: cinematic
            pass
        elif self.levelminder.is_continue():
            # Move the moped back to zero.
            self.moped.position.y = 0.0
            self.moped.customer_stack.clear()
            self.clear_entities()
            self.add_entity(self.moped)
            self.make_customers()
            self.make_baddies()
        elif self.levelminder.is_get_ready():
            # TODO: flash "DRIVE TO ROUND UP" text
            # get ready, player >>> drive to roundup >>>
            pass
        elif self.levelminder.is_roundup():
            # TODO: flash "ROUND EM UP" text
            # round 'em up, player
            pass
        elif self.levelminder.is_gauntlet():
            # TODO: flash "RACE TO ROOMAN'S PIZZA" text
            # get home, player; look out, baddies all around!!
            self.update_song('gauntlet')
            pass
        elif self.levelminder.is_delivery():
            # No reponse needed: cinematic
            # phew, we made it
            #self.logger.debug('garbage:{0}'.format(self.garbage_man.get_count()))
            self.garbage_man.collect()
            pass
        elif self.levelminder.is_need_more():
            # TODO: flash "OBJECTIVE IS ..." reminder text
            pass
        elif self.levelminder.is_finished():
            # No reponse needed: cinematic
            # yay, we're done with the level
            pass
        elif self.levelminder.is_game_won_r1chardj0n3s():
            # TODO: Game finished! You win!!!
            if self.now_playing:
                self.now_playing.fadeout(6000)
            pass
        elif self.levelminder.is_end_game():
            # Exit to command prompt/desktop/menu.
            pyknicpygame.pyknic.context.pop()
            pyknicpygame.pyknic.context.push(contextwon.ContextWon())

    def update_song(self, where):
        if PLAY_MUSIC is False:
            return
        if where == 'gauntlet':
            gauntlet_names = ('gauntlet1', 'gauntlet2', 'gauntlet3')
            gauntlet_songs = [sound.songs[n] for n in gauntlet_names]
            if self.now_playing is None:
                self.now_playing = random.choice(gauntlet_songs)
                self.now_playing.play()
            elif self.now_playing not in gauntlet_songs:
                self.now_playing.fadeout(3000)
            elif not self.now_playing.get_busy():
                choices = list(filter(lambda s: s != self.now_playing, gauntlet_songs))
                # TODO: BUG TypeError: object of type 'filter' has no len()
                if choices:
                    self.now_playing = random.choice(choices)
                    self.now_playing.play()
        elif where == 'menu':
            if self.now_playing is None:
                self.now_playing = sound.songs['menu']
                self.now_playing.queue()
            else:
                self.now_playing.fadeout(3000)
                self.now_playing = sound.songs['menu']
                self.now_playing.queue()
        elif where == 'credits':
            if self.now_playing is None:
                self.now_playing = sound.songs['credits']
                self.now_playing.queue()
            else:
                self.now_playing.fadeout(3000)
                self.now_playing = sound.songs['credits']
                self.now_playing.queue()

    def update_moped_sound(self, dt=None):
        if PLAY_MOPED_SOUNDS is False:
            return
        sfx = sound.sfx
        try:
            my_chan = self.moped_sound
            my_sound = my_chan.get_sound()
        except:
            my_sound = None
        snd = None
        if my_sound is None:
            # just starting: idle
            random.seed(time.time())
            self.moped_sound = sfx['rev_idle'].play()
        elif self.keys_pressed[pygame.K_UP]:
            # up key pressed: accelerating
            if my_sound is sfx['rev_idle']:
                my_chan.play(sfx['rev_up'])
            else:
                my_chan.queue(sfx[random.choice(['rev_down_up', 'rev_down_up', 'rev_sustained', 'rev_rev_rev'])])
        elif self.keys_pressed[pygame.K_DOWN] and self.moped.v > 0.0:
            # down key pressed: braking
            if my_sound is sfx['rev_down']:
                my_chan.queue(sfx['rev_idle'])
            elif my_sound is sfx['rev_idle']:
                my_chan.queue(sfx['rev_idle'])
            else:
                my_chan.play(sfx['rev_down'])
        else:
            # no keys pressed: coasting
            if my_sound is sfx['rev_idle']:
                my_chan.queue(sfx['rev_idle'])
            elif my_sound is sfx['rev_down']:
                my_chan.queue(sfx['rev_idle'])
            else:
                my_chan.play(sfx['rev_down'])

    def draw(self, screen, do_flip=True):
        for cam in self.cams:
            self.renderer.draw(screen, cam, fill_color=(127, 127, 127), do_flip=False, interpolation_factor=1.0)

            # ### DEBUG display for zones:
            if SHOW_ZONES and self.level:
                lev = self.level
                screen = pygame.display.get_surface()
                sr = screen.get_rect()
                cv = 20
                debug_rects = (
                    (Color(cv, 0, 0, 0), lev.start_rect),
                    (Color(0, cv, 0, 0), lev.roundup_rect),
                    (Color(0, 0, cv, 0), lev.gauntlet_rect),
                    (Color(cv, 0, cv, 0), lev.home_rect),
                )
                for c, r in debug_rects:
                    r2 = Rect(r)
                    r2.normalize()
                    screen_pos = cam.world_to_screen(Vec2(r.x, r.y))
                    r2.bottom = screen_pos.y
                    r2.centerx = screen_pos.x
                    if r2.colliderect(sr):
                        ### This crashes on my system - Gumm - don't fill outside of the screen - DR0ID
                        screen.fill(c, r2.clip(screen.get_rect()), BLEND_RGBA_ADD)
                        # pygame.draw.rect(screen, c, r2, 1)
            if self.levelminder.text:
                screen = pygame.display.get_surface()
                screen.blit(self.levelminder.text, self.levelminder.text_rect)

        if do_flip:
            pygame.display.flip()


    def _handle_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pyknicpygame.pyknic.context.pop()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_1:
                    print('press 1')
                    self.set_day()
                elif event.key == pygame.K_0:
                    print('press 0')
                    self.set_night()
                elif event.key == pygame.K_ESCAPE or event.key == pygame.K_p:
                    pyknicpygame.pyknic.context.push(ContextPause())
                elif event.key == pygame.K_UP:
                    self.event_accelerate.fire()
                elif event.key == pygame.K_DOWN:
                    self.event_break.fire()
                    # debug keys
                    # elif event.key == pygame.K_1:
                    # self.push_customer_stack()
                    # elif event.key == pygame.K_2:
                    #     self.pop_customer_stack()
                elif event.key == pygame.K_SPACE:
                    self.event_toggle_light.fire()
                #elif event.key == pygame.K_1:
                #   self.set_day()
                #elif event.key == pygame.K_0:
                #    self.set_night()
                elif event.key == pygame.K_9:
                    v = self.cams[0].ambient_factor - 0.1
                    if v < 0:
                        v = 1.0
                    self.cams[0].ambient_factor = v
                elif event.key == pygame.K_RETURN:
                    pygame.image.save(
                        pygame.display.get_surface(),
                        'screen-{0}.png'.format(time.strftime('%M%S')))
                # elif event.key == pygame.K_z:
                #     print('images={}'.format(len(image_cache._cache)))
                #     for name in image_cache._cache.keys():
                #         print('\n{} {}'.format(name, len(image_cache._cache[name])))
                #         for e in image_cache._cache[name]:
                #             print(e)
            elif event.type == pygame.KEYUP:
                if event.key == pygame.K_UP:
                    self.event_no_accelerate.fire()
                elif event.key == pygame.K_DOWN:
                    self.event_no_break.fire()

        pressed = pygame.key.get_pressed()
        self.keys_pressed = pressed
        direction = pressed[pygame.K_RIGHT] - pressed[pygame.K_LEFT]
        self.event_steer.fire(direction)  # -1: left  0: straight  1: right

    def _check_end_condition(self):
        # TODO
        pass

    def no_collision_function_found(self, a, b, *args):
        # self.logger.warn("no collision function found for types {0} and {1}, args {2}".format(a.type, b.type, args))
        if b.type == OTHER_TYPE:
            b.sprite.image.fill((255, 0, 0))

    # def pop_customer_stack(self):
    # ## for debug only (press key 2)
    #     ent = self.moped.customer_stack.pop()
    #     # self.remove_entity(ent)
    #     self.renderer.update_z_layers()
    #     return ent
    #
    # def push_customer_stack(self):
    #     ## for debug only (press key 1)
    #     img = pygame.Surface((30, 50))
    #     col = lambda: random.randint(0, 255)
    #     img.fill((col(), col(), col()))
    #     ent = entities.BaseSpriteEntity(NO_TYPE, self.moped.position.x, self.moped.position.y, img)
    #     ent.sprite.rotation = -self.moped.direction.angle
    #     self.moped.customer_stack.push(ent)
    #     self.add_entity(ent)

    def on_customer_dropped(self, customers):
        for dropped in customers:
            self.remove_entity(dropped)
            dropped.type = CUSTOMER_TYPE
            dropped.start_tween()
            dropped.position.copy_values(self.moped.position)
            dropped.sprite.z_layer = CUSTOMER_LAYER

            n = -self.moped.direction
            n.rotate(random.randint(-CRASH_JUMP_ANGLE, CRASH_JUMP_ANGLE))
            dist_ = n * random.randint(CRASH_JUMP_MIN_DIST, CRASH_JUMP_MAX_DIST)
            #self.logger.debug("moped pos, dir {0} -- {1}".format(self.moped.position, self.moped.direction))
            dropped.position += dist_
            #self.logger.debug("Customer lost  {0} -> {1}".format(dropped.position, dist_))

            dropped.run_away(self.moped.position)
            self.add_entity(dropped)
            self.renderer.update_z_layers()


    def on_moped_customer_collision(self, moped, customer):
        if customer.doing is None:
            # self.remove_entity(customer)
            moped.customer_stack.push(customer)
            self.renderer.update_z_layers()
            if COLLISION_LOGGING:
                self.logger.debug("customer collision")

    def on_moped_cheese_baddie_collision(self, moped, baddie1):
        t = time.time()
        if t - baddie1.time_last_hit > 5.0:
            sfx = sound.playn('skid', 1)
            baddie1.time_last_hit = t
        if moped.v > 35.0:
            moped.v *= 0.4
        if COLLISION_LOGGING:
            self.logger.debug("baddie1 collision!")
            # slowing down may be enough

    def on_moped_jumping_baddie_collision(self, moped, baddie):
        sound.sfx[random.choice(('splat_big', 'splat_gloop', 'splat_juicy', 'splat_splatter1', 'splat_splatter2'))].play()
        baddie.on_collision()
        self.moped.on_crash(moped.position.x, 1, immovable=False)
        self.renderer.update_z_layers()

    def on_moped_speed_baddie_collision(self, moped, baddie):
        baddie.on_collision()
        #self.remove_entity(baddie)
        # self.moped_looses_customer()
        if baddie.is_lethal:
            sound.sfx[random.choice(('crash_light1', 'crash_light2'))].play()
            baddie.set_is_lethal(False)
            self.moped.on_crash(moped.position.x, 3, immovable=False)

    def on_moped_tree_collision(self, moped, tree):
        sound.playn('crash_hard', 1)
        self.moped.on_crash(self.moped.position.x, immovable=True)

    def on_moped_road_border_collision(self):
        if self.moped.v < 200.0:
            num_to_drop = 0
            sound.playn('bump2', 1)
        else:
            num_to_drop = None
            sound.playn('crash_hard', 1)
        self.moped.on_crash(
            pyknicpygame.pyknic.mathematics.sign(self.moped.position.x) * HALF_STREET_WIDTH,
            num_to_drop, immovable=True)

    def on_break(self):
        if self.moped.v > 200.0:
            sound.sfx['skid'].play()
        self.renderer.add_sprite(self.moped_sprite.back_light_breaking_spr)
        self.renderer.add_sprite(self.moped_sprite.back_light_spr)

    def on_no_break(self):
        self.renderer.remove_sprite(self.moped_sprite.back_light_breaking_spr)
        if not self.moped.is_light_on:
            self.renderer.remove_sprite(self.moped_sprite.back_light_spr)

    def on_toggle_light(self):
        if self.moped.is_light_on:
            self.renderer.add_sprite(self.moped_sprite.front_light_spr)
            self.renderer.add_sprite(self.moped_sprite.back_light_on_spr)
            self.renderer.add_sprite(self.moped_sprite.back_light_spr)
        else:
            self.renderer.remove_sprite(self.moped_sprite.front_light_spr)
            self.renderer.remove_sprite(self.moped_sprite.back_light_on_spr)
            self.renderer.remove_sprite(self.moped_sprite.back_light_spr)



def bisect_right(a, x, lo=0, hi=None, key=lambda v: v):
    """Return the index where to insert item x in list a, assuming a is sorted.

    The return value i is such that all e in a[:i] have e <= x, and all e in
    a[i:] have e > x.  So if x already appears in the list, a.insert(x) will
    insert just after the rightmost x already there.

    Optional args lo (default 0) and hi (default len(a)) bound the
    slice of a to be searched.
    """
    if lo < 0:
        raise ValueError('lo must be non-negative')
    if hi is None:
        hi = len(a)
    while lo < hi:
        mid = (lo + hi) // 2
        if x < key(a[mid]):
            hi = mid
        else:
            lo = mid + 1
    return lo


def bisect_left(a, x, lo=0, hi=None, key=lambda v: v):
    """Return the index where to insert item x in list a, assuming a is sorted.

    The return value i is such that all e in a[:i] have e < x, and all e in
    a[i:] have e >= x.  So if x already appears in the list, a.insert(x) will
    insert just before the leftmost x already there.

    Optional args lo (default 0) and hi (default len(a)) bound the
    slice of a to be searched.
    """

    if lo < 0:
        raise ValueError('lo must be non-negative')
    if hi is None:
        hi = len(a)
    while lo < hi:
        mid = (lo + hi) // 2
        if key(a[mid]) < x:
            lo = mid + 1
        else:
            hi = mid
    return lo