# crop images

##############
# configure...
##############
# the files
glob_pattern = '*.gif'
rename_fmt = 'runningman{0}.png'

# clip size and offset
# e.g. native size of pic is 200x200
#      w, h = 180, 160
#      x, y = (200 - w) / 2, 200 - h
w, h = 180, 160
x, y = (200 - w) / 2, 200 - h

# resize to (w, h) pixels
resize_pixels = (100, 100)

#########################################################################
# end of configure (unless you want to modify the image conversion steps)
#########################################################################

import glob

import pygame
from pygame.locals import *

files = glob.glob(glob_pattern)
surfs = []

if not files:
    print 'No input files found'
    quit()

pygame.init()
screen = pygame.display.set_mode(resize_pixels)
clock = pygame.time.Clock()
grey_color = Color('grey')
white_color = Color('white')

# image conversion: load, clip, resize, save images
clip = Rect(x, y, w, h)
for idx, name in enumerate(files):
    # load
    surf = pygame.image.load(name).convert()
    # clip
    new_surf = pygame.Surface((w, h))
    new_surf.blit(surf, (0, 0), clip)
    # resize
    new_surf = pygame.transform.scale(new_surf, resize_pixels)
    # save
    pygame.image.save(new_surf, rename_fmt.format(idx))
    
    # queue for test animation
    new_surf.set_colorkey(new_surf.get_at((0,0)))
    surfs.append(new_surf)

# frame around image will be drawn using this rect
box = Rect((0, 0), new_surf.get_size())

idx = 0
running = True
while running:
    clock.tick(5)
    screen.fill(grey_color)
    for e in pygame.event.get():
        if e.type == KEYDOWN:
            if e.key == K_ESCAPE:
                running = False
        elif e.type == QUIT:
            running = False
    screen.blit(surfs[idx], (0,0))
    pygame.draw.rect(screen, white_color, box, 1)
    pygame.display.flip()
    idx += 1
    if idx >= len(surfs):
        idx = 0
