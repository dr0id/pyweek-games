# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek19-2014-10
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function


__version_number__ = (0, 0, 0, 0)
__version__ = ".".join([str(num) for num in __version_number__])

__author__ = 'dr0iddr0id {at} gmail [dot] com'
__copyright__ = "DR0ID @ 2014"
__credits__ = ["DR0ID", "Gummbum"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

import pygame

DOMAIN_NAME = "pyweek19-VROOM41"  # used for translation files

###############################################################################
##
## Tunables
##
###############################################################################

## For weirdos who don't like music or mopeds. =)
PLAY_MUSIC = True
PLAY_MOPED_SOUNDS = True

## Setting USE_DAY_AND_NIGHT to False is probably the biggest boost in
## performance. When True, random day and night courses are presented.
## When False, only day courses are presented. The lighting is expensive,
## but the effect is VERY FUN; thus, you should at least play some easy
## levels with lighting enabled.
##
## In game, pressing 0 will make it nighttime and 1 will make it daytime.
##
USE_DAY_AND_NIGHT = True  # en/disable lighting effects; False should help slower computers

## Lower levels are less demanding on the computer because they have fewer
## baddies. You can either pick the levels to play here, or edit the level
## data in level.py.
if __debug__:
    PICK_LEVELS = [0,1,2,3,4,5,6]  # run_game_debug.py: list of levels to play; see level.py
else:
    PICK_LEVELS = [0,1,2,3,4,5,6]  # run_game.py: list of levels to play; see level.py

## Smaller screen may give better performance; but the world isn't scalable
## and having a wide view helps in choosing your paths.
SCREEN_WIDTH = 1024
SCREEN_HEIGHT = 768
#SCREEN_WIDTH = 800
#SCREEN_HEIGHT = 600

###############################################################################
##
## No more tunables
##
###############################################################################

SCREEN_SIZE = (SCREEN_WIDTH, SCREEN_HEIGHT)
HALF_STREET_WIDTH = 400
CAPTION = "VROOM41!"
FLAGS = pygame.DOUBLEBUF
BIT_DEPTH = 32

MIXER_FREQUENCY = 0
MIXER_BUFFER_SIZE = 128

SIM_TIME_STEP = 0.025

SUN_RISE_AND_SET_MIN = 0.15  # valid: 0.0 to 1.0; recommended 0.15
SUN_RISE_AND_SET_MAX = 1.0  # valid: 0.0 to 1.0; recommended 1.0
SUN_RISE_AND_SET_DUR = 7.0  # seconds
NIGHT_MODE = False

MOPED_BREAKING_FORCE = 70000.0  # max force of the breaks
MOPED_ACCELERATION_FORCE = 50000.0  # max force of the motor causing acceleration
MOPED_STEER_CHANGE = 2.0  # how much the steering angle is changed per update
MOPED_MAX_STEERING_ANGLE_DEG = 30  # max steering angle of front wheel in degrees, recommended < 90
MOPED_MASS = 90  # total mass of the moped + person + ...
MOPED_DRAG_COEFFICIENT = 0.305  #0.105  # air drag coefficient, the smaller, the higher max speed
MOPED_LENGTH = 38  # the length of the moped in meters, err pixels
MOPED_STEERING_RECUPERATION_FACTOR = 0.89  # how fast the steering wheel turns back to 0, closer to 1.0 is slower
MOPED_RADIUS = 20
MOPED_LOGGING = False

CRASH_JUMP_ANGLE = 135
CRASH_JUMP_MIN_DIST = 20
CRASH_JUMP_MAX_DIST = 50

CUSTOMER_RADIUS = 20
CUSTOMER_STACK_OFFSET = 7  # moves the stack away from the driver

COLLISION_LOGGING = False

#ACTIVE_ENTITIES_Y_RANGE = 2000  # the range in where the collision checks will be done, moped.position.y +- Y_RANGE
ACTIVE_ENTITIES_Y_RANGE = 1000  # the range in where the collision checks will be done, moped.position.y +- Y_RANGE
ACTIVE_ENTITIES_UPDATE_INTERVAL = 1.0  # seconds

# increase this better rotation caching; but clearing the cache makes the game hiccup;
# lower it if the game periodically freezes for a split second for no apparent reason
# SPRITE_MAX_CACHE_IMAGE_COUNT = 10000
SPRITE_CACHE_MAX_MEMORY = 1.5 * 2 ** 30
SPRITE_CACHE_DEFAULT_EXPIRATION = 15.0
SPRITE_CACHE_AT_MOST = 50
SPRITE_CACHE_AUTO_TUNE_AGING = False
SPRITE_CACHE_TUNE_MIN_PERCENT = 94.0
SPRITE_CACHE_TUNE_MAX_PERCENT = 95.0
SPRITE_CACHE_TUNE_STEP = 0.01

SHOW_ZONES = False

# jumping baddie
J_BADDIE_RADIUS = 40
J_BADDIE_Y_DIST = 40
J_BADDIE_MAX_RND_Y_DIR = 1
J_BADDIE_MIN_RND_Y_DIR = -1
J_BADDIE_X_DIST = 50
J_BADDIE_MAX_RND_X_DIR = 1
J_BADDIE_MIN_RND_X_DIR = -1
J_BADDIE_MIN_RND_DUR = 1
J_BADDIE_MAX_RND_DUR = 3

# speed baddie
S_BADDIE_RADIUS = 15
S_BADDIE_Y_DIST = 200
S_BADDIE_MAX_RND_Y_DIR = 1
S_BADDIE_MIN_RND_Y_DIR = -1
S_BADDIE_X_DIST = 200
S_BADDIE_MAX_RND_X_DIR = 1
S_BADDIE_MIN_RND_X_DIR = -1
S_BADDIE_MIN_RND_DUR = 1
S_BADDIE_MAX_RND_DUR = 3
S_BADDIE_RESET_LETHAL_INTERVAL = 1  # seconds, after how long it will hurt again

TREE_RADIUS = 40
TREE_COUNT_MAX = 25  # cap on trees for performance
C_BADDIE_RADIUS = 35

# LAYERS
#
#
#       tree tops
#       (street lamp top) ??
# 150 ----------------
#       customer stack
#       driver
#       moped
#       lights
#       tires
# 100 ----------------
#       customers walking
#       baddies
#       tree base
#       street lamps
# 10 ----------------- (on top things for collision)
#       leaves
#       stones (small)
#       road marks
#       street
# 0 ------------------

TREE_LEAVES_LAYER = 160
CUSTOMER_STACK_LAYER = 130
MOPED_LIGHT_LAYER = 103
MOPED_LAYER = 102
TIRE_LAYER = 100
CUSTOMER_LAYER = 18
J_BADDIE_LAYER = 17
S_BADDIE_LAYER = 16
C_BADDIE_LAYER = 15
TREE_TRUNK_LAYER = 2
STREET_MARKS = 1
STREET_LAYER = 0

# type definitions, use bit shift to make sure, that one bit position is used for each type
# one could define groups of types by using the bitwise and, e.g. group = t1 & t2
NO_TYPE = 0
MOPED_TYPE = 1 << 0
OTHER_TYPE = 1 << 1
CUSTOMER_TYPE = 1 << 2
C_BADDIE_TYPE = 1 << 3
JUMPING_BADDIE_TYPE = 1 << 4
TREE_TYPE = 1 << 5
S_BADDIE_TYPE = 1 << 6

TEST_END_GAME = False
