# -*- coding: utf-8 -*-

"""
..todo:: docstring
"""

# -----------------------------------------------------------------------------

from optparse import OptionParser


parser = OptionParser()
parser.add_option("-f", "--file", dest="filename",
                  help="write report to FILE", metavar="FILE")
parser.add_option("-q", "--quiet",
                  action="store_false", dest="verbose", default=True,
                  help="don't print status messages to stdout")


mydict = {'mydictsetting':1}

(opts, args) =  parser.parse_args(["--file=outfile", "-q", "this is an argument"])

mydict.update(dict(opts.__dict__))

# -----------------------------------------------------------------------------

# just for testing manually
if __name__ == '__main__':
    print opts, opts.__class__
    print mydict
    print args



# -----------------------------------------------------------------------------
