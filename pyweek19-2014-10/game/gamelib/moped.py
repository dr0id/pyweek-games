# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek19-2014-10
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description

BUGS:
- Moped.direction is not calculated accurately in Moped.update.
DEBUG:moped:moped on steer 1
DEBUG:moped:update: sa: 16.6 angle: -90.0 dir: <Vec2(0.0, -1.0, w=0) at 0x3069710> pos: <Vec2(0.0, 0.0, w=0) at 0x2c5fd50>
                                    ^^^^^            ^^^^^^^^^

Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function
import math
import random
import pygame
import pyknicpygame

from settings import MOPED_BREAKING_FORCE, MOPED_ACCELERATION_FORCE, MOPED_STEER_CHANGE, MOPED_MAX_STEERING_ANGLE_DEG, \
    MOPED_MASS, MOPED_DRAG_COEFFICIENT, MOPED_LENGTH, MOPED_STEERING_RECUPERATION_FACTOR, MOPED_TYPE, MOPED_RADIUS, \
    NO_TYPE

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2014"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module


import logging
import entities
import settings
from pyknicpygame.pyknic.mathematics import sign


class Moped(entities.BaseEntity):

    def __init__(self, logger=None):
        entities.BaseEntity.__init__(self, MOPED_TYPE, 0.0, 0.0, MOPED_RADIUS)
        # steering_angle: angle of the front wheel
        self.steering_angle = 0.0

        self.logger = logging.getLogger(__name__)
        if logger is not None:
            self.logger = logger

        self.direction = pyknicpygame.pyknic.mathematics.Vec2(0.0, -1.0)  # the direction the moped is looking
        self.position = pyknicpygame.pyknic.mathematics.Point2(0.0, 0.0)  # back wheel
        self.position_delta = pyknicpygame.pyknic.mathematics.Vec2(0.0, 0.0)  # position_delta: how far moped moved in 2d
        self.a = 0  # acceleration
        self.v = 0  # velocity
        self.f = 0  # accumulated force for one update
        self.current_break_force = 0
        self.current_acc_force = 0.0

        self.drag_coefficient = MOPED_DRAG_COEFFICIENT

        self.customer_stack = CustomerStack(self)
        self.is_light_on = False

    def on_accelerate(self):
        if settings.MOPED_LOGGING:
            self.logger.debug("moped on accelerate")
        self.current_acc_force = MOPED_ACCELERATION_FORCE

    def on_no_accelerate(self):
        if settings.MOPED_LOGGING:
            self.logger.debug("moped on no acceleration")
        self.current_acc_force = 0.0

    def on_break(self):
        if settings.MOPED_LOGGING:
            self.logger.debug("moped on break")
        self.current_break_force = MOPED_BREAKING_FORCE
        if self.v <= 0.0:
            self.f -= MOPED_BREAKING_FORCE * 4

    def on_no_break(self):
        if settings.MOPED_LOGGING:
            self.logger.debug("moped on no break")
        self.current_break_force = 0.0

    def on_crash(self, x, count_to_loose=None, immovable=True):
        if immovable:
            self.v = 0
        else:
            self.v = max(self.v * 0.2, 30.0)
        self.a = 0
        self.f = 0
        self.position.x = x
        if count_to_loose is None:
            count_to_loose = int(len(self.customer_stack.stack) // 2)  # TODO: make it dependent of v?
        lost = []
        for i in range(count_to_loose):
            ent = self.customer_stack.pop()
            if ent is not None:
                lost.append(ent)
        self.customer_stack.event_customer_dropped.fire(lost)

    def on_steer(self, direction):
        """
        Handle steering action
        :param direction: -1: left   0: straight   1: right
        :return:
        """
        if direction == 0 and self.steering_angle != 0 and self.v > 0:
            self.steering_angle *= MOPED_STEERING_RECUPERATION_FACTOR
        else:
            if settings.MOPED_LOGGING:
                self.logger.debug("moped on steer " + str(direction))
            self.steering_angle += direction * MOPED_STEER_CHANGE
            if abs(self.steering_angle) > MOPED_MAX_STEERING_ANGLE_DEG:
                self.steering_angle = sign(self.steering_angle) * MOPED_MAX_STEERING_ANGLE_DEG

    def update(self, delta_time):
        # self.logger.debug("update " + str(delta_time))

        # update forces
        drag_force = self.v * self.v * self.drag_coefficient
        self.f += self.current_acc_force - sign(self.v) * (self.current_break_force + drag_force)

        # integrate acceleration and velocity
        self.a = self.f / MOPED_MASS
        self.v += self.a * delta_time

        # clamp
        clamp_value = 10.0
        if self.current_acc_force == 0.0 and (0 < self.v < clamp_value or 0 > self.v > -clamp_value):
            self.v = 0.0

        # calculate delta distance
        ds = self.v * delta_time

        # update position
        self.position_delta = self.direction * ds  # this is saved, maybe useful to implement scrolling

        # update direction and position
        front = self.position + self.direction * MOPED_LENGTH
        steer_direction = self.direction.rotated(self.steering_angle)
        steer_direction.normalize()

        self.position += self.position_delta

        self.direction = (front + steer_direction * ds) - self.position
        self.direction.normalize()

        if settings.MOPED_LOGGING:
            # self.logger.debug("update: df: {6}f: {0} a: {1} v: {2} ds: {3} vs: {4} p: {5}".format(self.f, self.a, self.v, ds, self.vs, self.position, drag_force))
            self.logger.debug("update: af: {7} bf: {8} df: {6} f: {0} a: {1} v: {2} ".format(
                self.f, self.a, self.v, ds,
                self.position_delta,
                self.position, drag_force,
                self.current_acc_force,
                self.current_break_force))
            # self.logger.debug("update: sa: {0} angle: {2} dir: {1} pos: {3}".format(self.steering_angle, self.direction, self.direction.angle, self.position))
        self.customer_stack.update(delta_time)

        self.f = 0.0

    def on_toggle_light(self):
        self.is_light_on = not self.is_light_on


class CustomerStack(entities.BaseEntity):

    def __init__(self, moped):
        entities.BaseEntity.__init__(self, NO_TYPE, 0, 0, 0)
        self.moped = moped
        self.position = moped.position
        self.direction = moped.direction
        self.stack = []
        self.event_customer_dropped = pyknicpygame.pyknic.events.Signal()

    def push(self, ent):
        # convert to different entity
        ent.type = NO_TYPE
        ent.stop_tween()
        ent.sprite.z_layer = len(self.stack) + settings.CUSTOMER_STACK_LAYER
        # ent.sprite.anchor = pyknicpygame.pyknic.mathematics.Vec2(10, 0)
        ent.sprite.image_state = 1
        ent.sprite.set_image(entities.CustomerEntity.images_lie[ent.index])
        ent.stack_orientation = random.choice((-1, 1))
        self.stack.append(ent)

    def pop(self):
        # convert back to other entity
        if self.stack:
            ent =  self.stack.pop()
            ent.sprite.z_layer = settings.CUSTOMER_LAYER
            ent.sprite.anchor = 'center'
            ent.sprite.image_state = 0
            ent.sprite.set_image(entities.CustomerEntity.images_walk[ent.index])
            return ent
        return None

    def clear(self):
        self.stack = []

    def update(self, delta_time):
        # update position and orientation
        to_loose = []

        customer_mass = 60
        # force = 0.0  # going straight more or less, no force
        # if abs(self.moped.steering_angle) > 2:
        #     drive_radius = MOPED_LENGTH / math.tan(self.moped.steering_angle)
        #     radial_acc = self.moped.v ** 2 / drive_radius
        #     force = customer_mass * radial_acc

        # dir = pyknicpygame.pyknic.mathematics.Vec2(0.0, 0.0)
        # if abs(self.moped.steering_angle) > 4:

        distance_offset = self.moped.direction * -settings.CUSTOMER_STACK_OFFSET

        offsetx = self.moped.direction.normal_right * self.moped.v * self.moped.steering_angle * 0.0005
        # offsety = self.moped.direction * self.moped.a * self.moped.v * 0.00005
        offsety = self.moped.direction * (+self.moped.v * 1.0 + self.moped.a * delta_time * 20.5) * 0.005

        for idx, ent in enumerate(self.stack):
            parent = self.stack[idx - 1]
            if idx == 0:
                parent = self.moped

            # actual_offset = pyknicpygame.pyknic.mathematics.Vec2(offset.x, offset.y)
            # offset.x = offset.x * (idx + 1)
            # offset.y = offset.y * (idx + 1)
            # actual_offset = offsetx * (idx + 1) - offsety * (idx + 1)
            actual_offset = offsetx - offsety
            ent.position = parent.position + actual_offset + distance_offset
            ent.sprite.position.copy_values(ent.position)
            ent.sprite.rotation = -self.moped.direction.angle - 90 * ent.stack_orientation

            distance_offset = pyknicpygame.pyknic.mathematics.Vec2(0, 0)

            if actual_offset.length > 3.5:
                # to_loose.append(ent)
                ent.sprite.image_state = 2
                ent.sprite.set_image(entities.CustomerEntity.images_lie_waving[ent.index])
            else:
                ent.sprite.image_state = 1
                ent.sprite.set_image(entities.CustomerEntity.images_lie[ent.index])

        # for ent in to_loose:
        #     self.event_loose_customer.fire(ent)


