import random

import pygame
from pygame.locals import *

import imagecache


num_initial_balls = 100

image_cache = imagecache.ComplexCache(default_expiration=5.0)


# Results of various steps (len(range(127, 255, step)) ** 3):
# 8 creates 4096 base images
# 16 creates 512 base images
# 24 creates 216 base images
# 32 creates 64 base images
# 48 creates 27 base images
# 64 creates 8 base images
step = 32
color_component_range = range(127, 255, step)
colors = []


def load_images():
    for r in color_component_range:
        for g in color_component_range:
            for b in color_component_range:
                color = (r, g, b)
                colors.append(color)
                image = pygame.Surface((64, 64))
                image.fill(color)
                # N.B.: Our base images must not be allowed to expire. We look
                # them up in cache when we have to make a new transformation.
                # There are other ways to do this, but we can demonstrate the
                # per-entry aging this way.
                image_cache.add(color, image, imagecache.vanilla_key, expire=0.0)


class Ball(object):

    def __init__(self):
        # our ident in the cache
        self._color = random.choice(colors)

        # our transforms in the cache
        rot, flipy, flipx, zoom, alpha = imagecache.vanilla_key
        self._rot = rot
        self._flip_y = flipy
        self._flip_x = flipx
        self._zoom = zoom
        self._alpha = alpha
        self._cache_key = self._make_key()

        # our blitting stuf
        self._image = None
        self.rect = None

        self.cache_hit = 1
        self.cache_miss = 1

        # init our image and rect - this is also triggered by transforms
        self._init_image()

        # set a randon location on the screen
        screen = pygame.display.get_surface()
        screen_rect = screen.get_rect()
        self.rect.x = random.randrange(screen_rect.x, screen_rect.right - self.rect.w)
        self.rect.y = random.randrange(screen_rect.y, screen_rect.bottom - self.rect.h)

        # set a random rotation rate
        self._rot_speed = (random.random() + 0.1) * random.choice((-1, 1))

    def update(self):
        """a little behavior to cause real-time interaction with the cache"""
        rot = (self._rot + self._rot_speed) % 360.0
        self.set('_rot', rot)

    @property
    def image(self):
        """if None, then image is uninitialized or a transform attr has changed"""
        if self._image is None:
            self._init_image()
        return self._image

    def set(self, key, value):
        """a hack to avoid coding a bunch of decorators to trigger the dirty state"""
        current = getattr(self, key)
        if current != value:
            setattr(self, key, value)
            self._cache_key = self._make_key()
            self._image = None

    def _init_image(self):
        """called when self._image is None

        Look up in cache. If not found, then create the image by transforming base image.
        """
        image = image_cache.get(self._color, self._cache_key)
        if image is None:
            self.cache_miss += 1
            image = image_cache.get(self._color, imagecache.vanilla_key)
            assert image is not None
            if self._rot != 0.0 or self._zoom != 1.0:
                image = pygame.transform.rotozoom(image, self._rot, self._zoom)
                if self._rot not in (0.0, 90.0, 180.0, 270.0):
                    image.set_colorkey((0, 0, 0))
            if self._alpha != 255:
                image.set_alpha(self._alpha)
            # ignoring flipy and flipx
            image_cache.add(self._color, image, self._cache_key)
        else:
            self.cache_hit += 1
        self._image = image
        rect = self.rect
        if rect is None:
            rect = self._image.get_rect()
        else:
            rect = self._image.get_rect(center=rect.center)
        self.rect = rect

    def _make_key(self):
        """when a transform triggers a new image, we need to update our key

        This is for efficiency only. We could construct a new key on the fly,
        but that is wasteful.
        """
        rot = int(round(self._rot)) % 360
        zoom = round(self._zoom, 1)
        alpha = int(round(self._alpha))
        return image_cache.make_transforms(rot, self._flip_x, self._flip_y, zoom, alpha)


class Game(object):

    def __init__(self):
        self.max_fps = 60
        self.dt = 1.0 / self.max_fps / 1.0
        #print self.dt; quit()
        self.fps_elapsed = self.max_fps
        self.clock = pygame.time.Clock()

        self.balls = []
        self.make_balls(num_initial_balls)

        # run-time behavior states
        self.aging = False
        self.running = False

        self.hits = 0
        self.misses = 0
        self.reset_hits = False

    def run(self):
        """loop it"""
        self.running = True
        while self.running:
            self.update()
            self.draw()
            self.clock.tick(self.max_fps)

    def update(self):
        """update the game"""
        self.hits = 0
        self.misses = 0
        for ball in self.balls:
            ball.update()
            if self.reset_hits:
                ball.cache_hit = 1
                ball.cache_miss = 1
            self.hits += ball.cache_hit
            self.misses += ball.cache_miss
        self.reset_hits = False

        if self.aging:
            image_cache.age()

        self.update_fps()

        for e in pygame.event.get():
            if e.type == KEYDOWN:
                if e.key == K_ESCAPE:
                    self.running = False
                elif e.key == K_a:
                    self.aging = not self.aging
                elif e.key == K_h:
                    self.reset_hits = True
                elif e.key == K_p:
                    self.print_cache()

    def update_fps(self):
        """update the caption"""
        self.fps_elapsed += self.dt
        if self.fps_elapsed >= 2.0:
            self.fps_elapsed -= 2.0
            pygame.display.set_caption('{0} fps | Aging {1} | {2} cached | Hit ratio {3}%'.format(
                int(self.clock.get_fps()), 'on' if self.aging else 'off', image_cache.count(),
                int(float(self.hits) / (self.hits + self.misses) * 100)))

    def draw(self):
        """typical draw routine"""
        screen = pygame.display.get_surface()
        screen.fill((0, 0, 0))
        for ball in self.balls:
            screen.blit(ball.image, ball.rect)
        pygame.display.flip()

    def make_balls(self, num):
        for i in range(num):
            self.balls.append(Ball())

    def print_cache(self):
        ball = self.balls[0]
        for transforms in sorted(image_cache._cache[ball._color]):
            print transforms
        print image_cache._cache.keys()


if __name__ == '__main__':
    pygame.init()
    pygame.display.set_mode((800, 600))
    load_images()
    game = Game()
    game.run()
