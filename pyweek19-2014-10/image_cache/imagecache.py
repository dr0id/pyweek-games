"""imagecache.py - Economic image caches

To do: aging
"""


import time


# This is a key that can be used as a default for the initially created image.
# It represents a without any rotation, flipy, flipx, zoom, and alpha.
vanilla_key = (0, False, False, 1.0, 255)


class _Cache(object):

    def __init__(self, default_expiration=0.0):
        self._cache = {}
        self.default_expiration = default_expiration

    @staticmethod
    def make_entry(image, expire):
        now = time.time()
        return [image, now, now, 0, expire]

    def clear(self):
        self._cache.clear()


class SimpleCache(_Cache):

    def __init__(self, default_expiration=0.0):
        _Cache.__init__(self, default_expiration)
        self._age1 = []
        self._age_ident = None

    def add(self, ident, image, expire=None):
        if expire is None:
            expire = self.default_expiration
        self._cache[ident] = self.make_entry(image, expire)

    def get(self, ident):
        image = None
        cache = self._cache
        if image in cache:
            entry = cache[ident]
            image = entry[0]
            entry[2] = time.time()
            entry[3] += 1
        return image

    def remove(self, ident):
        try:
            del self._cache[ident]
        except KeyError:
            pass

    def age(self):
        now = time.time()
        cache = self._cache
        if not len(self._age1):
            self._age1[:] = cache.keys()
        hunk = self._age1[0:100]
        del self._age1[0:100]
        for ident in hunk:
            try:
                entry = cache[ident]
                if entry[4] == 0.0:
                    continue
                if now - entry[2] > entry[4]:
                    del cache[ident]
            except KeyError:
                pass

    def count(self):
        return len(self._cache)


class ComplexCache(_Cache):

    def __init__(self, default_expiration=0.0):
        _Cache.__init__(self, default_expiration)
        self._age1 = []
        self._age2 = []
        self._age_ident = None

    def add(self, ident, image, transforms, expire=None):
        if expire is None:
            expire = self.default_expiration
        cache = self._cache
        if ident in cache:
            images = cache[ident]
        else:
            images = {}
            cache[ident] = images
        images[transforms] = self.make_entry(image, expire)

    def get(self, ident, transforms):
        image = None
        cache = self._cache
        if ident in cache:
            images = cache[ident]
            if transforms in images:
                entry = images[transforms]
                image = entry[0]
                entry[2] = time.time()
                entry[3] += 1
        return image

    def remove(self, ident, transforms):
        try:
            del self._cache[ident][transforms]
        except KeyError:
            pass

    def age(self):
        now = time.time()
        cache = self._cache
        if not len(self._age1):
            self._age1[:] = cache.keys()
        if not len(self._age2):
            self._age_ident = self._age1.pop()
            self._age2[:] = cache[self._age_ident].keys()
        ident = self._age_ident
        hunk = self._age2[0:100]
        del self._age2[0:100]
        for transforms in hunk:
            try:
                entry = cache[ident][transforms]
                if entry[4] == 0.0:
                    continue
                if now - entry[2] > entry[4]:
                    del cache[ident][transforms]
                    if len(cache[ident]) == 0:
                        del cache[ident]
            except KeyError:
                pass

    def count(self):
        n = 0
        for e in self._cache.values():
            n += len(e)
        return n

    @staticmethod
    def make_transforms(rot=0.0, flip_x=False, flip_y=False, zoom_factor=1.0, alpha=255):
        return rot, flip_x, flip_y, zoom_factor, int(alpha)
