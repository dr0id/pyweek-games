# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek22-2016-09
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division

import logging
import random

import pygame

from . import pyknic
from . import settings
from .entitybug import AntMoveState, AntClingState, AntEatState, AntStuckState, AntSuckedInState, AntInitialState, \
    AntCrushedState
from .entityvacuumcleaner import VcStuck, VcStateBagFullOn
from .gameresource import GameResource
from .pyknic import tweening
from .pyknic.mathematics import Vec3 as Vec, Vec2

logger = logging.getLogger(__name__)

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2016"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 


class FrameAnimation(object):

    def __init__(self, scheduler, start_index, frame_count, fps, cb=None):
        self.cb = cb
        self.fps = fps
        self.start_index = start_index
        self.last_index = start_index + frame_count
        self.frame_count = frame_count
        self.scheduler = scheduler
        self.current_index = start_index
        self._schedule_id = 0

    def start(self):
        if self.frame_count > 1 and self.fps > 0:
            # only bother the scheduler if there is something to animate!
            offset = random.random() / self.fps  # random offset so no all bugs start their animation at the same time
            self._schedule_id = self.scheduler.schedule(self._update, 0.0, offset)

    def stop(self):
        if self._schedule_id and self.frame_count > 1 and self.fps > 0:
            self.scheduler.remove(self._schedule_id)

    def reset(self):
        self.current_index = self.start_index

    # noinspection PyUnusedLocal
    def _update(self, *args, **kwargs):
        # self._old_time = current_time  ## for interpolation see draw()
        self.current_index += 1
        if self.current_index > self.last_index:
            self.current_index = self.start_index
            if self.cb:
                self.cb()
        return 1.0 / self.fps

    def draw(self, *args, **kwargs):
        # interpolation = (current_time - self._old_time) / self.interval  ## for interpolation
        pass


class EntityAnimation(object):

    def __init__(self, graphics, entity, resolution, anchor=Vec2(0, 0)):
        self.graphics = graphics
        self.resolution = resolution
        self.entity = entity
        self.anchor = anchor
        self.animation = FrameAnimation(None, 0, 1, 1)
        self.state = None
        self.size = entity.size // 10 / 10.0 + 1

    @property
    def angle(self):
        a = (-self.entity.direction.angle + self.resolution/2.0) % 360  # put in range [0, 360)
        return int(a / self.resolution) * self.resolution
    # 360 // a * (360 // r)

    @property
    def key(self):
        return self.entity.kind, self.state, self.animation.current_index, self.angle, self.size

    def update(self):
        if hasattr(self.entity, "sm"):
            self.state = self.entity.sm.current_state.__name__
        else:
            self.state = self.entity.__class__.__name__

        anim = self.graphics.get_anim(self.entity.kind, self.state, self.entity)
        if self.animation != anim:
            self.animation.stop()
            self.animation = anim
            self.animation.start()


class SuctionGfx(object):

    def __init__(self, rect, world):
        self.blit_pos = rect
        self.rect = pygame.Rect(rect.left + 4, rect.top + 6, 17, 192)
        self.world = world
        self.image = pygame.image.load("data/EmptyBar2.png").convert_alpha()
        self.bug = 0

    def draw(self, screen):
        strength = self.world.vc.get_strength()
        filling = 0
        if strength > 0:
            filling = 0.3 + random.random() * 0.05
            if self.world.vc.sm.current_state == VcStuck:
                filling = 0.9
            if self.bug > 0:
                filling += 0.4
                self.bug -= 1
            filling = filling if filling <= 1.0 else 1.0
        if self.world.vc.sm.current_state == VcStateBagFullOn:
            filling = 0.9

        h = int(self.rect.h * filling)
        r = pygame.Rect(0, 0, self.rect.w, h)
        r.midbottom = self.rect.midbottom

        screen.fill((255, 0, 0), r)
        screen.blit(self.image, self.blit_pos)


class BatteryGfx(object):

    def __init__(self, rect, world):
        self.blit_pos = rect
        self.rect = pygame.Rect(rect.left + 24*2, rect.top + 18*2, 17*2, 31*2)
        self.world = world
        self.image = pygame.image.load("data/icon-power-dark.png")
        self.image = pygame.transform.scale2x(self.image).convert_alpha()

    def draw(self, screen):
        bat_cap = self.world.vc.battery.get_left_capacity()
        h = int(self.rect.h * bat_cap)
        r = pygame.Rect(0, 0, self.rect.w, h)
        r.midbottom = self.rect.midbottom

        red = int(255 - bat_cap * 255)
        green = int(217 * bat_cap)
        screen.fill((red, green, 14), r)

        screen.blit(self.image, self.blit_pos)


class BagGfx(object):

    def __init__(self, rect, world):
        self.blit_pos = rect
        self.rect = pygame.Rect(rect.left + 24 * 2, rect.top + 18 * 2, 17 * 2, 31 * 2)
        self.world = world
        self.image = pygame.image.load("data/icon-bug-dark.png")
        self.image = pygame.transform.scale2x(self.image).convert_alpha()
        self.tween_in_progress = False
        self.dx = 0
        self.dy = 0

    def draw(self, screen):
        bat_cap = 1.0 - self.world.vc.bag.get_left_capacity()
        if bat_cap >= 1.0 and self.tween_in_progress is False:
            self.tween_in_progress = True
            duration = 1.0
            displacement_range = (-3, 3)
            self.world.tweener.create_tween(self, "dx", 0, 10, duration, tweening.ease_random_int_bounce, params=displacement_range, cb_end=self._tween_end)
            self.world.tweener.create_tween(self, "dy", 0, 10, duration, tweening.ease_random_int_bounce, params=displacement_range)
        h = int(self.rect.h * bat_cap)
        r = pygame.Rect(self.dx, self.dy, self.rect.w, h)
        r.midbottom = self.rect.midbottom

        screen.fill((255, 138, 56), r)
        screen.blit(self.image, self.blit_pos.move(self.dx, self.dy))

    def _tween_end(self, *args, **kwargs):
        self.tween_in_progress = False
        self.dx = 0
        self.dy = 0


class Graphics(object):

    def __init__(self, scheduler, world):
        self._image_cache = {}  # {key: image}
        self.animations = {}  # {entity.id : EntityAnimation}
        self.scheduler = scheduler
        self.sprites = []
        self.world = world
        self.battery = BatteryGfx(pygame.Rect(settings.SCREEN_WIDTH - 8 - 64 * 2, -3, 20, 64), world)
        self.bag = BagGfx(pygame.Rect(settings.SCREEN_WIDTH - 8 - 64 * 2 - 32 * 2, -3, 20, 64), world)
        self.suction = SuctionGfx(pygame.Rect(settings.SCREEN_WIDTH - 28, 21, 28, 206), world)
        self.vc = None
        self.rx = 0
        self.ry = 0

    def on_create(self, entity):
        if entity.id not in self.animations:
            anchor = Vec2(0, 0)
            if entity.kind == settings.KIND_POTATOES:
                w, h = entity.aabb.size
                anchor = Vec2(w // 2, h // 2 - 55)
            elif entity.kind == settings.KIND_VC:
                anchor = -Vec2(143 - 170/2, 107 - 131/2)  # 143, 107
                self.vc = EntityAnimation(self, entity, 45, anchor)  # TODO: resolution depends on kind ...
                self.vc.update()
                return
            elif entity.kind == settings.KIND_BUG_SPIGOT:
                duration = 1.0
                displacement_range = (-3, 3)
                self.world.tweener.create_tween(self, 'rx', 0, 10, duration,  tweening.ease_random_int_bounce,
                                                params=displacement_range)
                self.world.tweener.create_tween(self, 'ry', 0, 10, duration, tweening.ease_random_int_bounce,
                                                params=displacement_range, cb_end=self.reset_r)
            # elif entity.kind & settings.KIND_BUGS:
            #     anchor.x = -(entity.size // 10 / 10.0 + 1) * 10
            #     anchor.y = (entity.size // 10 / 10.0 + 1) * 20

            animation = EntityAnimation(self, entity, 45, anchor)  # TODO: resolution depends on kind ...
            self.animations[entity.id] = animation
            animation.update()

    def reset_r(self, *args, **kwargs):
        self.rx = 0
        self.ry = 0

    def on_remove(self, entity):
        if entity.id in self.animations:
            del self.animations[entity.id]

        if entity.kind & settings.KIND_BUGS:
            self.suction.bug = 10

    def on_state_changed(self, entity, new_state):
        if entity.id in self.animations:
            self.animations[entity.id].update()

    def _sort_by_z(self, anim):
        return anim.entity.pos.z, anim.entity.pos.y + anim.entity.aabb.h

    def draw(self, screen, interpolation_factor):
        sprites = self.animations.values()
        for spr in [_s for _s in sprites if _s.entity.kind & settings.KIND_POTATOES]:
            spr.animation.current_index = 5 - int(spr.entity.get_percent_left() * 100 // 20)

        for spr in sorted(sprites, key=self._sort_by_z):
            if spr.key in self._image_cache:
                image = self._image_cache[spr.key]
            else:
                image = self.load_image(spr)

            anchor = spr.anchor.rotated(-spr.angle)
            rect = image.get_rect(center=(spr.entity.pos.x + anchor.x + self.rx, spr.entity.pos.y + anchor.y + self.ry))
            if settings.DEBUG_RENDER:
                pygame.draw.rect(screen, (0, 255, 0), rect, 2)
            screen.blit(image, rect)

        # draw vc
        spr = self.vc
        if spr.key in self._image_cache:
            image = self._image_cache[spr.key]
        else:
            image = self.load_image(spr)

        anchor = spr.anchor.clone()
        if spr.entity.pos.x < settings.SCREEN_WIDTH // 2:
            image = pygame.transform.flip(image, True, False)  # TODO: cache this image
            anchor.x = -anchor.x

        rect = image.get_rect(center=(spr.entity.pos.x + anchor.x, spr.entity.pos.y + anchor.y))
        screen.blit(image, rect)

        self.battery.draw(screen)
        self.suction.draw(screen)
        self.bag.draw(screen)

    def load_image(self, spr):
        kind, state, idx, angle, size = spr.key
        k = kind, state, idx, angle

        if k in self._image_cache:
            image = self._image_cache[k]
        else:
            image = self.load(kind, state, idx, spr.entity)
            self._image_cache[k] = image

        angle = 0 if kind == settings.KIND_ANT else angle
        if angle != 0 or size != 1:
            image = pygame.transform.rotozoom(image, angle, size).convert_alpha()
        self._image_cache[spr.key] = image
        logger.info("image cache count: {0}", len(self._image_cache))
        return image

    def get_anim(self, kind, state, entity):
        if kind == settings.KIND_ANT:
            if (kind, state, 0, 0) not in self._image_cache:
                # noinspection PyBroadException
                try:
                    id = pyknic.pygame.resource.resources.load(pyknic.pygame.resource.SPRITESHEET, "data/antlion/antlion_0.png.sdef")
                    sprites = pyknic.pygame.resource.resources.get_resource(id)
                    gs = sprites.get_grouped_by_action_and_facing()

                    # THIS directions MAPPING IS MESSED UP (IN THE SDEF ALREADY)!
                    directions = (('W', 180), ('NW', 135), ('N', 90), ('NE', 45), ('E', 0), ('SE', 315), ('S', 270), ('SW', 225))

                    walk_sprites = gs['walk']
                    for facing, angle in directions:
                        fs = walk_sprites[facing]
                        for idx, sp in enumerate(sorted(fs, key=lambda x: x.gid)):
                            self._image_cache[(kind, AntMoveState.__name__, idx, angle)] = sp.image
                            self._image_cache[(kind, AntStuckState.__name__, idx, angle)] = sp.image

                    stance_sprites = gs['stance']
                    for facing, angle in directions:
                        fs = stance_sprites[facing]
                        for idx, sp in enumerate(sorted(fs, key=lambda x: x.gid)):
                            self._image_cache[(kind, AntClingState.__name__, idx, angle)] = sp.image
                            self._image_cache[(kind, AntInitialState.__name__, idx, angle)] = sp.image

                    block_sprites = gs['block']
                    for facing, angle in directions:
                        fs = block_sprites[facing]
                        for idx, sp in enumerate(sorted(fs, key=lambda x: x.gid)):
                            self._image_cache[(kind, AntEatState.__name__, idx, angle)] = sp.image

                    attack_sprites = gs['hit']
                    for facing, angle in directions:
                        fs = attack_sprites[facing]
                        for idx, sp in enumerate(sorted(fs, key=lambda x: x.gid)):
                            self._image_cache[(kind, AntSuckedInState.__name__, idx, angle)] = sp.image

                    attack_sprites = gs['die']
                    for facing, angle in directions:
                        fs = [attack_sprites[facing][-1]]
                        for idx, sp in enumerate(sorted(fs, key=lambda x: x.gid)):
                            self._image_cache[(kind, AntCrushedState.__name__, idx, angle)] = sp.image

                except Exception as ex:
                    logger.error(ex)

            if state == AntMoveState.__name__:
                return FrameAnimation(self.scheduler, 0, 8, fps=8 * entity.speed/24.0)  # 24px @ 8fps
            elif state == AntClingState.__name__:
                return FrameAnimation(self.scheduler, 0, 4, fps=30)
            elif state == AntEatState.__name__:
                return FrameAnimation(self.scheduler, 0, 2, fps=6)
            elif state == AntStuckState.__name__:
                return FrameAnimation(self.scheduler, 0, 8, fps=40)
            elif state == AntSuckedInState.__name__:
                return FrameAnimation(self.scheduler, 4, 2, fps=20)
            elif state == AntInitialState.__name__:
                return FrameAnimation(self.scheduler, 0, 4, fps=30)
                # return FrameAnimation(self.scheduler, 0, 1, fps=0)
            elif state == AntCrushedState.__name__:
                return FrameAnimation(self.scheduler, 0, 1, fps=0)
        elif kind == settings.KIND_POTATOES:
            return FrameAnimation(self.scheduler, 0, 6, fps=0)
        elif kind == settings.KIND_VC:
            return FrameAnimation(self.scheduler, 0, 1, fps=0)
        elif kind == settings.KIND_BUG_SPIGOT:
            return FrameAnimation(self.scheduler, 0, 1, fps=0)

        return FrameAnimation(self.scheduler, 0, 10, fps=20)

    def load(self, kind, state, animation_index, entity, angle=0):
        self.get_anim(kind, state, entity)

        if kind == settings.KIND_POTATOES:
            idx = animation_index
            name = "data/tower{0}.png".format(idx)
        elif kind == settings.KIND_VC:
            img = GameResource.get(GameResource.IMG_VACUUM)
            return img
        elif kind == settings.KIND_BUG_SPIGOT:
            name = "data/crack.png"
            img = pygame.image.load(name).convert()
            img.set_colorkey(img.get_at((0, 0)), pygame.RLEACCEL)
            return img
        else:
            name = "{0}_{1}_{2}".format(kind, state, animation_index)

        img_id = pyknic.pygame.resource.resources.load(pyknic.pygame.resource.IMAGE, name, scale=1.0, angle=angle, width=100, height=50, color=(255, 0, 0, 128))
        img = pyknic.pygame.resource.resources.get_resource(img_id)
        return img

