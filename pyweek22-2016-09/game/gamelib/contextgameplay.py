# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek22-2016-09
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

import pygame

from . import pyknic

from . import eventmapper
from . import level
from . import messagetypes
from . import ptext
from . import settings
from . import sound
from .binspace import BinSpace
from .collisiondetector import coarse_detection, get_col_func, register_collision_func
from .entityregistry import entity_registry
from .entityvacuumcleaner import VacuumCleaner, VcStateDepleted
from .graphics import Graphics
from .pyknic import tweening
from .pyknic.mathematics import Vec2
from .pyknic.timing import Scheduler, Timer
from .pyknic.tweening import Tweener
from .settings import ACTION_QUIT, ACTION_RESUME, ACTION_PAUSE, ACTION_ACTIVATE_PRIMARY, ACTION_SKIP_SONG, ACTION_SKIP_LEVEL

logger = logging.getLogger(__name__)

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2016"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 


class RetryContext(pyknic.context.Context):

    def __init__(self, current_level):
        self.current_level = current_level
        self.title_text = ptext.getsurf("Your Chips have been eaten!", 'BIOST', 84, **settings.HILIGHT_FONT_COLOR).convert_alpha()
        self.title_rect = self.title_text.get_rect(center=(settings.SCREEN_WIDTH // 2, settings.SCREEN_HEIGHT * 1 // 6))
        self.retry_text = ptext.getsurf("Retry (any key)", 'BIOST', 42, **settings.HILIGHT_FONT_COLOR).convert_alpha()
        self.retry_rect = self.retry_text.get_rect(center=(settings.SCREEN_WIDTH // 2, settings.SCREEN_HEIGHT * 3 // 6))
        self.quit_text = ptext.getsurf("Quit! (esc)", 'BIOST', 42, **settings.HILIGHT_FONT_COLOR).convert_alpha()
        self.quit_rect = self.quit_text.get_rect(center=(settings.SCREEN_WIDTH // 2, settings.SCREEN_HEIGHT * 5 // 6))

    def update(self, dt):
        for event in [pygame.event.wait()]:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self.quit_game()
                    return
                else:
                    self.retry_game()
                    return
            elif event.type == pygame.MOUSEBUTTONDOWN:
                pos = event.pos
                if self.retry_rect.collidepoint(pos):
                    self.retry_game()
                    return
                if self.quit_rect.collidepoint(pos):
                    self.quit_game()
                    return

    def retry_game(self):
        self.pop(self.get_stack_length() - 1)
        self.push(GamePlayContext(self.current_level))

    def quit_game(self):
        self.pop(self.get_stack_length() - 1)

    def draw(self, screen, do_flip=True, interpolation_factor=1.0):
        screen.blit(self.title_text, self.title_rect)
        screen.blit(self.retry_text, self.retry_rect)
        screen.blit(self.quit_text, self.quit_rect)
        pygame.display.flip()


class PauseContext(pyknic.context.Context):

    def __init__(self):
        self.text = ptext.getsurf("Paused!", 'BIOST', 84, **settings.HILIGHT_FONT_COLOR).convert_alpha()
        self.rect = self.text.get_rect(center=(settings.SCREEN_WIDTH // 2, settings.SCREEN_HEIGHT // 2))

    def enter(self):
        pygame.event.clear()

    def update(self, dt):
        events = [pygame.event.wait()]
        # events = pygame.event.get()
        for action, extra in eventmapper.get_actions(settings.event_map, events):
            if action == ACTION_RESUME or action == ACTION_ACTIVATE_PRIMARY:
                self.pop()
                return
            elif action == ACTION_QUIT:
                self.pop(self.get_stack_length() - 1)
                return

    def draw(self, screen, do_flip=True, interpolation_factor=1.0):
        screen.blit(self.text, self.rect)
        pygame.display.flip()


class _Sprite(object):

    def __init__(self, image, x, y):
        self.image = image
        self.rect = image.get_rect(center=(x, y))


def col_vc_ant(ant, vc):
    # print("vc VS ant " + str(ant.id))
    distance = Vec2(ant.pos.x, ant.pos.y).get_distance(Vec2(vc.pos.x, vc.pos.y))
    if distance < settings.BUG_MIN_DISTANCE:
        # to near, prevent 0 division!
        distance = settings.BUG_MIN_DISTANCE
        if vc.sm.current_state == VcStateDepleted:
            ant.handle_message(messagetypes.CRUSH)
            return

    drag_force = 1.0 / distance * vc.get_strength()

    # shortcut
    if drag_force < ant.cling_force:
        ant.handle_message(messagetypes.MOVE_NORMAL)
        return

    if drag_force >= ant.suck_force:
        # get sucked in
        vc.handle_message(messagetypes.SUCKED_IN, ant)
        return

    # cling to ground
    ant.handle_message(messagetypes.CLING_TO_GROUND)


class GamePlayContext(pyknic.context.Context):

    def __init__(self, starting_level):
        self.current_level = None
        self.vc = None
        self.bin_space = BinSpace(settings.BIN_SPACE_TILE_WIDTH, settings.BIN_SPACE_TILE_HEIGHT)
        self.scheduler = Scheduler()
        self.graphics = Graphics(self.scheduler, self)
        self.tweener = Tweener(logger_instance=False)
        self.sprites = []

        # to receive messages
        self.id = settings.WORLD_ID
        self.kind = settings.KIND_WORLD

        # tool selection
        self.current_tool = None
        self.starting_level = starting_level
        self.current_level = level.Level(self.add_entity)
        # self.level_started = 0

    def enter(self):
        self._reset_internals()

        entity_registry.register(self)

        self.vc = VacuumCleaner(pygame.Rect(0, 0, 200, 200))
        self.vc.pos.z = 10
        entity_registry.register(self.vc)
        self.vc.sm.event_switched_state.add(self.entity_state_changed)
        self.graphics.on_create(self.vc)

        self.current_tool = self.vc

        register_collision_func((settings.KIND_ANT, settings.KIND_VC), col_vc_ant)
        # register_collision_func((settings.KIND_ROACH, settings.KIND_VC), col_vc_roach)

        self.fps_timer = Timer(1.0 / 2.0, repeat=True, name="gp fps timer")
        self.fps_timer.event_elapsed.add(self._fps_elapsed)
        self.fps_timer.start()

        self.end_condition_timer = Timer(0.5, True, name="end condition timer")
        self.end_condition_timer.event_elapsed.add(self._check_end_condition)
        self.end_condition_timer.start()

        self.scheduler.schedule(self._check_bugs_at_potatoes, settings.CHECK_BUGS_AT_POTATOES_INTERVAL)
        pygame.mouse.set_visible(False)
        self.vc.pos.x = settings.SCREEN_WIDTH // 2
        self.vc.pos.y = settings.SCREEN_HEIGHT // 2
        pygame.mouse.set_pos(self.vc.pos.x, self.vc.pos.y)
        pygame.event.clear()

        self.new_level(self.starting_level)

    def resume(self):
        pygame.event.clear()
        pygame.mouse.set_visible(False)
        self.end_condition_timer.start()

    def suspend(self):
        pygame.mouse.set_visible(True)
        self.end_condition_timer.stop()

    def _reset_internals(self):
        self.scheduler.clear()
        self.bin_space = BinSpace(settings.BIN_SPACE_TILE_WIDTH, settings.BIN_SPACE_TILE_HEIGHT)
        entity_registry.clear()
        Timer.scheduler.clear()
        self.tweener.clear()

    def _check_bugs_at_potatoes(self, *args, **kwargs):
        for potatoes in entity_registry.get_by_kind_iter(settings.KIND_POTATOES):
            potential_col_entities = coarse_detection(self.bin_space, potatoes.aabb, settings.KIND_BUGS)
            for bug in potential_col_entities:
                if potatoes.aabb.collidepoint(bug.pos.x, bug.pos.y):
                    bug.handle_message(messagetypes.EAT, potatoes.id)

        return settings.CHECK_BUGS_AT_POTATOES_INTERVAL

    def exit(self):
        self.fps_timer.stop()
        self._reset_internals()
        pygame.mouse.set_visible(True)

    def _fps_elapsed(self, timer):
        current_level = self.current_level.level_number
        energy = self.vc.battery.energy
        bag_amount = self.vc.bag.amount
        fps = settings.clock.get_fps()
        caption = (settings.CAPTION + ' - N:{} L:{} E:{} B:{} Fps:{}').format(
            self.current_level.name, current_level, int(round(energy)), int(round(bag_amount)), round(fps, 1))
        pygame.display.set_caption(caption)

    def new_level(self, level_number):
        # TODO: bug: this is hack to catch rapid-fire calls that cause levels to be skipped
        # t = time.time()
        # if t - self.level_started < 5.0:
        #     print('EXTRA LEVEL UP CAUGHT')
        #     print('EXTRA LEVEL UP CAUGHT')
        #     print('EXTRA LEVEL UP CAUGHT')
        #     print('EXTRA LEVEL UP CAUGHT')
        #     # sys.stdout.flush()
        #     # return

        if level_number < len(level.level_data):
            logger.info("SETTING NEW LEVEL: " + str(level_number))
            # self.level_started = t
            # print('NEW LEVEL: {} {}'.format(level_number, time.time()))
            # sys.stdout.flush()
            self.current_level.reset(level_number)
            # print('New level: {}'.format(self.current_level.level_number))
            self.vc.battery.recharge()
            # self.end_condition_timer.start()
            text = "Level {0} - {1}".format(self.current_level.level_number, self.current_level.name)
            self._show_text(text, 4.5 if self.current_level.level_number < 13 else 10)
            logger.info("new level set: '{0}'".format(text))
            # TODO: clear out old bug carcasses
        else:
            logger.info("no levels left...")
            logger.info("YOU WIN   YOU WIN   YOU WIN   YOU WIN   YOU WIN   YOU WIN   YOU WIN")
            self.pop()

    def _show_text(self, text, duration):
        surf = ptext.getsurf(text, 'BIOST', 50, **settings.HILIGHT_FONT_COLOR).convert_alpha()
        spr = _Sprite(surf, settings.SCREEN_WIDTH // 2, settings.SCREEN_HEIGHT // 2)
        self.tweener.create_tween(spr.rect, "centery", spr.rect.centery, -settings.SCREEN_HEIGHT // 2 - 100, duration, tweening.ease_in_expo, cb_end=self._remove_spr)
        self.sprites.append(spr)

    def _remove_spr(self, tween):
        self.sprites.remove(tween.o)

    def update(self, dt):
        self.handle_events()
        sound.play_action_song()
        Timer.scheduler.update(dt)
        self.scheduler.update(dt)
        self.bin_space.update()
        self.tweener.update(dt)

        potential_col_entities = coarse_detection(self.bin_space, self.current_tool.aabb, settings.KIND_BUGS)
        self.detect_collision(self.current_tool, potential_col_entities)

        for ent in entity_registry.get_by_kind_iter(settings.KIND_BUGS):
            ent.update(dt)

        self.vc.update(dt)

    def _check_end_condition(self, timer):
        if self.is_level_won():
            # self.end_condition_timer.stop()
            self.new_level(self.current_level.level_number + 1)
        elif self.is_level_lost():
            self.end_condition_timer.stop()
            self.push(RetryContext(self.current_level.level_number))

    def add_entity(self, entity):
        entity_registry.register(entity)
        self.bin_space.add(entity)
        self.graphics.on_create(entity)
        entity.sm.event_switched_state.add(self.entity_state_changed)

    def remove_entity(self, entity):
        entity_registry.remove(entity)
        self.bin_space.remove(entity)
        self.graphics.on_remove(entity)

    def detect_collision(self, collision_requester, potential_col_entities):
        for pot_ent in potential_col_entities:
            if collision_requester.kind < pot_ent.kind:
                func = get_col_func((collision_requester.kind, pot_ent.kind))
                func(collision_requester, pot_ent)
            else:
                func = get_col_func((pot_ent.kind, collision_requester.kind))
                func(pot_ent, collision_requester)

    def is_level_won(self):
        if [_e for _e in entity_registry.get_by_kind(settings.KIND_BUGS) if _e.is_alive()]:
            return False
        for bug_spigot in self.current_level.bug_spigots:
            if not bug_spigot.is_empty():
                return False
        return True

    def is_level_lost(self):
        health = 0
        for p in entity_registry.get_by_kind_iter(settings.KIND_POTATOES):
            health += p.health

        if health <= 0:
            return True
        return False

    def handle_events(self):
        for action, extra in eventmapper.get_actions(settings.event_map, pygame.event.get()):
            if action == ACTION_QUIT:
                self.pop()
            elif action == ACTION_PAUSE:
                self.push(PauseContext())
            elif action == ACTION_SKIP_SONG:
                if sound.Song.now_playing and pygame.mixer.music.get_busy():
                    pygame.mixer.music.fadeout(1000)
            elif action == ACTION_SKIP_LEVEL:
                # print("## skipping level!", self.current_level.level_number)
                # self.new_level(self.current_level.level_number + 1)
                self.exchange(GamePlayContext(self.current_level.level_number + 1))
            else:
                # TODO: only selected one should handle action...
                self.current_tool.handle_action(action, extra)

    def handle_message(self, message):
        if message.message_id == messagetypes.SUCKED_IN:
            sender = entity_registry.get_by_id(message.sender_id)
            self.remove_entity(sender)

    def entity_state_changed(self, entity, new_state):
        self.graphics.on_state_changed(entity, new_state)

    def draw(self, screen, do_flip=True, interpolation_factor=1.0):
        """
        Refresh the screen

        :param screen: the screen surface
        :param do_flip: if set to true, the it should flip the screen, otherwise not. Default: True
        :param interpolation_factor: interpolation factor for drawing between sim updates, default: 1.0
        """
        screen.fill((200, 200, 200))
        self.graphics.draw(screen, interpolation_factor)

        if settings.DEBUG_RENDER:
            for bug in entity_registry.get_by_kind_iter(settings.KIND_BUGS):
                pygame.draw.line(screen, (0, 0, 255), (bug.pos.x, bug.pos.y), (bug.pos.x + bug.direction.x * 20, bug.pos.y + bug.direction.y * 20))
                r = bug.aabb.copy()
                r.center = bug.pos.as_xy_tuple()
                pygame.draw.rect(screen, (255, 255, 0), r, 2)

            pygame.draw.rect(screen, (255, 255, 0), self.current_tool.aabb, 2)
            for potatoes in entity_registry.get_by_kind_iter(settings.KIND_POTATOES):
                pygame.draw.rect(screen, (255, 255, 0), potatoes.aabb, 2)

        for spr in self.sprites:
            screen.blit(spr.image, spr.rect)

        if do_flip:
            pygame.display.flip()
