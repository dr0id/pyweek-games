import pygame

from . import bugspigot
from . import settings
from . import sound
from .entitypotato import Potatoes
from .entityregistry import entity_registry
from .pyknic.timing import Timer
from .pyknic.mathematics import Vec3 as Vec
from . import levelrank1
from . import levelrank2
from . import levelrank3
from . import levelrank4


logic = """
Game loads level 1.
Level 1 creates spigots.
Spigot callbacks spawn bugs on scheduler.
When all the spigots are empty, the level is won.

To do:
Need bag of chips.
"""


class Level(object):
    def __init__(self, register):
        """construct a Level from data

        :param level_number: int; any of level_data.keys()
        :param register: function; takes a bug to register (e.g. add to containers)
        """
        self._cached_spigot_positions = []
        self.level_number = None
        self.name = 'No name'
        self.level_data = None
        self.bug_spigots = []
        self.timers = {}
        self.register = register
        self.is_new_level = True

    def reset(self, level_number):
        settings.current_level = self
        self.level_number = level_number
        self.level_data = level_data[level_number]
        
        graphics = entity_registry.get_by_id(settings.WORLD_ID).graphics
        # clear old data
        for bs in self.bug_spigots:
            graphics.on_remove(bs)
        del self.bug_spigots[:]

        stale = []
        for p in entity_registry.get_by_kind_iter(settings.KIND_POTATOES):
            stale.append(p)
        for p in stale:
            entity_registry.remove(p)
            graphics.on_remove(p)

        for t in self.timers:
            t.stop()
        self.timers.clear()
        self._cached_spigot_positions = []

        # insert new level data
        screen_rect = pygame.display.get_surface().get_rect()
        self.name = self.level_data['name']
        for args in self.level_data['bug_spigots']:
            config = args['config']
            count = args['count']
            interval = args['interval']
            pos = args['pos']
            if isinstance(pos, str):
                pos = getattr(screen_rect, pos)
            pos = Vec(pos[0], pos[1], -10)
            delay = args['delay']
            sfx = args.get('rumble', False)
            timer = Timer(delay, repeat=False, name="bugspigot creation")
            timer.event_elapsed.add(self.make_spigot)
            timer.start()
            self.timers[timer] = config, count, interval, pos, self.register, graphics, sfx

        for level_potato in self.level_data['potatoes']:
            pos = level_potato['pos']
            potatoes = Potatoes(Vec(pos[0], pos[1]), level_potato['amount'], level_potato['w'], level_potato['h'])
            entity_registry.register(potatoes)
            graphics.on_create(potatoes)

        self.is_new_level = True

    def make_spigot(self, timer):
        config, count, interval, pos, register, graphics, sfx = self.timers[timer]
        bug_spigot = bugspigot.BugSpigot(config, count, interval, pos, register)
        self.bug_spigots.append(bug_spigot)

        pos_as_tuple = bug_spigot.pos.as_xy_tuple()
        if pos_as_tuple not in self._cached_spigot_positions:
            self._cached_spigot_positions.append(pos_as_tuple)
            graphics.on_create(bug_spigot)

        del self.timers[timer]

        if self.is_new_level or sfx:
            sound.earthquake()
        self.is_new_level = False

    def update(self, *args):
        pass


level_data = \
    levelrank1.level_data + \
    levelrank2.level_data + \
    levelrank3.level_data + \
    levelrank4.level_data
