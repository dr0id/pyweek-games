# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek21-2016-02
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function
import os
import random

import pygame

# import ptext
from .pyknic.generators import FlagGenerator, IdGenerator

__version_number__ = (0, 0, 0, 0)
__version__ = ".".join([str(num) for num in __version_number__])

__author__ = 'dr0iddr0id {at} gmail [dot] com'
__copyright__ = "DR0ID @ 2016"
__credits__ = ["DR0ID", "Gummbum"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

DOMAIN_NAME = "pyweek22-DRummb0ID"  # used for translation files

###############################################################################
#
# for tuning
#
###############################################################################

# Some people have trouble with numpy. It will fail gracefully if not found.
use_numpy = False

# Game play data
# TODO: Load saved game; update game play data
# Set starting_level to the INDEX of level.level_data to load at game start
starting_level = 0
current_level = starting_level
skip_to = (None, 'game', 'credits')[0]

# touch game/nomusic.txt to disable music locally without distributing the setting
PLAY_MUSIC = not os.access('nomusic.txt', os.F_OK)
MUSIC_VOLUME = 0.05
SFX_VOLUME = 1.0

# Note: graphics are not scaled, so you probably don't want to change these.
SCREEN_WIDTH = 1024
SCREEN_HEIGHT = 768

PRINT_FPS = True
DEBUG_RENDER = False

CHEATS_ENABLED = os.access('cheats_enabled.txt', os.F_OK)

###############################################################################
#
# end tuning
#
###############################################################################


clock = None
SCREEN_SIZE = (SCREEN_WIDTH, SCREEN_HEIGHT)
CAPTION = "You cANT let him in here"
FLAGS = pygame.DOUBLEBUF
BIT_DEPTH = 32

# if FLAGS & pygame.FULLSCREEN:
#     SCREEN_HEIGHT = 768

numpy = None
if use_numpy:
    try:
        import numpy
    except ImportError:
        pass

if numpy:
    HILIGHT_FONT_COLOR = dict(color='gold', scolor='black', shadow=(1, 1), owidth=1, gcolor='tomato')
    LOLIGHT_FONT_COLOR = dict(color='tomato', scolor='black', shadow=(1, 1), owidth=1, gcolor='gold')
    # Not sure which one I like better! - Gumm
    SPLASH_FONT_COLOR = dict(color='mediumslateblue', scolor='slateblue4', gcolor='slateblue4', shadow=(1, 1))
    # SPLASH_FONT_COLOR = dict(color='mediumslateblue', ocolor='slateblue4', gcolor='white', owidth=1)
else:
    HILIGHT_FONT_COLOR = dict(color='gold', scolor='black', shadow=(1, 1), owidth=1)
    LOLIGHT_FONT_COLOR = dict(color='tomato', scolor='black', shadow=(1, 1), owidth=1)
#     # Not sure which one I like better! - Gumm
#     SPLASH_FONT_COLOR = dict(color='mediumslateblue', scolor='slateblue4', shadow=(1, 1))
#     # SPLASH_FONT_COLOR = dict(color='mediumslateblue', ocolor='slateblue4', owidth=1)
# SAY_FONT_COLOR = dict(color='white')
# BLACKBOARD_TITLE_FONT_COLOR = dict(color='white', scolor='mediumslateblue', shadow=(1, 1), owidth=1)
# SAY_CHOICES_FONT_COLOR = dict(color='palevioletred', owidth=0.25, ocolor='palevioletred')

MIXER_FREQUENCY = 0
MIXER_BUFFER_SIZE = 128
# Reserved channels (see main.py)
MIXER_BAG_ALARM_CHANNEL = 1
MIXER_RESERVED_CHANNELS = 2  # bug alarm=0; earthquake=1

VOLUME_CHANGE = 0.005

SIM_TIME_STEP = 0.02  # 50 fps


# increase this better rotation caching; but clearing the cache makes the game hiccup;
# lower it if the game periodically freezes for a split second for no apparent reason
# SPRITE_MAX_CACHE_IMAGE_COUNT = 10000
SPRITE_CACHE_MAX_MEMORY = 1.5 * 2 ** 30
SPRITE_CACHE_DEFAULT_EXPIRATION = 15.0
SPRITE_CACHE_AT_MOST = 50
SPRITE_CACHE_AUTO_TUNE_AGING = False
SPRITE_CACHE_TUNE_MIN_PERCENT = 94.0
SPRITE_CACHE_TUNE_MAX_PERCENT = 95.0
SPRITE_CACHE_TUNE_STEP = 0.01

UUID_FOR_RANDOM = '1e5c96a9-7bb9-49f1-8c4c-2c9ee9e63f0f'
random.seed(UUID_FOR_RANDOM)  # make random repeatable

# Resources
FILENAME_GAME_PROGRESS = 'progress.json'

# Keys

if CHEATS_ENABLED:
    KEY_CHEAT_EXIT = pygame.K_F10
    KEY_CHEAT_COMBAT_TEST = pygame.K_F9
    KEY_CHEAT_LEVEL_SELECTION = pygame.K_F8
    KEY_CHEAT_DIALOGUE_SELECTION = pygame.K_1, pygame.K_2, pygame.K_3, pygame.K_4, pygame.K_5
else:
    KEY_CHEAT_EXIT = ''
    KEY_CHEAT_COMBAT_TEST = ''
    KEY_CHEAT_LEVEL_SELECTION = ''
    KEY_CHEAT_DIALOGUE_SELECTION = ['']
KEY_SKIP_DIALOGUE = pygame.K_SPACE, pygame.K_RETURN
KEY_MOVE_NEXT = pygame.K_SPACE, pygame.K_RETURN

WORLD_ID = 0

_kind_generator = FlagGenerator()
KIND_ANT = _kind_generator.next()
KIND_ROACH = _kind_generator.next()
KIND_SPIDER = _kind_generator.next()
KIND_FLY = _kind_generator.next()
KIND_BUGS = KIND_ANT | KIND_ROACH | KIND_SPIDER | KIND_FLY

KIND_POTATOES = _kind_generator.next()
KIND_WORLD = _kind_generator.next()
KIND_BUG_SPIGOT = _kind_generator.next()
KIND7 = _kind_generator.next()
KIND_VC = _kind_generator.next()

_action_generator = IdGenerator(0)
ACTION_QUIT = _action_generator.next()
ACTION_MOUSE_MOTION = _action_generator.next()
ACTION_ACTIVATE_PRIMARY = _action_generator.next()
ACTION_DEACTIVATE_PRIMARY = _action_generator.next()
ACTION_EXCHANGE = _action_generator.next()
ACTION_ACTIVATE_SECONDARY = _action_generator.next()
ACTION_DEACTIVATE_SECONDARY = _action_generator.next()
ACTION_RESUME = _action_generator.next()
ACTION_PAUSE = _action_generator.next()
ACTION_SKIP_SONG = _action_generator.next()
ACTION_SKIP_LEVEL = _action_generator.next()
ACTION_NO_ACTION = _action_generator.next()

NO_MOD = 0
event_map = {
    pygame.QUIT: {None: ACTION_QUIT},
    pygame.KEYDOWN: {
        (pygame.K_ESCAPE, NO_MOD): ACTION_QUIT,
        (pygame.K_F4, pygame.KMOD_ALT): ACTION_QUIT,
        (pygame.K_s, NO_MOD): ACTION_SKIP_SONG,
        (pygame.K_F8, NO_MOD): ACTION_SKIP_LEVEL,
        (None, NO_MOD): ACTION_NO_ACTION,
                     },
    pygame.MOUSEMOTION: {None: ACTION_MOUSE_MOTION},
    pygame.MOUSEBUTTONDOWN: {1: ACTION_ACTIVATE_PRIMARY, 3: ACTION_ACTIVATE_SECONDARY},
    pygame.MOUSEBUTTONUP: {1: ACTION_DEACTIVATE_PRIMARY, 3: ACTION_DEACTIVATE_SECONDARY},
    pygame.ACTIVEEVENT: {
        (1, 0): ACTION_PAUSE,  # mouse left win
        (1, 1): ACTION_RESUME  # mouse entered win
    }
}


BIN_SPACE_TILE_WIDTH = 50  # tile size of the bin space tiles
BIN_SPACE_TILE_HEIGHT = 50  # tile size of the bin space tiles


VC_ENERGY_DRAIN_INTERVAL = 0.5  # seconds after this interval an amount of energy is drained from the battery
VC_NORMAL_ENERGY_DRAIN = 10  # normal energy drain from the batter
VC_BAG_FULL_ENERGY_DRAIN = 15  # how much energy is drained from the batter when the bag is full
VC_STUCK_ENERGY_DRAIN = 20  # how much energy is drained from the batter while something is stuck
VC_CHANGE_BAG_DURATION = 1.0  # seconds, how long it takes to change the bag
VC_STRENGTH = 800  # suction strength
VC_OVERHEAT_INTERVAL = 0.500  # duration, every such interval the heat has increased by VC_HEAT_INCREASE
VC_HEAT_INCREASE = 5  # how much heat is generated in the last VC_OVERHEAT_INTERVAL
VC_OVERHEAT_LIMIT = 100  # heat limit when it will overheat!
VC_OVERHEAT_COOL_DOWN_DURATION = 0.01  # seconds, how long it takes to cool the vc down.. making the reload *VERY* fast
VC_NEW_BATTERY_ENERGY = 1000  # the energy capacity of the battery
VC_BAG_CAPACITY = 1000  # how many (actually the size of the bugs is summed) can be sucked in before the bag is full
VC_MAX_SUCK_SIZE = 20  # its like the diameter of the nozzle: smaller goes through, bigger will get stuck!
VC_BUG_SUCK_IN_DURATION = 0.1  # how long it takes to be sucked in, this is for the suck in animation

BUG_MIN_DISTANCE = 8
CHECK_BUGS_AT_POTATOES_INTERVAL = 1.0  # interval where a check is done which bugs should be eating potatoes!

key_to_text = {
    pygame.K_BACKSPACE: ("\\b", "backspace"),
    pygame.K_TAB: ("\\t", "tab"),
    pygame.K_CLEAR: ("", "clear"),
    pygame.K_RETURN: ("\\r", "return"),
    pygame.K_PAUSE: ("", "pause"),
    pygame.K_ESCAPE: ("^[", "escape"),
    pygame.K_SPACE: ("", "space"),
    pygame.K_EXCLAIM: ("!", "exclaim"),
    pygame.K_QUOTEDBL: ("\"", "quotedbl"),
    pygame.K_HASH: ("#", "hash"),
    pygame.K_DOLLAR: ("$", "dollar"),
    pygame.K_AMPERSAND: ("&", "ampersand"),
    pygame.K_QUOTE: ("", "quote"),
    pygame.K_LEFTPAREN: ("(", "left parenthesis"),
    pygame.K_RIGHTPAREN: ("),", "right parenthesis"),
    pygame.K_ASTERISK: ("*", "asterisk"),
    pygame.K_PLUS: ("+", "plus sign"),
    pygame.K_COMMA: (",", "comma"),
    pygame.K_MINUS: ("-", "minus sign"),
    pygame.K_PERIOD: (".", "period"),
    pygame.K_SLASH: ("/", "forward slash"),
    pygame.K_0: ("0", "0"),
    pygame.K_1: ("1", "1"),
    pygame.K_2: ("2", "2"),
    pygame.K_3: ("3", "3"),
    pygame.K_4: ("4", "4"),
    pygame.K_5: ("5", "5"),
    pygame.K_6: ("6", "6"),
    pygame.K_7: ("7", "7"),
    pygame.K_8: ("8", "8"),
    pygame.K_9: ("9", "9"),
    pygame.K_COLON: (":", "colon"),
    pygame.K_SEMICOLON: (";", "semicolon"),
    pygame.K_LESS: ("<", "less-than sign"),
    pygame.K_EQUALS: ("=", "equals sign"),
    pygame.K_GREATER: (">", "greater-than sign"),
    pygame.K_QUESTION: ("?", "question mark"),
    pygame.K_AT: ("@", "at"),
    pygame.K_LEFTBRACKET: ("[", "left bracket"),
    pygame.K_BACKSLASH: ("\\", "backslash"),
    pygame.K_RIGHTBRACKET: ("]", "right bracket"),
    pygame.K_CARET: ("^", "caret"),
    pygame.K_UNDERSCORE: ("_", "underscore"),
    pygame.K_BACKQUOTE: ("`", "grave"),
    pygame.K_a: ("a", "a"),
    pygame.K_b: ("b", "b"),
    pygame.K_c: ("c", "c"),
    pygame.K_d: ("d", "d"),
    pygame.K_e: ("e", "e"),
    pygame.K_f: ("f", "f"),
    pygame.K_g: ("g", "g"),
    pygame.K_h: ("h", "h"),
    pygame.K_i: ("i", "i"),
    pygame.K_j: ("j", "j"),
    pygame.K_k: ("k", "k"),
    pygame.K_l: ("l", "l"),
    pygame.K_m: ("m", "m"),
    pygame.K_n: ("n", "n"),
    pygame.K_o: ("o", "o"),
    pygame.K_p: ("p", "p"),
    pygame.K_q: ("q", "q"),
    pygame.K_r: ("r", "r"),
    pygame.K_s: ("s", "s"),
    pygame.K_t: ("t", "t"),
    pygame.K_u: ("u", "u"),
    pygame.K_v: ("v", "v"),
    pygame.K_w: ("w", "w"),
    pygame.K_x: ("x", "x"),
    pygame.K_y: ("y", "y"),
    pygame.K_z: ("z", "z"),
    pygame.K_DELETE: ("", "delete"),
    pygame.K_KP0: ("", "keypad 0"),
    pygame.K_KP1: ("", "keypad 1"),
    pygame.K_KP2: ("", "keypad 2"),
    pygame.K_KP3: ("", "keypad 3"),
    pygame.K_KP4: ("", "keypad 4"),
    pygame.K_KP5: ("", "keypad 5"),
    pygame.K_KP6: ("", "keypad 6"),
    pygame.K_KP7: ("", "keypad 7"),
    pygame.K_KP8: ("", "keypad 8"),
    pygame.K_KP9: ("", "keypad 9"),
    pygame.K_KP_PERIOD: (".", "keypad period"),
    pygame.K_KP_DIVIDE: ("/", "keypad divide"),
    pygame.K_KP_MULTIPLY: ("*", "keypad multiply"),
    pygame.K_KP_MINUS: ("-", "keypad minus"),
    pygame.K_KP_PLUS: ("+", "keypad plus"),
    pygame.K_KP_ENTER: ("\\r", "keypad enter"),
    pygame.K_KP_EQUALS: ("=", "keypad equals"),
    pygame.K_UP: ("", "up arrow"),
    pygame.K_DOWN: ("", "down arrow"),
    pygame.K_RIGHT: ("", "right arrow"),
    pygame.K_LEFT: ("", "left arrow"),
    pygame.K_INSERT: ("", "insert"),
    pygame.K_HOME: ("", "home"),
    pygame.K_END: ("", "end"),
    pygame.K_PAGEUP: ("", "page up"),
    pygame.K_PAGEDOWN: ("", "page down"),
    pygame.K_F1: ("", "F1"),
    pygame.K_F2: ("", "F2"),
    pygame.K_F3: ("", "F3"),
    pygame.K_F4: ("", "F4"),
    pygame.K_F5: ("", "F5"),
    pygame.K_F6: ("", "F6"),
    pygame.K_F7: ("", "F7"),
    pygame.K_F8: ("", "F8"),
    pygame.K_F9: ("", "F9"),
    pygame.K_F10: ("", "F10"),
    pygame.K_F11: ("", "F11"),
    pygame.K_F12: ("", "F12"),
    pygame.K_F13: ("", "F13"),
    pygame.K_F14: ("", "F14"),
    pygame.K_F15: ("", "F15"),
    pygame.K_NUMLOCK: ("", "numlock"),
    pygame.K_CAPSLOCK: ("", "capslock"),
    pygame.K_SCROLLOCK: ("", "scrollock"),
    pygame.K_RSHIFT: ("", "right shift"),
    pygame.K_LSHIFT: ("", "left shift"),
    pygame.K_RCTRL: ("", "right ctrl"),
    pygame.K_LCTRL: ("", "left ctrl"),
    pygame.K_RALT: ("", "right alt"),
    pygame.K_LALT: ("", "left alt"),
    pygame.K_RMETA: ("", "right meta"),
    pygame.K_LMETA: ("", "left meta"),
    pygame.K_LSUPER: ("", "left windows key"),
    pygame.K_RSUPER: ("", "right windows key"),
    pygame.K_MODE: ("", "mode shift"),
    pygame.K_HELP: ("", "help"),
    pygame.K_PRINT: ("", "print screen"),
    pygame.K_SYSREQ: ("", "sysrq"),
    pygame.K_BREAK: ("", "break"),
    pygame.K_MENU: ("", "menu"),
    pygame.K_POWER: ("", "power"),
    pygame.K_EURO: ("", "euro"),
}

mod_key_to_text = {
    pygame.KMOD_NONE: "",
    pygame.KMOD_LSHIFT: "left shift",
    pygame.KMOD_RSHIFT: "right shift",
    pygame.KMOD_SHIFT: "shift",
    pygame.KMOD_CAPS: "capslock",
    pygame.KMOD_LCTRL: "left ctrl",
    pygame.KMOD_RCTRL: "right ctrl",
    pygame.KMOD_CTRL: "ctrl",
    pygame.KMOD_LALT: "left alt",
    pygame.KMOD_RALT: "right alt",
    pygame.KMOD_ALT: "alt",
    pygame.KMOD_LMETA: "left meta",
    pygame.KMOD_RMETA: "right meta",
    pygame.KMOD_META: "meta",
    pygame.KMOD_NUM: "num",
    pygame.KMOD_MODE: "mode",
}


# Example custom module:
# import settings
# settings.starting_level = 1
# settings.skip_to = 'game'

try:
    import _gummbumcustom
except ImportError:
    _gummbumcustom = None

try:
    import _dr0idcustom
except ImportError:
    _dr0idcustom = None
