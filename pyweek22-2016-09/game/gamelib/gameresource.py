# -*- coding: utf-8 -*-
from .pyknic.pygame.resource import resources, FONT, IMAGE, SOUND, SONG

from . import settings


class GameResource(object):
    # FONT_* = -1
    # IMG_* = -1
    # *_positions = {}
    SOUND_BAG_ALARM = -1
    SOUND_EARTHQUAKE = -1
    SOUND_RECHARGE = -1
    SOUND_SLURP1 = -1
    SOUND_SLURP2 = -1
    SOUND_SLURP3 = -1
    SOUND_SLURP4 = -1
    SOUND_SLURP5 = -1
    SOUND_SLURP6 = -1
    SOUND_SLURP7 = -1
    SOUND_SLURP8 = -1
    SOUND_SLURP9 = -1
    SOUND_SLURPA = -1
    SOUND_SLURPB = -1
    SOUND_SLURPC = -1
    SOUND_SLURPD = -1
    SOUND_SLURPE = -1
    SOUND_SLURPF = -1
    sound_slurp_queue = []
    SOUND_MUNCH1 = -1
    SOUND_MUNCH2 = -1
    SOUND_MUNCH3 = -1
    SOUND_MUNCH4 = -1
    SOUND_MUNCH5 = -1
    SOUND_MUNCH6 = -1
    sound_munch_queue = []

    SONG_1 = -1
    SONG_ACTION1 = -1
    SONG_ACTION2 = -1
    SONG_ACTION3 = -1
    SONG_ACTION4 = -1
    SONG_ACTION5 = -1
    song_queue = []

    IMG_VACUUM = -1
    IMG_MOUSE = -1
    IMG_LMB = -1
    IMG_RMB = -1

    @staticmethod
    def load():
        # fonts
        # __*_font_path = "data/font/*.ttf"
        # GameResource.FONT_*_ID = resources.load(FONT, (__*_font_path, 21))

        # images
        GameResource.IMG_VACUUM = resources.load(IMAGE, 'data/vacuum.png', 1.0)
        GameResource.IMG_MOUSE = resources.load(IMAGE, 'data/mouse.png', 1.0)
        GameResource.IMG_LMB = resources.load(IMAGE, 'data/lmb.png', 1.0)
        GameResource.IMG_RMB = resources.load(IMAGE, 'data/rmb.png', 1.0)

        # GameResource.*_positions = {
        #     (420, 258, 185, 216): [GameResource.IMG_DOOR_0_CLOSED, GameResource.IMG_*, 0],
        # }

        # sounds
        vol = settings.SFX_VOLUME
        GameResource.SOUND_BAG_ALARM = resources.load(SOUND, 'data/sfx/bagalarm.ogg', vol * 0.05)
        GameResource.SOUND_EARTHQUAKE = resources.load(SOUND, 'data/sfx/earthquake.ogg', vol * 1.0)
        GameResource.SOUND_RECHARGE = resources.load(SOUND, 'data/sfx/recharge.ogg', vol * 1.0)

        GameResource.SOUND_SLURP1 = resources.load(SOUND, 'data/sfx/splurp1.ogg', vol * 0.3)
        GameResource.SOUND_SLURP2 = resources.load(SOUND, 'data/sfx/splurp2.ogg', vol * 0.3)
        GameResource.SOUND_SLURP3 = resources.load(SOUND, 'data/sfx/splurp3.ogg', vol * 0.3)
        GameResource.SOUND_SLURP4 = resources.load(SOUND, 'data/sfx/splurp4.ogg', vol * 0.3)
        GameResource.SOUND_SLURP5 = resources.load(SOUND, 'data/sfx/splurp5.ogg', vol * 0.3)
        GameResource.SOUND_SLURP6 = resources.load(SOUND, 'data/sfx/splurp6.ogg', vol * 0.3)
        GameResource.SOUND_SLURP7 = resources.load(SOUND, 'data/sfx/splurp7.ogg', vol * 0.3)
        GameResource.SOUND_SLURP8 = resources.load(SOUND, 'data/sfx/splurp8.ogg', vol * 0.3)
        GameResource.SOUND_SLURP9 = resources.load(SOUND, 'data/sfx/splurp9.ogg', vol * 0.3)
        GameResource.SOUND_SLURPA = resources.load(SOUND, 'data/sfx/splurpa.ogg', vol * 0.3)
        GameResource.SOUND_SLURPB = resources.load(SOUND, 'data/sfx/splurpb.ogg', vol * 0.3)
        GameResource.SOUND_SLURPC = resources.load(SOUND, 'data/sfx/splurpc.ogg', vol * 0.3)
        GameResource.SOUND_SLURPD = resources.load(SOUND, 'data/sfx/splurpd.ogg', vol * 0.3)
        GameResource.SOUND_SLURPE = resources.load(SOUND, 'data/sfx/splurpe.ogg', vol * 0.3)
        GameResource.SOUND_SLURPF = resources.load(SOUND, 'data/sfx/splurpf.ogg', vol * 0.3)
        GameResource.sound_slurp_queue = [
            GameResource.SOUND_SLURP1, GameResource.SOUND_SLURP2, GameResource.SOUND_SLURP3,
            GameResource.SOUND_SLURP4, GameResource.SOUND_SLURP5, GameResource.SOUND_SLURP6,
            GameResource.SOUND_SLURP7, GameResource.SOUND_SLURP8, GameResource.SOUND_SLURP9,
            GameResource.SOUND_SLURPA, GameResource.SOUND_SLURPB, GameResource.SOUND_SLURPC,
            GameResource.SOUND_SLURPD, GameResource.SOUND_SLURPE, GameResource.SOUND_SLURPF,
        ]

        GameResource.SOUND_MUNCH1 = resources.load(SOUND, 'data/sfx/munch1.ogg', vol * 1.0)
        GameResource.SOUND_MUNCH2 = resources.load(SOUND, 'data/sfx/munch2.ogg', vol * 1.0)
        GameResource.SOUND_MUNCH3 = resources.load(SOUND, 'data/sfx/munch3.ogg', vol * 1.0)
        GameResource.SOUND_MUNCH4 = resources.load(SOUND, 'data/sfx/munch4.ogg', vol * 1.0)
        GameResource.SOUND_MUNCH5 = resources.load(SOUND, 'data/sfx/munch5.ogg', vol * 1.0)
        GameResource.SOUND_MUNCH6 = resources.load(SOUND, 'data/sfx/munch6.ogg', vol * 1.0)
        GameResource.sound_munch_queue = [
            GameResource.SOUND_MUNCH1, GameResource.SOUND_MUNCH2, GameResource.SOUND_MUNCH3,
            GameResource.SOUND_MUNCH4, GameResource.SOUND_MUNCH5, GameResource.SOUND_MUNCH6,
        ]

        # songs
        vol = settings.MUSIC_VOLUME
        GameResource.SONG_1 = resources.load(
            SONG, 'Sunder', 'data/music/Nightstep - Sunder.ogg', vol * 1.0)
        GameResource.SONG_ACTION1 = resources.load(
            SONG, 'Alpha Machines', 'data/music/Alpha Machines - SAYF [Syrex Music Release].ogg', vol * 1.0)
        GameResource.SONG_ACTION2 = resources.load(
            SONG, 'Get Out Alive', 'data/music/Nightcore - Get Out Alive.ogg', vol * 1.0)
        GameResource.SONG_ACTION3 = resources.load(
            SONG, 'Enemies', 'data/music/Nightcore - Enemies (Krayzius).ogg', vol * 1.0)
        GameResource.SONG_ACTION4 = resources.load(
            SONG, 'Flute', 'data/music/Nightcore - Flute.ogg', vol * 1.0)
        GameResource.SONG_ACTION5 = resources.load(
            SONG, "Vivaldi's Four Seasons [Winter]",
            "data/music/Nightcore - Vivaldi's Four Seasons [Winter].ogg", vol * 1.0)
        GameResource.song_queue = [
            GameResource.SONG_ACTION1,
            GameResource.SONG_ACTION2,
            GameResource.SONG_ACTION3,
            GameResource.SONG_ACTION4,
            GameResource.SONG_ACTION5,
        ]
    @staticmethod
    def get(resource_id):
        return resources.get_resource(resource_id)
