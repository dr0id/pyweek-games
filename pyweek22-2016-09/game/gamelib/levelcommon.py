import pygame

from .pyknic.mathematics import Vec3 as Vec

from . import settings
from .settings import KIND_ANT


# ==========
# CODER_TIPS
# ==========
# ANT_SIZE_VAR, HEALTH_VAR, ANT_SPEED_VAR can be set to 0 to have no random component
# ANT_STRAIGHT_TIME_VAR should be some value since 0 would mean it is constantly changing direction
# ANT_ANGLE_VAR can be set to 0 but would mean it will walk always straight to the chips!
# ANT_ANGLE_VAR half of the time it will go left and the other half to the right
# health is used to make the stuck bugs killable by sucking
# crushing is always instantaneous

class AntConfig(object):
        kind = KIND_ANT
        ANT_SPEED = 20.2  # Speed of ants
        ANT_SPEED_VAR = 20.55  # ANT_SPEED + [0, ANT_SPEED_VAR)
        ANT_ANGLE_VAR = 120  # direction +- angle
        ANT_STRAIGHT_TIME_VAR = 5  # time it will walk straight [0, ANT_STRAIGHT_TIME_VAR) seconds
        ANT_CLING_FORCE = 3  # if the suction force is stronger than this it will cling
        ANT_SUCK_FORCE = 20  # if the suction force is stronger than this it will be sucked in
        ANT_SIZE = 10  # the size of the ant -> if bigger than VC_MAX_SUCK_SIZE then it will get stuck!
        ANT_SIZE_VAR = 10  # ANT_SIZE +- ANT_SIZE_VAR/2 -> [ANT_SIZE - ANT_SIZE_VAR/2 , ANT_SIZE + ANT_SIZE_VAR/2)
        HEALTH = 10
        HEALTH_VAR = 10
        BITE = 10  # how much is eaten each EAT_INTERVAL
        EAT_INTERVAL = 1.0  # eat interval
assert AntConfig.ANT_CLING_FORCE < AntConfig.ANT_SUCK_FORCE, "clinge force should be lower than suck force!"


class AntRacerConfig(object):
        kind = KIND_ANT
        ANT_SPEED = 100.2  # Speed of ants
        ANT_SPEED_VAR = 20.55  # ANT_SPEED + [0, ANT_SPEED_VAR)
        ANT_ANGLE_VAR = 100  # direction +- angle
        ANT_STRAIGHT_TIME_VAR = 2  # time it will walk straight [0, ANT_STRAIGHT_TIME_VAR) seconds
        ANT_CLING_FORCE = 2  # if the suction force is stronger than this it will cling
        ANT_SUCK_FORCE = 15  # if the suction force is stronger than this it will be sucked in
        ANT_SIZE = 5  # the size of the ant -> if bigger than VC_MAX_SUCK_SIZE then it will get stuck!
        ANT_SIZE_VAR = 5  # ANT_SIZE +- ANT_SIZE_VAR/2 -> [ANT_SIZE - ANT_SIZE_VAR/2 , ANT_SIZE + ANT_SIZE_VAR/2)
        HEALTH = 10
        HEALTH_VAR = 10
        BITE = 5  # how much is eaten each EAT_INTERVAL
        EAT_INTERVAL = 1.0  # eat interval


class RoachConfig(object):
        kind = KIND_ANT
        ANT_SPEED = 50.2  # Speed of ants
        ANT_SPEED_VAR = 10.55  # ANT_SPEED + [0, ANT_SPEED_VAR)
        ANT_ANGLE_VAR = 60  # direction +- angle
        ANT_STRAIGHT_TIME_VAR = 3  # time it will walk straight [0, ANT_STRAIGHT_TIME_VAR) seconds
        ANT_CLING_FORCE = 5  # if the suction force is stronger than this it will cling
        ANT_SUCK_FORCE = 30  # if the suction force is stronger than this it will be sucked in
        ANT_SIZE = 20  # the size of the ant -> if bigger than VC_MAX_SUCK_SIZE then it will get stuck!
        ANT_SIZE_VAR = 15  # ANT_SIZE +- ANT_SIZE_VAR/2 -> [ANT_SIZE - ANT_SIZE_VAR/2 , ANT_SIZE + ANT_SIZE_VAR/2)
        HEALTH = 20
        HEALTH_VAR = 15
        BITE = 20  # how much is eaten each EAT_INTERVAL
        EAT_INTERVAL = 1.0  # eat interval


class RoachBurlyConfig(object):
        kind = KIND_ANT
        ANT_SPEED = 80.2  # Speed of ants
        ANT_SPEED_VAR = 0  # ANT_SPEED + [0, ANT_SPEED_VAR)
        ANT_ANGLE_VAR = 30  # direction +- angle
        ANT_STRAIGHT_TIME_VAR = 3  # time it will walk straight [0, ANT_STRAIGHT_TIME_VAR) seconds
        ANT_CLING_FORCE = 5  # if the suction force is stronger than this it will cling
        ANT_SUCK_FORCE = 30  # if the suction force is stronger than this it will be sucked in
        ANT_SIZE = 500  # the size of the ant -> if bigger than VC_MAX_SUCK_SIZE then it will get stuck!
        ANT_SIZE_VAR = 1  # ANT_SIZE +- ANT_SIZE_VAR/2 -> [ANT_SIZE - ANT_SIZE_VAR/2 , ANT_SIZE + ANT_SIZE_VAR/2)
        HEALTH = 25
        HEALTH_VAR = 0
        BITE = 50  # how much is eaten each EAT_INTERVAL
        EAT_INTERVAL = 1.0  # eat interval


# A level has:
#   -   One or more bug spigots. Each spigot has (see bugspigot.BugSpigot constructor):
#       -   name: a descriptive name to aid debugging, if needed
#       -   factory: function; entitybug.*_factory
#       -   count: int; number of bugs to dispense
#       -   interval: int or float; number of bugs to spawn per second
#       -   position: str or tuple(x, y); if str, it is a rect attr for the screen rect
#   -   what else?

# contants for convenient inline data formulae
SR = pygame.Rect(0, 0, settings.SCREEN_WIDTH, settings.SCREEN_HEIGHT)
W, H = SR.size
CX = SR.centerx
CY = SR.centery
TL, TR, BL, BR = Vec(*SR.topleft), Vec(*SR.topright), Vec(*SR.bottomleft), Vec(*SR.bottomright)
ML, MT, MR, MB = Vec(*SR.midleft), Vec(*SR.midtop), Vec(*SR.midright), Vec(*SR.midbottom)
C = Vec(*SR.center)

# configs
a0 = AntConfig
ar = AntRacerConfig
r0 = RoachConfig
rb = RoachBurlyConfig
