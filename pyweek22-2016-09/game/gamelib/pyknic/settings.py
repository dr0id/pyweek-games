# -*- coding: utf-8 -*-

"""
.. TODO:: document
.. todo:: use a signaling dict for settings!? 
"""

import os
import logging

logger = logging.getLogger(__name__)
logger.debug("importing...")

_user_home_path = os.path.expanduser('~')
# _user_home_path = os.path.join(_user_home_path, ".pyknic")
#
# if not os.path.exists(_user_home_path):
#     try:
#         os.makedirs(_user_home_path)
#     except OSError as ex:
#         logging.getLogger('').warn(str(ex))

# settings
documenting = False

# paths
user_home_path = _user_home_path


# logging
log_record_format = "%(asctime)s [%(threadName)-10s %(thread)d]: %(levelname)-8s %(message)s [%(name)s: %(funcName)s in %(filename)s(%(lineno)d)]"
log_date_format = None
log_logger_level = logging.DEBUG if __debug__ else logging.INFO
log_disable_existing_loggers = False
log_logger_name = 'pyknic'
log_filename = "pyknic.log"
log_path = os.getcwd()
log_path_and_filename = os.path.join(log_path, log_filename)
log_file_mode = 'a'
log_max_bytes = 5 * 1024 * 1024  # 5 MB
log_backup_count = 5
log_encoding = 'utf8'
log_delayed_logging = 1  # delayed open of stream to when first time it is written to, the file is opened here already
log_separator_width = 50

logger.debug("imported")
