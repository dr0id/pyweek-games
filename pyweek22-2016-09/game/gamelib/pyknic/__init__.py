# -*- coding: utf-8 -*-
"""
.. todo:: initial pyknic doc
.. todo:: explain how logging works and is configured

"""
import sys
import logging

# log info first before importing anything
from . import info
info.log_info()

from . import logs
from . import context
from . import cache
from . import events
from . import generators
from . import mathematics
from . import options
from . import resource
from . import settings
from . import timing
from . import tweening
from . import compatibility

from .info import log_info, check_version

__all__ = [
    # modules
    "logs",
    "info",
    "context",
    "cache",
    "events",
    "generators",
    "mathematics",
    "options",
    "resource",
    "settings",
    "timing",
    "tweening",
    # functions
    "log_info",
    "check_version",
    "compatibility",
]

logger = logging.getLogger(__name__)

# check python version
# BE WARNED: if you change this line it might work or not (the code is probably untested with new versions)
info.check_version("python", (2, 7), tuple(sys.version_info), (3, 6))

# TODO: write a predefined main loop -> subsystem management? (because of e.g.  music fadeout)
# TODO: logging to another process...?

logger.debug("imported " + "-" * 50)
