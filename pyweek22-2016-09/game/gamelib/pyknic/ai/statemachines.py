# -*- coding: utf-8 -*-

"""
This module contains different state machine implementations.


A sample state transition::

        +---------------+                                       +---------------+
        | state1        |                                       | state2        |
        |               |                                       |               |
        | /entry        |            event / action             | /entry        |
        | /exit         |-------------------------------------->| /exit         |
        | /activate     |                                       | /activate     |
        | /deactivate   |                                       | /deactivate   |
        |               |                                       |               |
        | +update       |                                       | +update       |
        |               |                                       |               |
        +---------------+                                       +---------------+


Sequence diagram of a transition (with optional action)::

                           state machine                  state1                      state2
                                |                           |                           |
                handle_event    |        handle_event       |                           |
                    ----------->|-------------------------->|                           |
                                |        switch_state       |                           |
                                |<--------------------------|                           |
                                |        exit               |                           |
                                |-------------------------->|                           |
        event_switching_state   |                           |                           |
                    <-----------|                           |                           |
                action          |                           |                           |
                    <-----------|                           |                           |
                                |        enter              |                           |
                                |------------------------------------------------------>|
        event_switched_state    |                           |                           |
                    <-----------|                           |                           |
                                |        switch_state       |                           |
                                |-  -  -  -  -  -  -  -  - >|                           |
                handle_event    |        handle_event       |                           |
                    < -  -  -  -|<  -  -  -  -  -  -  -  -  |                           |
                                |                           |                           |
                                |                           |                           |


Looking at the code, you will miss the handle_event method. This is intentional since this code is thought to be a
base to implement state driven agent design. One can implement it in different ways.

The important thing is that the state machine only handles events and that each state decides or contains the logic
to switch to another state. The handle_event method is called by the surrounding code, the environment. Its the
method through which the interaction with the agent from outside is performed.

Example 1 is compact and all the switching logic is in the handle_event method of the
states. The Example 2 the events are handled through dedicated methods.

Example 1::

    class Agent1(object):

        def __init__(self):
            self.state_machine = SimpleStateMachine(InitialState)
            # ...

        def handle_event(self, event):
            # the state switching logic is encapsulated in the states itself
            self.state_machine.current_state.handle_event(self.state_machine, event)

Example 2::

    class Agent2(object):

        def __init__(self):
            self.state_machine = SimpleStateMachine(InitialState)
            # ...

        def event1(self):
            self.state_machine.current_state.event1(self.state_machine)

        def event2(self):
            self.state_machine.current_state.event2(self.state_machine)

        # .... and so on, each event is handled through a method

A not recommended way of implementing the state_machine is in Example 3. The drawback there is that the
state switching logic is encapsulated in the if ...(elif)...else constructs in the event handling methods and not in
in the state itself (this is why the state has to be checked, which is bad).

Example 3::

    class Agent3(object):

        def __init__(self):
            self.state_machine = SimpleStateMachine(InitialState)
            # ...

        def event1(self):
            self.state_machine.switch_state(StateA)

        def event2(self):
            if self.state_machine.current_state is StateA:
                self.state_machine.switch_state(StateB)
            if self.state_machine.current_state is StateX:
                self.state_machine.switch_state(StateB)
            # ...
            else:
                self.state_machine.switch_state(StateC

        # .... and so on, each event is handled through a method and the event switching logic is all here


"""

import logging

from .. import events

logger = logging.getLogger(__name__)
logger.debug("importing...")



class SwitchingStateException(Exception):
    pass


class SimpleStateMachine(object):
    """
    A simple StateMachine.
    
    Usage::

        sm = StateMachine(InitialState)
        #sm.switch_state(State1) # set the first state
        
        # in main loop
        sm.current_state.update(dt)
        

        Never ever set current_state directly, ALWAYS use switch_state

    """

    def __init__(self, initial_state, entity=None, name=None, previous_state=None, global_state=None, owner=None):
        """
        Constructor.
        :param initial_state: The initial state of the state machine.
        :param name: An optional name for this state machine used to identify this instance. If not set id(self) is used.
        :param previous_state: The previous state. If not set then initial_state is used.
        :param global_state: The global state to use. Its update method is called before update of the current state.
        :param owner: The owner of the state machine. If None then owner is set to self.
        """
        self.name = id(self) if name is None else name
        # self.entity = self if owner is None else owner
        self.entity = entity
        self.event_switched_state = events.Signal()
        self.event_switching_state = events.Signal()
        self._switching_state = False
        self.current_state = initial_state
        self.previous_state = previous_state if previous_state is not None else initial_state
        self.global_state = global_state

    def switch_state(self, new_state, action=None):
        """
        Change to the next state. First exit is called on the current state,
        then the new state is made the current state and finally enter is called
        on the new state.

        During this method execution two events will be fired:
        event_switching_state with the arguments (state_machine, previous_state, new_state)
        This event is fired after current_state.exit and before action (if provided) and new_state.enter is called.

        The other event is fired after the switching is done.
        event_switched_state with the arguments (state_machine, previous_state).
        This event is fired after new_state.enter has been called.
        
        :Parameters:
            new_state : object
                Can be any object that has following methods: enter(sm) and 
                exit(sm), can not be None (use a end state instead).
            action : function
                A callback to be called as transition action. It will be called after the event_state_switching event
                and has only the state machine reference as argument, e.g. action(state_machine)
        
        """
        assert new_state is not None
        assert hasattr(new_state, "enter"), "new state should have an enter method"
        assert hasattr(getattr(new_state, "enter"), '__call__'), "new states enter attribute should be callable"
        assert hasattr(new_state, "exit"), "new state should have an exit method"
        assert hasattr(getattr(new_state, "exit"), '__call__'), "new states exit attribute should be callable"

        if self._switching_state is True:
            msg = "state machine is switching state, try to switch the state after (e.g. in the update " \
                  "method instead of enter or exit)!"
            raise SwitchingStateException(msg)

        self._switching_state = True

        self.current_state.exit(self)
        self.previous_state = self.current_state
        self.current_state = new_state

        if self.event_switching_state.has_observers:  # avoid extra method call for performance
            self.event_switching_state.fire(self.entity, self.previous_state, new_state)
        if action is not None:
            action(self.entity)

        self.current_state.enter(self)
        if self.event_switched_state.has_observers:  # avoid extra method call for performance
            self.event_switched_state.fire(self.entity, self.previous_state)

        self._switching_state = False

    def revert_to_previous_state(self):
        """
        Reverts the state machine to the previous state (actually it does self.switch_state(self.previous_state) )
        """
        self.switch_state(self.previous_state)

    def update(self, dt, *args, **kwargs):
        if self.global_state is not None:
            self.global_state.update(self.entity, dt, *args, **kwargs)
        self.current_state.update(self.entity, dt, *args, **kwargs)


class StateDrivenAgentBase(object):
    """
    A base class for a state driven agent. It is already working, but normally it should be inherited to extend
    the interaction parts or other convenience methods.
    """

    def __init__(self, initial_state, owner=None):
        """
        The initial data needed.
        :param initial_state: The initial state. The enter method is not called. If needed, do it in your code.
        :param owner: The owner of this agent. If it is None then self is used.
        """
        owner = self if owner is None else owner
        self.state_machine = SimpleStateMachine(initial_state, owner=owner)

    def handle_event(self, event):
        self.state_machine.current_state.handle_event(self.state_machine.owner, event)

    def update(self, dt, *args, **kwargs):
        self.state_machine.update(dt, *args, **kwargs)

logger.debug("imported")
