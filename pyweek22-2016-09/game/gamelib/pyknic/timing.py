# -*- coding: utf-8 -*-

"""
Module about timing classes and helpers.

"""
from __future__ import print_function, division

import warnings

import sys
import logging
import heapq
from heapq import heappop, heappush

from . import events

logger = logging.getLogger(__name__)
logger.debug("importing...")


__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id [at] gmail [dot] com"
__copyright__ = "DR0ID @ 2016"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"


class GameTime(object):
    """
    GameTime allows you to manipulate the virtual time in the game. You can
    slow down or speed up the time in your game. Also you can pause any time
    dependent code by setting the dilatation factor to 0. Trying to set a
    negative factor value will raise a NegativeFactorException.
    Each call to update will fire the (public) 'event_update' with the
    arguments: gdt
    (GameTime instance, game delta time, game time, real delta time, real time)

    Through the attribute time the current game time can be read (both in [ms]).

        :Variables:
            event_update : Event
                update event that is fired every time update is called,
                it passes gdt, the delta time passed in the game
    """

    def __init__(self, *args, **kwargs):
        """
        Init the GameTime with tick_speed=1.0.
        """
        # noinspection PyArgumentList
        super(GameTime, self).__init__(*args, **kwargs)
        self._factor = 1.0
        self._time = 0
        self._real_time = 0
        self.event_update = events.Signal("game time event_update")

    def _set_factor(self, factor):
        """
        For use in the factor property only. You get a warning when setting a
        negative value, but it is permitted.

        :Parameters:
            factor : float
              time dilatation factor, 0.0 pauses, negative values run the time
              backwards (scheduling does not work when running backwards).
        """
        if __debug__:
            if 0 > factor:
                warnings.warn('Using negative time factor!!')
        self._factor = factor

    def _get_factor(self):
        """
        Returns the dilation factor.
        """
        return self._factor

    tick_speed = property(_get_factor, _set_factor, doc="""
        Set the dilatation factor, 0.0 pauses, negative raise a warning and let
        the time run backwards (scheduling does not work with negative values)
        """)

    def _get_time(self):
        """
        Returns the game time.
        """
        return self._time

    time = property(_get_time, doc=""" game time since start, read-only""")

    def _get_real_time(self):
        """
        Returns the real time.
        """
        return self._real_time

    real_time = property(_get_real_time, doc=""" real time since start, read-only""")

    def update(self, real_delta_t):
        """
        Should be called each frame to update the game time.
        real_delta_t is the time passes in the last 'frame'.
        It fires the event_update with the arguments: gdt

            gdt : float
                game delta time

        :Parameters:
            real_delta_t : int
                real delta time (same as input)

        The game time is only advanced if update is called.
        """
        self._real_time += real_delta_t
        gdt = self._factor * real_delta_t
        self._time += gdt
        self.event_update.fire(gdt)


class LockStepper(object):
    """
    Based on: `<http://gafferongames.com/game-physics/fix-your-timestep/>`_

    This will advance its 'sim_time' in a 'lock step' mode.

    To register a integrate method of your simulation:

    >>> stepper = LockStepper()
    >>> def integrate(delta_seconds, sim_time): pass
    ...
    >>> stepper.event_integrate += integrate
    >>>

    :param max_steps: max number of steps taken in this frame, needed for
        the under sampling case, this will slow down the simulation
    :type max_steps: int

    """

    TIMESTEP_SECONDS = 0.01

    def __init__(self, max_steps=8, max_dt=0.05):
        """
        """
        self._accumulator = 0.0
        self._sim_time = 0
        self.event_integrate = events.Signal()
        self.max_steps = max_steps
        self.max_dt = max_dt

    def update(self, dt_seconds, timestep_seconds=TIMESTEP_SECONDS):
        """
        Updates the simulation using a in discrete time deltas (lock step).

        This triggers the 'event_integrate' signal

        :param dt_seconds: time in seconds passed since last frame
            make sure to clamp this value to a maximum value (because
            if a large value is passed in you don't want to wait until
            all steps have been calculated)
        :type dt_seconds: float
        :param timestep_seconds: the step size of the lock step
        :type timestep_seconds: float
        :returns: alpha in range [0, 1.0], is the remainder and means how
            far between the current step and the next step the simulation is.
            This should be used to interpolate the position of the current and
            previous state so the graphics are smooth.
        :rtype: float

        """
        # prevent spiral of death
        dt_seconds = self.max_dt if dt_seconds > self.max_dt else dt_seconds

        self._accumulator += dt_seconds
        num_steps = 0
        while self._accumulator >= timestep_seconds and num_steps < self.max_steps:
            self._sim_time += timestep_seconds
            self._accumulator -= timestep_seconds
            num_steps += 1
            self.event_integrate.fire(self, timestep_seconds, self._sim_time)

        if __debug__:
            # about it don't know how to achieve it in a good way
            if num_steps >= self.max_steps:
                logger.info("LockStepper::update() took max_steps" + str(self.max_steps))
            if self._accumulator > 200 * timestep_seconds:
                logger.warn("LockStepper::update() is way to slow to keep up!")

        alpha = self._accumulator / timestep_seconds
        return alpha - int(alpha)  # because of max_steps alpha can be > 1.0


class Scheduler(object):
    """
    This is a scheduler that works on a frame basis. Call update() on each
    frame and all scheduled methods (== callbacks) will be called.

    The return value of the callback decides if the callback is re-scheduled.
    If a callback returns 0 the callback will be removed and no further
    re-scheduling is done (the callback is called once).

    To re-schedule the callback it should return a integer number describing
    the next frame it should be called back.

    ::

        def callback(*args, **kwargs):
            # code
            return 10 # this callback is called in 10 frames again

    """

    STOP_REPEAT = 0.0

    def __init__(self):
        self._heap = []
        self._next_time = sys.maxsize
        self._current_time = 0
        self._next_id = 999

    def schedule(self, func, interval, offset=0, *args, **kwargs):
        """
        Schedule a function or method to be called back a time later.

        :param func: function or method to call back in the future
        :type func: function of method reference
        :param interval: time that should pass until next call of func,
            has to be >= 0 (a value of 0 means it is called next time
            the update method is called)
        :type interval: int (Scheduler) or float (Scheduler)
        :param offset: the start offset. This helps to distribute repeating
            callbacks evenly over time manually to prevent a high number
            of callbacks in a single frame.
        :type offset:  int (Scheduler) or float (Scheduler)
        :param args: arguments of the scheduled function
        :type args: list
        :param kwargs: keyword arguments of the scheduled function
        :type kwargs: dict

        .. todo:: using func to identify isn't enough because what if you schedule the same function multiple times
        and want to unschedule the last entry? use an ID to identify schedule??
        """
        assert interval >= 0
        self._next_id += 1
        _id = self._next_id
        next_time = self._current_time + interval + offset
        heappush(self._heap, (next_time, _id, _id, func, args, kwargs))
        if next_time < self._next_time:
            self._next_time = next_time
        return _id

    def update(self, delta_time):
        """
        Call this every frame. This will call any callbacks which time has passed.

        :param delta_time: delta time passed, can be whatever time unit is used
            (even a frame count)
        :type delta_time: float, int
        """
        self._current_time += delta_time
        assert self._current_time < sys.maxsize  # can this ever happened?
        heap = self._heap
        while self._next_time <= self._current_time:
            next_time, schedule_id, _id, func, args, kwargs = heappop(heap)

            interval = func(*args, **kwargs)

            assert interval is not None, "Scheduled method '{0}' should return interval!".format(func.__name__)

            if interval is not None and interval > 0.0:
                heappush(heap, (next_time + interval, self._next_id, _id, func, args, kwargs))
                self._next_id += 1

            self._next_time = heap[0][0] if heap else sys.maxsize  # else is the break part

    def clear(self):
        """
        removes all callbacks.
        """
        self._heap[:] = []
        self._next_time = sys.maxsize

    def remove(self, schedule_id):
        """
        Remove a callback function.

        :param schedule_id: the id of the function to remove, does nothing if id is not
            in schedule
        :type schedule_id: id returned by schedule
        """
        for entry in self._heap:
            next_time, sc, entry_id, entry_func, args, kwargs = entry
            if entry_id == schedule_id:
                self._heap.remove(entry)
                break
        heapq.heapify(self._heap)  # make sure that the heap invariants are still valid
        self._next_time = self._heap[0][0] if self._heap else sys.maxsize  # else is the break part


class Timer(object):
    """
    Timer is a convenience class to use a :class:`Scheduler` or one of its
    derived classes. By default it uses a :class:`Scheduler`. If
    another scheduler should be used, assign another instance of a
    Scheduler to Timer.scheduler *before* **any** use of the Timer class.

    Once a frame you need to call the schedulers update method:

    ::

        # once a frame in your mainloop
        Timer.scheduler.update(delta_time)

    You could assign different schedulers to instances of the Timer class
    by overriding the class attribute scheduler with an instance attribute

    ::

        my_scheduler = Scheduler()
        timer = Timer(10)
        timer.scheduler = my_scheduler

    Don't forget to update the used scheduler once per frame.

    The timer class has an event_elapsed attribute which is a
    :class:`events.Signal` instance. It is used like this:

    ::

        # my_callback should have following signature
        def my_callback(self, timer_that_fired):
            pass

        # register my_callback at the timer
        timer = Timer(0.5, False)
        timer.event_elapsed += my_callback



    :classattr scheduler: a Scheduler or a derived class

    :attr interval: the interval to be used
    :attr repeat: repeated firing of the elapsed event

    :param interval: the time to wait until the event_elapsed is fired
    :type interval: depends what the scheduler uses, defaults to a float
        representing seconds
    :param repeat: defaults to False, if the elapsed event should be fired
        repeatedly with the given interval
    :type repeat: bool
    :param name: to name th timer so one can distinguish it in the logs
    :type name: string
    :param scheduler: the scheduler to use. If None is passed then the Timer.scheduler is used.
    :type scheduler: Scheduler

    """

    scheduler = Scheduler()

    def __init__(self, interval, repeat=False, name=None, scheduler=None):
        """
        Constructor.
        """
        self.interval = interval
        self.repeat = repeat
        self.event_elapsed = events.Signal()
        self._is_running = False
        self._cb_id = -1
        self._name = id(self) if name is None else name
        logger.debug("Timer {0} created", self._name)
        if scheduler is not None:
            self.scheduler = scheduler
        self._is_in_callback = False

    def __del__(self):
        if self._is_running:
            print("Timer should not be running anymore if garbage collected! ", self._name, file=sys.stderr)
            self.scheduler.remove(self._cb_id)

    def start(self, interval=None, repeat=None):
        """
        Starts the timer to fire the elapsed events (internally: it schedules
        a callback method to the scheduler).

        :param interval: defaults to None, overwrites the interval otherwise
        :param repeat: defaults to None, overwrites the repeat attribute if
            set to a boolean value
        :type repeat: bool
        """
        if self._is_running is True:
            logger.debug("timer '{0}' already started doing nothing", self._name)
            return

        self._is_running = True

        if interval is not None:
            self.interval = interval

        if repeat is not None:
            self.repeat = repeat

        if not self._is_in_callback:
            self._cb_id = self.scheduler.schedule(self._callback, self.interval)
        logger.debug("timer '{0}' started", self._name)

    def stop(self):
        """
        Stops the timer from firing the elapsed event (internally: it removes
        all callbacks from the scheduler).
        """
        if self._is_running is False:
            logger.debug("timer '{0}' already stopped doing nothing.", self._name)
            return

        self._is_running = False
        self.scheduler.remove(self._cb_id)
        logger.debug("timer '{0}' stopped", self._name)

    def _callback(self):
        """
        Callback method that gets called from the scheduler.
        """
        self._is_in_callback = True
        if self._is_running is False:
            logger.warning("timer {0} callback called even if it is not running!", self._name)
        logger.debug("timer '{0}' callback...", self._name)

        # _was_running = self._is_running
        self._is_running = None

        self.event_elapsed.fire(self)

        if self._is_running is None:
            self._is_running = self.repeat

        if self._is_running is True:
            self._is_running = True
            logger.debug("timer '{0}' re-scheduling after callback", self._name)
            self._is_in_callback = False
            return self.interval
        else:
            self._is_running = False
            logger.debug("timer '{0}' callback done and timer stopped", self._name)
            self._is_in_callback = False
            return 0

        # if you make this line of code reachable then dont forget this: self._is_in_callback = True


logger.debug("imported")
