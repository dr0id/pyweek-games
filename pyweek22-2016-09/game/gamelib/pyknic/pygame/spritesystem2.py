# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function
import pygame
from pyknic.mathematics import Point2, Point3, Vec3, Vec2, TYPE_POINT, TYPE_VECTOR
from pyknic.events import Signal
# from pyknicpygame.pyknic.mathematics import Point2, Vec2
import spritesystem2

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module


#
# bird            b                ---
# street light    l2           -----------------
# tree            t                  -------------------
# moped light     l1       ---------------------
# street          s     ---------------------------------------
# street behind   g     ---------------------------------------
#
# ^ normal blit
# + blend add
# * blend mult
# no operator precedence except the brackets, just left to right
# a = ambient color (darkness, assume nearly black)
#
# in spritesystem this was used: pixel = (a + l1 + l2) * (g ^ s ^ t ^ b)
# disadvantage: no sprites between layers of light
#
#
#
# can only have max the original color
# pixel = (g ^ s) * (a + l1 + l2) ^ (t * (a + l2)) ^ (b * a)
#
# this has color compression!
# pixel = (g ^ s) * a + (l1 + l2) ^ (t * a + l2) ^ (b * a)
#
#
#

class Sprite(object):
    """
    Sprite class designed almost as a pure container class. It has to be set dirty if a value changes,
    there is no logic that does that automatically for performance reasons (there may be some exceptions).

    :param image: The image to blit.
    :param x: The x position relative to tracked. If tracked is None then World is assumed.
    :param y: The y position relative to tracked. If tracked is None then World is assumed.
    :param z: The z position relative to tracked. If tracked is None then World is assumed.
    :param layer: The layer it is drawn in. Blit order is from small to big (e.g. 0, ..., n).
    :param rotation: The rotation relative to the tracked rotation.
    :param flip_x: Flipping the image in x direction.
    :param flip_y: Flipping the image in y direction.
    :param alpha: The alpha value, 0 transparent, 255 opaque. Can also be set with per pixel alpha.
    :param zoom: The zoom level. 1.0 corresponds to original size.
    :param parallax_x: The parallax factor in x direction. No parallax with 1.0.
    :param parallax_y: The parallax factor in y direction. No parallax with 1.0.
    :param offset_x: The offset in x direction from the x position.
    :param offset_y: The offset in y direction from the y position.
    :param offset_z: The offset in z direction from the z position.
    :param tracked: The entity to track. Its coordinate system is used to calculate the world position and rotation.
    :param dirty_set: The set where the dirty sprites are added. Should be a shared set.
    """
    dirty_sprites = set()

    def __init__(self, image_id, pos, layer=0, angle=0.0, flip_x=False, flip_y=False, alpha=255, zoom=1.0,
                 parallax_x=1.0, parallax_y=1.0, parallax_z=1.0, offset=Vec2.zero(), tracked=None, dirty_set=None,
                 shadow_id=None, is_light=False, zoom_x=None, zoom_h=None, after_zoom_x=None, after_zoom_y=None):
        # other
        """

        :type dirty_set: set
        """
        # relational attributes
        # TODO: if set, use position of parent and update children?
        self.parent = None
        self.children = []
        if tracked:
            self.track(tracked)

        # behavior attributes
        self.always_dirty = False
        self.static = False
        self.is_visible = True
        self.is_special_draw = False

        # positional attributes
        # TODO: use position point to be shared (either Point2 or Point3)?
        # TODO: position vs parent?
        # TODO: use Point2 or Point3 for position
        # assert isinstance(pos, Point2) or isinstance(pos, Point3)
        assert pos.w == TYPE_POINT
        self.position = pos
        # TODO: use Vec2 or Vec3 for offset?
        # assert isinstance(offset, Vec2) or isinstance(offset, Vec3)
        assert offset.w == TYPE_VECTOR
        self.offset = offset

        # TODO: use Vec2 or Vec3 for parallax?
        self.parallax_x = parallax_x
        self.parallax_y = parallax_y
        self.parallax_z = parallax_z

        self.layer = layer

        # size manipulating
        # TODO: maybe use a transformation command chain so the user can set up the transformation order
        self.flip_x = flip_x
        self.flip_y = flip_y
        # TODO: alpha resolution to minimize the number of steps for caching
        self.alpha = alpha  # 0-255  # TODO: how to make that sharable? Vec1/Point1 ?
        self.zoom_x = zoom_x  # scale image to width before rotation
        self.zoom_h = zoom_h
        # TODO: angle resolution to minimize the number of steps for caching
        self.angle = angle  # [0, 360)
        self.after_zoom_x = after_zoom_x
        self.after_zoom_y = after_zoom_y

        self.zoom = zoom

        self.angle_resolution = 3  # min angle step -> class field for default?
        self.zoom_resolution = 0.001  # min resolution step

        # sub-pixel
        self.use_sub_pixel = False
        self.sub_pixels_x = 3
        self.sub_pixels_y = 3

        self.blur_amount = 1.0  # TODO: not sure if this belongs here

        # blit attributes, pygame properties
        self.area = None  # source area to use for blit
        self.surf_flags = 0  # blit flags
        assert isinstance(image_id, int)
        self.image_id = image_id

        # light and shadow
        self.shadow_id = shadow_id
        self.is_light = is_light  # TODO: how to handle this correctly??

        # dirty handling attributes
        if dirty_set is not None:
            assert isinstance(dirty_set, set)
            self.dirty_sprites = dirty_set  # TODO: convert to property
        # self.is_dirty = True

        # TODO: these values are set by the renderer
        # the screen rect has to be handled by render system (region....), set as soon added to the renderer
        # # screen rect to identify rect on screen, can span multiples regions?
        self.rect = None
        self.old_position = None

    @property
    def tracked(self):
        """
        If set, then it will use this entities attributes for position and rotation.

        :return: The track entity or None.
        """
        return self.parent

    def track(self, tracked, share_position=True):
        """
        If set, then it will use this entities attributes for position and rotation.

        :param tracked: The entity to track.
        """
        if self.parent is tracked:
            return

        if tracked is self:
            raise Exception("bla")

        if self.parent:
            self.parent.children.remove(self)
            self.position = self.position.clone()  # un-share position

        assert hasattr(tracked, "children")
        assert isinstance(tracked.children, list)

        self.parent = tracked
        if tracked is not None:
            tracked.children.append(self)

            if share_position:
                assert hasattr(tracked, "position")
                assert isinstance(tracked.position, Vec2) or isinstance(tracked.position, Vec3)
                self.position = tracked.position

        self.is_dirty = True

    def _get_dirty(self):
        # TODO: is this even needed!? -> dirty sprites are in the set!!!
        # TODO: performance of this is poor (O(n)), maybe use a dirty attribute! <-- how to reset it?
        return self in self.dirty_sprites

    # noinspection PyUnusedLocal
    def _set_dirty(self, value):
        self.dirty_sprites.add(self)
        for c in self.children:
            c.is_dirty = True

    is_dirty = property(_get_dirty, _set_dirty)

    # TODO: define arguments
    def draw(self, surf, cam, interpolation_factor=1.0):
        """
        If 'draw_special' is set to True, then this method will be called from the renderer. Here special draw
        code can be implemented. All coordinate transformations have to be done here (simplest is to use the
        cam.world_to_screen() method). It should return a rect in screen coordinates that will be used for drawing
        and picking.

        :returns: rect in screen coordinates (returns a Rect(0, 0, 0, 0) by default)

        :Parameters:
            surf : Surface
                the surface to draw onto
            cam : Camera
                the camera that is used to render, determines the world position and the screen area
            interpolation_factor[1.0] : float
                the interpolation factor to use, should be in the range [0, 1.0), other values might work too but
                the result might be unwanted
        """

        pass


# TODO: algorithm, speed?
# for spr in dirty sprite:
#   find cells, mark dirty
# for cam in cams:
#   for cell in dirty_cells
#      render cell to cams surf, maks cam dirty
# for cam in dirty_cams:
#   transform cams surf to blit on screen



class RenderSystem(object):

    # TODO: dependency Resources
    # TODO: unit to pixel (cam?)?
    # TODO: init parameters for windows, screen?
    def __init__(self, dirty_set=Sprite.dirty_sprites):
        assert isinstance(dirty_set, set)
        self.dirty_sprites = dirty_set
        self._sprites = set()
        self._cams = set()

    # sprites
    def add_sprite(self, sprite):
        sprite.is_dirty = True
        self._sprites.add(sprite)

    def add_sprites(self, sprites):
        self._sprites.update(sprites)
        self.dirty_sprites.update(sprites)

    def remove_sprite(self, sprite):
        self.dirty_sprites.add(sprite)
        self._sprites.remove(sprite)

    def remove_sprites(self, sprites):
        self.dirty_sprites.update(sprites)
        self._sprites.difference_update(sprites)

    def clear_sprites(self):
        self.dirty_sprites.update(self._sprites)
        self._sprites.clear()

    def get_sprites(self):  # TODO: replace with get property?
        return list(self._sprites)

    def get_sprites_in_rect(self, rect): pass
    def push_sprites(self, sprites): pass
    def pop_sprites(self): pass

    # cams
    def add_cam(self, cam): pass  # TODO: create_cam(...) ??
    def remove_cam(self, cam): pass
    def get_cams(self): pass

    # draw
    def draw(self, surf=None, interpolation_factor=1.0, clear_color=(255, 0, 255), clear_flags=0):
        if self.dirty_sprites:

            if surf is None:
                surf = pygame.display.get_surface()

            if clear_color is not None:
                surf.fill(clear_color, None, clear_flags)

            # # find cells and mark them dirty
            # dirty_cells = set()
            # for dirty_sprite in self.dirty_sprites:
            #     pass
            #
            # # find cams and mark them dirty
            # for cam in self._cams:
            #     pass


            sprites = list(self._sprites)
            sprites.sort(key=lambda s: s.layer)
            dirty_rects = []
            for spr in sprites:
                screen_rect = surf.blit(spr)
                dirty_rects.append(screen_rect)
                spr.rect = screen_rect

            self.dirty_sprites.clear()
            return dirty_rects




class Camera(object):

    dirty_cams = set()

    def __init__(self, world_position, view_port, angle=0.0, ppu=1.0):
        self._position = Point2(0.0, 0.0)
        self._old_position = Point2(0.0, 0.0)
        self.position = world_position
        self._angle = 0.0
        self._old_angle = 0.0
        self.angle = angle  # [0, 360) rotation of camera in the world, TODO: should be property?
        self.zoom = 1.0  # TODO: should be property?
        self.zoom_x = 1.0  # TODO: should be property?
        self.zoom_y = 1.0  # TODO: should be property?
        self._ppu = ppu


        # viewport settings, offset is to center from topleft corner
        self._rect_offset_in_pixels = 0
        self._rect_screen_position_in_pixels = 0
        self._rect = None
        self.rect = view_port

    @property
    def rect(self):
        return self._rect  # rect on screen, the viewport

    @rect.setter
    def rect(self, value):
        self._rect = value
        self._rect_offset_in_pixels = Vec2(self._rect.w // 2, self._rect.h // 2)
        self._rect_screen_position_in_pixels = Vec2(self._rect.x, self._rect.y)

    @property
    def world_rect(self): return None  # the area of the world covered by this camera
    @property
    def position(self): return self._position  # TODO: consider tracked object
    @position.setter
    def position(self, value):
        # TODO: consider tracked object
        self._old_position = self._position
        self._position = value

    @property
    def angle(self): return self._angle
    @angle.setter
    def angle(self, value):
        self._old_angle = self._angle
        self._angle = value

    # entity tacking
    @property
    def tracked_object(self): return self._tracked_obj
    @tracked_object.setter
    def tracked_object(self, obj): self._tracked_obj = obj  # TODO: asserts!

    # coord conversion
    def world_to_screen(self, world_pos, parallax_factors=Vec2(1.0, 1.0), offset_in_pixels=Vec2(0, 0), interpolation_factor=1.0):
        _cam_world_pos = self._old_position + interpolation_factor * (self._position - self._old_position)
        cam_coord = world_pos * self._ppu - (_cam_world_pos * self._ppu).mult_comp(parallax_factors)
        cam_coord += offset_in_pixels
        cam_coord *= self.zoom
        if self._angle != 0.0:
            angle = self._old_angle + (self._angle - self._old_angle) * interpolation_factor
            cam_coord.rotate(angle)
        screen_pos = cam_coord + self._rect_screen_position_in_pixels + self._rect_offset_in_pixels
        return screen_pos

    def screen_to_world(self, screen_pos, parallax_factors=None, offset_in_pixels=Vec2(0, 0), ppu=1, interpolation_factor=1.0):
        return 0



class LazySprite(object):
    def __init__(self, image_id, position, dirty_set=None):
        self.dirties = dirty_set
        self.position = Point3(0, 0, 0)
        self.children = []
        self.parent = None
        self.event_x_changed = Signal("x")
        self.event_x_changed.add(self._set_dirty)
        self.event_y_changed = Signal("y")
        self.event_y_changed.add(self._set_dirty)
        self.event_z_changed = Signal("z")
        self.event_z_changed.add(self._set_dirty)
        self.event_track_changed = Signal("track")
        self.event_track_changed.add(self._set_dirty)
        assert isinstance(position, Vec2) or isinstance(position, Vec3)
        self.position = position
        self.image_id = image_id

    @property
    def x(self):
        """
        Relative x position to track.

        :return: Position in the coordinate system of track.
        """
        return self.position.x

    @x.setter
    def x(self, value):
        """
        Relative position to track.

        :param value: x coordinate.
        """
        old_x = self.position.x
        self.position.x = value
        self.event_x_changed.fire(old_x, value)

    @property
    def y(self):
        """
        Relative x position to track.

        :return: Position in the coordinate system of track.
        """
        return self.position.y

    @y.setter
    def y(self, value):
        """
        Relative position to track.

        :param value: y coordinate.
        """
        old_y = self.position.y
        self.position.y = value
        self.event_y_changed.fire(old_y, value)

    @property
    def z(self):
        """
        Relative x position to track.

        :return: Position in the coordinate system of track.
        """
        return self._z

    @z.setter
    def z(self, value):
        """
        Relative position to track.

        :param value: y coordinate.
        """
        old_z = self._z
        self._z = value
        self.event_z_changed.fire(old_z, value)

    @property
    def tracked(self):
        """
        If set, then it will use this entities attributes for position and rotation.

        :return: The track entity or None.
        """
        return self.parent

    def track(self, tracked):
        """
        If set, then it will use this entities attributes for position and rotation.

        :param tracked: The entity to track.
        """
        if self.parent is tracked:
            return

        if tracked is self:
            raise Exception("bla")

        old_parent = self.parent
        if self.parent:
            self.parent.children.remove(self)

        assert hasattr(tracked, "children")

        self.parent = tracked
        tracked.children.append(self)

        self.event_track_changed.fire(old_parent, tracked)

    def _get_dirty(self):
        # TODO: performance of this is poor (O(n)), maybe use a dirty attribute!
        return self in self.dirties

    # noinspection PyUnusedLocal
    def _set_dirty(self, *args, **kwargs):
        self.dirties.add(self)
        for c in self.children:
            c.is_dirty = True

    is_dirty = property(_get_dirty, _set_dirty)
