# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

import pygame

from .audio import Song
from ..resource import Resources, AbstractResourceLoader
from ... import spritesheetlib

logger = logging.getLogger(__name__)
logger.debug("importing...")


__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 


IMAGE = 1
SOUND = 2
SONG = 3
FONT = 4
SPRITESHEET = 5


resources = Resources()


class ImageLoader(AbstractResourceLoader):
    def load(self, filename, scale=1.0, angle=0, width=100, height=50, color=(255, 0, 0, 255)):
        """
        Loads an image from disk and applies scaling and/or rotation to it. If the file is not found a default surface
        is created using the optional arguments width, height and color.
        :param filename: name of the file to load
        :param scale: initial scaling, defaults to 1.0
        :param angle: initial angle, defaults to 0
        :param width: optional argument, defaults to 20
        :param height: optional argument, defaults to 20
        :param color:  optional argument, defaults to (255, 255, 0, 255)
        :return: pygame.Surface
        """
        try:
            img = pygame.image.load(filename).convert_alpha()
        except pygame.error:
            # TODO: log error
            img = self._create_surface(width, height, color, filename)
        if scale != 1.0 or angle != 0:
            img = pygame.transform.rotozoom(img, angle, scale)
        return img

    def _create_surface(self, width, height, color, file_name):
        img = pygame.Surface((width, height))
        img.fill(color)

        self._put_name_on_surf(file_name, img)

        img = img.convert_alpha()
        return img

    def _put_name_on_surf(self, file_name, img):
        try:
            pygame.font.init()
            if pygame.font.get_init():
                import os
                text = os.path.basename(file_name)
                font = pygame.font.Font(None, 15)
                name_surf = font.render(text, 0, (255, 255, 255))
                img.blit(name_surf, (0, 0))
        except:
            # TODO: log error...
            pass

    def unload(self, img):
        pass


class SoundLoader(AbstractResourceLoader):
    def load(self, filename, volume):
        import os
        if pygame.mixer.get_init() is None or not os.path.exists(filename):
            sfx = self._DummySound(filename)
        else:
            sfx = pygame.mixer.Sound(filename)
        sfx.set_volume(volume)
        return sfx

    def unload(self, res):
        pass

    class _DummySound(object):
        def __init__(self, file_name, volume=1.0):
            self.file_name = file_name
            self._volume = volume

        def play(self, loops=0, maxtime=0, fade_ms=0):
            # TODO: return Channel fake object
            print("sound play(): " + self.file_name)
            return None

        def stop(self):
            pass

        def fadeout(self, time):
            pass

        def set_volume(self, value):
            self._volume = value

        def get_volume(self):
            return self._volume

        def get_num_channels(self):
            return 0

        def get_length(self):
            return 0

        def get_raw(self):
            return bytes("")  # py3


class SongLoader(AbstractResourceLoader):
    def load(self, name, filename, volume):
        song = Song(name, filename, volume)
        return song

    def unload(self, res):
        pass


class FontLoader(AbstractResourceLoader):
    # TODO: load(self, filename, size)
    def load(self, filename):
        name, size = filename
        return pygame.font.Font(name, size)

    def unload(self, res):
        pass


class SpritesheetLoader(AbstractResourceLoader):
    def load(self, filename, zoom=1.0):
        sprites = spritesheetlib.SpritesheetLib10().load_spritesheet(spritesheetlib.FileInfo(filename))
        if zoom != 1.0:
            for spr in sprites:
                spr.image = pygame.transform.rotozoom(spr.image, 0.0, zoom)
        return sprites

    def unload(self, res):
        pass

resources.register_loader(IMAGE, ImageLoader())
resources.register_loader(SOUND, SoundLoader())
resources.register_loader(SONG, SongLoader())
resources.register_loader(FONT, FontLoader())
resources.register_loader(SPRITESHEET, SpritesheetLoader())

logger.debug("imported")
