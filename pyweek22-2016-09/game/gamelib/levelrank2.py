"""Rank 2 - simple ants, speedy ants, and roaches

================================
This set contains levels 6 - 11.
================================
"""

from .levelcommon import *


level_data = [
    {
        'tuned': True,
        'name': 'Rank 2',  #: : 2 corners, 2 roaches',
        'bug_spigots': [
            {'config': a0, 'count': 15, 'interval': 4.0, 'pos': TL, 'delay': 0.0},
            {'config': a0, 'count': 15, 'interval': 4.0, 'pos': BR, 'delay': 0.0},
            {'config': r0, 'count': 4, 'interval': 6.0, 'pos': BL, 'delay': 9.0, 'rumble': True},
            {'config': r0, 'count': 4, 'interval': 6.0, 'pos': TR, 'delay': 24.0, 'rumble': True},
        ],
        'potatoes': [
            {"pos": (CX + 50, H * 0.45), "amount": 50, 'w': 100, 'h': 75},
            {"pos": (CX - 50, H * 0.55), "amount": 50, 'w': 100, 'h': 75},
        ],
    },
    {
        'tuned': True,
        'name': 'Rank 2',  #: : 3 corners, 4 roaches',
        'bug_spigots': [
            {'config': a0, 'count': 9, 'interval': 6.0, 'pos': TL, 'delay': 0.0},
            {'config': a0, 'count': 9, 'interval': 6.0, 'pos': BR, 'delay': 0.0},
            {'config': a0, 'count': 9, 'interval': 6.0, 'pos': BL, 'delay': 0.0},
            {'config': r0, 'count': 2, 'interval': 8.0, 'pos': MB, 'delay': 10.0, 'rumble': True},
            {'config': r0, 'count': 2, 'interval': 8.0, 'pos': MT, 'delay': 13.0, 'rumble': True},
            {'config': r0, 'count': 2, 'interval': 8.0, 'pos': ML, 'delay': 16.0, 'rumble': True},
            {'config': r0, 'count': 2, 'interval': 8.0, 'pos': (W*0.9, H*0.8), 'delay': 24.0, 'rumble': True},
        ],
        'potatoes': [
            {"pos": (CX + 50, H * 0.45), "amount": 50, 'w': 100, 'h': 75},
            {"pos": (CX - 100, H * 0.55), "amount": 50, 'w': 100, 'h': 75},
        ],
    },
    {
        'tuned': True,
        'name': 'Rank 2',  #: : 4 corners, 1 roach',
        'bug_spigots': [
            {'config': ar, 'count': 7, 'interval': 8.0, 'pos': TL, 'delay': 0.0},
            {'config': ar, 'count': 7, 'interval': 8.0, 'pos': BR, 'delay': 0.0},
            {'config': ar, 'count': 7, 'interval': 8.0, 'pos': BL, 'delay': 0.0},
            {'config': ar, 'count': 7, 'interval': 8.0, 'pos': TR, 'delay': 0.0},
            {'config': a0, 'count': 6, 'interval': 6.0, 'pos': ML, 'delay': 8.0, 'rumble': True},
            {'config': a0, 'count': 6, 'interval': 6.0, 'pos': MR, 'delay': 12.0, 'rumble': True},
            {'config': r0, 'count': 6, 'interval': 6.0, 'pos': (W*0.5, H*0.7), 'delay': 20.0, 'rumble': True},
        ],
        'potatoes': [
            {"pos": (CX + 100, H * 0.45), "amount": 100, 'w': 100, 'h': 75},
            {"pos": (CX - 100, H * 0.55), "amount": 100, 'w': 100, 'h': 75},
        ],
    },
    {
        'tuned': True,
        'name': 'Rank 2',  #: : 4 waves from all edges, three popups',
        'bug_spigots': [
            {'config': ar, 'count': 4, 'interval': 4.0, 'pos': TL, 'delay': 0.0},
            {'config': ar, 'count': 4, 'interval': 4.0, 'pos': MT, 'delay': 0.0},
            {'config': ar, 'count': 4, 'interval': 4.0, 'pos': TR, 'delay': 0.0},
            {'config': ar, 'count': 4, 'interval': 4.0, 'pos': MR, 'delay': 0.0},
            {'config': ar, 'count': 4, 'interval': 4.0, 'pos': BR, 'delay': 0.0},
            {'config': ar, 'count': 4, 'interval': 4.0, 'pos': MB, 'delay': 0.0},
            {'config': ar, 'count': 4, 'interval': 4.0, 'pos': BL, 'delay': 0.0},
            {'config': ar, 'count': 4, 'interval': 4.0, 'pos': ML, 'delay': 0.0},
            {'config': r0, 'count': 4, 'interval': 4.0, 'pos': (W*0.4, H*0.5), 'delay': 16.0, 'rumble': True},
            {'config': ar, 'count': 4, 'interval': 4.0, 'pos': (W*0.3, H*0.5), 'delay': 18.5, 'rumble': True},
            {'config': ar, 'count': 4, 'interval': 4.0, 'pos': (W*0.7, H*0.3), 'delay': 18.5},
        ],
        'potatoes': [
            {"pos": (CX + 50, H * 0.45), "amount": 50, 'w': 100, 'h': 75},
            {"pos": (CX - 50, H * 0.55), "amount": 50, 'w': 100, 'h': 75},
        ],
    },
    {
        'tuned': True,
        'name': 'Rank 2',  #: : fan of streaming speeders, 2 popups',
        'bug_spigots': [
            {'config': ar, 'count': 4, 'interval': 0.5, 'pos': TL, 'delay': 0.0, 'rumble': True},
            {'config': ar, 'count': 4, 'interval': 0.5, 'pos': BR, 'delay': 2.0, 'rumble': True},
            {'config': ar, 'count': 4, 'interval': 0.5, 'pos': ML, 'delay': 4.0, 'rumble': True},
            {'config': ar, 'count': 4, 'interval': 0.5, 'pos': TR, 'delay': 6.0, 'rumble': True},
            {'config': ar, 'count': 4, 'interval': 0.5, 'pos': BL, 'delay': 8.0, 'rumble': True},
            {'config': ar, 'count': 4, 'interval': 0.5, 'pos': MR, 'delay': 10.0, 'rumble': True},
            {'config': ar, 'count': 4, 'interval': 0.5, 'pos': MT, 'delay': 12.0, 'rumble': True},
            {'config': ar, 'count': 4, 'interval': 0.5, 'pos': MB, 'delay': 14.0, 'rumble': True},
            {'config': ar, 'count': 20, 'interval': 0.25, 'pos': MR * 0.9, 'delay': 16.0, 'rumble': True},
            {'config': ar, 'count': 10, 'interval': 0.25, 'pos': MR * 0.9, 'delay': 20.0, 'rumble': True},
        ],
        'potatoes': [
            {"pos": (CX + 50, H * 0.45), "amount": 50, 'w': 100, 'h': 75},
            {"pos": (CX - 100, H * 0.55), "amount": 50, 'w': 100, 'h': 75},
        ],
    },
    {
        'tuned': False,
        'name': 'Rank 2',  #: : so many roaches, so little juice',
        'bug_spigots': [
            {'config': r0, 'count': 4, 'interval': 5.0, 'pos': (W*0.3, 0), 'delay': 0.0, 'rumble': True},
            {'config': r0, 'count': 4, 'interval': 5.0, 'pos': (0, H*0.8), 'delay': 14.0, 'rumble': True},
            {'config': r0, 'count': 4, 'interval': 5.0, 'pos': (W*0.6, H), 'delay': 28.0, 'rumble': True},
            {'config': r0, 'count': 4, 'interval': 5.0, 'pos': (W*0.2, H*0.3), 'delay': 42.0, 'rumble': True},
        ],
        'potatoes': [
            {"pos": (CX + 100, H * 0.45), "amount": 100, 'w': 100, 'h': 75},
            {"pos": (CX - 100, H * 0.55), "amount": 100, 'w': 100, 'h': 75},
        ],
    },
]
