# -*- coding: utf-8 -*-

import random

import pygame

from . import settings
from .gameresource import GameResource


class Song(object):
    now_playing = None


class Sfx(object):
    bug_slurp = None
    bug_munch = None
    bag_alarm_channel = None
    earthquake_channel = None


def bug_slurp():
    # if Sfx.bug_slurp and Sfx.bug_slurp.get_busy():
    #     return
    sfx_id = random.choice(GameResource.sound_slurp_queue)
    sfx = GameResource.get(sfx_id)
    Sfx.bug_slurp = sfx.play()


def bug_munch():
    # if Sfx.bug_munch and Sfx.bug_munch.get_busy():
    #     return
    sfx_id = random.choice(GameResource.sound_munch_queue)
    sfx = GameResource.get(sfx_id)
    Sfx.bug_munch = sfx.play()


def bag_alarm():
    chan = Sfx.bag_alarm_channel
    if not chan:
        chan = pygame.mixer.Channel(0)
        Sfx.bag_alarm_channel = chan
    if not chan.get_busy():
        chan.play(GameResource.get(GameResource.SOUND_BAG_ALARM))


def bag_alarm_silence():
    chan = Sfx.bag_alarm_channel
    if chan:
        chan.stop()


def earthquake():
    chan = Sfx.earthquake_channel
    if not chan:
        chan = pygame.mixer.Channel(1)
        Sfx.earthquake_channel = chan
    if not chan.get_busy():
        chan.play(GameResource.get(GameResource.SOUND_EARTHQUAKE))


def recharge():
    sfx_id = GameResource.SOUND_RECHARGE
    sfx = GameResource.get(sfx_id)
    sfx.play()


def play_intermission():
    if not settings.PLAY_MUSIC:
        return
    song_id = GameResource.SONG_1
    if Song.now_playing not in (None, song_id):
        # print('Fading action song: returning')
        Song.now_playing = None
        pygame.mixer.music.fadeout(1000)
        return
    if pygame.mixer.music.get_busy():
        # print('Busy: returning')
        return
    Song.now_playing = song_id
    song = GameResource.get(song_id)
    # print('Loading song: {}'.format(song.name))
    pygame.mixer.music.load(song.file_name)
    pygame.mixer.music.play()
    pygame.mixer.music.set_volume(song.volume)


def play_action_song():
    if not settings.PLAY_MUSIC:
        return
    if Song.now_playing == GameResource.SONG_1 and pygame.mixer.music.get_busy():
        # print('Fading intermission song: returning')
        Song.now_playing = None
        pygame.mixer.music.fadeout(250)
        return
    if pygame.mixer.music.get_busy():
        # print('Busy: returning')
        return
    song_id = GameResource.song_queue.pop(0)
    GameResource.song_queue.append(song_id)
    Song.now_playing = song_id
    song = GameResource.get(song_id)
    # print('Loading song: {}'.format(song.name))
    pygame.mixer.music.load(song.file_name)
    pygame.mixer.music.play()
    pygame.mixer.music.set_volume(song.volume)


if __name__ == '__main__':
    import os
    os.chdir('..')
    pygame.init()
    screen = pygame.display.set_mode((500, 50))
    screen_rect = screen.get_rect()
    clock = pygame.time.Clock()

    GameResource.load()
    play_intermission()

    was_playing = None
    running = True
    while running:
        for e in pygame.event.get():
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_ESCAPE:
                    # escape key: exit
                    pygame.mixer.music.fadeout(1000)
                    running = False
                else:
                    # any key: next song
                    pygame.mixer.music.fadeout(1000)
            elif e.type == pygame.QUIT:
                # window close: exit
                pygame.mixer.music.fadeout(1000)
                running = False

        if not pygame.mixer.music.get_busy():
            play_action_song()

        is_playing = Song.now_playing
        if was_playing != is_playing:
            pygame.display.set_caption(GameResource.get(is_playing).name)
            was_playing = is_playing

        clock.tick(1)
