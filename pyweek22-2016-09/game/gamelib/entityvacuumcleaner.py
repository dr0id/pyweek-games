# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek22-2016-09
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

from . import messagedispatcher
from . import messagetypes
from . import settings
from . import sound
from .entity import BaseEntity
from .pyknic.mathematics import Vec3 as Vec
from .pyknic.ai.statemachines import SimpleStateMachine
from .pyknic.timing import Timer

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2016"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 

logger = logging.getLogger(__name__)

#
#               +---+
#               |   |
#               |   V
#           +-----------+               +-----------+
#    +------| stuck     |-------------->| overheat  |
#    |      +-----------+               +-----------+
#    |           ^   |                          |
#    |           |   +--------------+           |
#    |           |                  |           V
#    |      +-----------+           +-->+-----------+
#    | +----| sucking   |<--------------| off       |<--+
#    | |    |           |-------------->|           |   |
#    | |    +-----------+               +-----------+   |
#    | |      |  ^  |                                   |
#    | |      |  |  |                                   |
#    | |      +--+  |                                   |
#    | |            |                                   |
#    | |            V                                   |
#    | |    +-----------+               +-----------+   |
#    | |    | full on   |<--------------| full off  |   |
#    | | +--|           |-------------->|           |   |
#    | | |  +-----------+               +-----------+   |
#    | | |     |  ^                         |           |
#    | | |     |  |                         |           |
#    | | |     +--+                         V           |
#    | | |  +-----------+               +-----------+   |
#    | | +->| bat depl. |               | change bag|---+
#    | +--->|           |               |           |
#    +----->|           |               |           |
#           +-----------+               +-----------+
#            |  ^  |  ^
#            |  |  |  |
#            +--+  +--+
#

class VcBaseState(object):

    @staticmethod
    def enter(sm):
        pass

    @staticmethod
    def exit(sm):
        pass

    @staticmethod
    def activate(sm):
        pass

    @staticmethod
    def deactivate(sm):
        pass

    @staticmethod
    def on_drain_energy(sm):
        pass

    @staticmethod
    def on_switch_on(sm):
        pass

    @staticmethod
    def on_switch_off(sm):
        pass

    @staticmethod
    def on_depleted(sm):
        pass

    @staticmethod
    def on_bag_full(sm):
        pass

    @staticmethod
    def on_exchange_bag(sm):
        pass

    @staticmethod
    def on_new_bag(sm):
        pass

    @staticmethod
    def on_overheat(sm):
        pass

    @staticmethod
    def on_stuck(sm):
        pass

    @staticmethod
    def on_heating(sm):
        pass

    @staticmethod
    def on_sucked_in(sm, bug):
        pass


class VcStateDepleted(VcBaseState):

    @staticmethod
    def enter(sm):
        sm.entity.strength = 0

    @staticmethod
    def on_switch_off(sm):
        # TODO: trigger audio?
        pass

    @staticmethod
    def on_switch_on(sm):
        # TODO: trigger audio?
        pass


class VcEnergyDraining(VcBaseState):

    @staticmethod
    def enter(sm):
        # drain initial amount of energy
        # if sm.entity.battery.handle_message(messagetypes.DRAIN_ENERGY, settings.VC_NORMAL_ENERGY_DRAIN):
        #     return
        sm.entity.battery_timer.start(settings.VC_ENERGY_DRAIN_INTERVAL)
        sm.entity.strength = settings.VC_STRENGTH

    @staticmethod
    def exit(sm):
        sm.entity.battery_timer.stop()

    @staticmethod
    def on_depleted(sm):
        sm.switch_state(VcStateDepleted)
        return


class VcChangeBag(VcBaseState):

    @staticmethod
    def enter(sm):
        sm.entity.battery_timer.start(settings.VC_CHANGE_BAG_DURATION)
        sm.entity.strength = 0

    @staticmethod
    def exit(sm):
        sm.entity.battery_timer.stop()

    @staticmethod
    def on_drain_energy(sm):
        sm.entity.bag.handle_message(messagetypes.BAG_RESET)
        sm.switch_state(VcStateOff)
        return


class VcStateBagFullOff(VcBaseState):

    @staticmethod
    def on_switch_on(sm):
        sm.switch_state(VcStateBagFullOn)
        return

    @staticmethod
    def on_bag_full(sm):
        sm.switch_state(VcChangeBag)
        return

    @staticmethod
    def on_exchange_bag(sm):
        sm.switch_state(VcChangeBag)
        return


class VcStateBagFullOn(VcEnergyDraining):

    @staticmethod
    def enter(sm):
        VcEnergyDraining.enter(sm)
        sm.entity.strength = 0

    @staticmethod
    def on_switch_off(sm):
        sm.switch_state(VcStateBagFullOff)
        return

    @staticmethod
    def on_drain_energy(sm):
        sm.entity.battery.handle_message(messagetypes.DRAIN_ENERGY, settings.VC_BAG_FULL_ENERGY_DRAIN)
        sm.entity.battery_timer.start()


class VcOverheated(VcBaseState):

    @staticmethod
    def enter(sm):
        sm.entity.overheat_timer.start(settings.VC_OVERHEAT_COOL_DOWN_DURATION)
        sm.entity.strength = 0

    @staticmethod
    def exit(sm):
        sm.entity.overheat_timer.stop()
        sm.entity.heat = 0

    @staticmethod
    def on_heating(sm):
        sm.switch_state(VcStateOff)
        return


class VcStuck(VcEnergyDraining):

    @staticmethod
    def enter(sm):
        VcEnergyDraining.enter(sm)
        sm.entity.overheat_timer.start(settings.VC_OVERHEAT_INTERVAL)

    @staticmethod
    def exit(sm):
        VcEnergyDraining.exit(sm)
        sm.entity.overheat_timer.stop()
        sm.entity.stuck_bug.handle_message(messagetypes.MOVE_NORMAL)
        sm.entity.stuck_bug = None
        sm.entity.heat = 0

    @staticmethod
    def on_switch_off(sm):
        sm.switch_state(VcStateOff)
        return

    @staticmethod
    def on_overheat(sm):
        sm.switch_state(VcOverheated)
        return

    @staticmethod
    def on_drain_energy(sm):
        sm.entity.battery.handle_message(messagetypes.DRAIN_ENERGY, settings.VC_STUCK_ENERGY_DRAIN)
        sm.entity.battery_timer.start()

    @staticmethod
    def on_heating(sm):
        sm.entity.heat += settings.VC_HEAT_INCREASE
        sm.entity.heat = settings.VC_OVERHEAT_LIMIT if sm.entity.heat > settings.VC_OVERHEAT_LIMIT else sm.entity.heat
        logger.info("vc heat {0} / {1}", sm.entity.heat, settings.VC_OVERHEAT_LIMIT)
        sm.entity.stuck_bug.handle_message(messagetypes.HEAT, sm.entity.heat)
        if sm.entity.stuck_bug.size < sm.entity.max_suck_size or sm.entity.stuck_bug.health <= 0:
            sm.entity.stuck_bug.handle_message(messagetypes.SUCKED_IN)
            sm.switch_state(VcStateOn)
            return
        if sm.entity.heat >= settings.VC_OVERHEAT_LIMIT:
            sm.switch_state(VcOverheated)
            return
        else:
            sm.entity.overheat_timer.start()


class VcStateOn(VcEnergyDraining):

    @staticmethod
    def on_switch_off(sm):
        sm.switch_state(VcStateOff)
        return

    @staticmethod
    def on_drain_energy(sm):
        sm.entity.battery.handle_message(messagetypes.DRAIN_ENERGY, settings.VC_NORMAL_ENERGY_DRAIN)
        sm.entity.battery_timer.start()

    @staticmethod
    def on_bag_full(sm):
        sm.switch_state(VcStateBagFullOn)
        return

    @staticmethod
    def on_stuck(sm):
        sm.switch_state(VcStuck)
        return

    @staticmethod
    def on_sucked_in(sm, bug):
        if bug.size > sm.entity.max_suck_size and bug.health > 0:
            bug.handle_message(messagetypes.STUCK)
            sm.entity.stuck_bug = bug
            sm.switch_state(VcStuck)
            return
        else:
            sm.entity.bag.handle_message(messagetypes.FILL_BAG, bug.size)
            bug.handle_message(messagetypes.SUCKED_IN)


class VcStateOff(VcBaseState):

    @staticmethod
    def enter(sm):
        sm.entity.strength = 0

    @staticmethod
    def on_switch_on(sm):
        sm.switch_state(VcStateOn)
        return

    @staticmethod
    def on_exchange_bag(sm):
        sm.switch_state(VcChangeBag)
        return


class Battery(BaseEntity):
    def __init__(self, energy, owner_id, id_to_use=None):
        super(Battery, self).__init__(None, id_to_use=None)
        self.capacity = energy
        self.energy = energy
        self.owner_id = owner_id

    def recharge(self):
        sound.recharge()
        self.energy = self.capacity
        messagedispatcher.messaging.send_message(self.owner_id, messagetypes.RECHARGED)

    def get_left_capacity(self):
        return self.energy / float(self.capacity)

    def handle_message(self, message, extra=None):
        """Override this in derived class to handle a message."""
        if message == messagetypes.DRAIN_ENERGY:
            self.energy -= extra
            logger.info("vc battery state {0}", self.energy)
            if self.energy <= 0:
                self.energy = 0
                messagedispatcher.messaging.send_message(self.owner_id, messagetypes.BATTERY_DEPLETED)
                return True
        return False  # indicates that the message was not handled


class Bag(BaseEntity):
    def __init__(self, capacity, owner_id, id_to_use=None):
        super(Bag, self).__init__(None, id_to_use=None)
        self.capacity = capacity
        self.owner_id = owner_id
        self.amount = 0

    def get_left_capacity(self):
        return (self.capacity - self.amount) / float(self.capacity)

    def update(self, dt):
        if self.get_left_capacity() <= 0.0:
            sound.bag_alarm()
        else:
            sound.bag_alarm_silence()

    def handle_message(self, message, extra=None):
        """Override this in derived class to handle a message."""
        if message == messagetypes.FILL_BAG:
            self.amount += extra
            if self.amount >= self.capacity:
                self.amount = self.capacity
                messagedispatcher.messaging.send_message(self.owner_id, messagetypes.BAG_FULL)
            return True
        elif message == messagetypes.BAG_RESET:
            self.amount = 0
            return True
        return False  # indicates that the message was not handled


class VacuumCleaner(BaseEntity):

    kind = settings.KIND_VC

    def __init__(self, aabb, max_suck_size=settings.VC_MAX_SUCK_SIZE):
        BaseEntity.__init__(self, aabb)
        self.pos = Vec(0, 0, 0)
        self.battery = Battery(settings.VC_NEW_BATTERY_ENERGY, self.id)
        self.bag = Bag(settings.VC_BAG_CAPACITY, self.id)
        self.overheat_timer = Timer(settings.VC_OVERHEAT_INTERVAL, name="vc overheat")
        self.overheat_timer.event_elapsed.add(self._on_overheat_timer_elapsed)
        self.battery_timer = Timer(settings.VC_ENERGY_DRAIN_INTERVAL, name="vc battery")
        self.battery_timer.event_elapsed.add(self._on_battery_timer_elapsed)
        self.sm = SimpleStateMachine(VcStateOff, self)
        self.strength = 0
        self.heat = 0
        self.max_suck_size = max_suck_size
        self.stuck_bug = None

    def _on_overheat_timer_elapsed(self, timer):
        self.sm.current_state.on_heating(self.sm)

    def _on_battery_timer_elapsed(self, timer):
        self.sm.current_state.on_drain_energy(self.sm)

    def get_strength(self):
        return self.strength #* self.bag.get_left_capacity()

    def update(self, dt):
        self.bag.update(dt)

    def handle_message(self, message, extra=None):
        if message == messagetypes.BAG_FULL:
            self.sm.current_state.on_bag_full(self.sm)
            return True
        elif message == messagetypes.BATTERY_DEPLETED:
            self.sm.current_state.on_depleted(self.sm)
            return True
        elif message == messagetypes.SUCKED_IN:
            bug = extra
            self.sm.current_state.on_sucked_in(self.sm, bug)
            return True
        elif message == messagetypes.RECHARGED:
            self.sm.switch_state(VcStateOff)
            return True
        return False  # not handled

    def handle_action(self, action, extra=None):
        if action == settings.ACTION_ACTIVATE_PRIMARY:
            self.sm.current_state.on_switch_on(self.sm)
        elif action == settings.ACTION_DEACTIVATE_PRIMARY:
            self.sm.current_state.on_switch_off(self.sm)
        elif action == settings.ACTION_EXCHANGE:
            self.sm.current_state.on_exchange_bag(self.sm)
        elif action == settings.ACTION_ACTIVATE_SECONDARY:
            self.sm.current_state.on_exchange_bag(self.sm)
        elif action == settings.ACTION_DEACTIVATE_SECONDARY:
            pass
        elif action == settings.ACTION_MOUSE_MOTION:
            self.pos.x = extra[0][0]
            self.pos.y = extra[0][1]
            self.aabb.center = self.pos.x, self.pos.y
            if self.stuck_bug:
                self.stuck_bug.pos.x = self.pos.x
                self.stuck_bug.pos.y = self.pos.y

