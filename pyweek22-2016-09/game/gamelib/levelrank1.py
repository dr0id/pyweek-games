"""Rank 1 - simple ants and speedy ants

===============================
This set contains levels 0 - 5.
===============================
"""

from .levelcommon import *


level_data = [
    {
        'tuned': True,
        'name': 'Rank 1',  #: 2 corners',
        'bug_spigots': [
            {'config': a0, 'count': 10, 'interval': 4.0, 'pos': TL, 'delay': 0.0},
            {'config': a0, 'count': 10, 'interval': 4.0, 'pos': TL, 'delay': 2.0},
            {'config': a0, 'count': 10, 'interval': 4.0, 'pos': BR, 'delay': 0.0},
            {'config': a0, 'count': 10, 'interval': 4.0, 'pos': BR, 'delay': 2.0},
        ],
        'potatoes': [
            {"pos": (W * 0.45, H * 0.45), "amount": 100, 'w': 100, 'h': 75},
        ],
    },
    {
        'tuned': True,
        'name': 'Rank 1',  #: : 3 corners',
        'bug_spigots': [
            {'config': a0, 'count': 7, 'interval': 4.0, 'pos': TL, 'delay': 0.0},
            {'config': ar, 'count': 9, 'interval': 4.0, 'pos': TL, 'delay': 6.0},
            {'config': a0, 'count': 7, 'interval': 4.0, 'pos': BR, 'delay': 0.0},
            {'config': ar, 'count': 9, 'interval': 4.0, 'pos': BR, 'delay': 6.0},
            {'config': a0, 'count': 7, 'interval': 4.0, 'pos': BL, 'delay': 0.0},
            {'config': ar, 'count': 9, 'interval': 4.0, 'pos': BL, 'delay': 6.0},
        ],
        'potatoes': [
            {"pos": (W * 0.65, H * 0.45), "amount": 100, 'w': 100, 'h': 75},
        ],
    },
    {
        'tuned': True,
        'name': 'Rank 1',  #: : 4 corners',
        'bug_spigots': [
            {'config': a0, 'count': 5, 'interval': 4.0, 'pos': TL, 'delay': 0.0},
            {'config': ar, 'count': 7, 'interval': 4.0, 'pos': TL, 'delay': 4.0},
            {'config': a0, 'count': 5, 'interval': 4.0, 'pos': BR, 'delay': 0.0},
            {'config': ar, 'count': 7, 'interval': 4.0, 'pos': BR, 'delay': 4.0},
            {'config': a0, 'count': 5, 'interval': 4.0, 'pos': BL, 'delay': 0.0},
            {'config': ar, 'count': 7, 'interval': 4.0, 'pos': BL, 'delay': 4.0},
            {'config': a0, 'count': 5, 'interval': 4.0, 'pos': TR, 'delay': 0.0},
            {'config': ar, 'count': 7, 'interval': 4.0, 'pos': TR, 'delay': 4.0},
        ],
        'potatoes': [
            {"pos": (W * 0.35, H * 0.45), "amount": 100, 'w': 100, 'h': 75},
        ],
    },
    {
        'tuned': True,
        'name': 'Rank 1',  #: : 2 sides',
        'bug_spigots': [
            {'config': a0, 'count': 10, 'interval': 4.0, 'pos': MT, 'delay': 0.0},
            {'config': ar, 'count': 5, 'interval': 8.0, 'pos': MT, 'delay': 2.0},
            {'config': a0, 'count': 10, 'interval': 4.0, 'pos': MB, 'delay': 0.0},
            {'config': ar, 'count': 5, 'interval': 8.0, 'pos': MB, 'delay': 2.0},
            {'config': ar, 'count': 3, 'interval': 3.0, 'pos': (W*0.8, H*0.5), 'delay': 20.0, 'rumble': True},
        ],
        'potatoes': [
            {"pos": (W * 0.45, H * 0.45), "amount": 100, 'w': 100, 'h': 75},
        ],
    },
    {
        'tuned': True,
        'name': 'Rank 1',  #: : 3 sides',
        'bug_spigots': [
            {'config': a0, 'count': 7, 'interval': 4.0, 'pos': MT, 'delay': 0.0},
            {'config': ar, 'count': 9, 'interval': 4.0, 'pos': MT, 'delay': 6.0},
            {'config': a0, 'count': 7, 'interval': 4.0, 'pos': MB, 'delay': 0.0},
            {'config': ar, 'count': 9, 'interval': 4.0, 'pos': MB, 'delay': 6.0},
            {'config': a0, 'count': 7, 'interval': 4.0, 'pos': MR, 'delay': 0.0},
            {'config': ar, 'count': 9, 'interval': 4.0, 'pos': MR, 'delay': 6.0},
            {'config': ar, 'count': 5, 'interval': 3.0, 'pos': (W*0.2, H*0.5), 'delay': 20.0, 'rumble': True},
        ],
        'potatoes': [
            {"pos": (W * 0.55, H * 0.45), "amount": 100, 'w': 100, 'h': 75},
        ],
    },
    {
        'tuned': False,
        'name': 'Rank 1',  #: : 4 sides',
        'bug_spigots': [
            {'config': a0, 'count': 5, 'interval': 4.0, 'pos': MT, 'delay': 0.0},
            {'config': ar, 'count': 7, 'interval': 4.0, 'pos': MT, 'delay': 4.0},
            {'config': a0, 'count': 5, 'interval': 4.0, 'pos': MB, 'delay': 0.0},
            {'config': ar, 'count': 7, 'interval': 4.0, 'pos': MB, 'delay': 4.0},
            {'config': a0, 'count': 5, 'interval': 4.0, 'pos': MR, 'delay': 0.0},
            {'config': ar, 'count': 7, 'interval': 4.0, 'pos': MR, 'delay': 4.0},
            {'config': a0, 'count': 5, 'interval': 4.0, 'pos': ML, 'delay': 0.0},
            {'config': ar, 'count': 7, 'interval': 4.0, 'pos': ML, 'delay': 4.0},
            {'config': ar, 'count': 5, 'interval': 3.0, 'pos': (W*0.6, H*0.4), 'delay': 20.0, 'rumble': True},
            {'config': ar, 'count': 5, 'interval': 3.0, 'pos': (W*0.6, H*0.6), 'delay': 24.0, 'rumble': True},
        ],
        'potatoes': [
            {"pos": (W * 0.35, H * 0.45), "amount": 100, 'w': 100, 'h': 75},
        ],
    },
]
