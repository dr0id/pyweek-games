# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_spritesheetlib
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
joinsprites.py

usage: sprite_joiner.py [--horiz|--vert] outfile infiles...

The default behavior is to join files horizontally in a row.

outfile must have a supported image format extension.

infiles can be filenames or globs.


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function
import sys
import traceback

try:
    __version__ = '1.0.1.0'

    # for easy comparison as in sys.version_info but digits only
    __version_info__ = tuple([int(d) for d in __version__.split('.')])

    __author__ = "Gummbum"
    __email__ = "dr0iddr0id {at} gmail [dot] com"
    __copyright__ = "Gummbum @ 2015"
    __credits__ = ["Gummbum"]  # list of contributors
    __maintainer__ = "DR0ID"
    __license__ = "New BSD license"

    import glob
    import pygame


    def main():
        behavior = '--horize'
        if sys.argv[1].startswith('--'):
            behavior = sys.argv[1]
            sys.argv.pop(1)
        outfile = sys.argv[1]
        fileargs = sys.argv[2:]

        if not fileargs:
            print('usage: sprite_joiner.py outfile infiles...')
            sys.exit(1)

        infiles = []
        for pattern in fileargs:
            names = glob.glob(pattern)
            assert len(names), 'error: file not found: {}'.format(pattern)
            infiles.extend(names)

        screen = pygame.display.set_mode((400, 400))
        images = []
        image_names = {}
        for name in infiles:
            print('loading {}'.format(name))
            image = pygame.image.load(name)
            images.append(image)
            image_names[image] = name

        w = h = 0
        flags = None
        for image in images:
            w = max(w, image.get_width())
            h = max(h, image.get_height())
            if flags is None:
                flags = image.get_flags()
            else:
                try:
                    assert flags == image.get_flags()
                except AssertionError:
                    print("error: image has different display flags: {}".format(image_names[image]))
                    print("error: don't know what I should do, so giving up")
                    sys.exit(1)

        if behavior == '--horize':
            sheet = pygame.Surface((w * len(images), h), flags, image)
            for i, image in enumerate(images):
                sheet.blit(image, (i * w, 0))
        else:
            sheet = pygame.Surface((w, h * len(images)), flags, image)
            for i, image in enumerate(images):
                sheet.blit(image, (0, i * h))

        print('saving {}'.format(outfile))
        pygame.image.save(sheet, outfile)


    if __name__ == '__main__':  # pragma: no cover
        main()

except Exception as ex:
    # trying to catch any exceptions and print them out in a way that the user will see them!
    sys.stderr.write("Error:")
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    sys.stderr.write(str(ex))
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback.print_tb(exc_traceback, file=sys.stderr)
    sys.stderr.write('\n')
    sys.stderr.write('Press any key... ')
    sys.stdin.readline()
