#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of spritesheetlib
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
# * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The spritesheet_mask_generator is a tool to generate a spritesheet definition. It also can create a corresponding
image that can be used as a spritesheet template (for drawing your own sprites). Internally it uses the spritesheet
lib for all manipulations. What follows are the command line arguments that are implemented so far.

::

        usage: SpriteSheetMaskGenerator [-h] [-s SPACING] [-m MARGIN] [-v | -q]
                                        [-f | --dry-run] [-i] [-p PROPERTIES]
                                        [--version]
                                        {create_from_file,create_grid} ... filename

        Create a sprite definition file and optionally a image file, e.g. 'x.png.sdef'
        and 'x.png'. It uses pygame (>= 1.8) to create the file.Sub command help
        available, e.g. 'spritesheet_mask_generator.py create_grid -h'. Place the
        arguments at the same position as described in this help.

        positional arguments:
          {create_from_file,create_grid}
                                This are the available commands. Either create a
                                sprite definition file with a checkers pattern or
                                generate a new sprite definition from a existing
                                sprite definition file. In both cases optionally an
                                image can be created too.
            create_from_file    This command creates a new sprite definition based on
                                the given sprite definition file. Using the spacing
                                and margin arguments it can be transformed. If none is
                                set, then a copy will be generated.
            create_grid         Creates a checkers board grid pattern according to the
                                parameters.
          filename              A filename to store the generated sprite sheet mask.
                                Should be in the form of 'out.png.sdef' or
                                'out.bmp.sdef'. Follow formats are supported by
                                pygame: BMP, TGA, PNG, JPEG. If the filename extension
                                is unrecognized it will default to TGA. Both TGA, and
                                BMP file formats create uncompressed files.

        optional arguments:
          -h, --help            show this help message and exit
          -s SPACING, --spacing SPACING
                                The spacing in pixels between the sprites, greater or
                                equal to 0. If set to None, no sprites are moved
                                because spacing is then disabled.Otherwise if used
                                with an existing sprite definition file it may
                                separate sprites. (default: None)
          -m MARGIN, --margin MARGIN
                                The margin around the tiles in pixels in this image,
                                greater or equal to 0. (default: 0)
          -v, --verbose         Verbose (debug) logging. no arg: logging.ERROR, '-v':
                                logging.WARNING,'-vv': logging.INFO,'-vvv':
                                logging.DEBUG '--verbose': this is the same as '-v'
                                which is logging.WARNING. The default log level is
                                logging.ERROR. (default: 0)
          -q, --quiet           Silent mode, only log warnings. (default: None)
          -f, --force           Try to override existing file. (default: False)
          --dry-run             Do not write anything. (default: False)
          -i, --save_to_image   Generates and saves the image file. (default: False)
          -p PROPERTIES, --properties PROPERTIES
                                Provide a string describing a dictionary or a file
                                name of a json file. In both cases they should provide
                                a dictionary containing the properties that should be
                                set for some or all sprites. The string (dictionary)
                                has following structure:
                                {'gid_key':{'prop_key':'prop_value'}}. MAKE SURE TO
                                REMOVE ANY WHITESPACES! (otherwise you get a strange
                                arg parse error). In a big spritesheet it might be a
                                long list therefore the slice or range notation
                                (start, stop, step) is accepted for the gid_key (think
                                of it as a slice of range(N)[slice]), e.g.:'1' => [1];
                                '0:' => [0, 1, 2, 3, ..., N-1]; '0:3' => [0, 1, 2];
                                '0:10:2' => [0, 2, 4, 6, 8]; '0::2' => [0, 2, 4, 6,
                                ..., N-2] (if N even, otherwise N-1); ':5' => [0, 1,
                                2, 3, 4]; '::2' => see '0::2'; ':' => [0, 1, 2, ...,
                                N-1]; Negative steps are not allowed as ':' in keys
                                other than gid range notation.Note that when putting
                                the dict into a json file, the gid_key need to be
                                strings. Also the same gid might be addressed in
                                multiple keys with following limitation:the the
                                defined dictionaries will be merged as long they don't
                                have the same key defined. Here an example:props={'1':
                                {'k1': v1}, '0:': {'k2' : v2}} will generate
                                internally following: {'1': {'k1': v1, 'k2': v2}, '2':
                                {'k2': v2}, '2': {'k2': v2}, ...'N': {'k2': v2}. The
                                following will all generate errors: props={'1': {'k1':
                                v1}, '0:': {'k1': v3, 'k2' : v2}} <= 'k1' is
                                duplicated for gid '1' or props={'1': {'k1': v1, 'k1':
                                vx}} <= duplicated key 'k1' for gid '1' (default:
                                None)
          --version             show program's version number and exit







Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function
import sys
import traceback

sys.path.insert(0, '../')  # find the lib is only the repository has been cloned


try:
    import json
    import argparse
    import logging
    from spritesheetlib import SpritesheetLib10
    from spritesheetlib.spritesheetmaskgeneratorlib import SpriteSheetMaskGenerator, DictParseError, __version__, __version_info__

    __author__ = "DR0ID"
    __email__ = "dr0iddr0id {at} gmail [dot] com"
    __copyright__ = "DR0ID @ 2015"
    __credits__ = ["DR0ID"]  # list of contributors
    __maintainer__ = "DR0ID"
    __license__ = "New BSD license"

    def main(args=None):
        """
        The main  function.

        :param args: Argument list to use. This is a list of strings like sys.argv. See -h for complete argument list.
            Default=None so the sys.args are used.
        """
        logging.basicConfig(level=logging.DEBUG)  # make sure that the root logger has DEBUG level set
        logger = logging.getLogger(__name__)
        logger.level = logging.INFO
        sprite_sheet_mask_generator = SpriteSheetMaskGenerator(SpritesheetLib10(logger), logger)
        sprite_sheet_mask_generator.main(args)

    if __name__ == '__main__':  # pragma: no cover
        main()

except Exception as ex:
    # trying to catch any exceptions and print them out in a way that the user will see them!
    sys.stderr.write("Error:")
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    sys.stderr.write(str(ex))
    sys.stderr.write('\n')
    sys.stderr.write('\n')
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback.print_tb(exc_traceback, file=sys.stderr)
    sys.stderr.write('\n')
    sys.stderr.write('Press any key... ')
    sys.stdin.readline()
