# -*- coding: utf-8 -*-
"""
TODO: docstring
"""
from __future__ import division, print_function

# do not use __file__ because it is not set if using py2exe
import logging
import os

import pygame
import sys

from . import pyknic
from .pyknic import settings as pyknic_settings
from . import settings
from . import entitybug
from . import gameresource
from . import ptext

ptext.FONT_NAME_TEMPLATE = 'data/font/%s.TTF'

# TODO: clean up imports so they work as usual
# this has to be here so the contextintro import works as expected

# pyknicpygame.init()

# from contextgameplay import GamePlayContext

logger = logging.getLogger("pyknic.main")
logger.setLevel(pyknic_settings.log_logger_level)

pyknic_settings.appdir = sys.path[0]

def main():
    # TODO: this should go into config
    logging.basicConfig(level=logging.DEBUG)

    # TODO: this should go into config
    os.environ['SDL_VIDEO_CENTERED'] = '1'
    # TODO: this should go into config
    pygame.mixer.pre_init(frequency=settings.MIXER_FREQUENCY, buffer=settings.MIXER_BUFFER_SIZE)
    pygame.init()
    pygame.mixer.set_reserved(settings.MIXER_RESERVED_CHANNELS)
    # TODO: this should go into param of init method
    pygame.display.set_caption(settings.CAPTION)
    # TODO: this should go into param of init method
    icon = pygame.image.load("icon.png")
    # TODO: this should go into pyknicpygame init method
    pygame.display.set_icon(icon)
    pygame.mixer.set_reserved(2)

    # TODO: this should go into config or param values
    screen = pygame.display.set_mode(settings.SCREEN_SIZE, settings.FLAGS, settings.BIT_DEPTH)

    clock = pygame.time.Clock()
    settings.clock = clock

    gameresource.GameResource.load()

    # TODO: should this be done by the context entry/exit (unload)?
    # Load default resources
    entitybug.init()

    # TODO: this should go into param of init method (first context)
    from .contextoutro import ContextOutro
    pyknic.context.push(ContextOutro())

    if settings.skip_to == 'game':
        from .contextgameplay import GamePlayContext
        pyknic.context.push(GamePlayContext(settings.starting_level))
    else:
        from .contextintro import ContextIntro
        pyknic.context.push(ContextIntro())

    # TODO: this should go into application class with a run() method
    lock_stepper = pyknic.timing.LockStepper()

    pyknic.context.set_deferred_mode(True)  # avoid routing problems, do stack operations only at update
    context_len = pyknic.context.length
    context_top = pyknic.context.top
    context_update = pyknic.context.update

    def _update_top_context(ls, dt, simt):
        context_top().update(dt) if context_top() else None
        context_update()

    lock_stepper.event_integrate.add(_update_top_context)

    # if __debug__:
    scheduler = pyknic.timing.Scheduler()
    scheduler.schedule(_print_fps, 2, 0, clock)

    def _update_scheduler(ls, dt, simt):
        scheduler.update(dt)

    lock_stepper.event_integrate.add(_update_scheduler)
    alpha = 1.0

    while context_len():
        # limit the fps
        dt = clock.tick() / 1000.0  # convert to seconds
        # if settings.PLAY_MUSIC:
        #     # loop intro song while playing intro
        #     sound.roll_songs(context_top() is context_intro)
        context_top().draw(screen, do_flip=True, interpolation_factor=alpha)
        alpha = lock_stepper.update(dt, timestep_seconds=settings.SIM_TIME_STEP)
        # lock_stepper.update(dt, timestep_seconds=settings.SIM_TIME_STEP)
        context_update()
        # if __debug__:
        #     fps = clock.get_fps()
        #     # if fps < 60:
        #     logger.debug('fps: %s, %s', str(fps), alpha)

    try:
        pygame.mixer.music.fadeout(2000)
        while pygame.mixer.music.get_busy():
            pygame.time.wait(100)
    except IndexError:
        pass

    pygame.quit()


def _print_fps(clock):
    if settings.PRINT_FPS:
        fps = clock.get_fps()
        logger.debug('fps: %s', str(fps))
    return 1  # return next interval

# this is needed in order to work with py2exe
if __name__ == '__main__':
    main()
