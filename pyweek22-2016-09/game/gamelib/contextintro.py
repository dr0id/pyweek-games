# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek22-2016-09
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import pygame
from pygame.locals import Color

from . import pyknic
from .pyknic.pygame.context.effects import TopleftBottomrightDiagonalSweep
from .pyknic.mathematics import Point2
from .pyknic.tweening import Tweener
from .settings import SCREEN_SIZE

from .gameresource import GameResource
from . import settings
from . import sound
from . import ptext

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2016"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 


NEW = 'control', 2 << 0
MORE = 'control', 2 << 1
WAIT = 'control', 2 << 2
END = 'control', 2 << 3
SCREENS = [
    NEW,
    "Peace fled on the day the ants invaded.",
    "Most ran screaming for the hills. A few determined to hold out: but one can't eat determination. They, too are gone.",
    "The de facto king of a desolate heap, only I remain. While I still got these last bags of potato chips I aim to keep it that way.",
    "Only two things haven't changed. One: ants suck. And two: there ain't enough potato chips to go around.",
    WAIT,
    NEW,
    "To mount a solid defense you need walls, weapons, and wit.",
    "I got walls, but they come in through the cracks. I plugged those and they made new ones. I broke all the flyswatters, spatulas, wooden spoons, and hammers on the first day. And I got this blister.",
    "Then I found a thing of beauty in the closet. This lady, the T-1000 Turbo Portable Vacuum, with bio-fuel conversion kit...", GameResource.IMG_VACUUM,
    "Power...we lost the cloud on the first night. I can't remember my last hot shower... The T-1000's conversion kit generates a trickle charge from organic material. She auto-charges during downtime. Fuel sources at hand: potato chips...or ants.",
    WAIT,
    NEW,
    "So here's what I learned.",
    "The ants come in waves. I guess they can only make 'em so fast, heh. One day soon I'll get to their queen.",
    'When the ants come in, point this remote control at the vacuum...', GameResource.IMG_MOUSE,
    'Press the LMB to spin up the suction...', GameResource.IMG_LMB,
    "When the hopper is full of bugs press the RMB and hold it open until it's empty...", GameResource.IMG_RMB,
    "Don't worry. You WILL know when the hopper is full. Just remember: LMB, RMB.",
    WAIT,
    NEW,
    "In long battles I've run out of juice and had to get physical. When you're in the lurch, run that vacuum nozzle repeatedly over the bugs to smash 'em. She can take it.",
    "Some of the big fat ones get stuck. The motor labors but it gets 'em after a few moments. If the action gets too hot I just drag them aside with the suction.",
    "So far I've had enough rest between attacks to let the battery recharge. It's 4:00 AM, about time for another one...",
    # NEW,
    # "",
    # "".format(settings.mod_key_to_text[settings.MOD_KEY_SECONDARY_FIRE_MODE].upper(), settings.key_to_text[settings.KEY_SECONDARY_FIRE_MODE][1].upper()),
    # '',
    WAIT,
    END,
]


class ContextIntro(pyknic.context.Context):
    def __init__(self):
        self.tweener = Tweener()
        self.scheduler = pyknic.timing.Scheduler()
        sw, sh = SCREEN_SIZE
        self.renderer = pyknic.pygame.spritesystem.DefaultRenderer()
        cam = pyknic.pygame.spritesystem.Camera(pygame.Rect(0, 0, sw, sh), position=Point2(0, 0))
        self.cam = cam

        self.sprites = []
        self.doing = NEW

        self.background = Color('brown')
        self.font_args = settings.HILIGHT_FONT_COLOR

        # key_text = settings.key_to_text[settings.KEY_MOVE_NEXT][1]
        # self.spacebar = ptext.getsurf('[{0}]'.format(key_text.upper()), 'BIOST', 42, **self.font_args)
        key_text = '{0}'.format([settings.key_to_text[k][1].upper() for k in settings.KEY_MOVE_NEXT]).replace("'", '')
        self.spacebar = ptext.getsurf(key_text, 'BIOST', 42, **self.font_args)
        self.spacebar_rect = self.spacebar.get_rect(bottomright=settings.SCREEN_SIZE)

        self.orig_scr = id(pygame.display.get_surface())
        self.old_screen = pygame.display.get_surface().copy()
        self.transitioning = False
        self.screens = [None, None]

    def update(self, delta_time):
        """
        Update the game.
        :param delta_time: time passed in last time step (game time)
        :return:
        """
        sound.play_action_song()
        self._handle_events()
        self.tweener.update(delta_time)
        self.scheduler.update(delta_time)
        self.screens[0] = None
        self.screens[1] = None

        if self.doing == NEW:
            # print('NEW')
            x = 32
            y = 0
            del self.sprites[:]
            while SCREENS:
                item = SCREENS.pop(0)
                if item in (WAIT, MORE):
                    # print('new doing', item)
                    self.doing = item
                    break
                elif isinstance(item, str):
                    # print('new text', item)
                    y += 32
                    sprite = pygame.sprite.Sprite()
                    sprite.image = ptext.getsurf(
                        item, 'BIOST', 42, width=settings.SCREEN_WIDTH * 2/3, **self.font_args)
                    sprite.rect = sprite.image.get_rect(x=x, y=y)
                    y += sprite.rect.h
                    self.sprites.append(sprite)
                else:
                    # print('new image', item)
                    text_rect = self.sprites[-1].rect
                    sprite = pygame.sprite.Sprite()
                    sprite.image = GameResource.get(item)
                    sprite.rect = sprite.image.get_rect(x=text_rect.right + 32, centery=text_rect.centery)
                    self.sprites.append(sprite)
        elif self.doing == WAIT:
            pass
        elif self.doing == MORE:
            pass
        elif self.doing == END:
            self.pop(1000, do_resume=False)  # remove everything
            from .contextoutro import ContextOutro
            pyknic.context.push(ContextOutro())
            from .contextgameplay import GamePlayContext
            pyknic.context.push(GamePlayContext(settings.starting_level))

    def _handle_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.pop()
            elif event.type == pygame.KEYDOWN:
                if event.key in (pygame.K_RETURN, pygame.K_SPACE):
                    if self.doing == WAIT:
                        self.transitioning = True
                        pygame.event.clear()
                        self.exchange(self, TopleftBottomrightDiagonalSweep(SCREEN_SIZE))
                        return
                elif event.key == settings.KEY_CHEAT_EXIT:
                    self.doing = END

    def draw(self, screen, do_flip=True, interpolation_factor=1.0):
        # TODO: big hack. if you lookin in here, this is not how it should be done :)
        # if id(screen) != self.orig_scr:
        #     print('old={} new={}'.format(self.orig_scr, id(screen)))
        #     quit()
        if id(screen) not in self.screens:
            if self.screens[0] is None:
                self.screens[0] = id(screen)
            else:
                self.screens[1] = id(screen)

        # self.renderer.draw(screen, self.cam, fill_color=(127, 127, 127), do_flip=False, interpolation_factor=1.0)

        if self.orig_scr != id(screen):
            pygame.event.clear()

        if self.orig_scr == id(screen) or id(screen) == self.screens[1]:
            screen.fill(self.background)

            for sprite in self.sprites:
                screen.blit(sprite.image, sprite.rect)

            screen.blit(self.spacebar, self.spacebar_rect)
        else:
            screen.blit(self.old_screen, (0, 0))

        if do_flip:
            pygame.display.flip()

    def enter(self):
        self.doing = SCREENS.pop(0)
        if self.transitioning:
            self.old_screen.blit(pygame.display.get_surface(), (0, 0))
            self.update(0)

    #     img = pygame.image.load('data/image/credits.png')
    #     spr = Sprite(img, Point2(0, 0), name='context credits')
    #     self.renderer.add_sprite(spr)

    def exit(self):
        pass
