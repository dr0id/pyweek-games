"""Rank 4 - approx 50 ants, 20 roaches
"""

from .levelcommon import *


level_data = [
    {
        'name': 'Thanks for playing',
        'bug_spigots': [
            {'config': a0, 'count': 8, 'interval': 2.0, 'pos': TL, 'delay': 0.0},
            {'config': a0, 'count': 8, 'interval': 2.0, 'pos': BR, 'delay': 0.0},
            {'config': a0, 'count': 8, 'interval': 2.0, 'pos': TR, 'delay': 0.0},
            {'config': a0, 'count': 8, 'interval': 2.0, 'pos': BL, 'delay': 0.0},
            {'config': a0, 'count': 8, 'interval': 2.0, 'pos': MT, 'delay': 0.0},
            {'config': a0, 'count': 8, 'interval': 2.0, 'pos': MB, 'delay': 0.0},
            {'config': a0, 'count': 8, 'interval': 2.0, 'pos': ML, 'delay': 0.0},
            {'config': a0, 'count': 8, 'interval': 2.0, 'pos': MR, 'delay': 0.0},
        ],
        'potatoes': [
            {'pos': (410.12, 431.83), 'amount': 1000000, 'w': 100, 'h': 75},
            {'pos': (538.16, 311.18), 'amount': 1000000, 'w': 100, 'h': 75},
            {'pos': (568.78, 517.87), 'amount': 1000000, 'w': 100, 'h': 75},
            {'pos': (505.29, 389.03), 'amount': 1000000, 'w': 100, 'h': 75},
            {'pos': (475.25, 395.16), 'amount': 1000000, 'w': 100, 'h': 75},
            {'pos': (383.55, 378.4), 'amount': 1000000, 'w': 100, 'h': 75},
            {'pos': (333.0, 528.16), 'amount': 1000000, 'w': 100, 'h': 75},
            {'pos': (536.74, 310.89), 'amount': 1000000, 'w': 100, 'h': 75},
            {'pos': (335.07, 346.07), 'amount': 1000000, 'w': 100, 'h': 75},
            {'pos': (367.46, 332.56), 'amount': 1000000, 'w': 100, 'h': 75},
            {'pos': (346.31, 461.61), 'amount': 1000000, 'w': 100, 'h': 75},
            {'pos': (543.67, 490.95), 'amount': 1000000, 'w': 100, 'h': 75},
            {'pos': (480.34, 321.64), 'amount': 1000000, 'w': 100, 'h': 75},
            {'pos': (546.69, 527.2), 'amount': 1000000, 'w': 100, 'h': 75},
            {'pos': (490.73, 520.35), 'amount': 1000000, 'w': 100, 'h': 75},
            {'pos': (575.5, 423.96), 'amount': 1000000, 'w': 100, 'h': 75},
            {'pos': (432.31, 307.99), 'amount': 1000000, 'w': 100, 'h': 75},
            {'pos': (442.8, 420.45), 'amount': 1000000, 'w': 100, 'h': 75},
            {'pos': (576.16, 471.74), 'amount': 1000000, 'w': 100, 'h': 75},
            {'pos': (540.15, 415.99), 'amount': 1000000, 'w': 100, 'h': 75},
            {'pos': (572.06, 325.45), 'amount': 1000000, 'w': 100, 'h': 75},
            {'pos': (562.33, 429.69), 'amount': 1000000, 'w': 100, 'h': 75},
            {'pos': (365.59, 481.83), 'amount': 1000000, 'w': 100, 'h': 75},
            {'pos': (440.35, 478.88), 'amount': 1000000, 'w': 100, 'h': 75},
        ],
    },
]

# _plist = level_data[0]['potatoes']
# for m in range(6):
#     for n in range(3, 7):
#         x = round(W * (0.3 + 0.3 * random.random()), 2)
#         y = round(H * (0.4 + 0.3 * random.random()), 2)
#         _plist.append(
#             {'pos': (x, y), 'amount': 1000000, 'w': 100, 'h': 75},
#         )
#         print("{'pos': (" + "{}, {}".format(x, y) + "), 'amount': 1000000, 'w': 100, 'h': 75},")
