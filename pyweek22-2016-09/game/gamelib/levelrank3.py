"""Rank 3 - approx 40 ants, 16 roaches

================================
This set contains levels 12 - ?.
================================
"""

from .levelcommon import *


level_data = [
    {
        'name': 'Rank 3 - BOSS!!!',  #: : 4 corners 2 sides',
        'bug_spigots': [
            {'config': rb, 'count': 1, 'interval': 7.0, 'pos': (W * 0.7, H * 0.7), 'delay': 0.0},
        ],
        'potatoes': [
            {"pos": (W * 0.1, H * 0.1), "amount": 100, 'w': 100, 'h': 75},
        ],
    },
]
