import random
import logging

import pygame
from pygame import Color

from . import messagedispatcher
from . import messagetypes
from . import entity
from . import settings
from . import sound
from . import pyknic
from .entityregistry import entity_registry
from .pyknic import tweening
from .pyknic.mathematics import Vec3 as Vec, Vec3
from .pyknic.ai.statemachines import SimpleStateMachine
from .pyknic.timing import Timer
import time

logger = logging.getLogger(__name__)

ant_img_id = None
roach_img_id = None
spider_img_id = None
fly_img_id = None


class EntityBug(entity.BaseEntity):

    def __init__(self, image_id, size, health):
        self.image_id = image_id
        _size = size // 10 / 10.0 + 1 * 80
        self.rect = pygame.Rect(0, 0, _size, _size)
        entity.BaseEntity.__init__(self, self.rect)  # TODO: rect is actually wrong here...!
        self.anchor = 'center'
        self._pos = Vec(0, 0, 0)
        self.target_id = None
        self.direction = Vec(1, 0, 0)
        self.speed = 0

        self.suck_force = 10
        self.cling_force = 5
        self.size = size
        self.health = health

    def get_image(self):
        return pyknic.pygame.resource.resources.get_resource(self.image_id)
    image = property(get_image)

    def get_pos(self):
        return self._pos
    def set_pos(self, p):
        self._pos = p
    pos = property(get_pos, set_pos)

    def move(self, x=0, y=0):
        self.pos = x, y

    def update(self, dt):
        setattr(self.rect, self.anchor, self.pos[:2])


class BugBaseState(object):

    @staticmethod
    def enter(sm):
        pass

    @staticmethod
    def exit(sm):
        pass

    @staticmethod
    def activate(sm):
        pass

    @staticmethod
    def deactivate(sm):
        pass

    @staticmethod
    def on_timer_elapsed(sm):
        pass

    @staticmethod
    def on_move_normal(sm):
        pass

    @staticmethod
    def on_cling(sm):
        pass

    @staticmethod
    def on_sucked_in(sm):
        sm.switch_state(AntSuckedInState)
        return

    @staticmethod
    def on_stuck(sm):
        sm.switch_state(AntStuckState)
        return

    @staticmethod
    def update(sm, dt):
        pass

    @staticmethod
    def on_heat(sm, heat):
        pass

    @staticmethod
    def on_eat(sm, potatoes_id):
        sm.switch_state(AntEatState)
        return

    @staticmethod
    def on_crush(sm):
        sm.switch_state(AntCrushedState)


class AntSuckedInState(BugBaseState):

    @staticmethod
    def enter(sm):
        sound.bug_slurp()
        duration = settings.VC_BUG_SUCK_IN_DURATION
        sm.entity.timer.start(duration)
        world = entity_registry.get_by_id(settings.WORLD_ID)
        world.tweener.create_tween(sm.entity.pos, 'x', sm.entity.pos.x, world.vc.pos.x - sm.entity.pos.x, duration, tweening.ease_in_sine)
        world.tweener.create_tween(sm.entity.pos, 'y', sm.entity.pos.y, world.vc.pos.y - sm.entity.pos.y, duration, tweening.ease_in_sine)

    @staticmethod
    def on_sucked_in(sm):
        pass

    @staticmethod
    def exit(sm):
        sm.entity.timer.stop()

    @staticmethod
    def on_timer_elapsed(sm):
        # remove from world
        msg = messagedispatcher.Message(sm.entity.id, settings.WORLD_ID, messagetypes.SUCKED_IN)
        messagedispatcher.messaging.send_message(settings.WORLD_ID, msg)


class AntStuckState(BugBaseState):

    @staticmethod
    def enter(sm):
        sm.entity.pos.z = 9

    @staticmethod
    def exit(sm):
        sm.entity.pos.z = 0

    @staticmethod
    def on_move_normal(sm):
        sm.switch_state(AntInitialState)
        return

    @staticmethod
    def on_heat(sm, heat):
        if settings.current_level.level_number == 12:
            sm.entity.health -= 1
        else:
            sm.entity.health *= int(((settings.VC_OVERHEAT_LIMIT - heat) / float(settings.VC_OVERHEAT_LIMIT) + 0.1))
        logger.debug("bug size {}", sm.entity.health)

    @staticmethod
    def on_eat(sm, potatoes_id):
        pass


class AntClingState(BugBaseState):

    @staticmethod
    def enter(sm):
        sm.entity.timer.start(1.0)

    @staticmethod
    def exit(sm):
        sm.entity.timer.stop()

    @staticmethod
    def on_timer_elapsed(sm):
        AntClingState.on_move_normal(sm)

    @staticmethod
    def on_move_normal(sm):
        sm.switch_state(AntMoveState)
        return


class AntMoveState(BugBaseState):

    @staticmethod
    def enter(sm):
        sm.entity.timer.start()

    @staticmethod
    def exit(sm):
        sm.entity.timer.stop()

    @staticmethod
    def update(sm, dt):
        if entity_registry.get_by_id(sm.entity.target_id).health <= 0:
            sm.switch_state(AntInitialState)
            return
        sm.entity.pos += sm.entity.speed * sm.entity.direction * dt

    @staticmethod
    def on_timer_elapsed(sm):
        sm.entity.set_direction(entity_registry.get_by_id(sm.entity.target_id))
        sm.entity.timer.interval = random.random() * sm.entity.config.ANT_STRAIGHT_TIME_VAR
        sm.entity.timer.start()

    @staticmethod
    def on_cling(sm):
        sm.switch_state(AntClingState)
        return


class AntInitialState(BugBaseState):

    @staticmethod
    def enter(sm):
        pass  # don't change state during enter!

    @staticmethod
    def update(sm, dt):
        AntInitialState.select_potatoes(sm)

    @staticmethod
    def select_potatoes(sm):
        potatoes = entity_registry.get_by_kind(settings.KIND_POTATOES)
        potatoes = [_p for _p in potatoes if not _p.is_empty]
        if potatoes:
            pot = random.choice(potatoes)
            sm.entity.target_id = pot.id
            sm.entity.set_direction(pot)
            sm.switch_state(AntMoveState)
            return

    @staticmethod
    def on_eat(sm, potatoes_id):
        pass


class AntCrushedState(BugBaseState):

    @staticmethod
    def on_eat(sm, potatoes_id):
        pass

    @staticmethod
    def on_stuck(sm):
        pass

    @staticmethod
    def on_crush(sm):
        pass


class AntEatState(BugBaseState):

    @staticmethod
    def enter(sm):
        # sound.bug_munch()
        sm.entity.timer.start(sm.entity.config.EAT_INTERVAL)

    @staticmethod
    def exit(sm):
        sm.entity.timer.stop()

    @staticmethod
    def on_timer_elapsed(sm):
        target = entity_registry.get_by_id(sm.entity.target_id)
        target.handle_message(messagetypes.EAT, sm.entity.config.BITE)
        if target.is_empty:
            sm.switch_state(AntInitialState)
            return
        sm.entity.timer.start()

    @staticmethod
    def on_eat(sm, potatoes_id):
        t = time.time()
        if not hasattr(sm.entity, 'gummate'):
            sound.bug_munch()
            sm.entity.gummate = t
        elif t - sm.entity.gummate >= 5.0:
            sound.bug_munch()
            sm.entity.gummate = t


class EntityAnt(EntityBug):

    def __init__(self, config):
        size = config.ANT_SIZE + (random.random() - 0.5) * config.ANT_SIZE_VAR
        health = config.HEALTH + (random.random() - 0.5) * config.HEALTH_VAR
        EntityBug.__init__(self, ant_img_id, size, health)
        self.speed = config.ANT_SPEED + random.random() * config.ANT_SPEED_VAR
        self.timer = Timer(random.random() * config.ANT_STRAIGHT_TIME_VAR, name="timer ant " + str(self.id))
        self.timer.event_elapsed.add(self._timer_elapsed)

        self.suck_force = config.ANT_SUCK_FORCE
        self.cling_force = config.ANT_CLING_FORCE
        self.config = config
        self.kind = config.kind

        self.sm = SimpleStateMachine(AntInitialState, self)

    def _timer_elapsed(self, timer):
        self.sm.current_state.on_timer_elapsed(self.sm)

    def update(self, dt):
        self.sm.current_state.update(self.sm, dt)

    def set_direction(self, target):
        self.direction = (target.pos - self.pos).normalized
        self.direction = self.direction.rotated((random.random() - 0.5) * self.config.ANT_ANGLE_VAR, Vec3(0, 0, 1))

    def handle_message(self, message, extra=None):
        if message == messagetypes.MOVE_NORMAL:
            self.sm.current_state.on_move_normal(self.sm)
        elif message == messagetypes.CLING_TO_GROUND:
            self.sm.current_state.on_cling(self.sm)
        elif message == messagetypes.SUCKED_IN:
            self.sm.current_state.on_sucked_in(self.sm)
        elif message == messagetypes.STUCK:
            self.sm.current_state.on_stuck(self.sm)
        elif message == messagetypes.HEAT:
            self.sm.current_state.on_heat(self.sm, extra)
        elif message == messagetypes.EAT:
            if extra == self.target_id:
                self.sm.current_state.on_eat(self.sm, extra)
        elif message == messagetypes.CRUSH:
            self.sm.current_state.on_crush(self.sm)

    def is_alive(self):
        return self.sm.current_state != AntCrushedState


class EntityRoach(EntityBug):

    kind = settings.KIND_ROACH

    def __init__(self):
        EntityBug.__init__(self, roach_img_id)


class EntitySpider(EntityBug):

    kind = settings.KIND_SPIDER

    def __init__(self):
        EntityBug.__init__(self, spider_img_id)


class EntityFly(EntityBug):

    kind = settings.KIND_FLY

    def __init__(self):
        EntityBug.__init__(self, fly_img_id)


def factory(Clas, position, config, count=1):
    for i in range(count):
        bug = Clas(config)
        bug.pos = position.clone()
        yield bug


def ant_factory(config, position, count=1):
    for bug in factory(EntityAnt, position, config, count):
        yield bug




def init():
    global ant_img_id, roach_img_id, spider_img_id, fly_img_id
    IMAGE = pyknic.pygame.resource.IMAGE
    ant_img_id = pyknic.pygame.resource.resources.load(
        IMAGE, "ant.png", scale=1.0, angle=0, width=20, height=20, color=Color('red'))
    roach_img_id = pyknic.pygame.resource.resources.load(
        IMAGE, "roach.png", scale=1.0, angle=0, width=20, height=20, color=Color('pink'))
    spider_img_id = pyknic.pygame.resource.resources.load(
        IMAGE, "spider.png", scale=1.0, angle=0, width=20, height=20, color=Color('brown'))
    fly_img_id = pyknic.pygame.resource.resources.load(
        IMAGE, "fly.png", scale=1.0, angle=0, width=20, height=20, color=Color('grey'))
