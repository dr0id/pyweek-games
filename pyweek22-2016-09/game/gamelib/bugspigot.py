import pygame

from . import entitybug
from . import settings
from .entity import BaseEntity
from .pyknic.mathematics import Vec3 as Vec, Vec2
from .pyknic.timing import Timer


class BugSpigot(BaseEntity):
    """Place a bug spigot to release a fixed number of bugs at a fixed rate

    This class can be used directly, but is designed to be used with a scheduler. The callback is BugSpigot.spawn.
    """
    def __init__(self, config, count, interval, position, register):
        """construct a BugSpigot

        :param config: entitybug.ant_factory, etc. to spawn bugs
        :param count: number of bug to dispense
        :param interval: time interval between bugs
        :param position: position in game space to spawn the bug
        :param register: function; takes a bug to register (e.g. add to containers)
        """
        BaseEntity.__init__(self, pygame.Rect(position.as_xy_tuple(), (0, 0)))
        self.config = config
        self.count = count
        self.interval = interval
        self.pos = position
        self.timer = Timer(interval, repeat=True, name="bugspigot")
        self.timer.event_elapsed.add(self._timer_elapsed)
        self.timer.start()
        self.register = register
        self.kind = settings.KIND_BUG_SPIGOT

    def _timer_elapsed(self, timer):
        self.spawn()

    def is_empty(self):
        return self.count <= 0

    def spawn(self, *args):
        """spawn a number of bugs using the factory

        :param *args: dt, etc.
        :return: None
        """
        if not self.is_empty():
            self.count -= 1
            for bug in entitybug.ant_factory(self.config, Vec(self.pos.x, self.pos.y, 0), 1):
                self.register(bug)
        else:
            self.timer.stop()
