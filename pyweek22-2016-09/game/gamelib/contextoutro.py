# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek22-2016-09
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import pygame

from . import pyknic
from . import settings
from . import sound
from .pyknic import tweening
from .pyknic.tweening import Tweener

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2016"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 


class Sprite(object):

    def __init__(self, image, rect):
        self.image = image
        self.rect = rect
        self.zoom = 1.0
        self.x = -500.0
        self.y = -500.0


class ContextOutro(pyknic.context.Context):

    def __init__(self):
        self.tweener = Tweener()

        self.vc_right = self.load_image("data/vacuum_monster.png")
        self.dr_right = self.load_image("data/dr_right.png")
        self.g_right = self.load_image("data/g_right.png")

        self.vc_left = pygame.transform.flip(self.vc_right, True, False)
        self.dr_left = self.load_image("data/dr_left.png")
        self.g_left = self.load_image("data/g_left.png")

        images = [self.vc_right, self.dr_right, self.g_right, self.vc_left, self.dr_left, self.g_left]
        self.sprites = [Sprite(_img, _img.get_rect()) for _img in images]

        name = "data/crack.png"
        crack = pygame.image.load(name).convert()
        crack.set_colorkey(crack.get_at((0, 0)), pygame.RLEACCEL)
        spr = Sprite(crack, crack.get_rect())
        spr.x = settings.SCREEN_WIDTH // 2
        spr.y = 300
        self.sprites.insert(0, spr)

        self.reset()

    def reset(self, *args, **kwargs):
        cx = 1500
        cy = 800
        duration = 4.0
        d_between = 1.1
        d_side = duration + 2 * d_between
        sw, sh = settings.SCREEN_SIZE

        self.create_tween(-100, 100, cx, cy, d_between, self.sprites[1+1], duration)  # dr right
        self.create_tween(-100, 100, cx, cy, 2 * d_between, self.sprites[0+1], duration)  # vc right

        self.create_tween(sw + 100, 100, -cx, cy, d_side, self.sprites[-1], duration)  # g left
        self.create_tween(sw + 100, 100, -cx, cy, d_side + 2 * d_between, self.sprites[-3], duration)  # vc left

        d_side += d_between

        self.create_tween(-100, 100, cx, cy, 2 * d_side, self.sprites[2+1], duration)  # g right
        self.create_tween(-100, 100, cx, cy, 2 * d_side + d_between, self.sprites[0+1], duration)  # vc right

        self.create_tween(sw + 100, 100, -cx, cy, 3 * d_side, self.sprites[-2], duration)  # dr left
        self.create_tween(sw + 100, 100, -cx, cy, 3 * d_side + 2 * d_between, self.sprites[-3], duration, self.reset)  # vc left

    def create_tween(self, bx, by, cx, cy, delay, spr, duration, cb_end=None):
        self.tweener.create_tween(spr, "x", bx, cx, duration, delay=delay)
        self.tweener.create_tween(spr, "y", by, cy, duration, delay=delay)
        self.tweener.create_tween(spr, "zoom", 1.0, 1.5, duration, delay=delay, cb_end=cb_end)

    def load_image(self, img_path):
        return pygame.image.load(img_path).convert_alpha()

    def enter(self):
        pygame.event.clear()

    def resume(self):
        pygame.event.clear()
        sound.earthquake()
        duration = 1.0
        displacement_range = (-3, 3)
        self.tweener.create_tween(self.sprites[0], 'x', self.sprites[0].x, 10, duration, tweening.ease_random_int_bounce,
                                        params=displacement_range)
        self.tweener.create_tween(self.sprites[0], 'y', self.sprites[0].y, 10, duration, tweening.ease_random_int_bounce,
                                        params=displacement_range)

    def update(self, dt):
        sound.play_action_song()
        self.tweener.update(dt)
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                self.pop()
                return
            elif event.type == pygame.QUIT:
                self.pop()
                return

    def draw(self, screen, do_flip=True, interpolation_factor=1.0):
        screen.fill((200, 200, 200))
        for spr in self.sprites:
            img = spr.image
            if spr.zoom != 1.0:
                img = pygame.transform.rotozoom(img, 0.0, spr.zoom)
            spr.rect.center = spr.x, spr.y
            screen.blit(img, spr.rect)

        pygame.display.flip()

