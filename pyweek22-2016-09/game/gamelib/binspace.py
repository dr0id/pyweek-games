# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek22-2016-09
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function


__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2016"
__credits__ = ["DR0ID"]     # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []    # list of public visible parts of this module 


class BinSpace(object):

    def __init__(self, tile_width, tile_height):
        self.tile_width = tile_width
        self.tile_height = tile_height
        self.buckets = {}  # {(x, y): [entities], ...}

    def get(self, x_min, y_min, x_max, y_max):
        result = []
        for x in range(x_min, x_max):
            for y in range(y_min, y_max):
                result.extend(self.buckets.get((x, y), []))

        return result

    def add(self, entity):
        x = entity.pos.x // self.tile_width
        y = entity.pos.y // self.tile_height
        self.buckets.setdefault((x, y), []).append(entity)

    def remove(self, entity):
        x = entity.pos.x // self.tile_width
        y = entity.pos.y // self.tile_height
        try:
            # try shortcut first
            self.buckets[(x, y)].remove(entity)
        except ValueError:
            # might have updated position and therefore wrong bucket is calculated
            self._remove_slow(entity)
        except KeyError:
            self._remove_slow(entity)

    def _remove_slow(self, entity):
        for val in self.buckets.values():
            try:
                val.remove(entity)
                break
            except ValueError:
                pass

    def update(self):
        """updates all present entities buckets"""
        values = [_v for _v in self.buckets.values()]
        self.buckets.clear()
        for entities in values:
            for ent in entities:
                x = ent.pos.x // self.tile_width
                y = ent.pos.y // self.tile_height
                self.buckets.setdefault((x, y), []).append(ent)
