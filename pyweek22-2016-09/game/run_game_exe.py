#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
TODO: docstring
"""

# # run in right directory
# if not sys.argv[0]:
#     appdir = os.path.abspath(os.path.dirname(__file__))
# else:
#     appdir = os.path.abspath(os.path.dirname(sys.argv[0]))
# os.chdir(appdir)
# # make sure that sub modules in gamelib are imported corectly
# appdir = os.path.join(appdir, 'gamelib')
# if not appdir in sys.path:
#     sys.path.insert(0,appdir)
# import os

# os.chdir(os.path.join(".", "gamelib"))

import skellington


def run_debug():
    from gamelib import main
    # running in debug mode
    main.main()


if __name__ == '__main__':
    skellington.run_main(run_debug, True, "run_game_exe.py.log")
