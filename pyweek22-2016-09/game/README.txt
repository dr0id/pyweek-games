﻿You cANT let him in here
========================

CONTACT:

Homepage: https://pyweek.org/e/DRummb0ID_22/
Name: You cANT let him in here
Team: DRummb0ID_22
Members: Gummbum, DR0ID


DEPENDENCIES:

You might need to install some of these before running the game:

  Python:     http://www.python.org/
  PyGame:     http://www.pygame.org/


RUNNING THE GAME:

**** **** **** **** **** **** **** **** **** **** **** **** **** **** **** ****

VERY IMPORTANT: There is a potentially very LOUD alarm during gameplay. We tried
to soften the volume to a safe hearing level. Just to be safe, please don't play
this with headphones on.

**** **** **** **** **** **** **** **** **** **** **** **** **** **** **** ****

Also important: You'll have a better audio experience if you turn off dynamic EQ
in your soundcard's settings. Especially voice and volume levelers, and dynamic
sound shaping.

Staring the game:

On Windows or Mac OS X, locate the "run_game.pyw" file and double-click it.

Otherwise open a terminal / console and "cd" to the game directory and run:

  python run_game.py


HOW TO PLAY THE GAME:

Move the cursor around the screen with the mouse.
Use left and right mouse buttons.
Detailed instructions are integrated into the back story, which you can read in game.

ITS A MOUSE HEAVY GAME, YOU NEED A MOUSE TO PLAY!!

Description from the project web page:
-------------------------------------------------------------------------------
Your home has become a war zone. If you conserve your very limited resources
carefully, you just might make it. Air being one of them - make sure you take a
breath in between the relentless, frantic waves of enemy incursions.

Forget the mess, clean up is for later. Get in the fight! You cAN'T let 'em in
here! :)
-------------------------------------------------------------------------------


LICENSE:

This game skellington has same license as pyknic.


ART:

antlion                     CC-BY 3.0               http://opengameart.org/content/antlion
byttery power icon          CC0                     http://opengameart.org/content/battery-power-icon
Empty bar                   CC-BY-SA 3.0            http://opengameart.org/content/health-and-mana-bars
potato chips                ???                     http://wrim.dvrlists.com/potato-chips/
crack                       ???                     https://www.dreamstime.com/royalty-free-stock-images-crack-image10288259

See *.txt in the data/* subdirectories for other credits and sources.

Finit.
