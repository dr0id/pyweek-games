# -*- coding: utf-8 -*-

import ConfigParser
import os

#-------------------------------------------------------------------------------

CAMMASK = 0
CAMCURSOR = 1
TREE = 2
GRASS = 3
GRASS1 = 4
FRONTGRASS = 5
SKY = 6
MOUNTAINS = 7
HILL1 = 8
HILL2 = 9
OWL = 10

#-------------------------------------------------------------------------------

_path_prefix = "data"

class Props(object):
    def __init__(self, name):
        config = ConfigParser.SafeConfigParser()
        loaded = config.read(os.path.join(_path_prefix, name))
        if __debug__:
            print "loaded config", loaded
            
        self.name = config.get("entity", "name")
        self.type = config.get("entity", "type")
        self.image_name = config.get("image", "image_name")
        self.path = None
        
        if config.has_option("path", "waypoints"):
            self.path = []
            coords = config.get("path", "waypoints").split(",")
            for coord in coords:
                self.path.append(float(coord))


                
entity_props = {
                CAMMASK:Props("cam_mask.config"), 
                CAMCURSOR:Props("cam_cursor.config"), 
                TREE:Props("tree.config"), 
                GRASS:Props("grass.config"), 
                GRASS1:Props("grass1.config"), 
                FRONTGRASS:Props("frontgrass.config"), 
                SKY:Props("sky.config"), 
                MOUNTAINS:Props("mountains.config"), 
                HILL1:Props("hill1.config"), 
                HILL2:Props("hill2.config"), 
                OWL:Props("owl.config"), 
                }


