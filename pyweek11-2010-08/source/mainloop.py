# -*- coding: utf-8 -*-


import sys
import os

import pygame
# from params import params
#-------------------------------------------------------------------------------

class App(object):

    def __init__(self, world_impl, view_impl, screen_size=(800, 600)):
        self.screen_size = screen_size
        self.clock = None
        
        self._gen = self.__gen_next_id()
        
        self.world_impl = world_impl
        self.view_impl = view_impl
        
        self.world = world_impl(self)
        self.view = view_impl(self)
        
        self.debug_events_on = False
        
    def handle_events(self):
        self.view.on_events(pygame.event.get())

    def reset(self):
        # reset
        if __debug__:
            print "reset"
        self.world = self.world_impl(self.screen_size)
        self.view = self.view_impl(self.world)
        
            
    def run(self):
        pygame.init()
        pygame.display.set_caption(self.world.title)
        self.clock = pygame.time.Clock()

        self.running = True
        
        self.view.init()
        self.world.init()
        while self.running:

            dt_seconds = self.clock.tick() / 1000.0
            # prevent too big timesteps, no good for simulaton
            # this happens when you drag the window
            if dt_seconds > 0.3:
                dt_seconds = 0
            self.world.update(dt_seconds)

            # draw everything
            self.handle_events()
            self.view.update(dt_seconds)
            
    def get_new_ID(self):
        id = self._gen.next()
        if __debug__:
            print "new id generated", id
        return id
        
    def __gen_next_id(self):
        for x in xrange(1000, sys.maxint):
            yield x
    
    def load_image(self, name, scale=None, prefix="data/images"):
        try:
            p = os.path.abspath(os.path.join(prefix, name))
            if __debug__:
                print "load image", p
            surf = pygame.image.load(p)
            if scale:
                assert scale > 0.0
                if __debug__:
                    print '??? scaling', scale
                surf = pygame.transform.rotozoom(surf, 0, scale)
            
            
            #TODO: cache?
            #TODO: convert?
            
            return surf.convert_alpha()
        except Exception, e:
            if __debug__:
                print "!!exception loading", e
                print "!!creating image for", name
            surf = pygame.Surface((10, 10))
            if scale:
                assert scale > 0.0
                if __debug__:
                    print '??? scaling', scale
                surf = pygame.transform.rotozoom(surf, 0, scale)
            surf.fill((255, 0, 0))
            return surf

    

if __name__ == '__main__':
    import view
    import world
    app = App(world.World, view.View)
    print app.get_new_ID()
    print app.get_new_ID()
    print app.get_new_ID()
    print app.get_new_ID()
    print app.get_new_ID()

