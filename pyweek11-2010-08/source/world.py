# -*- coding: utf-8 -*-

import statemachine
import worldstates
import eventtypes

#-------------------------------------------------------------------------------

class World(object):

    def __init__(self, app):
        self.app = app
        self.title = "Caught on picture"
        self.statemachine = statemachine.StateMachine(self)
        self.size = 1600, 600
        self.t = 0
        self.entities = {}
        
    def init(self):
        self.statemachine.change_state(worldstates.Intro)
        
    def handle_message(self, message):
        self.statemachine.handle_message(message)
        
    def on_event(self, event):
        if __debug__:
            print "world event", event.type
        self.statemachine.on_events(event)
        
    def update(self, dt_seconds):
        self.statemachine.update(dt_seconds)
        # print "world updating", dt_seconds
        
    def send_create_entity_event(self, ID, entity_type, x, y, layer, rotation, scale):
        newevent = eventtypes.Event()
        newevent.type = eventtypes.CREATEENTITY
        newevent.ID = ID
        newevent.x = x
        newevent.y = y
        newevent.layer = layer
        newevent.rotation = rotation
        newevent.scale = scale
        newevent.entity_type = entity_type
        if __debug__:
            print "send create entity event", ID
        self.app.view.on_event(newevent)

    def send_remove_entity_event(self, ID):
        event = eventtypes.Event()
        event.type = eventtypes.REMOVEENTITY
        event.ID = ID
        self.app.view.on_event(event)
        
    def send_update_entity_event(self, entity): #ID, x, y, layer, rotation=0.0, scale=1.0):
        event = eventtypes.Event()
        event.type = eventtypes.UPDATEENTITY
        event.ID = entity.ID
        event.x = entity.x
        event.y = entity.y
        event.layer = entity.layer
        # event.rotation = entity.rotation
        # event.scale = entity.scale
        self.app.view.on_event(event)
        
    def create_entity(self, entity_type, x, y, layer, rotation=0.0, scale=1.0):
        ID = self.app.get_new_ID()
        self.entities[ID] = Entity(ID, entity_type, x, y, layer)
        if __debug__:
            print "world created new entity", ID
        self.send_create_entity_event(ID, entity_type, x, y, layer, rotation, scale)
        
    
#-------------------------------------------------------------------------------

class Entity(object):

    def __init__(self, ID, entity_type, x, y, layer):
        self.type = entity_type
        self.x = x
        self.y = y
        self.layer = layer
        self.ID = ID