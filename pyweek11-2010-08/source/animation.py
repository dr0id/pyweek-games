# -*- coding: utf-8 -*-

import scheduler
#-------------------------------------------------------------------------------

class Animation(object):

    _scheduler = scheduler.Scheduler() 

    def __init__(self, frames, end_callback, loop=False):
        self.frames = frames
        self.loop = False
        self.idx = 0
        self.end_callback = end_callback
        
    def start(self):
        self._scheduler.schedule(self._callback_update, 0.5)
        
    def stop(self):
        self._scheduler.remove(self._callback_update, True)
        
    def reset(self):
        self.idx = 0

    def _callback_update(self):
        self.idx += 1
        if self.idx >= len(self.frames):
            self.stop()
            self.reset()
            self.end_callback(self)
            
        
