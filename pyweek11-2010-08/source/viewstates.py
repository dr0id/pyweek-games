# -*- coding: utf-8 -*-

import os
import random
import datetime

import pygame


from view import Picture
from view import Sprite
import eventtypes
import entitytypes

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
class Intro(object):

    @staticmethod
    def enter(view):
        if __debug__:
            print "enter viewintro" 
        w, h = view.app.screen_size
        # view.buttons = [pygame.Rect(w - 100 - 10 - 100 - 10, h - 30 - 10, 100, 30), pygame.Rect(w -100 - 10, h - 30 - 10, 100, 30)]
        view.buttons = [pygame.Rect(w -100 - 10, h - 30 - 10, 100, 30)]
        # TODO: load button images
        pygame.mouse.set_visible(True)
        img = view.app.load_image("intro1.png")
        # spr = Sprite(ID, props, image, x, y, layer=0, rotation=0, scale=1.0):
        spr = Sprite(0, None, img, w/2, h/2)
        view.add_sprite(spr)
        
    @staticmethod
    def exit(view):
        if __debug__:
            print "exit viewintro"
        del view.buttons
        del view.hit
        pygame.mouse.set_visible(False)
        view.clear_sprites()

    @staticmethod
    def on_events(view, events):
        while events:
            event = events.pop(0)
            if __debug__:
                if view.app.debug_events_on:
                    print "intro event", event, event.type
            if event.type == pygame.QUIT:
                view.app.running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                        view.app.running = False
                elif event.key == pygame.K_r:
                    view.app.reset()
                elif event.key == pygame.K_F1:
                    view.schow_fps = not view.schow_fps
                elif event.key == pygame.K_F2:
                    view.do_clear_screen = not view.do_clear_screen
                elif event.key == pygame.K_F3:
                    view.show_debug_vectors = not view.show_debug_vectors
            elif event.type == pygame.MOUSEBUTTONDOWN:
                view.hit = pygame.Rect(event.pos[0], event.pos[1], 1, 1).collidelist(view.buttons)
            elif event.type == pygame.MOUSEBUTTONUP:
                if view.hit > -1:
                    if view.hit == pygame.Rect(event.pos[0], event.pos[1], 1, 1).collidelist(view.buttons):
                        event = eventtypes.Event()
                        event.type = eventtypes.BUTTONCLICKED
                        event.buttonID = view.hit
                        view.app.world.on_event(event)
                view.hit = -1
            elif event.type == eventtypes.CHANGESTATE:
                view.statemachine.change_state(event.state)
                return
                    
            
        
    @staticmethod
    def on_message(view, message):
        pass
        
    @staticmethod
    def execute(view, dt_seconds):
        for spr in view.sprites:
            view.screen.blit(spr.image, spr.rect)
        for button in view.buttons:
            pygame.draw.rect(view.screen, (255, 0, 0), button)
        w, h = view.app.screen_size
        # view.draw_text(w - 210, h - 30, "Tutorial")
        view.draw_text(w - 100, h - 30, "Start")


#-------------------------------------------------------------------------------
class Instructions(object):

    @staticmethod
    def enter(view):
        if __debug__:
            print "enter viewintro" 
        w, h = view.app.screen_size
        # view.buttons = [pygame.Rect(w - 100 - 10 - 100 - 10, h - 30 - 10, 100, 30), pygame.Rect(w -100 - 10, h - 30 - 10, 100, 30)]
        view.buttons = [pygame.Rect(w -100 - 10, h - 30 - 10, 100, 30)]
        # TODO: load button images
        pygame.mouse.set_visible(True)
        img = view.app.load_image("instructions.png")
        # spr = Sprite(ID, props, image, x, y, layer=0, rotation=0, scale=1.0):
        spr = Sprite(0, None, img, w/2, h/2)
        view.add_sprite(spr)
        
    @staticmethod
    def exit(view):
        if __debug__:
            print "exit viewintro"
        del view.buttons
        del view.hit
        pygame.mouse.set_visible(False)
        view.clear_sprites()

    @staticmethod
    def on_events(view, events):
        while events:
            event = events.pop(0)
            if __debug__:
                if view.app.debug_events_on:
                    print "view instruction", event, event.type
            if event.type == pygame.QUIT:
                view.app.running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                        view.app.running = False
                elif event.key == pygame.K_r:
                    view.app.reset()
                elif event.key == pygame.K_F1:
                    view.schow_fps = not view.schow_fps
                elif event.key == pygame.K_F2:
                    view.do_clear_screen = not view.do_clear_screen
                elif event.key == pygame.K_F3:
                    view.show_debug_vectors = not view.show_debug_vectors
            elif event.type == pygame.MOUSEBUTTONDOWN:
                view.hit = pygame.Rect(event.pos[0], event.pos[1], 1, 1).collidelist(view.buttons)
            elif event.type == pygame.MOUSEBUTTONUP:
                if view.hit > -1:
                    if view.hit == pygame.Rect(event.pos[0], event.pos[1], 1, 1).collidelist(view.buttons):
                        event = eventtypes.Event()
                        event.type = eventtypes.BUTTONCLICKED
                        event.buttonID = view.hit
                        view.app.world.on_event(event)
                view.hit = -1
            elif event.type == eventtypes.CHANGESTATE:
                view.statemachine.change_state(event.state)
                return
                    
            
        
    @staticmethod
    def on_message(view, message):
        pass
        
    @staticmethod
    def execute(view, dt_seconds):
        for spr in view.sprites:
            view.screen.blit(spr.image, spr.rect)
        for button in view.buttons:
            pygame.draw.rect(view.screen, (255, 0, 0), button)
        w, h = view.app.screen_size
        # view.draw_text(w - 210, h - 30, "Tutorial")
        view.draw_text(w - 100, h - 30, "Start")

#-------------------------------------------------------------------------------
class GamePlay(object):

    @staticmethod
    def enter(view):
        if __debug__:
            print "enter viewgameplay"
            pygame.mouse.set_visible(True)
            
        if view.saved_mouse_pos:
            pygame.mouse.set_pos(view.saved_mouse_pos)
        if view.cam_cursor is None:
            w, h = view.app.screen_size
            view.cam_cursor = view.create_sprite(entitytypes.CAMCURSOR, None, w/2, h/2, 100000)
        view.add_sprite(view.cam_cursor)
        view.cam_cursor.rect.center = pygame.mouse.get_pos()
        
        # load the images and sprites here for the scene (should go into a scene.config and be loaded by the world)
        if not view.world_is_created:
            view.world_is_created = True
            w, h = view.app.screen_size
            view.add_sprite(view.create_sprite(entitytypes.FRONTGRASS, None, 800, h - 50, 1.0))
            # view.add_sprite(view.create_sprite(entitytypes.GRASS, None, 800, h - 50, 0.5))
            # view.add_sprite(view.create_sprite(entitytypes.GRASS1, None, 800, h - 50, 0.8))
            
            # for i in range(1600/300):
                # for layer in range(4):
                    # tree = view.create_sprite(entitytypes.TREE, None, 300*i + ((layer+1) * 150), 400 - layer*60, 0.9 - layer/10.0, 0, 1.0 - layer/5.0)
                    # print "tree", tree.rect.center, tree.layer, tree.scale
                    # tree.image = pygame.transform.flip(tree.image, random.randint(0,1), 0).convert_alpha()
                    # view.add_sprite(tree)
                    
            # def create_sprite(self, entitytype,    ID=None, x=0, y=0, layer=0, rotation=0, scale=None):
            tree = view.create_sprite(entitytypes.TREE, None, 150, 400, 0.9, 0, 1.0)
            tree.image = pygame.transform.flip(tree.image, random.randint(0,1), 0).convert_alpha()
            view.add_sprite(tree)
            tree = view.create_sprite(entitytypes.TREE, None, 450, 400, 0.88, 0, 0.75)
            tree.image = pygame.transform.flip(tree.image, random.randint(0,1), 0).convert_alpha()
            view.add_sprite(tree)
            tree = view.create_sprite(entitytypes.TREE, None, 750, 400, 0.89, 0, 0.9)
            tree.image = pygame.transform.flip(tree.image, random.randint(0,1), 0).convert_alpha()
            view.add_sprite(tree)
            tree = view.create_sprite(entitytypes.TREE, None, 1050, 400, 0.899, 0, 0.85)
            tree.image = pygame.transform.flip(tree.image, random.randint(0,1), 0).convert_alpha()
            view.add_sprite(tree)
            tree = view.create_sprite(entitytypes.TREE, None, 1350, 400, 0.99, 0, 1.2)
            tree.image = pygame.transform.flip(tree.image, random.randint(0,1), 0).convert_alpha()
            view.add_sprite(tree)

            tree = view.create_sprite(entitytypes.TREE, None, 300, 340, 0.8, 0, 0.8)
            tree.image = pygame.transform.flip(tree.image, random.randint(0,1), 0).convert_alpha()
            view.add_sprite(tree)
            tree = view.create_sprite(entitytypes.TREE, None, 600, 360, 0.7, 0, 0.75)
            tree.image = pygame.transform.flip(tree.image, random.randint(0,1), 0).convert_alpha()
            view.add_sprite(tree)
            tree = view.create_sprite(entitytypes.TREE, None, 900, 350, 0.6999, 0, 0.8)
            tree.image = pygame.transform.flip(tree.image, random.randint(0,1), 0).convert_alpha()
            view.add_sprite(tree)
            tree = view.create_sprite(entitytypes.TREE, None, 1200, 340, 0.8, 0, 0.8)
            tree.image = pygame.transform.flip(tree.image, random.randint(0,1), 0).convert_alpha()
            view.add_sprite(tree)
            tree = view.create_sprite(entitytypes.TREE, None, 1280, 320, 0.7, 0, 0.7)
            tree.image = pygame.transform.flip(tree.image, random.randint(0,1), 0).convert_alpha()
            view.add_sprite(tree)

            tree = view.create_sprite(entitytypes.TREE, None, 200, 300, 0.25, 0, 0.3)
            tree.image = pygame.transform.flip(tree.image, random.randint(0,1), 0).convert_alpha()
            view.add_sprite(tree)
            tree = view.create_sprite(entitytypes.TREE, None, 450, 320, 0.39, 0, 0.55)
            tree.image = pygame.transform.flip(tree.image, random.randint(0,1), 0).convert_alpha()
            view.add_sprite(tree)
            tree = view.create_sprite(entitytypes.TREE, None, 750, 300, 0.45, 0, 0.6)
            tree.image = pygame.transform.flip(tree.image, random.randint(0,1), 0).convert_alpha()
            view.add_sprite(tree)
            tree = view.create_sprite(entitytypes.TREE, None, 1050, 300, 0.41, 0, 0.4)
            tree.image = pygame.transform.flip(tree.image, random.randint(0,1), 0).convert_alpha()
            view.add_sprite(tree)
            # tree = view.create_sprite(entitytypes.TREE, None, 350, 280, 0.51, 0, 0.6)
            # tree.image = pygame.transform.flip(tree.image, random.randint(0,1), 0).convert_alpha()
            # view.add_sprite(tree)

            # tree = view.create_sprite(entitytypes.TREE, None, 1800, 220, 0.6, 0, 0.4)
            # tree.image = pygame.transform.flip(tree.image, random.randint(0,1), 0).convert_alpha()
            # view.add_sprite(tree)
            # tree = view.create_sprite(entitytypes.TREE, None, 1500, 220, 0.6, 0, 0.4)
            # tree.image = pygame.transform.flip(tree.image, random.randint(0,1), 0).convert_alpha()
            # view.add_sprite(tree)
            # tree = view.create_sprite(entitytypes.TREE, None, 1200, 220, 0.6, 0, 0.4)
            # tree.image = pygame.transform.flip(tree.image, random.randint(0,1), 0).convert_alpha()
            # view.add_sprite(tree)
            # tree = view.create_sprite(entitytypes.TREE, None, 600, 220, 0.6, 0, 0.4)
            # tree.image = pygame.transform.flip(tree.image, random.randint(0,1), 0).convert_alpha()
            # view.add_sprite(tree)
            # tree = view.create_sprite(entitytypes.TREE, None, 900, 220, 0.6, 0, 0.4)
            # tree.image = pygame.transform.flip(tree.image, random.randint(0,1), 0).convert_alpha()
            # view.add_sprite(tree)
                    
            ww, wh = view.app.world.size
            sky = view.create_sprite(entitytypes.SKY, None, ww/2, wh/2, 0.0, 0, 1.0)
            view.add_sprite(sky)
            mountains = view.create_sprite(entitytypes.MOUNTAINS, None, 500, wh/2 - 100, 0.1, 0, 1.0)
            view.add_sprite(mountains)
            
            hill = view.create_sprite(entitytypes.HILL1, None, 300, 500, 0.8, 0, 1.0)
            view.add_sprite(hill)
            
            hill = view.create_sprite(entitytypes.HILL1, None, 0, 430, 0.79999, 0, 0.8)
            view.add_sprite(hill)
            
            hill = view.create_sprite(entitytypes.HILL2, None, 900, 500, 0.7, 0, 1.0)
            view.add_sprite(hill)
            
            hill = view.create_sprite(entitytypes.HILL1, None, 1300, 430, 0.5, 0, 0.75)
            view.add_sprite(hill)
        
            
            hill = view.create_sprite(entitytypes.HILL1, None, 950, 350, 0.4, 0, 0.75)
            view.add_sprite(hill)
        
            hill = view.create_sprite(entitytypes.HILL2, None, 600, 410, 0.4, 0, 0.7)
            view.add_sprite(hill)
        
            hill = view.create_sprite(entitytypes.HILL2, None, 450, 340, 0.2, 0, 1.0)
            view.add_sprite(hill)
            
            hill = view.create_sprite(entitytypes.HILL2, None, 0, 300, 0.199, 0, 0.7)
            view.add_sprite(hill)
        
        
        
            owl = view.create_sprite(entitytypes.OWL, None, 300, 300, 0.8, 0, 1.0)
            view.add_sprite(owl)
        
            owl = view.create_sprite(entitytypes.OWL, None, 500, 280, 0.2, 0, 1.0)
            view.add_sprite(owl)
        

        # num = 100
        # if len(view.sprites) < num:
            # import random
            # rint = random.randint
            # for i in range(num):
                # sprite = view.create_sprite(99, None, rint(0, 800), rint(0, 600), random.random(), 0, rint(1, 20))
                # sprite.image.fill((rint(0, 255), rint(0, 255), rint(0, 255)))
                # view.add_sprite(sprite)
        
        # sprite = view.create_sprite(99, None, 110, 60, 0)
        # sprite.image.fill((0, 50, 100))
        # view.add_sprite(sprite)
        # sprite = view.create_sprite(99, None, 110, 65, 0.5)
        # sprite.image.fill((50, 100, 150))
        # view.add_sprite(sprite)
        # sprite = view.create_sprite(99, None, 110, 70, 0.50001)
        # sprite.image.fill((50, 200, 150))
        # view.add_sprite(sprite)
        # sprite = view.create_sprite(99, None, 110, 75, 1.0)
        # sprite.image.fill((100, 150, 200))
        # view.add_sprite(sprite)
        
        
        # sprite = view.create_sprite(99, None, 300, 300, 4.0)
        # sprite.image.fill((200, 150, 100))
        # view.add_sprite(sprite)
        # sprite = view.create_sprite(99, None, 300, 350, 1000.0)
        # sprite.image.fill((100, 100, 50))
        # view.add_sprite(sprite)
            
        
    @staticmethod
    def exit(view):
        if __debug__:
            print "exit viewgameplay"
            pygame.mouse.set_visible(False)
        view.remove_sprite(view.cam_cursor)
        view.saved_mouse_pos = tuple(pygame.mouse.get_pos())
            
    @staticmethod
    def on_events(view, events):
        while events:
            event = events.pop(0)
            if __debug__:
                if view.app.debug_events_on:
                    print "viewGameplay event", event, event.type
            if event.type == pygame.QUIT:
                view.app.running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                        view.app.running = False
                elif event.key == pygame.K_r:
                    view.app.reset()
                elif event.key == pygame.K_F1:
                    view.schow_fps = not view.schow_fps
                elif event.key == pygame.K_F2:
                    view.do_clear_screen = not view.do_clear_screen
                elif event.key == pygame.K_RIGHT:
                    view.offset_x += 10
                    print view.offset_x
                elif event.key == pygame.K_LEFT:
                    view.offset_x -= 10
                    print view.offset_x
                elif event.key == pygame.K_F3:
                    view.show_debug_vectors = not view.show_debug_vectors
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    if __debug__:
                        print "taking picture"
                    # newevent = eventtypes.Event()
                    # newevent.type = eventtypes.TAKEPICTURE
                    # newevent.x = event.pos[0] # TODO: if scrolling, convert this to world coordinates (offset)
                    # newevent.y = event.pos[1] # TODO: if scrolling, convert this to world coordinates (offset)
                    # view.app.world.on_event(newevent)
                elif event.button == 3:
                    if __debug__:
                        print "focuschange"
                    # newevent = eventtypes.Event()
                    # newevent.type = eventtypes.CHANGEFOCUS
                    # view.app.world.on_event(newevent)
                    view.statemachine.change_state(Focused)
            elif event.type == pygame.MOUSEMOTION:
                view.cam_cursor.rect.center = event.pos
                # grab
                mx, my = event.pos
                w, h = view.app.screen_size
                if my < 20 or my > h - 20:
                    pygame.event.set_grab(False)
                else:
                    pygame.event.set_grab(True)
                
            elif event.type == eventtypes.CHANGESTATE:
                view.statemachine.change_state(event.state)
                return
            elif event.type == eventtypes.UPDATEENTITY:
                if event.ID in view.entities:
                    spr = view.entities[event.ID]
                    spr.rect.center = (event.x, event.y)
                    spr.layer = event.layer
                    # TODO: rotation and scale are not used
                else:
                    if __debug__:
                        print "!!! UPDATEENTITY event for an unkown entity!", event.ID
            elif event.type == eventtypes.CREATEENTITY:
                if __debug__:
                    print "????? creating :", event.entity_type, event.ID, event.x, event.y, event.layer, event.rotation, event.scale
                spr = view.create_sprite(event.entity_type, event.ID, event.x, event.y, event.layer, event.rotation, event.scale)
                view.add_sprite(spr)
            elif event.type == eventtypes.REMOVEENTITY:
                if event.ID in view.entities:
                    if __debug__:
                        print "removing entity", event.ID
                    spr = view.entities[event.ID]
                    view.remove_sprite(spr)
                else:
                    if __debug__:
                        print "!!! could not remove unkown entity", event.ID
                
            
        
    @staticmethod
    def on_message(view, message):
        pass
        
    @staticmethod
    def execute(view, dt_seconds):
        w, h = view.app.screen_size
        mx, my = pygame.mouse.get_pos()
        scroll_area = 150
        scroll_rate = 250
        if mx > w - scroll_area:
            view.offset_x += dt_seconds * scroll_rate
            if view.offset_x > view.app.world.size[0] - w:
                view.offset_x = view.app.world.size[0] - w
        if mx < scroll_area:
            view.offset_x -= dt_seconds * scroll_rate
            if view.offset_x < 0:
                view.offset_x = 0
        view.render(view.screen)
        
        
#-------------------------------------------------------------------------------
class Focused(object):

    @staticmethod
    def enter(view):
        pygame.event.set_grab(True)
        if __debug__:
            print "enter viewfocused" 
            pygame.mouse.set_visible(True)
        if view.cam_mask is None:
            w, h = view.app.screen_size
            view.cam_mask = view.create_sprite(entitytypes.CAMMASK, None, w/2, h/2, 100000)
        # view.add_sprite(view.cam_mask)
        view.double_screen = pygame.Surface(view.app.world.size)
        
        
    @staticmethod
    def exit(view):
        pygame.event.set_grab(False)
        if __debug__:
            print "exit viewFocused"
        try:
            view.sprites.remove(view.cam_mask)
        except:
            pass

    @staticmethod
    def on_events(view, events):
        while events:
            event = events.pop(0)
            if __debug__:
                if view.app.debug_events_on:
                    print event
            if event.type == pygame.QUIT:
                view.app.running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                        view.app.running = False
                elif event.key == pygame.K_r:
                    view.app.reset()
                elif event.key == pygame.K_F1:
                    view.schow_fps = not view.schow_fps
                elif event.key == pygame.K_F2:
                    view.do_clear_screen = not view.do_clear_screen
                elif event.key == pygame.K_F3:
                    view.show_debug_vectors = not view.show_debug_vectors
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    if __debug__:
                        print "taking picture focused"
                    # newevent = eventtypes.Event()
                    # newevent.type = eventtypes.TAKEPICTURE
                    # newevent.x = event.pos[0] # TODO: if scrolling, convert this to world coordinates (offset)
                    # newevent.y = event.pos[1] # TODO: if scrolling, convert this to world coordinates (offset)
                    # view.app.world.on_event(newevent)
                    animals = view.get_all_sprites_of_type_at(view.saved_mouse_pos, "animal", include_scroll=False, include_gui=False)
                    if animals:
                        if __debug__:
                            print "??? found animals", animals
                        spr = None
                        dist = 99999999
                        for animal in animals:
                            dx = animal.rect.centerx - view.saved_mouse_pos[0]
                            dy = animal.rect.centery - view.saved_mouse_pos[1]
                            test = dx * dx + dy * dy
                            if __debug__:
                                print "??? animal", animal.props.name, test, dist, dx, dy
                            if dist > test:
                                spr = animal
                                dist = test
                        if spr:
                            if __debug__:
                                print "??? found animal", spr, spr.ID, spr.props.name
                            pass
                            pygame.draw.rect(view.screen, (255, 0, 0), spr.screen_rect)
                            
                            view.render(view.double_screen, spr)
                            
                            qual = 0
                            border = 25
                            mx, my = view.saved_mouse_pos
                            for j in range(int(my) - border, int(my) + border + 1):
                                for i in range(int(mx) - border, int(mx) + border + 1):
                                    r, g, b, a = view.double_screen.get_at((i,j))
                                    if g == 0 and b == 0:
                                        qual += r
                                        if __debug__:
                                            view.double_screen.set_at((i,j), (0, 0, r, 255))
                                    else:
                                        if __debug__:
                                            # Y = 0.3*R + 0.59*G + 0.11*B
                                            g = 0.3 * r + 0.59 * g + 0.11 * b
                                            view.double_screen.set_at((i,j), (g, g, g, 255))
                            
                            if __debug__:
                                print "?!?!? qual: ", qual
                                mx, my = view.saved_mouse_pos
                                now = datetime.datetime.now()
                                current_time= now.strftime("%Y-%m-%d-%H-%M-%S")

                                view.double_screen = view.double_screen.copy()
                                
                                pygame.draw.line(view.double_screen, (255, 255, 0), (mx, my-border), (mx, my+border), 1)
                                pygame.draw.line(view.double_screen, (255, 255, 0), (mx-border, my), (mx+border, my), 1)
                                rect = pygame.Rect(0, 0, 2*border, 2*border)
                                rect.center = (mx, my)
                                pygame.draw.rect(view.double_screen, (255, 255, 0), rect, 1)
                                
                                name = "debug/screenshots/%s_%i_%i_target.png" % (current_time, len(view.pictures)-1, qual)
                                name = os.path.abspath(name)
                                print name
                                pygame.image.save(view.get_focus_region(view.double_screen), name)

                                
                                view.render(view.double_screen)
                                
                                pygame.draw.line(view.double_screen, (255, 255, 0), (mx, my-border), (mx, my+border), 1)
                                pygame.draw.line(view.double_screen, (255, 255, 0), (mx-border, my), (mx+border, my), 1)
                                rect = pygame.Rect(0, 0, 2*border, 2*border)
                                rect.center = (mx, my)
                                pygame.draw.rect(view.double_screen, (255, 255, 0), rect, 1)
                                
                                name = "debug/screenshots/%s_%i_%i.png" % (current_time, len(view.pictures)-1, qual)
                                name = os.path.abspath(name)
                                print name
                                pygame.image.save(view.get_focus_region(view.double_screen), name)
                                

                                print "screenshot saved"
                                
                            view.render(view.double_screen)
                            
                            #                           image, quality, name, spr, timestamp=None):
                            view.pictures.append(Picture(view.double_screen, qual, spr.props.name, spr))
                                
                    
                elif event.button == 4:
                    if view.zoom_factor < 16:
                        view.zoom_factor *= 2
                        view.zoom_move_factor *= 2
                        # view.zoom_factor += 1
                    if __debug__:
                        print "zoom +", view.zoom_factor
                elif event.button == 5:
                    if view.zoom_factor > 2:
                        view.zoom_factor /= 2
                        view.zoom_move_factor /= 2
                        # view.zoom_factor -= 1
                    if __debug__:
                        print "zoom -", view.zoom_factor
            elif event.type == pygame.MOUSEBUTTONUP:
                if event.button == 3:
                    view.statemachine.change_state(GamePlay)
            elif event.type == pygame.MOUSEMOTION:
                if __debug__:
                    print event.pos, event.rel
                    
                x, y = view.saved_mouse_pos
                mx, my = event.pos
                w, h = view.app.screen_size
                dx = mx - w/2
                dy = my - h/2
                # dx, dy = event.rel
                
                x += view.zoom_move_factor*dx/view.zoom_factor
                y += view.zoom_move_factor*dy/view.zoom_factor
                view.saved_mouse_pos = (x, y, )
                
            elif event.type == eventtypes.CHANGESTATE:
                view.statemachine.change_state(event.state)
                return
                    
            elif event.type == eventtypes.UPDATEENTITY:
                if event.ID in view.entities:
                    spr = view.entities[event.ID]
                    spr.rect.center = (event.x, event.y)
                    spr.layer = event.layer
                    # TODO: rotation and scale are not used
                else:
                    if __debug__:
                        print "!!! UPDATEENTITY event for an unkown entity!"
            elif event.type == eventtypes.CREATEENTITY:
                spr = view.create_sprite(event.entity_type, event.entityID, event.x, event.y, event.layer, event.rotation, event.scale)
                view.add_sprite(spr)

        
    @staticmethod
    def on_message(view, message):
        pass
        
    @staticmethod
    def execute(view, dt_seconds):
        w, h = view.app.screen_size
        pygame.mouse.set_pos(w/2, h/2)

        view.double_screen.fill((255, 255, 255))
        view.render(view.double_screen)
        
        surf = view.get_focus_region(view.double_screen, True)
        
        rect = surf.get_rect()
        rect.center = w/2, h/2
        view.screen.blit(surf, rect)
        
        view.screen.blit(view.cam_mask.image, (0,0))
        
        
        
        
        
        