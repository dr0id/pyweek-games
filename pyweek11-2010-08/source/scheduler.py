# -*- coding: utf-8 -*-


#-------------------------------------------------------------------------------

class Scheduler(object):
    """

        def callback(*args, **kwargs):
            # code
            return 10 # this callback is called in 10 frames again

    """

    STOPREPEAT = 0

    def __init__(self):
        self._heap = []
        heapq.heapify(self._heap)
        self._nexttime = sys.maxsize
        self._current_time = 0
        self._current_frame = 0

    def schedule(self, func, interval, offset=0, *args, **kwargs):
        assert interval >= 0
        nex_time = self._current_time + interval + offset
        heappush(self._heap, (nex_time, func, args, kwargs))
        if nex_time < self._nexttime:
            self._nexttime = nex_time

    def update(self, delta_time):
        self._current_time += delta_time
        assert self._current_time < sys.maxsize # can this ever happend?
        heap = self._heap
        while self._nexttime <= self._current_time:
            nex_time, func, args, kwargs = heappop(heap)

            interval = func(*args, **kwargs)

            if interval > 0.0:
                heappush(heap, (self._current_time + interval, \
                                                            func, args, kwargs))

            if heap:
                self._nexttime = heap[0][0]
            else:
                self._nexttime = sys.maxsize

    def clear(self):
        self._heap[:] = []
        self._check_next()

    def remove(self, func, all_occurences=False):
        to_remove = []
        for entry in self._heap:
            nex_time, entry_func, args, kwargs = entry
            if entry_func == func:
                to_remove.append(entry)
                if not all_occurences:
                    break
        for entry in to_remove:
            self._heap.remove(entry)
        self._check_next()

    def _check_next(self):
        if self._heap:
            self._nexttime = self._heap[0][0]
        else:
            self._nexttime = sys.maxsize



