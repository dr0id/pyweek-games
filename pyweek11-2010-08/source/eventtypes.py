# -*- coding: utf-8 -*-



#-------------------------------------------------------------------------------
# x, y are in world coordinates
CREATEENTITY = 100 # entityID, entitytype, x, y, layer, rotation, scale
REMOVEENTITY = 101 # entityID
UPDATEENTITY = 102 # entityID, x, y, layer, rotation, scale
CHANGESTATE = 103 # state
BUTTONCLICKED = 104 # buttonID
TAKEPICTURE = 105 # x, y
CHANGEZOOM = 106 # zoom
CHANGEFOCUS = 107 
PLAYSOUND = 108 # soundID or name

#-------------------------------------------------------------------------------

class Event(object):
    def __init__(self):
        self.type = None
        self.buttonID = None
        self.ID = None
        self.entity_type = None
        self.x = 0
        self.y = 0
        self.layer = 500
        self.rotation = 0
        self.scale = 1.0
        self.zoom = 0
        self.state = None
        
        

