# -*- coding: utf-8 -*-

"""
This module contains different state machine implementations.
"""

#-------------------------------------------------------------------------------

class StateMachine(object):
    """
    A simple StateMachine.
    
    Usage::
    
        state1 = State1()
        sm = StateMachine()
        sm.switch_state(state1) # set the first state
        
        # in main loop
        sm._current_state.update(sm, dt)
        
        
    
    """

    _previous_state = None
    _current_state = None
    _global_state = None
    
    def __init__(self, owner):
        # self.owner = owner
        object.__setattr__(self, "owner", owner)
        
    def set_current_state(self, state):
        print "setting current state '%s' for '%s'" % (state.__name__, self.owner.ID)
        object.__setattr__(self, "_current_state", state)
        assert self._current_state

    def set_previous_state(self, state):
        print "setting previous state '%s' for '%s'" % (state.__name__, self.owner.ID)
        object.__setattr__(self, "_previous_state", state)
        assert self._previous_state

    def set_global_state(self, state):
        print "setting global state '%s' for '%s'" % (state.__name__, self.owner.ID)
        object.__setattr__(self, "_global_state", state)
        assert self._global_state

    def change_state(self, new_state):
        """
        Change to the next state. First exit is called on the current state,
        then the new state is made the current state and finally enter is called
        on the new state.
        
        :Parameters:
            new_state : object
                Can be any object that has following methods: enter(sm) and 
                exit(sm), can not be None (use a end state instead).
        
        """

        assert new_state is not None
        assert hasattr(new_state, "enter")
        assert hasattr(getattr(new_state, "enter"), '__call__')
        assert hasattr(new_state, "exit")
        assert hasattr(getattr(new_state, "exit"), '__call__')
        if __debug__:
            print 'sm', self.owner.__class__.__name__, 'switchin from', self._current_state, '==>', new_state.__class__.__name__
            
        object.__setattr__(self, "_previous_state", self._current_state)
        
        if self._current_state is not None:
            self._current_state.exit(self.owner)
        object.__setattr__(self, "_current_state", new_state)
        new_state.enter(self.owner)
        
        assert self._current_state != StateMachine._current_state

    def update(self, dt_seconds):
        if self._global_state:
            self._global_state.execute(self.owner, dt_seconds)
        self._current_state.execute(self.owner, dt_seconds)
        
    def handle_message(self, message):
        if self._current_state and self._current_state.on_message(self.owner, message):
            return True
        if self._global_state and self._global_state.on_message(self.owner, message):
            return True
        return False
        
    def on_events(self, events):
        if self._current_state and self._current_state.on_events(self.owner, events):
            return True
        if self._global_state and self._global_state.on_events(self.owner, events):
            return True
        return False
            
    def revert_to_previous_state(self):
        assert self._previous_state
        self.change_state(self._previous_state)
        
    def is_in_state(self, state):
        # if __debug__: print "?? is in state %s, current %s, res %s" % (state.__name__, self._current_state.__name__, self._current_state == state)
        return self._current_state == state
        
    def get_name_of_current_state(self):
        return self._current_state.__name__
        
    def __setattr__(self, name, val):
        msg = "this class '%s' attributes are read only: name %s, value: %s" % (self.__class__.__name__, name, val)
        raise Exception(msg)
        # if name not in dir(self):
            # msg = 'not allowed to set attributes: name %s, value: %s' % (name, val)
            # raise Exception(msg)
        # else:
            # object.__setattr__(self, name, val)

    # def _get__current_state(self):
        # """
        # Returns the current state.
        # """
        # return self.__current_state

    # _current_state = property(_get__current_state, doc="current state, read only")

#-------------------------------------------------------------------------------

class StateBase(object):
    @staticmethod
    def enter(owner):
        pass
    @staticmethod
    def exit(owner):
        pass
    @staticmethod
    def on_message(owner, message):
        pass
    @staticmethod
    def execute(owner, dt_seconds):
        pass
    
   
