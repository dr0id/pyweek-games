# -*- coding: utf-8 -*-


#-------------------------------------------------------------------------------
import mainloop
import world
import view

def run():
    mainloop.App(world.World, view.View).run()
    


