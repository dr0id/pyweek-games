# -*- coding: utf-8 -*-

import pygame

import statemachine
import viewstates
import entitytypes

#-------------------------------------------------------------------------------
class View(object):
    def __init__(self, app):
        self.app = app
        
        self.statemachine = statemachine.StateMachine(self)
        self.event_queue = []
        
        # font handling and speed up
        self._font = None
        self._text_cache = {} # {text:surf}
        
        self.schow_fps = False
        self.do_clear_screen = True
        self.show_debug_vectors = False
        
        self.entities = {} # {ID: sprite}
        
        self.sprites = []
        self.front_to_back_sprites = []
        self.cam_mask = None
        self.cam_cursor = None
        
        self.double_screen = None
        self.saved_mouse_pos = None
        
        self.offset_x = 0
        self.offset_y = 0
        self.focus_offset_x = 0
        self.focus_offset_y = 0
        self.zoom_factor = 2
        self.zoom_move_factor = 0.5
        self.scaled_surfs = {}
        
        self.world_is_created = False
        

        self.screen = None
        self.pictures = []
        self.quality_image = None
        
    def init(self):
        self.screen = pygame.display.set_mode(self.app.screen_size)
        self.statemachine.change_state(viewstates.Intro)
        self.quality_image = self.app.load_image("quality.png")

    def on_events(self, events):
        self.event_queue.extend(events)
        
    def on_event(self, event):
        self.event_queue.append(event)
        
    def update(self, dt_seconds):
        self.statemachine.on_events(self.event_queue)
        
        self.statemachine.update(dt_seconds)
        
        
        # update screen buffers
        if self.schow_fps:
            w, h = self.app.screen_size
            self.draw_text(10, h - 20, str(self.app.clock.get_fps()))
            self.draw_text(10, 20, str(self.app.clock.get_fps()))
            self.draw_text(w - 110, 20, str(self.app.clock.get_fps()))
            self.draw_text(w - 110, h - 20, str(self.app.clock.get_fps()))
        pygame.display.flip()
        if self.do_clear_screen:
            self.screen.fill((255, 255, 255))
            
        
            
    #---- coord convert routines ----#
    
    def screen_to_world(self, screen_x, screen_y):
        return (self.offset_x + screen_x, self.offset_y + screen_y)
        
    def world_to_screen(self, world_x, world_y):
        return (world_x - self.offset_x, world_y - self.offset_y)
        
    def focus_to_world(self, fx, fy, zoom):
        return (fx/zoom + self.focus_offset_x, fy/zoom + self.focus_offset_y)
    
    def world_to_focus(self, wx, wy, zoom):
        return ((wx - self.focus_offset_x)*zoom, (wy - self.focus_offset_y)*zoom)
    
    #---- draw routines ----#
                
    def draw_text(self, posx, posy, text, overwrite=False):
        if text in self._text_cache and not overwrite:
            surf = self._text_cache[text]
        else:
            if not self._font:
                self._font = pygame.font.Font(None, 21)
            if len(self._text_cache) > 2000:
                print "Warning, text cache has more than 2000 entries, reseting"
                self._text_cache = {}
            surf = self._font.render(text, 0, (100, 100, 100))
            # surf = self._font.render(text, 0, (255, 255, 255))
            surf.set_alpha(128)
            surf = surf.convert_alpha()
            self._text_cache[text] = surf

        self.screen.blit(surf, (posx, posy))
        
    def create_sprite(self, entitytype, ID=None, x=0, y=0, layer=0, rotation=0, scale=None):
        
        if __debug__:
            if scale:
                assert scale > 0.0
        if ID in self.entities:
            return self.entities[ID]
        else:
            if entitytype in entitytypes.entity_props:
                props = entitytypes.entity_props[entitytype]
                image = self.app.load_image(props.image_name, scale)
                if ID is None:
                    ID = self.app.get_new_ID()
                sprite = Sprite(ID, props, image, x, y, layer, rotation, scale)
                self.entities[ID] = sprite
                return sprite
            else:
                if __debug__:
                    print "!!! unkown entity type", entitytype
                    image = self.app.load_image("", scale)
                    if ID is None:
                        ID = self.app.get_new_ID()
                    sprite = Sprite(ID, None, image, x, y, layer, rotation, scale)
                    return sprite
                else:
                    raise Exception("Entitytype unkown")
            
    def add_sprite(self, sprite):
        self.sprites.append(sprite)
        self.sprites.sort(key=lambda spr:spr.layer)
        
        self.front_to_back_sprites.append(sprite)
        self.front_to_back_sprites.sort(key=lambda spr:-spr.layer)
        
        self.entities[sprite.ID] = sprite
        
    def clear_sprites(self):
        self.sprites[:] = []
        self.front_to_back_sprites[:] = []
        self.entities.clear()
        
    def remove_sprite(self, sprite):
        try:
            self.sprites.remove(sprite)
            self.front_to_back_sprites.remove(sprite)
            del self.entities[sprite.ID]
        except:
            if __debug__:
                print "failed to remove sprite", sprite, sprite.ID
        
    def get_sprite_at(self, screen_pos, include_scroll=False, include_gui=False):
        for spr in self.front_to_back_sprites:
            if spr.screen_rect.collidepoint(screen_pos):
                if spr.layer > 100 and include_gui:
                    return spr
                elif spr.layer > 1.0 and include_scroll:
                    return spr
                elif spr.layer <= 1.0:
                        return spr
        return None
        
    def get_all_sprites_at(self, screen_pos, include_scroll=False, include_gui=False):
        sprites = []
        for spr in self.front_to_back_sprites:
            if spr.screen_rect.collidepoint(screen_pos):
                if spr.layer > 100 and include_gui:
                    sprites.append(spr)
                elif spr.layer > 1.0 and include_scroll:
                    sprites.append(spr)
                elif spr.layer <= 1.0:
                        sprites.append(spr)
        return sprites
        
    def get_all_sprites_of_type_at(self, screen_pos, type_name, include_scroll=False, include_gui=False):
        res = []
        for spr in self.get_all_sprites_at(screen_pos, include_scroll, include_gui):
            if spr.props.type == type_name:
                res.append(spr)
        return res
        
    def render(self, screen, special=None):
        screen_blit = screen.blit
        for sprite in self.sprites:
            if sprite.layer <= 1: # paralax scrolling sprites
            
                if special and special == sprite:
                    w, h = self.quality_image.get_size()
                    # qimg = pygame.transform.scale(self.quality_image, (w * self.zoom_factor / 16.0, h * self.zoom_factor / 16.0))
                    qimg = pygame.transform.scale(self.quality_image, (int(w * self.zoom_factor / 16.0 * sprite.layer), int(h * self.zoom_factor / 16.0 * sprite.layer)))
                    # qimg = pygame.transform.scale(self.quality_image, (w * self.layer, h * self.layer))
                    sr = qimg.get_rect()
                    sr.center = sprite.rect.center
                
                    x,y = sr.topleft
                    topleft = (x - (self.offset_x * sprite.layer), y)
                    screen_blit(qimg, topleft)
                else:
                    x,y = sprite.rect.topleft
                    sprite.screen_rect.topleft = x - (self.offset_x * sprite.layer), y
                    screen_blit(sprite.image, sprite.screen_rect.topleft)
                
            elif sprite.layer < 100: # sprites in front, scrolling, not sure
                sprite.screen_rect.topleft = self.world_to_screen(*sprite.rect.topleft)
                screen_blit(sprite.image, sprite.screen_rect)
            
            else: # sprites in front, fix to screen (gui elements)
                screen_blit(sprite.image, sprite.rect.topleft)
                sprite.screen_rect.topleft = sprite.rect.topleft

    def get_focus_region(self, screen, sharp=False):
        w, h = self.app.screen_size
        rect = pygame.Rect(0, 0, w/self.zoom_factor, h/self.zoom_factor)
        rect.center = self.saved_mouse_pos
        surf = pygame.Surface(rect.size)
        surf.blit(screen, (0,0), rect)
        
        # too sloooow
        # for i in range(self.zoom_factor/2):
            # sw, sh = surf.get_size()
            # double_size = (2*sw, 2*sh)
            # print '???', i, double_size
            # if double_size not in self.scaled_surfs:
                # print "??? creating surf", double_size
                # self.scaled_surfs[double_size] = pygame.Surface(double_size)
            # dest_surf = self.scaled_surfs[double_size]
            # pygame.transform.scale2x(surf, dest_surf)
            # surf = dest_surf
        # return surf
            
        if sharp:
            w, h = rect.size
            return pygame.transform.scale(surf, (w*self.zoom_factor, h*self.zoom_factor))
        return pygame.transform.rotozoom(surf, 0, self.zoom_factor)
        
        
#-------------------------------------------------------------------------------


class Sprite(object):
    
    def __init__(self, ID, props, image, x, y, layer=0, rotation=0, scale=1.0):
        self.props = props
        self.image = image
        self.rect = image.get_rect()
        self.rect.center = (x, y)
        self.layer = layer
        self.rotation = rotation
        self.scale = scale
        self.ID = ID
        self.screen_rect = image.get_rect()

#-------------------------------------------------------------------------------

class Picture(object):
    
    def __init__(self, image, quality, name, spr, timestamp=None):
        self.image = image
        self.quality = quality
        self.timestamp = timestamp

            