# -*- coding: utf-8 -*-

import eventtypes
import viewstates
import entitytypes

#-------------------------------------------------------------------------------
class Intro(object):

    @staticmethod
    def enter(world):
        if __debug__:
            print "enter Intro"
        pass
        
    @staticmethod
    def exit(world):
        pass
        
    @staticmethod
    def on_message(world, message):
        pass
        
    @staticmethod
    def on_events(world, event):
        if event.type == eventtypes.BUTTONCLICKED:
            if event.buttonID == 0:
                newevent = eventtypes.Event()
                newevent.type = eventtypes.CHANGESTATE
                newevent.state = viewstates.Instructions
                world.app.view.on_event(newevent)
                world.statemachine.change_state(Instructions)
                return True
        return False
                
        
    @staticmethod
    def execute(world, dt_seconds):
        pass
#-------------------------------------------------------------------------------
class Instructions(object):

    @staticmethod
    def enter(world):
        if __debug__:
            print "enter Intro"
        pass
        
    @staticmethod
    def exit(world):
        pass
        
    @staticmethod
    def on_message(world, message):
        pass
        
    @staticmethod
    def on_events(world, event):
        if event.type == eventtypes.BUTTONCLICKED:
            if event.buttonID == 0:
                newevent = eventtypes.Event()
                newevent.type = eventtypes.CHANGESTATE
                newevent.state = viewstates.GamePlay
                world.app.view.on_event(newevent)
                world.statemachine.change_state(GamePlay)
                return True
        return False
                
        
    @staticmethod
    def execute(world, dt_seconds):
        pass
        
#-------------------------------------------------------------------------------

class GamePlay(object):

    @staticmethod
    def enter(world):
        if __debug__:
            print "enter gameplay"
        world.create_entity(entitytypes.OWL, 50, 50, 1, 0, 0.5)
          
    @staticmethod
    def exit(world):
        if __debug__:
            print "exit gameplay"
        
    @staticmethod
    def on_message(world, message):
        pass
        
    @staticmethod
    def on_events(world, event):
        if __debug__:
            print "world gameplay event", event
        # if event.type == eventtypes.CHANGEFOCUS :
            # newevent = eventtypes.Event()
            # newevent.type = eventtypes.CHANGESTATE
            # newevent.state = viewstates.Focused
            # world.app.view.on_event(newevent)
            
        
    @staticmethod
    def execute(world, dt_seconds):
        for ent in world.entities.values():
            ent.x += 20 * dt_seconds
            world.send_update_entity_event(ent)
        
#-------------------------------------------------------------------------------

