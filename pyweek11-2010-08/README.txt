Pyweek 11 - "Caught"
Team: Alpha-Place
Game: Caught on picture

Prerequisites:

    pygame
        http://www.pygame.org


How to Run:

    Execute run_game.py from the console

                   or

    Double-click run_game.pyw


How to Play:

    

Licenses:

    Rustle.ogg and rustle2.ogg are based on the recording of freesound.org user Spleencast (Creative Commons Sampling Plus 1.0 License)
    Wind.ogg is based on the recording of freesound.org user surrey_film. (Creative Commons Sampling Plus 1.0 License)