# -*- coding: utf-8 -*-

"""
This module contains transformations involving pygame.Surface.
"""
from __future__ import print_function, division

import base64
import logging
import time
import zlib

import pygame

logger = logging.getLogger(__name__)
logger.debug("importing...")

# __all__ = [""]


RED_FACTOR = 0.299
GREEN_FACTOR = 0.587
BLUE_FACTOR = 0.114

# HDTV color factors
RED_FACTOR = 0.2126
GREEN_FACTOR = 0.7152
BLUE_FACTOR = 0.0722

assert RED_FACTOR + GREEN_FACTOR + BLUE_FACTOR < 1.0 + 0.005, "color factors summed should be 1.0 but are " + str(
    RED_FACTOR + GREEN_FACTOR + BLUE_FACTOR)
assert RED_FACTOR + GREEN_FACTOR + BLUE_FACTOR > 1.0 - 0.005, "color factors summed should be 1.0 but are " + str(
    RED_FACTOR + GREEN_FACTOR + BLUE_FACTOR)

GREY_PALETTE = [(i, i, i) for i in range(255)]
COLOR_FACTORS = (int(round(RED_FACTOR * 255)), int(round(GREEN_FACTOR * 255)), int(round(BLUE_FACTOR * 255)))

assert sum(COLOR_FACTORS) < (1.0 + 0.005) * 255, "color factors summed should be 255"


def to_grey_scale_accurate(color_surf, r_factor=RED_FACTOR, g_factor=GREEN_FACTOR, b_factor=BLUE_FACTOR,
                           dest=None):
    if dest is None:
        dest = color_surf.copy()

    assert dest is not color_surf, "dest and surf are the same! they should not!"
    assert dest.get_size() == color_surf.get_size(), "surfaces have not same dimensions!"

    sw, sh = color_surf.get_size()
    color_surf_get_at = color_surf.get_at
    dest_set_at = dest.set_at
    color_surf.lock()
    dest.lock()
    for y in range(sh):
        for x in range(sw):
            c = color_surf_get_at((x, y))
            grey = int(round(c[0] * r_factor + c[1] * g_factor + c[2] * b_factor))
            grey = grey if grey <= 255 else 255
            dest_set_at((x, y), (grey, grey, grey, c[3] if len(c) == 4 else 255))
    dest.unlock()
    color_surf.unlock()
    return dest


def to_grey_scale(color_surf, color_factors=COLOR_FACTORS, dest=None, palette=GREY_PALETTE, intensity=3):
    temp1 = color_surf.copy()
    # this adjusts the color values
    temp1.fill(color_factors, None, pygame.BLEND_RGB_MULT)

    if dest is None:
        dest = pygame.Surface(color_surf.get_size(), 0, 8)
        dest.set_palette(palette)

    assert dest is not color_surf, "dest and surf are the same! they should not!"
    assert dest.get_size() == color_surf.get_size(), "surfaces have not same dimensions!"

    # preserve alpha
    alpha = color_surf.copy()
    alpha.fill((0, 0, 0, 255), None, pygame.BLEND_RGBA_MULT)

    # blit 3 times because a color blit to a 8 bit surface does (r+g+b) / 3
    # this is not that accurate, but almost accurate
    dest.blit(temp1, (0, 0), None, pygame.BLEND_RGB_ADD)

    for n in range(intensity):
        alpha.blit(dest, (0, 0), None, pygame.BLEND_RGB_ADD)
    # alpha.blit(dest, (0, 0), None, pygame.BLEND_RGB_ADD)
    # alpha.blit(dest, (0, 0), None, pygame.BLEND_RGB_ADD)
    return alpha


def image_to_string(image, format='RGBA', level=9):
    """
    Converts the image into a compressed, base64 encoded string.
    """
    str_img = pygame.image.tostring(image, format)
    w, h = image.get_size()
    str_img = '|'.join((format, str(w), str(h), str_img))
    if level:
        str_img = zlib.compress(str_img, level)
    return base64.b64encode(str_img)


def string_to_image(input_str, flipped=False):
    """
    Converts a string encoded previously with :py:func:`image_to_string()` back to an image.
    """
    input_str = base64.b64decode(input_str)
    try:
        input_str = zlib.decompress(input_str)
    except:
        pass
    format, w, h, str_img = input_str.split('|', 3)
    size = (int(w), int(h))
    return pygame.image.fromstring(str_img, size, format, flipped)


#   http://www.akeric.com/blog/?p=720

def blur_surf(surface, amount_x, amount_y=None, border=None, background=(0, 0, 0, 0)):
    """
    From: http://www.akeric.com/blog/?p=720

    Blur the given surface by the given 'amount'.  Only values 1 and greater
    are valid.  Value 1 = no blur.

    """

    amount_y = amount_y or amount_x  # use x value if y is not set

    if amount_x < 1.0:
        raise ValueError("Arg 'amount_x' must be greater than 1.0, passed in value is %s" % amount_x)
    if amount_y < 1.0:
        raise ValueError("Arg 'amount_y' must be greater than 1.0, passed in value is %s" % amount_y)

    width, height = surface.get_size()
    if border:
        width += 2 * border
        height += 2 * border
        border_surface = pygame.Surface((width, height), pygame.SRCALPHA)
        border_surface.fill(background)
        border_surface.blit(surface, (border, border))
        surface = border_surface

    scale_x = 1.0 / float(amount_x)
    scale_y = 1.0 / float(amount_y)
    min_pixel = 1
    scale_size = (int(round(max(min_pixel, width * scale_x))), int(round(max(min_pixel, height * scale_y))))
    # print '? scale', scale_x, scale_y, 'scale size:', scale_size

    # surf = pygame.transform.scale(surface, scale_size)
    surf = pygame.transform.smoothscale(surface, scale_size)
    return pygame.transform.smoothscale(surf, (width, height))

    # surf = pygame.transform.rotozoom(surface, 0, scale_x)
    # w, h = surf.get_size()
    # print 'new', width / float(w), 'amount', amount_x
    # return pygame.transform.rotozoom(surf, 0, width / float(w))
    # # return pygame.transform.rotozoom(surf, 0, amount_x)


def box_blur(surface, box_size, iterations=1, border=None, background=(0, 0, 0, 0)):
    """
    This is a box blur function.
    :param surface: The surface to blur.
    :param box_size: The size of the box to use to blur.
    :param border: The border to add.
    :param background: The background color to use for the border.
    :param iterations: Number of iterations.
    :return: Blurred image.
    """
    width, height = surface.get_size()
    if border:
        width += 2 * border
        height += 2 * border
        border_surface = pygame.Surface((width, height), pygame.SRCALPHA)
        border_surface.fill(background)
        border_surface.blit(surface, (border, border))
        surface = border_surface
    bw, bh = box_size
    box = pygame.Rect(0, 0, bw, bh)
    surf_rect = surface.get_rect()
    for i in range(iterations):
        dest = surface.copy()
        for x in range(surf_rect.width):
            for y in range(surf_rect.height):
                # for x in range(-bw // 2 + 1, surf_rect.width + bw // 2 - 1):
                #     for y in range(-bh // 2 + 1, surf_rect.height + bh // 2 -1):
                box.center = (x - 1, y - 1)
                if box.colliderect(surf_rect):
                    c = pygame.transform.average_color(surface, box)
                    dest.set_at(box.center, c)
        surface = dest.copy()
    return dest


logger.debug("imported")





if __name__ == '__main__':

    def draw_blurs(blurs, w, h, radius, screen):
        screen.fill((0, 0, 0))
        screen.blit(original, (0, 0))
        info = f"size: {w}, {h} radius: {radius}"
        label = font.render(info, True, (255, 255, 255))
        screen.blit(label, (0, 0))
        for idx, bt in enumerate(blurs):
            b, l = bt
            x = idx % len(box_sizes) * w
            y = idx // len(box_sizes) * h + h
            screen.blit(b, (x, y))
            screen.blit(l, (x, y))
        pygame.display.flip()


    pygame.init()
    pygame.display.set_caption("box blur demo - be patient")
    screen = pygame.display.set_mode((1280, 1024))
    font = pygame.font.Font(None, 15)

    w = 100
    h = 100
    radius = 20

    original = pygame.Surface((w, h))
    pygame.draw.circle(original, (255, 255, 255), original.get_rect().center, radius)

    box_sizes = [(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (11, 11), (21, 21), (42, 42), (50, 50), (31, 5), (3, 31)]
    iterations_to_test = [1, 2, 3, 4, 5, 6]
    blurs = []

    for it in iterations_to_test:
        for bw, bh in box_sizes:
            start = time.time()
            blurred = box_blur(original, (bw, bh), iterations=it)
            diff = time.time() - start
            pygame.draw.circle(blurred, (255, 255, 255), original.get_rect().center, radius, 1)
            info = f"box:{bw}x{bh} itr:{it}"
            label = font.render(info, True, (255, 255, 255))
            print(f"{info} t:{diff:.3}s")
            blurs.append((blurred, label))
            draw_blurs(blurs,w, h, radius, screen)

    running = True
    while running:
        draw_blurs(blurs,w, h, radius, screen)
        for e in [pygame.event.wait()]:
            if e.type == pygame.QUIT:
                running = False
