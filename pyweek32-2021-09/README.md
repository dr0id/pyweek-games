# README #

This repo is created for pyweek 32. See [pyweek.org](https://pyweek.org) for more info.

### What is this repository for? ###

* This is a game create for the pyweek 32 challenge.

### How do I get set up? ###

1. download [python](https://www.python.org). Version 3.9.7 was used for development.
2. pip install pygame (version 2.0.1 was used for development). Open command line and execute (or similar):

       'python -m pip install pygame' 
       win: 'py -m pip install pygame'

3. run the game using 'python run_game.py' on the command line. 


