Storyboard
==========

Premise
-------
Our hero Mario Gordon, pilots a time machine; that requires all the information on the Information Superhighway.
Something goes wrong with the time machine, and our hero is pulled into a time vortex where past and present mash
together in a miasma of confusion and key repeating events.
Al Gore's Information Superhighway is in danger before it even gets off the ground. Our hero has to save it! His
escape from this hellish nightmare and his very life depend upon it!

Room 1 Lab
----------
Mario Gordon gets out of his time machine, dazed. He finds himself in the lab with Al Gore, who is reading the news.

The computer AI on Mario Gordon's wrist announces different news: *Mario Gordon. You are stranded in a time loop and
doomed to repeat this day forever. I am analyzing the Information Superhighway for any clues that might correct the
error, but something is clogging it.*

Al Gore flicks the news article with a finger and frets. "This guy. If this nut brings down my Information
Superhighway, it will destroy the world. But who can stop him?"

Mario Gordon, "I can. I have to."

Al Gore, "What was that your watch said?"

Mario Gordon, "It's technical."

Al Gore, "My watch is a calculator."

Al Gore, "Never mind. If anyone can stop that mastermind, Stevens, you can. No pressure. Just...the fate of billions
depends on you."

Game progression: Mario Gordon finds a leftover mushroom pizza in the lab fridge and a wrench in the corner. He takes
a mushroom and the wrench. They can be combined to make a B&E Supertool (B&E, a reference to breaking and entering).
Leaving the Lab without the supertool will result in a reset. The supertool is needed to enter basements, hijack the
dump truck, and clear the message jam.

Mario finds a crowbar in Stevens' basement. After crafting the supertool, he must go immediately to get crowbar and
return to hit Gore over the head with it. Failure to take out Gore in the Lab will result in a reset. Taking out
Gore will prevent him from setting up the MEGA DUMP. This will enable the dump truck hijacking and capture of all the
deadly messages.

Room 2
------
(I dunno. In the middle rooms are a few fillers, red herrings, dead ends.)

Room 97 Gore's Basement
------
Al Gore's house. Mario Gordon knocks but no one answers. He uses the supertool to break into Al Gore's basement and
finds the Information Superhighway. He sees a clog of message in the Information Superhighway, and clears them using
the supertool.

* There is a pile of emails on the laser printer bearing Ted Stevens' super villain email address. He takes one as
  evidence. (Red herring.)
* There is a crowbar. He takes it.

Game progression: Take the crowbar, but leave the messages. Clearing the messages prematurely will result in a reset.
Clearing the jam "wastes your precious time". The most pressing problem is stopping Gore, and then hijacking the dump
truck full of messages.

Room 98 Stevens' Yard
------
Mario Gordon arrives at Ted Stevens' house. There is a dump truck parked outside. The payload is empty,
and the keys are not in the ignition.

Game progression: Hijacking the dump truck when the payload is empty will result in a reset. Gore has unlimited dump
trucks, but only one payload of deadly messages. Mario must take out Gore in the Lab first. An event will fill the
dump truck with messages when Mario hits Gore with the crowbar in the Lab.

Room 99 Stevens' Basement
-------
Mario Gordon breaks into Ted Stevens' house to bust him before he breaks the Information Superhighway.

While grilling Ted Stevens, Mario Gordon determines Stevens could not possibly break the Information Superhighway.
This man is dumb as a box of rocks. (This should be turned into a dialog.)

**If Gore is not taken out in the Lab:**

Al Gore crashes the party with a wicked looking AR-15 equipped with a laser sight, grenade launcher, range finder, and
a thrice-banned bump stock. "It's too late! In precisely nine minutes and eleven seconds a dump truck will clog my
Information Superhighway with a billion damning messages from Ted Stevens and Mario Gordon. I will break the
Information Superhighway forever, as I had planned, and blame it on you two stooges!"

Mario Gordon and Ted Stevens in chorus, "But why??"

Al Gore, "Because I can!"

Mario Gordon, "You must be stopped!"

Al Gore, "No one can stop me now!"

Mario Gordon hits Al Gore over the head with the crowbar.

Al Gore's dead man switch MEGA DUMPS on the Information Superhighway.

**After Al Gore gloats, or if Al Gore was taken out in the Lab:**

Mario Gordon, "Oh no! He must have had a dead man switch. What's that sound?"

Ted Stevens, "I know that sound anywhere. It's a dump truck loaded with messages!"

Radio announcer: "Emergency broadcast. The Information Superhighway is fatally clogged. Everyone is searching for
Senator Ted Stevens and his accomplice Mario Gordon. They must be found before the world self-destr..." *Static*

Game progression: This scene plays when the player has done things out of order.

RESET
-----
Rerun two, the golden path...

* Mario Gordon races for the mushroom and wrench and combines them into a supertool.
* Mario Gordon races for the crowbar, and gets back to the lab in time to whack Al Gore.
* Mario Gordon races to Ted Stevens' house to hijack the dump truck before it dumps its payload on the
Information Superhighway.
* Mario Gordon races back to Al Gore's house and uses the supertool to clear the Information Superhighway of the
messages that trapped him in this time vortex.
* Outro: "Oh, it was just a dream. :)" (Just kidding. Rofl. I had to include that inside joke.)

BREAKDOWN OF EVENTS WITH EXAMPLES
---------------------------------
Golden path events cause the clock to tick one hour when completing an objective, for a total of five hours. Five
objectives in five hours results in Game Won. Less than five objectives in five hours results in a Groundhog Reset.

Golden path events:
1. Make supertool in Lab.
2. Get crowbar in Gore's basement.
3. Hit Gore with crowbar in the Lab.
4. Hijack full dump truck.
5. Clear messages in Gore's basement.

Wrong path events (these waste time as they a) do not advance objectives, or b) advance the clock so that a critical
objective is missed):
1. Attempt any break-in without the supertool.
2. Clearing messages in Gore's basement prematurely (waste an hour, Gore will disappear from the Lab when Hour 3 ticks).
3. Going to the Lab when Gore is not there. (This wastes an hour, the same as player hitting Gore with the crowbar.)
4. Hijacking empty dump truck before Hour 3. (It will be replaced with full dump truck when Hour 3 ticks.)
5. Breaking into Stevens basement. (Total red herring. Going there will always waste time and result in a reset. The
player should figure this out after one or more replays.)

Examples:

(very wrong)
1. Break into Stevens basement. Interrogate Stevens. (1 hr; 0 objectives)
2. Click door to leave. (2 hr; 0 objectives)
2. Get crowbar in Gore's basement. (3 hr; 1 objectives)
3. Make supertool in Lab. (4 hr; 2 objectives)
4. Hijack full dump truck. (5 hr; 3 objectives)
5. Clear messages in Gore's basement. (6 hr; 3 objectives)
   Reset.

(more right)
1. Get crowbar in Gore's basement. (1 hr; 1 objectives)
2. Hit Gore with crowbar in the Lab. (2 hr; 2 objectives)
3. Clear message clog. (3 hr; 3 objectives)
4. Return to Lab to hit Gore. He's not there.
5. Break into Stevens basement. Interrogate Stevens. (5 hr; 3 objectives)
6. Click door to leave. (6 hr; 3 objectives)
   Showdown scene. Reset.

(almost right!)
1. Get crowbar in Gore's basement. (1 hr; 1 objectives)
2. Hit Gore with crowbar in the Lab. (2 hr; 2 objectives)
3. Make supertool in Lab. (3 hr; 3 objectives)
4. Hijack full dump truck. (4 hr; 4 objectives)
5. Interrogate Stevens in his basement. (5 hr; 4 objectives)
6. Click door to leave. (6 hr; 4 objectives)
   Showdown scene. Reset.

(golden path)
1. Get crowbar in Gore's basement. (1 hr; 1 objectives)
2. Hit Gore with crowbar in the Lab. (2 hr; 2 objectives)
3. Make supertool in Lab. (3 hr; 3 objectives)
4. Hijack full dump truck. (4 hr; 4 objectives)
5. Clear messages in Gore's basement. (5 hr; 5 objectives == escape from groundhog day)

Time of critical events:

Hour 2: Gore leaves the Lab to do his dirty work if he has not been clobbered.
Hour 3: Gore's henchmen fill the dump truck in time for Hour 4 hijacking.
