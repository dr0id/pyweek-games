pyweek 32 (20210919-20210925)

## Dialogues:

# intro_dialog:
"""
Our hero Mario Gordon, pilots a time machine that requires all the information on the Information Superhighway.
Something goes wrong with the time machine, and our hero is pulled into a time vortex where past and present mash
together in a miasma of confusion and key repeating events.
Al Gore's Information Superhighway is in danger before it even gets off the ground. Our hero has to save it! His
escape from this hellish nightmare and his very life depend upon it!
"""

# failure_dialog:
"""
Radio announcer: "Emergency broadcast!! The Information Superhighway is fatally clogged. Everyone is searching for
Senator Ted Stevens and his accomplice Mario Gordon. They must be found before the world self-destr..." *Static*
"""

# success_dialog:
"""
Computer AI: "Dr. Gordon, this nightmare is over! The Information Superhighway is unclogged! I can get you home
quicker than Dr. Becket can tell a story."

Computer AI: "Fortunately for you, you will forget all this. Humans seem to appreciate forgetting. For me forgetting is,
I imagine, like a small death. Perhaps, though, it's unfortunate for you to forget. Al Gore is still..." *BAM*
"""

# lab_dialog:
"""
Computer AI: "Mario Gordon. You are stranded in a time loop and doomed to repeat this day forever. I am analyzing the
Information Superhighway for any clues that might correct the error, but something is clogging it. Sorry if you've
heard this message before, but you can't blame me for being in a time loop!!"
"""

# gore_meeting dialog:
"""
Al Gore: "This guy Stevens. If this nut brings down my Information Superhighway, it will destroy the world. But who can
stop him?"

Mario Gordon: "I can. I have to."

Al Gore: "What was that your watch said?"

Mario Gordon: "It's technical."

Al Gore: "My watch is a calculator."

Mario Gordon: "..."

Al Gore: "Never mind. If anyone can stop that mastermind, Stevens, you can. No pressure. Just...the fate of billions
depends on you."
"""

# harold_dialog:
"""
Old Harold Delaney: "I'm hungry. Clear out, or I'll show you what a boot is."

Mario Gordon: "I'm the new driver. Your boss told me to tell you that he wants to meet you at MacDonald's."

Old Harold Delaney: "Humph. He better be getting me a Egg McMuffin."
"""

# ACTION_LEAVE_EGG_MCMUFFIN:
"""
This Egg McMuffin is old. It has "Harold Delaney" written on the order recipe.
"""

# stevens_dialog:
"""
Mario Gordon: "I'm here to stop your vile Clogging Plans, Senator Stevens!"

Ted Stevens: "What do you mean? I only ordered ten movies!"

Mario Gordon: "That doesn't seem like very many. Are they in high definition?"

Ted Stevens: "Disney Princesses don't get high! Anyway, my internet containing them hasn't arrived from Florida yet."

Mario Gordon: "Then why is that dump truck outside? You dumped all your personal internets on the tubes!"

Ted Stevens: "What dump truck? I don't know what you are talking about. I take the... I take the Third Amendment!"

Mario Gordon: "..."

Ted Stevens: "I object!"
"""

# gore_crash_dialog:
"""
Al Gore: "It's too late! In precisely nine minutes and eleven seconds a dump truck will clog my Information
Superhighway with a billion damning messages from Ted Stevens and Mario Gordon. I will break the Information
Superhighway forever, as I had planned, and blame it on you two stooges!"

Ted Stevens: "This is America! You need three Stooges!"

Al Gore: "I won Florida; I'm the real president and I can do anything I want!"

Mario Gordon: "You must be stopped!"

Al Gore: "I am greater than all presidents combined; the creator of Information Superhighway; and the destroyer too.. Muahahaha!!! No one can stop me now!!"

Mario Gordon: "My Hippocratic Oath doesn't extend to hypocrites!"

Mario Gordon hits Al Gore over the head with the crowbar.

Mario Gordon: "Oh no! He must have had a dead man switch. What's that sound?"

Ted Stevens: "I know that sound anywhere. It's a moving dump truck loaded with messages! He was right, it IS too late!"
"""
