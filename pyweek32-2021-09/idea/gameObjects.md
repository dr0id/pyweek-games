pyweek 32 (20210919-20210925)

## Rooms:
# KIND_ROOM_LAB:
background_img = lab.png
Items:
    KIND_WRENCH_ITEM
        SPRITE = wrench.png
        POSITION = (865, 333)
    KIND_FRIDGE
        ACTION_OPEN_FRIDGE
        SPRITE = open_fridge.png
        POSITION = (142, 151)
    KIND_PIZZA_ITEM
    KIND_MUSHROOM_ITEM
    KIND_SUPERTOOL_ITEM
    KIND_AL_GORE_ITEM
        SPRITE = al_gore_sit.png
        POSITION = (-38, 171)
Exposition: lab_dialog

# KIND_ROOM_STEVENS_YARD:
background_img = stevens_yard.png
Items:
    door to basement:
        SPRITE = yard_door_to_basement.png
        POSITION = (48, 435)

    KIND_DUMP_TRUCK_ITEM
        SPRITE = dumptruck.png
        POSITION = (357, 5)

    dumptruck_dirt:
        SPRITE = dumptruck_dirt.png
        POSITION = (664,15)

    KIND_HAROLD_ITEM
        SPRITE = harold.png
        POSITION = (524, 235)

    signpost:
        SPRITE = singpost.png
        POSITION = (939, 482)


# KIND_ROOM_STEVENS_BASEMENT:
background_img = stevens_basement.png
Items:
    KIND_TED_STEVENS_ITEM
    POSITION = (324, 210)
    SPRITE = stevens.png

    KIND_FAKE_CLOG_ITEM
    POSITION = (21, 245)
    SPRITE = fake_clog.png

    Al_GORE_STAND
    POSITION = (652, 210)
    SPRITE = al_gore_stand.png

    stevens_basement_door
    POSITION = (UNCHANGED)
    SPRITE = stevens_basement_door.png


# KIND_ROOM_GORE_BASEMENT:
background_img = gore_basement.png
Items:
    KIND_CROWBAR_ITEM
        SPRITE = crowbar.png
        POSITION = (260, 66)
    KIND_MESSAGE_CLOG_ITEM
    (311, 221) to (540, 444)?
    KIND_EMAIL_PILE_ITEM
        SPRITE = email_pile.png
        POSITION = (96, 382)
    KIND_EGG_MCMUFFIN_ITEM
        SPRITE = egg_mcmuffin.png
        POSITION = (514, 259)

## Items to Add to settings.py, *etc:
    KIND_AL_GORE_ITEM = _global_id_generator.next("KIND_AL_GORE_ITEM")
        ACTION_TALK_GORE = _global_id_generator.next("ACTION_TALK_GORE")
    KIND_HAROLD_ITEM = _global_id_generator.next("KIND_HAROLD_ITEM")
        ACTION_TALK_HAROLD = _global_id_generator.next("ACTION_TALK_HAROLD")
    KIND_TED_STEVENS_ITEM = _global_id_generator.next("KIND_TED_STEVENS_ITEM")
        ACTION_TALK_STEVENS = _global_id_generator.next("ACTION_TALK_STEVENS")
    KIND_FAKE_CLOG_ITEM = _global_id_generator.next("KIND_FAKE_CLOG_ITEM")
        ACTION_CLEAR_FAKE_CLOG = _global_id_generator.next("ACTION_CLEAR_FAKE_CLOG")
    KIND_EMAIL_PILE_ITEM = _global_id_generator.next("KIND_EMAIL_PILE_ITEM")
        ACTION_GET_EMAIL_EVIDENCE = _global_id_generator.next("ACTION_GET_EMAIL_EVIDENCE")
    KIND_EGG_MCMUFFIN_ITEM = _global_id_generator.next("KIND_EGG_MCMUFFIN_ITEM")
        ACTION_LEAVE_EGG_MCMUFFIN = _global_id_generator.next("ACTION_LEAVE_EGG_MCMUFFIN")

# Characters Voices:
    MC - Mario Gordon.
    Computer AI
    Radio Announcer
    Narrator
    KIND_AL_GORE_ITEM
    KIND_HAROLD_ITEM
    KIND_TED_STEVENS_ITEM
