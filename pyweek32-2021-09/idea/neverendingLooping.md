pyweek 32 (20210919-20210925)

### neverending (theme that won the voting)

![neverendingLooping.png](neverendingLooping.png)

Questions:

1. Solving a mystery?
2. Why the loop?
3. Limit the branches! Alter the timeline?
4. Should it reset if the wrong branch ist taken? No, let the time run out!
   Does the player have to re-do all the things?
5. Just learn something at an event that is needed later, e.g.
   - An observation
   - info to use (e.g. a pin for a door/vault)
6. Story? Setting? Office? How many rooms?
7. How does the player know what to do?
8. Influence the outcome of an event, so it counts for the escape
   (e.g. like in the Groundhog day he has to help => you need
   (ev1: true, ev2: true, ev3: true, ...) to 'win' the game and break the loop
9. What could these events be?
10.


Story Spoilers/Rules.
Day 1 - 3pm. MC Backs up Neural Network.
Day 5 - 5pm. MC tries to send Brain Data to Berkley Computers.
   Area 51 connects Alien Computer to internet.
   As Brain Data is going through the internet, it goes through the Alien Computer and gets stored.
Whenever MC dies: Alien Computer, being a Faster Than Light, uploads MC Brain Data to Day 1 - 3pm.

Ways to end the Loop:
   1. Break the Alien Computer in Area 51. (Bad-End)
   2. Unclog the internet so the MC Brain can be re-upload to the MC on Day 5 at 5pm.

Story Mechanics to Keep the MC on Target/Under Pressure:
   1. Scientist refuse to listen to MC if he talks about time travel as it would invalidate the experiment.
   2. Area 51 Guards have notified and tracked that the Alien Computer has turned itself on and has uploaded data to MC House. At ~30min Past Upload (i.e. Day 1, 3:30), there are guards outside the MC Apartment. By Day 5, the guards have caught up to MC with a shoot-first-question-later order, no matter where he is.
