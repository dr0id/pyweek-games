pyweek 32 (20210919-20210925)

# Locations:
Boston, MA:
   MIT Lab.
   MC Apartment.

Area 51, AZ: Revealed by Discovering "Who Issued the BOLO?"
   O.Donnell's. MacDonald's proxy. Home of the Egg O'Muffin.
   O'Donnell's Interior.
   UFO Dave's Campervan Interior.
   Guard House.
   Fence. Revealed by talking to UFO Dave.
   Hanger. Revealed by distracting Old Harold Delaney, or Cutting Fence with [Bolt Cutters].


# Items:
   [Rat Trap]. MC Apartment.
      Powered by [Power Source]. Will kill MC if it is touched while live power (Immediate Trigger of Reset to show players the premise of the game). 
   [Power Source]. MC Apartment.
      "Provides power for an appliance."
   [Microwave Transmitter]. UFO Dave's Campervan.
      "This looks dangerous but could cook a burrito very quickly"
      Can be overloaded with the [Power Source] to create an EMP.
   [Bolt Cutters]. UFO Dave's Campervan.
   [Egg O'Muffin]. O'Donnell's Interior.
   [Grenades]. Guard House.
   [Alien Computer]. Hanger.
      Can be destroyed with Grenades or EMP.

# Characters:
   MC - Mario Gordon.
   Dr. Williams. Scientist at MIT.
   Dr. Thaddeus Cahill. Scientist at MIT.
   UFO Dave. UFO enthusiast. Reveals Fence Location via dialog.
   Old Harold Delaney. Guard at Area 51.
      Will ask you to get him an Egg O'Muffin if you talk to him about Scrabble.
      Will get distracted if you bring him an Egg O'Muffin.

# Discoveries:
   "Who Issued the BOLO?" - Overhear/Encounter the Cops. MC Apartment.
