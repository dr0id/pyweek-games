# -*- coding: utf-8 -*-
"""
This it the main module. This is the main entry point of your code (put in the main() method).
"""
from __future__ import division, print_function

import logging
import random

import pygame

from gamelib import settings
from gamelib import resources
from gamelib.context_devmenu import DevMenuContext
from gamelib.context_global import GlobalContext
from gamelib.context_intro import IntroContext
from gamelib.context_load_resources import ResourceLoaderContext
from gamelib.game_state import GameState
from gamelib.resources import resource_def
from pyknic import context
from pyknic.pyknic_pygame.context import PygameInitContext, AppInitContext
from pyknic.pyknic_pygame.sfx import MusicPlayer

logger = logging.getLogger(__name__)


def main(log_level):
    # put here your code
    import pyknic
    logging.getLogger().setLevel(log_level)
    pyknic.logs.print_logging_hierarchy()

    random_state = random.getstate()
    logger.info(f"Set random seed state to: {random_state}")

    context.push(PygameInitContext(_custom_display_init))
    music_player = MusicPlayer(settings.master_volume, settings.music_ended_pygame_event)
    context.push(AppInitContext(music_player, settings.path_to_icon, settings.title))

    # order of context
    game_state = GameState()
    # context.push(GameplayContext(game_state))
    if settings.show_dev_menu:
        context.push(DevMenuContext(game_state, music_player))
    else:
        global_context = GlobalContext(game_state, music_player)
        context.push(global_context)
        global_context.load_room(False)  # hack to get the first room after init
        context.push(IntroContext())
    context.push(ResourceLoaderContext(resource_def))

    music_player.fill_music_carousel([resources.resource_def[resources.resource_music_track_1]])
    music_player.start_music_carousel()

    screen = pygame.display.get_surface()  # hack!
    clock = pygame.time.Clock()
    sim_time = 0.0
    context.set_deferred_mode(True)
    while context.length():
        top = context.top()
        if top:
            dt = clock.tick()
            dt /= 1000.0  # convert to seconds
            sim_time += dt
            top.update(dt, sim_time)
            top.draw(screen)
        context.update()  # handle deferred context operations

    logger.debug('Finished. Exiting.')


def _custom_display_init(display_info, driver_info, wm_info):
    accommodated_height = int(display_info.current_h * 0.9)
    if accommodated_height < settings.screen_height:
        logger.info("accommodating height to {0}", accommodated_height)
        settings.screen_height = accommodated_height  # accommodate for title bar and task bar ?
    settings.screen_size = settings.screen_width, settings.screen_height
    logger.info("using screen size: {0}", settings.screen_size)

    # initialize the screen
    settings.screen = pygame.display.set_mode(settings.screen_size, settings.screen_flags)

    # mixer values to return
    frequency = settings.MIXER_FREQUENCY
    channels = settings.MIXER_CHANNELS
    mixer_size = settings.MIXER_SIZE
    buffer_size = settings.MIXER_BUFFER_SIZE
    # pygame.mixer.pre_init(frequency, mixer_size, channels, buffer_size)
    return frequency, mixer_size, channels, buffer_size


# this is needed in order to work with py2exe
if __name__ == '__main__':
    main(settings.log_level)
