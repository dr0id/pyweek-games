"""spacez - a spatialhash module built for speed

Spacez is an unbounded spacial hash with collision detection. The space will grow and shrink on demand. The buckets are
dynamically created and destroyed, as needed. If you need bounds (e.g. to remove out-of-bound objects) you must manage
this externally or subclass Spaces and add the logic.

One important rule: each entity must have a 'rect' attribute that is a pygame Rect.

This module provides multiple interfaces:
1. A module-level functional interface that operates on a SpaceTuple.
2. The SpacezTuple interface which contains all the data and functions in its attributes.
3. The Spacez traditional object-style interface.

All of the interfaces provide about the same performance. In general the function defaults provide the best
performance, but some use cases may benefit from extra features. (Even with all features in use, this module
performs significantly better than Gummbum's SpatialHash. This is likely due to a reduction in bytecodes and
the heavy use of builtin types.)

Tip: collidepairs() is generally a little faster than collidedict().

Tip: The size argument should be large enough to hold a few objects. If you make them too big, you may get large
clusters. Make them too small and you'll have many objects touching multiple buckets. Both cases may hurt performance.

Tip: You can have fast adds or fast removals. There is a trade off, in that fast removal needs to build the
lookup table during add() and addlist(). The default favors adds. The decision should depend on whether the
entities being added contain many duplicates. If speed is a concern, the build_entity_map=False optimization
is best served by a design that avoids using remove(): i.e. keeps unique entity sets, and uses clear() and
addlist() per game loop. If you want or need to be sloppy, then use the build_entity_map=True optimization. The
trade off is a few milliseconds per batch.
"""

from collections import namedtuple, defaultdict
from itertools import chain
from types import GeneratorType

from pygame import Rect


SpacezTuple = namedtuple('SpaceZ', 'name size metrics entity_map buckets cache build_entity_map runtime' +
                         ' new add addlist remove entities count clear' +
                         ' entities_in_rect buckets_in_rect' +
                         ' collidedict collidealldict collidepairs collideallpairs')


def new(name='anonymous', size=50, build_entity_map=False):
    """create a new spacez hash table

    :param name: any type; optional user data, not used by this module
    :param size: int; bucket size must be a positive integer
    :param build_entity_map: bool; if False, performance will favor adds; else removes are favored
    :return: SpacezTuple
    """
    space = SpacezTuple(
        name=name, size=size, metrics=[0, 0],
        entity_map=defaultdict(set) if build_entity_map else set(),
        buckets=defaultdict(set), cache={}, build_entity_map=build_entity_map, runtime={'collision_tests': 0},
        new=new, add=add, addlist=addlist, remove=remove, entities=entities, count=count, clear=clear,
        entities_in_rect=entities_in_rect, buckets_in_rect=buckets_in_rect, collidedict=collidedict,
        collidealldict=collidealldict, collidepairs=collidepairs, collideallpairs=collideallpairs)
    return space


def add(space, *entities):
    """add entities

    If entity exists it will be removed first.

    :param space: SpacezTuple; as created by new()
    :param entities: varargs; entities to add
    :return: None
    """
    space.addlist(space, entities)


def _get_cache(space, rect):
    # the function call overhead is too high
    size = space.size
    cache = space.cache
    x = rect[0]
    y = rect[1]
    left = x // size
    top = y // size
    right = (x + rect[2]) // size
    bottom = (y + rect[3]) // size
    key = left, top, right, bottom
    if key in cache:
        bucket_ids = cache[key]
    else:
        xr = range(left, right + 1)
        yr = range(top, bottom + 1)
        bucket_ids = tuple((x, y) for x in xr for y in yr)
        cache[key] = bucket_ids
    return bucket_ids


def addlist(space, entities):
    """add entities from a sequence

    :param space: SpacezTuple; as created by new()
    :param entities: sequence; entities to add
    :return: None
    """
    buckets = space.buckets
    entity_map = space.entity_map
    build_entity_map = space.build_entity_map
    size = space.size
    cache = space.cache
    set_add = set.add
    set_update = set.update

    # bug fix: set.update does not like GeneratorType
    if isinstance(entities, GeneratorType):
        entities = tuple(entities)
    for e in entities:
        if e in entity_map:
            space.remove(space, e)
        rect = e.rect
        x = rect[0]
        y = rect[1]
        left = x // size
        top = y // size
        right = (x + rect[2]) // size
        bottom = (y + rect[3]) // size
        key = left, top, right, bottom
        if key in cache:
            bucket_ids = cache[key]
        else:
            xr = range(left, right + 1)
            yr = range(top, bottom + 1)
            bucket_ids = tuple((x, y) for x in xr for y in yr)
            cache[key] = bucket_ids
        for key in bucket_ids:
            set_add(buckets[key], e)
        if build_entity_map:
            set_update(entity_map[e], bucket_ids)
    if not build_entity_map:
        set_update(entity_map, entities)


def remove(space, entity):
    """remove an entity

    :param space: SpacezTuple; as created by new()
    :param entity: sprite; entity to remove
    :return: None
    """
    buckets = space.buckets
    entity_map = space.entity_map
    if space.build_entity_map:
        if entity in entity_map:
            for key in entity_map[entity]:
                buckets[key].discard(entity)
            del entity_map[entity]
    else:
        for bucket in buckets.values():
            bucket.discard(entity)
        entity_map.discard(entity)


def entities(space):
    """return a tuple of all entities

    :param space: SpacezTuple; as created by new()
    :return: tuple
    """
    return tuple(space.entity_map)


def count(space):
    """return the number of entities

    :param space: SpacezTuple; as created by new()
    :return: int
    """
    return len(space.entity_map)


def clear(space):
    """clear all entities

    :param space: SpacezTuple; as created by new()
    :return: None
    """
    space.buckets.clear()
    space.entity_map.clear()


def entities_in_rect(space, rect):
    """return a set of entities that collide with rect

    :param space: SpacezTuple; as created by new()
    :param rect: pygame.Rect
    :return: set of entities
    """
    result = set()
    buckets = space.buckets
    size = space.size
    cache = space.cache
    result_add = result.add
    colliderect = rect.colliderect
    x = rect[0]
    y = rect[1]
    left = x // size
    top = y // size
    right = (x + rect[2]) // size
    bottom = (y + rect[3]) // size
    key = left, top, right, bottom
    if key in cache:
        bucket_ids = cache[key]
    else:
        xr = range(left, right + 1)
        yr = range(top, bottom + 1)
        bucket_ids = tuple((x, y) for x in xr for y in yr)
        cache[key] = bucket_ids
    for key in bucket_ids:
        if key in buckets:
            for e in buckets[key]:
                if colliderect(e.rect):
                    result_add(e)
    return result


def buckets_in_rect(space, rect):
    """return a list of buckets that intersect with rect

    WARNING: For speed's sake the buckets are not copied. Modifying the returned buckets will modify the space
    contents.

    :param space: SpacezTuple; as created by new()
    :param rect: pygame.Rect
    :return: list of buckets
    """
    result = []
    result_append = result.append
    x = rect[0]
    y = rect[1]
    buckets = space.buckets
    size = space.size
    cache = space.cache
    left = x // size
    top = y // size
    right = (x + rect[2]) // size
    bottom = (y + rect[3]) // size
    key = left, top, right, bottom
    if key in cache:
        bucket_ids = cache[key]
    else:
        xr = range(left, right + 1)
        yr = range(top, bottom + 1)
        bucket_ids = tuple((x, y) for x in xr for y in yr)
        cache[key] = bucket_ids
    for key in bucket_ids:
        if key in buckets:
            result_append(buckets[key])
    return result


def collide(space, *entities):
    """return a tuple of space entities that collide with arg entities

    :param space: SpacezTuple; as created by new()
    :param entities: varargs; one or more entities to be collision-checked against the space contents
    :return: tuple(space_entity, ...)
    """
    return tuple(c[1] for c in space.collidepairs(space, entities))


def collidedict(space, entities):
    """return a dict of collisions between arg entities and space entities

    The returned dict is {arg_entity: {space_entity, ...}}.

    :param space: SpacezTuple; as created by new()
    :param entities: sequence; entities to be collision-checked against the space contents
    :return: {arg_entity: {space_entity, ...}}
    """
    result = defaultdict(set)
    buckets = space.buckets
    size = space.size
    cache = space.cache
    metrics = space.metrics
    colls = []
    append = list.append
    num_tested = 0
    num_returned = 0
    for e in entities:
        rect = e.rect
        x = rect[0]
        y = rect[1]
        left = x // size
        top = y // size
        right = (x + rect[2]) // size
        bottom = (y + rect[3]) // size
        key = left, top, right, bottom
        if key in cache:
            bucket_ids = cache[key]
        else:
            xr = range(left, right + 1)
            yr = range(top, bottom + 1)
            bucket_ids = tuple((x, y) for x in xr for y in yr)
            cache[key] = bucket_ids
        e_colliderect = rect.colliderect
        for key in bucket_ids:
            if key in buckets:
                for o in buckets[key]:
                    num_tested += 1
                    if e_colliderect(o.rect):
                        num_returned += 1
                        append(colls, o)
        if colls:
            result[e].update(colls)
            result[e].discard(e)
            colls = []
    metrics[0] = num_tested
    metrics[1] = num_returned
    return result


def collidealldict(space, rect=None):
    """return a set of collisions pairs between all of the space entities

    If rect is specified, then collision checks are confined to the buckets that intersect rect. Otherwise, all
    buckets are checked.

    :param space: SpacezTuple; as created by new()
    :param rect: pygame.Rect; optional area to restrict the collisions checks
    :return: {entity: {other, ...}}
    """
    result = defaultdict(set)
    metrics = space.metrics
    set_add = set.add
    set_discard = set.discard
    colliderect = Rect.colliderect
    if rect:
        buckets = space.buckets_in_rect(space, rect)
    else:
        buckets = space.buckets.values()
    num_tested = 0
    num_returned = 0
    for bucket in buckets:
        bucket = tuple(bucket)
        for i, e in enumerate(bucket):
            e_rect = e.rect
            for o in bucket[i + 1:]:
                num_tested += 1
                if e is not o and colliderect(e_rect, o.rect):
                    num_returned += 1
                    if o in result:
                        set_add(result[o], e)
                    else:
                        set_add(result[e], o)
            if e in result:
                set_discard(result[e], e)
                if not result[e]:
                    del result[e]
    metrics[0] = num_tested
    metrics[1] = num_returned
    return result


def collidepairs(space, entities):
    """return a list of collision pairs between arg entities and space entities

    The returned list of tuples is designed so that each collision pair has pair[0] as the entity in entity
    argument, and pair[1] as the entity in the space.

    If entities argument contain duplicates then the returned list will contain duplicates.

    :param space: SpacezTuple; as created by new()
    :param entities: sequence; entities to be collision-checked against the space contents
    :return: [(arg_entity: space_entity), ...]
    """
    result = []
    buckets = space.buckets
    size = space.size
    cache = space.cache
    metrics = space.metrics
    result_append = result.append
    num_tested = 0
    num_returned = 0
    for e in entities:
        rect = e.rect
        x = rect[0]
        y = rect[1]
        left = x // size
        top = y // size
        right = (x + rect[2]) // size
        bottom = (y + rect[3]) // size
        key = left, top, right, bottom
        if key in cache:
            bucket_ids = cache[key]
        else:
            xr = range(left, right + 1)
            yr = range(top, bottom + 1)
            bucket_ids = tuple((x, y) for x in xr for y in yr)
            cache[key] = bucket_ids
        e_colliderect = rect.colliderect
        for key in bucket_ids:
            if key not in buckets:
                continue
            for o in buckets[key]:
                num_tested += 1
                if e_colliderect(o.rect):
                    num_returned += 1
                    result_append((e, o))
    metrics[0] = num_tested
    metrics[1] = num_returned
    return result


def collideallpairs(space, rect=None, fat=False):
    """return a set of collisions pairs between all of the space entities

    If rect is specified, then collision checks are confined to the buckets that intersect rect. Otherwise, all
    buckets are checked.

    If fat is True, then forward and reverse collision pairs are returned. If False, then only one pair is
    returned per collision, in the order in which they occur is used. Although there is a cost associated with
    fat=True, the convenience may be worth it. The default is False.

    :param space: SpacezTuple; as created by new()
    :param rect: pygame.Rect; optional area to restrict the collisions checks
    :param fat: bool;
    :return: {(entity, other), ...}
    """
    result = set()
    collision_tests = 0
    metrics = space.metrics
    result_add = result.add
    colliderect = Rect.colliderect
    if rect:
        buckets = space.buckets_in_rect(space, rect)
    else:
        buckets = space.buckets.values()
    num_tested = 0
    num_returned = 0
    for bucket in buckets:
        bucket = tuple(bucket)
        for i, e in enumerate(bucket):
            e_rect = e.rect
            for o in bucket[i + 1:]:
                if e is o:
                    continue
                num_tested += 1
                if colliderect(e_rect, o.rect):
                    collision_tests += 1
                    num_returned += 2 if fat else 1
                    result_add((e, o))
                    if fat:
                        result_add((o, e))
    metrics[0] = num_tested
    metrics[1] = num_returned
    space.runtime['collision_tests'] = collision_tests
    return result


class Spacez(object):
    """an object-style wrapper for the SpacezTuple
    """

    def __init__(self, name='anonymous', size=50, build_entity_map=False):
        """create a new spacez hash table

        Tip: You can have fast adds or fast removals. There is a trade off, in that fast removal needs to build
        the lookup table during add() and addlist(). The default favors adds. The decision should depend on
        whether the sprites being added contain many duplicates. If speed is a concern, the
        build_entity_map=False optimization is best served by a design that avoids using remove(): i.e. keeps
        unique sprite sets, and uses clear() and addlist() per game loop. If you want or need to be sloppy,
        then use the build_entity_map=True optimization. The trade off is a few milliseconds per batch.

        :param name: any type; optional user data, not used by this module
        :param size: int; bucket size must be a positive integer
        :param build_entity_map: bool; if False, performance will favor adds; else removes are favored
        :return: SpacezTuple
        """
        self.spacez = new(name, size, build_entity_map)

    def add(self, *entities):
        addlist(self.spacez, entities)

    def addlist(self, entities):
        addlist(self.spacez, entities)

    def remove(self, entity):
        remove(self.spacez, entity)

    def clear(self):
        clear(self.spacez)

    @property
    def entities(self):
        return entities(self.spacez)

    @property
    def buckets(self):
        return self.spacez.buckets

    @property
    def count(self):
        return count(self.spacez)

    @property
    def collision_tests(self):
        return self.spacez.runtime['collision_tests']

    def entities_in_rect(self, rect):
        return entities_in_rect(self.spacez, rect)

    def buckets_in_rect(self, rect):
        return buckets_in_rect(self.spacez, rect)

    def collide(self, *entities):
        return collide(self.spacez, *entities)

    def collidedict(self, entities):
        return collidedict(self.spacez, entities)

    def collidealldict(self, rect=None):
        return collidealldict(self.spacez, rect)

    def collidepairs(self, entities):
        return collidepairs(self.spacez, entities)

    def collideallpairs(self, rect=None, fat=False):
        return collideallpairs(self.spacez, rect, fat)

    @property
    def num_tested(self):
        return self.spacez.metrics[0]

    @property
    def num_returned(self):
        return self.spacez.metrics[1]

    @property
    def name(self):
        return self.spacez.name

    @property
    def size(self):
        return self.spacez.size

    @property
    def entity_map(self):
        return self.spacez.entity_map

    @property
    def buckets(self):
        return self.spacez.buckets

    @property
    def cache(self):
        return self.spacez.cache

    @property
    def build_entity_map(self):
        return self.spacez.build_entity_map

    def __len__(self):
        return count(self.spacez)


if __name__ == '__main__':
    def main():
        from random import randrange
        import pygame
        from pygame import Color, QUIT, KEYDOWN, MOUSEMOTION, K_ESCAPE, K_TAB, K_SPACE

        class Sprite(object):
            def __init__(self, rect):
                self.rect = Rect(rect)

        pygame.init()
        screen = pygame.display.set_mode((640, 480))
        screen_rect = screen.get_rect()
        clock = pygame.time.Clock()
        max_fps = 30

        mouse_rect = Rect(0, 0, 75, 75)
        arranged_sprites = set([Sprite((x, y, 20, 20))
                               for x in range(0, screen_rect.w, 20 * 3)
                               for y in range(0, screen_rect.h, 20 * 3)])
        random_sprites = set(Sprite((randrange(0, screen_rect.w), randrange(0, screen_rect.h), 20, 20))
                             for i in range(25))
        space = Spacez()
        space.addlist(arranged_sprites)

        display_names = ('Tab> (0) entities_in_rect: mouse vs hash',
                         'Tab> (1) collidepairs: random vs hash',
                         'Tab> (2) collidedict: random vs hash')
        display_colls = 2
        pygame.display.set_caption(display_names[display_colls])

        running = True

        while running:
            clock.tick(max_fps)
            for e in pygame.event.get():
                if e.type == QUIT:
                    running = False
                elif e.type == KEYDOWN:
                    if e.key == K_ESCAPE:
                        running = False
                    elif e.key == K_TAB:
                        display_colls += 1
                        display_colls %= 3
                        pygame.display.set_caption(display_names[display_colls])
                    elif e.key == K_SPACE:
                        random_sprites = set(
                            Sprite((randrange(0, screen_rect.w), randrange(0, screen_rect.h), 20, 20))
                            for i in range(25))
                elif e.type == MOUSEMOTION:
                    mouse_rect.center = e.pos
            screen.fill((0, 0, 0))
            w = space.size
            for x, y in space.buckets:
                pygame.draw.rect(screen, Color('grey25'), (x * w, y * w, w, w), 1)
            if display_colls == 0:
                # entities_in_rect: mouse vs hash
                colls = space.entities_in_rect(mouse_rect)
                for e in arranged_sprites - colls:
                    pygame.draw.rect(screen, Color('green'), e.rect, 1)
                for e in colls:
                    pygame.draw.rect(screen, Color('red'), e.rect, 1)
            elif display_colls == 1:
                # collidepairs: random vs hash
                colls = space.collidepairs(random_sprites)
                for e in arranged_sprites - set([c[1] for c in colls]):
                    pygame.draw.rect(screen, Color('green'), e.rect, 1)
                for e in random_sprites - set([c[0] for c in colls]):
                    pygame.draw.rect(screen, Color('yellow'), e.rect, 2)
                for rsprite, asprite in colls:
                    pygame.draw.rect(screen, Color('orange'), rsprite.rect, 2)
                    pygame.draw.rect(screen, Color('orange'), asprite.rect, 1)
            elif display_colls == 2:
                # collidedict: random vs hash
                colls = space.collidedict(random_sprites)
                for e in arranged_sprites - set(colls):
                    pygame.draw.rect(screen, Color('green'), e.rect, 1)
                for e in random_sprites - set(chain.from_iterable(colls.values())):
                    pygame.draw.rect(screen, Color('yellow'), e.rect, 2)
                for rsprite, others in colls.items():
                    pygame.draw.rect(screen, Color('orange'), rsprite.rect, 2)
                    for asprite in others:
                        pygame.draw.rect(screen, Color('orange'), asprite.rect, 1)
            pygame.draw.rect(screen, Color('white'), mouse_rect, 1)
            pygame.display.flip()

    main()
