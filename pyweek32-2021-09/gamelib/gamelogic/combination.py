# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'combination.py' is part of pw-32
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The Combiner class for easy combining items.

"""
from __future__ import print_function, division

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2021"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class KindsInWrongOrderException(Exception):
    pass


class AlreadyRegisteredException(Exception):
    pass


class Combiner(object):

    def __init__(self):
        self._registry = {}  # {(kind1, kind2): mix_method}

    def register_pair(self, kind1, kind2, mix_method):
        """
        Register a combination method to combine two item types into another.

        *Important*: the order of the kinds is important: kind1 <= kind2

        :param kind1: The first item type.
        :param kind2: The other item type.
        :param mix_method: the method that combines the items. It should return a new item or None (no success).
               Expected signature: combine_a_and_b(a, b) -> c|None
        :return: the combined item | None if no combination is possible
        """
        if kind1 > kind2:
            raise KindsInWrongOrderException("kind1 <= kind2 and check the mix_method to expect them in same order!")
        key = (kind1, kind2)
        if key in self._registry:
            raise AlreadyRegisteredException(f"({kind1}, {kind2})")
        self._registry[key] = mix_method

        # support the interchanged case too by converting it to the ordered case
        if kind1 == kind2:
            return

        def _inverted(k2, k1):
            return self._registry[(k1.kind, k2.kind)](k1, k2)

        self._registry[(kind2, kind1)] = _inverted

    def combine(self, item1, item2):
        """
        Try to combine item1 and item2 to something else defined by the callback if present.
        Keep in mind that item1 and item2 need to have a 'kind' attribute as a unique kind identifier.

        :param item1: The first item to combine.
        :param item2: The second item to combine.
        :return: None if no combination is possible. The combined object if the combination can be done successfully.
        """
        key = (item1.kind, item2.kind)
        if not key in self._registry:
            return

        return self._registry[key](item1, item2)


logger.debug("imported")

if __name__ == '__main__':

    KIND_A = 0
    KIND_B = 2
    KIND_C = 3


    class A(object):
        kind = KIND_A


    class B(object):
        kind = KIND_B


    class C(object):
        kind = KIND_C


    item_a = A()
    item_b = B()
    item_c = C()


    def mix_a_and_b_to_c(a, b):
        return item_c


    combiner = Combiner()
    combiner.register_pair(KIND_A, KIND_B, mix_a_and_b_to_c)
    combiner.register_pair(KIND_A, KIND_A, mix_a_and_b_to_c)

    mix1 = combiner.combine(item_a, item_b)
    mix2 = combiner.combine(item_b, item_a)
    assert mix1.kind == mix2.kind == KIND_C

    # this should not work
    mix3 = combiner.combine(item_c, item_b)
    mix4 = combiner.combine(item_b, item_c)
    assert mix3 is None == mix4 is None

    mix5 = combiner.combine(item_a, item_a)
    assert mix5.kind == KIND_C
