# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'settings.py' is part of pw---__
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Settings and constants.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import datetime
import logging

import pygame

from pyknic.generators import GlobalIdGenerator
from pyknic.pyknic_pygame import pygametext
from pyknic.pyknic_pygame.eventmapper import EventMapper, ANY_MOD

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")

# --------------------------------------------------------------------------------
# IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT
# --------------------------------------------------------------------------------
#
# This file contains the distributed default settings.
#
# Don't override settings by editing this file, else they will end up in the repo.
# Instead, see the DEBUGS section at the end of this file for instructions on
# keeping your custom settings out of the repo.
#
# --------------------------------------------------------------------------------
# IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT
# --------------------------------------------------------------------------------


#
#
# # ------------------------------------------------------------------------------
# # Tunables
# # ------------------------------------------------------------------------------
#
# print_fps = False
max_fps = 60
master_volume = 0.5  # min: 0.0; max: 1.0
music_volume = 1.0  # resulting level = master_volume * music_volume
sfx_volume = 1.0  # resulting level = master_volume * sfx_volume
log_level = [logging.WARNING, logging.DEBUG, logging.INFO][1]

#
#
# # ------------------------------------------------------------------------------
# # Display
# # ------------------------------------------------------------------------------
#
# os.environ['SDL_VIDEO_CENTERED'] = '1'
#
screen_width = 1024
screen_height = 768
screen_size = screen_width, screen_height
screen_flags_presets = (
    0,
    pygame.FULLSCREEN | pygame.DOUBLEBUF,
)
screen_flags = screen_flags_presets[0]
default_fill_color = 100, 100, 100

title = "Aaaaaa! Make It Stop!"
path_to_icon = "./data/icon.png"

# ------------------------------------------------------------------------------
# Game
# ------------------------------------------------------------------------------


# ------------------------------------------------------------------------------
# Sound
# ------------------------------------------------------------------------------

# Mixer
MIXER_FREQUENCY = 0  # default:22050
MIXER_SIZE = -16  # default: -16
MIXER_CHANNELS = 2  # default: 2 stereo or 1 mono
MIXER_BUFFER_SIZE = 16  # default: 4096

MIXER_NUM_CHANNELS = 24
MIXER_RESERVED_CHANNELS = 1
(
    channel_gong,
) = range(MIXER_RESERVED_CHANNELS)


music_ended_pygame_event = pygame.USEREVENT

global_id_names = {}
GLOBAL_ID_ACTIONS = 1000
_global_id_generator = GlobalIdGenerator(GLOBAL_ID_ACTIONS)  # avoid collision with event ids

# ------------------------------------------------------------------------------
# Events
# ------------------------------------------------------------------------------
EVT_NOISE = _global_id_generator.next("EVT_NOISE")  # (noisemaker)
EVT_MOUSE_GRABBED = _global_id_generator.next()
EVT_MOUSE_DROPPED = _global_id_generator.next()
EVT_MOUSE_DROP_CANCELED = _global_id_generator.next()
EVT_ITEMS_COMBINED = _global_id_generator.next()
EVT_FOCUS_LOST = _global_id_generator.next()
EVT_FOCUS_GAINED = _global_id_generator.next()
EVT_FOCUS_CHANGED = _global_id_generator.next()
EVT_ADDED_TO_INVENTORY = _global_id_generator.next()
EVT_REMOVED_FROM_INVENTORY = _global_id_generator.next()
EVT_PLAY_SOUND_DATA = _global_id_generator.next()
EVT_GUMM_SOUND = _global_id_generator.next()

EVT_GONG_SOUND = _global_id_generator.next()

# actions

ACTION_TOGGLE_DEBUG_RENDER = _global_id_generator.next("ACTION_TOGGLE_DEBUG_RENDER")
ACTION_QUIT = _global_id_generator.next("ACTION_QUIT")
ACTION_LMB = _global_id_generator.next("ACTION_LMB")
ACTION_RMB = _global_id_generator.next("ACTION_RMB")
ACTION_MOUSE_MOTION = _global_id_generator.next("ACTION_MOUSE_MOTION")
# actions for travel
ACTION_GO_EAST = _global_id_generator.next("ACTION_GO_EAST")
ACTION_GO_NORTH = _global_id_generator.next("ACTION_GO_NORTH")
ACTION_GO_WEST = _global_id_generator.next("ACTION_GO_WEST")
# actions with items
ACTION_OPEN_FRIDGE = _global_id_generator.next("ACTION_OPEN_FRIDGE")
ACTION_OPEN_STEVENS_BASEMENT = _global_id_generator.next("ACTION_OPEN_STEVENS_BASEMENT")
ACTION_OPEN_GORE_BASEMENT = _global_id_generator.next("ACTION_OPEN_GORE_BASEMENT")
ACTION_MAKE_SUPERTOOL = _global_id_generator.next("ACTION_MAKE_SUPERTOOL")
ACTION_BASH_GORE = _global_id_generator.next("ACTION_BASH_GORE")
ACTION_HIJACK_DUMP_TRUCK = _global_id_generator.next("ACTION_HIJACK_DUMP_TRUCK")
ACTION_GET_WRENCH = _global_id_generator.next("ACTION_GET_WRENCH")
ACTION_GET_MUSHROOM = _global_id_generator.next("ACTION_GET_MUSHROOM")
ACTION_GET_CROWBAR = _global_id_generator.next("ACTION_GET_CROWBAR")
ACTION_CLEAR_MESSAGE_CLOG = _global_id_generator.next("ACTION_CLEAR_MESSAGE_CLOG")
# actions with NPCs
ACTION_POKE_GORE = _global_id_generator.next("ACTION_POKE_GORE")
ACTION_POKE_STEVENS = _global_id_generator.next("ACTION_POKE_STEVENS")

gameplay_event_mapper = EventMapper({
    pygame.MOUSEBUTTONDOWN: {
        1: ACTION_LMB,  # LMB
        #     # 2: ACTION_HERO_TRIGGER_DISCHARGE,  # MMB
        3: ACTION_RMB,  # RMB
        #     # 4: ACTION_HERO_SWITCH,  # mouse scroll up(?)
        #     # 5: ACTION_HERO_SWITCH,  # mouse scroll down(?)
    },
    # pygame.MOUSEBUTTONUP: {
    #     # 1: ACTION_HERO_HOLD_FIRE,
    #     # 2: ACTION_HERO_HOLD_SECONDARY,  # MMB
    #     # 3: ACTION_HERO_HOLD_SECONDARY,  # RMB
    # },
    pygame.MOUSEMOTION: {
        None: ACTION_MOUSE_MOTION,
    },
    pygame.KEYDOWN: {

        (pygame.K_F1, ANY_MOD): ACTION_TOGGLE_DEBUG_RENDER,
        (pygame.K_F4, pygame.KMOD_ALT): ACTION_QUIT,
        (pygame.K_ESCAPE, ANY_MOD): ACTION_QUIT,
        (pygame.K_e, ANY_MOD): ACTION_GO_EAST,
        (pygame.K_n, ANY_MOD): ACTION_GO_NORTH,
        (pygame.K_w, ANY_MOD): ACTION_GO_WEST,
        # hero
        # (pygame.K_a, ANY_MOD): ACTION_HERO_MOVE_LEFT,
        # (pygame.K_d, ANY_MOD): ACTION_HERO_MOVE_RIGHT,
        # (pygame.K_w, ANY_MOD): ACTION_HERO_MOVE_UP,
        # (pygame.K_s, ANY_MOD): ACTION_HERO_MOVE_DOWN,
        # (pygame.K_e, ANY_MOD): ACTION_HERO_USE,
        # (pygame.K_LSHIFT, ANY_MOD): ACTION_HERO_CROUCH,
        # (pygame.K_RSHIFT, ANY_MOD): ACTION_HERO_CROUCH,
        # (pygame.K_TAB, ANY_MOD): ACTION_HERO_SWITCH,
        # # (pygame.K_q, ANY_MOD): ACTION_HERO_SWITCH_SECONDARY,
        # (pygame.K_SPACE, ANY_MOD): ACTION_HERO_TRIGGER_DISCHARGE
    },
    # pygame.KEYUP: {
    #     # (pygame.K_ESCAPE, ANY_MOD): ACTION_WIN_LEVEL,
    #     # (pygame.K_a, ANY_MOD): ACTION_HERO_STOP_LEFT,
    #     # (pygame.K_d, ANY_MOD): ACTION_HERO_STOP_RIGHT,
    #     # (pygame.K_w, ANY_MOD): ACTION_HERO_STOP_UP,
    #     # (pygame.K_s, ANY_MOD): ACTION_HERO_STOP_DOWN,
    #     # (pygame.K_q, ANY_MOD): ACTION_HERO_HOLD_SECONDARY,
    #     # (pygame.K_LSHIFT, ANY_MOD): ACTION_HERO_STAND,
    #     # (pygame.K_RSHIFT, ANY_MOD): ACTION_HERO_STAND,
    # },
    pygame.QUIT: {None: ACTION_QUIT},

})

# ------------------------------------------------------------------------------
# Kinds
# ------------------------------------------------------------------------------
KIND_HERO = _global_id_generator.next("KIND_HERO")
KIND_GRAB_ITEM = _global_id_generator.next("KIND_GRAB_ITEM")
KIND_MIX_ITEM = _global_id_generator.next("KIND_MIX_ITEM")
KIND_ACTIVATE_ITEM = _global_id_generator.next("KIND_ACTIVATE_ITEM")
KIND_INERT_ITEM = _global_id_generator.next("KIND_INERT_ITEM")

# specific kinds
KIND_INVENTORY_ITEM = _global_id_generator.next("KIND_INVENTORY_ITEM")
KIND_SUPERTOOL_ITEM = _global_id_generator.next("KIND_SUPERTOOL_ITEM")
KIND_WRENCH_ITEM = _global_id_generator.next("KIND_WRENCH_ITEM")
KIND_PIZZA_ITEM = _global_id_generator.next("KIND_PIZZA_ITEM")
KIND_MUSHROOM_ITEM = _global_id_generator.next("KIND_MUSHROOM_ITEM")
KIND_CLOSED_FRIDGE_ITEM = _global_id_generator.next("KIND_FRIDGE_ITEM")
KIND_OPEN_FRIDGE_ITEM = _global_id_generator.next("KIND_OPEN_FRIDGE_ITEM")
KIND_CROWBAR_ITEM = _global_id_generator.next("KIND_CROWBAR_ITEM")
KIND_DUMP_TRUCK_ITEM = _global_id_generator.next("KIND_DUMP_TRUCK_ITEM")
KIND_MESSAGE_CLOG_ITEM = _global_id_generator.next("KIND_MESSAGE_CLOG_ITEM")
KIND_ROOM_DOOR = _global_id_generator.next("KIND_ROOM_DOOR")
KIND_YARD_DOOR_TO_BASEMENT = _global_id_generator.next("KIND_YARD_DOOR_TO_BASEMENT")
KIND_YARD_DOOR_TO_BASEMENT_OPEN = _global_id_generator.next("KIND_YARD_DOOR_TO_BASEMENT_OPEN")
KIND_BACKGROUND = _global_id_generator.next("KIND_ROOM_LAB_DOOR")
KIND_NPC_GORE = _global_id_generator.next("KIND_NPC_GORE")
KIND_GORE_HEAD = _global_id_generator.next("KIND_GORE_HEAD")
KIND_NPC_STEVENS = _global_id_generator.next("KIND_NPC_STEVENS")
KIND_TEXT_BUBBLE = _global_id_generator.next("KIND_TEXT_BUBBLE")
KIND_TEXT_BUBBLE_BUTTON = _global_id_generator.next("KIND_TEXT_BUBBLE_BUTTON")
KIND_MENU_ITEM = _global_id_generator.next("KIND_MENU_ITEM")

KIND_DIALOGUE_1 = _global_id_generator.next("KIND_DIALOGUE_1")
KIND_DIALOGUE_2 = _global_id_generator.next("KIND_DIALOGUE_2")

KIND_DIALOGUE_LAB = _global_id_generator.next("KIND_DIALOGUE_LAB")
KIND_DIALOGUE_LAB_WHO = _global_id_generator.next("KIND_DIALOGUE_LAB_WHO")
KIND_DIALOGUE_LAB_WHAT = _global_id_generator.next("KIND_DIALOGUE_LAB_WHAT")
KIND_DIALOGUE_LAB_WHEN = _global_id_generator.next("KIND_DIALOGUE_LAB_WHEN")
KIND_DIALOGUE_LAB_WHERE = _global_id_generator.next("KIND_DIALOGUE_LAB_WHERE")
KIND_DIALOGUE_LAB_WHY = _global_id_generator.next("KIND_DIALOGUE_LAB_WHY")
KIND_DIALOGUE_LAB_HOW = _global_id_generator.next("KIND_DIALOGUE_LAB_HOW")

KIND_DIALOGUE_STEVENS_BASEMENT = _global_id_generator.next("KIND_DIALOGUE_STEVENS_BASEMENT")
KIND_DIALOGUE_STEVENS_BASEMENT_WHO = _global_id_generator.next("KIND_DIALOGUE_STEVENS_BASEMENT_WHO")
KIND_DIALOGUE_STEVENS_BASEMENT_WHAT = _global_id_generator.next("KIND_DIALOGUE_STEVENS_BASEMENT_WHAT")
KIND_DIALOGUE_STEVENS_BASEMENT_WHEN = _global_id_generator.next("KIND_DIALOGUE_STEVENS_BASEMENT_WHEN")
KIND_DIALOGUE_STEVENS_BASEMENT_WHERE = _global_id_generator.next("KIND_DIALOGUE_STEVENS_BASEMENT_WHERE")
KIND_DIALOGUE_STEVENS_BASEMENT_WHY = _global_id_generator.next("KIND_DIALOGUE_STEVENS_BASEMENT_WHY")
KIND_DIALOGUE_STEVENS_BASEMENT_HOW = _global_id_generator.next("KIND_DIALOGUE_STEVENS_BASEMENT_HOW")

# Rooms
KIND_ROOM_LAB = _global_id_generator.next("KIND_ROOM_LAB")
KIND_ROOM_STEVENS_YARD = _global_id_generator.next("KIND_ROOM_STEVENS_YARD")
KIND_ROOM_STEVENS_BASEMENT = _global_id_generator.next("KIND_ROOM_STEVENS_BASEMENT")
KIND_ROOM_GORE_BASEMENT = _global_id_generator.next("KIND_ROOM_GORE_BASEMENT")

# ------------------------------------------------------------------------------
# Story Milestones
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# Entities
# ------------------------------------------------------------------------------
Z_LAYER_BACKGROUND = 0
Z_LAYER_SURFACE = 10
Z_LAYER_OVER = 20
Z_LAYER_INVENTORY = 30
Z_LAYER_IN_INVENTORY = 40
Z_LAYER_GRABBED = 50
Z_LAYER_TOP = 800

FOCUS_ZOOM_CHANGE = 0.02
#
# CritterParams = namedtuple("CritterParams", "turn_rate speed stand_max_duration stand_min_duration stand_turn_chance "
#                                             "turn_min_duration turn_max_duration turn_stand_chance noise_interval")
# critter_params = CritterParams(
#     turn_rate=120,
#     speed=30,
#     stand_min_duration=0.5,
#     stand_max_duration=3,
#     stand_turn_chance=0.5,  # or else walk
#     turn_min_duration=0.2,
#     turn_max_duration=2,
#     turn_stand_chance=0.05,  # or else walk
#     noise_interval=0.1,
# )

# ------------------------------------------------------------------------------
# World
# ------------------------------------------------------------------------------
cell_size = 100
initial_date_time = datetime.datetime(1991, 3, 24, 9, 10, 45)
reset_after_n_hours = 6
tick_delta_in_hours = 1

# ------------------------------------------------------------------------------
# Map
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# Stepper
# ------------------------------------------------------------------------------
STEP_DT = 1.0 / 120.0  # [1.0 / fps] = [s]
MAX_DT = 20 * STEP_DT  # [s]
DRAW_FPS = 60  # [fps]

# ------------------------------------------------------------------------------
# UI
# ------------------------------------------------------------------------------

menu_item_distance = 30
speech_bubble_pointer_color = (200, 200, 200)
speech_bubble_pointer_width = 50
cam_speed = 2  # hero_params.jump_speed * 0.1
highlight_items = True
fade_duration = 0.2

# ------------------------------------------------------------------------------
# Graphics
# ------------------------------------------------------------------------------
gfx_path = "data/gfx"
# cached surfaces (required by spritefx)
# Note: pygametext does its own caching. No need to cache surfaces from pygametext.getsurf() unless you transform them.
# Note: these settings are not tunable on the fly. If you want to change the image cache settings during runtime, you'll
# have to call the image_cache instance's methods.
# Note: If you rely on aging or memory cap, remember to call image_cache.update(), or purge_memory() or purge_old().

# image_cache_expire = 1 * 60  # expire unused textures after N seconds; recommend 1 min (1 * 60)
# image_cache_memory = 5 * 2 ** 20  # force expiration if memory is exceeded; recommend 5 MB (5 * 2**20)
# image_cache = imagecache.ImageCache(image_cache_expire, image_cache_memory)
# image_cache.enable_age_cap(True)

pygametext.FONT_NAME_TEMPLATE = 'data/fonts/%s'
pygametext.MEMORY_LIMIT_MB = 32  # it takes about 5 min to hit the default 64 MB, so 32 is not too aggressive

# # cached surfaces (required by spritefx)
# # Note: pygametext does its own caching. No need to cache surfaces from pygametext.getsurf() unless you transform them.
# # Note: these settings are not tunable on the fly. If you want to change the image cache settings during runtime, you'll
# # have to call the image_cache instance's methods.
# # Note: If you rely on aging or memory cap, remember to call image_cache.update(), or purge_memory() or purge_old().
# image_cache_expire = 1 * 60  # expire unused textures after N seconds; recommend 1 min (1 * 60)
# image_cache_memory = 5 * 2 ** 20  # force expiration if memory is exceeded; recommend 5 MB (5 * 2**20)
# image_cache = imagecache.ImageCache(image_cache_expire, image_cache_memory)
# image_cache.enable_age_cap(True)

# font_themes is used with the functions in module pygametext
# Example call to render some text using a theme:
#   image = pygametext.getsurf('some text', **settings.font_themes['intro'])
# font_themes can be accessed by nested dict keys:
#   font_theme['intro']['fontname']
font_themes = dict(
    # Game title
    title=dict(
        fontname='Bubblegum_Sans.ttf',
        fontsize=50,
        color='red2',
        gcolor='orange1',
        ocolor='yellow',
        owidth=0.5,
        scolor=None,
        shadow=None,
    ),
    loose=dict(
        fontname='Bubblegum_Sans.ttf',
        fontsize=50,
        color='red2',
        gcolor='orange1',
        ocolor='yellow',
        owidth=0.5,
        scolor=None,
        shadow=None,
    ),
    win=dict(
        fontname='Bubblegum_Sans.ttf',
        fontsize=50,
        color='red2',
        gcolor='orange1',
        ocolor='yellow',
        owidth=0.5,
        scolor=None,
        shadow=None,
    ),
    intro1=dict(
        fontname='VeraBd.ttf',
        fontsize=35,
        color='white',
        gcolor='grey80',
        # ocolor=None,
        # owidth=0.5,
        scolor='grey20',
        shadow=(1, 1),
    ),
    intro2=dict(
        fontname='VeraBd.ttf',
        fontsize=25,
        color='white',
        gcolor='grey80',
        # ocolor=None,
        # owidth=0.5,
        scolor='grey20',
        shadow=(1, 1),
    ),
    speechbubble=dict(
        fontname='ComicNoteSmooth.ttf',
        fontsize=20,
        color='black',
        width=300,
        # gcolor='orange',
        # ocolor='black',
        # owidth=0.1,
        # scolor=None,
        # shadow=None,
        background=(255, 240, 142, 255)
    ),
    navigation=dict(
        fontname='ComicNoteSmooth.ttf',
        fontsize=20,
        color='black',
        width=300,
        # gcolor='orange',
        # ocolor='black',
        # owidth=0.1,
        # scolor=None,
        # shadow=None,
        background=(117, 255, 147, 200)
    ),
    gamehud=dict(
        fontname='digital-7.mono.ttf',  # fontname='digital-7.regular.ttf',
        fontsize=30,
        color='black',
        gcolor=None,  # 'orange2',
        ocolor=None,
        owidth=0.5,
        scolor='grey20',
        shadow=(1, 1),
    ),
    floatingtext=dict(
        fontname='Vera.ttf',
        fontsize=10,
        color='white',
        # gcolor='orange',
        ocolor='black',
        owidth=0.1,
        scolor=None,
        shadow=None,
        background=(0, 0, 0, 128)
    ),
    debug=dict(
        fontname='Vera.ttf',
        fontsize=16,
        color='yellow',
        # gcolor='orange',
        ocolor='black',
        owidth=0.5,
        scolor='black',
        shadow=(1, 1),
    ),
)

# ------------------------------------------------------------------------------
# DEBUGS
# ------------------------------------------------------------------------------

# IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT
#
# Don't override debug settings in this file. Instead, do the following:
# This allows customizations for devs, and insures play-testers pulling from the
# repo will use the intended default settings.
#
# HOW TO:
# 1. Create gamelib/_custom.py.
# 2. Override your developer settings in _custom.py.
# 3. DO NOT add _custom.py to the repo. :)
#
# SAMPLE _custom.py:
# debug_quit = True
#

# Enable/disable a local developer context.
# True: invoke the shim in gamelib.main.main(); you will need to create a module
# gamelib._custom

show_dev_menu = False
fake_full_dump_truck = False

# noinspection PyBroadException
try:
    # define a _custom.py file to override some settings (useful while developing)
    # noinspection PyUnresolvedReferences,PyProtectedMember
    from gamelib._custom import *
except ImportError:
    pass

logger.debug("imported")
