# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'context_intro.py' is part of pw-32
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This is the intro context, executed once.

"""
from __future__ import print_function, division

import logging
import random

import pygame.display

import pyknic.pyknic_pygame.context.effects
from gamelib import settings, resources
from pyknic import tweening
from pyknic.mathematics import Vec2, Point2, Point3
from pyknic.pyknic_pygame.context import TimeSteppedContext
from pyknic.pyknic_pygame.resource2 import resource_manager
from pyknic.pyknic_pygame.spritesystem import DefaultRenderer, Camera, Sprite
from pyknic.timing import Timer, Scheduler
from pyknic.tweening import Tweener

logger = logging.getLogger(__name__)
logger.debug("importing...")


class Line(object):

    def __init__(self, p1, p2, color, life, speed):
        self.color = color
        self.p1 = p1
        self.p2 = p2
        self.life = life
        self.speed = speed
        self.direction = (self.p2 - self.p1).normalized * self.speed
        self.update_points(p1, p2)

    def update_points(self, p1, p2):
        self.p1 = p1
        self.p2 = p2
        self.direction = (self.p2 - self.p1).normalized * self.speed

    def update(self, dt):
        self.life -= dt
        # move the line
        self.p1 += self.direction * dt
        self.p2 += self.direction * dt


class WinContext(TimeSteppedContext):

    def __init__(self, *args):
        TimeSteppedContext.__init__(self, settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS)
        self.screen = None
        self.lines = []
        self.center = None
        self.max_speed = 100
        self.min_speed = 1000
        self.max_duration_in_seconds = 2
        self.renderer = DefaultRenderer()
        screen_rect = pygame.Rect(0, 0, settings.screen_width, settings.screen_height)
        self.cam = Camera(screen_rect, padding=196 // 2)
        self.cam.set_position(Point3(screen_rect.centerx, screen_rect.centery))
        self.tweener = Tweener()
        self.scheduler = Scheduler()
        self.timer = Timer(self.scheduler)
        self.timer.event_elapsed += self.quit

    def enter(self):
        pygame.event.clear()
        self.screen = pygame.display.get_surface()  # assign the screen surface to draw on.
        self.center = Point2(settings.screen_width / 2, settings.screen_height / 2)
        for i in range(100):
            line = Line(self.center.clone(), self.get_random_point(), self.get_random_color(),
                        self.get_random_duration(), self.get_random_speed())
            self.lines.append(line)
            line.update(random.random() * 10)

        win_surf = resource_manager.resources[resources.resource_win_text].image
        spr = Sprite(win_surf, self.center.clone())
        self.renderer.add_sprite(spr)
        self.tweener.create_tween_by_end(spr, "zoom", 0.1, 1.5, 2, tweening.ease_in_out_quad, cb_end=self.done)

    def quit(self, *args):
        effect = pyknic.pyknic_pygame.context.effects.DisappearingRectsTransition(
            (settings.screen_width, settings.screen_height))
        self.pop(effect=effect)

    def done(self, *args):
        self.timer.start(3)

    def exit(self):
        pass

    def suspend(self):
        pass

    def resume(self):
        pass

    def update_step(self, delta, sim_time, *args):
        for event in pygame.event.get():
            if event.type == pygame.QUIT or event.type == pygame.KEYDOWN or event.type == pygame.MOUSEBUTTONDOWN:
                self.quit()
        for line in self.lines:
            line.update(delta)

        for dead in (line for line in self.lines if line.life <= 0):
            # re-initialize them
            dead.update_points(self.center.clone(), self.get_random_point())
            dead.life = self.get_random_duration()
            dead.color = self.get_random_color()
            dead.speed = self.get_random_speed()
        self.tweener.update(delta)
        self.scheduler.update(delta, sim_time)

    def get_random_point(self):
        half_screen_width = settings.screen_width / 2
        half_screen_height = settings.screen_height / 2
        x = random.randint(-half_screen_width, half_screen_width)
        y = random.randint(-half_screen_height, half_screen_height)
        return self.center + Vec2(x, y)

    def draw_step(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0, screen=None):
        # draw to the screen... and flip it.
        screen = self.screen if screen is None else screen
        screen.fill((0, 0, 0))
        for line in self.lines:
            pygame.draw.aaline(screen, line.color, line.p1.as_xy_tuple(int), line.p2.as_xy_tuple(int))
        self.renderer.draw(screen, self.cam, do_flip=False)
        if do_flip:
            pygame.display.flip()

    def get_random_duration(self):
        return random.random() * self.max_duration_in_seconds

    def get_random_color(self):
        return (
            random.randint(0, 255),
            random.randint(0, 255),
            random.randint(0, 255),
            random.randint(0, 255),
        )

    def get_random_speed(self):
        return random.random() * self.max_speed + self.min_speed


logger.debug("imported")
