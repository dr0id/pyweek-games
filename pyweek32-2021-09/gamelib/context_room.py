# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'gameplay.py' is part of pw-30
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This is the game context where the actual game code is executed.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import datetime
import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2020"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = ["RoomContext"]  # list of public visible parts of this module

import pygame

import pyknic
from gamelib import resources
from gamelib import settings
from gamelib.game_state import GameState
from gamelib.gamelogic.sfx_handler import SfxHandler
from gamelib.gamerender import hudlight
from gamelib.gamelogic.eventdispatcher import event_dispatcher
from gamelib.gamelogic.combination import Combiner
from gamelib.gamelogic.collider import Collider
from gamelib.context_loose import LooseContext
from gamelib.context_win import WinContext
from pyknic.animation import Animation
from pyknic.pyknic_pygame.context import TimeSteppedContext
from pyknic.pyknic_pygame.resource2 import resource_manager
from pyknic.pyknic_pygame.spritesystem import DefaultRenderer, Camera, Sprite
from pyknic.mathematics import Vec3, Point3 as Point
from pyknic.timing import Scheduler
from pyknic.tweening import Tweener

logger = logging.getLogger(__name__)
logger.debug("importing...")


def add_event_listener(event_type, listener_callback):
    event_dispatcher.add_listener(event_type, listener_callback)
    room_listeners.append(listener_callback)


room_listeners = []


def remove_event_listeners():
    for listener in room_listeners:
        event_dispatcher.remove_listener(listener)
    event_dispatcher.clear()


class BaseSprite(Sprite):
    entity = None

    def on_focus(self):
        pass

    def on_focus_lost(self):
        pass


class EntitySprite(BaseSprite):

    def __init__(self, entity, image, position, anchor=None, z_layer=0, parallax_factors=None, do_init=True,
                 name='_NO_NAME_'):
        BaseSprite.__init__(self, image, position, anchor, z_layer, parallax_factors, do_init, name)
        self.entity = entity
        self.hover_sprite = None
        # border = 15
        # hover_image, center = self.get_hover_image(image, border)
        # vec = Vec(-center[0] + border, -center[1] + border) if anchor == 'topleft' else Vec(0, 0)
        # self.hover_sprite = BaseSprite(hover_image, position, vec, z_layer - 1, parallax_factors, do_init,
        #                                "hover_" + name)
        # self.hover_sprite.visible = False

    # def get_hover_image(self, image, border):
    #     if not settings.highlight_items:
    #         return image, image.get_rect().center
    #
    #     border_size = 2 * border
    #     r = image.get_rect().inflate(border_size, border_size)  # add 10 border
    #     r.move_ip(-r.x, -r.y)
    #     i = pygame.Surface(image.get_size(), pygame.SRCALPHA)
    #     mask = pygame.mask.from_surface(image, 100)
    #     i.fill((0, 0, 0, 0), r, pygame.BLEND_RGBA_MULT)
    #     step_size = 1
    #     outline = mask.outline(step_size)
    #     yellow = (255, 255, 0)
    #     pygame.draw.polygon(i, yellow, outline)
    #     factor = 0.5
    #     # i = pyknic.pyknic_pygame.transform.blur_surf(i, border * factor, border * factor, border_size)
    #     i = pyknic.pyknic_pygame.transform.box_blur(i, (21, 21), iterations=4, border=border_size)
    #     iw, ih = i.get_size()
    #     i = pygame.transform.smoothscale(i, (iw + 21, ih + 21))
    #     return i, r.center

    def on_focus(self):
        if self.hover_sprite:
            self.hover_sprite.visible = True

    def on_focus_lost(self):
        if self.hover_sprite:
            self.hover_sprite.visible = False


class BubblePointerSprite(EntitySprite):
    draw_special = True
    dirty_update = False  # performs the dirty sprite update if set, nothing otherwise

    def __init__(self, entity, position, attache_point_1, attache_point_2, color, anchor, z_layer):
        assert attache_point_2 is not None
        EntitySprite.__init__(self, entity, None, position, anchor, z_layer)
        self.color = color
        self.attache_point_1 = attache_point_1
        self.attache_point_2 = attache_point_2

    def draw(self, surf, cam: Camera, renderer, interpolation_factor=1.0):
        # draw a two lines from position -> attache_point1 and position -> attache_point2
        p = cam.world_to_screen(self.position)
        p1 = cam.world_to_screen(self.attache_point_1)
        p2 = cam.world_to_screen(self.attache_point_2)
        points = [p2.as_xy_tuple(), p.as_xy_tuple(), p1.as_xy_tuple()]
        return pygame.draw.polygon(surf, self.color, points)
        # return pygame.draw.aalines(surf, self.color, False, points)


class AnimatedSprite(BaseSprite, Animation):

    def __init__(self, frames, fps, scheduler, position, z_layer):
        Animation.__init__(self, 0, len(frames), fps, scheduler)
        BaseSprite.__init__(self, frames[0], position, z_layer=z_layer)
        self.event_index_changed += self._idx_changed
        self.frames = frames
        self.visible = False

    def _idx_changed(self, *args):
        self.set_image(self.frames[self.current_index])


class BaseItem(object):
    kind = None
    hidden = False

    def __init__(self, position):
        self.position = position

    def on_click(self):
        """
        Called when a mouse click occurs on this item.
        :return: true to be grabbed by the mouse, false|None otherwise
        """
        # react the way it should
        raise NotImplementedError()

    def on_drop(self, other):
        """
        Called if the mouse holds something and drops it onto this.
        :param other: the other item that is dropped here.
        :return: true to accept the dropped item, false|None to reject it.
        """
        raise NotImplementedError()

    def show(self):
        self.hidden = False

    def hide(self):
        self.hidden = True


class InertItem(BaseItem):
    kind = settings.KIND_INERT_ITEM

    def __init__(self, position, kind):
        position = position
        if not isinstance(position, Vec3):
            position = Point(*position)
        BaseItem.__init__(self, position)
        self.kind = kind
        self.hidden = False

    def on_click(self):
        event_dispatcher.fire(settings.EVT_GUMM_SOUND, resources.resource_sound_click)

    def on_drop(self, other):
        event_dispatcher.fire(settings.EVT_GUMM_SOUND, resources.resource_sound_click)


class SpeechBubble(InertItem):

    def __init__(self, position, button, sound_data, world, kind, dialogue_id, play_only_once):
        InertItem.__init__(self, position, kind)
        self.dialogue_id = dialogue_id
        self.sound_data = sound_data
        button.action = self.activate_next_bubble
        self.button = button
        self.next = None
        self.world = world
        self.menu_items = [button]
        self.visible = False
        add_event_listener(settings.EVT_FOCUS_CHANGED, self.on_focus_changed)
        self.play_only_once = play_only_once

    def activate_next_bubble(self, button):
        logger.info("next bubble")
        # remove this one (the active one)
        self.world.change_visibility(self, False)
        if self.next:
            # activate the next one
            self.world.change_visibility(self.next, True)
            pass

    def show(self):
        # todo start audio
        event_dispatcher.fire(settings.EVT_PLAY_SOUND_DATA, self.sound_data)
        # show on screen
        self.visible = True
        self.world.change_visibility(self.button, True)

    def hide(self):
        # todo cutoff audio if still playing?
        self.visible = False
        self.world.change_visibility(self.button, False)
        if self.play_only_once:
            self.world.remove_entity(self.button)
            self.world.remove_entity(self)
            self.world.game_state.played_dialogues.add(self.dialogue_id)

    def on_focus_changed(self, old, new):
        if not self.visible:
            return

        # todo try to capture the mouse within self and self.button... but somehow sometimes the mouse escapes.
        new_ent = None if new is None else new.entity
        old_ent = None if old is None else old.entity

        if ((old_ent == self and new_ent == self.button) or (old_ent == self.button and new_ent == self)) and \
                (not isinstance(old, BubblePointerSprite) and not isinstance(new, BubblePointerSprite)):
            # logger.error("focus(%s): %s -> %s  | %s > %s", self.world.mouse.focused_sprite, old_ent, new_ent, self, self.button)
            pass
        else:
            # move mouse back like a glitch
            pygame.mouse.set_pos(self.button.position.as_xy_tuple())
            extra = (self.position.as_xy_tuple(), (0, 0), [])
            self.world.mouse.handle_action(settings.ACTION_MOUSE_MOTION, extra)


class ItemToGrab(BaseItem):

    def __init__(self, item_data, kind):
        self.kind = kind
        self.name = settings._global_id_generator.names[item_data.kind][10:]
        self.hidden = item_data.hidden
        self.reveal_action = item_data.reveal_action
        position = item_data.position
        if not isinstance(position, Vec3):
            position = Point(*position)
        BaseItem.__init__(self, position)
        self.old_position = Point(0, 0)
        self.old_position.copy_values(self.position)

    def on_click(self):
        if self.hidden:
            return False
        event_dispatcher.fire(settings.EVT_GUMM_SOUND, resources.resource_sound_click)
        self.old_position.copy_values(self.position)
        return True

    def on_drop(self, other):
        event_dispatcher.fire(settings.EVT_GUMM_SOUND, resources.resource_sound_click)
        return False

    def set_new_position(self):
        self.old_position.copy_values(self.position)


class ItemToActivate(BaseItem):
    kind = settings.KIND_ACTIVATE_ITEM

    def __init__(self, position, kind, action, hidden=False, reveal_action=None):
        position = position
        if not isinstance(position, Vec3):
            position = Point(*position)
        BaseItem.__init__(self, position)
        self.kind = kind
        self.action = action
        self.hidden = hidden
        self.reveal_action = reveal_action

    def on_click(self):
        event_dispatcher.fire(settings.EVT_GUMM_SOUND, resources.resource_sound_click)
        self.action(self)

    def on_drop(self, other):
        event_dispatcher.fire(settings.EVT_GUMM_SOUND, resources.resource_sound_click)
        return False


class Door(ItemToActivate):

    def __init__(self, position, kind, hidden, menu_items, world):
        ItemToActivate.__init__(self, position, kind, hidden)
        self.menu_items = menu_items
        self.world = world
        add_event_listener(settings.EVT_FOCUS_CHANGED, self.on_focus_changed)

    def on_click(self):
        for item in self.menu_items:
            self.world.change_visibility(item, True)

    def on_focus_changed(self, old, new):

        if old is not None \
                and ((old.entity == self or old.entity in self.menu_items) and
                     (new is None or (new.entity not in self.menu_items and new.entity != self))):

            # hide menu items
            for item in self.menu_items:
                self.world.change_visibility(item, False)


class Inventory(BaseItem):
    """
    The inventory entity in a room. The items are actually stored in the global game state. Interacts with the events
    generated by the Mouse class to manage the items in it. Only BaseItem types can be added.

    Combinations to test:

    1. out grab, cancel
    2. out grab, drop into inventory -> add
    3. out grab, drop in (combine) -> remove, add new
    4. out grab, drop out (combine)
    5. in grab, cancel -> stay (actually: remove, add)
    6. in grab, drop into inventory -> remove, add
    7. in grab, drop in (combine) -> remove, remove, add new
    8. in grab, drop out (combine) -> remove
    """

    kind = settings.KIND_INVENTORY_ITEM

    def __init__(self, position, game_state):
        BaseItem.__init__(self, position)
        self.game_state = game_state
        self._was_in_inventory = None
        event_dispatcher.register_event_type(settings.EVT_MOUSE_DROPPED)
        event_dispatcher.register_event_type(settings.EVT_MOUSE_GRABBED)
        event_dispatcher.register_event_type(settings.EVT_MOUSE_DROP_CANCELED)
        event_dispatcher.register_event_type(settings.EVT_ITEMS_COMBINED)

        add_event_listener(settings.EVT_MOUSE_GRABBED, self._entity_grabbed)
        add_event_listener(settings.EVT_MOUSE_DROPPED, self._entity_dropped)
        add_event_listener(settings.EVT_MOUSE_DROP_CANCELED, self._entity_drop_canceled)
        add_event_listener(settings.EVT_ITEMS_COMBINED, self._entities_combined)

    def _entities_combined(self, item1, item2, combination):
        if self.game_state.inventory_contains(item1) or self.game_state.inventory_contains(item2):
            if isinstance(combination, BaseItem):
                self.game_state.remove_from_inventory(item1)
                self.game_state.remove_from_inventory(item2)
                self.game_state.add_to_inventory(combination)
            else:
                logger.warning("combined entity not added to inventory because its not a BaseEntity: %s", combination)

    def _entity_drop_canceled(self, entity, position):
        if self._was_in_inventory == entity:
            event_dispatcher.fire(settings.EVT_GUMM_SOUND, resources.resource_sound_click)
            self.game_state.add_to_inventory(entity)
            self._was_in_inventory = None

    def _entity_dropped(self, dropped_entity, target_entity, position):
        if target_entity == self:
            event_dispatcher.fire(settings.EVT_GUMM_SOUND, resources.resource_sound_click)
            self.game_state.add_to_inventory(dropped_entity)
        elif self._was_in_inventory is dropped_entity:
            # added by Gumm
            event_dispatcher.fire(settings.EVT_GUMM_SOUND, resources.resource_sound_click)
            pass
        else:
            self._was_in_inventory = None
            event_dispatcher.fire(settings.EVT_GUMM_SOUND, resources.resource_sound_click)
            self.game_state.remove_from_inventory(dropped_entity)

    def _entity_grabbed(self, entity):
        if self.game_state.remove_from_inventory(entity):
            event_dispatcher.fire(settings.EVT_GUMM_SOUND, resources.resource_sound_click)
            self._was_in_inventory = entity

    def on_click(self):
        return False

    def on_drop(self, other):
        if isinstance(other, BaseItem):
            return True


class ItemMix(ItemToGrab):

    def __init__(self, position, world, kind):
        ItemToGrab.__init__(self, position, kind)
        self.world = world

    def on_click(self):
        return ItemToGrab.on_click(self)

    def on_drop(self, other):
        return False


# hover
# hover with grabbed
# +grab
# +drop
# magnify
# de-magnify
# +activate

class Mouse(object):

    def __init__(self, combiner, renderer, cam):
        self.combiner = combiner
        self.grabbed_item = None
        self.renderer = renderer
        self.cam = cam
        self.focused_sprite = None

    def right_click(self):
        if self.grabbed_item:
            self.grabbed_item.position.copy_values(self.grabbed_item.old_position)
            event_dispatcher.fire(settings.EVT_MOUSE_DROP_CANCELED, self.grabbed_item,
                                  self.grabbed_item.position.clone())
            self.grabbed_item = None

    def click(self, entity):
        if not entity:
            return

        if self.grabbed_item:
            new = self.combiner.combine(entity, self.grabbed_item)
            if new:
                event_dispatcher.fire(settings.EVT_MOUSE_DROPPED, self.grabbed_item, entity,
                                      self.grabbed_item.position.clone())
                event_dispatcher.fire(settings.EVT_ITEMS_COMBINED, self.grabbed_item, entity, new)
                self.grabbed_item = None
            else:
                accept = entity.on_drop(self.grabbed_item)
                if accept:
                    self.grabbed_item.set_new_position()
                    event_dispatcher.fire(settings.EVT_MOUSE_DROPPED, self.grabbed_item, entity,
                                          self.grabbed_item.position.clone())
                    self.grabbed_item = None
        else:
            do_grab = entity.on_click()
            if do_grab:
                self.grabbed_item = entity
                event_dispatcher.fire(settings.EVT_MOUSE_GRABBED, entity)

    def on_motion(self, world_pos):
        if self.grabbed_item:
            self.grabbed_item.position.copy_values(world_pos)

    def handle_action(self, action, extra):
        if action == settings.ACTION_RMB:
            self.right_click()
        elif action == settings.ACTION_LMB:
            sprites = self.renderer.get_sprites_at_tuple(extra)
            entity = None
            if sprites:
                pos = Point(*extra)
                for spr in sprites:
                    if isinstance(spr, EntitySprite) and self._hit_sprite(spr, pos) and spr.entity != self.grabbed_item:
                        entity = spr.entity
                        break
            self.click(entity)
        elif action == settings.ACTION_MOUSE_MOTION:
            # do hover highlight on the sprite?
            pos, rel, buttons = extra
            mouse_pos = Point(*pos)
            world_pos = self.cam.screen_to_world(mouse_pos)
            self.on_motion(world_pos)
            sprites = self.renderer.get_sprites_at_tuple(pos)
            hover_sprites = [spr for spr in sprites if spr.entity != self.grabbed_item]
            focused_before = self.focused_sprite
            to_hover = self._hit_test(hover_sprites, mouse_pos)
            if to_hover:
                if self.focused_sprite != to_hover:
                    if self.focused_sprite:
                        self.focused_sprite.on_focus_lost()
                        event_dispatcher.fire(settings.EVT_FOCUS_LOST, self.focused_sprite)
                    self.focused_sprite = to_hover
                    self.focused_sprite.on_focus()
                    event_dispatcher.fire(settings.EVT_FOCUS_GAINED, self.focused_sprite)
            else:
                if self.focused_sprite:
                    self.focused_sprite.on_focus_lost()
                    event_dispatcher.fire(settings.EVT_FOCUS_LOST, self.focused_sprite)
                    self.focused_sprite = None
            if focused_before != self.focused_sprite:
                event_dispatcher.fire(settings.EVT_FOCUS_CHANGED, focused_before, self.focused_sprite)

    def _hit_sprite(self, spr, pos):
        if spr.image is None:
            return False
        if spr.entity is not None and spr.entity.kind == settings.KIND_GORE_HEAD:
            return True
        if not hasattr(spr, 'cached_mask'):
            mask = pygame.mask.from_surface(spr.image)
            spr.cached_mask = mask
        p = pos - Point(*spr.rect.topleft)
        try:
            hit = spr.cached_mask.get_at(p.as_xy_tuple(int))
            if hit:
                return True
        except IndexError:
            logger.error("IndexError, position %s outside of mask with rect %s", pos.as_xy_tuple(int), spr.rect)
        return False

    def _hit_test(self, hover_sprites, pos):
        # return hover_sprites[0]
        for spr in hover_sprites:
            if self._hit_sprite(spr, pos):
                return spr
        return None


class RoomContext(TimeSteppedContext):

    def __init__(self, game_state: GameState, music_player):
        TimeSteppedContext.__init__(self, settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS)
        self.game_state = game_state
        self.renderer = DefaultRenderer()
        screen_rect = pygame.Rect(0, 0, settings.screen_width, settings.screen_height)
        self.cam = Camera(screen_rect, padding=196 // 2)
        self.cam.set_position(Point(screen_rect.centerx, screen_rect.centery))
        self.screen = None
        self._is_debug_render_on = self.game_state._is_debug_render_on
        self.tweener = Tweener()
        self.focused_sprite = None
        self.sfx_handler = SfxHandler(music_player)
        self.sfx_handler.register_events(event_dispatcher)

        # world
        self.scheduler = Scheduler()
        self.collider = Collider()

        # game
        self.combiner = Combiner()
        self.combiner.register_pair(settings.KIND_WRENCH_ITEM, settings.KIND_MUSHROOM_ITEM,
                                    self._combine_wrench_and_mushroom)
        self.combiner.register_pair(settings.KIND_CROWBAR_ITEM, settings.KIND_GORE_HEAD,
                                    self._combine_crowbar_and_gore_head)
        self.combiner.register_pair(settings.KIND_SUPERTOOL_ITEM, settings.KIND_MESSAGE_CLOG_ITEM,
                                    self._combine_supertool_and_message_clog)
        self.combiner.register_pair(settings.KIND_CROWBAR_ITEM, settings.KIND_DUMP_TRUCK_ITEM,
                                    self._combine_crowbar_and_dump_truck)
        self.combiner.register_pair(settings.KIND_SUPERTOOL_ITEM, settings.KIND_YARD_DOOR_TO_BASEMENT,
                                    self._combine_supertool_and_yard_basement_door)
        self.mouse = Mouse(self.combiner, self.renderer, self.cam)
        self.entities = []
        self.game_keys = self.game_state.get_keys()
        self.sprites = set()

        self.debug_hud = hudlight.HUD(**settings.font_themes['debug'])
        self._make_debug_hud()
        self.hud = hudlight.HUD(**settings.font_themes['gamehud'])
        self.hud.x = 33
        self.hud.y = 36
        self._make_hud()

        event_dispatcher.register_event_type(settings.EVT_FOCUS_GAINED)
        event_dispatcher.register_event_type(settings.EVT_FOCUS_LOST)
        event_dispatcher.register_event_type(settings.EVT_FOCUS_CHANGED)
        event_dispatcher.register_event_type(settings.EVT_ADDED_TO_INVENTORY)
        event_dispatcher.register_event_type(settings.EVT_REMOVED_FROM_INVENTORY)

        event_dispatcher.register_event_type(settings.EVT_MOUSE_DROPPED)
        event_dispatcher.register_event_type(settings.EVT_MOUSE_GRABBED)
        event_dispatcher.register_event_type(settings.EVT_MOUSE_DROP_CANCELED)
        event_dispatcher.register_event_type(settings.EVT_ITEMS_COMBINED)

        add_event_listener(settings.EVT_MOUSE_GRABBED, self._do_z_layer_grabbed)
        add_event_listener(settings.EVT_MOUSE_DROPPED, self._do_z_layer_dropped)
        add_event_listener(settings.EVT_MOUSE_DROP_CANCELED, self._do_z_layer_drop_canceled)
        add_event_listener(settings.EVT_ITEMS_COMBINED, self._do_z_layer_combined)
        add_event_listener(settings.EVT_ADDED_TO_INVENTORY, self._do_z_layer_inventory)
        add_event_listener(settings.EVT_FOCUS_CHANGED, self._on_focus_changed)

        # todo: dev stuff; delete me when no longer needed
        if hasattr(self, 'dev_init'):
            self.dev_init()

    def _make_hud(self):
        hud = self.hud

        def get_time():
            return self.game_state.clock.get_time_as_string()

        hud.add('time', '{}', get_time(), callback=get_time)

    def hours_passed(self):
        return int(self.game_state.clock.hours_passed_since_init())

    def tick_hour(self):
        self.game_state.clock.tick_hour(1)
        elapsed = self.hours_passed()
        if elapsed == 1:
            pass
        elif elapsed == 2:
            # make Gore and his head hidden in the room_state
            room_state = self.game_state.get_room_state(settings.KIND_ROOM_LAB)
            gore_ents = tuple(e for e in room_state.entities if e.kind == settings.KIND_NPC_GORE)
            gore_ents += tuple(e for e in room_state.entities if e.kind == settings.KIND_GORE_HEAD)
            for e in gore_ents:
                e.hidden = True
                e.reveal_action = None
            # remove Gore entities and sprites from room context
            gore_ents = self.get_entities_by_kind(settings.KIND_NPC_GORE)
            gore_ents += self.get_entities_by_kind(settings.KIND_GORE_HEAD)
            for e in gore_ents:
                self.remove_entity(e)
                sprs = self.get_sprites_by_entity(e)
                self.remove_sprites(sprs)
        elif elapsed == 3:
            pass
        elif elapsed == 4:
            pass
        elif elapsed == 5:
            pass  # checked in the update_step
        #     if self.game_keys.key_count() == 5:
        #         self.game_state.game_won = True
        #         self.pop(1, effect=pyknic.pyknic_pygame.context.effects.FadeOutFadeInEffect(settings.fade_duration))
        #         self.push(WinContext(self.game_state))
        #     else:
        #         self.game_state.global_reset = True
        #         self.pop(1, effect=pyknic.pyknic_pygame.context.effects.FadeOutFadeInEffect(settings.fade_duration))
        #         self.push(LooseContext(self.game_state))
        elif elapsed == 6:
            pass
        elif elapsed > settings.reset_after_n_hours:
            pass

    def _make_debug_hud(self):
        hud = self.debug_hud
        hud.y = 50

        def get_room_name():
            kind = self.game_state.current_room
            name = settings._global_id_generator.names.get(kind, None)
            return name if name is None else name[10:]

        hud.add('room', 'Room: {}', get_room_name(), callback=get_room_name)

        def get_grabbed():
            item = self.mouse.grabbed_item
            if item is not None:
                name = settings._global_id_generator.names.get(item.kind, None)
                return 'KIND unknown' if name is None else name[5:]

        hud.add('grabbed', 'Grabbed: {}', get_grabbed(), callback=get_grabbed)

        def get_fo():
            return self.game_keys.fridge_open

        def get_hw():
            return self.game_keys.has_wrench

        def get_hm():
            return self.game_keys.has_mushroom

        def get_hs():
            return self.game_keys.has_supertool

        def get_hc():
            return self.game_keys.has_crowbar

        def get_wg():
            return self.game_keys.whacked_gore

        def get_dt():
            return self.game_keys.hijacked_dump_truck

        def get_um():
            return self.game_keys.unclogged_messages

        def get_reset_count():
            return self.game_state.get_reset_count()

        hud.add('fo', 'fridge_open: {}', get_fo(), callback=get_fo)
        hud.add('hw', 'has_wrench: {}', get_hw(), callback=get_hw)
        hud.add('hm', 'has_mushroom: {}', get_hm(), callback=get_hm)
        hud.add('hs', 'has_supertool: {}', get_hs(), callback=get_hs)
        hud.add('hc', 'has_crowbar: {}', get_hc(), callback=get_hc)
        hud.add('wg', 'whacked_gore: {}', get_wg(), callback=get_wg)
        hud.add('dt', 'hijacked_dump_truck: {}', get_dt(), callback=get_dt)
        hud.add('um', 'unclogged_messages: {}', get_um(), callback=get_um)
        hud.add('progress', 'progress: {}', self.game_keys.key_count(), callback=self.game_keys.key_count)
        hud.add('hours', 'hours: {:0.2f}', self.game_state.clock.hours_passed_since_init(),
                callback=self.game_state.clock.hours_passed_since_init)
        hud.add('rc', 'reset count: {}', get_reset_count(), callback=get_reset_count)

    def dev_use_exit(self, action):
        """todo: delete me; keyboard navigation for early dev"""
        if action == settings.ACTION_GO_EAST:
            s = self.game_state.get_room_state()
            # logger.error(f's={s}')
            # logger.error(f'exit={s.east_exit}')
            # logger.error('{}', settings._global_id_generator.names[s.east_exit])
            # quit()
            self.game_state.move_to_room = s.east_exit
        elif action == settings.ACTION_GO_NORTH:
            self.game_state.move_to_room = self.game_state.get_room_state().north_exit
        elif action == settings.ACTION_GO_WEST:
            self.game_state.move_to_room = self.game_state.get_room_state().west_exit

    def enter(self):
        # pygame.mouse.set_visible(False)
        # event_dispatcher.register_event_type(settings.EVT_NOISE)
        # self.screen = pygame.display.get_surface()
        if resource_manager.resources:
            self.resume()

    def exit(self):
        # pygame.mouse.set_visible(True)
        # event_dispatcher.clear()  # todo need to clear all events that might still be in the queue and remove all listeners?
        names = settings._global_id_generator.names
        logger.info(f'EXIT: RoomContext {names[self.game_state.current_room]}')
        for e in self.game_state.get_inventory_entities():
            logger.info(f'EXIT: item in inventory {names[e.kind]}')
        self.game_state.add_to_inventory_sprites(self.sprites)

        self.game_state._is_debug_render_on = self._is_debug_render_on

        if self.game_state.get_room_state().kind == settings.KIND_ROOM_STEVENS_BASEMENT:
            self.tick_hour()

        remove_event_listeners()

    def suspend(self):
        pass

    def resume(self):
        pygame.mouse.set_visible(True)
        if not self.screen:
            self.screen = pygame.display.get_surface()

        if self.game_state.get_room_state().kind == settings.KIND_ROOM_STEVENS_BASEMENT:
            # going here always wastes time
            self.tick_hour()

        # self.game_keys = self.game_state.get_keys()

        # todo: make room's items here, using LabState data (etc.) in self.game_state.room_states
        # self.add_grab_item(Point(50, 50), resources.resource_item1)
        # self.add_mix_item(Point(100, 300))
        self._load_game_objects()

        inventory_img = resource_manager.resources[resources.resource_item_inventory].image
        w, h = inventory_img.get_size()
        inventory = Inventory(Point(settings.screen_width / 2, settings.screen_height - h / 2 - 6), self.game_state)
        self.entities.append(inventory)
        spr2 = EntitySprite(inventory, inventory_img, inventory.position, anchor='center',
                            z_layer=settings.Z_LAYER_INVENTORY)
        self.add_sprite(spr2)

        clock_frame = resource_manager.resources[resources.clock_frame].image
        spr3 = BaseSprite(clock_frame, Point(5, 5), anchor='topleft', z_layer=settings.Z_LAYER_INVENTORY)
        self.add_sprite(spr3)

        # clock_face = resource_manager.resources[resources.clock_face].image
        # spr4 = BaseSprite(clock_face, Point(98, 1), anchor='topleft', z_layer=settings.Z_LAYER_INVENTORY+1)
        # self.add_sprite(spr4)

        # register collider funcs

    def _load_game_objects(self):
        names = settings._global_id_generator.names
        logger.debug('ROOM: loading objects into {}', names[self.game_state.current_room])
        room_state = self.game_state.get_room_state()

        # todo
        # # make closed fridge's image transparent
        # closed_fridge_sprite = self.get_sprites_by_entity(closed_fridge_ent)[0]
        # logger.debug('FRIDGE: found closed fridge sprite')

        # restore Inventory from game_state cache
        for ent, spr_door in self.game_state.get_inventory_sprites().items():
            self.entities.append(ent)
            self.add_sprite(spr_door)

        for item_data in room_state.entities:
            logger.debug('loading entity: {} {}', names[item_data.kind], item_data.position)

            # don't load if found in inventory
            if self.game_state.inventory_contains(kind=item_data.kind):
                continue

            # create an item and place it in the room
            kind = item_data.kind
            if self.game_state.get_ent_by_kind(kind):
                continue
            if kind == settings.KIND_WRENCH_ITEM and not self.game_keys.has_wrench:
                self.add_grab_item(item_data, resources.resource_wrench, zoom=0.5)
            elif kind == settings.KIND_CLOSED_FRIDGE_ITEM:
                e = self.add_activate_item(item_data, resources.resource_closed_fridge, self._open_fridge,
                                           zoom=1.0, z_layer=settings.Z_LAYER_BACKGROUND)
                if self.game_keys.fridge_open:
                    e.hidden = True
                    s = self.get_sprites_by_entity(e)[0]
                    s.visible = False
            elif kind == settings.KIND_OPEN_FRIDGE_ITEM:
                # self.add_entity_sprite(item_data, resources.resource_open_fridge,
                #                        zoom=0.8, z_layer=settings.Z_LAYER_BACKGROUND)
                e = self.add_activate_item(item_data, resources.resource_open_fridge, self._close_fridge,
                                           zoom=1.0, z_layer=settings.Z_LAYER_BACKGROUND)
                if self.game_keys.fridge_open:
                    e.hidden = False
                    s = self.get_sprites_by_entity(e)[0]
                    s.visible = True
            elif kind == settings.KIND_PIZZA_ITEM:
                self.add_activate_item(item_data, resources.resource_pizza, self._grab_pizza,
                                       z_layer=settings.Z_LAYER_SURFACE)
            elif kind == settings.KIND_MUSHROOM_ITEM:
                self.add_grab_item(item_data, resources.resource_mushroom,
                                   zoom=0.25, z_layer=settings.Z_LAYER_GRABBED)
            elif kind == settings.KIND_SUPERTOOL_ITEM:
                st_in_inv = self.game_state.get_ent_by_kind(settings.KIND_SUPERTOOL_ITEM)
                if st_in_inv is None:
                    self.add_grab_item(item_data, resources.resource_super_tool,
                                       zoom=0.5, z_layer=settings.Z_LAYER_SURFACE)
            # todo: add Gore activate item
            elif kind == settings.KIND_CROWBAR_ITEM:
                self.add_grab_item(item_data, resources.resource_crowbar,
                                   zoom=0.8, z_layer=settings.Z_LAYER_SURFACE)
            elif kind == settings.KIND_DUMP_TRUCK_ITEM:
                # dump truck is only full during hour 3
                # prior to hour 3 it is empty, and wastes time to hijack it
                # after hour 3 it is too late to stop Gore
                logger.error('HOURS PASSED: {} keys={}', self.hours_passed(), self.game_keys.key_count())
                if self.hours_passed() <= 3:
                    logger.error('MAKE TRUCK: {}', self.hours_passed())
                    self.add_entity_sprite(item_data, resources.resource_dump_truck_empty,
                                           z_layer=settings.Z_LAYER_BACKGROUND)
                if self.hours_passed() == 3 and self.game_keys.key_count() == 3 or settings.fake_full_dump_truck:
                    logger.error('MAKE DIRT: {}', self.hours_passed())
                    # 3=grab crowbar, whack gore, make supertool
                    self.add_entity_sprite(item_data, resources.resource_dump_truck_full,
                                           z_layer=settings.Z_LAYER_BACKGROUND, custom_position=(900, 100))
            elif kind == settings.KIND_MESSAGE_CLOG_ITEM:
                self.add_entity_sprite(item_data, resources.resource_email_pile, zoom=0.8, z_layer=settings.Z_LAYER_BACKGROUND),
            elif kind == settings.KIND_YARD_DOOR_TO_BASEMENT:
                position = Point(*item_data.position)
                img = resource_manager.resources[resources.resource_stevens_yard_door_to_basement].image
                if not self.game_keys.enter_stevens_basement:
                    # add basement door
                    entity = InertItem(position, kind)
                    spr = EntitySprite(entity, img, entity.position,
                                       z_layer=settings.Z_LAYER_BACKGROUND + 2)  # just to make sure its behind the other one
                    self.entities.append(entity)
                    self.add_sprite(spr)

                # add hidden door for navigation, will be set visible when supertool has been combined with door
                hidden = not self.game_keys.enter_stevens_basement
                dest_id = settings.KIND_ROOM_STEVENS_BASEMENT
                cb = lambda x: self.navigate_to(x.kind)
                nav_item = self.add_activate_item_pure(position.clone(), dest_id, dest_id, cb, True, None, z_layer=199)
                menu_items = [nav_item]
                door = Door(position.clone(), settings.KIND_YARD_DOOR_TO_BASEMENT_OPEN, hidden, menu_items, self)
                self.entities.append(door)
                door_spr = EntitySprite(door, img, door.position, z_layer=settings.Z_LAYER_BACKGROUND + 1)
                door_spr.visible = not hidden
                self.add_sprite(door_spr)

            elif kind == settings.KIND_ROOM_DOOR:
                # # add menu with navigation properties
                destination_ids = [e for e in [room_state.west_exit, room_state.north_exit, room_state.east_exit] if
                                   e is not None]
                destinations = [(settings._global_id_generator.names.get(d_id, "Unnamed kind!"), d_id) for d_id in
                                destination_ids]
                img = resource_manager.resources[item_data.resource_id].image
                door_position = Point(*item_data.position)
                door_position += Vec3(*img.get_size()) / 2  # center
                pos = door_position.clone()
                items_height = len(destinations) * settings.menu_item_distance
                r = items_height / 2 - settings.menu_item_distance / 2
                pos.y -= r
                menu_items = []
                logger.debug(f"Creating door with destinations: {destinations}")
                for name, dest_id in destinations:
                    cb = lambda x: self.navigate_to(x.kind)
                    menu_item = self.add_activate_item_pure(pos.clone(), dest_id, dest_id, cb,
                                                            True, None, z_layer=199)
                    menu_items.append(menu_item)
                    pos += Vec3(0, settings.menu_item_distance)

                # door = self.add_activate_item(item_data, resources.resource_lab_door, self._activate_door)
                door = Door(door_position, kind, item_data.hidden, menu_items, self)
                self.entities.append(door)
                spr_door = EntitySprite(door, img, door.position, z_layer=10)
                spr_door.visible = not door.hidden
                self.add_sprite(spr_door)
            elif kind == settings.KIND_BACKGROUND:
                background_img = resource_manager.resources[item_data.resource_id].image
                background_spr = BaseSprite(background_img, Point(0, 0), anchor='topleft', z_layer=-100)
                self.add_sprite(background_spr)
            elif kind == settings.KIND_NPC_STEVENS:
                img = resource_manager.resources[item_data.resource_id].image
                pos = Point(*item_data.position)

                logger.debug("ted stevens in basement")
                menu_items = self._add_menu_dialogue(pos, item_data.menu_dialogue)

                door = Door(pos, kind, item_data.hidden, menu_items, self)
                self.entities.append(door)
                spr_door = EntitySprite(door, img, door.position, z_layer=settings.Z_LAYER_OVER)
                spr_door.visible = not door.hidden
                self.add_sprite(spr_door)
            elif kind == settings.KIND_GORE_HEAD and self.hours_passed() < 2:
                pos = Point(*item_data.position)
                ent = InertItem(pos, kind)
                self.entities.append(ent)
                img = resource_manager.resources[item_data.resource_id].image
                spr = EntitySprite(ent, img, pos, z_layer=settings.Z_LAYER_TOP)
                self.add_sprite(spr)
            elif kind == settings.KIND_NPC_GORE and self.hours_passed() < 2:
                img = resource_manager.resources[item_data.resource_id].image
                pos = Point(*item_data.position) + Vec3(*img.get_size()) / 2
                # action = self._conversion_lab
                # self.add_activate_item_pure(pos, item_data.kind, item_data.resource_id, action, item_data.hidden,
                #                             item_data.reveal_action, settings.Z_LAYER_OVER)

                logger.debug("lab al gore npc")
                menu_items = self._add_menu_dialogue(pos, item_data.menu_dialogue)

                door = Door(pos, kind, item_data.hidden, menu_items, self)
                self.entities.append(door)
                spr_door = EntitySprite(door, img, door.position, z_layer=settings.Z_LAYER_OVER)
                spr_door.visible = not door.hidden
                self.add_sprite(spr_door)

            elif kind == settings.KIND_TEXT_BUBBLE:

                self._add_dialogue_data(item_data.dialogue, item_data.hidden, False)

            else:
                name = settings._global_id_generator.names.get(kind, 'name unknown')
                logger.error('unhandled KIND: id={} name={}', kind, name)

    def _add_menu_dialogue(self, position, menu_dialogue, play_only_once=False):
        menu_items = []

        def call_back(entity):
            ents = [ent for ent in self.get_entities_by_kind(settings.KIND_TEXT_BUBBLE) if
                    ent.dialogue_id == entity.dialogue_id]
            if ents:
                self.change_visibility(ents[0], True)

        pos = position.clone() if menu_dialogue.position is None else Point(*menu_dialogue.position)
        for menu_item_resource_id, dialogue in menu_dialogue.menu_dialogue_data.items():
            menu_item = self.add_activate_item_pure(pos.clone(), settings.KIND_MENU_ITEM, menu_item_resource_id,
                                                    call_back,
                                                    True, None, z_layer=settings.Z_LAYER_OVER + 1)
            menu_item.dialogue_id = dialogue.id
            pos += Vec3(0, settings.menu_item_distance)
            if self._add_dialogue_data(dialogue, True, play_only_once):
                menu_items.append(menu_item)

        return menu_items

    def _add_dialogue_data(self, dialogue, hidden, play_only_once):
        if dialogue.reset_count != -1 and dialogue.reset_count != self.game_state.get_reset_count():
            # only allow certain dialogs at certain reset counts (or always if -1)
            logger.info("dialogue %s not added since reset_count did not match", dialogue.id)
            return False

        if dialogue.id in self.game_state.played_dialogues:
            # the dialogue has been played once, will reset
            logger.info("dialogue %s not added because it should only played once until reset!", dialogue.id)
            return False

        bubbles = []
        first = True
        for bubble_data in dialogue.bubble_data:
            res = resource_manager.resources[bubble_data.resource_id]
            z_layer = settings.Z_LAYER_TOP
            position = Point(*bubble_data.position)
            rect = res.image.get_rect(topleft=position.as_xy_tuple())

            dialogue_id = dialogue.id if first else -1
            first = False
            button = self.add_activate_item_pure(Point(*rect.midbottom), settings.KIND_TEXT_BUBBLE_BUTTON,
                                                 resources.resource_next_button, None,
                                                 hidden, None, z_layer=z_layer + 1)
            speech_bubble = SpeechBubble(position, button, res.sound_data, self, settings.KIND_TEXT_BUBBLE, dialogue_id,
                                         play_only_once)

            spr_door = EntitySprite(speech_bubble, res.image, position, anchor='topleft', z_layer=z_layer)
            spr_door.visible = not hidden
            self.add_sprite(spr_door)
            source_pos = Point(*bubble_data.source_position)
            c = Point(*rect.center)
            v = (source_pos - c).normalized
            p1 = c + v.normal_right * settings.speech_bubble_pointer_width / 2
            p2 = c - v.normal_right * settings.speech_bubble_pointer_width / 2
            # this would be a nicer version with.... but no time
            # rect = res.image.get_rect(topleft=bubble_data.position)
            # if source_pos.x <= rect.left:
            #     if source_pos.y <= rect.top:
            #         diff = source_pos - Point(*rect.topleft)
            #         if diff.x > diff.y:
            #             p1 = Point(*rect.topleft)
            #             p2 = Point(*rect.bottomleft)
            #         else:
            #             p1 = Point(*rect.topleft)
            #             p2 = Point(*rect.topright)
            #     elif source_pos.y >= rect.bottom:
            #         diff = source_pos - Point(*rect.bottomleft)
            #         if abs(diff.x) > abs(diff.y):
            #             p1 = Point(*rect.topleft)
            #             p2 = Point(*rect.bottomleft)
            #         else:
            #             p1 = Point(*rect.topleft)
            #             p2 = Point(*rect.topright)

            color = settings.speech_bubble_pointer_color
            pointer_spr = BubblePointerSprite(speech_bubble, source_pos, p1, p2, color, 'topleft', z_layer - 1)
            pointer_spr.visible = not hidden
            self.add_sprite(pointer_spr)

            bubbles.append(speech_bubble)
            self.entities.append(speech_bubble)

        # link
        prev = bubbles[0]
        for b in bubbles[1:]:
            prev.next = b
            prev = b

        return True

    def _conversion_lab(self, npc):
        logger.info("start talking to al gore in lab")
        ents = [ent for ent in self.get_entities_by_kind(settings.KIND_TEXT_BUBBLE) if
                ent.dialogue_id == settings.KIND_DIALOGUE_1]
        if ents:
            self.change_visibility(ents[0], True)

    def navigate_to(self, destination_id):
        logger.info(
            f"navigate to {destination_id} {settings._global_id_generator.names.get(destination_id, 'Unknown')}")
        event_dispatcher.fire(settings.EVT_GUMM_SOUND, resources.resource_sound_door)
        self.game_state.move_to_room = destination_id

    def remove_entity(self, entity):
        if entity in self.entities:
            self.entities.remove(entity)
            sprites = self.get_sprites_by_entity(entity)
            self.remove_sprites(sprites)

    def add_sprite(self, spr):
        self.renderer.add_sprite(spr)
        self.sprites.add(spr)

    def remove_sprites(self, sprites):
        for spr in sprites:
            spr.visible = False  # hack to avoid re-adding on any other change on the sprite
        self.renderer.remove_sprites(sprites)
        self.sprites.difference_update(sprites)

    def _get_cached_inventory_by_kind(self, kind):
        ent = self.game_state.get_ent_by_kind(kind)
        spr = self.game_state.get_inventory_sprite(ent)
        return ent, spr

    def add_entity_sprite(self, item_data, resource_id, z_layer=100, zoom=1.0, custom_position=None):
        entity = InertItem(item_data.position, item_data.kind)
        if item_data.hidden:
            entity.hide()
        self.entities.append(entity)
        img = resource_manager.resources[resource_id].image
        spr = EntitySprite(entity, img, entity.position, z_layer=z_layer)
        spr.visible = not entity.hidden
        spr.zoom = zoom
        if custom_position is not None:
            spr.position = Point(*custom_position)
        self.add_sprite(spr)
        return entity

    def add_activate_item(self, item_data, resource_id, action, z_layer=100, zoom=1.0):
        return self.add_activate_item_pure(item_data.position, item_data.kind, resource_id, action, item_data.hidden,
                                           item_data.reveal_action, z_layer, zoom)

    def add_activate_item_pure(self, position, kind, resource_id, action, hidden, reveal_action, z_layer, zoom=1.0):
        entity = ItemToActivate(position, kind, action, hidden, reveal_action)
        self.entities.append(entity)
        img = resource_manager.resources[resource_id].image
        spr = EntitySprite(entity, img, entity.position, z_layer=z_layer)
        spr.visible = not entity.hidden
        spr.zoom = zoom
        self.add_sprite(spr)
        return entity

    def add_grab_item(self, item_data, resource_id, z_layer=100, position=None, zoom=1.0):
        kind = item_data.kind
        entity = ItemToGrab(item_data, kind)
        if position:
            entity.position = position.clone()
        self.entities.append(entity)
        resource = resource_manager.resources[resource_id]
        img = resource.image
        spr1 = EntitySprite(entity, img, entity.position, z_layer=z_layer)
        spr1.zoom = zoom
        spr1.visible = not item_data.hidden
        if kind == settings.KIND_CROWBAR_ITEM:
            spr1.rotation = -75
        self.add_sprite(spr1)
        # hover
        if hasattr(resource, "get_hover_image"):
            h_img, h_anchor = resource.get_hover_image('center')
            spr1.hover_sprite = BaseSprite(h_img, entity.position, anchor=h_anchor, z_layer=z_layer - 1)
            spr1.hover_sprite.visible = False

        return entity

    def _combine_wrench_and_mushroom(self, wrench, mushroom):
        event_dispatcher.fire(settings.EVT_GUMM_SOUND, resources.resource_sound_success)
        self.remove_entity(wrench)
        self.remove_entity(mushroom)
        super_tool = self.get_entities_by_kind(settings.KIND_SUPERTOOL_ITEM)[0]  # use the existing one
        super_tool.position.copy_values(wrench.position)
        self.change_visibility(super_tool, True)
        self.tick_hour()
        return super_tool

    def _combine_crowbar_and_gore_head(self, crowbar, gore_head):
        logger.debug('combine: CROWBAR + GORE HEAD')
        event_dispatcher.fire(settings.EVT_GUMM_SOUND, resources.resource_sound_hit_gore)
        ents = self.get_entities_by_kind(settings.KIND_NPC_GORE)
        ents += self.get_entities_by_kind(settings.KIND_GORE_HEAD)
        for e in ents:
            sprs = self.get_sprites_by_entity(e)
            self.remove_sprites(sprs)
            self.remove_entity(e)
        self.mouse.right_click()
        self.game_keys.whacked_gore = True
        self.tick_hour()
        return False

    def _combine_supertool_and_message_clog(self, supertool, message_clog):
        logger.debug('combing: SUPERTOOL + MESSAGE CLOG')
        event_dispatcher.fire(settings.EVT_PLAY_SOUND_DATA, resources.resource_sound_hit_gore)
        self.remove_entity(message_clog)
        sprs = self.get_sprites_by_entity(message_clog)
        self.remove_sprites(sprs)
        self.game_keys.unclogged_messages = True
        self.tick_hour()
        return False

    def _combine_crowbar_and_dump_truck(self, crowbar, dump_truck):
        logger.debug('combining: BNE + DUMP TRUCK')
        event_dispatcher.fire(settings.EVT_PLAY_SOUND_DATA, resources.resource_sound_hit_gore)
        self.remove_entity(dump_truck)
        sprs = self.get_sprites_by_kind(dump_truck.kind)
        self.remove_sprites(sprs)
        self.mouse.right_click()  # drop it back to the inventory
        self.tick_hour()
        if self.game_keys.key_count() == 3:
            self.game_keys.hijacked_dump_truck = True
        return False

    def _combine_supertool_and_yard_basement_door(self, super_tool, door):
        logger.info("combining supertool and door")
        event_dispatcher.fire(settings.EVT_PLAY_SOUND_DATA, resources.resource_sound_hit_gore)
        # self.remove_entity(super_tool)
        # todo open sound to let the player know something happened?
        self.mouse.right_click()  # drop it back to the inventory
        self.remove_entity(door)
        self.game_keys.enter_stevens_basement = True
        ents = self.get_entities_by_kind(settings.KIND_YARD_DOOR_TO_BASEMENT_OPEN)
        if ents:
            self.change_visibility(ents[0], True)
        else:
            logger.error("Could not find yard to basement door open!")
        return False  # let the mouse drop it

    def _find_item_data(self, kind):
        return [i for i in self.game_state.get_room_state().entities if i.kind == kind]

    def _close_fridge(self, open_fridge_ent):
        event_dispatcher.fire(settings.EVT_GUMM_SOUND, resources.resource_sound_door)
        self.change_visibility(open_fridge_ent, False)
        closed_fridge_ent = self.get_entities_by_kind(settings.KIND_CLOSED_FRIDGE_ITEM)[0]
        self.change_visibility(closed_fridge_ent, True)
        closed_fridge_ent = self.get_entities_by_kind(settings.KIND_PIZZA_ITEM)[0]
        self.change_visibility(closed_fridge_ent, False)

        if not self.game_keys.has_mushroom:
            closed_fridge_ent = self.get_entities_by_kind(settings.KIND_MUSHROOM_ITEM)[0]
            self.change_visibility(closed_fridge_ent, False)

        self.game_keys.fridge_open = False

    def _open_fridge(self, closed_fridge_ent):
        logger.debug('FRIDGE: open fridge')
        if not self.game_keys.fridge_open:
            event_dispatcher.fire(settings.EVT_GUMM_SOUND, resources.resource_sound_door)
            self.change_visibility(closed_fridge_ent, False)

            open_fridge_ent = self.get_entities_by_kind(settings.KIND_OPEN_FRIDGE_ITEM)[0]
            self.change_visibility(open_fridge_ent, True)
            # show pizza sprite
            pizza_ent = self.get_entities_by_kind(settings.KIND_PIZZA_ITEM)[0]
            logger.debug('FRIDGE: found pizza entity')
            self.change_visibility(pizza_ent, True)
            self.game_keys.fridge_open = True
            logger.debug('FRIDGE: found pizza sprite')

    def _grab_pizza(self, pizza_ent):
        logger.debug('PIZZA: grab pizza')
        if not self.game_keys.has_mushroom:
            mushroom_ent = self.get_entities_by_kind(settings.KIND_MUSHROOM_ITEM)
            if mushroom_ent:
                mushroom_ent = mushroom_ent[0]
                logger.debug('PIZZA: found mushroom entity')
                self.change_visibility(pizza_ent, False)
                self.change_visibility(mushroom_ent, True)
                logger.debug('PIZZA: found mushroom sprite')
                # self.mouse.right_click()
                # self.mouse.click(mushroom_ent)

    def _do_z_layer_grabbed(self, ent, *args):
        names = settings._global_id_generator.names
        logger.debug(f'_do_z_layer_grabbed: {names[ent.kind]}')
        if ent.kind != settings.KIND_INVENTORY_ITEM:
            for spr in tuple(self.get_sprites_by_entity(ent)):
                spr.z_layer = settings.Z_LAYER_GRABBED

    def _do_z_layer_dropped(self, ent, *args):
        names = settings._global_id_generator.names
        logger.debug(f'_do_z_layer_dropped: {names[ent.kind]}')
        logger.debug('inventory_contains {}: {}', names[ent.kind], self.game_state.inventory_contains(ent))
        if ent.kind != settings.KIND_INVENTORY_ITEM and self.game_state.inventory_contains(ent):
            for spr in tuple(self.get_sprites_by_entity(ent)):
                spr.z_layer = settings.Z_LAYER_IN_INVENTORY

    def _do_z_layer_drop_canceled(self, ent, *args):
        names = settings._global_id_generator.names
        logger.debug(f'_do_z_layer_drop_canceled: {names[ent.kind]}')
        if ent.kind != settings.KIND_INVENTORY_ITEM and not self.game_state.inventory_contains(ent):
            event_dispatcher.fire(settings.EVT_GUMM_SOUND, resources.resource_sound_click)
            for spr in tuple(self.get_sprites_by_entity(ent)):
                spr.z_layer = settings.Z_LAYER_SURFACE

    def _do_z_layer_combined(self, ent, *args):
        names = settings._global_id_generator.names
        logger.debug(f'_do_z_layer_combined: {names[ent.kind]}')
        # todo: not sure about this (Gumm)
        # if ent.kind != settings.KIND_INVENTORY_ITEM:
        #     for spr in tuple(self.get_sprites_by_entity(ent)):
        #         spr.z_layer = settings.Z_LAYER_IN_INVENTORY
        pass

    def _do_z_layer_inventory(self, ent, *args):
        names = settings._global_id_generator.names
        logger.debug(f'_do_z_layer_inventory: {names[ent.kind]}')
        if ent.kind == settings.KIND_WRENCH_ITEM:
            self.game_keys.has_wrench = True
        elif ent.kind == settings.KIND_MUSHROOM_ITEM:
            self.game_keys.has_mushroom = True
        elif ent.kind == settings.KIND_SUPERTOOL_ITEM:
            self.game_keys.has_wrench = True
            self.game_keys.has_mushroom = True
            self.game_keys.has_supertool = True
        elif ent.kind == settings.KIND_CROWBAR_ITEM:
            if not self.game_keys.has_crowbar:
                self.tick_hour()
            self.game_keys.has_crowbar = True
        if ent.kind != settings.KIND_INVENTORY_ITEM:
            for spr in tuple(self.get_sprites_by_entity(ent)):
                spr.z_layer = settings.Z_LAYER_IN_INVENTORY

    def _on_focus_changed(self, old_spr, new_spr):
        # logger.info("focus change handling")
        if old_spr:
            if old_spr.entity is not None and old_spr.entity.kind == settings.KIND_TEXT_BUBBLE:
                pass
            else:
                if isinstance(old_spr, EntitySprite):
                    old_spr.zoom = old_spr.zoom - settings.FOCUS_ZOOM_CHANGE
        if new_spr:
            if new_spr.entity is not None and new_spr.entity.kind == settings.KIND_TEXT_BUBBLE:
                pass
            else:
                if isinstance(new_spr, EntitySprite):
                    new_spr.zoom = new_spr.zoom + settings.FOCUS_ZOOM_CHANGE

    def change_visibility(self, entity, is_visible):
        if is_visible:
            entity.show()
        else:
            entity.hide()
        sprites = self.get_sprites_by_entity(entity)
        for s in sprites:
            s.visible = is_visible

    def get_entities_by_kind(self, kind):
        return tuple(e for e in self.entities if e.kind == kind)

    def get_sprites_by_kind(self, kind):
        return tuple(s for s in self.sprites if s.entity is not None and s.entity.kind == kind)

    def get_sprites_by_entity(self, entity):
        return tuple(s for s in self.sprites if s.entity == entity)

    def update_step(self, delta, sim_time, *args):
        # logger.debug(f"update_step dt {delta}, sim_t {sim_time}")
        self.tweener.update(delta)
        self.scheduler.update(delta, sim_time)

        elapsed = self.hours_passed()
        if elapsed == 5:
            if self.game_keys.key_count() == 5:
                self.game_state.game_won = True
                self.pop(1, effect=pyknic.pyknic_pygame.context.effects.FadeOutFadeInEffect(settings.fade_duration))
                self.push(WinContext(self.game_state))
            else:
                self.game_state.global_reset = True
                self.pop(1, effect=pyknic.pyknic_pygame.context.effects.FadeOutFadeInEffect(settings.fade_duration))
                self.push(LooseContext(self.game_state))
            return

        self.game_state.clock.tick(datetime.timedelta(0, delta))  # todo not sure about this

        self.handle_events()

        if self._is_debug_render_on:
            self.debug_hud.update()

        # cam control
        # t = settings.cam_speed * delta
        # self.cam.lerp(self.game_logic.hero.position, t)

    def handle_events(self):
        actions, unmapped_events = settings.gameplay_event_mapper.get_actions(pygame.event.get())
        for action, extra in actions:
            if action == settings.ACTION_QUIT:
                self.game_state.quit_game = True
                self.pop()
            elif action == settings.ACTION_TOGGLE_DEBUG_RENDER:
                self._is_debug_render_on = not self._is_debug_render_on
            # elif hasattr(self, 'dev_use_exit'):
            #     # todo: dev stuff; delete me when no longer needed
            #     if action in (settings.ACTION_GO_EAST, settings.ACTION_GO_NORTH, settings.ACTION_GO_WEST):
            #         self.dev_use_exit(action)
            self.mouse.handle_action(action, extra)

        # todo: ? music is automgically restarting ?
        # for event in unmapped_events:
        #     if event == settings.music_ended_pygame_event:
        #         self.sfx_handler._on_music_event(None)

        # todo: delete me; keyboard navigation for early dev
        if self.game_state.move_to_room is not None:
            self.pop(effect=pyknic.pyknic_pygame.context.effects.FadeOutFadeInEffect(settings.fade_duration))

    def draw_step(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0, screen=None):
        if self.game_state.game_won or self.game_state.global_reset:
            return

        screen = screen if screen is not None else self.screen
        self.renderer.draw(screen, self.cam, (0, 0, 0), False)

        self.hud.update()  # only update when drawing
        self.hud.draw(screen)

        if self._is_debug_render_on:
            self._draw_debug()

        if do_flip:
            pygame.display.flip()

    def _draw_debug(self):
        font = pygame.font.Font(None, 15)
        names = settings._global_id_generator.names
        for ent in self.entities:
            # pygame.draw.rect(self.screen, (255, 255, 255), ent.rect, 1)
            color = (100, 100, 100) if ent.hidden else (255, 255, 255)
            pygame.draw.circle(self.screen, color, ent.position.as_xy_tuple(), 10)
            label = font.render(f"{names[ent.kind]}", True, (180, 180, 180))
            pos = ent.position + Vec3(10, 10)
            self.screen.blit(label, pos.as_xy_tuple())

        for spr in self.sprites:
            if spr.draw_special:
                continue
            color = (100, 100, 100) if not spr.visible else (255, 255, 255)
            rect = spr.image.get_rect(center=spr.position.as_xy_tuple())
            if spr.anchor == 'topleft':
                rect.topleft = rect.center
            pygame.draw.rect(self.screen, color, rect, 1)

        self.debug_hud.draw(self.screen)


logger.debug("imported")
