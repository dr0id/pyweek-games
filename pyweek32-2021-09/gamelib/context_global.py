# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'gameplay.py' is part of pw-30
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This is the game context where the actual game code is executed.

.. versionchanged:: 0.0.0.0
    initial version

NOTES FOR NEW NAVIGABLE ROOMS:

The files are:
game_state.py: the persistent room state data
settings.py: the KINDS and ACTIONS used
context_devmenu.py: launches GlobalContext; see menu item "Gumm Dev"
context_global.py: the starter context that loads new rooms when an exit is used; see resume() and load_room()
context_room.py: the base RoomContext class
context_room_lab.py: the RoomContext subclass LabContext
context_room_stevens_yard.py: the RoomContext subclass SteversYardContext
context_room_stevens_basement.py: the RoomContext subclass StevensBasementContext
context_room_gore_basement.py: the RoomContext subclass GoreGasementContext
"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2020"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["GlobalContext"]  # list of public visible parts of this module

import pygame

import pyknic.pyknic_pygame.transform
from gamelib import settings
from gamelib.context_loose import LooseContext
from gamelib.context_room import RoomContext
from pyknic.pyknic_pygame.context import TimeSteppedContext
from pyknic.pyknic_pygame.resource2 import resource_manager
from pyknic.pyknic_pygame.spritesystem import DefaultRenderer, Camera
from pyknic.mathematics import Point3 as Point
from pyknic.timing import Scheduler
from pyknic.tweening import Tweener

logger = logging.getLogger(__name__)
logger.debug("importing...")


class GlobalContext(TimeSteppedContext):

    def __init__(self, game_state, music_player):
        TimeSteppedContext.__init__(self, settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS)
        self.music_player = music_player
        self.game_state = game_state
        self.renderer = DefaultRenderer()
        screen_rect = pygame.Rect(0, 0, settings.screen_width, settings.screen_height)
        self.cam = Camera(screen_rect, padding=196 // 2)
        self.cam.set_position(Point(screen_rect.centerx, screen_rect.centery))
        self.screen = None
        self._is_debug_render_on = False
        self.tweener = Tweener()
        self.focused_sprite = None

        # world
        self.scheduler = Scheduler()

        # self.room_class_lookup = {}
        self.current_room = None

    def enter(self):
        # pygame.mouse.set_visible(False)
        # event_dispatcher.register_event_type(settings.EVT_NOISE)
        # self.screen = pygame.display.get_surface()
        self.game_state.clock.initialize(settings.initial_date_time)
        if resource_manager.resources:
            self.resume()

    def exit(self):
        # pygame.mouse.set_visible(True)
        # event_dispatcher.clear()  # todo need to clear all events that might still be in the queue and remove all listeners?
        pass

    def suspend(self):
        pass

    def resume(self):
        pygame.mouse.set_visible(True)
        if not self.screen:
            self.screen = pygame.display.get_surface()

    def load_room(self, use_transition=True):
        if self.game_state.game_won:
            return
        if self.game_state.quit_game:
            self.game_state.current_room = None

            self.game_state.move_to_room = None
            self.game_state.quit_game = False
            self.game_state._is_debug_render_on = self.current_room._is_debug_render_on
            self.current_room = None
            self.exchange(LooseContext())
        else:
            # call game_state.update_room once after each Context change
            self.game_state.update_room()
            room_name = settings._global_id_generator.names[self.game_state.current_room]
            logger.info(f'+++ Loading room {room_name}')
            self.current_room = RoomContext(self.game_state, self.music_player)
            self.current_room._is_debug_render_on = self.game_state._is_debug_render_on
            effect = None
            if use_transition:
                effect = pyknic.pyknic_pygame.context.effects.FadeOutFadeInEffect(settings.fade_duration, False)
            self.push(self.current_room, effect=effect)

    def update_step(self, delta, sim_time, *args):
        # logger.debug(f"update_step dt {delta}, sim_t {sim_time}")
        self.tweener.update(delta)
        self.scheduler.update(delta, sim_time)

        self.handle_events()

        if self.game_state.game_won:
            self.pop()
        elif self.game_state.global_reset:
            self.reset()
        self.load_room()

        # cam control
        # t = settings.cam_speed * delta
        # self.cam.lerp(self.game_logic.hero.position, t)

    def reset(self):
        logger.info("RESET at time {}", self.game_state.clock.hours_passed_since_init())
        self.game_state.reset()

    def handle_events(self):
        actions, unmapped_events = settings.gameplay_event_mapper.get_actions(pygame.event.get())
        for action, extra in actions:
            if action == settings.ACTION_QUIT:
                self.pop()

    def draw_step(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0, screen=None):
        pass

    def _draw_debug(self):
        pass


logger.debug("imported")
