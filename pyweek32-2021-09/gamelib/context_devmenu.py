# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'gameplay.py' is part of pw-30
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This is the game context where the actual game code is executed.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2020"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["GameplayContext"]  # list of public visible parts of this module

import pygame

from gamelib import settings
from gamelib.context_intro import IntroContext
from gamelib.context_loose import LooseContext
from gamelib.context_win import WinContext
from gamelib.gamerender import pygametext
from gamelib.context_gameplay import GameplayContext
from gamelib.context_global import GlobalContext
from gamelib.context_sound_toy import SoundToyContext
# from pyknic.animation import Animation
from pyknic.pyknic_pygame.context import TimeSteppedContext
from pyknic.pyknic_pygame.spritesystem import DefaultRenderer, Camera
from pyknic.mathematics import Point3 as Point
# from pyknic.mathematics import Vec3 as Vec
from pyknic.timing import Scheduler
from pyknic.tweening import Tweener

logger = logging.getLogger(__name__)
logger.debug("importing...")


class DevMenuContext(TimeSteppedContext):

    def __init__(self, game_state, music_player):
        TimeSteppedContext.__init__(self, settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS)
        self.music_player = music_player
        self.game_state = game_state
        self.renderer = DefaultRenderer()
        screen_rect = pygame.Rect(0, 0, settings.screen_width, settings.screen_height)
        self.cam = Camera(screen_rect, padding=196 // 2)
        self.cam.set_position(Point(0, 0))
        self.screen = None
        self._is_debug_render_on = False
        self.tweener = Tweener()
        self.scheduler = Scheduler()

        self.cursor_sprite = None
        self.sprites = []
        self.clear_color = pygame.Color('midnightblue')

        self.menu_data = [
            ('Intro', IntroContext),
            ('Win', WinContext),
            ('loose', LooseContext),
            ('Game', GlobalContext),
            ('Sound Toy', SoundToyContext),
            ('Graphics Toy', None),
        ]

    def enter(self):
        pygame.mouse.set_visible(False)
        # event_dispatcher.register_event_type(settings.EVT_NOISE)

        font_name = 'data/fonts/Boogaloo.ttf'
        font_size = 40
        single_height = 0
        full_height = 0
        widest = 0
        images = []

        for text, ctx in self.menu_data:
            # img = font.render(text, True, pygame.Color('yellow'))
            img = pygametext.getsurf(text, font_name, font_size,
                                     color='yellow', gcolor='orange', ocolor='black', antialias=True)
            images.append((img, ctx))
            single_height = img.get_height()
            widest = max(widest, img.get_width())
            full_height += single_height

        menu_rect = pygame.Rect(0, 0, widest, full_height)
        menu_rect.center = settings.screen_width // 2, settings.screen_height // 2
        menu_rect.top -= single_height

        y = menu_rect.top
        for img, ctx in images:
            rect = img.get_rect(centerx=settings.screen_width // 2, y=y)
            spr = pygame.sprite.Sprite()
            spr.rect = rect
            spr.image = img
            spr.launch_context = ctx
            self.sprites.append(spr)
            y += single_height

        spr = pygame.sprite.Sprite()
        spr.rect = pygame.Rect(0, 0, 20, 20)
        spr.image = pygame.Surface(spr.rect.size)
        spr.image.set_colorkey('black')
        pygame.draw.circle(spr.image, pygame.Color('red'), spr.rect.center, spr.rect.centerx)
        spr.rect.center = pygame.mouse.get_pos()
        self.cursor_sprite = spr
        self.sprites.append(spr)

    def exit(self):
        pygame.mouse.set_visible(True)
        # event_dispatcher.clear()  # todo need to clear all events that might still be in the queue and remove all listeners?

    def suspend(self):
        pass

    def resume(self):
        self.screen = pygame.display.get_surface()
        pass

    def draw_step(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0,
                  screen=None):  # fixme: AFTER_PYWEEK this dt is wrong...
        # logger.debug(f"draw dt {dt}, sim_t {sim_dt}")

        # draw to screen!
        screen = screen if screen is not None else pygame.display.get_surface()

        screen.fill(self.clear_color)
        for s in self.sprites:
            screen.blit(s.image, s.rect)

        if do_flip:
            pygame.display.flip()

    def update_step(self, delta, sim_time, *args):
        # logger.debug(f"update_step dt {delta}, sim_t {sim_time}")
        self.tweener.update(delta)
        self.scheduler.update(delta, sim_time)

        self.handle_events()

        # cam control
        t = settings.cam_speed * delta
        # self.cam.lerp(self.game_logic.hero.position, t)

    def handle_events(self):
        for e in pygame.event.get():
            if e.type == pygame.MOUSEBUTTONUP:
                self._do_mouse_button_up(e)
            elif e.type == pygame.KEYDOWN:
                self._do_key_down(e)
            elif e.type == pygame.MOUSEMOTION:
                self._do_mouse_motion(e)
            elif e.type == pygame.QUIT:
                self.pop()

    def _do_mouse_button_up(self, e):
        if e.button == 1:
            for s in self.sprites:
                if not hasattr(s, 'launch_context') or s.launch_context is None:
                    continue
                if s.rect.collidepoint(e.pos):
                    if s.launch_context:
                        ctx = s.launch_context(self.game_state, self.music_player)
                        self.push(ctx)

    def _do_mouse_motion(self, e):
        self.cursor_sprite.rect.center = e.pos

    def _do_key_down(self, e):
        if e.key == pygame.K_ESCAPE:
            self.pop()

    def _draw_debug(self):
        pass


logger.debug("imported")
