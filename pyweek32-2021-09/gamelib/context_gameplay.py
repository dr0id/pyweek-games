# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'gameplay.py' is part of pw-30
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This is the game context where the actual game code is executed.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2020"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["GameplayContext"]  # list of public visible parts of this module

import pygame

from gamelib import resources
from gamelib import settings
from gamelib.gamelogic.combination import Combiner
from gamelib.gamelogic.eventdispatcher import event_dispatcher
from pyknic.animation import Animation
from pyknic.pyknic_pygame.context import TimeSteppedContext
from pyknic.pyknic_pygame.resource2 import resource_manager
from pyknic.pyknic_pygame.spritesystem import DefaultRenderer, Camera, Sprite
from pyknic.mathematics import Point3 as Point
from pyknic.timing import Scheduler
from pyknic.tweening import Tweener

logger = logging.getLogger(__name__)
logger.debug("importing...")


class BaseSprite(Sprite):

    def on_focus(self):
        pass

    def on_focus_lost(self):
        pass


class EntitySprite(BaseSprite):

    def __init__(self, entity, image, position, anchor=None, z_layer=0, parallax_factors=None, do_init=True,
                 name='_NO_NAME_'):
        BaseSprite.__init__(self, image, position, anchor, z_layer, parallax_factors, do_init, name)
        self.entity = entity
        self.hover_sprite = None
        # border = 15
        # hover_image, center = self.get_hover_image(image, border)
        # vec = Vec(-center[0] + border, -center[1] + border) if anchor == 'topleft' else Vec(0, 0)
        # self.hover_sprite = BaseSprite(hover_image, position, vec, z_layer - 1, parallax_factors, do_init,
        #                                "hover_" + name)
        # self.hover_sprite.visible = False

    # def get_hover_image(self, image, border):
    #     if not settings.highlight_items:
    #         return image, image.get_rect().center
    #
    #     border_size = 2 * border
    #     r = image.get_rect().inflate(border_size, border_size)  # add 10 border
    #     r.move_ip(-r.x, -r.y)
    #     i = pygame.Surface(image.get_size(), pygame.SRCALPHA)
    #     mask = pygame.mask.from_surface(image, 100)
    #     i.fill((0, 0, 0, 0), r, pygame.BLEND_RGBA_MULT)
    #     step_size = 1
    #     outline = mask.outline(step_size)
    #     yellow = (255, 255, 0)
    #     pygame.draw.polygon(i, yellow, outline)
    #     factor = 0.5
    #     # i = pyknic.pyknic_pygame.transform.blur_surf(i, border * factor, border * factor, border_size)
    #     i = pyknic.pyknic_pygame.transform.box_blur(i, (21, 21), iterations=4, border=border_size)
    #     iw, ih = i.get_size()
    #     i = pygame.transform.smoothscale(i, (iw + 21, ih + 21))
    #     return i, r.center

    def on_focus(self):
        if self.hover_sprite:
            self.hover_sprite.visible = True

    def on_focus_lost(self):
        if self.hover_sprite:
            self.hover_sprite.visible = False


class AnimatedSprite(BaseSprite, Animation):

    def __init__(self, frames, fps, scheduler, position, z_layer):
        Animation.__init__(self, 0, len(frames), fps, scheduler)
        BaseSprite.__init__(self, frames[0], position, z_layer=z_layer)
        self.event_index_changed += self._idx_changed
        self.frames = frames
        self.visible = False

    def _idx_changed(self, *args):
        self.set_image(self.frames[self.current_index])


class CircleSprite(BaseSprite):

    def __init__(self, the_hero, z_layer, color):
        self.draw_special = True
        self.dirty_update = False
        BaseSprite.__init__(self, None, the_hero.position, z_layer=z_layer)
        self.color = color
        self.hero = the_hero

    def draw(self, surf, cam, renderer, interpolation_factor=1.0):
        if self.hero.energy_indicator.current_energy < 1:
            return pygame.Rect(0, 0, 0, 0)
        center = cam.world_to_screen(self.position).as_xy_tuple(int)
        return pygame.draw.circle(surf,
                                  self.color,
                                  center,
                                  int(self.hero.energy_indicator.current_energy) + 1,
                                  1)


class BaseItem(object):
    kind = None

    def __init__(self, position):
        self.position = position

    def on_click(self):
        """
        Called when a mouse click occurs on this item.
        :return: true to be grabbed by the mouse, false|None otherwise
        """
        # react the way it should
        raise NotImplementedError()

    def on_drop(self, other):
        """
        Called if the mouse holds something and drops it onto this.
        :param other: the other item that is dropped here.
        :return: true to accept the dropped item, false|None to reject it.
        """
        raise NotImplementedError()


class ItemToGrab(BaseItem):
    kind = settings.KIND_GRAB_ITEM

    def __init__(self, position):
        BaseItem.__init__(self, position)
        self.old_position = Point(0, 0)
        self.old_position.copy_values(self.position)

    def on_click(self):
        self.old_position.copy_values(self.position)
        return True

    def on_drop(self, other):
        return False

    def set_new_position(self):
        self.old_position.copy_values(self.position)


class ItemToActivate(BaseItem):

    def __init__(self, position, action):
        BaseItem.__init__(self, position)
        self.action = action

    def on_click(self):
        self.action(self)

    def on_drop(self, other):
        return False


class Inventory(BaseItem):
    """
    The inventory entity in a room. The items are actually stored in the global game state. Interacts with the events
    generated by the Mouse class to manage the items in it. Only BaseItem types can be added.

    Combinations to test:

    1. out grab, cancel
    2. out grab, drop into inventory -> add
    3. out grab, drop in (combine) -> remove, add new
    4. out grab, drop out (combine)
    5. in grab, cancel -> stay (actually: remove, add)
    6. in grab, drop into inventory -> remove, add
    7. in grab, drop in (combine) -> remove, remove, add new
    8. in grab, drop out (combine) -> remove
    """

    kind = settings.KIND_INVENTORY_ITEM

    def __init__(self, position, game_state):
        BaseItem.__init__(self, position)
        self.game_state = game_state
        self._was_in_inventory = None
        event_dispatcher.register_event_type(settings.EVT_MOUSE_DROPPED)
        event_dispatcher.register_event_type(settings.EVT_MOUSE_GRABBED)
        event_dispatcher.register_event_type(settings.EVT_MOUSE_DROP_CANCELED)
        event_dispatcher.register_event_type(settings.EVT_ITEMS_COMBINED)

        event_dispatcher.add_listener(settings.EVT_MOUSE_GRABBED, self._entity_grabbed)
        event_dispatcher.add_listener(settings.EVT_MOUSE_DROPPED, self._entity_dropped)
        event_dispatcher.add_listener(settings.EVT_MOUSE_DROP_CANCELED, self._entity_drop_canceled)
        event_dispatcher.add_listener(settings.EVT_ITEMS_COMBINED, self._entities_combined)

    def _entities_combined(self, item1, item2, combination):
        if self.game_state.inventory_contains(item1) or self.game_state.inventory_contains(item2):
            if isinstance(combination, BaseItem):
                self.game_state.remove_from_inventory(item1)
                self.game_state.remove_from_inventory(item2)
                self.game_state.add_to_inventory(combination)
            else:
                logger.warning("combined entity not added to inventory because its not a BaseEntity: %s", combination)

    def _entity_drop_canceled(self, entity, position):
        if self._was_in_inventory == entity:
            self.game_state.add_to_inventory(entity)
            self._was_in_inventory = None

    def _entity_dropped(self, dropped_entity, target_entity, position):
        if target_entity == self:
            self.game_state.add_to_inventory(dropped_entity)
        else:
            self._was_in_inventory = None
            self.game_state.remove_from_inventory(dropped_entity)

    def _entity_grabbed(self, entity):
        if self.game_state.remove_from_inventory(entity):
            self._was_in_inventory = entity

    def on_click(self):
        return False

    def on_drop(self, other):
        if isinstance(other, BaseItem):
            return True


class ItemMix(ItemToGrab):
    kind = settings.KIND_MIX_ITEM

    def __init__(self, position, world):
        ItemToGrab.__init__(self, position)
        self.world = world

    def on_click(self):
        return ItemToGrab.on_click(self)

    def on_drop(self, other):
        return False


# hover
# hover with grabbed
# +grab
# +drop
# magnify
# de-magnify
# +activate

class Mouse(object):

    def __init__(self, combiner, renderer, cam):
        self.combiner = combiner
        self.grabbed_item = None
        self.renderer = renderer
        self.cam = cam
        self.focused_sprite = None

    def right_click(self):
        if self.grabbed_item:
            self.grabbed_item.position.copy_values(self.grabbed_item.old_position)
            event_dispatcher.fire(settings.EVT_MOUSE_DROP_CANCELED, self.grabbed_item,
                                  self.grabbed_item.position.clone())
            self.grabbed_item = None

    def click(self, entity):
        if not entity:
            return

        if self.grabbed_item:
            new = self.combiner.combine(entity, self.grabbed_item)
            if new:
                event_dispatcher.fire(settings.EVT_MOUSE_DROPPED, self.grabbed_item, entity,
                                      self.grabbed_item.position.clone())
                event_dispatcher.fire(settings.EVT_ITEMS_COMBINED, self.grabbed_item, entity, new)
                self.grabbed_item = None
            else:
                accept = entity.on_drop(self.grabbed_item)
                if accept:
                    self.grabbed_item.set_new_position()
                    event_dispatcher.fire(settings.EVT_MOUSE_DROPPED, self.grabbed_item, entity,
                                          self.grabbed_item.position.clone())
                    self.grabbed_item = None
        else:
            do_grab = entity.on_click()
            if do_grab:
                self.grabbed_item = entity
                event_dispatcher.fire(settings.EVT_MOUSE_GRABBED, entity)

    def on_motion(self, world_pos):
        if self.grabbed_item:
            self.grabbed_item.position.copy_values(world_pos)

    def handle_action(self, action, extra):
        if action == settings.ACTION_RMB:
            self.right_click()
        elif action == settings.ACTION_LMB:
            pos = extra
            sprites = self.renderer.get_sprites_at_tuple(pos)
            entity = None
            if sprites:
                for spr in sprites:
                    if isinstance(spr, EntitySprite) and spr.entity != self.grabbed_item:
                        entity = spr.entity
                        break
            self.click(entity)
        elif action == settings.ACTION_MOUSE_MOTION:
            # do hover highlight on the sprite?
            pos, rel, buttons = extra
            world_pos = self.cam.screen_to_world(Point(*pos))
            self.on_motion(world_pos)
            sprites = self.renderer.get_sprites_at_tuple(pos)
            hover_sprites = [spr for spr in sprites if
                             isinstance(spr, EntitySprite) and spr.entity != self.grabbed_item]
            if hover_sprites:
                to_hover = hover_sprites[0]  # just take the first
                if self.focused_sprite != to_hover:
                    if self.focused_sprite:
                        self.focused_sprite.on_focus_lost()
                    self.focused_sprite = to_hover
                    self.focused_sprite.on_focus()
            else:
                if self.focused_sprite:
                    self.focused_sprite.on_focus_lost()
                    self.focused_sprite = None


class GameplayContext(TimeSteppedContext):

    def __init__(self, game_state):
        TimeSteppedContext.__init__(self, settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS)
        self.game_state = game_state
        self.renderer = DefaultRenderer()
        screen_rect = pygame.Rect(0, 0, settings.screen_width, settings.screen_height)
        self.cam = Camera(screen_rect, padding=196 // 2)
        self.cam.set_position(Point(screen_rect.centerx, screen_rect.centery))
        self.screen = None
        self._is_debug_render_on = False
        self.tweener = Tweener()
        self.focused_sprite = None

        # world
        self.scheduler = Scheduler()
        self.combiner = Combiner()
        self.combiner.register_pair(ItemToGrab.kind, ItemMix.kind, self._combine_grab_mix)
        self.mouse = Mouse(self.combiner, self.renderer, self.cam)
        self.entities = []
        # event_dispatcher.register_event_type(settings.EVT_MOUSE_DROPPED)
        # event_dispatcher.register_event_type(settings.EVT_MOUSE_GRABBED)
        # event_dispatcher.register_event_type(settings.EVT_MOUSE_DROP_CANCELED)

    def _combine_grab_mix(self, grab_item, mix_item):
        self.remove_entity(grab_item)
        self.remove_entity(mix_item)
        return self.add_grab_item(mix_item.position.clone(), resources.resource_item5)

    def remove_entity(self, entity):
        if entity in self.entities:
            self.entities.remove(entity)
            sprites = [spr for spr in self.renderer.get_sprites() if
                       isinstance(spr, EntitySprite) and spr.entity == entity]
            self.renderer.remove_sprites(sprites)

    # def add_entity(self, entity):
    #     self.entities.append(entity)
    #
    #     img = resource_manager.resources[entity.resource_id].image
    #     spr = EntitySprite(entity, img, entity.position)
    #     self.renderer.add_sprite(spr)

    def enter(self):
        # pygame.mouse.set_visible(False)
        # event_dispatcher.register_event_type(settings.EVT_NOISE)
        # self.screen = pygame.display.get_surface()
        if resource_manager.resources:
            self.resume()

    def exit(self):
        # pygame.mouse.set_visible(True)
        # event_dispatcher.clear()  # todo need to clear all events that might still be in the queue and remove all listeners?
        pass

    def suspend(self):
        pass

    def resume(self):
        pygame.mouse.set_visible(True)
        if not self.screen:
            self.screen = pygame.display.get_surface()

            self.add_grab_item(Point(50, 50), resources.resource_item1)
            self.add_grab_item(Point(150, 50), resources.resource_item1)
            self.add_grab_item(Point(50, 150), resources.resource_item1)

            inventory = Inventory(Point(6, settings.screen_height - 248 - 6), self.game_state)
            self.entities.append(inventory)
            inventory_img = resource_manager.resources[resources.resource_item_inventory].image
            spr2 = EntitySprite(inventory, inventory_img, inventory.position, anchor='topleft')
            self.renderer.add_sprite(spr2)

            item_to_activate = ItemToActivate(Point(833, 135), lambda i: logger.warning(f"activation of item {i}"))
            self.entities.append(item_to_activate)
            item_img = resource_manager.resources[resources.resource_item3].image
            spr3 = EntitySprite(item_to_activate, item_img, item_to_activate.position, anchor='topleft')
            self.renderer.add_sprite(spr3)

            self.add_mix_item(Point(100, 300))
            self.add_mix_item(Point(200, 400))
            self.add_mix_item(Point(500, 500))
            self.add_mix_item(Point(700, 400))

            background_img = resource_manager.resources[resources.resource_background1].image
            background_spr = BaseSprite(background_img, Point(0, 0), anchor='topleft', z_layer=-100)
            self.renderer.add_sprite(background_spr)

    def add_mix_item(self, position, z_layer=100):
        item_mix = ItemMix(position, self)
        self.entities.append(item_mix)
        mix_img = resource_manager.resources[resources.resource_item_mix].image
        mix_sprite = EntitySprite(item_mix, mix_img, item_mix.position, z_layer=z_layer)
        # i, v = resource_manager.resources[resources.resource_item_mix].get_hover_image('center')
        # mix_sprite.hover_sprite = EntitySprite(item_mix, i, item_mix.position, anchor=v, z_layer=z_layer - 1)
        # mix_sprite.hover_sprite.visible = False
        self.renderer.add_sprite(mix_sprite)

    def add_grab_item(self, position, resource_id, z_layer=100):
        entity = ItemToGrab(position)
        self.entities.append(entity)
        resource = resource_manager.resources[resource_id]
        img = resource.image
        spr1 = EntitySprite(entity, img, entity.position, z_layer=z_layer)
        self.renderer.add_sprite(spr1)
        # hover
        if hasattr(resource, "get_hover_image"):
            h_img, h_anchor = resource.get_hover_image('center')
            spr1.hover_sprite = BaseSprite(h_img, entity.position, anchor=h_anchor, z_layer=z_layer - 1)
            spr1.hover_sprite.visible = False

        return entity

    def draw_step(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0,
                  screen=None):  # fixme: AFTER_PYWEEK this dt is wrong...
        # logger.debug(f"draw dt {dt}, sim_t {sim_dt}")

        # draw to screen!
        screen = screen if screen is not None else self.screen
        self.renderer.draw(screen, self.cam, (0, 0, 0), False)

        if self._is_debug_render_on:
            self._draw_debug()

        # self.hud.draw(self.screen)

        if do_flip:
            pygame.display.flip()

    def update_step(self, delta, sim_time, *args):
        # logger.debug(f"update_step dt {delta}, sim_t {sim_time}")
        self.tweener.update(delta)
        self.scheduler.update(delta, sim_time)

        self.handle_events()

        # cam control
        # t = settings.cam_speed * delta
        # self.cam.lerp(self.game_logic.hero.position, t)

    def handle_events(self):
        actions, unmapped_events = settings.gameplay_event_mapper.get_actions(pygame.event.get())
        for action, extra in actions:
            if action == settings.ACTION_QUIT:
                self.pop()
            elif action == settings.ACTION_TOGGLE_DEBUG_RENDER:
                self._is_debug_render_on = not self._is_debug_render_on
            self.mouse.handle_action(action, extra)

    def _draw_debug(self):
        font = pygame.font.Font(None, 15)
        for ent in self.entities:
            # pygame.draw.rect(self.screen, (255, 255, 255), ent.rect, 1)
            pygame.draw.circle(self.screen, (255, 255, 255), ent.position.as_xy_tuple(), 10)
            label = font.render(f"{ent}", True, (180, 180, 180))
            self.screen.blit(label, ent.position.as_xy_tuple())


logger.debug("imported")
