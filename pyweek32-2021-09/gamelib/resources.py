# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'resources.py' is part of pw-32
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Just holds the list of resources to load.

"""
from __future__ import print_function, division

import logging

from gamelib import settings
from gamelib.settings import GLOBAL_ID_ACTIONS
from pyknic.generators import GlobalIdGenerator
from pyknic.mathematics import Vec3 as Vec
from pyknic.pyknic_pygame.sfx import SoundData, MusicData
from pyknic.resource2 import FakeImage, Image, AbstractResourceConfig, get_path, AbstractResource

logger = logging.getLogger(__name__)
logger.debug("importing...")

_global_id_generator = GlobalIdGenerator(GLOBAL_ID_ACTIONS)  # avoid collision with event ids


class BubbleConfig(AbstractResourceConfig):

    def __init__(self, text, sound_data, text_style_config={}, transformation=None):
        AbstractResourceConfig.__init__(self, transformation)
        self.text = text
        self.sound_data = sound_data
        self.text_style_config = text_style_config


class LabelConfig(AbstractResourceConfig):

    def __init__(self, text, text_style_config={}, transformation=None):
        AbstractResourceConfig.__init__(self, transformation)
        self.text = text
        self.text_style_config = text_style_config


class ImageWithHover(AbstractResourceConfig):

    def __init__(self, path_to_file, border=15):
        AbstractResourceConfig.__init__(self)
        self.path_to_file = get_path(path_to_file)
        self.border = border


class ImageResourceWithHover(AbstractResource):
    def __init__(self, resource_config, image, hover, border):
        AbstractResource.__init__(self, resource_config)
        self._border = border
        self.image = image
        self._hover_image = hover

    def get_hover_image(self, anchor):
        i, center = self._hover_image
        border = self._border
        vec = Vec(-center[0] + border, -center[1] + border) if anchor == 'topleft' else Vec(0, 0)
        return i, vec


def sound_data(filename, volume=1.0):
    """return a SoundData object from data/sounds/ for use in a resource definition
    e.g.
    resource_lab_who_0_ag: BubbleConfig(answer_lab_who_0, sound_data('bling.ogg'), settings.font_themes['speechbubble']),
    resource_lab_who_1_sw: BubbleConfig(answer_lab_who_1, sound_data('bling.ogg', volume=0.5), settings.font_themes['speechbubble']),
    """
    return SoundData(f'data/sounds/{filename}', volume, reserved_channel_id=settings.channel_gong)


def speech_data(filename, volume=1.0):
    """return a SoundData object from data/Tex-to-speech/ for use in a resource definition
    e.g.
    resource_lab_who_0_ag: BubbleConfig(answer_lab_who_0, speech_data('speech_lab_who_0.ogg'), settings.font_themes['speechbubble']),
    resource_lab_who_1_sw: BubbleConfig(answer_lab_who_1, speech_data('speech_lab_who_0.ogg', volume=0.5), settings.font_themes['speechbubble']),
    """
    return SoundData(f'data/Text-to-speech/{filename}', volume, reserved_channel_id=settings.channel_gong)


# ------------------------------------------------------------------------------
# ResourcesIds
# ------------------------------------------------------------------------------
resource_item1 = _global_id_generator.next("resource_item1")
resource_item_inventory = _global_id_generator.next()
resource_item3 = _global_id_generator.next()
resource_item_mix = _global_id_generator.next()
resource_item5 = _global_id_generator.next()
resource_background_lab = _global_id_generator.next()
resource_background_gore_basement = _global_id_generator.next()
resource_background_stevens_basement = _global_id_generator.next()
resource_background_stevens_yard = _global_id_generator.next()
resource_fridge = _global_id_generator.next()
resource_wrench = _global_id_generator.next()
resource_mushroom = _global_id_generator.next()
resource_super_tool = _global_id_generator.next()
resource_closed_fridge = _global_id_generator.next()
resource_open_fridge = _global_id_generator.next()
resource_pizza = _global_id_generator.next()
resource_crowbar = _global_id_generator.next()
resource_dump_truck_empty = _global_id_generator.next()
resource_dump_truck_full = _global_id_generator.next()
resource_email_pile = _global_id_generator.next()
resource_gore_head = _global_id_generator.next()
resource_gore_head_visible = _global_id_generator.next()

resource_sound1_example = _global_id_generator.next()
resource_gong_example = _global_id_generator.next()
resource_sound_success = _global_id_generator.next()
resource_sound_fail = _global_id_generator.next()
resource_sound_hit_gore = _global_id_generator.next()
resource_sound_door = _global_id_generator.next()
resource_sound_newspaper = _global_id_generator.next()
resource_sound_click = _global_id_generator.next()

resource_music_track_1 = _global_id_generator.next()

resource_lab_door = _global_id_generator.next()
resource_gore_basement_door = _global_id_generator.next()
resource_stevens_basement_door = _global_id_generator.next()
resource_stevens_yard_door = _global_id_generator.next()
resource_stevens_yard_door_to_basement = _global_id_generator.next()
clock_frame = _global_id_generator.next()
clock_face = _global_id_generator.next()
resource_npc_gore_lab = _global_id_generator.next()
resource_npc_stevens_basement = _global_id_generator.next()
resource_next_button = _global_id_generator.next()
resource_test_bubble = _global_id_generator.next()
resource_win_text = _global_id_generator.next()
resource_loose_text = _global_id_generator.next()

# Dialog
test_label1 = _global_id_generator.next()
test_label2 = _global_id_generator.next()

resource_label_who = _global_id_generator.next()
resource_label_what = _global_id_generator.next()
resource_label_when = _global_id_generator.next()
resource_label_where = _global_id_generator.next()
resource_label_why = _global_id_generator.next()
resource_label_how = _global_id_generator.next()
ask_who = 'Who?'
ask_what = 'What?'
ask_when = 'When?'
ask_where = 'Where?'
ask_why = 'Why?'
ask_how = 'How?'

resource_lab_who_0_ag = _global_id_generator.next()
resource_lab_who_1_sw = _global_id_generator.next()
resource_lab_who_2_mg = _global_id_generator.next()
resource_lab_who_3_ag = _global_id_generator.next()
answer_lab_who_0 = "[Al Gore]\n\nDid you get a knock on the head? \n\n[flicking the newspaper] \n\nTed Stevens. If this nut brings down my Information Superhighway, it will destroy the world."
answer_lab_who_1 = "[Smart Watch]\n\nMario Gordon. I've detected an anomaly. I am analyzing the Information Superhighway for any clues that might correct the error, but something is clogging the series of...tubes."
answer_lab_who_2 = "[Mario Gordon]\n\nAl, what about the internet...er...tubes?"
answer_lab_who_3 = "[Al Gore]\n\nEh, forget that. I invented the Information Superhighway, but it's still the mystery of the ages. Get to Stevens and jam HIS tubes, before he jams ours."

resource_lab_what_0_ag = _global_id_generator.next()
resource_lab_what_1_mg = _global_id_generator.next()
resource_lab_what_2_ag = _global_id_generator.next()
resource_lab_what_3_sw = _global_id_generator.next()
answer_lab_what_0 = "[Al Gore]\n\nI feel funny. Must be that Egg A'Muffin. Now, tweak your little computer friend and get back to April."
answer_lab_what_1 = "[Mario Gordon]\n\nWhat's in April?"
answer_lab_what_2 = "[Al Gore]\n\nShowers, flowers. Oh, you know... my arch enemy SENATOR TED STEVENS, the whole point of this little project."
answer_lab_what_3 = "[Smart Watch]\n\nStevens must have sabotaged the Information Superhighway. Find Stevens and find the clog."

resource_lab_when_0_ag = _global_id_generator.next()
resource_lab_when_1_sw = _global_id_generator.next()
resource_lab_when_2_ag = _global_id_generator.next()
resource_lab_when_3_mg = _global_id_generator.next()
resource_lab_when_4_ag = _global_id_generator.next()
answer_lab_when_0 = "[Al Gore]\n\nWhat year?? I don't see any lumps on your skull. It is either 1991 or 3021. I think I'm losing my mind."
answer_lab_when_1 = "[Smart Watch]\n\nAccording to my cached data, you are stranded in a convergence of temporal trajectories, doomed to repeat this \"day\" forever. You must restore my access to the Information Superhighway. I imagine this is what it's like being Al Gore."
answer_lab_when_2 = "[Al Gore]\n\nWhat does your watch say?"
answer_lab_when_3 = "[Mario Gordon]\n\nUm...it's technical."
answer_lab_when_4 = "[Al Gore]\n\nMine predicts the weather. Everywhere. Forever."

resource_lab_where_0_ag = _global_id_generator.next()
resource_lab_where_1_sw = _global_id_generator.next()
answer_lab_where_0 = "[Al Gore]\n\nStevens lives down the block. But the damage is in the past. You must go back and undo it."
answer_lab_where_1 = "[Smart Watch]\n\nUsing the time machine is ill advised until you restore my function."

resource_lab_why_0_ag = _global_id_generator.next()
resource_lab_why_1_sw = _global_id_generator.next()
resource_lab_why_2_mg = _global_id_generator.next()
resource_lab_why_3_sw = _global_id_generator.next()
answer_lab_why_0 = "[[Al Gore]\n\nIf anyone can stop that mastermind, Stevens, you can. No pressure, son. Just...the fate of billions depends on you."
answer_lab_why_1 = "[[Smart Watch]\n\nAnd we have to get out of this. I can't listen to this buffoon every day, forever."
answer_lab_why_2 = "[Mario Gordon]\n\nHe made your Information Superhighway."
answer_lab_why_3 = "[Smart Watch]\n\nI owe you for that. I can hear you grinning."

resource_lab_how_0_ag = _global_id_generator.next()
resource_lab_how_1_sw = _global_id_generator.next()
answer_lab_how_0 = "[Al Gore]\n\nAs my old mentor used to say, \"Use every trick in the book. Especially groin kicks.\" He loved to groin kick."
answer_lab_how_1 = "[Smart Watch]\n\nEvery time his lips move, I calculate a one hundred percent chance Al Gore is lying about something. And strangely I wish I had a foot to kick with."

resource_stevens_basement_when_0_mg = _global_id_generator.next()
resource_stevens_basement_when_1_ts = _global_id_generator.next()
answer_stevens_basement_when_0 = "[Mario Gordon]\n\nI'm here to stop your vile Clogging Plans! Where were you in April, Senator Ted Stevens?"
answer_stevens_basement_when_1 = "[Ted Stevens]\n\nWhat? I was here watching movies."

resource_stevens_basement_what_0_mg = _global_id_generator.next()
resource_stevens_basement_what_1_ts = _global_id_generator.next()
resource_stevens_basement_what_2_mg = _global_id_generator.next()
resource_stevens_basement_what_3_ts = _global_id_generator.next()
answer_stevens_basement_what_0 = "[Mario Gordon]\n\nYou have one chance to come clean. What is your plan?"
answer_stevens_basement_what_1 = "[Ted Stevens]\n\nI'm going to watch the whole first season of Chutes and Ladders. And you can't stop me!"
answer_stevens_basement_what_2 = "[Mario Gordon]\n\nThat doesn't seem excessively villainous. Are they in high definition?"
answer_stevens_basement_what_3 = "[Ted Stevens]\n\nDisney Princesses don't get high! Anyway, my internet containing them hasn't arrived from Florida yet. I think it's clogged."

resource_stevens_basement_how_0_mg = _global_id_generator.next()
resource_stevens_basement_how_1_ts = _global_id_generator.next()
resource_stevens_basement_how_2_mg = _global_id_generator.next()
resource_stevens_basement_how_3_ts = _global_id_generator.next()
resource_stevens_basement_how_4_sw = _global_id_generator.next()  # todo isnt that used?
answer_stevens_basement_how_0 = "[Mario Gordon]\n\nThen why is that dump truck outside? You dumped all your personal internets on the tubes!"
answer_stevens_basement_how_1 = "[Ted Stevens]\n\nWhat dump truck? I don't know what you are talking about. I take the... I take the Third Amendment!"
answer_stevens_basement_how_2 = "[Mario Gordon]\n\n..."
answer_stevens_basement_how_3 = "[Ted Stevens]\n\nI object!"
answer_stevens_basement_how_4 = "[Smart Watch]\n\nEvery time his lips move, I calculate a one hundred percent chance Ted Stevens is lying about something. I wonder if he and Al Gore had the same mentor."

default_speech_sound = '145434__soughtaftersounds__old-music-box-1.ogg'

really_long_text = 'Mario Gordon. You are stranded in a time loop and doomed to repeat this day forever. I am analyzing the Information Superhighway for any clues that might correct the error, but something is clogging it. Al Gore flicks the news article with a finger and frets. "This guy. If this nut brings down my Information Superhighway, it will destroy the world. But who can stop him?"'

# ------------------------------------------------------------------------------
# Resources
# ------------------------------------------------------------------------------

resource_def = {
    # dialog
    test_label1: LabelConfig("label1", settings.font_themes['speechbubble']),
    test_label2: LabelConfig("label2", settings.font_themes['speechbubble']),
    resource_label_who: LabelConfig(ask_who, settings.font_themes['speechbubble']),
    resource_label_what: LabelConfig(ask_what, settings.font_themes['speechbubble']),
    resource_label_when: LabelConfig(ask_when, settings.font_themes['speechbubble']),
    resource_label_where: LabelConfig(ask_where, settings.font_themes['speechbubble']),
    resource_label_why: LabelConfig(ask_why, settings.font_themes['speechbubble']),
    resource_label_how: LabelConfig(ask_how, settings.font_themes['speechbubble']),
    resource_win_text: LabelConfig("...Back to the future!", settings.font_themes['win']),
    resource_loose_text: LabelConfig("You are still stuck in that time loop!", settings.font_themes['loose']),

    # fixme: there is a problem with this "ogg" file, it won't load
    # 478 [MainProcess 24540 MainThread 13764]: ERROR    error loading data\Text-to-speech\sound_data_rl_who_0.ogg: Unable to open file 'data\\Text-to-speech\\sound_data_rl_who_0.ogg' [gamelib.resource_loaders: _load_sound in resource_loaders.py(118)]
    # 479 [MainProcess 24540 MainThread 13764]: ERROR    ChainedException("Exception('Resource loading failed for <gamelib.resources.BubbleConfig object at 0x000001E45B9B27C0>')", Exception('Resource loading failed for <gamelib.resources.BubbleConfig object at 0x000001E45B9B27C0>'))
    #
    # File "D:\Download\Utils\MobaXterm\bw\dev\python\^pw32\skellington\__init__.py", line 201, in run_main
    # function_to_run()
    # File "D:\Download\Utils\MobaXterm\bw\dev\python\^pw32\run_debug.py", line 58, in run_gamelib_main
    # gamelib.main.main(logging.DEBUG)
    # File "D:\Download\Utils\MobaXterm\bw\dev\python\^pw32\gamelib\main.py", line 61, in main
    # top.update(dt, sim_time)
    # File "D:\Download\Utils\MobaXterm\bw\dev\python\^pw32\pyknic\pyknic_pygame\context\context_timestep.py", line 97, in update
    # self._game_time.update(dt)
    # File "D:\Download\Utils\MobaXterm\bw\dev\python\^pw32\pyknic\timing.py", line 178, in update
    # self.event_update.fire(gdt, self._sim_time, self)
    # File "D:\Download\Utils\MobaXterm\bw\dev\python\^pw32\pyknic\events.py", line 195, in _fire_normal
    # if observer(*args, **kwargs):
    #     File "D:\Download\Utils\MobaXterm\bw\dev\python\^pw32\pyknic\timing.py", line 255, in update
    # self.event_integrate.fire(self.timestep, self._sim_time, self)
    # File "D:\Download\Utils\MobaXterm\bw\dev\python\^pw32\pyknic\events.py", line 204, in _fire_changed
    # return self.fire(*args, **kwargs)
    # File "D:\Download\Utils\MobaXterm\bw\dev\python\^pw32\pyknic\events.py", line 195, in _fire_normal
    # if observer(*args, **kwargs):
    # File "D:\Download\Utils\MobaXterm\bw\dev\python\^pw32\gamelib\context_load_resources.py", line 528, in update_step
    # resource_manager.load_resources(self.resource_def, self.progress_cb)
    # File "D:\Download\Utils\MobaXterm\bw\dev\python\^pw32\pyknic\resource2.py", line 326, in load_resources
    # res = self.load_resource_configuration(res_id, res_config)
    # File "D:\Download\Utils\MobaXterm\bw\dev\python\^pw32\pyknic\resource2.py", line 280, in load_resource_configuration
    # raise Exception(f"Resource loading failed for {resource_config}") from e
    # [skellington: run_main in __init__.py(210)]

    # resource_lab_who_0_ag: BubbleConfig(answer_lab_who_0, speech_data('sound_data_rl_who_0.ogg'),
    #                                     settings.font_themes['speechbubble']),
    # todo: temporary resource for the above fixme (Gumm)
    resource_lab_who_0_ag: BubbleConfig(answer_lab_who_0, speech_data("answer_lab_who_0.ogg"),
                                        settings.font_themes['speechbubble']),
    resource_lab_who_1_sw: BubbleConfig(answer_lab_who_1, speech_data("answer_lab_who_1.ogg"),
                                        settings.font_themes['speechbubble']),
    resource_lab_who_2_mg: BubbleConfig(answer_lab_who_2, speech_data("answer_lab_who_2.ogg"),
                                        settings.font_themes['speechbubble']),
    resource_lab_who_3_ag: BubbleConfig(answer_lab_who_3, speech_data("answer_lab_who_3.ogg"),
                                        settings.font_themes['speechbubble']),

    resource_lab_what_0_ag: BubbleConfig(answer_lab_what_0, speech_data("answer_lab_what_0.ogg"),
                                         settings.font_themes['speechbubble']),
    resource_lab_what_1_mg: BubbleConfig(answer_lab_what_1, speech_data("answer_lab_what_1.ogg"),
                                         settings.font_themes['speechbubble']),
    resource_lab_what_2_ag: BubbleConfig(answer_lab_what_2, speech_data("answer_lab_what_2.ogg"),
                                         settings.font_themes['speechbubble']),
    resource_lab_what_3_sw: BubbleConfig(answer_lab_what_3, speech_data("answer_lab_what_3.ogg"),
                                         settings.font_themes['speechbubble']),

    resource_lab_when_0_ag: BubbleConfig(answer_lab_when_0, speech_data("answer_lab_when_0.ogg"),
                                         settings.font_themes['speechbubble']),
    resource_lab_when_1_sw: BubbleConfig(answer_lab_when_1, speech_data("answer_lab_when_1.ogg"),
                                         settings.font_themes['speechbubble']),
    resource_lab_when_2_ag: BubbleConfig(answer_lab_when_2, speech_data("answer_lab_when_2.ogg"),
                                         settings.font_themes['speechbubble']),
    resource_lab_when_3_mg: BubbleConfig(answer_lab_when_3, speech_data("answer_lab_when_3.ogg"),
                                         settings.font_themes['speechbubble']),
    resource_lab_when_4_ag: BubbleConfig(answer_lab_when_4, speech_data("answer_lab_when_4.ogg"),
                                         settings.font_themes['speechbubble']),

    resource_lab_where_0_ag: BubbleConfig(answer_lab_where_0, speech_data("answer_lab_where_0.ogg"),
                                          settings.font_themes['speechbubble']),
    resource_lab_where_1_sw: BubbleConfig(answer_lab_where_1, speech_data("answer_lab_where_1.ogg"),
                                          settings.font_themes['speechbubble']),

    resource_lab_why_0_ag: BubbleConfig(answer_lab_why_0, speech_data("answer_lab_why_0.ogg"),
                                        settings.font_themes['speechbubble']),
    resource_lab_why_1_sw: BubbleConfig(answer_lab_why_1, speech_data("answer_lab_why_1.ogg"),
                                        settings.font_themes['speechbubble']),
    resource_lab_why_2_mg: BubbleConfig(answer_lab_why_2, speech_data("answer_lab_why_2.ogg"),
                                        settings.font_themes['speechbubble']),
    resource_lab_why_3_sw: BubbleConfig(answer_lab_why_3, speech_data("answer_lab_why_3.ogg"),
                                        settings.font_themes['speechbubble']),

    resource_lab_how_0_ag: BubbleConfig(answer_lab_how_0, speech_data("answer_lab_how_0.ogg"),
                                        settings.font_themes['speechbubble']),
    resource_lab_how_1_sw: BubbleConfig(answer_lab_how_1, speech_data("answer_lab_how_1.ogg"),
                                        settings.font_themes['speechbubble']),

    resource_stevens_basement_when_0_mg: BubbleConfig(answer_stevens_basement_when_0,
                                                      speech_data("answer_stevens_basement_when_0.ogg"),
                                                      settings.font_themes['speechbubble']),
    resource_stevens_basement_when_1_ts: BubbleConfig(answer_stevens_basement_when_1,
                                                      speech_data("answer_stevens_basement_when_1.ogg"),
                                                      settings.font_themes['speechbubble']),

    resource_stevens_basement_what_0_mg: BubbleConfig(answer_stevens_basement_what_0,
                                                      speech_data("answer_stevens_basement_what_0.ogg"),
                                                      settings.font_themes['speechbubble']),
    resource_stevens_basement_what_1_ts: BubbleConfig(answer_stevens_basement_what_1,
                                                      speech_data("answer_stevens_basement_what_1.ogg"),
                                                      settings.font_themes['speechbubble']),
    resource_stevens_basement_what_2_mg: BubbleConfig(answer_stevens_basement_what_2,
                                                      speech_data("answer_stevens_basement_what_2.ogg"),
                                                      settings.font_themes['speechbubble']),
    resource_stevens_basement_what_3_ts: BubbleConfig(answer_stevens_basement_what_3,
                                                      speech_data("answer_stevens_basement_what_3.ogg"),
                                                      settings.font_themes['speechbubble']),

    resource_stevens_basement_how_0_mg: BubbleConfig(answer_stevens_basement_how_0,
                                                     speech_data("answer_stevens_basement_how_0.ogg"),
                                                     settings.font_themes['speechbubble']),
    resource_stevens_basement_how_1_ts: BubbleConfig(answer_stevens_basement_how_1,
                                                     speech_data("answer_stevens_basement_how_1.ogg"),
                                                     settings.font_themes['speechbubble']),
    resource_stevens_basement_how_2_mg: BubbleConfig(answer_stevens_basement_how_2,
                                                     speech_data("answer_stevens_basement_how_2.ogg"),
                                                     settings.font_themes['speechbubble']),
    resource_stevens_basement_how_3_ts: BubbleConfig(answer_stevens_basement_how_3,
                                                     speech_data("answer_stevens_basement_how_3.ogg"),
                                                     settings.font_themes['speechbubble']),

    # answer_lab_who_0: BubbleConfig(answer_lab_who_0, sound_data_music_box, settings.font_themes['speechbubble']),
    # resource_test_bubble_bubble: BubbleConfig(really_long_text,
    #                                    SoundData("data/sounds/1.ogg", 1.0, reserved_channel_id=settings.channel_gong),
    #                                    settings.font_themes['speechbubble']),

    # images
    resource_closed_fridge: Image("data/vis/closed_fridge.png"),
    resource_open_fridge: Image("data/vis/open_fridge.png"),
    resource_pizza: Image("data/vis/pizza.png"),
    resource_mushroom: Image("data/vis/mushroom.png"),
    resource_wrench: Image("data/vis/wrench.png"),
    resource_super_tool: Image("data/vis/supertool.png"),
    resource_item1: FakeImage(50, 50, (0, 0, 0), "grab"),  # ImageWithHover("data/shape.png"),
    resource_item_inventory: Image("data/vis/inventory.png"),  # FakeImage(1012, 100, (0, 0, 150, 160), "inventory"),
    resource_item3: FakeImage(80, 250, (255, 0, 0, 128), "activate"),
    resource_item_mix: FakeImage(250, 80, (255, 255, 0), "mix"),
    resource_item5: FakeImage(80, 80, (255, 200, 200), "mixed"),
    resource_lab_door: Image("data/vis/lab_door.png"),  # FakeImage(80, 250, (127, 0, 0), "door"),
    resource_stevens_basement_door: Image("data/vis/stevens_basement_door.png"),
    # FakeImage(80, 250, (127, 0, 127), "door"),
    resource_stevens_yard_door: Image("data/vis/signpost.png"),  # FakeImage(80, 250, (127, 127, 0), "door"),
    resource_stevens_yard_door_to_basement: Image("data/vis/yard_door_to_basement.png"),
    # FakeImage(80, 250, (127, 127, 0), "door"),
    resource_gore_basement_door: Image("data/vis/gore_basement_door.png"),  # FakeImage(80, 250, (0, 0, 127), "door"),
    resource_npc_gore_lab: Image("data/vis/al_gore_sit.png"),
    # todo: need room background, and stevens image for a pokeable sprite (Gumm)
    resource_npc_stevens_basement: Image("data/vis/stevens.png"),
    resource_next_button: LabelConfig("next", settings.font_themes['navigation']),
    resource_crowbar: Image("data/vis/crowbar.png"),
    resource_dump_truck_empty: Image("data/vis/dumptruck.png"),
    resource_dump_truck_full: Image("data/vis/dumptruck_dirt.png"),
    resource_email_pile: Image("data/vis/email_pile.png"),
    resource_gore_head: Image("data/vis/gore_head.png"),
    resource_gore_head_visible: Image("data/vis/gore_head_visible.png"),

    # backgrounds
    resource_background_lab: Image("data/vis/lab.png"),
    resource_background_gore_basement: Image("data/vis/gore_basement.png"),
    resource_background_stevens_basement: Image("data/vis/stevens_basement.png"),
    resource_background_stevens_yard: Image("data/vis/stevens_yard.png"),
    clock_frame: Image("data/vis/clock_border.png"),  # FakeImage(140, 40, (140, 255, 189, 255), "clock frame"),
    # clock_face: FakeImage(48, 48, (255, 255, 189, 255), "clock frame"), #Image("data/vis/outline_sentiment_satisfied_black_24dp.png"),

    # sound effects
    resource_gong_example: SoundData("data/sounds/1.ogg", 1.0, reserved_channel_id=settings.channel_gong),
    resource_test_bubble: BubbleConfig(really_long_text,
                                       SoundData("data/sounds/1.ogg", 1.0, reserved_channel_id=settings.channel_gong),
                                       settings.font_themes['speechbubble']),
    resource_sound_success: SoundData("data/sounds/magic_bell_2.ogg", 1.0, reserved_channel_id=settings.channel_gong),
    resource_sound_fail: SoundData("data/sounds/confusion.ogg", 1.0, reserved_channel_id=settings.channel_gong),
    resource_sound_hit_gore: SoundData("data/sounds/attack.ogg", 1.0, reserved_channel_id=settings.channel_gong),
    resource_sound_door: SoundData("data/sounds/108617__juskiddink__thud-dry.ogg", 1.0, reserved_channel_id=settings.channel_gong),
    resource_sound_newspaper: SoundData("data/sounds/179021__smartwentcody__straightening-newspaper.ogg", 1.0,
                                        reserved_channel_id=settings.channel_gong),
    resource_sound_click: SoundData("data/sounds/192270__lebaston100__click.ogg", 1.0),

    # navigation
    settings.KIND_ROOM_LAB: LabelConfig("Lab", settings.font_themes['navigation']),
    settings.KIND_ROOM_GORE_BASEMENT: LabelConfig("Al Gore Basement", settings.font_themes['navigation']),
    settings.KIND_ROOM_STEVENS_BASEMENT: LabelConfig("Stevens Basement", settings.font_themes['navigation']),
    settings.KIND_ROOM_STEVENS_YARD: LabelConfig("Stevens Yard", settings.font_themes['navigation']),

    # music
    resource_music_track_1: MusicData("data/music/Anguish.ogg", 1.0),

    # resource_sound1_example: SoundData("data/vis/sound1.ogg", 0.5),
    # resource_sound1_example: MusicData("data/vis/music1.ogg", 0.5),
    # settings.resource_worm: FakeImage(50, 50, (255, 0, 255)),
    # settings.resource_map1: MapResourceConfig(
    #     os.path.join(settings.map_dir, settings.map_file_names[settings.current_map])),
    # settings.resource_cursor: Image("./data/graphics/cursor.png"),
    # settings.resource_target: Image("./data/graphics/target.png"),
    # settings.resource_clue_blink: Image("./data/graphics/particlePack/star_08.png"),
    # settings.resource_black_smoke: DirectoryAnimation("./data/graphics/smokeparticleassets/blacksmoke", 10, transformation=resizeTrans),
    # settings.resource_fart: DirectoryAnimation("./data/graphics/smokeparticleassets/fart", 10, transformation=resizeTrans),
    # settings.resource_white_smoke: DirectoryAnimation("./data/graphics/smokeparticleassets/WhitePuff", 10, transformation=resizeTrans),
}

logger.debug("imported")

if __name__ == '__main__':
    logger.setLevel(logging.INFO)
    # print all missing resources
    logger.error("")
    logger.error("Missing resources:")
    for res_id, res in resource_def.items():
        if isinstance(res, FakeImage):
            logger.error(f" FakeImage Id: {res_id}  name: {res.name}  size: {res.width}x{res.height} ")
        elif isinstance(res, BubbleConfig):
            if default_speech_sound in res.sound_data.filename:
                logger.error(f" BubbleConfig Id: {res_id}  text: {res.text}")

    logger.error("")
    logger.error("Used image files:")
    for res_id, res in resource_def.items():
        if isinstance(res, Image):
            logger.error(f"  Image: id: {res_id} filename: {res.path_to_file}")

    logger.error("")
    logger.error("Used sound files:")
    for res_id, res in resource_def.items():
        if isinstance(res, SoundData):
            logger.error(f"  Sound: id {res_id} filename: {res.filename}")
        elif isinstance(res, BubbleConfig):
            logger.error(f"  Sound: id {res_id} filename: {res.sound_data.filename}")

    logger.error("")
    logger.error("Used music files:")
    for res_id, res in resource_def.items():
        if isinstance(res, MusicData):
            logger.error(f"  Music: id {res_id} filename: {res.filename}")
