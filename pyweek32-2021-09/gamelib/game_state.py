# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'game_state.py' is part of pw-32
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The GameState class to manage the global game state.

"""
from __future__ import print_function, annotations

import logging
from datetime import datetime, timedelta

from gamelib import settings, resources
from gamelib.gamelogic.eventdispatcher import event_dispatcher

logger = logging.getLogger(__name__)
logger.debug("importing...")


class MoveToRoomError(Exception):
    """Error raised when moving to another room fails"""
    pass


class BubbleData(object):

    def __init__(self, bubble_resource_id, position, source_position):
        """
        Represents a speech bubble.

        :param bubble_resource_id: the SpeechBubbleResourceId
        :param position: the position where the speech bubble shall appear on screen.
        :param source_position: the position of the source of the audio (e.g. pointer to where it originates from)
        """
        self.resource_id = bubble_resource_id
        self.position = position
        self.source_position = source_position


class Dialogue(object):

    def __init__(self, _dialogue_id, bubble_data: list[BubbleData], reset_count=-1):
        """
        Define a dialogue.
        :param _dialogue_id: Unique identifier of the dialogue. Use it to find the right dialog in the activation code.
        :param bubble_data: The dialogue data as a list of BubbleData
        :param reset_count: When the dialogue should be played, accessible. Default: -1 (always)
        """
        self.reset_count = reset_count
        self.id = _dialogue_id
        self.bubble_data = bubble_data


class MenuDialogue(object):

    def __init__(self, _dialogue_id, menu_dialogue_data, reset_count=-1, position=None):
        """
        Define a dialogue.

        Basic idea: The idea is that you have some menu items, each of them with its resource_id.
        When this menu item is activated, then the according dialogue is started. There is one SpeechBubble visible
        at the time and when the next button is clicked, then the following SpeechBubble is made visible and the current
        one disappears.

        Menu items setup:

            The menu_dialogue_data is just a dictionary of {menu_item_resource_id : Dialogue }

        Dialogue setup:

            The Dialogue class is basically a list of BubbleData that will follow each other.
            The dialogue id is probably the most important thing since there will be multiple instances of SpeechBubbles.
            It will allow to find the correct one and activate it.

            Each SpeechBubble needs following information: bubble_resource_id, position and source_position
            The bubble_resource_id is basically the rendered text and the corresponding audio.
            Position is where the bubble will be placed.
            Source_position is who is speaking or what is generating the audio, e.g. if its from the watch then set the
            coordinates of the clock, if its from an npc use their position, ect.

            If a dialogue should only be played at a certain reset_count then set this to the corresponding number of reset.
            Use -1 to ignore this and play it always, in any number of resets.





        :param _dialogue_id: Unique identifier of the dialogue. Use it to find the right dialog in the activation code.
        :param menu_dialogue_data: The menu dialogue data as a dictionary of {resource_id : Dialogue}. The resource_id used for the menu items.
        :param reset_count: When the dialogue should be played, accessible. Default: -1 (always)
        :param position: the midtop position of the menu items
        """
        self.position = position
        self.reset_count = reset_count
        self.id = _dialogue_id
        self.menu_dialogue_data = menu_dialogue_data


class _ItemData(object):

    def __init__(self, kind, position, activate, hidden=False, reveal_action=None, resource_id=None,
                 dialogue: Dialogue = None, menu_dialogue=None):
        """an entity and sprite can be created from the data in this item

        This is a static item used for creation of entities and sprites. It is not meant to be changed during runtime.

        kind: settings.KIND_*
        position: room placement coordinates (world coordinates)
        activate: settings.ACTION_*; what triggers this item's purpose in the story
        hidden: is the entity first invisible (e.g. the mushroom pizza is invisible until the fridge is open)
        reveal_action: settings.ACTION_* (the action that makes the entity visible)
        resource_id: the resource id to use
        dialogue: setup of dialogues when something is triggered, see Dialogue class
        menu_dialogue: setup of a menu dialogue, see MenuDialogue class
        """
        self.menu_dialogue = menu_dialogue
        self.dialogue: list[BubbleData] = dialogue
        self.resource_id = resource_id
        self.kind = kind
        self.position = position
        self.activate = activate
        self.hidden = hidden
        self.reveal_action = reveal_action


class _RoomState(object):
    kind = None
    west_exit = None
    north_exit = None
    east_exit = None
    # todo: attributes for tracking player progress in each room
    entities = []

    def __init__(self):
        pass

    # todo: methods for managing player progress in each room


sw_position = (150, 50)
ag_lab_position = (200, 285)
mg_position = (settings.screen_width / 2, settings.screen_height)
ts_ts_basement = (500, 300)


class LabState(_RoomState):
    kind = settings.KIND_ROOM_LAB
    east_exit = settings.KIND_ROOM_STEVENS_YARD
    entities = [
        _ItemData(settings.KIND_WRENCH_ITEM, (890, 330), settings.ACTION_MAKE_SUPERTOOL),
        _ItemData(settings.KIND_SUPERTOOL_ITEM, (930, 360), settings.ACTION_MAKE_SUPERTOOL,
                  hidden=True, reveal_action=settings.ACTION_MAKE_SUPERTOOL),
        _ItemData(settings.KIND_CLOSED_FRIDGE_ITEM, (311, 265), settings.ACTION_OPEN_FRIDGE),
        _ItemData(settings.KIND_OPEN_FRIDGE_ITEM, (270, 260), None,
                  hidden=True, reveal_action=settings.ACTION_OPEN_FRIDGE),
        _ItemData(settings.KIND_PIZZA_ITEM, (270 - 115 + 138 + 39, 260 - 115 + 57 + 19), settings.ACTION_MAKE_SUPERTOOL,
                  hidden=True, reveal_action=settings.ACTION_OPEN_FRIDGE),
        _ItemData(settings.KIND_MUSHROOM_ITEM, (315, 220), settings.ACTION_MAKE_SUPERTOOL,
                  hidden=True, reveal_action=settings.ACTION_GET_MUSHROOM),
        _ItemData(settings.KIND_ROOM_DOOR, (482, 101), None, hidden=False, resource_id=resources.resource_lab_door),
        _ItemData(settings.KIND_BACKGROUND, (0, 0), None, hidden=False, resource_id=resources.resource_background_lab),
        # _ItemData(settings.KIND_NPC_GORE, (-62, 168), settings.ACTION_POKE_GORE,
        #           resource_id=resources.resource_npc_gore_lab),
        # _ItemData(settings.KIND_TEXT_BUBBLE, (102, 160), None, hidden=True, dialogue=
        # Dialogue(settings.KIND_DIALOGUE_1, [
        #     BubbleData(resources.resource_test_bubble, (102, 160), (10, 10)),
        #     BubbleData(resources.resource_test_bubble, (202, 260), (1000, 510)),
        # ])),
        _ItemData(settings.KIND_GORE_HEAD, (130, 285 - 72), settings.ACTION_BASH_GORE,
                  resource_id=resources.resource_gore_head),
        _ItemData(settings.KIND_NPC_GORE, (-62, 168), settings.ACTION_POKE_GORE,
                  resource_id=resources.resource_npc_gore_lab,
                  # menu_dialogue=MenuDialogue(settings.KIND_DIALOGUE_1, {
                  #     resources.test_label1: Dialogue(settings.KIND_DIALOGUE_1, [
                  #         BubbleData(resources.resource_test_bubble, (102, 160), (10, 10)),
                  #         BubbleData(resources.resource_test_bubble, (202, 260), (1000, 510)),
                  #         BubbleData(resources.resource_test_bubble, (102, 160), (10, 10)),
                  #     ]),
                  #     resources.test_label2: Dialogue(settings.KIND_DIALOGUE_2, [
                  #         BubbleData(resources.resource_test_bubble, (202, 260), (1000, 510))
                  #
                  #     ]),
                  # }))
                  menu_dialogue=MenuDialogue(settings.KIND_DIALOGUE_LAB, {
                      resources.resource_label_who: Dialogue(settings.KIND_DIALOGUE_LAB_WHO, [
                          BubbleData(resources.resource_lab_who_0_ag, (300, 160), ag_lab_position),
                          BubbleData(resources.resource_lab_who_1_sw, (310, 170), sw_position),
                          BubbleData(resources.resource_lab_who_2_mg, (320, 180), mg_position),
                          BubbleData(resources.resource_lab_who_3_ag, (330, 190), ag_lab_position),
                      ]),
                      resources.resource_label_what: Dialogue(settings.KIND_DIALOGUE_LAB_WHAT, [
                          BubbleData(resources.resource_lab_what_0_ag, (300, 160), ag_lab_position),
                          BubbleData(resources.resource_lab_what_1_mg, (310, 170), mg_position),
                          BubbleData(resources.resource_lab_what_2_ag, (320, 180), ag_lab_position),
                          BubbleData(resources.resource_lab_what_3_sw, (330, 190), sw_position),
                      ]),
                      resources.resource_label_when: Dialogue(settings.KIND_DIALOGUE_LAB_WHEN, [
                          BubbleData(resources.resource_lab_when_0_ag, (300, 160), ag_lab_position),
                          BubbleData(resources.resource_lab_when_1_sw, (310, 170), sw_position),
                          BubbleData(resources.resource_lab_when_2_ag, (320, 180), ag_lab_position),
                          BubbleData(resources.resource_lab_when_3_mg, (330, 190), mg_position),
                          BubbleData(resources.resource_lab_when_4_ag, (340, 200), ag_lab_position),
                      ]),
                      resources.resource_label_where: Dialogue(settings.KIND_DIALOGUE_LAB_WHERE, [
                          BubbleData(resources.resource_lab_where_0_ag, (300, 160), ag_lab_position),
                          BubbleData(resources.resource_lab_where_1_sw, (310, 170), sw_position),
                      ]),
                      resources.resource_label_why: Dialogue(settings.KIND_DIALOGUE_LAB_WHY, [
                          BubbleData(resources.resource_lab_why_0_ag, (300, 160), ag_lab_position),
                          BubbleData(resources.resource_lab_why_1_sw, (310, 170), sw_position),
                          BubbleData(resources.resource_lab_why_2_mg, (320, 180), mg_position),
                          BubbleData(resources.resource_lab_why_3_sw, (330, 190), sw_position),
                      ]),
                      resources.resource_label_how: Dialogue(settings.KIND_DIALOGUE_LAB_HOW, [
                          BubbleData(resources.resource_lab_how_0_ag, (300, 160), ag_lab_position),
                          BubbleData(resources.resource_lab_how_1_sw, (310, 170), sw_position),
                      ]),
                  }, position=(130, 310))),
    ]


class StevensYardState(_RoomState):
    kind = settings.KIND_ROOM_STEVENS_YARD
    west_exit = settings.KIND_ROOM_LAB
    north_exit = None  # settings.KIND_ROOM_STEVENS_BASEMENT <- done in code
    east_exit = settings.KIND_ROOM_GORE_BASEMENT
    entities = [
        _ItemData(settings.KIND_DUMP_TRUCK_ITEM, (750, 300), settings.ACTION_HIJACK_DUMP_TRUCK),
        _ItemData(settings.KIND_BACKGROUND, (0, 0), None, hidden=False,
                  resource_id=resources.resource_background_stevens_yard),
        _ItemData(settings.KIND_ROOM_DOOR, (879, 482), None, hidden=False,
                  resource_id=resources.resource_stevens_yard_door),
        _ItemData(settings.KIND_YARD_DOOR_TO_BASEMENT, (80, 503), None, hidden=False,
                  resource_id=resources.resource_stevens_yard_door_to_basement),
        # fix supertool combining in other rooms
        _ItemData(settings.KIND_SUPERTOOL_ITEM, (930, 360), settings.ACTION_MAKE_SUPERTOOL,
                  hidden=True, reveal_action=settings.ACTION_MAKE_SUPERTOOL),

    ]


class StevensBasementState(_RoomState):
    kind = settings.KIND_ROOM_STEVENS_BASEMENT
    north_exit = settings.KIND_ROOM_STEVENS_YARD
    entities = [
        _ItemData(settings.KIND_BACKGROUND, (0, 0), None, hidden=False,
                  resource_id=resources.resource_background_stevens_basement),
        _ItemData(settings.KIND_ROOM_DOOR, (810, 153), None, hidden=False,
                  resource_id=resources.resource_stevens_basement_door),
        _ItemData(settings.KIND_NPC_STEVENS, (400, 400), settings.ACTION_POKE_STEVENS,
                  resource_id=resources.resource_npc_stevens_basement,
                  menu_dialogue=MenuDialogue(settings.KIND_DIALOGUE_LAB, {
                      resources.resource_label_when: Dialogue(settings.KIND_DIALOGUE_STEVENS_BASEMENT_WHEN, [
                          BubbleData(resources.resource_stevens_basement_when_0_mg, (600, 200), mg_position),
                          BubbleData(resources.resource_stevens_basement_when_1_ts, (610, 210), ts_ts_basement),
                      ]),
                      resources.resource_label_what: Dialogue(settings.KIND_DIALOGUE_STEVENS_BASEMENT_WHAT, [
                          BubbleData(resources.resource_stevens_basement_what_0_mg, (600, 200), mg_position),
                          BubbleData(resources.resource_stevens_basement_what_1_ts, (610, 210), ts_ts_basement),
                          BubbleData(resources.resource_stevens_basement_what_2_mg, (620, 220), mg_position),
                          BubbleData(resources.resource_stevens_basement_what_3_ts, (630, 230), ts_ts_basement),
                      ]),
                      resources.resource_label_how: Dialogue(settings.KIND_DIALOGUE_STEVENS_BASEMENT_HOW, [
                          BubbleData(resources.resource_stevens_basement_how_0_mg, (600, 200), mg_position),
                          BubbleData(resources.resource_stevens_basement_how_1_ts, (610, 210), ts_ts_basement),
                          BubbleData(resources.resource_stevens_basement_how_2_mg, (620, 220), mg_position),
                          BubbleData(resources.resource_stevens_basement_how_3_ts, (630, 230), ts_ts_basement),
                      ]),
                  })),
        # fix supertool combining in other rooms
        _ItemData(settings.KIND_SUPERTOOL_ITEM, (930, 360), settings.ACTION_MAKE_SUPERTOOL,
                  hidden=True, reveal_action=settings.ACTION_MAKE_SUPERTOOL),

    ]


class GoreBasementState(_RoomState):
    kind = settings.KIND_ROOM_GORE_BASEMENT
    west_exit = settings.KIND_ROOM_STEVENS_YARD
    entities = [
        _ItemData(settings.KIND_CROWBAR_ITEM, (705, 360), settings.ACTION_GET_CROWBAR),
        _ItemData(settings.KIND_MESSAGE_CLOG_ITEM, (130, 270), settings.ACTION_CLEAR_MESSAGE_CLOG),
        _ItemData(settings.KIND_BACKGROUND, (0, 0), None, hidden=False,
                  resource_id=resources.resource_background_gore_basement),
        _ItemData(settings.KIND_ROOM_DOOR, (873, 0), None, hidden=False,
                  resource_id=resources.resource_gore_basement_door),
        # fix supertool combining in other rooms
        _ItemData(settings.KIND_SUPERTOOL_ITEM, (930, 360), settings.ACTION_MAKE_SUPERTOOL,
                  hidden=True, reveal_action=settings.ACTION_MAKE_SUPERTOOL),

    ]


class DefaultKeys(object):
    fridge_open = False
    # set by _do_z_layer_inventory
    has_wrench = False
    has_mushroom = False
    has_supertool = False
    has_crowbar = False
    # set by .... ?
    whacked_gore = False
    hijacked_dump_truck = False
    unclogged_messages = False
    enter_stevens_basement = False

    def key_count(self):
        n = 0
        if self.has_crowbar:
            n += 1
        if self.has_supertool:
            n += 1
        if self.whacked_gore:
            n += 1
        if self.hijacked_dump_truck:
            n += 1
        if self.unclogged_messages:
            n += 1
        return n


class GameClock(object):

    def __init__(self):
        start_time = datetime.now()
        self._current_time = start_time
        self._initial_time = start_time

    def initialize(self, start_time):
        self._current_time = start_time
        self._initial_time = start_time
        logger.info("global clock initialized: %s", self.get_time_as_string())

    def tick(self, time_delta: timedelta):
        self._current_time = self._current_time + time_delta
        # logger.info("global clock ticked: %s  now: %s", time_delta, self.get_time_as_string())

    def tick_hour(self, count):
        hours_in_seconds = 60 * 60 * count
        self.tick(timedelta(0, hours_in_seconds))
        logger.info('TICK HOUR: time={}', self.hours_passed_since_init())

    def hours_passed_since_init(self):
        time_delta = self._current_time - self._initial_time
        return time_delta.seconds / 60 / 60

    def reset(self):
        self._current_time = self._initial_time
        logger.info("global clock reset: %s", self.get_time_as_string())

    def get_time_as_string(self):
        return self._current_time.strftime("%H:%M:%S")


class GameState:
    room_states = [
        LabState(),
        StevensYardState(),
        StevensBasementState(),
        GoreBasementState(),
    ]
    current_room = None  # settings.KIND_ROOM_*
    move_to_room = None  # settings.KIND_ROOM_*
    global_reset = False  # set True to trigger groundhog day reset in GlobalContext
    quit_game = False  # signals global context if QUIT event received
    game_won = False
    _is_debug_render_on = False

    _keys = None
    _inventory = []
    _inventory_sprites = {}

    clock = GameClock()

    _reset_count = 0

    played_dialogues = set()

    def reset(self):
        self._reset_count += 1
        logger.info("RESET count: %s", self.get_reset_count())
        self._inventory = []
        self._inventory_sprites = {}
        self._keys = None
        self.clock.reset()
        self.current_room = None
        self.move_to_room = None
        self.played_dialogues.clear()
        self.global_reset = False

    def get_reset_count(self):
        return self._reset_count

    def get_keys(self):
        if self._keys:
            return self._keys
        else:
            self._keys = DefaultKeys()
            return self._keys

    def get_inventory_entities(self):
        return self._inventory.copy()

    def get_inventory_sprites(self):
        return self._inventory_sprites.copy()

    def get_inventory_sprite(self, ent):
        return self._inventory_sprites.get(ent, None)

    def add_to_inventory(self, entity):
        names = settings._global_id_generator.names
        if entity not in self._inventory:
            self._inventory.append(entity)
            logger.info("Inventory: added entity to inventory %s \n Entries: %s %s",
                        names[entity.kind], entity, self._inventory)
            event_dispatcher.fire(settings.EVT_ADDED_TO_INVENTORY, entity)

        else:
            logger.debug("Inventory: skipped entity since already in inventory %s %s", names[entity.kind], entity)

    def add_to_inventory_sprites(self, sprites):
        for sprite in sprites:
            ent = getattr(sprite, 'entity', None)
            if ent in self._inventory:
                self._inventory_sprites[ent] = sprite

    def get_ent_by_kind(self, kind):
        ents = tuple(e for e in self._inventory if e.kind == kind)
        if ents:
            return ents[0]

    def inventory_contains(self, entity=None, kind=None):
        if entity:
            return entity in self._inventory
        elif kind:
            return bool(tuple(e for e in self._inventory if e.kind == kind))
        return False

    def remove_from_inventory(self, entity):
        try:
            self._inventory.remove(entity)
            names = settings._global_id_generator.names
            logger.info("Inventory: removed entity from inventory %s \n Entries: %s %s", names[entity.kind], entity,
                        self._inventory)
            self._inventory_sprites.pop(entity, None)
            event_dispatcher.fire(settings.EVT_REMOVED_FROM_INVENTORY, entity)
            return True
        except ValueError:
            # ignore
            logger.debug("Inventory: ignored entity since not present in inventory: %s", entity)
            pass
        return False

    def get_room_state(self, kind=None) -> _RoomState:
        """get room state object for kind, or current_room if kind is None
        Return: room state, or None if not found
        """
        if kind is None:
            kind = self.current_room
        for r in self.room_states:
            if r.kind == kind:
                return r

    def update_room(self):
        # This assumes the room is only updated when:
        # a) starting the game
        # b) moving to another room
        # If this assumption changes, then remove the final case, "raise MoveToRoomError". - Gumm
        if self.current_room is None:
            self.current_room = settings.KIND_ROOM_LAB
        elif self.move_to_room is not None:
            self.current_room = self.move_to_room
            self.move_to_room = None
        # elif not self.quit_game:
        #     raise MoveToRoomError("GameState.move_to_room is None: KIND_ROOM_* expected")


logger.debug("imported")
