# -*- coding: utf-8 -*-

from __future__ import print_function

import logging

import pygame

from gamelib import settings
from gamelib.gamelogic.eventdispatcher import event_dispatcher
from gamelib.gamelogic.sfx_handler import SfxHandler
from pyknic.pyknic_pygame.context import TimeSteppedContext

logger = logging.getLogger(__name__)
logger.debug("importing...")

"""SoundToyContext - setup instructions

Adding A Sound

1. If you want a dedicated sound channel you have to reserve them in pygame. See settings.MIXER_RESERVED_CHANNELS and
    the channel_* names that follow it. pygame mixer does not manage the reserved channels. The programmer is free to
    use reserved channels exclusively for specific sounds.
    
    This is useful if, for example, you have rapid gunfire and you want all the gunfire on one channel to avoid channel
    contention with other sounds. If you let pygame find a channel and they are all in use, it will steal a random
    channel. This cuts off a playing sound before it finishes, to give the channel to gunfire. The solution is to
    reserve a channel and use it exclusively for gunfire. That way gunfire only cuts off other gunfire; or you can check
    if the channel is busy and ignore the new gunfire to allow the playing gunfire to finish (this more complex behavior
    is handled by pyknic.pyknic_pygame.sfx.SoundPlayer.play(), which accepts extra='queue' or extra='mutex' when a
    reserved channel is used).

1. Add the sound resource to resources.py.
1.1 Create an ID, e.g.,
    resource_gong_example = _global_id_generator.next()
1.2 Create an entry in resource_def dict. The first example lets pygame mixer find a channel. The second example uses
    a dedicated channel, e.g.,
    resource_gong_example: SoundData("data/sounds/1.ogg", 1.0),
    resource_gong_example: SoundData("data/sounds/1.ogg", 1.0, reserved_channel_id=settings.channel_gong),

2. Create an event for the sound in settings.py, e.g.,
    EVT_GONG_SOUND = _global_id_generator.next()

3. Register the event in gamelib.gamelogic.sfx_handler.SfxHandler.register_events and add the listener, e.g.,
    event_dispatcher.register_event_type(settings.EVT_GONG_SOUND)
    event_dispatcher.add_listener(settings.EVT_GONG_SOUND, self._on_gong)

4. Create the callback for the event in gamelib.gamelogic.sfx_handler.SfxHandler, e.g.,
    def _on_gong(self, *args):
        self.sfx_player.play(resource_manager.resources[resources.resource_gong_example])

5. Somewhere in the game logic there will be an object that calls  event_dispatcher.fire, e.g. pseudocode,
    if user clicks on bell:
        event_dispatcher.fire(EVT_GONG_SOUND)
"""


class SoundToyContext(TimeSteppedContext):
    """Sound Toy Context
    This is intended to allow testing and playing with sound files and sound events. There is very little code in here.
    Most of the configuration is done during game loading, so that we only have to call event_dispatcher.fire() to play
    a sound. See the method handle_events() for this.
    """

    def __init__(self, game_state):
        TimeSteppedContext.__init__(self, settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS)
        self.game_state = game_state
        self.sfx_handler = SfxHandler(None)
        self.sfx_handler.register_events(event_dispatcher)
        self.screen = None
        self.clear_color = pygame.Color('midnightblue')

    def enter(self):
        pygame.mouse.set_visible(False)
        self.screen = pygame.display.get_surface()

    def resume(self):
        pass

    def draw_step(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0,
                  screen=None):  # fixme: AFTER_PYWEEK this dt is wrong...
        screen = screen if screen is not None else self.screen
        screen.fill(self.clear_color)
        pygame.display.flip()

    def update_step(self, delta, sim_time, *args):
        self.handle_events()

    def handle_events(self):
        for e in pygame.event.get():
            if e.type == pygame.MOUSEBUTTONDOWN:
                logger.error('MOUSE CLICK PLAY GONG')
                event_dispatcher.fire(settings.EVT_GONG_SOUND)
            elif e.type == pygame.KEYDOWN:
                if e.key == pygame.K_ESCAPE:
                    self.pop()
            elif e.type == pygame.QUIT:
                self.pop()


logger.debug("imported")
