# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'bubble_resource_loader.py' is part of pw-32
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""


"""
from __future__ import print_function

import logging

import pygame

import pyknic
import pyknic.pyknic_pygame.pygametext
from gamelib import settings
from gamelib.resources import ImageWithHover, ImageResourceWithHover, logger, BubbleConfig, LabelConfig
from pyknic.pyknic_pygame.sfx import SoundData, MusicData
from pyknic.resource2 import AbstractResourceLoader, AbstractResource

logger = logging.getLogger(__name__)
logger.debug("importing...")


class ImageWithHoverLoader(AbstractResourceLoader):
    def __init__(self):
        AbstractResourceLoader.__init__(self, ImageWithHover)

    def load(self, resource_config, file_cache):
        key = (resource_config.path_to_file, resource_config.border)
        image = file_cache.get(key, None)
        if image is None:
            image = pygame.image.load(resource_config.path_to_file)
            image = image.convert_alpha()
            file_cache[key] = image
        key = (resource_config.path_to_file, resource_config.border, "hover")
        hover = file_cache.get(key, None)
        if hover is None:
            hover = self.get_hover_image(image, resource_config.border)
            file_cache[key] = hover
        return ImageResourceWithHover(resource_config, image, hover, resource_config.border)

    def get_hover_image(self, image, border):
        if not settings.highlight_items:
            return image, image.get_rect().center

        border_size = 2 * border
        r = image.get_rect().inflate(border_size, border_size)  # add 10 border
        r.move_ip(-r.x, -r.y)
        i = pygame.Surface(image.get_size(), pygame.SRCALPHA)
        mask = pygame.mask.from_surface(image, 100)
        i.fill((0, 0, 0, 0), r, pygame.BLEND_RGBA_MULT)
        step_size = 1
        outline = mask.outline(step_size)
        yellow = (255, 255, 0)
        pygame.draw.polygon(i, yellow, outline)
        factor = 0.5
        # i = pyknic.pyknic_pygame.transform.blur_surf(i, border * factor, border * factor, border_size)
        i = pyknic.pyknic_pygame.transform.box_blur(i, (21, 21), iterations=4, border=border_size)
        iw, ih = i.get_size()
        i = pygame.transform.smoothscale(i, (iw + 21, ih + 21))
        return i, r.center

    def unload(self, res):
        pass


class SoundDataLoader(AbstractResourceLoader):

    def __init__(self):
        AbstractResourceLoader.__init__(self, SoundData)

    def load(self, sound_data_config, file_cache):
        key = (sound_data_config.filename, sound_data_config.volume, sound_data_config.reserved_channel_id)
        sound = file_cache.get(key, None)
        if sound is None:
            sound = self._load_sound(sound_data_config)
            file_cache[key] = sound
        return sound  # this is unusual, returns a SoundData instance with sound and channel attributes filled

    def _load_sound(self, data):
        if pygame.mixer.get_init() is None:
            raise Exception("pygame.mixer not initialized!")

        reserved_channel_ids = set()
        if isinstance(data, SoundData):
            logger.debug('loading sfx: {}', data.filename)
            try:
                data.sound = pygame.mixer.Sound(data.filename)
            except Exception as e:
                logger.error("error loading %s: %s", data.filename, e)
                raise e
            data.sound.set_volume(data.volume)
            if data.reserved_channel_id is not None:
                reserved_channel_ids.add(data.reserved_channel_id)
                # if len(reserved_channel_ids) > self._num_reserved_channels:  # todo after pw: not sure how to handle this
                #     raise OverflowError("loading more reserved channels as configured reserved channels!")
                data.channel = pygame.mixer.Channel(data.reserved_channel_id)
        else:
            raise TypeError("cannot load other than SoundData instances, but it is " + type(data))
        return data

    def unload(self, res):
        pass


class MusicLoader(AbstractResourceLoader):

    def __init__(self):
        AbstractResourceLoader.__init__(self, MusicData)

    def load(self, music_config, file_cache):
        key = (music_config.filename, music_config.volume)
        music = file_cache.get(key, None)
        if music is None:
            music = music_config
            file_cache[key] = music
        return music  # this is unusual, returns a MusicData instance without modifications

    def unload(self, res):
        pass


class BubbleResource(AbstractResource):

    def __init__(self, resource_config, image: pygame.Surface, sound_data: SoundData):
        AbstractResource.__init__(self, resource_config)
        self.image = image
        self.sound_data = sound_data


class BubbleTextLoader(AbstractResourceLoader):

    def __init__(self, sound_data_loader: SoundDataLoader):
        AbstractResourceLoader.__init__(self, BubbleConfig)
        self.sound_data_loader = sound_data_loader

    def load(self, bubble_config, file_cache):
        text = bubble_config.text + "\n"
        key = (text, bubble_config.sound_data)
        bubble_resource = file_cache.get(key, None)
        if bubble_resource is None:
            sound_data = self.sound_data_loader.load(bubble_config.sound_data, file_cache)
            image = pyknic.pyknic_pygame.pygametext.getsurf(text, **bubble_config.text_style_config)
            bubble_resource = BubbleResource(bubble_config, image, sound_data)
            file_cache[key] = bubble_resource
        return bubble_resource

    def unload(self, res):
        pass


class LabelResource(AbstractResource):

    def __init__(self, resource_config, image: pygame.Surface):
        AbstractResource.__init__(self, resource_config)
        self.image = image


class TextLabelLoader(AbstractResourceLoader):

    def __init__(self):
        AbstractResourceLoader.__init__(self, LabelConfig)

    def load(self, label_config, file_cache):
        key = label_config.text
        label_resource = file_cache.get(key, None)
        if label_resource is None:
            image = pyknic.pyknic_pygame.pygametext.getsurf(label_config.text, **label_config.text_style_config)
            label_resource = LabelResource(label_config, image)
            file_cache[key] = label_resource
        return label_resource

    def unload(self, res):
        pass


logger.debug("imported")
