# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'resources.py' is part of pw29pw---_-
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The resource loading module.


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2020"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module

import pygame

import pyknic
from pyknic.pyknic_pygame.sfx import MusicData

logger = logging.getLogger(__name__)
logger.debug("importing...")


class _ResInfo(object):

    def __init__(self):
        self.data = None


class ImageInfo(_ResInfo):

    def __init__(self, image_path, do_convert=True, scale=1.0, offset=(0, 0)):
        self.offset = offset
        self.scale = scale
        self.do_convert = do_convert
        self.image_path = image_path


class SurfInfo(_ResInfo):

    def __init__(self, size, color, offset=(0, 0)):
        self.color = color
        self.size = size
        self.offset = offset


class FontInfo(_ResInfo):

    def __init__(self, font_path, size=-1):
        self.font_path = font_path
        self.size = size


_gen = pyknic.generators.IdGenerator(1000)
POLE1 = _gen.next()
POLE_PLUS = _gen.next()
POLE_MINUS = _gen.next()
POLE_DISPLAY = _gen.next()
# BUTTERFLY = _gen.next()
MOUSE_DEFAULT = _gen.next()
MOUSE_GRAB = _gen.next()
MOUSE_DRAG = _gen.next()
MOUSE_CLICK = _gen.next()
MOUSE_DROP = _gen.next()
MOUSE_NO_DROP = _gen.next()
BUTTON = _gen.next()
NEEDLE = _gen.next()
DEPOT = _gen.next()
LAYOUT = _gen.next()
OFFBUTTON = _gen.next()
SLIDERBUTTON = _gen.next()
SLIDERBUTTONVOLUME = _gen.next()
SLIDERBACKGROUND = _gen.next()
FRAME = _gen.next()
STORY = _gen.next()
FONT_DISPLAY = _gen.next()
FONT_LCD = _gen.next()
TILE_FRAME = _gen.next()
TILE_BROKEN = _gen.next()
TILE_FIXED = _gen.next()
TILE_HIGHLIGHT = _gen.next()
TILE_SHADE = _gen.next()
TILE_FONT = _gen.next()
BUTTERFLY01 = _gen.next()
BUTTERFLY02 = _gen.next()
BUTTERFLY03 = _gen.next()
BUTTERFLY04 = _gen.next()
BUTTERFLY05 = _gen.next()
BUTTERFLY06 = _gen.next()
BUTTERFLY07 = _gen.next()
BUTTERFLY08 = _gen.next()
BUTTERFLY09 = _gen.next()
BUTTERFLY10 = _gen.next()
BUTTERFLY11 = _gen.next()
BUTTERFLY12 = _gen.next()
BUTTERFLY13 = _gen.next()
BUTTERFLY14 = _gen.next()
BUTTERFLY15 = _gen.next()
BUTTERFLY16 = _gen.next()


class ColorKeyImageInfo(object):
    def __init__(self, image_path, color_key, flags=0, scale=1.0):
        self.scale = scale
        self.image_path = image_path
        self.color_key = color_key
        self.flags = flags


resource_info = {
    POLE1: ImageInfo("./data/magnet.png", offset=(0, -4)), #SurfInfo((50, 50), (0, 0, 200)),  # ImageInfo("./data/needle.png"),
    POLE_PLUS: ImageInfo("./data/north.png"), #SurfInfo((15, 15), (255, 0, 0)),  # ImageInfo("./data/needle.png"),
    POLE_MINUS: ImageInfo("./data/south.png"), #SurfInfo((15, 15), (0, 0, 255)),  # ImageInfo("./data/needle.png"),
    POLE_DISPLAY: ImageInfo("./data/poledisplay.png", offset=(0, -4)), #SurfInfo((15, 15), (100, 150, 0)),  # ImageInfo("./data/needle.png"),
    # BUTTERFLY: ImageInfo("./data/butterfly.jpg"),  # SurfInfo((600, 400), (100, 100, 100)),
    # BUTTERFLY: ImageInfo("./data/butterfly-clip-art-4-transparent-small.png"),  # SurfInfo((600, 400), (100, 100, 100)),
    # SurfInfo((5, 5), (255, 255, 255)),
    MOUSE_DEFAULT: ImageInfo("./data/iconfinder_icon_22_one_finger_329391.png", scale=0.5, offset=(-3, -13)),
    # SurfInfo((5, 5), (255, 0, 255)),
    MOUSE_CLICK: ImageInfo("./data/iconfinder_icon_27_one_finger_click_329387.png", scale=0.5, offset=(-3, -13)),
    # SurfInfo((5, 5), (200, 200, 255)),
    MOUSE_GRAB: ImageInfo("./data/iconfinder_icon_3_high_five_329409.png", scale=0.5),  # SurfInfo((5, 5), (0, 130, 0)),
    MOUSE_DRAG: ImageInfo("./data/iconfinder_icon_22_one_finger_329391_modified.png", scale=0.5),
    MOUSE_DROP: ImageInfo("./data/iconfinder_icon_22_one_finger_329391_modified.png", scale=0.5),
    # SurfInfo((5, 5), (255, 255, 0)),
    MOUSE_NO_DROP: ImageInfo("./data/iconfinder_icon_22_one_finger_329391_modified_nodrop.png", scale=0.5), # SurfInfo((5, 5), (255, 0, 0)),
    LAYOUT: ImageInfo("./data/layout.png"), # SurfInfo((5, 5), (255, 0, 0)),
    BUTTON: ImageInfo("./data/fixbutton.png"), #SurfInfo((25, 25), (0, 255, 255)),
    OFFBUTTON: ImageInfo("./data/offbutton.png"), #SurfInfo((25, 25), (0, 255, 255)),
    NEEDLE: ColorKeyImageInfo("./data/needle.PNG", (255, 255, 255, 255), pygame.RLEACCEL, 0.15),
    DEPOT: ImageInfo("./data/depot.png"), #SurfInfo((104, 466), (200, 0, 0)),
    SLIDERBUTTON: ImageInfo("./data/sliderbutton.png"), #SurfInfo((23, 10), (50, 50, 50)),
    SLIDERBUTTONVOLUME: ImageInfo("./data/sliderbuttonvolume.png"), #SurfInfo((23, 10), (50, 50, 50)),
    SLIDERBACKGROUND: ImageInfo("./data/sliderbackground.png"), #SurfInfo((23, 10), (50, 50, 50)),
    FRAME: ImageInfo("./data/frame.png"), #SurfInfo((23, 10), (50, 50, 50)),
    STORY: ImageInfo("./data/story.png"), #SurfInfo((23, 10), (50, 50, 50)),
    FONT_DISPLAY: FontInfo("./data/lcd_phone_font_4079/LCDPHONE.TTF", 22),
    FONT_LCD: FontInfo("./data/dsdigital_font_4075/DS-DIGIB.TTF", 13),
    TILE_FRAME: ImageInfo("./data/tileframe.png"), #SurfInfo((120, 90), (200, 43, 123)),
    TILE_BROKEN: ImageInfo("./data/tilebroken.png"), #SurfInfo((150, 100), (50, 53, 53)),
    TILE_FIXED: ImageInfo("./data/tilefixed.png"), #SurfInfo((150, 100), (0, 200, 0)),
    TILE_HIGHLIGHT: ImageInfo("./data/tilehighlight.png"), #SurfInfo((170, 120), (255, 255, 255)),
    TILE_SHADE: ImageInfo("./data/tileshade.png"), #SurfInfo((170, 120), (0, 0, 0, 128)),
    TILE_FONT: FontInfo("./data/dsdigital_font_4075/DS-DIGI.TTF", 15),
    # BUTTERFLY01: ImageInfo("./data/butterfly.jpg"),
    # BUTTERFLY02: ImageInfo("./data/butterfly.jpg"),
    # BUTTERFLY03: ImageInfo("./data/butterfly.jpg"),
    # BUTTERFLY04: ImageInfo("./data/butterfly.jpg"),
    # BUTTERFLY05: ImageInfo("./data/butterfly.jpg"),
    # BUTTERFLY06: ImageInfo("./data/butterfly.jpg"),
    # BUTTERFLY07: ImageInfo("./data/butterfly.jpg"),
    # BUTTERFLY08: ImageInfo("./data/butterfly.jpg"),
    # BUTTERFLY09: ImageInfo("./data/butterfly.jpg"),
    # BUTTERFLY10: ImageInfo("./data/butterfly.jpg"),
    # BUTTERFLY11: ImageInfo("./data/butterfly.jpg"),
    # BUTTERFLY12: ImageInfo("./data/butterfly.jpg"),
    # BUTTERFLY13: ImageInfo("./data/butterfly.jpg"),
    # BUTTERFLY14: ImageInfo("./data/butterfly.jpg"),
    # BUTTERFLY15: ImageInfo("./data/butterfly.jpg"),
    # BUTTERFLY16: ImageInfo("./data/butterfly.jpg"),


    BUTTERFLY01: ImageInfo("./data/butterflies/gray-and-black-butterfly-sniffing-white-flower-91946.jpg"),
    BUTTERFLY02: ImageInfo("./data/butterflies/macro-photography-of-butterfly-near-lights-1021685.jpg"),
    BUTTERFLY03: ImageInfo("./data/butterflies/nature-couple-flower-insects-36709.jpg"),
    BUTTERFLY04: ImageInfo("./data/butterflies/nature-orange-butterfly-silver-bordered-fritillary-33073.jpg"),
    BUTTERFLY05: ImageInfo("./data/butterflies/36838807772_737b309522_o.png"),
    BUTTERFLY06: ImageInfo("./data/butterflies/animal-beverage-blur-butterfly-603868.jpg"),
    BUTTERFLY07: ImageInfo("./data/butterflies/black-and-white-butterfly-on-red-flower-53957.jpg"),
    BUTTERFLY08: ImageInfo("./data/butterflies/black-and-white-butterfly-perch-on-yellow-petaled-flower-67811.jpg"),
    BUTTERFLY09: ImageInfo("./data/butterflies/bloom-blooming-blossom-blur-462118.jpg"),
    BUTTERFLY10: ImageInfo("./data/butterflies/blue-brown-white-black-66877.jpg"),
    BUTTERFLY11: ImageInfo("./data/butterflies/butter.jpg"),
    BUTTERFLY12: ImageInfo("./data/butterflies/butterflies-black-and-white-9159.jpg"),
    BUTTERFLY13: ImageInfo("./data/butterflies/butterflies-on-sand-2143981.jpg"),
    BUTTERFLY14: ImageInfo("./data/butterflies/close-up-photography-of-a-butterfly-672142.jpg"),
    BUTTERFLY15: ImageInfo("./data/butterflies/closeup-photo-of-black-and-gray-housefly-on-white-surface-1046492.jpg"),
    BUTTERFLY16: ImageInfo("./data/butterflies/db5dm5y-81e6240d-050d-4b5d-be12-8271f6328427.jpg"),
}

songs = [
    MusicData("./data/audio/carefree-by-kevin-macleod-from-filmmusic-io.ogg", 1.0),
    MusicData("./data/audio/anguish-by-kevin-macleod-from-filmmusic-io.ogg", 1.0),
]

def load_resources(resource_definitions, progress_cb=None):
    total = len(resource_definitions)
    name = "..."
    if loaded_resources is not None:
        logger.warning("resources already loaded!")
        if progress_cb:
            progress_cb(total, total, name)
        return loaded_resources

    cache = {}
    result = {}  # {id: resource}
    for idx, item in enumerate(resource_definitions.items()):
        res_id, info = item
        if isinstance(info, ImageInfo):
            name = info.image_path
            key = (info.image_path, info.do_convert, info.scale)
            img = cache.get(key, None)
            if img is None:
                img = cache.get(info.image_path, None)
                if img is None:
                    img = pygame.image.load(info.image_path)
                    cache[info.image_path] = img
                if info.scale != 1.0:
                    img = pygame.transform.rotozoom(img, 0, info.scale)
                if info.do_convert:
                    if img.get_flags() & pygame.SRCALPHA:
                        img = img.convert_alpha()
                    else:
                        img = img.convert()
                cache[key] = img
            info.data = img
        elif isinstance(info, ColorKeyImageInfo):
            name = info.image_path
            key = (info.image_path, info.color_key, info.flags, info.scale)
            img = cache.get(key, None)
            if img is None:
                img = cache.get(info.image_path, None)
                if img is None:
                    img = pygame.image.load(info.image_path)
                    cache[info.image_path] = img
                img = img.copy().convert()
                img = pygame.transform.rotozoom(img, 0, info.scale)
                img.set_colorkey(info.color_key, info.flags)
                cache[key] = img
            info.data = img
        elif isinstance(info, SurfInfo):
            name = f"Surface: {info.size}, {info.color}"
            img = pygame.Surface(info.size, pygame.SRCALPHA)
            img.fill(info.color)
            info.data = img
        elif isinstance(info, FontInfo):
            name = info.font_path
            key = info.font_path, info.size
            font = cache.get(key, None)
            if font is None:
                font = pygame.font.Font(info.font_path, info.size)
                cache[key] = font
            info.data = font
        else:
            raise Exception("unknown resource type: " + info)
        result[res_id] = info
        if progress_cb:
            progress_cb(idx + 1, total, name)

    return result


loaded_resources = None

logger.debug("imported")
