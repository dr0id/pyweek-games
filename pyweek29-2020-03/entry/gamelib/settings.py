# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'settings.py' is part of pw29pw---_-
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The settings of the game.
"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2020"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module
import pygame

import pyknic

logger = logging.getLogger(__name__)
logger.debug("importing...")


# application
path_to_icon = "./data/icon.png"
title = "Magnetic Pictures"
log_level = logging.DEBUG

# screen
screen_height = 768
screen_width = 1024
screen_size = None  # this will be set bei the initialization
screen_flags = 0 #pygame.NOFRAME

# Mixer
MIXER_FREQUENCY = 0  # default:22050
MIXER_SIZE = -16  # default: -16
MIXER_CHANNELS = 2  # default: 2 stereo or 1 mono
MIXER_BUFFER_SIZE = 16  # default: 4096

MIXER_NUM_CHANNELS = 24
MIXER_RESERVED_CHANNELS = 6  # default:

master_volume = 1.0
music_ended_pygame_event = pygame.USEREVENT

# main loop
max_dt = 0.2
step_dt = 0.02
draw_fps = 30

logger.debug("imported")
_gen = pyknic.generators.IdGenerator(2000)
KIND_MOUSE = _gen.next()
KIND_POLE = _gen.next()
KIND_FIELD = _gen.next()
KIND_BUTTON = _gen.next()
KIND_SLIDER = _gen.next()
KIND_DEPOT = _gen.next()
KIND_SLIDER_BACKGROUND = _gen.next()
