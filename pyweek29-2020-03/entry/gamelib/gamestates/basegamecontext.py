# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'basegamecontext.py' is part of pw29pw---_-
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The game context base. All context should inherit from The BaseGameContext.
.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2020"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []    # list of public visible parts of this module

from abc import ABCMeta

import pygame

from gamelib import settings
from gamelib.settings import KIND_MOUSE, KIND_BUTTON
from pyknic.events import Signal
from pyknic.mathematics import Point2
from pyknic.pyknic_pygame.context import TimeSteppedContext
from pyknic.pyknic_pygame.spritesystem import Sprite

logger = logging.getLogger(__name__)
logger.debug("importing...")


class BaseGameContext(TimeSteppedContext, metaclass=ABCMeta):

    def __init__(self, app):
        self.app = app
        max_dt = settings.max_dt
        step_dt = settings.step_dt
        draw_fps = settings.draw_fps
        TimeSteppedContext.__init__(self, max_dt, step_dt, draw_fps)

    def enter(self):
        TimeSteppedContext.enter(self)
        pygame.event.clear()

    def exit(self):
        TimeSteppedContext.exit(self)

    def update_music_player(self):
        for event in pygame.event.get(settings.music_ended_pygame_event):
            if event.type == settings.music_ended_pygame_event:
                self.app.music_player.on_music_ended()


class InteractionOptions(object):
    Default = 0
    Click = 1
    Grab = 2
    Drop = 3,
    NoDrop = 4
    Drag = 5


class BaseObject(object):

    def __init__(self, kind):
        self.kind = kind

    def on_focus(self, mouse):
        logger.debug("on_focus %s", self)

    def on_focus_lost(self, mouse):
        logger.debug("on_focus_lost %s", self)

    def on_drag_enter(self, mouse):
        logger.debug("on_drag_enter %s", self)
        mouse.interaction_option = InteractionOptions.NoDrop

    def on_drag_leave(self, mouse, next_hover):
        logger.debug("on_drag_leave %s", self)

    def on_drop(self, other):
        logger.debug("on_drop %s (returning False)", self)
        return False  # drop failed, return true for success

    def click(self, button):
        logger.debug("clicked %s button %s", self, button)

    def on_drop_failed(self, mouse):
        mouse.interaction_option = InteractionOptions.Default
        logger.debug("on_drop_failed %s", self, mouse)

    def on_drag_start(self, mouse, button):
        """
        Called when dragging starts.
        :param mouse: The mouse object
        :return: True if dragging is enable, False otherwise.
        """
        logger.debug("on_drag_start %s (returning False)", self, mouse)
        return False  # no dragging, return True to allow dragging

    def on_drag_end(self, mouse):
        logger.debug("on_drag_end")

    def on_drag(self, mouse, x, y, dx, dy):
        logger.debug("on_drag pos: %s %s rel: %s %s", x, y, dx, dy)


class GameSprite(Sprite):

    def __init__(self, image, position, obj, anchor=None, z_layer=None, parallax_factors=None, do_init=True,
                 name='_NO_NAME_'):
        Sprite.__init__(self, image, position, anchor, z_layer, parallax_factors, do_init, name)
        self.obj = obj


class Mouse(BaseObject):

    def __init__(self):
        BaseObject.__init__(self, KIND_MOUSE)
        self.position = Point2(0, 0)
        self.hover_obj = None
        self.grabbed_obj = None
        self._interaction_option = InteractionOptions.Default
        self._press_obj = {}  # {button: hover_obj}
        self.event_interaction_option_changed = Signal("mouse")

    @property
    def interaction_option(self):
        return self._interaction_option

    @interaction_option.setter
    def interaction_option(self, value):
        old_option = self._interaction_option
        self._interaction_option = value
        if old_option != value:
            self.event_interaction_option_changed.fire(self, old_option)

    def on_drag(self, pos, rel, buttons, objects):
        if self in objects:
            objects.remove(self)

        if self.grabbed_obj:
            if self.grabbed_obj in objects:
                objects.remove(self.grabbed_obj)
            hover_obj = objects[0] if objects else None
            if hover_obj != self.hover_obj:
                if self.hover_obj and self.hover_obj != self.grabbed_obj:
                    self.hover_obj.on_drag_leave(self, hover_obj)  # todo is it a hack to pass the next hover_obj?
                self.hover_obj = hover_obj
                if self.hover_obj:
                    self.hover_obj.on_drag_enter(self)

            if buttons[0] != 0:
                self.grabbed_obj.on_drag(self, *pos, *rel)

        self.set_position(*pos)

    def set_position(self, mx, my):
        self.position.x = mx
        self.position.y = my

    def check_hover(self, objects, pos):
        self.set_position(*pos)
        if self in objects:
            objects.remove(self)
        if self.grabbed_obj in objects:
            objects.remove(self.grabbed_obj)
        hover_obj = objects[0] if objects else None
        if hover_obj != self.hover_obj:
            if self.hover_obj:
                self.hover_obj.on_focus_lost(self)
            self.hover_obj = hover_obj
            if self.hover_obj:
                self.hover_obj.on_focus(self)

    def press(self, button, pos, objects):
        logger.debug(f"mouse mB press {button}")
        self.check_hover(objects, pos)
        self._press_obj[button] = self.hover_obj
        if self.hover_obj and self.hover_obj.on_drag_start(self, button):
            self.grabbed_obj = self.hover_obj

    def release(self, button, pos, objects):
        logger.debug(f"mouse mB release {button}")
        self.check_hover(objects, pos)
        if self.grabbed_obj:
            if button == 1:
                if self.hover_obj is None or not self.hover_obj.on_drop(self.grabbed_obj):
                    self.grabbed_obj.on_drop_failed(self)
                    if self.hover_obj:
                        # self.hover_obj.on_drag_leave(self)
                        self.hover_obj.on_focus(self)
                else:
                    # if self.hover_obj:
                    #     self.hover_obj.on_drag_leave(self)
                    self.grabbed_obj.on_drag_end(self)
                    self.hover_obj = self.grabbed_obj
                    self.hover_obj.on_focus(self)
                self.grabbed_obj = None
        else:
            tracked_obj = self._press_obj.get(button, None)
            if tracked_obj and tracked_obj == self.hover_obj:
                if self.hover_obj:
                    self.hover_obj.click(button)
        self._press_obj[button] = None


class Button(BaseObject):

    def __init__(self, position, on_click_cb):
        BaseObject.__init__(self, KIND_BUTTON)
        self.position = position
        self.on_click_cb = on_click_cb

    def click(self, button):
        self.on_click_cb()

    def on_focus(self, mouse):
        mouse.interaction_option = InteractionOptions.Click

    def on_focus_lost(self, mouse):
        mouse.interaction_option = InteractionOptions.Default


logger.debug("imported")
