# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'gamecontext.py' is part of pw29pw---_-
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The GameContext

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2020"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module
import math
import random
from collections import OrderedDict

import pygame

from gamelib import resources
from gamelib.gamestates.basegamecontext import BaseGameContext, BaseObject, InteractionOptions, GameSprite, Mouse, \
    Button
from gamelib.gamestates.gameobjects import Pole, POLE_CAPABILITY_NONE, POLE_CAPABILITY_FLIP, POLE_CAPABILITY_FULL
from gamelib.settings import KIND_FIELD, KIND_SLIDER, KIND_DEPOT, KIND_SLIDER_BACKGROUND
from pyknic.events import Signal
from pyknic.mathematics import Point2, Vec2
from pyknic.pyknic_pygame.spritesystem import DefaultRenderer, Camera, TextSprite
from pyknic.timing import Timer, Scheduler
from pyknic.tweening import Tweener

logger = logging.getLogger(__name__)
logger.debug("importing...")

BG_LAYER = 0
FIELD_LAYER = 1
FRAME_LAYER = FIELD_LAYER + 1
DEPOT_LAYER = BG_LAYER + 1
DISPLAY_FONT_LAYER = 50
POLE_LAYER = 100
BUTTON_LAYER = 5
SLIDER_BG = 6
SLIDER_LAYER = 7


class Needle(object):

    def __init__(self, rect, offset, angle=0):
        self.offset = offset
        self.position = rect.center
        self.angle = angle
        self._old_angle = angle
        self.solution_angle = 0

        self.image = None
        self._image = None
        self.size = 1.0
        self.rect = rect

    # def update(self, others):
    #     px, py = self.position
    #     fx = 0
    #     fy = 0
    #     for pole in others:
    #         ox, oy = pole.position.as_xy_tuple()  # todo use vector math
    #         ox, oy = snap_to_grid(ox, oy)
    #         of = pole.polarity
    #         dx = px - ox
    #         dy = py - oy
    #         d = dx * dx + dy * dy
    #         if d == 0:
    #             continue
    #         fx -= of / d * (-px + ox)
    #         fy -= of / d * (py - oy)
    #
    #     self.angle = int(math.degrees(math.atan2(fy, fx)))
    #
    #     # # todo hack move draw code outside
    #     # if self._image is None:
    #     #     return
    #     #
    #     # if self.angle != self._old_angle:
    #     #     self._old_angle = self.angle
    #     #     a = (self.angle - self.solution_angle) % 360
    #     #
    #     #     if True:
    #     #         # alpha blended, very hard
    #     #         self.image = pygame.Surface(self._image.get_size(), flags=pygame.SRCALPHA)
    #     #         self.image.blit(self._image, (0, 0))
    #     #         c = pygame.Color(255, 255, 255, 255)
    #     #         h, s, v, _ = c.hsva
    #     #         aa = 360 - a
    #     #         if aa < 180:
    #     #             alpha = 100 - int(aa / 180.0 * 100)
    #     #         else:
    #     #             alpha = int((aa - 180) / 180.0 * 100)
    #     #         c.hsva = (h, s, v, alpha)
    #     #         self.image.fill(c, None, pygame.BLEND_RGBA_MULT)  # todo maybe use another field for alpha?
    #     #         self.image = pygame.transform.rotozoom(self.image, a, self.size)
    #     #         self.rect = self.image.get_rect(center=self.position)
    #     #     else:
    #     #         # rotational
    #     #         self.image = pygame.transform.rotozoom(self._image, a, self.size)
    #     #         self.rect = self.image.get_rect(center=self.position)
    #
    #     # self.angles = [self.get_angle(*x) for x in diff]
    #     # self.rects = list(map(lambda i, p: self.stock[i].get_rect(center=p), self.angles, self.needlePos))

    def set_image(self, image, size, solution_angle):
        self.solution_angle = solution_angle
        self._image = image
        self.size = size
        self.rect = image.get_rect(center=self.position)
        self.image = pygame.transform.rotozoom(self._image, self.angle, self.size)


def snap_to_grid(x, y, grid_size=5):
    return x // grid_size * grid_size, y // grid_size * grid_size


class Depot(BaseObject):

    def __init__(self, topleft_position):
        BaseObject.__init__(self, KIND_DEPOT)
        self.position = topleft_position

    def on_drag_enter(self, mouse):
        if mouse.grabbed_obj and isinstance(mouse.grabbed_obj, Pole):
            mouse.interaction_option = InteractionOptions.Drop
            return
        BaseObject.on_drag_enter(self, mouse)

    def on_drag_leave(self, mouse, next_hover):
        mouse.interaction_option = InteractionOptions.NoDrop

    def on_drop(self, other):
        if isinstance(other, Pole):
            return True
        return False


class Field(BaseObject):

    def __init__(self, solution_poles, rect, integrity):
        BaseObject.__init__(self, KIND_FIELD)
        self.rect = rect
        self.poles = set()
        self.solution_poles = solution_poles
        # snap the coordinates to the grid
        for p in self.solution_poles:
            x, y = snap_to_grid(*p.position.as_xy_tuple())
            p.position.x = x
            p.position.y = y
        self._needles = []
        self.bg_img = None
        self._initialized = False
        self.touched = False
        self.integrity = integrity

    def init(self, bg_img, grid_step=25):
        self.bg_img = bg_img
        max_jitter_offset = 5
        r = pygame.Rect(0, 0, grid_step, grid_step)
        bounding_rect = self.rect.inflate(2 * max_jitter_offset, 2 * max_jitter_offset)
        for y in range(bounding_rect.top + grid_step - max_jitter_offset - 2, bounding_rect.top + bounding_rect.h,
                       grid_step):
            for x in range(bounding_rect.left + grid_step - max_jitter_offset - 2, bounding_rect.left + bounding_rect.w,
                           grid_step):
                offset = (0, 0)
                if random.random() > self.integrity: #< 0.95:  # todo tune
                    offset = (random.randint(-max_jitter_offset, max_jitter_offset),
                              random.randint(-max_jitter_offset, max_jitter_offset))
                r.center = (x, y)
                r.move_ip(*offset)
                self._needles.append(Needle(r.copy(), offset))
        self._update_needles(self.solution_poles)

        for n in self._needles:
            off_x, off_y = n.offset
            max_jitter_offset = max(abs(2 * off_x), abs(2 * off_y))
            r = pygame.Rect(0, 0, grid_step + max_jitter_offset, grid_step + max_jitter_offset)
            r.center = n.position
            r.move_ip(-bounding_rect.left, -bounding_rect.top)
            s = pygame.Surface(r.size, flags=pygame.SRCALPHA)
            s.blit(bg_img, (0, 0), r)
            solution_angle = n.angle
            if self.solution_poles:
                n.angle += random.randint(2, 350)  # randomize start angle
            n.set_image(s, 1.0, solution_angle)
            # n.set_image(resources.loaded_resources[resources.NEEDLE].data, 1.0, 0)  # todo debug option?
        self._initialized = True

    def _update_needles(self, others):
        if len(others) == 0:
            return
        for n in self._needles:
            # n.update(others)
            px, py = n.position
            fx = 0
            fy = 0
            for pole in others:
                ox, oy = pole.position.as_xy_tuple()  # todo use vector math
                ox, oy = snap_to_grid(ox, oy)
                of = pole.polarity
                dx = px - ox
                dy = py - oy
                d = dx * dx + dy * dy
                if d == 0:
                    continue
                fx -= of / d * (-px + ox)
                fy -= of / d * (py - oy)

            n.angle = int(math.degrees(math.atan2(fy, fx)))

    def update(self):
        self._update_needles(self.poles)

    def draw(self, screen):
        screen.set_clip(self.rect)

        if not self._initialized:
            return
        for n in self._needles:

            if n.angle != n._old_angle:
                n._old_angle = n.angle
                a = (n.angle - n.solution_angle) % 360

                choice = 0  # todo tune
                if choice == 0:
                    # alpha blended, very hard
                    n.image = pygame.Surface(n._image.get_size(), flags=pygame.SRCALPHA)
                    n.image.blit(n._image, (0, 0))
                    c = pygame.Color(255, 255, 255, 255)
                    h, s, v, _ = c.hsva
                    aa = 360 - a
                    if aa < 180:
                        alpha = 100 - int(aa / 180.0 * 100)
                    else:
                        alpha = int((aa - 180) / 180.0 * 100)
                    c.hsva = (h, s, v, alpha)
                    n.image.fill(c, None, pygame.BLEND_RGBA_MULT)  # todo maybe use another field for alpha?
                    n.image = pygame.transform.rotozoom(n.image, a, n.size)
                    n.rect = n.image.get_rect(center=n.position)
                elif choice == 1:
                    # alpha blended, very hard
                    n.image = pygame.Surface(n._image.get_size(), flags=pygame.SRCALPHA)
                    n.image.blit(n._image, (0, 0))
                    c = pygame.Color(255, 255, 255, 255)
                    h, s, v, _ = c.hsva
                    aa = 360 - a
                    alpha = int(aa / 360.0 * 100)
                    c.hsva = (h, s, v, alpha)
                    n.image.fill(c, None, pygame.BLEND_RGBA_MULT)  # todo maybe use another field for alpha?
                    n.image = pygame.transform.rotozoom(n.image, a, n.size)
                    n.rect = n.image.get_rect(center=n.position)

                elif choice == 2:
                    # rotational
                    n.image = pygame.Surface(n._image.get_size(), flags=pygame.SRCALPHA)
                    n.image.blit(n._image, (0, 0))
                    n.image = pygame.transform.rotozoom(n.image, a, n.size)
                    n.rect = n.image.get_rect(center=n.position)
                else:
                    raise Exception("unknown choice")

            screen.blit(n.image, n.rect)

        # if self.bg_img:
        # screen.blit(self.bg_img, self.rect)
        # pygame.draw.rect(screen, (255, 255, 255), self.rect, 1)

        screen.set_clip(None)
        return self.rect

    def on_focus(self, mouse):
        mouse.interaction_option = InteractionOptions.Default

    def on_focus_lost(self, mouse):
        mouse.interaction_option = InteractionOptions.Default

    def on_drag_enter(self, mouse):
        if mouse.grabbed_obj and isinstance(mouse.grabbed_obj, Pole):
            mouse.interaction_option = InteractionOptions.Drop
            self.poles.add(mouse.grabbed_obj)
            self.touched = True
            logger.debug("field drag enter pole %s", mouse.grabbed_obj)
            return
        BaseObject.on_drag_enter(self, mouse)

    def on_drag_leave(self, mouse, next_hover):
        mouse.interaction_option = InteractionOptions.NoDrop
        if not isinstance(next_hover, Pole):
            self.poles.discard(mouse.grabbed_obj)
        logger.debug("field drag leave pole %s", mouse.grabbed_obj)

    def on_drop(self, other):
        if isinstance(other, Pole):
            self.poles.add(other)
            self.touched = True
            logger.debug("field on_drop pole %s", other)
            return True  # drop failed, return true for success
        return False


class FieldSprite(GameSprite):
    draw_special = True
    dirty_update = False

    def __init__(self, obj):
        p = Point2(obj.rect.left, obj.rect.top)
        img = pygame.Surface((1, 1))  # todo replace with dummy
        GameSprite.__init__(self, img, p, obj, z_layer=FIELD_LAYER)

    def draw(self, surf, cam, renderer, interpolation_factor=1.0):
        return self.obj.draw(surf)


class SliderBackground(BaseObject):

    def __init__(self):
        BaseObject.__init__(self, KIND_SLIDER_BACKGROUND)

    def on_drag_enter(self, mouse):
        pass


class Slider(BaseObject):

    def __init__(self, rect, initial_value, min_value=0.0, max_value=1.0):
        BaseObject.__init__(self, KIND_SLIDER)
        self.rect = rect
        self.min_value = min_value
        self.max_value = max_value
        self.position = Point2(*self.rect.center)
        self.value = initial_value
        self.event_value_changed = Signal()
        self._set_value(initial_value)

    def _set_value(self, val):
        assert self.min_value <= val <= self.max_value, f"slider value out of bounds {self.min_value}<= {self.value} <= {self.max_value}"
        self.position.y = (1.0 - (val - self.min_value) / float(
            self.max_value - self.min_value)) * self.rect.h + self.rect.top
        self.event_value_changed.fire(self)

    def on_drag(self, mouse, x, y, dx, dy):
        self.position.y += dy
        if self.position.y > self.rect.bottom:
            self.position.y = self.rect.bottom
        if self.position.y < self.rect.top:
            self.position.y = self.rect.top

        self._update_value()

    def _update_value(self):
        self.value = (1.0 - (self.position.y - self.rect.top) / float(self.rect.h)) * (
                self.max_value - self.min_value) + self.min_value
        self.event_value_changed.fire(self)

    def on_drag_start(self, mouse, button):
        if button == 1:  # LMB
            mouse.interaction_option = InteractionOptions.Drag
            return True
        return False

    def on_drag_end(self, mouse):
        self._update_value()

    def on_focus(self, mouse):
        mouse.interaction_option = InteractionOptions.Grab

    def on_focus_lost(self, mouse):
        mouse.interaction_option = InteractionOptions.Default

    def on_drag_enter(self, mouse):
        pass


class View(object):

    def __init__(self, renderer, cam, tweener):
        self._cam = cam
        self.renderer = renderer
        self.tweener = tweener

    def add_sprite(self, sprite):
        self.renderer.add_sprite(sprite)

    def draw(self, screen, bg_color, do_flip):
        self.renderer.draw(screen, self._cam, bg_color, do_flip)

    def get_sprites_at_tuple(self, pos):
        return self.renderer.get_sprites_at_tuple(pos)

    def create_mouse_sprites(self, mouse):
        layer = 10000
        m_sprites = {}

        def _create_sprite(_res, _layer, sprites, option):
            offset = Vec2(*_res.offset)
            sprites[option] = GameSprite(_res.data, mouse.position, mouse, anchor=offset, z_layer=_layer)

        _create_sprite(resources.loaded_resources[resources.MOUSE_DEFAULT], layer, m_sprites,
                       InteractionOptions.Default)
        _create_sprite(resources.loaded_resources[resources.MOUSE_DRAG], layer, m_sprites, InteractionOptions.Drag)
        _create_sprite(resources.loaded_resources[resources.MOUSE_DROP], layer, m_sprites, InteractionOptions.Drop)
        _create_sprite(resources.loaded_resources[resources.MOUSE_NO_DROP], layer, m_sprites, InteractionOptions.NoDrop)
        _create_sprite(resources.loaded_resources[resources.MOUSE_CLICK], layer, m_sprites, InteractionOptions.Click)
        _create_sprite(resources.loaded_resources[resources.MOUSE_GRAB], layer, m_sprites, InteractionOptions.Grab)

        for _id, spr in m_sprites.items():
            spr.visible = False
            self.renderer.add_sprite(spr)
        m_sprites[InteractionOptions.Default].visible = True

        def _update_mouse_sprite(_mouse, old_option):
            m_sprites[old_option].visible = False
            m_sprites[_mouse.interaction_option].visible = True

        mouse.event_interaction_option_changed.add(_update_mouse_sprite)

    def create_pole_sprites(self, pole):
        POLE_ALPHA_DURING_DRAG = 100
        if pole.capability == POLE_CAPABILITY_NONE:
            res = resources.loaded_resources[resources.POLE1]
            spr = GameSprite(res.data, pole.position, pole, z_layer=POLE_LAYER, anchor=Vec2(*res.offset))
            # spr.zoom = abs(pole.polarity) + 0.1
            self.renderer.add_sprite(spr)

            def _drag_handler(_pole):
                spr.alpha = POLE_ALPHA_DURING_DRAG if _pole.is_dragging else 255

            pole.event_drag_changed.add(_drag_handler)

        elif pole.capability == POLE_CAPABILITY_FLIP:
            res = resources.loaded_resources[resources.POLE1]
            spr = GameSprite(res.data, pole.position, pole, z_layer=POLE_LAYER, anchor=Vec2(*res.offset))
            # spr.zoom = abs(pole.polarity) + 0.1
            self.renderer.add_sprite(spr)
            plus_spr = GameSprite(resources.loaded_resources[resources.POLE_PLUS].data, pole.position, pole,
                                  z_layer=POLE_LAYER + 1)
            neg_spr = GameSprite(resources.loaded_resources[resources.POLE_MINUS].data, pole.position, pole,
                                 z_layer=POLE_LAYER + 1)
            plus_spr.visible = True if pole.polarity > 0 else False
            neg_spr.visible = True if pole.polarity < 0 else False
            self.renderer.add_sprite(plus_spr)
            self.renderer.add_sprite(neg_spr)

            def _change_polarity(_pole):
                if _pole.polarity > 0:
                    plus_spr.visible = True
                    neg_spr.visible = False
                else:
                    plus_spr.visible = False
                    neg_spr.visible = True

            def _drag_handler(_pole):
                alpha = POLE_ALPHA_DURING_DRAG if _pole.is_dragging else 255
                spr.alpha = alpha
                plus_spr.alpha = alpha
                neg_spr.alpha = alpha

            pole.event_drag_changed.add(_drag_handler)

            pole.event_polarity_changed.add(_change_polarity)
        elif pole.capability == POLE_CAPABILITY_FULL:
            res = resources.loaded_resources[resources.POLE1]
            spr = GameSprite(res.data, pole.position, pole, z_layer=POLE_LAYER, anchor=Vec2(*res.offset))
            self.renderer.add_sprite(spr)
            display_res = resources.loaded_resources[resources.POLE_DISPLAY]
            display_spr = GameSprite(display_res.data, pole.position, pole,
                                     z_layer=POLE_LAYER + 1, anchor=Vec2(*display_res.offset))
            self.renderer.add_sprite(display_spr)
            lcd_font = resources.loaded_resources[resources.FONT_LCD].data
            text_spr = TextSprite(str(round(pole.polarity, 1)), pole.position, font=lcd_font, color=(0, 0, 0),
                                  z_layer=POLE_LAYER + 2)  # resources.loaded_resources[resources.POLE_PLUS].data, pole.position, pole, z_layer=POLE_LAYER+1)
            text_spr.obj = pole
            self.renderer.add_sprite(text_spr)

            def _change_polarity(_pole):
                if abs(_pole.polarity) > 9:
                    text_spr.text = "Err"
                else:
                    text_spr.text = f"{round(_pole.polarity, 1):+1.1f}"

            pole.event_polarity_changed.add(_change_polarity)

            def _drag_handler(_pole):
                alpha = POLE_ALPHA_DURING_DRAG if _pole.is_dragging else 255
                spr.alpha = alpha
                display_spr.alpha = alpha
                text_spr.alpha = alpha

            pole.event_drag_changed.add(_drag_handler)



class GameContext(BaseGameContext):

    def __init__(self, app, puzzle):
        BaseGameContext.__init__(self, app)
        self.puzzle = puzzle
        solution_poles = self.puzzle.solution_poles if puzzle.solved is False else []
        self.field = Field(solution_poles, pygame.Rect(178, 134, 800, 600), puzzle.integrity)
        self.poles = []
        self.renderer = DefaultRenderer()
        self.screen_rect = rect = self.app.screen.get_rect()
        self._cam = Camera(rect, Point2(rect.centerx, rect.centery))
        self.tweener = Tweener()
        self._view = View(self.renderer, self._cam, self.tweener)
        self._mouse = Mouse()
        self._back_ground_color = 0, 0, 0
        self._layout = None
        self.txt1_spr = None
        self.txt2_spr = None
        self.txt3_spr = None
        self.scheduler = Scheduler()
        self.timer = Timer(self.scheduler, 0.5, True, "display timer", logger)

    def enter(self):
        BaseGameContext.enter(self)

        f = resources.loaded_resources[resources.FONT_DISPLAY].data
        self.txt1_spr = TextSprite(self.puzzle.instruction1, Point2(175, 43), f, True, (0, 0, 0), anchor="topleft",
                                   z_layer=DISPLAY_FONT_LAYER)
        self.txt2_spr = TextSprite(self.puzzle.instruction2, Point2(175, 72), f, True, (0,0,0), anchor="topleft", z_layer=DISPLAY_FONT_LAYER)
        self.txt3_spr = TextSprite("", Point2(175, 101), f, True, (0,0,0), anchor="topleft", z_layer=DISPLAY_FONT_LAYER)
        self.txt1_spr.obj = self.txt2_spr.obj = self.txt3_spr.obj = BaseObject(-98)
        self.renderer.add_sprite(self.txt1_spr)
        self.renderer.add_sprite(self.txt2_spr)
        self.renderer.add_sprite(self.txt3_spr)

        def _update_txt(*args):
            self.txt3_spr.visible = not self.txt3_spr.visible

        self.timer.event_elapsed.add(_update_txt)

        self._layout = resources.loaded_resources[resources.LAYOUT].data
        self._view.add_sprite(
            GameSprite(self._layout, Point2(0, 0), BaseObject(-99), anchor='topleft', z_layer=BG_LAYER))

        x = 99
        y = 236 + 20
        dy = 50
        for _p in self.puzzle.available_poles:
            _p.position.x = x
            _p.position.y = y
            y += dy
            self.poles.append(_p)
            self._view.create_pole_sprites(_p)

        self.field.init(resources.loaded_resources[self.puzzle.resource_id].data)
        field_spr = FieldSprite(self.field)
        self._view.add_sprite(field_spr)
        frame_img = resources.loaded_resources[resources.FRAME].data
        spr = GameSprite(frame_img, Point2(*self.field.rect.center), self.field, z_layer=FRAME_LAYER)
        self._view.add_sprite(spr)

        fix_button = Button(Point2(942, 80), self._check_fix)
        btn_img = resources.loaded_resources[resources.BUTTON].data
        buttons_spr = GameSprite(btn_img, fix_button.position, fix_button, anchor='center', z_layer=BUTTON_LAYER)
        self._view.add_sprite(buttons_spr)

        slider_button_h = 22
        slider = Slider(pygame.Rect(142, 34 + slider_button_h // 2, 23, 94 - slider_button_h), 0, 0, 255)

        def _bg_u(_slider):
            self._back_ground_color = tuple([int(_slider.value)] * 3)

        slider.event_value_changed.add(_bg_u)
        slider_img = resources.loaded_resources[resources.SLIDERBUTTON].data
        slider_spr = GameSprite(slider_img, slider.position, slider, z_layer=SLIDER_LAYER)
        self._view.add_sprite(slider_spr)
        slider_bg = resources.loaded_resources[resources.SLIDERBACKGROUND].data
        slider_spr = GameSprite(slider_bg, Point2(*slider.rect.center), SliderBackground(), z_layer=SLIDER_BG)
        self._view.add_sprite(slider_spr)

        slider_volume = Slider(pygame.Rect(116, 34 + slider_button_h // 2, 23, 94 - slider_button_h), 0.5, 0, 1.0)

        def _change_volume(_slider):
            self.app.music_player.volume = _slider.value

        slider_volume.event_value_changed.add(_change_volume)
        slider_img = resources.loaded_resources[resources.SLIDERBUTTONVOLUME].data
        slider_spr = GameSprite(slider_img, slider_volume.position, slider_volume, z_layer=SLIDER_LAYER)
        self._view.add_sprite(slider_spr)
        slider_bg = resources.loaded_resources[resources.SLIDERBACKGROUND].data
        slider_spr = GameSprite(slider_bg, Point2(*slider_volume.rect.center), SliderBackground(), z_layer=SLIDER_BG)
        self._view.add_sprite(slider_spr)

        # 73, 57   on/off button center
        on_off_button = Button(Point2(73, 57), self._quit)
        btn_img = resources.loaded_resources[resources.OFFBUTTON].data
        buttons_spr = GameSprite(btn_img, on_off_button.position, on_off_button, anchor='center', z_layer=BUTTON_LAYER)
        self._view.add_sprite(buttons_spr)
        # btn_text_spr = TextSprite("on/off", on_off_button.position, anchor='center', z_layer=BUTTON_LAYER + 1)
        # btn_text_spr.obj = on_off_button
        # self._view.add_sprite(btn_text_spr)

        depot = Depot(Point2(47, 236))
        img = resources.loaded_resources[resources.DEPOT].data
        depot_spr = GameSprite(img, depot.position, depot, anchor="topleft", z_layer=DEPOT_LAYER)
        self._view.add_sprite(depot_spr)

        self._view.create_mouse_sprites(self._mouse)
        pygame.mouse.set_visible(False)

    def _check_fix(self):
        # check win/loose condition
        current = [(*snap_to_grid(p.position.x, p.position.y), p.polarity) for p in self.field.poles]
        solution = [(p.position.x, p.position.y, p.polarity) for p in self.field.solution_poles]
        solution.sort()
        current.sort()
        win = solution == current

        if win:
            logger.info("WIN WIN WIN")
            self.puzzle.solved = True
            self.hide_all_poles()
            self.txt3_spr.text = "Fixed"
            self.txt3_spr.visible = True
            self.timer.stop()
            self.field.touched = False  # todo: hack!
        else:
            logger.info("Try again!")
            self.puzzle.solved = False
            self.txt3_spr.text = "Error"
            self.timer.start()

    def hide_all_poles(self):
        for spr in self.renderer.get_sprites():
            if hasattr(spr, 'obj'):
                if isinstance(spr.obj, Pole):
                    self.tweener.create_tween_by_end(spr, "alpha", 255, 0, 1.0)

    def exit(self):
        pygame.mouse.set_visible(True)
        BaseGameContext.exit(self)

    def draw(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0):
        screen = self.app.screen
        screen.fill(self._back_ground_color, self.field.rect)
        self._view.draw(screen, None, do_flip)

    def update_step(self, delta_time, sim_time, *args):
        self.tweener.update(delta_time)
        self.scheduler.update(delta_time, sim_time)
        self._handle_events()
        self.field.update()

    def _handle_events(self):
        BaseGameContext.update_music_player(self)  # this has to be called before getting all events
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self._quit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self._quit()
            elif event.type == pygame.MOUSEMOTION:
                # pos, rel, buttons
                objects = self._get_objects_at(event.pos)
                if event.buttons != (0, 0, 0):
                    # drag!
                    self._mouse.on_drag(event.pos, event.rel, event.buttons, objects)
                else:
                    self._mouse.check_hover(objects, event.pos)

            elif event.type == pygame.MOUSEBUTTONDOWN:
                logger.debug(f"py mB down {event.button}")
                objects = self._get_objects_at(event.pos)
                self._mouse.press(event.button, event.pos, objects)
            elif event.type == pygame.MOUSEBUTTONUP:
                logger.debug(f"py mB up {event.button}")
                objects = self._get_objects_at(event.pos)
                self._mouse.release(event.button, event.pos, objects)

    def _get_objects_at(self, pos):
        sprites = self._view.get_sprites_at_tuple(pos)
        objects = [s.obj for s in sprites]
        objects = list(OrderedDict.fromkeys(objects))  # make them distinct but keep order
        return objects

    def _quit(self):
        if self.field.touched:
            self.puzzle.solved = False
        self.pop()


logger.debug("imported")
