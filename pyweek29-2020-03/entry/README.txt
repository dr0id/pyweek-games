﻿Magnetic Pictures
=================

CONTACT:

Homepage: https://pyweek.org/e/pw---_-/
Name: Magnetic Pictures
Team: pw---_-
Members: DR0ID


DEPENDENCIES:

You might need to install some of these before running the game:

  Python:     http://www.python.org/
  PyGame:     http://www.pygame.org/

Tested with python 3.6 and pygame 1.9.4


RUNNING THE GAME:

On Windows or Mac OS X, locate the "run_game.pyw" file and double-click it.

Othewise open a terminal / console and "cd" to the game directory and run:

  python run_game.py



HOW TO PLAY THE GAME:

Move the cursor around the screen with the mouse.

You need a mouse with left- and right-button and a scroll wheel.



LICENSE:

This game skellington has same license as pyknic.

