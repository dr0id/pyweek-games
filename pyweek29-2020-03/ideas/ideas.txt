Themes
------
1. Print, erase
2. The butterfly effect
3. Riders on the Storm
4. Did we get it?
5. Frictionless

1. Print, erase
---------------

Doodle game where the paint erases after some time.
For inspiration: https://www.youtube.com/watch?v=I6IqoSAtjb0



2. The Butterfly effect
-----------------------
2.1 Butter-fly (effect)  -> catapult shooting butters...
2.2 time jumping space battle game
2.3 Magnetic photo: restore them by placing the right magnet at the right spot (small change, big effect
    misunderstanding!)


3. Riders on the Storm
----------------------
Balloon game idea (see pw 28 notes): ride on the storm to get faster from A to B


4. Did we get it?
-----------------
4.1 Did we get it?
We could have a bomb hell game where things come at you from all angles as you run around bombing the place. And of course there is so much flash and smoke you can't see if you got it until the action is over. :slight_smile:
or so much JUICE

4.2 Hunt game? Monster slaughter game, by weapons, traps, jobs give money & fame & reward
4.3 avoid getting it. Plague like shooter: Missions:
        Move from A to B without getting it
        Kill all infected
        get components to create antidote


5. Fictionless
--------------
5.1 Air Hockey like game where you have to pass the puck through obstacles

