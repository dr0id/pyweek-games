TO DO
=====

Logic
-----
- [DONE] World or level class. It handles or wraps:
  - load the map
  - make entities and sprites
  - create and monitor the mission objectives
- [DONE] ground
- [DONE] air above ground: smog vs. grades of clear
- [    ] cloud layer: can walk or float on it? if so, how to get out or fall through it?
- [DONE] missions: see Maps
- (if we have time)
  - [    ] player can place a trampoline?
- [DONE] impose a small time gap between mission speech
- [DONE] win event, game ending


User Interface
--------------
- [DONE] With a screen size of 1024x768 and display ration 3:4, our dimension will be:
  - 768 * 0.75 = 576
  - (1024-576, 768) = (margin:576, w:448, h:768)
- [DONE] jumping and edge-of-the-world gaga
- [WONT] main message area w/ juices

notes below [1]


Maps
----
Note: check map loader from pw22

Decision:
A: One big map, with missions that accumulate the player's effect on the world?
B: Multiple small maps, each with its own mission(s)?

Map layers:
0. horizon
1. background
2. foreground
2. objects
3. smog

Missions:
1. [DONE] Use all five trampolines.
2. [DONE] Get above the smog layer, into the clean air.
3. [DONE] Collect some clean air and clear away the smog around [someone/thing].
4. [DONE] Collect birdies
5. [DONE] Boss mission


Graphics
--------
- [DONE] trampolines with visual angle
- [TODO] smog patches fx
- [DONE] clean air trail fx
- [DONE] collectibles
- [Done] images TBD
- [TODO] juicy anims TBD


Sounds
------
- [DONE] moody sounds: smoggy vs. happy


Music
-----
- [DONE] moody music: smoggy vs. happy


[1]: from the chat

7[20:26]	DR0ID_: I have been thinking what should happen if the player misses the ground..... option 1: die and restart mission  option 2: just add a trampolin effect so he jumps up on the neares platform (maybe with a warning or something)
8[20:26]	Gummbum: lol
7[20:26]	DR0ID_: I would prefer option
7[20:26]	DR0ID_: 2
7[20:26]	DR0ID_: you?
8[20:26]	Gummbum: yes!
7[20:26]	DR0ID_: "this was too low"
7[20:27]	DR0ID_: "no, not going to the underworld"
8[20:27]	Gummbum: I placed a couple signs for you to add your gag messages
7[20:27]	DR0ID_: "uh I didn't like nirvana that much"
7[20:28]	DR0ID_: "uh didn't like to stay at nirvana too long"
8[20:28]	Gummbum: on the edge of the world, there is a sign on each end
8[20:28]	Gummbum: "yikes. nirvana is not so nirvana."
