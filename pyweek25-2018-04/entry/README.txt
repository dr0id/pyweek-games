﻿# CoDoGuPywk25

Team entry for Pyweek 25

* Homepage: https://pyweek.org/e/codogupywk25/
* Team CoDoGuPywk25
* Game name: Where have all the birds, flowers, white clouds, blue skies gone?
* Members: DR0ID, gummbum, Axiom(aka Conjecture)
* Theme: Two Worlds

## Requirements

* Python 2.7 or Python 3.x
* pygame 1.9
* Modern CPU (if your computer is wimpy see how to disable fog FX below)

## Support

If you run into trouble playing our game, we would like to help. Please
visit IRC @freenode.net, channels #pygame and #pyweek. There you will
find DR0ID_, Gummbum, and Conjecture.

## How To Play

### Starting the game

The game is bundled as a Gzip'ed TAR file. You will need an archive
utility that can handle this format. For example 7z, or its predecessor
7zip. Unpack the archive into its own folder or directory.

On Microsoft Windows find run.pyw and double click it.

On other platforms open a shell and use the command: python run.py.

### Playing the game

The game uses the cursor keys to control the hero.

* UP: Jump
* LEFT, RIGHT: Walk or float horizontally

#### Play Guide

There are five missions. Each new mission announces the objectives in
big print, top center. If you miss it don't worry. Just walk to the
trampolines that lead upwards to the objective. At first it is smoggy
so you may accidentally trip over a trampoline. Watch your step. :)

Once the mission starts, the hero's banter offers some background. You
may want to pause where you are (or just fall, you can't be hurt) and
read the speech bubbles. They are not necessary to progressing, but you
will miss out on some of the game's charm if you ignore them.

Missions sometimes have collection objectives. The collectibles can be
taken one by one down to the surface to dispell the smog in stages.
If you spend some effort you'll eventually clear the smog, so the air
will no longer taste like a dirty sock, and you'll be able to see
better. But if you just collect all of them at once and descend, the
thinning smog will still provide some relief.

Don't miss out on the unique experience of jumping off the edge of the
world!

#### Concept

The theme Two Worlds is woven into the background story and the game
play. There are two worlds: everyday people and the hoity toity. Two
worlds: a polluted surface and the clean heavens. Two worlds: the world
as a child sees it, and the mysterious adult world. Two worlds: the
known and the alien.

#### Trivia (or maybe not)

The hero mentions a few, possibly little known, cultural and "business"
phenomena.

_It's the Corpa Talker, see._ If you need to google "Corporatocracy"
you've been sleeping. :)

_What if it rains cats and dogs? Now I know how they got up here._
"Raining cats and dogs" is an old English idiom, so stormy the winds
swept up the household pets and rained them down. Storms have been
known to sweep up and rain down small critters, such as toads. Little
children do not yet understand advanced literary devices such as idioms.
The resulting misunderstandings can be quite comical.

_Once I ate a Tide pod._ Google "Tide pod challenge".

_Nobody would eat a big fat juicy spider._ Google "Tarantula burger".

_And why [do] they make the air dirty._ Google "geoengineering".

_WE have to clean our own backyard._ You can't google "climate change
bad science", or "weather industrial complex", or "climate control for
profit". Google censors balanced opinions, even if they come from
credentialed climate and weather experts. But you can read UN Agenda 21
and the Sustainable Development Goals, learn by reading between the
lines, and investigate how the goals are being implemented in your
city, county and state.

_Corpa Talkers are weird. Like another planet._ The Corporatocracy, the
legal fictions and the people who breathe life into them, are indeed
very, very weird. They are not like you and me. It may be prudent not to
lift that lid too high. Some things can't be unseen.
