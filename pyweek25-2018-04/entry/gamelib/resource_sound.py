# -*- coding: utf-8 -*-
import logging
from pyknic.generators import IdGenerator
from gamelib import settings
from gamelib.sfx import MusicData, SoundData


logger = logging.getLogger(__name__)


def play_sound(idx, queue=False):
    settings.sfx_handler.handle_message(None, None, idx, 'queue' if queue else None)


def play_song(idx):
    settings.sfx_handler.handle_message(None, None, idx, None)


_gen_id = IdGenerator(1).next

SFX_PLAYER_JUMP = _gen_id()
# SFX_PLAYER_FALL = _gen_id()
SFX_PLAYER_LAND = _gen_id()
SFX_TRAMPOLINE1 = _gen_id()
SFX_TRAMPOLINE2 = _gen_id()
SFX_TRAMPOLINE3 = _gen_id()
sfx_trampolines = (
    SFX_TRAMPOLINE1,
    SFX_TRAMPOLINE2,
    SFX_TRAMPOLINE3
)

SONG_FACTORY_REMEMBER = _gen_id()


sfx_data = {
    SFX_PLAYER_JUMP: SoundData('NYX_JUMP4.ogg', 0.5, reserved_channel_id=settings.channel_jumping),
    # SFX_PLAYER_FALL: SoundData('falling.ogg', 0.3, reserved_channel_id=settings.channel_jumping),
    SFX_PLAYER_LAND: SoundData('NYX_LAND3.ogg', 0.5, reserved_channel_id=settings.channel_jumping),
    SFX_TRAMPOLINE1: SoundData('boing1.ogg', 0.3, reserved_channel_id=settings.channel_trampoline),
    SFX_TRAMPOLINE2: SoundData('boing2.ogg', 0.3, reserved_channel_id=settings.channel_trampoline),
    SFX_TRAMPOLINE3: SoundData('boing3.ogg', 0.3, reserved_channel_id=settings.channel_trampoline),

    SONG_FACTORY_REMEMBER: MusicData('Factory - Remember.ogg', 1.0),
}
