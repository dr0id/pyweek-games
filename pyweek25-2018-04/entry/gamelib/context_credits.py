# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'context_credits.py' is part of CoDoGuPywk25
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]                    # For instance:
                   |                                  * 1.2.0.1 instead of 1.2-a
                   +-|* 0 for alpha (status)          * 1.2.1.2 instead of 1.2-b2 (beta with some bug fixes)
                     |* 1 for beta (status)           * 1.2.2.3 instead of 1.2-rc (release candidate)
                     |* 2 for release candidate       * 1.2.3.0 instead of 1.2-r (commercial distribution)
                     |* 3 for (public) release        * 1.2.3.5 instead of 1.2-r5 (commercial dist with many bug fixes)


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division

import logging
import random

import pygame

from gamelib import settings
from gamelib.animated import Animated
from gamelib.ptext_sprite import PtextSprite
from pyknic.context import Context
from pyknic.mathematics import Point2
from pyknic.pygame_wrapper.spritesystem import Camera, DefaultRenderer
from pyknic.tweening import Tweener, ease_out_sine

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2018"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 

logger = logging.getLogger(__name__)
logger.debug("importing...")


class Entity(Animated):

    def __init__(self, position, frames, fps):
        Animated.__init__(self, position, frames, fps)


class ContextCredits(Context):

    def __init__(self):
        Context.__init__(self)
        self.cam = Camera(pygame.Rect(0, 0, settings.screen_width, settings.screen_height))
        self.cam.position = Point2(settings.screen_width // 2, settings.screen_height // 2)
        self.renderer = DefaultRenderer()
        self.tweener = Tweener()

    def enter(self):
        pygame.event.clear()
        img1 = pygame.image.load("data/gfx/platformer-pack-redux-360-assets/PNG/Tiles/spring.png").convert_alpha()
        img1 = pygame.transform.smoothscale(img1, (32, 32)).convert_alpha()
        img2 = pygame.image.load("data/gfx/platformer-pack-redux-360-assets/PNG/Tiles/sprung.png").convert_alpha()
        img2 = pygame.transform.smoothscale(img2, (32, 32)).convert_alpha()

        trampoline_frames = [img1, img2]

        delay = 1
        fade_duration = 4
        sw, sh = settings.screen_size
        dx = 1.0 / 6.0
        self._crate_trampoline(1 * dx * sw, 0.5 * sh, delay, fade_duration, trampoline_frames, "DR0ID")
        self._crate_trampoline(2 * dx * sw, 0.9 * sh, delay, fade_duration, trampoline_frames, "Gummbum")
        self._crate_trampoline(3 * dx * sw, 0.8 * sh, delay, fade_duration, trampoline_frames, "Pyweek25")
        self._crate_trampoline(4 * dx * sw, 0.6 * sh, delay, fade_duration, trampoline_frames, "Conjecture")
        self._crate_trampoline(5 * dx * sw, 0.9 * sh, delay, fade_duration, trampoline_frames, "April 2018")

        tardis_img = pygame.image.load('data/gfx/misc/tardis_by_queennekoyasha-36x64.png')
        tardis_img = pygame.transform.scale2x(tardis_img)
        spr = Animated(Point2(0.5 * sw, 0.3 * sh), [tardis_img], 0)
        self.renderer.add_sprite(spr)
        self.tweener.create_tween(spr, 'alpha', 0, 255, 3, cb_end=self._tardis_fade)

        _fog_imgs = pygame.image.load("data/gfx/misc/fog01.png").convert_alpha()
        spr = Animated(Point2(0.5 * sw, sh), [_fog_imgs], 0, 'midbottom')
        spr.z_layer = 10
        self.renderer.add_sprite(spr)

    def _tardis_fade(self, tween):
        spr = tween.o
        start = 255 if spr.alpha > 0 else 0
        change = -255 if spr.alpha > 0 else 255
        self.tweener.create_tween(spr, 'alpha', start, change, 3, cb_end=self._tardis_fade)

    def _crate_trampoline(self,x, y, delay, fade_duration, trampoline_frames, text):
        tramp = Entity(Point2(x, y), trampoline_frames, 4)
        self._create_delayed_fade(tramp, delay, fade_duration)
        tramp._dirty_sprites.add(tramp)
        self.renderer.add_sprite(tramp)

        self._create_text(x, y, text, delay, fade_duration)

    def _create_delayed_fade(self, entity, delay, fade_duration):
        self.tweener.create_tween(entity, "alpha", 0, 255, fade_duration, delay=delay, immediate=True)

    def update(self, delta_time, sim_time):
        self.tweener.update(delta_time)
        Animated.scheduler.update(delta_time, sim_time)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.pop()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self.pop()

    def draw(self, screen, do_flip=True, interpolation_factor=1.0):
        sky_blue = (70, 169, 244)
        self.renderer.draw(screen, self.cam, sky_blue, True)

    def _create_text(self, x, y, text, delay, fade_duration):
        spr = PtextSprite(text, Point2(x, y - 20), **settings.font_themes['mission_splash'])
        self._create_delayed_fade(spr, delay, fade_duration)
        self.renderer.add_sprite(spr)
        d = random.random() * 2
        self.jump_duration = 0.5
        self.tweener.create_tween(spr.position, "y", spr.position.y, -100, self.jump_duration, ease_out_sine, cb_end=self._end, delay=d)

    def _end2(self, tween, **kwargs):
        self.tweener.create_tween(tween.o, "y", tween.o.y, -100, self.jump_duration, ease_out_sine, cb_end=self._end )

    def _end(self, *args, **kwargs):
        tween = args[0]
        self.tweener.create_tween(tween.o, "y", tween.o.y, 100, self.jump_duration, ease_out_sine, cb_end=self._end2 )


logger.debug("imported")
