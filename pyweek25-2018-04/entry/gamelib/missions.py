# -*- coding: utf-8 -*-

import logging

from gamelib.settings import MISSION_STATUS_PENDING, MISSION_STATUS_IN_PROGRESS, MISSION_STATUS_COMPLETE
from pyknic.pygame_wrapper.spritesystem import TextSprite

logger = logging.getLogger(__name__)


class _Mission(object):
    number = 0
    name = ''
    description = ''
    status = MISSION_STATUS_PENDING
    achieved = 0
    needed = 1

    def __init__(self,):
        self.status = MISSION_STATUS_IN_PROGRESS
        self.speech = []

        try:
            speech_file = 'data/text/speech{}.txt'.format(self.number)
            with open(speech_file) as f:
                for s in f:
                    s = s.rstrip()
                    self.speech.append(s)
        except IOError:
            pass

    def is_complete(self):
        """test if mission objective is completed"""
        if not self.status == MISSION_STATUS_COMPLETE:
            if self.achieved >= self.needed:
                self.status = MISSION_STATUS_COMPLETE
        return self.status == MISSION_STATUS_COMPLETE

    def on_trampoline_hit(self, trampoline):
        """advance mission objective: trampoline"""
        pass

    def on_reach_the_clouds(self, cloud):
        """advance mission objective: ascend to the clouds"""
        pass

    def on_get_some_air(self, air):
        """advance mission objective: get some air"""
        pass

    def on_catch_a_sunbird(self, birdy):
        """advance mission objective: catch and release a sunbird"""
        pass

    def on_reach_the_boss(self, boss):
        """advance mission objective: reach the boss"""
        pass


class _Mission1(_Mission):
    """use five trampolines"""
    number = 1
    name = 'Mission 1'
    description = 'Use Five (5) Trampolines'
    needed = 5

    def __init__(self):
        super(_Mission1, self).__init__()
        self.visited = []

    def on_trampoline_hit(self, trampoline):
        """check if trampoline was visited: if not, mark it and increment the mission progress"""
        if trampoline not in self.visited:
            self.visited.append(trampoline)
            self.achieved = len(self.visited)
            logger.info('>>> trampoline {} of {} hit', self.achieved, self.needed)
            return '#' + str(self.achieved)


class _Mission2(_Mission):
    """reach the clouds"""
    number = 2
    name = 'Mission 2'
    description = 'Reach The Clouds Four (4) Times'
    needed = 4

    def on_reach_the_clouds(self, cloud):
        """increment mission progress"""
        if cloud.amount > 0:
            cloud.amount -= 1
            self.achieved += 1
            return '#' + str(self.achieved)


class _Mission3(_Mission):
    """get some air"""
    number = 3
    name = 'Mission 3'
    description = 'Get Some (20) Air'
    needed = 20

    def on_get_some_air(self, air):
        """increment mission progress"""
        if air.amount > 0:
            air.amount -= 1
            self.achieved += 1
            return '#' + str(self.achieved)


class _Mission4(_Mission):
    """catch and release a sunbird"""
    number = 4
    name = 'Mission 4'
    description = 'Catch And Release Five (5) Sunbirds'
    needed = 5

    def on_catch_a_sunbird(self, birdy):
        if birdy.amount > 0:
            birdy.amount -= 1
            self.achieved += 1
            return '#' + str(self.achieved)


class _Mission5(_Mission):
    """face the corporatocracy"""
    number = 5
    name = 'Mission 5'
    description = 'Face the Corporatocracy'
    needed = 1

    def on_reach_the_boss(self, boss):
        if boss.amount > 0:
            boss.amount -= 1
            self.achieved += 1
            return '#' + str(self.achieved)


class Missions(object):
    def __init__(self):
        self.mission_idx = -1
        self.missions = _Mission1, _Mission2, _Mission3, _Mission4, _Mission5
        self.current_mission = None

        self.finished = False

    def new_mission(self, idx=None):
        """start a new mission

        :param idx: mission number (the mission advances if None)
        :return: None
        """
        if idx is None:
            idx = self.mission_idx + 1
        self.mission_idx = idx
        if idx > len(self.missions) - 1:
            self.finished = True
        else:
            self.current_mission = self.missions[idx]()

    def next_mission(self):
        self.new_mission()

    # expose _Mission* data and methods

    def current_name(self):
        return self.current_mission.name

    def current_description(self):
        return self.current_mission.description

    def current_needed(self):
        return self.current_mission.needed

    def current_achieved(self):
        return self.current_mission.achieved

    def current_status(self):
        return self.current_mission.status

    def current_is_complete(self):
        return self.current_mission.is_complete()

    # safe to call these every time the event occurs

    def on_trampoline_hit(self, trampoline):
        """advance mission objective: trampoline"""
        return self.current_mission.on_trampoline_hit(trampoline)

    def on_reach_the_clouds(self, cloud):
        """advance mission objective: ascend to the clouds"""
        return self.current_mission.on_reach_the_clouds(cloud)

    def on_get_some_air(self, air):
        """advance mission objective: get some air"""
        return self.current_mission.on_get_some_air(air)

    def on_catch_a_sunbird(self, birdy):
        """advance mission objective: catch and release a sunbird"""
        return self.current_mission.on_catch_a_sunbird(birdy)

    def on_reach_the_boss(self, boss):
        """advance mission objective: catch and release a sunbird"""
        return self.current_mission.on_reach_the_boss(boss)
