# -*- coding: utf-8 -*-
import sys
import time

import pygame
from pygame import Rect


__version__ = '3.0.0'
__author__ = 'Gummbum, (c) 2017'
__vernum__ = tuple(int(s) for s in __version__.split('.'))
__all__ = ['CompositeBuffer', 'CompositeTile', 'default_sort_key']


if sys.version_info[0] == 3:
    xrange = range


def default_sort_key(s):
    """sprite sort key used by get_tiles() to order the tiles for compositing"""
    r = s.rect
    # rect.bottom, rect.x
    return r[1] + r[3], r[0]


class CompositeBuffer(object):
    """utility class that builds and refreshes composite tiles from source sprites
    
    One or more CompositeBuffer's can be maintained for layers of sprites that can be combined to save blit() overhead.
    
    The space is plotted in arbitrary coordinates. This is taken from the rect passed to get_tiles(area, src_sprites).
    The source sprites are assumed to exist in the same coordinate space as the area rect. Conversion to screen space
    must be handled for blitting (see the next example).
    
    The source tiles must be a sequence of SpatialHash. A small setup might be:
    
        composite_buffer = CompositeBuffer(64)
        source_tiles = (
            SpatialHash(64),    # background, ground
            SpatialHash(64),    # surface, mobile, interactive
            SpatialHash(64),    # over
        )
        load_sprites_into_layers(source_tiles)  # hypothetical sprite loader / factory
        screen = pygame.display.get_surface()
        camera = screen.get_rect().move(1000, 1000)
        on_screen_tiles = composite_buffer.get_tiles(camera, source_tiles)
        for tile in sprites_sorted:
            screen.blit(tile.image, tile.rect.move(-camera.x, -camera.y))
    
    Part of the logic must track when a mobile or mutable sprite has changed, and mark the CompositeBuffer dirty. A
    crude example:
    
        dirty_rects = []
        for sprite in sprites:
            dirty = sprite.update()
            if dirty:
                dirty_rects.append(sprite.rect)
        composite_buffer.set_dirty(dirty_rects)
    
    You may want to occasionally prune your tile space by age:
    
        five_minutes = 60 * 5
        composite_buffer.prune(five_minutes)
    
    But take note. After a long pause all the tiles might be stale. You can touch the tiles to make the fresh before
    resuming your normal aging schedule:
    
        for tile in composite_buffer.tiles():
            tile.touch()
    
    The tile_size specifies how wide and tall the backing tiles will be. They are square in shape. The smaller the size,
    more blits will result. The larger the size, the more pixel overage and culling will result. A balance would be
    desired: something larger than the "busiest" tile, and less than or equal to one eighth of the screen width. It is
    recommended that the SpatialHash'es containing the source sprite layers use the same size: SpatialHash(size), but
    not required.
    
    The fetch_ahead tunable pads the area in get_tiles() by this amount on every side. Some use cases want to enough
    tiles to accommodate some sliding of the camera, such as is needed when smoothing a scrolling area by interpolation.
    One could manage a second camera for this purpose, but fetch_ahead can automatically adjust for this.
    
    The cache_max_px tunables constrain the size of the cache. The unit is pixels. Some cache is beneficial, as tile
    surfaces can be reused. The defaults (None) cause the calculation to be taken from the display. This would cache up
    to nine full screens of tiles for reuse. To disable caching entirely, choose something really tiny like 1
    (0 < value < tile_size). For effectively unlimited cache, choose something really huge like a billion
    (1 Gb = 2 ** 30; width * height * surf.bytesize() will roughly determine the memory consumption).
    
    The make_surf callable is used to create a custom tile surface, and to initialize it when it is reclaimed from the
    cache. Example usage: if the composites are to be a transparent overlay, or a specific bitsize, or have some kind of
    procedurally controlled appearance. The callable must have the form make_surf(surf, size), and return a surface of
    that size. If make_surf is None, tiles are created with the same characteristics as the display. Here is a function
    that creates and initializes a transparent overlay:

        def make_overhead_surf(surf, size):
            if not surf:
                surf = pygame.Surface(size, flags=pygame.SRCALPHA)
            surf.fill((0, 0, 0, 0), special_flags=pygame.BLEND_RGBA_MIN)
            return surf

    
    The sort_key callable is used to sort the source tiles so they render in a proper order. The default_sort_key
    function sorts them by the tuple (rect.bottom, rect.x).
    """

    def __init__(self, tile_size=64, inflate=0, cache_max_px=None, make_surf=None, sort_key=default_sort_key):
        """create a composite buffer
        
        :param tile_size: int; width and height of the composite tiles
        :param inflate: int; extra pixels to include on each border (e.g. if the area is subject to interpolation)
        :param cache_max_px: int; cap cached surface at this total number of pixels (e.g. screen width * height)
        :param make_surf: callable; called as make_surf((w, h)), must return a surface
        :param sort_key: callable; used by sorted() builtin to sort the source tiles
        """
        self._tile_size = tile_size
        self._make_surf = make_surf

        self._tiles = {}
        self._cache = []
        self._cache_px = 0

        screen_width, screen_height = pygame.display.get_surface().get_size()

        # Tunable
        self.inflate = inflate
        self.cache_max_px = cache_max_px or (screen_width * screen_height) * 9
        self.sort_key = sort_key

        # Metrics
        self.num_new = 0
        self.num_dirty = 0
        self.num_tiles_rendered = 0
        self.num_sprites_rendered = 0
        self.num_returned = 0
        self.num_pruned = 0

    @property
    def num_cached(self):
        """number of tiles in the cache"""
        return len(self._cache)

    @property
    def cache_size(self):
        """cache size in pixels"""
        return self.num_cached * 2 * self._tile_size

    def tiles(self):
        """return all tiles as an iterator"""
        return iter(self._tiles.values())

    def set_dirty(self, areas):
        """specify areas to re-render
        
        The areas argument must be a sequence of pygame.Rect. The tiles that intersect with the areas will be
        re-rendered the next time get_tiles() selects tiles in the area.
        """
        def sort_key(r):
            return r[1] + r[3], r[0]
        self__tiles = self._tiles
        self__remove = self._remove
        self__tile_size = self._tile_size
        num_dirty = 0
        prev_area = None
        for rect in sorted(areas, key=sort_key):
            if rect == prev_area:
                continue
            prev_area = rect
            x_start = rect[0] // self__tile_size
            x_end = (rect[0] + rect[2]) // self__tile_size
            y_start = rect[1] // self__tile_size
            y_end = (rect[1] + rect[3]) // self__tile_size
            y_range = xrange(y_start, y_end + 1)
            for x in xrange(x_start, x_end + 1):
                for y in y_range:
                    idx = x, y
                    if idx in self__tiles:
                        self__remove(idx)
                        num_dirty += 1
        self.num_dirty += num_dirty

    def _remove(self, idx):
        """remove a tile and add it to cache"""
        self._cache_add(self._tiles.pop(idx))

    def _cache_add(self, tile):
        """add a tile to cache"""
        px = self._tile_size * 2
        if self._cache_px + px <= self.cache_max_px:
            self._cache.append(tile)
            self._cache_px += px

    def _cache_get(self):
        """get a tile from cache"""
        tile = None
        if self._cache:
            tile = self._cache.pop()
            px = self._tile_size * 2
            self._cache_px -= px
            if self._make_surf:
                self._make_surf(tile.image, None)
            else:
                tile.image.fill((0, 0, 0))
        return tile

    def get_tiles(self, area, getters, inflate=None, sort_key=None):
        """get the composite tiles within area

        The getters argument is a seq of methods. They are called in sequence order as getters[i](area). Each must
        return a sequence of sprites. The sprites will be rendered on the tile in order like so:

            for get_sprites in getters:
                sprites = get_sprites(area)
                for sprite in sorted(sprites, sort_key):
                    tile_surf.blit(sprite.image, sprite.rect...)

        :param area: pygame.Rect; the space within which to get tiles
        :param getters: seq of methods to get sprites in area
        :param inflate: int; override self.inflate; see class docs for details
        :param sort_key: callable; override self.sort_key; see class docs for details
        :return: list of tiles
        """
        result = []

        if inflate is None:
            inflate = self.inflate
        inflate *= 2

        if sort_key is None:
            sort_key = self.sort_key

        self__tiles = self._tiles
        self__tiles__get_tile = self__tiles.get
        self__tile_size = self._tile_size

        x, y, w, h = area.inflate(inflate, inflate) if inflate else area
        right = (x + w) // self__tile_size
        bottom = (y + h) // self__tile_size
        x = x // self__tile_size
        y = y // self__tile_size

        num_new = 0
        num_tiles_rendered = 0
        num_sprites_rendered = 0

        y_range = xrange(y, bottom + 1)
        for x in xrange(x, right + 1):
            for y in y_range:
                idx = x, y
                tile = None
                if idx in self__tiles:
                    # tile found, already rendered
                    tile = self__tiles__get_tile(idx)
                else:
                    # get the sprites within this tile's area
                    layers = []
                    rect = Rect(x * self__tile_size, y * self__tile_size, self__tile_size, self__tile_size)
                    for get_sprites in getters:
                        layer = get_sprites(rect)
                        if layer:
                            num_sprites_rendered += len(layer)
                            layers.append(layer)
                    # if there are sprites, get a tile to render them on...
                    if layers:
                        num_tiles_rendered += 1
                        tile = self._cache_get()
                        if tile:
                            tile.init(idx, layers, sort_key)
                            self__tiles[idx] = tile
                        else:
                            num_new += 1
                            tile = CompositeTile(idx, layers, self__tile_size, sort_key, self._make_surf)
                            self__tiles[idx] = tile
                if tile:
                    tile.touch()
                    result.append(tile)

        self.num_new = num_new
        self.num_tiles_rendered = num_tiles_rendered
        self.num_sprites_rendered = num_sprites_rendered
        self.num_returned = len(result)
        self.num_dirty = 0

        return result

    def prune(self, age_in_seconds):
        """clear tiles that have not been accessed sooner than age_in_second
        
        Note: This assumes real time. If you call this after a long pause it will likely clear everything. One idea:
        after resuming from a pause, iterate over the tiles and tile.touch() each one. This will update the timestamps,
        and subsequent calls will let aging take its course.
        :param age_in_seconds: int or float; tiles older than this are moved to the cache
        :return: None
        """
        dead = []
        num_pruned = 0
        current_time = time.time()
        for tile in self._tiles.values():
            if current_time - tile._access_time > age_in_seconds:
                dead.append(tile)
                num_pruned += 1
        for tile in dead:
            self._remove(tile.idx)
        self.num_pruned = num_pruned

    def clear(self):
        """clear all tiles, without caching"""
        self._tiles.clear()

    def clear_cache(self):
        """clear cached tiles"""
        self._cache = []

    def drain(self):
        """clear all tiles, caching them as appropriate"""
        for idx in tuple(self._tiles.keys()):
            self._remove(idx)

    def __len__(self):
        return len(self._tiles)


class CompositeTile(object):
    def __init__(self, idx, src_layers, composite_tile_size, sort_key, make_surf=None):
        self.idx = 0
        size = composite_tile_size, composite_tile_size
        if make_surf:
            self.image = make_surf(None, size)
        else:
            self.image = pygame.Surface(size)
        x = idx[0] * composite_tile_size
        y = idx[1] * composite_tile_size
        self.rect = self.image.get_rect(topleft=(x, y), size=size)
        self._access_time = 0
        self.init(idx, src_layers, sort_key)

    def init(self, idx, src_layers, sort_key):
        self.idx = idx
        self__rect = self.rect
        self__rect__colliderect = self__rect.colliderect
        self__rect__size = self.rect.size
        cx, cy = self__rect.topleft = idx[0] * self__rect__size[0], idx[1] * self__rect__size[1]
        self__image__blit = self.image.blit
        for layer in src_layers:
            for sprite in sorted(layer, key=sort_key):
                # some sprite implementations may not have an image, e.g. Tiled Object tiles
                image = getattr(sprite, 'image', None)
                if image:
                    sprite__rect = sprite.rect
                    if self__rect__colliderect(sprite__rect):
                        sx, sy = sprite.rect.topleft
                        self__image__blit(image, (sx - cx, sy - cy))

    def touch(self):
        self._access_time = time.time()
