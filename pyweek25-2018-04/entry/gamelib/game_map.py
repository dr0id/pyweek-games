# -*- coding: utf-8 -*-

from collections import defaultdict
import logging

import pygame
from pygame import KEYDOWN, KEYUP, QUIT, K_ESCAPE, K_RIGHT, K_LEFT, K_DOWN, K_UP, K_TAB

from pyknic.pygame_wrapper.transform import to_grey_scale
from pytmxloader import pytmxloader
from pytmxloader import TILE_LAYER_TYPE


logger = logging.getLogger(__name__)


def get_cached_image(img_cache, ti):
    key = (ti.tileset.image_path_rel_to_cwd, ti.spritesheet_x, ti.spritesheet_y, ti.flip_x, ti.flip_y, ti.angle)
    img = img_cache.get(key, None)
    if img is None:
        spritesheet = img_cache.get(ti.tileset.image_path_rel_to_cwd, None)
        if spritesheet is None:
            # noinspection PyUnresolvedReferences
            spritesheet = pygame.image.load(ti.tileset.image_path_rel_to_cwd).convert_alpha()
            # spritesheet = to_grey_scale(spritesheet)  # this is slower than just converting the single images
            img_cache[ti.tileset.image_path_rel_to_cwd] = spritesheet
        area = pygame.Rect(ti.spritesheet_x, ti.spritesheet_y, ti.tileset.tile_width, ti.tileset.tile_height)
        img = pygame.Surface(area.size, spritesheet.get_flags(), spritesheet)
        img.blit(spritesheet, (0, 0), area)
        img = pygame.transform.flip(img, ti.flip_x, ti.flip_y)
        if ti.angle != 0:
            img = pygame.transform.rotate(img, ti.angle)
        ip = ti.tileset.image_path_rel_to_cwd
        if 'space1_0_4x_smooth.png' in ip:
            img = img.convert()
        # img = to_grey_scale(img)
        img_cache[key] = img
    return img


def load_map(name):
    """load a map and parse its contents into Python data collections

    ground = [TileInfo,]
    objects = [ObjectBaseInfo,]
    missions = {"mission1": [ObjectBaseInfo,],}

    :param name: path and filename of the map (*.json)
    :return: ground, background, objects, missions, map_info
    """
    # img_cache = {key: img,}  # see get_cached_image() for key construction
    img_cache = {}

    # ground and background sprites, object layers
    ground = []
    background = []
    objects = []

    # missions = {"mission1": [obj1,],}
    missions = defaultdict(list)

    map_info = pytmxloader.load_map_from_file_path(name)
    for idx, layer in enumerate(map_info.layers):
        if not layer.visible:
            continue
        if layer.layer_type == TILE_LAYER_TYPE:
            for tile_y in range(layer.height):
                for tile_x in range(layer.width):
                    pygame.event.pump()
                    tile = layer.data_yx[tile_y][tile_x]
                    if tile is None:
                        continue
                    # pos = tile.tile_x + tile.offset_x, tile.tile_y + tile.offset_y
                    ti = tile.tile_info
                    img = get_cached_image(img_cache, ti)
                    tile.image = img

                    # kind = ti.properties.get("kind", None)
                    # if kind != "background":
                    #     ground.append(ti)
                    if layer.name == 'ground':
                        ground.append(tile)
                    elif layer.name == 'background':
                        tile.z = 3
                        background.append(tile)
                    elif layer.name == 'buildings':
                        tile.z = 0
                        background.append(tile)
                    elif layer.name == 'windows':
                        tile.z = 1
                        background.append(tile)

        elif layer.layer_type == pytmxloader.OBJECT_LAYER_TYPE:
            logger.info('>> layer={}', layer.name)
            if 'mission' in layer.name:
                for ot in layer.objects:
                    missions[layer.name].append(ot)
            else:
                for coll in layer.objects, layer.rectangles, layer.polygons, layer.poly_lines:
                    for ot in coll:
                        objects.append(ot)

    return ground, background, objects, missions, map_info


def get_map_size(map_info):
    layer0 = map_info.layers[0]
    mw, mh, tw, th = layer0.width, layer0.height, layer0.tile_w, layer0.tile_h
    return mw * tw, mh * th
