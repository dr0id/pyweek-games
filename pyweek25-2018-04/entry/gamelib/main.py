# -*- coding: utf-8 -*-
from __future__ import division, print_function

import gc
import logging

import pygame

from gamelib.context_credits import ContextCredits
from pyknic import context
import pyknic
from gamelib import settings
from gamelib import context_intro
from gamelib import sfx
from gamelib import resource_sound

logger = logging.getLogger(__name__)


def init_timing():
    lock_stepper = pyknic.timing.LockStepper(timestep=settings.sim_time_step, max_steps=8, max_dt=settings.sim_time_step * 10)

    pyknic.context.set_deferred_mode(True)  # avoid routing problems, due stack operations only at update
    context_top = pyknic.context.top
    context_update = pyknic.context.update

    update_clock = pygame.time.Clock()

    def _update_top_context(dt, simt, ls):
        top_context = context_top()
        if top_context:
            top_context.update(dt, simt)
        context_update()
        if settings.print_fps:
            update_clock.tick()
            logger.debug("update_clock fps: %s", str(update_clock.get_fps()))

    lock_stepper.event_integrate += _update_top_context

    draw_clock = pygame.time.Clock()
    def _do_draw(dt, simt, *args):
        top_context = context_top()
        if top_context:
            top_context.draw(settings.screen, do_flip=True, interpolation_factor=lock_stepper.alpha)
        if settings.print_fps:
            draw_clock.tick()
            logger.debug("draw_clock fps:   %s", str(draw_clock.get_fps()))

    frame_cap = pyknic.timing.FrameCap()
    frame_cap.event_update += _do_draw

    game_time = pyknic.timing.GameTime()
    game_time.event_update += frame_cap.update
    game_time.event_update += lock_stepper.update

    return game_time


# def _print_fps(clock, draw_clock, sim_clock):
#     if settings.print_fps:
#         fps = clock.get_fps()
#         # gfx_fps = draw_clock.get_fps()
#         # sim_fps = sim_clock.get_fps()
#         # logger.debug('main loop fps: %s   draw fps: %s   sim fps: %s', str(fps), str(gfx_fps), str(sim_fps))
#         logger.debug('main loop fps: %s  ', str(fps))
#     return 1  # return next interval


def main():
    pyknic.logs.log_environment()
    pyknic.logs.print_logging_hierarchy()

    init_pygame()

    intro = context_intro.Intro()
    ctx = ContextCredits()
    context.push(ctx)
    context.push(intro)

    game_time = init_timing()
    context_len = context.length
    clock = pygame.time.Clock()  # this is the only clock we need

    gc.disable()

    while context_len():
        dt = clock.tick() / 1000.0  # convert to seconds
        pygame.event.pump()
        game_time.update(dt)
        if settings.print_fps:
            logger.debug("FPS: %s", str(clock.get_fps()))
        c1, c2, c3 = gc.get_count()
        if c1 >= 10000 or c2 >= 10000 or c3 >= 10000:
            logger.info("gc collecting because %s %s %s", str(c1), str(c2), str(c3))
            gc.collect()

    logger.debug('Finished. Exiting.')


def init_pygame():
    logger.info("%s pygame init %s", "#" * 10, "#"*10)
    pygame.mixer.pre_init(22050, -16, 2, 64)
    # num_pass, num_fail = pygame.init()
    # logger.info("pygame.init(): pass: %s  failed: %s", num_pass, num_fail)
    # if num_fail != 0:
    #     logger.error("pygame failed init pass:%s failed:%s", num_pass, num_fail)
    #
    # if num_pass <= 0:
    #     logger.error("pygame didn't initialize anything!")
    import pkgutil
    import importlib

    for module_info in pkgutil.walk_packages(pygame.__path__, "pygame."):
        # logger.info("!! %s", module_info)
        try:
            _finder, name, is_pkg = module_info
            if "examples" in name \
                    or "tests" in name\
                    or "docs" in name\
                    or "threads" in name\
                    or "scrap" in name \
                    :
                continue  # skip
            logger.info("loading module: %s", name)
            _module = importlib.import_module(name)
            if hasattr(_module, 'init'):
                _init_module(_module)
            else:
                logger.info("  module has no init(): %s", _module)
        except ImportError as mnfe:
            logger.warning(mnfe)
        except AttributeError as ae:
            logger.error(ae)

    del pkgutil
    del importlib

    display_info = pygame.display.Info()
    csh = display_info.current_h
    if int(csh*0.9) < settings.screen_height:
        settings.screen_height = int(csh * 0.9)  # accommodate for title bar and task bar ?
    settings.screen_size = settings.screen_width, settings.screen_height

    settings.screen = pygame.display.set_mode(settings.screen_size, settings.screen_flags)

    pygame.mixer.init()
    pygame.mixer.set_num_channels(settings.num_channels)
    pygame.mixer.set_reserved(settings.num_reserved_channels)
    settings.sfx_handler = sfx.Sound()
    settings.sfx_handler.load(resource_sound.sfx_data)
    settings.sfx_handler.fill_music_carousel([resource_sound.SONG_FACTORY_REMEMBER])
    # reserved mixer channels
    settings.channel_trampoline = pygame.mixer.Channel(settings.channel_trampoline)
    settings.channel_collectibles = pygame.mixer.Channel(settings.channel_collectibles)
    settings.channel_jumping = pygame.mixer.Channel(settings.channel_jumping)

    # scrap has to come after display init
    try:
        logger.info("loading module: %s", pygame.scrap)
        _init_module(pygame.scrap)
    except AttributeError as ae:
        logger.warning(ae)

    icon = pygame.image.load("./data/gfx/misc/icon.png")
    pygame.display.set_icon(icon)
    pygame.display.set_caption(settings.title)
    pygame.mixer.set_num_channels(settings.num_channels)
    settings.screen.fill(settings.default_fill_color)
    pygame.display.flip()
    logger.info("%s pygame init DONE %s", "#" * 10, "#"*10)



def _init_module(m):
    logger.info("  init() %s", m)
    m.init()
    try:
        if m.get_init():
            logger.info("    get_init() True %s", m)
        else:
            logger.error("    get_init() False %s", m)
    except AttributeError as ae:
        # module has not get_init() method
        logger.error(ae)


# this is needed in order to work with py2exe
if __name__ == '__main__':
    main()
