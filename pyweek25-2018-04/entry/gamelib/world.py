# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'world.py' is part of CoDoGuPywk25
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]                    # For instance:
                   |                                  * 1.2.0.1 instead of 1.2-a
                   +-|* 0 for alpha (status)          * 1.2.1.2 instead of 1.2-b2 (beta with some bug fixes)
                     |* 1 for beta (status)           * 1.2.2.3 instead of 1.2-rc (release candidate)
                     |* 2 for release candidate       * 1.2.3.0 instead of 1.2-r (commercial distribution)
                     |* 3 for (public) release        * 1.2.3.5 instead of 1.2-r5 (commercial dist with many bug fixes)


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division

import logging
from collections import defaultdict

from gamelib import settings
from pyknic.mathematics.geometry import SphereBinSpace2

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2018"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["World"]  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class World(object):

    def __init__(self):
        self.ground = None
        self.smog = -1200
        self.smog_thickness = 400
        self.entities_by_kind = defaultdict(list)
        self.space = SphereBinSpace2(settings.screen_width // 2)


    def _add(self, k, ent, *args):
        use_rect = args[0]
        self.entities_by_kind[k].append(ent)
        if use_rect:
            self.space.adds_aabb([ent])
        else:
            self.space.add(ent)

    def add_entity(self, entity, use_rect=False):

        self._do_action_per_kind(entity, self._add, use_rect)

        if hasattr(entity, 'sprite'):
            self._do_action_per_kind(entity.sprite, self._add, use_rect)

    @staticmethod
    def _do_action_per_kind(entity, action, *args):
        k = 1 << 0
        n = 0
        while k <= entity.kind:
            if k & entity.kind:
                action(k, entity, *args)
            n += 1
            k = 1 << n

    def add_entities(self, entities):
        for ent in entities:
            self.add_entity(ent)

    def _rmv(self, k, ent, *args):
        try:
            self.entities_by_kind[k].remove(ent)
        except Exception as e:
            pass
        self.space.remove(ent)


    def remove_entity(self, entity):
        self._do_action_per_kind(entity, self._rmv)


logger.debug("imported")
