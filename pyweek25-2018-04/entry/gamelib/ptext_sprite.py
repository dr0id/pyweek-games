# -*- coding: utf-8 -*-


from pyknic.pygame_wrapper.spritesystem import TextSprite, Sprite
from gamelib import pygametext


class PtextSprite(TextSprite):
    def __init__(self, text, position, fontname=None, fontsize=24, color=None, gcolor=None, ocolor=None, scolor=None,
                 owidth=None, shadow=None, antialias=True, anchor=None, z_layer=1000, parallax_factors=None, alpha=255):
        self.fontname = fontname
        self.fontsize = fontsize
        self.color = color
        self.gcolor = gcolor
        self.ocolor = ocolor
        self.scolor = scolor
        self.owidth = owidth
        self.shadow = shadow
        self.antialias = antialias
        self.alpha = 255
        self.text = ''
        image = self.render()
        Sprite.__init__(self, image, position, anchor=anchor, z_layer=z_layer, parallax_factors=parallax_factors)
        self.text = text

    def render(self):
        img = pygametext.getsurf(self.text, self.fontname, self.fontsize, color=self.color, antialias=self.antialias,
                                 ocolor=self.ocolor, owidth=self.owidth, scolor=self.scolor, shadow=self.shadow,
                                 gcolor=self.gcolor, alpha=self.alpha / 255.0)
        return img
