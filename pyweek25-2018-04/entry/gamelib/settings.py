# -*- coding: utf-8 -*-
import os
import pygame
from pyknic.pygame_wrapper.eventmapper import EventMapper, ANY_KEY, ANY_MOD
from pyknic.generators import GlobalIdGenerator, FlagGenerator, IdGenerator
from gamelib import pygametext


# ------------------------------------------------------------------------------
# Tunables
# ------------------------------------------------------------------------------

print_fps = False
grey_contrast = 2  # 1..3, bigger values will get white and 0 would just be the alpha mask
master_volume = 1.0             # min: 0.0; max: 1.0

boss_level_difficulty = 0.8     # 1.0 is probably impossible; 0.5 is about as hard as level 4
boss_level_hero_health = 300    # for normal levels see PlayerTemplate.player_health
boss_level_ant_eater = 0.5      # ant damage slowed a bit for boss level
ambient_color = 96, 96, 96      # pygame color values; higher for more visibility in dark levels; None for OFF
player_is_strafe_while_shooting_allowed = False
fog_enabled = True              # disable if fog is too thick to play, or hurts performance
fog_alpha = 175                 # 0..255 alpha value for fog
below_ground_offset = 128
# ------------------------------------------------------------------------------
# CPU
# ------------------------------------------------------------------------------

ticks_per_second = 30
frames_per_second = 150
time_step = 1.0 / ticks_per_second
sim_time_step = time_step
frame_step = 1.0 / frames_per_second
sfx_step = 1.0


# ------------------------------------------------------------------------------
# Display
# ------------------------------------------------------------------------------

os.environ['SDL_VIDEO_CENTERED'] = '1'

screen_width = 448
screen_height = 768
screen_size = screen_width, screen_height
screen_flags_presets = (
    0,
    pygame.FULLSCREEN | pygame.DOUBLEBUF,
)
screen_flags = screen_flags_presets[0]
default_fill_color = 0, 0, 64
title = "Pyweek 25 - CoDoGuPywk25"
fog_move_amount = 480 // 4

# ------------------------------------------------------------------------------
# Game
# ------------------------------------------------------------------------------

# 0: context_intro
# 1: context_mainmenu
# 2: context_gameplay
# 3: context_credits
# 4: context_cutscene
startup_context = 0
startup_level = 1  # valid: 1 through 4

# Map
game_map_file = 'data/map/game_map.json'
gag_messages = [
    'Look what I stepped in.',
    'Nirvana is not so nirvana.',
    "Relax, it's not the end of the world.",
    "It's the Corporatocracy.",
    'Who put a banana peel there?',
]

# Missions
MISSION_STATUS_PENDING = 0
MISSION_STATUS_IN_PROGRESS = 1
MISSION_STATUS_COMPLETE = 2

# EventMappers (events -> actions)
debug_event_handlers = False
debug_render = False

_action_generator = GlobalIdGenerator(0)
ACTION_QUIT = _action_generator.next()
ACTION_ESCAPE = _action_generator.next()
ACTION_WIN_LEVEL = _action_generator.next()
ACTION_LOOSE_LEVEL = _action_generator.next()
ACTION_FULL_STOP = _action_generator.next()

ACTION_MOVE_UP = _action_generator.next()
ACTION_MOVE_DOWN = _action_generator.next()
ACTION_MOVE_LEFT = _action_generator.next()
ACTION_MOVE_RIGHT = _action_generator.next()
ACTION_STOP_UP = _action_generator.next()
ACTION_STOP_DOWN = _action_generator.next()
ACTION_STOP_LEFT = _action_generator.next()
ACTION_STOP_RIGHT = _action_generator.next()
ACTION_FIRE = _action_generator.next()
ACTION_HOLD_FIRE = _action_generator.next()
ACTION_USE_ITEM = _action_generator.next()
ACTION_MOUSE_MOTION = _action_generator.next()

ACTION_SECONDARY = _action_generator.next()

ACTION_TOGGLE_DEBUG_RENDER = _action_generator.next()
ACTION_HUD_SHOW_DEBUGS = _action_generator.next()
ACTION_EAT_CHIPS = _action_generator.next()

# context_intro
# intro_event_map = EventMapper({
#     pygame.QUIT: {None: ACTION_QUIT},
#     pygame.MOUSEBUTTONUP: {
#         1: ACTION_WIN_LEVEL,
#         2: ACTION_WIN_LEVEL,
#         3: ACTION_WIN_LEVEL,
#     },
#     pygame.KEYUP: {
#         (ANY_KEY, ANY_MOD): ACTION_WIN_LEVEL,
#     },
# })
#
# # context_mainmenu
# menu_event_map = EventMapper({
#     pygame.QUIT: {None: ACTION_QUIT},
#     # pygame.MOUSEBUTTONUP: {
#     #     1: ACTION_WIN_LEVEL,
#     #     2: ACTION_WIN_LEVEL,
#     #     3: ACTION_WIN_LEVEL,
#     # },
#     pygame.KEYUP: {
#         (pygame.K_RETURN, ANY_MOD): ACTION_WIN_LEVEL,
#         (pygame.K_SPACE, ANY_MOD): ACTION_WIN_LEVEL,
#     },
# })

# context_gameplay
# gameplay_event_map = EventMapper({
#     pygame.QUIT: {None: ACTION_QUIT},
#     pygame.MOUSEBUTTONDOWN: {
#         1: ACTION_FIRE,
#         3: ACTION_USE_ITEM,
#     },
#     pygame.MOUSEBUTTONUP: {
#         1: ACTION_HOLD_FIRE,
#     },
#     pygame.MOUSEMOTION: {
#         None: ACTION_MOUSE_MOTION,
#     },
#     pygame.KEYDOWN: {
#         (pygame.K_a, ANY_MOD): ACTION_MOVE_LEFT,
#         (pygame.K_d, ANY_MOD): ACTION_MOVE_RIGHT,
#         (pygame.K_w, ANY_MOD): ACTION_MOVE_UP,
#         (pygame.K_s, ANY_MOD): ACTION_MOVE_DOWN,
#         (pygame.K_q, ANY_MOD): ACTION_USE_ITEM,
#         (pygame.K_F1, ANY_MOD): ACTION_TOGGLE_DEBUG_RENDER,
#         (pygame.K_F2, ANY_MOD): ACTION_EAT_CHIPS,
#     },
#     pygame.KEYUP: {
#         (pygame.K_ESCAPE, ANY_MOD): ACTION_WIN_LEVEL,
#         (pygame.K_a, ANY_MOD): ACTION_STOP_LEFT,
#         (pygame.K_d, ANY_MOD): ACTION_STOP_RIGHT,
#         (pygame.K_w, ANY_MOD): ACTION_STOP_UP,
#         (pygame.K_s, ANY_MOD): ACTION_STOP_DOWN,
#     },
# })

# example for reference
# __event_map = EventMapper({
#     pygame.QUIT: {None: ACTION_QUIT},
#     pygame.KEYDOWN: {
#         (pygame.K_ESCAPE, ANY_MOD): ACTION_ESCAPE,
#         (pygame.K_F4, pygame.KMOD_ALT): ACTION_QUIT,
#         # cheat keys
#         (pygame.K_F10, ANY_MOD): ACTION_WIN_LEVEL,
#         (pygame.K_F9, ANY_MOD): ACTION_LOOSE_LEVEL,
#         (pygame.K_F1, ANY_MOD): ACTION_TOGGLE_DEBUG_RENDER,
#         (pygame.K_F2, ANY_MOD): ACTION_FULL_STOP,
#         (pygame.K_F3, ANY_MOD): ACTION_HUD_SHOW_DEBUGS,
#         # movement (arrows, joystick?, ...)
#         (pygame.K_UP, ANY_MOD): ACTION_MOVE_UP,
#         (pygame.K_DOWN, ANY_MOD): ACTION_MOVE_DOWN,
#         (pygame.K_LEFT, ANY_MOD): ACTION_MOVE_LEFT,
#         (pygame.K_RIGHT, ANY_MOD): ACTION_MOVE_RIGHT,
#         (pygame.K_w, ANY_MOD): ACTION_MOVE_UP,
#         (pygame.K_s, ANY_MOD): ACTION_MOVE_DOWN,
#         (pygame.K_a, ANY_MOD): ACTION_MOVE_LEFT,
#         (pygame.K_SPACE, ANY_MOD): ACTION_FIRE,
#         (pygame.K_LCTRL, ANY_MOD): ACTION_FIRE,
#         (pygame.K_m, ANY_MOD): ACTION_SECONDARY,
#         (pygame.K_d, ANY_MOD): ACTION_MOVE_RIGHT,
#     },
#     pygame.KEYUP: {
#         (pygame.K_SPACE, ANY_MOD): ACTION_HOLD_FIRE,
#         (pygame.K_LCTRL, ANY_MOD): ACTION_HOLD_FIRE,
#         (pygame.K_UP, ANY_MOD): ACTION_STOP_UP,
#         (pygame.K_DOWN, ANY_MOD): ACTION_STOP_DOWN,
#         (pygame.K_LEFT, ANY_MOD): ACTION_STOP_LEFT,
#         (pygame.K_RIGHT, ANY_MOD): ACTION_STOP_RIGHT,
#         (pygame.K_w, ANY_MOD): ACTION_STOP_UP,
#         (pygame.K_s, ANY_MOD): ACTION_STOP_DOWN,
#         (pygame.K_a, ANY_MOD): ACTION_STOP_LEFT,
#         (pygame.K_d, ANY_MOD): ACTION_STOP_RIGHT,
#     },
# })


# ------------------------------------------------------------------------------
# messages
# ------------------------------------------------------------------------------
# MSG_ANIM_END = _action_generator.next()



# ------------------------------------------------------------------------------
# kinds
# ------------------------------------------------------------------------------
_flag_gen = FlagGenerator()
KIND_UPDATE = _flag_gen.next()
KIND_HERO = _flag_gen.next() | KIND_UPDATE
KIND_TRAMP = _flag_gen.next()
KIND_GROUND = _flag_gen.next()
KIND_COLLECTION = _flag_gen.next()
KIND_CLOUD = _flag_gen.next()
KIND_SPRITE = _flag_gen.next()
KIND_FOG = _flag_gen.next()

KIND_SFX = _flag_gen.next()
KIND_MUSIC = _flag_gen.next()


# ------------------------------------------------------------------------------
# Graphics
# ------------------------------------------------------------------------------

image_cache_expire = 1 * 60          # expire unused textures after N seconds; recommend 1 min (1 * 60)
image_cache_memory = 5 * 2 ** 20     # force expiration if memory is exceeded; recommend 5 MB (5 * 2**20)


# ------------------------------------------------------------------------------
# Sound
# ------------------------------------------------------------------------------

sfx_path = 'data/sfx'
music_path = 'data/music'
num_channels = 16

# reserved channels
num_reserved_channels = 3
(channel_trampoline,
 channel_collectibles,
 channel_jumping) = tuple(range(num_reserved_channels))

sfx_handler = None


# ------------------------------------------------------------------------------
# Fonts
# ------------------------------------------------------------------------------

pygametext.FONT_NAME_TEMPLATE = 'data/font/%s'
font_themes = dict(
    intro_title=dict(
        fontname='VeraBd.ttf',
        fontsize=50,
        color='red2',
        gcolor='gold',
        ocolor=None,
        owidth=0.5,
        scolor='grey10',
        shadow=(2, 2),
    ),
    intro_subtitle=dict(
        fontname='Vera.ttf',
        fontsize=38,
        color='lightblue',
        gcolor='green4',
        ocolor=None,
        owidth=0.5,
        scolor='grey10',
        shadow=(1, 1),
    ),
    mainmenu=dict(
        fontname='BIOST.TTF',
        fontsize=42,
        color='gold',
        gcolor='tomato',
        ocolor=None,
        owidth=0.5,
        scolor='black',
        shadow=(1, 1),
    ),
    gameplay_prompt=dict(
        fontname='Boogaloo.ttf',
        fontsize=55,
        color='yellow',
        gcolor='orange',
        ocolor='black',
        owidth=0.5,
        scolor=None,
        shadow=None,
    ),
    mission_splash=dict(
        fontname='VeraBd.ttf',
        fontsize=20,
        color='red',
        gcolor='gold',
        ocolor=None,
        owidth=0.5,
        scolor='grey10',
        shadow=(2, 2),
    ),
    mission_speech=dict(
        fontname='VeraBd.ttf',
        fontsize=13,
        color='yellow',
        gcolor=(255, 196, 0, 255),  # 'orange',
        ocolor='black',
        owidth=0.5,
        scolor=None,
        shadow=None,
    ),
)
font_themes['default'] = font_themes['intro_subtitle']

key_to_text = {
    pygame.K_BACKSPACE: ("\\b", "backspace"),
    pygame.K_TAB: ("\\t", "tab"),
    pygame.K_CLEAR: ("", "clear"),
    pygame.K_RETURN: ("\\r", "return"),
    pygame.K_PAUSE: ("", "pause"),
    pygame.K_ESCAPE: ("^[", "escape"),
    pygame.K_SPACE: ("", "space"),
    pygame.K_EXCLAIM: ("!", "exclaim"),
    pygame.K_QUOTEDBL: ("\"", "quotedbl"),
    pygame.K_HASH: ("#", "hash"),
    pygame.K_DOLLAR: ("$", "dollar"),
    pygame.K_AMPERSAND: ("&", "ampersand"),
    pygame.K_QUOTE: ("", "quote"),
    pygame.K_LEFTPAREN: ("(", "left parenthesis"),
    pygame.K_RIGHTPAREN: ("),", "right parenthesis"),
    pygame.K_ASTERISK: ("*", "asterisk"),
    pygame.K_PLUS: ("+", "plus sign"),
    pygame.K_COMMA: (",", "comma"),
    pygame.K_MINUS: ("-", "minus sign"),
    pygame.K_PERIOD: (".", "period"),
    pygame.K_SLASH: ("/", "forward slash"),
    pygame.K_0: ("0", "0"),
    pygame.K_1: ("1", "1"),
    pygame.K_2: ("2", "2"),
    pygame.K_3: ("3", "3"),
    pygame.K_4: ("4", "4"),
    pygame.K_5: ("5", "5"),
    pygame.K_6: ("6", "6"),
    pygame.K_7: ("7", "7"),
    pygame.K_8: ("8", "8"),
    pygame.K_9: ("9", "9"),
    pygame.K_COLON: (":", "colon"),
    pygame.K_SEMICOLON: (";", "semicolon"),
    pygame.K_LESS: ("<", "less-than sign"),
    pygame.K_EQUALS: ("=", "equals sign"),
    pygame.K_GREATER: (">", "greater-than sign"),
    pygame.K_QUESTION: ("?", "question mark"),
    pygame.K_AT: ("@", "at"),
    pygame.K_LEFTBRACKET: ("[", "left bracket"),
    pygame.K_BACKSLASH: ("\\", "backslash"),
    pygame.K_RIGHTBRACKET: ("]", "right bracket"),
    pygame.K_CARET: ("^", "caret"),
    pygame.K_UNDERSCORE: ("_", "underscore"),
    pygame.K_BACKQUOTE: ("`", "grave"),
    pygame.K_a: ("a", "a"),
    pygame.K_b: ("b", "b"),
    pygame.K_c: ("c", "c"),
    pygame.K_d: ("d", "d"),
    pygame.K_e: ("e", "e"),
    pygame.K_f: ("f", "f"),
    pygame.K_g: ("g", "g"),
    pygame.K_h: ("h", "h"),
    pygame.K_i: ("i", "i"),
    pygame.K_j: ("j", "j"),
    pygame.K_k: ("k", "k"),
    pygame.K_l: ("l", "l"),
    pygame.K_m: ("m", "m"),
    pygame.K_n: ("n", "n"),
    pygame.K_o: ("o", "o"),
    pygame.K_p: ("p", "p"),
    pygame.K_q: ("q", "q"),
    pygame.K_r: ("r", "r"),
    pygame.K_s: ("s", "s"),
    pygame.K_t: ("t", "t"),
    pygame.K_u: ("u", "u"),
    pygame.K_v: ("v", "v"),
    pygame.K_w: ("w", "w"),
    pygame.K_x: ("x", "x"),
    pygame.K_y: ("y", "y"),
    pygame.K_z: ("z", "z"),
    pygame.K_DELETE: ("", "delete"),
    pygame.K_KP0: ("", "keypad 0"),
    pygame.K_KP1: ("", "keypad 1"),
    pygame.K_KP2: ("", "keypad 2"),
    pygame.K_KP3: ("", "keypad 3"),
    pygame.K_KP4: ("", "keypad 4"),
    pygame.K_KP5: ("", "keypad 5"),
    pygame.K_KP6: ("", "keypad 6"),
    pygame.K_KP7: ("", "keypad 7"),
    pygame.K_KP8: ("", "keypad 8"),
    pygame.K_KP9: ("", "keypad 9"),
    pygame.K_KP_PERIOD: (".", "keypad period"),
    pygame.K_KP_DIVIDE: ("/", "keypad divide"),
    pygame.K_KP_MULTIPLY: ("*", "keypad multiply"),
    pygame.K_KP_MINUS: ("-", "keypad minus"),
    pygame.K_KP_PLUS: ("+", "keypad plus"),
    pygame.K_KP_ENTER: ("\\r", "keypad enter"),
    pygame.K_KP_EQUALS: ("=", "keypad equals"),
    pygame.K_UP: ("", "up arrow"),
    pygame.K_DOWN: ("", "down arrow"),
    pygame.K_RIGHT: ("", "right arrow"),
    pygame.K_LEFT: ("", "left arrow"),
    pygame.K_INSERT: ("", "insert"),
    pygame.K_HOME: ("", "home"),
    pygame.K_END: ("", "end"),
    pygame.K_PAGEUP: ("", "page up"),
    pygame.K_PAGEDOWN: ("", "page down"),
    pygame.K_F1: ("", "F1"),
    pygame.K_F2: ("", "F2"),
    pygame.K_F3: ("", "F3"),
    pygame.K_F4: ("", "F4"),
    pygame.K_F5: ("", "F5"),
    pygame.K_F6: ("", "F6"),
    pygame.K_F7: ("", "F7"),
    pygame.K_F8: ("", "F8"),
    pygame.K_F9: ("", "F9"),
    pygame.K_F10: ("", "F10"),
    pygame.K_F11: ("", "F11"),
    pygame.K_F12: ("", "F12"),
    pygame.K_F13: ("", "F13"),
    pygame.K_F14: ("", "F14"),
    pygame.K_F15: ("", "F15"),
    pygame.K_NUMLOCK: ("", "numlock"),
    pygame.K_CAPSLOCK: ("", "capslock"),
    pygame.K_SCROLLOCK: ("", "scrollock"),
    pygame.K_RSHIFT: ("", "right shift"),
    pygame.K_LSHIFT: ("", "left shift"),
    pygame.K_RCTRL: ("", "right ctrl"),
    pygame.K_LCTRL: ("", "left ctrl"),
    pygame.K_RALT: ("", "right alt"),
    pygame.K_LALT: ("", "left alt"),
    pygame.K_RMETA: ("", "right meta"),
    pygame.K_LMETA: ("", "left meta"),
    pygame.K_LSUPER: ("", "left windows key"),
    pygame.K_RSUPER: ("", "right windows key"),
    pygame.K_MODE: ("", "mode shift"),
    pygame.K_HELP: ("", "help"),
    pygame.K_PRINT: ("", "print screen"),
    pygame.K_SYSREQ: ("", "sysrq"),
    pygame.K_BREAK: ("", "break"),
    pygame.K_MENU: ("", "menu"),
    pygame.K_POWER: ("", "power"),
    pygame.K_EURO: ("", "euro"),
}

mod_key_to_text = {
    pygame.KMOD_NONE: "",
    pygame.KMOD_LSHIFT: "left shift",
    pygame.KMOD_RSHIFT: "right shift",
    pygame.KMOD_SHIFT: "shift",
    pygame.KMOD_CAPS: "capslock",
    pygame.KMOD_LCTRL: "left ctrl",
    pygame.KMOD_RCTRL: "right ctrl",
    pygame.KMOD_CTRL: "ctrl",
    pygame.KMOD_LALT: "left alt",
    pygame.KMOD_RALT: "right alt",
    pygame.KMOD_ALT: "alt",
    pygame.KMOD_LMETA: "left meta",
    pygame.KMOD_RMETA: "right meta",
    pygame.KMOD_META: "meta",
    pygame.KMOD_NUM: "num",
    pygame.KMOD_MODE: "mode",
}

try:
    from gamelib._dr0id_custom import *
except:
    pass

try:
    from gamelib._gummbum_custom import *
except:
    pass
