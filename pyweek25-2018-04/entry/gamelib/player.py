# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'player.py' is part of CoDoGuPywk25
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]                    # For instance:
                   |                                  * 1.2.0.1 instead of 1.2-a
                   +-|* 0 for alpha (status)          * 1.2.1.2 instead of 1.2-b2 (beta with some bug fixes)
                     |* 1 for beta (status)           * 1.2.2.3 instead of 1.2-rc (release candidate)
                     |* 2 for release candidate       * 1.2.3.0 instead of 1.2-r (commercial distribution)
                     |* 3 for (public) release        * 1.2.3.5 instead of 1.2-r5 (commercial dist with many bug fixes)


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging
import random

import pygame

from gamelib import settings
from pyknic.ai.statemachines import BaseState, StateDrivenAgentBase
from pyknic.events import Signal
from pyknic.mathematics import Vec2, Point2
from gamelib import resource_sound

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2018"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["Player", "PlayerEvents", "PlayerConfig"]  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class PlayerConfig(object):

    def __init__(self):
        self.tramp_jump_h = 180  # 150  # pixels, highest point of jump
        self.tramp_jump_t = 1.0  # time needed to reach apex of jump
        self.side_fly_speed = 10
        # g = 2 * h / t**2          # see: http://error454.com/2013/10/23/platformer-physics-101-and-the-3-fundamental-equations-of-platformers/
        self.tramp_g = (2 * self.tramp_jump_h) / (self.tramp_jump_t ** 2)
        # v = (2 * g * h)**0.5         # see: http://error454.com/2013/10/23/platformer-physics-101-and-the-3-fundamental-equations-of-platformers/
        self.tramp_v = (2 * self.tramp_g * self.tramp_jump_h) ** 0.5
        self.jump_v = self.tramp_v * 0.6
        self.walk_speed = 100
        self.radius = 10


class _PlayerStand(BaseState):

    @staticmethod
    def enter(owner):
        owner.v.x = 0.0
        owner.v.y = 0.0
        # TODO: player landing sound; nope, not here
        # resource_sound.play_sound(resource_sound.SFX_PLAYER_LAND)

    @staticmethod
    def exit(owner):
        pass

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        pass

    @staticmethod
    def handle_event(owner, event):
        if event == PlayerEvents.left:
            owner.state_machine.switch_state(_PlayerWalkL)
        elif event == PlayerEvents.right:
            owner.state_machine.switch_state(_PlayerWalkR)
        elif event == PlayerEvents.jump:
            owner.v.y = -owner.config.jump_v
            owner.state_machine.switch_state(_PlayerFlyUp)
        owner.update_sprite_direction()


class _PlayerWalk(BaseState):

    @staticmethod
    def exit(owner):
        pass

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        owner.position += owner.v * dt

    @staticmethod
    def handle_event(owner, event):
        if event == PlayerEvents.stop:
            owner.state_machine.switch_state(_PlayerStand)
        elif event == PlayerEvents.jump:
            owner.v.y = -owner.config.jump_v
            owner.state_machine.switch_state(_PlayerFlyUp)
        elif event == PlayerEvents.boing:
            owner.state_machine.switch_state(_PlayerFlyUp)
        elif event == PlayerEvents.falling:
            owner.state_machine.switch_state(_PlayerFlyDown)


class _PlayerWalkL(_PlayerWalk):

    @staticmethod
    def enter(owner):
        owner.v.y = 0.0
        owner.v.x = -owner.config.walk_speed


class _PlayerWalkR(_PlayerWalk):

    @staticmethod
    def enter(owner):
        owner.v.y = 0.0
        owner.v.x = owner.config.walk_speed


class _PlayerFlyBase(BaseState):

    @staticmethod
    def exit(owner):
        pass

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        # integrate forces
        _forces = owner.forces + owner.gravity
        # _forces -= (owner.v.normalized * owner.v.length ** 2) * 0.01  # drag
        owner.a = _forces / owner.mass  # TODO: maybe calculate forces some where else
        owner.v += owner.a * dt
        owner.position += owner.v * dt

        # reset forces
        owner.forces.x = 0.0
        owner.forces.y = 0.0


class _PlayerFlyUp(_PlayerFlyBase):
    @staticmethod
    def enter(owner):
        resource_sound.play_sound(resource_sound.SFX_PLAYER_JUMP)

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        _PlayerFlyBase.update(owner, dt, sim_t, *args, **kwargs)
        if owner.v.y > 0:
            owner.state_machine.switch_state(_PlayerFlyDown)

    @staticmethod
    def handle_event(owner, event):
        if event == PlayerEvents.left:
            owner.v.x += -owner.config.side_fly_speed
        elif event == PlayerEvents.right:
            owner.v.x += owner.config.side_fly_speed
        owner.update_sprite_direction()
        # elif event == PlayerEvents.hit_ground:
        #     owner.state_machine.switch_state(_PlayerStand)
        # elif event == PlayerEvents.stop_walking:
        #     owner.v.x = 0.0
        # elif event == PlayerEvents.boing:
        #     _PlayerFlyUp._trampolin_impuls(owner)

class _PlayerFlyUpRescue(_PlayerFlyUp):

    @staticmethod
    def handle_event(owner, event):
        pass

class _PlayerFlyDown(_PlayerFlyBase):

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        _PlayerFlyBase.update(owner, dt, sim_t, *args, **kwargs)
        world = args[0]
        if owner.position.y > world.ground.position.y + settings.below_ground_offset:
            # find nearest platform
            grounds = world.entities_by_kind[settings.KIND_GROUND]
            ground = grounds[0]
            dist = owner.position.get_distance_sq(ground.position)
            for g in grounds:
                sq = owner.position.get_distance_sq(g.position)
                if sq < dist and g.position.y <= world.ground.position.y:
                    dist = sq
                    ground = g

            # calc jump_v
            dy = owner.position.y - ground.position.y
            vx = (ground.position.x - owner.position.x ) / (4 * owner.config.tramp_jump_t)
            vy = (2 * owner.config.tramp_g * dy) ** 0.5
            owner.v.y = -vy
            owner.v.x = vx
            # set it and change state
            owner.state_machine.switch_state(_PlayerFlyUpRescue)
            # TODO: gag message! see todo.txt notes [1]

    @staticmethod
    def handle_event(owner, event):
        if event == PlayerEvents.left:
            owner.v.x += -owner.config.side_fly_speed
        elif event == PlayerEvents.right:
            owner.v.x += owner.config.side_fly_speed
        elif event == PlayerEvents.hit_ground:
            owner.state_machine.switch_state(_PlayerStand)
            resource_sound.play_sound(resource_sound.SFX_PLAYER_LAND)
        # elif event == PlayerEvents.stop_walking:
        #     owner.v.x = 0.0
        elif event == PlayerEvents.boing:
            owner.state_machine.switch_state(_PlayerFlyUp)
        #     _PlayerFlyUp._trampolin_impuls(owner)
        owner.update_sprite_direction()

class PlayerEvents(object):
    right = 0
    left = 1
    stop = 2
    jump = 3
    boing = 4
    hit_ground = 5
    falling = 6




class Player(StateDrivenAgentBase):

    kind = settings.KIND_HERO

    def __init__(self, config):
        StateDrivenAgentBase.__init__(self, _PlayerFlyDown, self)
        self.position = Point2(200, 0)
        self.old_position = self.position.clone()
        self.v = Vec2(0.0, 0.0)
        self.a = Vec2(0.0, 0.0)
        self.mass = 1.0
        self.forces = Vec2(0.0, 0.0)
        self.gravity = Vec2(0.0, config.tramp_g)
        self.config = config
        self.radius = config.radius
        self.collectibles = []
        self.speech_bubble = None

        self.state_machine.event_switched_state += self._on_switched_state

        _fall = pygame.image.load(
            "data/gfx/kenney_platformercharacters/PNG/Female/Poses/female_fall.png").convert_alpha()
        _jump = pygame.image.load(
            "data/gfx/kenney_platformercharacters/PNG/Female/Poses/female_jump.png").convert_alpha()
        _stand = pygame.image.load(
            "data/gfx/kenney_platformercharacters/PNG/Female/Poses/female_stand.png").convert_alpha()
        _walk1 = pygame.image.load(
            "data/gfx/kenney_platformercharacters/PNG/Female/Poses/female_walk1.png").convert_alpha()
        _walk2 = pygame.image.load(
            "data/gfx/kenney_platformercharacters/PNG/Female/Poses/female_walk2.png").convert_alpha()

        self._images = {
            _PlayerStand: [_stand, _stand],
            _PlayerFlyUp: [_jump, _jump],
            _PlayerFlyUpRescue: [_jump, _jump],
            _PlayerFlyDown: [_fall, _fall],
            _PlayerWalkR: [_walk1, _walk2],
            _PlayerWalkL: [_walk1, _walk2],
        }

        self.gag_messages = [
            "This was too low!",
            "No, not going to the underworld!",
            "Uh didn't like to stay at nirvana too long",
             "Yikes. Nirvana is not so nirvana!",
             "No, not again!",
             "There is nothing there!",
        ]

        self.event_gag_message = Signal()

    # noinspection PyUnusedLocal
    @staticmethod
    def _on_switched_state(owner, previous_state, state_machine):
        logger.info("player: %s -> %s", previous_state, state_machine.current_state)
        owner.sprite.stop()
        owner.sprite.reset()
        owner.sprite.frames = owner._images.get(state_machine.current_state, None)
        if owner.sprite.frames is None:
            owner.sprite.frames = owner._images.get(_PlayerStand, None)
        owner.sprite.anchor = 'midbottom'
        owner.sprite.zoom = 0.5
        owner.sprite.set_image(owner.sprite.frames[owner.sprite.current_index])
        owner.update_sprite_direction()
        owner.sprite.start()

        if state_machine.current_state is _PlayerFlyUpRescue:
            message = random.choice(owner.gag_messages)
            owner.event_gag_message.fire(owner, message)

    def update_sprite_direction(self):
        self.sprite.flipped_x = True if self.v.x < 0 else False

    def handle_message(self, sender, receiver, msg_type, extra):
        pass

    def collect(self, collectible):
        self.collectibles.append(collectible)

    def is_standing(self):
        return self.state_machine.current_state is _PlayerStand

    def update(self, dt, sim_t, *args, **kwargs):
        StateDrivenAgentBase.update(self, dt, sim_t, *args, **kwargs)
        if self.collectibles:
            world = args[0]
            collides = world.space.collides([self])
            for t in collides[self]:
                if t.position.get_distance_sq(self.position) <= 70**2:
                    if self.collectibles:
                        if t.kind == settings.KIND_SPRITE:
                            if hasattr(t, 'orig_frames'):
                                if t.orig_frames:
                                    t.frames = t.orig_frames
                                    t.orig_frames = None
                                    t.set_image(t.frames[t.current_index])
                                    c = self.collectibles[0]
                                    c.amount -= 1
                                    if c.amount <= 0:
                                        self.collectibles.pop(0)


logger.debug("imported")
