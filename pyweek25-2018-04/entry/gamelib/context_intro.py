# -*- coding: utf-8 -*-
import logging
from collections import defaultdict
import random

import pygame

from gamelib import settings, collisiondetector, resource_sound
from gamelib.animated import Animated
from gamelib.camera import Camera
from gamelib.game_map import load_map
from gamelib.ground import Ground
from gamelib.missions import Missions
from gamelib.ptext_sprite import PtextSprite
from gamelib.player import PlayerEvents, Player, PlayerConfig
from gamelib.trampoline import Trampoline
from gamelib.collectibles import Collectible
from gamelib.world import World
from pyknic.context import Context
from pyknic.entity import BaseEntity
from pyknic.mathematics import Vec2, Point2, TYPE_VECTOR
from pyknic.mathematics.clipping import clip_segment_against_aabb
from pyknic.pygame_wrapper import spritesystem
from pyknic.pygame_wrapper.spritesystem import Sprite, TextSprite, DefaultRenderer
from pyknic.pygame_wrapper.transform import to_grey_scale
from pyknic.tweening import Tweener, ease_in_out_sine

logger = logging.getLogger(__name__)


tweener = Tweener(logger_instance=logger)


class Fog(BaseEntity):
    pass

class Intro(Context):
    def __init__(self):
        self.player = None
        self.world = None
        self.cam = None

        self.map_ground = None
        self.map_background = None
        self.map_objects = None
        self.map_missions = None
        self.map_info = None
        self.player_spawn = None
        self.cloud_bank = None
        self.mission_manager = None

        self.trampoline_frames = []
        self.renderer = DefaultRenderer()
        self.camera = spritesystem.Camera(pygame.Rect(0, 0, settings.screen_width, settings.screen_height))
        self._fog_push_enabled = False
        self._dots = []
        self.sprn = None
        self.sprd = None
        self.gag_msg = None
        self.sprn_cache = []
        self.sprd_cache = []

    def _pump_loading(self, msg):
        def _append_new_dot(self, surf):
            x = random.randint(0, settings.screen_width)
            y = random.randint(0, settings.screen_height)
            r = random.randint(4, 30)
            c = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255), random.randint(0, 200))
            self._dots.append((surf, c, (x, y), r, 0))

        pygame.event.pump()
        surf = pygame.display.get_surface()
        surf.fill(settings.default_fill_color)
        if not self._dots:
            for i in range(100):
                _append_new_dot(self, surf)
        else:
            for i in range(1):
                self._dots.pop(0)
                _append_new_dot(self, surf)

        for i in self._dots:
            pygame.draw.circle(*i)
        the_font = pygame.font.Font(None, 30)
        loading = the_font.render("loading...", 1, (255, 255, 255))
        surf.blit(loading, loading.get_rect(center=(settings.screen_width // 2, settings.screen_height // 2)))
        loading = the_font.render(msg, 1, (255, 255, 255))
        surf.blit(loading, loading.get_rect(center=(settings.screen_width // 2, settings.screen_height // 2 + 40)))
        pygame.display.flip()


    def enter(self):
        settings.sfx_handler.start_music_carousel()
        self.world = World()

        # map
        self.map_ground, self.map_background, self.map_objects, self.map_missions, self.map_info = load_map(settings.game_map_file)
        self._pump_loading("map_objects")
        for o in self.map_objects:
            # logger.info('object name: {}', o.name)
            if o.name == 'player spawn':
                self.player_spawn = o
            elif o.name == 'cloud':
                self.cloud_bank = o

        # ground
        # grounds = [
        #     Ground(pygame.Rect(-100, -10, 600, 5)),
        #     Ground(pygame.Rect(200, -100, 300, 5)),
        # ]
        # self.world.add_entities(grounds)
        # self.world.ground = grounds[0]  # once ground have to be the world ground
        self._pump_loading("more map_objects")
        for e in self.map_objects:
            if e.name == 'ground':
                the_ground = Ground(pygame.Rect(e.x, e.y, e.width, e.height))
                self.world.add_entity(the_ground)
                self.world.ground = the_ground
            else:
                kind_ = e.properties.get('kind', None)
                if kind_ == 'cloud':
                    self.world.smog = e.y
                    self.world.smog_thickness = e.height
                elif kind_ is None:
                    # background building ?
                    pass
                else:
                    # raise Exception("???")
                    pass

        # player
        self.player = Player(PlayerConfig())
        self.player.position.copy_values(Point2(self.player_spawn.x, self.player_spawn.y))

        img = pygame.Surface((10, 10))
        img.fill((200, 200, 200))

        animated = Animated(self.player.position, [img, img], 6)
        animated.radius = self.player.radius
        animated.z_layer = 10
        animated.orig_frames = None  # prevent conversion to grey and re-color
        self.player.sprite = animated
        self.player.event_gag_message += self._on_gag_message


        self.world.add_entity(self.player)

         # cam
        self.cam = Camera(settings.screen_width, settings.screen_height)
        self.camera.track = self.cam
        self.cam.track = self.player

        # fog
        if settings.fog_enabled:
            _fog_imgs = [pygame.image.load("data/gfx/misc/fog01.png").convert_alpha()]
            _fw, _fh = _fog_imgs[0].get_size()
            _g_rect = self.world.ground.rect
            self._pump_loading("fog")
            for n in range(_g_rect.left - _fw - _fw, _g_rect.right + _fw + _fw, _fw):
                pos = Point2(n, _g_rect.top + 32)
                aabb = _fog_imgs[0].get_rect(center=pos.as_tuple(int))
                fog = Fog(settings.KIND_FOG, pos, aabb=aabb)

                spr = Animated(fog.position, _fog_imgs, 1.0)
                spr.alpha = settings.fog_alpha
                spr.z_layer = 15
                spr.flipped_x = random.choice([True, False])
                spr.name = "fog"
                spr.anchor = 'midbottom'
                spr.orig_frames = _fog_imgs  # prevent conversion to gray scale
                spr.rect = _fog_imgs[0].get_rect(center=spr.position.as_tuple(int)) # this is only for the binspace, will be overridden by the spitesystem

                fog.sprite = spr
                self.world.add_entity(fog, True)

        # # sky (proof of concept for gradient sky, spr need update in binspace!!)
        # surf = pygame.Surface((100, 100))
        # surf.fill((255, 255, 0))
        # spr = Animated(Point2(self.player.position.x, self.player.position.y), [surf], 0)
        # spr.z_layer = 100
        # spr.orig_frames = []
        # spr.parallax_factors = Vec2(0.0, 1.0)
        # spr.rect = surf.get_rect(center=spr.position.as_tuple(int))
        # self.world.add_entity(spr, True)


        self._pump_loading("ground")
        for e in self.map_ground:
            x = e.tile_x + e.offset_x
            y = e.tile_y + e.offset_y
            w, h = e.image.get_size()
            ground = Ground(pygame.Rect(x, y, w, h))
            self.world.add_entity(ground, True)
            animated = Animated(ground.position, [e.image], 0)
            animated.radius = w
            animated.z_layer = 1
            self.world.add_entity(animated)

        for b in self.map_background:
            self._pump_loading("background")
            pygame.event.pump()
            anim = Animated(Point2(b.tile_x + b.offset_x, b.tile_y + b.offset_y), [b.image], 0)
            anim.anchor = 'topleft'
            anim.radius = b.image.get_size()[0]
            anim.z_layer = b.z
            self.world.add_entity(anim)

        img1 = pygame.image.load("data/gfx/platformer-pack-redux-360-assets/PNG/Tiles/spring.png").convert_alpha()
        img1 = pygame.transform.smoothscale(img1, (32, 32)).convert_alpha()
        img2 = pygame.image.load("data/gfx/platformer-pack-redux-360-assets/PNG/Tiles/sprung.png").convert_alpha()
        img2 = pygame.transform.smoothscale(img2, (32, 32)).convert_alpha()

        self.trampoline_frames = [img1, img2]

        # mission
        mission_idx = settings.startup_level - 1
        self.mission_manager = Missions()
        for c in self.mission_manager.missions:
            name = c.name
            desc = c.description
            sprn = self.add_text_sprite(name, Point2(0, 0), theme='mission_splash', add_to_world=False)
            sprd = self.add_text_sprite(desc, Point2(0, 0 + 65), theme='mission_splash', add_to_world=False)
            self.sprn_cache.append(sprn)
            self.sprd_cache.append(sprd)
        self.next_mission(mission_idx)

        # collisions
        collisiondetector.register_collision_func((settings.KIND_HERO, settings.KIND_TRAMP),
                                                  self._collide_player_vs_trampolines)
        collisiondetector.register_collision_func((settings.KIND_HERO, settings.KIND_GROUND),
                                                  self._collide_player_vs_ground)
        collisiondetector.register_collision_func((settings.KIND_HERO, settings.KIND_COLLECTION),
                                                  self._collide_player_vs_collectible)
        collisiondetector.register_collision_func((settings.KIND_HERO, settings.KIND_FOG), self._collide_player_vs_fog)

        self._pump_loading("Done!")

    def _on_gag_message(self, player, message):

        def remove(tween, *args):
            spr = tween.o
            tweener.remove_tween(tween)
            self.world.remove_entity(spr)
            self.gag_msg = None
        if self.gag_msg:
            remove(self.gag_msg)
        msg = settings.gag_messages.pop(0)
        settings.gag_messages.append(msg)
        msg = '[AIIEEE! {}]'.format(msg)
        logger.info("player gag message: %s", msg)
        x, y = self.player.position.as_tuple()
        spr = self.add_text_sprite(msg, Point2(x, y - 64))
        self.world.add_entity(spr)
        self.gag_msg = tweener.create_tween(spr, 'tween_duration', 0, 5, 5, cb_end=remove)

    def next_mission(self, mission_idx=None):

        tweener.clear()

        self.mission_manager.new_mission(mission_idx)
        current_mission = self.mission_manager.current_mission
        logger.info('>>> new mission loaded index={}', self.mission_manager.mission_idx)
        logger.info('>>> mission info: number={} description={}', current_mission.number, current_mission.description)
        mission_name = 'mission{}'.format(current_mission.number)
        mission_objects = self.map_missions[mission_name]

        initial_mission = 1
        if current_mission.number <= initial_mission:
            self._pump_loading("next mission")

        # remove the TextSprites
        for spr in tuple(self.world.entities_by_kind[settings.KIND_SPRITE]):
            if isinstance(spr, TextSprite):
                self.world.remove_entity(spr)
        if self.player.speech_bubble:
            self.world.remove_entity(self.player.speech_bubble)
            self.player.speech_bubble = None
        if self.sprn:
            self.world.remove_entity(self.sprn)
            self.sprn = None
        if self.sprd:
            self.world.remove_entity(self.sprd)
            self.sprd = None
        if self.gag_msg:
            self.world.remove_entity(self.gag_msg.o)
            self.gag_msg = None

        # trampolines
        # self.world.add_entity(Trampoline(400.0, -5.0))
        # count = 10
        # for n in range(count):
        #     self.world.add_entity(Trampoline(420.0, (n + 1) * -150.0))
        # self.world.add_entity(Trampoline(420.0, -(count + 1) * 150.0, 0.5, direction=Vec2(0.0, -1.0).rotated(-45)))
        ents = self.world.entities_by_kind
        for c in tuple(ents[settings.KIND_TRAMP]), tuple(ents[settings.KIND_COLLECTION]):
            for e in c:
                self.world.remove_entity(e)
                self.world.remove_entity(e.sprite)

        if current_mission.number <= initial_mission:
            self._pump_loading("mission objects")
        for e in mission_objects:
            kind_ = e.properties.get('kind', None)
            if kind_ == 'trampoline':
                strength = 1.0
                trampoline = Trampoline(e.x, e.y, strength, direction=Vec2(0.0, -1.0).rotated(e.rotation))
                anim = Animated(trampoline.position, self.trampoline_frames, 4.0)
                anim.rotation = -e.rotation
                anim.radius = trampoline.radius
                anim.loop = False
                anim.stop()
                anim.reset()
                anim.z_layer = 2
                trampoline.sprite = anim
                self.world.add_entity(trampoline)
            elif kind_ == 'missioncloud':
                mission_cloud = Collectible(Point2(e.x, e.y), 32, 50, name='missioncloud')
                mission_cloud.sprite = self.make_sprite(
                    pygame.image.load('data/gfx/misc/Cloud-clipart-6-cropped.png'), Point2(e.x, e.y), e.height)
                self.world.add_entity(mission_cloud)
            elif kind_ == 'air':
                mission_air = Collectible(Point2(e.x, e.y), 15, 10, name='air')
                mission_air.sprite = self.make_sprite(
                    pygame.image.load('data/gfx/misc/Curves-15px.png'), Point2(e.x, e.y), 15)
                self.world.add_entity(mission_air)
            elif kind_ == 'birdy':
                mission_birdy = Collectible(Point2(e.x, e.y), 15, 20, name='birdy')
                mission_birdy.sprite = self.make_sprite(
                    pygame.image.load('data/gfx/misc/Curves-15px.png'), Point2(e.x, e.y), 15)
                self.world.add_entity(mission_birdy)
            elif kind_ == 'boss':
                # NOTE: do not link entity and sprite; we want the sprite to remain after collision
                mission_boss = Collectible(Point2(e.x, e.y), e.width, 1, name='boss')
                mission_boss_sprite = self.make_sprite(
                    pygame.image.load('data/gfx/misc/tardis_by_queennekoyasha-36x64.png'), Point2(e.x, e.y), e.width // 2)
                self.world.add_entity(mission_boss)
                self.world.add_entity(mission_boss_sprite)
            elif kind_ is None:
                # background buildings?
                pass

        # collectibles
        # collectibles = [
        #     Collectible(Vec2(300, -150), 20, 30)
        # ]
        # self.world.add_entities(collectibles)

        for spr in self.world.entities_by_kind[settings.KIND_SPRITE]:
            if current_mission.number <= initial_mission:
                self._pump_loading("converting to grey")
            if spr.position.y > self.world.smog:
                if not hasattr(spr, 'orig_frames'):
                    spr.orig_frames = spr.frames
                    spr.frames = [to_grey_scale(fr, intensity=settings.grey_contrast) for fr in spr.frames]
                    spr.set_image(to_grey_scale(spr.image, intensity=settings.grey_contrast))

        # mission splash

        self.mission_splash()

        # mission speech
        if self.mission_manager.mission_idx == initial_mission - 1:
            logger.info('mission {}, immediate speech', current_mission.number)
            self.mission_speech()
        else:
            def delayed_speech(*args):
                self.mission_speech()
            logger.info('mission {}, delayed speech', current_mission.number)
            tweener.create_tween(
                self.mission_manager.current_mission, 'speech_delay', 0.0, 6.0, 6.0, cb_end=delayed_speech)

    def update(self, delta_time, sim_time):
        self._handle_events()
        tweener.update(delta_time)

        need_update = self.world.entities_by_kind[settings.KIND_UPDATE]
        for u in need_update:
            u.old_position = u.position.clone()
            u.update(delta_time, sim_time, self.world)
            self.world.space.add(u)  # update in binspace
            if hasattr(u, 'sprite'):
                self.world.space.add(u.sprite)

        # cam update, needs to be after player update
        self.cam.update(delta_time, sim_time, self.world)
        if self.sprn:
            self.sprn.position.x = self.camera.position.x
            self.sprn.position.y = self.camera.position.y - 160
            self.world.add_entity(self.sprn)
        if self.sprd:
            self.sprd.position.x = self.camera.position.x
            self.sprd.position.y = self.camera.position.y - 96
            self.world.add_entity(self.sprd)

        nearby_entities = self.world.space.collides([self.player])
        grouped = self._get_grouped_by_kind(nearby_entities[self.player])

        for others in grouped.values():
            collisiondetector.check_collisions([self.player], others)

        if self.mission_manager.current_is_complete():
            logger.info('>>> mission {} complete!', self.mission_manager.current_mission.name)
            self.next_mission()

        if self.player.speech_bubble:
            x, y = self.player.position.as_tuple()
            self.player.speech_bubble.position.copy_values(Point2(x, y - 45))
            self.world.add_entity(self.player.speech_bubble)
        if self.gag_msg:
            x, y = self.player.position.as_tuple()
            self.gag_msg.o.position.copy_values(Point2(x, y - 64))
            self.world.add_entity(self.gag_msg.o)

        Animated.scheduler.update(delta_time, sim_time)

        settings.sfx_handler.update(delta_time, sim_time)

    def game_won(self):
        # TODO: do what? roll credits?
        logger.info('<<<>>> YOU WON <<<>>> YOU WON <<<>>> YOU WON <<<>>>')
        self.pop()

    def make_sprite(self, image, p2_pos, radius):
        spr = Sprite(image, p2_pos)
        spr.kind = settings.KIND_SPRITE
        spr.radius = radius
        spr.orig_frames = []
        return spr

    def add_text_sprite(self, text, pos, theme='mission_speech', add_to_world=True):
        if pos.w == TYPE_VECTOR:
            pos = Point2(pos.x, pos.y)

        spr = PtextSprite(text, pos, **settings.font_themes[theme])
        spr.kind = settings.KIND_SPRITE
        spr.orig_frames = []
        spr.radius = 1
        if add_to_world:
            self.world.add_entity(spr)
        return spr

    def mission_splash(self):
        def display(tween, *args):
            spr = tween.o
            self.world.add_entity(spr)
            tweener.create_tween(spr, 'tween_display', 7, -7, 7, cb_end=fade)

        def fade(tween, *args):
            spr = tween.o
            tweener.create_tween(spr, 'alpha', 255, -196, 3, cb_end=remove)

        def remove(tween, *args):
            spr = tween.o
            self.world.remove_entity(spr)
            self.sprn = None
            self.sprd = None
        # current_mission = self.mission_manager.current_mission
        # name = current_mission.name
        # desc = current_mission.description
        self.cam.update(0, 0, self.world)
        x, y = self.camera.position.x, self.camera.position.y
        # logger.error('>>> x,y %s', str((x, y)))  # not sure why this should be logged as error anyway
        # sprn = self.add_text_sprite(name, Point2(x, y), theme='mission_splash', add_to_world=False)
        # sprd = self.add_text_sprite(desc, Point2(x, y + 65), theme='mission_splash', add_to_world=False)
        sprn = self.sprn_cache[self.mission_manager.mission_idx]
        sprd = self.sprd_cache[self.mission_manager.mission_idx]
        sprn.anchor = 'center'
        sprd.anchor = 'center'
        sprn.position = Point2(x, y)
        sprd.position = Point2(x, y + 65)
        logger.error('>>> pos {}', sprd.position)
        self.sprn = sprn
        self.sprd = sprd
        tweener.create_tween(sprn, 'tween_delay', 2, -2, 2, cb_end=display)
        tweener.create_tween(sprd, 'tween_delay', 2, -2, 2, cb_end=display)

    def mission_speech(self):
        def next_speech(tween, *args):
            self.mission_speech()

        def remove(tween, *args):
            spr = tween.o
            self.world.remove_entity(spr)
            self.player.speech_bubble = None
            tweener.create_tween(spr, 'tween_total', 1, -1, 1, cb_end=next_speech)
        speech = self.mission_manager.current_mission.speech
        if speech:
            text = speech.pop(0)
            x, y = self.player.position.as_tuple()
            spr = self.add_text_sprite(text, Point2(x, y - 32))
            spr.tween_total = 0
            tweener.create_tween(spr, 'tween_total', 5, -5, 5, cb_end=remove)
            self.player.speech_bubble = spr

    # @staticmethod
    def _collide_player_vs_trampolines(self, players, trampolines):
        for player in players:
            for t in trampolines:
                if t.position.get_distance_sq(player.position) < t.radius ** 2:
                    sound_idx = random.choice(resource_sound.sfx_trampolines)
                    resource_sound.play_sound(sound_idx)
                    player.gravity = Vec2(0.0, player.config.tramp_g)
                    player.v = player.config.tramp_v * t.strength * t.normal
                    player.handle_event(PlayerEvents.boing)
                    logger.debug("activating trampoline g: %s  v: %s", player.config.tramp_g, player.config.tramp_v)
                    msg = self.mission_manager.on_trampoline_hit(t)
                    if msg:
                        logger.info('>>> trampoline hit msg: {}', msg)
                        self.add_text_sprite(msg, t.position + Vec2(20, -20))
                    t.sprite.reset()
                    t.sprite.start()
                    break

    @staticmethod
    def _collide_player_vs_ground(players, grounds):
        ground_hits = 0
        falls = 0
        for player in players:
            for ground in grounds:
                px, py = player.old_position.as_tuple()
                ex, ey = player.position.as_tuple()
                intersect, ux, uy, wx, wy = clip_segment_against_aabb(px, py, ex, ey, ground.rect.left, ground.rect.top,
                                                                      ground.rect.right, ground.rect.bottom)
                if intersect and (uy == ground.rect.top or wy == ground.rect.top):
                    player.position.y = ground.rect.top - player.radius
                    ground_hits += 1
                elif ground.rect.left < player.position.x < ground.rect.right and player.position.y + player.radius == ground.rect.top:
                    player.position.y = ground.rect.top - player.radius
                    ground_hits += 1
                else:
                    falls += 1

        if ground_hits > 0:
            player.handle_event(PlayerEvents.hit_ground)
            return
        if falls > 0:
            # TODO: player landing sound; nope, not here
            # resource_sound.play_sound(resource_sound.SFX_PLAYER_LAND)
            player.handle_event(PlayerEvents.falling)

    def swap_to_color(self, t):
        if t.kind & settings.KIND_SPRITE and isinstance(t, Animated):
            if hasattr(t, 'orig_frames'):
                if t.orig_frames is not None:
                    t.frames = t.orig_frames
                    t.orig_frames = None
                    t.set_image(t.frames[t.current_index])
                    return True
        return False

    def _collide_player_vs_fog(self, players, fogs):
        for player in players:
            for fog in fogs:
                if not self._fog_push_enabled:
                    continue

                if fog.rect.collidepoint(player.position.as_tuple(int)):
                    if len(player.collectibles) > 0:
                        self._fog_push_enabled = False
                        amount = settings.fog_move_amount
                        for f in self.world.entities_by_kind[settings.KIND_FOG]:
                            tweener.create_tween(f.position, "y", f.position.y, amount, 10, ease_in_out_sine)

                            # #smog boundary
                            # self.world.smog += amount  # TODO: store where the boudnary should be in tha mission
                            # r = self.world.ground.rect.inflate(200, amount)
                            # r.midbottom = (self.world.ground.rect.centerx, self.world.smog)
                            # entities = self.world.space.get_in_rect(*r)
                            # for ent in entities:
                            #     if ent.kind & settings.KIND_SPRITE and ent.position.y < self.world.smog:
                            #         self.swap_to_color(ent)



    def _collide_player_vs_collectible(self, players, collectibles):
        for player in players:
            for collectible in collectibles:
                if player.position.get_distance_sq(collectible.position) < (player.radius + collectible.radius) ** 2:
                    self._fog_push_enabled = True
                    player.collect(collectible)
                    self.world.remove_entity(collectible)
                    if collectible.name != 'boss':
                        self.world.remove_entity(collectible.sprite)
                    else:
                        self.game_won()
                    logger.debug("collected: %s %s", collectible.position, collectible.amount)
                    if collectible.name == 'missioncloud':
                        msg = self.mission_manager.on_reach_the_clouds(collectible)
                        if msg:
                            logger.info('>>> cloud hit msg: {}', msg)
                            self.add_text_sprite(msg, collectible.position)
                    elif collectible.name == 'air':
                        msg = self.mission_manager.on_get_some_air(collectible)
                        if msg:
                            logger.info('>>> air hit msg: {}', msg)
                            self.add_text_sprite(msg, collectible.position)
                    elif collectible.name == 'birdy':
                        msg = self.mission_manager.on_catch_a_sunbird(collectible)
                        if msg:
                            logger.info('>>> sunbird hit msg: {}', msg)
                            self.add_text_sprite(msg, collectible.position)

    def _handle_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.pop()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self.pop()
                elif event.key == pygame.K_RIGHT:
                    self._evaluate_move_side()
                elif event.key == pygame.K_LEFT:
                    self._evaluate_move_side()
                elif event.key == pygame.K_UP:
                    self.player.handle_event(PlayerEvents.jump)
                elif event.key == pygame.K_1:
                    settings.debug_render = not settings.debug_render
            elif event.type == pygame.KEYUP:
                if event.key in (pygame.K_LEFT, pygame.K_RIGHT):
                    self._evaluate_move_side()

        self._evaluate_move_side()

    def _evaluate_move_side(self):
        pressed = pygame.key.get_pressed()
        left_or_right = pressed[pygame.K_RIGHT] - pressed[pygame.K_LEFT]
        if left_or_right == 0:
            self.player.handle_event(PlayerEvents.stop)
        else:
            self.player.handle_event(PlayerEvents.right if left_or_right > 0 else PlayerEvents.left)

    def draw(self, screen, do_flip=True, interpolation_factor=1.0):

        off_w, off_h = self.cam.rect.topleft

        # screen.fill(settings.default_fill_color)
        smog_screen_y = self.world.smog - off_h
        sad_rect = pygame.Rect(0, 0, settings.screen_width, settings.screen_height)
        happy_rect = sad_rect.copy()
        if smog_screen_y < 0:
            # all in smog
            happy_rect.size = (0, 0)
        elif smog_screen_y > settings.screen_height:
            # all in sky
            sad_rect.size = (0, 0)
        else:
            # fifty fifty
            happy_rect.h = smog_screen_y
            sad_rect.top = smog_screen_y
        screen.fill((70, 70, 70), sad_rect)
        screen.fill((70, 169, 244), happy_rect)

        # TODO: its drawing way too much... but there is a strange draw order defect...!
        ents =  self.world.space.get_in_rect(*self.camera.world_rect) #self.world.entities_by_kind[settings.KIND_SPRITE]
        visible_sprites = self._get_grouped_by_kind(ents, True).get(settings.KIND_SPRITE, [])
        visible_sprites.sort()
        self.renderer.draw_sprites(screen, self.camera, visible_sprites)

        if settings.debug_render:

            for s in visible_sprites:
                pygame.draw.rect(screen, (255, 0, 255), s.rect, 1)


            # cells
            _c_s = self.world.space.cell_size
            for _cx, _cy in self.world.space.cell_ids:
                cell = pygame.Rect(_cx * _c_s - off_w, _cy * _c_s - off_h, _c_s, _c_s)
                pygame.draw.rect(screen, (0, 50, 0), cell, 1)

            # ground
            # pygame.draw.line(screen, red, (-1000 - off_w, 0 - off_h), (1000 - off_w, 0 - off_h))
            for ground in self.world.entities_by_kind[settings.KIND_GROUND]:
                pygame.draw.rect(screen, (120, 40, 40), ground.rect.move(-off_w, -off_h))

            # trampolines
            for t in self.world.entities_by_kind[settings.KIND_TRAMP]:
                tr = pygame.Rect(0, 0, 20, 20)
                tx, ty = t.position.as_tuple(int)
                tr.center = (tx - off_w, ty - off_h)
                pygame.draw.rect(screen, (0, 0, 255), tr)

            # smog boundary
            pygame.draw.line(screen, (100, 100, 100), (-1000 - off_w, self.world.smog - off_h),
                             (1000 - off_w, self.world.smog - off_h), self.world.smog_thickness)

            # collectibles
            for c in self.world.entities_by_kind[settings.KIND_COLLECTION]:
                cx, cy = c.position.as_tuple(int)
                pygame.draw.circle(screen, (255, 255, 0), (cx - off_w, cy - off_h), c.radius, 3)

            # player
            px, py = self.player.position.as_tuple(int)
            p_radius = 10
            p_color = (255, 0, 255) if py < self.world.smog else (150, 150, 150)
            pygame.draw.circle(screen, p_color, (px - off_w, py - off_h), p_radius)
            # draw player movement segment
            ox, oy = self.player.old_position.as_tuple(int)
            pygame.draw.line(screen, (255, 255, 255), (ox - off_w, oy - off_h), (px - off_w, py - off_h))

        # update screen
        if do_flip:
            pygame.display.flip()

    @staticmethod
    def _get_grouped_by_kind(entities, include_sprites=False):
        result = defaultdict(list)
        for ent in entities:
            c = 0
            k = 1 << c
            while k <= ent.kind:
                if ent.kind & k:
                    result[k].append(ent)
                c += 1
                k = 1 << c
        if not include_sprites:
            result.pop(settings.KIND_SPRITE, None)
        return result
