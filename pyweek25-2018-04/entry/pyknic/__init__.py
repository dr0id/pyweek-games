# -*- coding: utf-8 -*-
"""
.. todo:: initial pyknic doc
.. todo:: explain how logging works and is configured

"""
import sys
import logging

# log info first before importing anything
from pyknic import info
info.log_info()

from pyknic import logs
from pyknic import context
from pyknic import cache
from pyknic import events
from pyknic import generators
from pyknic import mathematics
from pyknic import options
from pyknic import resource
from pyknic import settings
from pyknic import timing
from pyknic import tweening
from pyknic import compatibility
from pyknic import animation

from pyknic.info import log_info, check_version

__all__ = [
    # modules
    "animation",
    "logs",
    "info",
    "context",
    "cache",
    "events",
    "generators",
    "mathematics",
    "options",
    "resource",
    "settings",
    "timing",
    "tweening",
    # functions
    "log_info",
    "check_version",
    "compatibility",
    "registry",
]

logger = logging.getLogger(__name__)

# check python version
# BE WARNED: if you change this line it might work or not (the code is probably untested with new versions)
info.check_version("python", (2, 7), tuple(sys.version_info[:2]), (3, 7), [(3, 4)])

# TODO: write a predefined main loop -> subsystem management? (because of e.g.  music fadeout)
# TODO: logging to another process...?

logger.debug("imported " + "-" * 50)
