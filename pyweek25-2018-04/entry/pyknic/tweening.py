# -*- coding: utf-8 -*-

"""
Tweening
========

Tweening is about ease functions. These ease functions facilitate to interpolate a value between a and b in different
ways in a certain time. The ease functions are often used to move an object in a visually pleasant way. Instead of
moving an object from a to b in a linear movement in a certain time, another ease function can be used that starts
moving the object fast and slows down when approaching target position b.


So how is this all implemented?
There are many predefined ease functions (see the demo, just run this file a a script or look through the code). The
interface of such a ease functions is always the same:

:code:

    def ease_xy(t, b, c, d, p)

t is the current *t*ime of the tween that is calculated.
b is the *b*egin value
c is the *c*hange value
d is the *d*uration, how long it will take until the target b + c is reached
p are the *p*arameters that most ease functions don't use, but some can be parametrized, see descriptions for details

With such a function one could do easing already, but would be cumbersome because all the values have to be managed.
Therefore the :class:`Tweener` class has been introduced. It manages the life cycle and updating of the :class:`Tween`
objects. The reason is to have just one update method instead of many (one on each tween). This has also the nice
side effect to save some performance since a method call in python is not that cheap. In this way all tweens
created by this instance of a tweener can be updated efficiently.

The tweening works by calling a ease function that calculates the value corresponding to the time passed. This value
is then set to the defined attribute of an object.

To make the tweening even more useful a callback can be passed that is called when the tween has ended. This can be
used to chain multiple tweens in a sequence and move an object in interesting ways around by combining different
ease functions. Or other things can be triggered by this callback of course.



Example:

In a platformer game there is always a moving platform. Typically such a platform is moving forth and back. This
movement can be implemented in a very simple way using a tween (well actually using two tweens). Assume such
a moving platform p has to move between the coordinates 200 and 250 in 3 seconds. So p.x should be set
initially to 200. Using the tweener this would look like this:

:code:

    p = Platform()
    tweener = Tweener()
    tween = tweener.create_tween(p, "x", 200, 50, 3)  # linear movement between 200 to 250 in 3 seconds

But wait, this is only half of the way. Once the platform has reached the far end, it should return. One solution
is to add a callback that creates another tween that moves the platform back.


:code:

    def move_forth(tween, tweener):
        tweener.create_tween(tween.o, tween.a, 200, 50, 3, cb_end=move_back, cb_args=[tweener])

    def move_back(tween, tweener):
        tweener.create_tween(tween.o, tween.a, 250, -50, 3, cb_end=move_forth, cb_args=[tweener])

    tween = tweener.create_tween(p, "x", 200, 50, 3, cb_end=move_back, cb_args=[tweener])

    while 1:
        ...
        tweener.update(dt)
        ...


With this setup the platform would move forth and back as long tweener.update(dt) is called. The trick here is
that the move_forth() callback creates a tween that calls move_back() at the end. Then a new tween is created
that calls move_forth() again at the end and so on.

There are some more features implemented. Take a look at the create_tween doc string.


Have fun using tweening!


This code and equations are based on:

http://wiki.python-ogre.org/index.php?title=CodeSnippits_pyTweener
http://hosted.zeh.com.br/tweener/docs/en-us/misc/transitions.html
http://code.google.com/p/tweener/source/browse/trunk/as3/caurina/transitions/Equations.as

Design of the tweens is based on:

http://www.robertpenner.com/easing/penner_chapter7_tweening.pdf

"""
from __future__ import print_function, division
import logging
import random

from math import cos
from math import sin
from math import pow
from math import asin
from math import sqrt
from math import isnan
# noinspection PyPep8Naming
from math import pi as PI

logger = logging.getLogger(__name__)
logger.debug("importing...")


__version__ = '2.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

TWO_PI = PI * 2
HALF_PI = PI / 2.0


#  noinspection PyUnusedLocal
def ease_linear(t, b, c, d, p):  # pragma: nocover
    return c * t / d + b


# noinspection PyUnusedLocal
def ease_in_quad(t, b, c, d, p):  # pragma: nocover
    t /= d
    return c * t * t + b


# noinspection PyUnusedLocal
def ease_out_quad(t, b, c, d, p):  # pragma: nocover
    t /= d
    return -c * t * (t - 2) + b


# noinspection PyUnusedLocal
def ease_in_out_quad(t, b, c, d, p):  # pragma: nocover
    t /= d / 2.0
    if t < 1:
        return c / 2.0 * t * t + b
    t -= 1
    return -c / 2.0 * (t * (t - 2) - 1) + b


def ease_out_in_quad(t, b, c, d, p):  # pragma: nocover
    if t < d / 2:
        return ease_out_quad(t * 2, b, c / 2, d, p)
    return ease_in_quad((t * 2) - d, b + c / 2, c / 2, d, p)


# noinspection PyUnusedLocal
def ease_in_cubic(t, b, c, d, p):  # pragma: nocover
    t /= d
    return c * t * t * t + b


# noinspection PyUnusedLocal
def ease_out_cubic(t, b, c, d, p):  # pragma: nocover
    t = t / d - 1
    return c * (t * t * t + 1) + b


# noinspection PyUnusedLocal
def ease_in_out_cubic(t, b, c, d, p):  # pragma: nocover
    t /= d / 2
    if t < 1:
        return c / 2 * t * t * t + b
    t -= 2
    return c / 2 * (t * t * t + 2) + b


# noinspection PyUnusedLocal
def ease_out_in_cubic(t, b, c, d, p):  # pragma: nocover
    if t < d / 2:
        return ease_out_cubic(t * 2, b, c / 2, d, p)
    return ease_in_cubic((t * 2) - d, b + c / 2, c / 2, d, p)


# noinspection PyUnusedLocal
def ease_in_quart(t, b, c, d, p):  # pragma: nocover
    t /= d
    return c * t * t * t * t + b


# noinspection PyUnusedLocal
def ease_out_quart(t, b, c, d, p):  # pragma: nocover
    t = t / d - 1
    return -c * (t * t * t * t - 1) + b


# noinspection PyUnusedLocal
def ease_in_out_quart(t, b, c, d, p):  # pragma: nocover
    t /= d / 2
    if t < 1:
        return c / 2 * t * t * t * t + b
    t -= 2
    return -c / 2 * (t * t * t * t - 2) + b


def ease_out_in_quart(t, b, c, d, p):  # pragma: nocover
    if t < d / 2:
        return ease_out_quart(t * 2.0, b, c / 2, d, p)
    return ease_in_quart((t * 2) - d, b + c / 2, c / 2, d, p)


# noinspection PyUnusedLocal
def ease_in_quint(t, b, c, d, p):  # pragma: nocover
    t = t / d
    return c * t * t * t * t * t + b


# noinspection PyUnusedLocal
def ease_out_quint(t, b, c, d, p):  # pragma: nocover
    t = t / d - 1
    return c * (t * t * t * t * t + 1) + b


# noinspection PyUnusedLocal
def ease_in_out_quint(t, b, c, d, p):  # pragma: nocover
    t /= d / 2
    if t < 1:
        return c / 2 * t * t * t * t * t + b
    t -= 2
    return c / 2 * (t * t * t * t * t + 2) + b


# noinspection PyUnusedLocal
def ease_out_in_quint(t, b, c, d, p):  # pragma: nocover
    c /= 2
    if t < d / 2:
        return ease_out_quint(t * 2, b, c, d, p)
    return ease_in_quint(t * 2 - d, b + c, c, d, p)


# noinspection PyUnusedLocal
def ease_in_sine(t, b, c, d, p):  # pragma: nocover
    return -c * cos(t / d * HALF_PI) + c + b


# noinspection PyUnusedLocal
def ease_in_sine2(t, b, c, d, p):  # pragma: nocover
    return c * cos(t / d * TWO_PI) + b


# noinspection PyUnusedLocal
def ease_in_sine3(t, b, c, d, p):  # pragma: nocover
    return c * sin(t / d * TWO_PI) + b


# noinspection PyUnusedLocal
def ease_out_sine(t, b, c, d, p):  # pragma: nocover
    return c * sin(t / d * HALF_PI) + b


# noinspection PyUnusedLocal
def ease_in_out_sine(t, b, c, d, p):  # pragma: nocover
    return -c / 2 * (cos(PI * t / d) - 1) + b


def ease_out_in_sine(t, b, c, d, p):  # pragma: nocover
    if t < d / 2:
        return ease_out_sine(t * 2, b, c / 2, d, p)
    return ease_in_sine((t * 2) - d, b + c / 2, c / 2, d, p)


# noinspection PyUnusedLocal
def ease_in_circ(t, b, c, d, p):  # pragma: nocover
    t /= d + 0.0005
    return -c * (sqrt(1 - t * t) - 1) + b


# noinspection PyUnusedLocal
def ease_out_circ(t, b, c, d, p):  # pragma: nocover
    t = t / d - 1
    return c * sqrt(1 - t * t) + b


# noinspection PyUnusedLocal
def ease_in_out_circ(t, b, c, d, p):  # pragma: nocover
    t /= d / 2
    if t < 1:
        return -c / 2 * (sqrt(1 - t * t) - 1) + b
    t -= 2
    return c / 2 * (sqrt(1 - t * t) + 1) + b


# noinspection PyUnusedLocal
def ease_out_in_circ(t, b, c, d, p):  # pragma: nocover
    if t < d / 2:
        return ease_out_circ(t * 2, b, c / 2, d, p)
    return ease_in_circ(t * 2 - d, b + c / 2, c / 2, d, p)


# noinspection PyUnusedLocal
def ease_in_expo(t, b, c, d, p):  # pragma: nocover
    return b if t == 0 else c * (2 ** (10 * (t / d - 1))) + b - c * 0.001


# noinspection PyUnusedLocal
def ease_out_expo(t, b, c, d, p):  # pragma: nocover
    return b + c if (t == d) else c * (-2 ** (-10 * t / d) + 1) + b


# noinspection PyUnusedLocal
def ease_in_out_expo(t, b, c, d, p):  # pragma: nocover
    if t == 0:
        return b
    if t == d:
        return b + c
    t /= d / 2
    if t < 1:
        return c / 2 * pow(2, 10 * (t - 1)) + b - c * 0.0005
    return c / 2 * 1.0005 * (-pow(2, -10 * (t - 1)) + 2) + b


# noinspection PyUnusedLocal
def ease_out_in_expo(t, b, c, d, p):  # pragma: nocover
    if t < d / 2:
        return ease_out_expo(t * 2, b, c / 2, d, p)
    return ease_in_expo((t * 2) - d, b + c / 2, c / 2, d, p)


# noinspection PyUnusedLocal
def ease_in_elastic(t, b, c, d, params):  # pragma: nocover
    if t == 0:
        return b
    t /= d
    if t == 1:
        return b + c
    p = d * 0.3 if params is None or isnan(params.period) else params.period
    a = 0 if params is None or isnan(params.amplitude) else params.amplitude
    if a == 0 or a < abs(c):
        a = c
        s = p / 4
    else:
        s = p / TWO_PI * asin(c / a)
    t -= 1
    return -(a * pow(2, 10 * t) * sin((t * d - s) * TWO_PI / p)) + b


# noinspection PyUnusedLocal
def ease_out_elastic(t, b, c, d, params):  # pragma: nocover
    if t == 0:
        return b
    t /= d
    if t == 1:
        return b + c
    p = d * 0.3 if params is None or isnan(params.period) else params.period
    a = 1.0 if params is None or isnan(params.amplitude) else params.amplitude
    if a == 0 or a < abs(c):
        a = c
        s = p / 4
    else:
        s = p / TWO_PI * asin(c / a)

    return a * pow(2, -10 * t) * sin((t * d - s) * TWO_PI / p) + c + b


def ease_in_out_elastic(t, b, c, d, params):  # pragma: nocover
    if t == 0:
        return b
    t /= d / 2
    if t == 2:
        return b + c
    p = d * 0.3 * 1.5 if params is None or isnan(params.period) else params.period
    a = 0 if params is None or isnan(params.amplitude) else params.amplitude
    if a == 0 or a < abs(c):
        a = c
        s = p / 4
    else:
        s = p / TWO_PI * asin(c / a)
    if t < 1:
        t -= 1
        return -0.5 * (a * pow(2, 10 * t) * sin((t * d - s) * TWO_PI / p)) + b
    t -= 1
    return a * pow(2, -10 * t) * sin((t * d - s) * TWO_PI / p) * 0.5 + c + b


def ease_out_in_elastic(t, b, c, d, p):  # pragma: nocover
    if t < d / 2:
        return ease_out_elastic(t * 2, b, c / 2, d, p)
    return ease_in_elastic((t * 2) - d, b + c / 2, c / 2, d, p)


def ease_in_back(t, b, c, d, p):  # pragma: nocover
    s = 1.70158 if p is None or isnan(p.overshoot) else p.overshoot
    t /= d
    return c * t * t * ((s + 1) * t - s) + b


def ease_out_back(t, b, c, d, p):  # pragma: nocover
    s = 1.70158 if p is None or isnan(p.overshoot) else p.overshoot
    t = t / d - 1
    return c * (t * t * ((s + 1) * t + s) + 1) + b


def ease_in_out_back(t, b, c, d, p):  # pragma: nocover
    s = 1.70158 if p is None or isnan(p.overshoot) else p.overshoot
    t /= d / 2
    if t < 1:
        s *= 1.525
        return c / 2 * (t * t * ((s + 1) * t - s)) + b
    s *= 1.525
    t -= 2
    return c / 2 * (t * t * ((s + 1) * t + s) + 2) + b


def ease_out_in_back(t, b, c, d, p):  # pragma: nocover
    if t < d / 2:
        return ease_out_back(t * 2, b, c / 2, d, p)
    return ease_in_back((t * 2) - d, b + c / 2, c / 2, d, p)


# noinspection PyUnusedLocal
def ease_in_bounce(t, b, c, d, p=None):  # pragma: nocover
    return c - ease_out_bounce(d - t, 0, c, d) + b


# noinspection PyUnusedLocal
def ease_out_bounce(t, b, c, d, p=None):  # pragma: nocover
    t /= d
    if t < (1 / 2.75):
        return c * (7.5625 * t * t) + b
    elif t < (2 / 2.75):
        t -= (1.5 / 2.75)
        return c * (7.5625 * t * t + 0.75) + b
    elif t < (2.5 / 2.75):
        t -= (2.25 / 2.75)
        return c * (7.5625 * t * t + 0.9375) + b
    else:
        t -= (2.625 / 2.75)
        return c * (7.5625 * t * t + 0.984375) + b


# noinspection PyUnusedLocal
def ease_in_out_bounce(t, b, c, d, p):  # pragma: nocover
    if t < d / 2:
        return ease_in_bounce(t * 2, 0, c, d) * 0.5 + b
    else:
        return ease_out_bounce(t * 2 - d, 0, c, d) * 0.5 + c * 0.5 + b


def ease_out_in_bounce(t, b, c, d, p):  # pragma: nocover
    if t < d / 2:
        return ease_out_bounce(t * 2, b, c / 2, d, p)
    return ease_in_bounce((t * 2) - d, b + c / 2, c / 2, d, p)


def ease_random_int_bounce(t, b, c, d, p=(0, 1)):  # pragma: nocover
    """
    Random bouncing in range provided in p.

    :param t: current time
    :param b: begin
    :param c: change
    :param d: duration
    :param p: (x, y) tuple defining random range [x, y]
    :return: for t==0 returns b, random value in range [x, y] for t < d, otherwise b + c
    """
    if t < d:
        if t == 0:
            return b
        return b + random.randint(*p)
    else:
        return b + c


class Parameters(object):
    """
    The parameters for the ease functions.

    :param period: default is nan, the period of the function
    :param amplitude: default is nan, the amplitude of the function
    :param overshoot: default is nan, the overshoot of the function
    """

    def __init__(self, period=float("nan"), amplitude=float("nan"), overshoot=float("nan")):
        self.period = float(period)
        self.amplitude = float(amplitude)
        self.overshoot = float(overshoot)


class _TweenStates(object):
    error = 0
    created = 1
    active = 2
    paused = 3
    ended = 4


class UnknownTweenStateException(Exception):
    """Exception raised when a unknown state is provided."""
    def __init__(self, state):
        """
        The unknown TweenState exception.
        :param state: The unknown state to report.
        """
        msg = "Unknown state '{0}'! Should be one from Tweener.TweenState.".format(state)
        Exception.__init__(self, msg)


#: The conversion dictionary between string description and state values e.g.
#   TweenStateNames["active"] -> Tweener.TweenStates.active
#   TweenStateNames[Tweener.TweenStates.active] -> "active"
TweenStateNames = {
    _TweenStates.error: "error",
    _TweenStates.active: "active",
    _TweenStates.paused: "paused",
    _TweenStates.ended: "ended",
    _TweenStates.created: "created",
    "error": _TweenStates.error,
    "active": _TweenStates.active,
    "paused": _TweenStates.paused,
    "ended": _TweenStates.ended,
    "created": _TweenStates.created,
}


class _Tween(object):
    States = _TweenStates

    def __init__(self, tweener, obj, attr_name, begin, change, duration, ease_function, params, delay, delta_update,
                 cb_end, *cb_args):
        assert duration > 0.0, "duration should be > 0.0"
        self.tweener = tweener  # the controlling Tweener instance
        self.o = obj
        self.a = attr_name
        self.cb = cb_end
        self.cb_args = cb_args
        self.state = self.States.created

        # tween variables
        self.delay = delay
        self.t = float(-delay)
        self.f = ease_function

        # tween parameters
        self.b = float(begin)
        self.c = float(change)
        self.d = float(duration)
        self.p = params

        # delta update
        self.v = self.b
        self.delta_update = delta_update

    def pause(self):
        self.tweener.pause_tween(self)

    def resume(self):
        self.tweener.resume_tween(self)

    def stop(self):
        self.tweener.remove_tween(self)

    def start(self, immediate=False):
        self.tweener.start_tween(self, immediate=immediate)

    def __str__(self):
        return "{0}<{1}, {2}, {3}>()".format(self.__class__.__name__, self.o, self.a, TweenStateNames[self.state])


class TweenBelongsToOtherTweenerException(Exception):
    pass


class Tweener(object):
    TweenStates = _TweenStates

    def __init__(self, logger_instance=None):
        """
        The tweener class. It manages the life cycle of the tween (creation, update, remove).

        :param logger_instance: if set to None, then the default logger is used, if set to False no logging is done at
            all and if an instance is provided, then this one is used.
        """
        self._active_tweens = []  # ? maybe a dict? {entity: [tween]}
        self._paused_tweens = []
        self._logger = logger if logger_instance is None else (None if logger_instance is False else logger_instance)

    def update(self, delta_time):
        """
        The update method. It updates the tweens and calculates the new values of the tweens based on the time that
        has passed.

        :param delta_time: The time difference since last update.
        """
        ended_tweens = []
        for tween in self._active_tweens:
            tween.t += delta_time
            if tween.t >= 0.0:
                if tween.t >= tween.d:
                    ended_tweens.append(tween)
                    tween.t = tween.d
                value = tween.f(tween.t, tween.b, tween.c, tween.d, tween.p)
                if tween.delta_update:
                    delta = value - tween.v
                    tween.v = value
                    attr_value = getattr(tween.o, tween.a)
                    setattr(tween.o, tween.a, attr_value + delta)
                else:
                    setattr(tween.o, tween.a, value)
        for ended in ended_tweens:
            if self._logger:
                self._logger.debug("tween ended for obj: %s attr: %s", ended.o.__class__, ended.a)
            self._active_tweens.remove(ended)
            ended.state = self.TweenStates.ended
            if ended.cb is not None:
                args = list(ended.cb_args)
                args.append(ended)
                ended.cb(*args)

    # noinspection PyUnusedLocal,PyIncorrectDocstring
    def create_tween_by_end(self, obj, attr_name, begin, end, duration, ease_function=ease_linear, params=None,
                            cb_end=None, cb_args=tuple(), immediate=False, delay=0.0, delta_update=False,
                            do_start=True, *args):
        """
        Does the same thing as create_tween except that it takes a end instead of a change argument.


        :param end: What the end it should have in the given time.

        See create_tween for other params.
        """
        return self.create_tween(obj, attr_name, begin, end-begin, duration, ease_function, params, cb_end, cb_args,
                                 immediate, delay, delta_update, do_start, *args)

    # TODO:  maybe create_tween should be renamed to create_tween_by_change? to avoid confusion again
    # noinspection PyUnusedLocal
    def create_tween(self, obj, attr_name, begin, change, duration, ease_function=ease_linear, params=None,
                     cb_end=None, cb_args=tuple(), immediate=False, delay=0.0, delta_update=False, do_start=True,
                     *args):
        """
        This method is used create a tween.

        A tween has following attributes and methods:

            state current state for the tween (read only)
            States the available states (read only)

            def pause() -> None
                pauses this tween

            def resume() -> None
                resumes the tween

            def stop() -> None
                removes the tween from tweener

        There are some more attribute that really should only be used to look up value since they are used for the
        internal workings of the tween and tweener. These values correspond mostly the arguments of this method:

            o as the object it will manipulate
            a as the attribute name
            tweener as the tweener it was created

            cb as the end callback, might be None
            cb_args as the additional arguments for the end callback, might be an empty tuple

            t as the current time of the tween
            f as the reference to the used eas function

            b as the begin value
            c as the change value
            d as the duration value
            p as the parameters value, depends on the ease function

            delta_update is set to True when delta update is used, otherwise False
            v the value of the current step, only used if delta_update is set to True


        :param obj: The object to apply the ease function on.
        :param attr_name: The attribute of the object to apply the value.
        :param begin: The starting value.
        :param change: The amount of change it should cover in the given time. The end-value is determined as
            begin + change
        :param duration: The time it should take to reach the end value (begin + change) in seconds (actually it
            depends on the unit you use in the update method).
        :param ease_function: The ease function it should use. Default is :function:`ease_linear`
        :param params: Some ease functions can be parametrized, see ease function description for details.
        :param cb_end: A callback when the tween has ended. Default is None. Last argument in *args is the ended tween
            instance.
        :param cb_args: The arguments to pass to the callback additionally to the tween itself, should be a
            list or tuple. Default is an empty tuple.
        :param delay: Delays the starting of the tween in seconds (actually it depends on the unit you use in
            the update method). Sometimes one wants to schedule multiple tween at once but let them start at
            different times. This delay does that. Default is 0.0
        :param immediate: If set to True, then the initial value is set directly after creation (even when delayed).
            Default is False.
        :param delta_update: Default is False. If set to True, then only the difference between the two update calls is
            added to the attribute on the object instead of setting the calculated value. This is interesting to move
            an object that does not have to be at a fixed point in space. Typically the begin value is set to 0 when
            using delta_updates. Using another value might give strange results.
        :param do_start: Starts the tween if set to True (default), otherwise either twee.start() or
            tweener.start_tween(tween) has to be used to start the tween.

        :return: a :class:`_Tween` instance.
        """
        tween = _Tween(self, obj, attr_name, begin, change, duration, ease_function, params, delay, delta_update,
                       cb_end, *cb_args)
        if do_start:
            self.start_tween(tween, immediate=immediate)
        if self._logger:
            self._logger.debug("tween created for obj: %s attr: %s", obj.__class__, attr_name)
        return tween

    def clear(self):
        """
        Removes all tweens from the tweener.
        """
        self._active_tweens[:] = []
        self._paused_tweens[:] = []
        if self._logger:
            self._logger.debug("cleared all tweens.")

    def pause_tweens(self):
        """
        Pauses all active tweens.
        """
        self._paused_tweens.extend(self._active_tweens)
        self._active_tweens[:] = []
        if self._logger:
            self._logger.debug("paused all tweens.")

    def resume_tweens(self):
        """
        Resumes all paused tweens.
        """
        self._active_tweens.extend(self._paused_tweens)
        self._paused_tweens[:] = []
        if self._logger:
            self._logger.debug("resume all tweens.")

    def pause_tween(self, tween):
        """
        Pauses the given tween.

        :param tween: The tween to pause (it just won't be updated, but is still registered).
        """
        for active in self._active_tweens:
            if tween == active:
                self._active_tweens.remove(active)
                self._paused_tweens.append(active)
                tween.state = self.TweenStates.paused
                if self._logger:
                    self._logger.debug("tween paused for obj: %s attr: %s", tween.o.__class__, tween.a)
                break
        else:
            if self._logger:
                self._logger.debug("tween not found for pausing for obj: %s attr: %s", tween.o.__class__, tween.a)

    def resume_tween(self, tween):
        """
        Resumes the given tween.

        :param tween: The tween to resume (it will be updated again).
        """
        for paused in self._paused_tweens:
            if tween == paused:
                self._paused_tweens.remove(paused)
                self._active_tweens.append(paused)
                tween.state = self.TweenStates.active
                if self._logger:
                    self._logger.debug("tween resumed for obj: %s attr: %s", tween.o.__class__, tween.a)
                break
            else:
                if self._logger:
                    self._logger.warning("trying to resume non existing or not paused tween for obj: %s attr: %s", tween
                                         .o.__class__, tween.a)
        else:
            if self._logger:
                self._logger.debug("tween not found for resuming for obj: %s attr: %s", tween.o.__class__, tween.a)

    def remove_tween(self, tween):
        """
        Removes the given tween from this tweener independent of its state (active as also paused tween are removed).

        :param tween: The tween to remove.
        """
        if tween in self._active_tweens:
            self._active_tweens.remove(tween)
        if tween in self._paused_tweens:
            self._paused_tweens.remove(tween)
        if self._logger:
            self._logger.debug("tween removed from active and paused for obj: %s attr: %s", tween.o.__class__, tween.a)

    def get_all_tweens_for(self, obj, state=None):
        """
        Returns all tween for a given object according to the state filter. Raises an UnknownTweenStateException if a
        invalid state filter is provided.

        :param obj: The object where the tween operates on.
        :param state: The filter so only tween for that object in that state are returned. If set to None all tweens
            for that object are returned. See Tweener.TweenState.
        :return: List of tweens for the given object.
        """
        if state is not None and state not in TweenStateNames:
            raise UnknownTweenStateException(state)

        result = []
        if state is None or state == self.TweenStates.active:
            for tween in self._active_tweens:
                if tween.o == obj:
                    result.append(tween)
        if state is None or state == self.TweenStates.paused:
            for tween in self._paused_tweens:
                if tween.o == obj:
                    result.append(tween)
        if self._logger:
            self._logger.debug("get all tweens with filter '%s': %s", state, result)
        return result

    def start_tween(self, tween, immediate=False):
        """
        Starts a tween. If the tween is not created by the same instance of a tweener then a
        TweenBelongsToOtherTweenerException is raised.

        :param tween: The tween to start.
        :param immediate: If set to True, then the initial value is set directly after creation (even when delayed).
            Default is False.
        """
        tween.stop()

        if tween.tweener != self:
            raise TweenBelongsToOtherTweenerException("Tween has been created with another tweener!")

        # reset tween
        tween.t = -float(tween.delay)
        tween.v = tween.b

        # make it active
        self._active_tweens.append(tween)
        tween.state = self.TweenStates.active

        # calculate first value if immediate is True
        if immediate is True:
            value = tween.f(0.0, tween.b, tween.c, tween.d, tween.p)
            setattr(tween.o, tween.a, value)

    def get_all_tweens(self, state=None):
        """
        A method to retrieve the active and paused tweens.

        :param state: The filter for the state to query. All tweens are returned if set to None, otherwise take a
            look at Tweener.TweenState.
        :return: Returns a list of tweens, filtered by state if not None.
        """
        if state is not None and state not in TweenStateNames:
            raise UnknownTweenStateException(state)

        result = []
        if state is None or state == self.TweenStates.active:
            result += list(self._active_tweens)

        if state is None or state == self.TweenStates.paused:
            result += list(self._paused_tweens)

        return result


ease_functions = [

    ease_in_sine,
    ease_out_sine,
    ease_in_out_sine,
    ease_out_in_sine,

    ease_in_quad,
    ease_out_quad,
    ease_in_out_quad,
    ease_out_in_quad,

    ease_in_cubic,
    ease_out_cubic,
    ease_in_out_cubic,
    ease_out_in_cubic,

    ease_in_quart,
    ease_out_quart,
    ease_in_out_quart,
    ease_out_in_quart,

    ease_in_quint,
    ease_out_quint,
    ease_in_out_quint,
    ease_out_in_quint,

    ease_in_expo,
    ease_out_expo,
    ease_in_out_expo,
    ease_out_in_expo,

    ease_in_circ,
    ease_out_circ,
    ease_in_out_circ,
    ease_out_in_circ,

    ease_in_elastic,
    ease_out_elastic,
    ease_in_out_elastic,
    ease_out_in_elastic,

    ease_in_back,
    ease_out_back,
    ease_in_out_back,
    ease_out_in_back,

    ease_in_bounce,
    ease_out_bounce,
    ease_in_out_bounce,
    ease_out_in_bounce,

    ease_linear,
    ease_in_sine2,
    ease_in_sine3,
    ease_random_int_bounce,

]


def show_tweens_demo():  # pragma: nocover
    """
    The demo code for tweens. Shows some graphs.
    """
    class BoxWithLabel(object):

        # noinspection PyShadowingNames
        def __init__(self, box, label, label_dist=5):
            self.label = label
            self.rect = box
            # self.rect = box.inflate(2, 2)
            self.label_rect = label.get_rect(topleft=(box.left, box.bottom + label_dist))

        def draw(self, surf, color):
            pygame.draw.rect(surf, color, self.rect, 1)
            surf.blit(self.label, self.label_rect)

    class Slider(object):

        # noinspection PyShadowingNames
        def __init__(self, box, start, end, label):
            self.box = box
            self.start = start
            self.end = end
            self.label = label

        def draw(self, surf):
            start_pos = (self.start, self.box.centery)
            end_pos = (self.end, self.box.centery)
            pygame.draw.line(surf, grey, start_pos, end_pos, 3)
            pygame.draw.rect(surf, red, self.box)
            surf.set_at(self.box.center, white)
            surf.blit(self.label, (self.start, self.box.bottom + self.label.get_size()[1] / 3))

    # noinspection PyShadowingNames
    def reset(screen_w, tile_w, tile_h, spacing, margin, ease_functions, duration, trail_rects, boxes, tweener, trails,
              sliders):
        # clear
        trail_rects[:] = []
        boxes[:] = []
        trails.clear()
        tweener.clear()
        sliders[:] = []

        # calculate the coordinates for the boxes and sliders
        tile_w_spacing = tile_w + spacing
        tile_row_count = (screen_w - 2 * margin - tile_w - spacing) // tile_w_spacing
        slider_length = (screen_w - 2 * margin) // 4 - 10 * spacing
        slider_and_spacing = slider_length + 10 * spacing
        slider_row_count = (screen_w - 2 * margin) // slider_and_spacing
        label_h = 20
        for idx, func in enumerate(ease_functions):
            left = margin + idx % tile_row_count * tile_w_spacing
            top = margin + idx // tile_row_count * (tile_h + spacing + label_h)

            # tween objects
            r = pygame.Rect(0, 0, 3, 3)
            r.center = (left, top + tile_h)
            trail_rects.append(r)

            params = (-tile_h, 0) if func.__name__ == "ease_random_int_bounce" else None
            tweener.create_tween(r, "centerx", r.centerx, tile_w, duration, ease_linear)
            tweener.create_tween(r, "centery", r.centery, -tile_h, duration, func, params)
            trails[id(r)] = [r.center]  # add first point here so pygame.draw.lines has two points at first draw

            # boxes
            label = font.render(func.__name__, 1, white)
            box_rect = pygame.Rect(left, top, tile_w, tile_h)
            boxes.append(BoxWithLabel(box_rect, label))

            # sliders
            # left = margin + tile_row_count * tile_w_spacing
            left = margin + idx % slider_row_count * slider_and_spacing
            y = margin + idx // slider_row_count * 30 + screen_h // 2 + tile_h
            r = pygame.Rect(left, y, 7, 7)
            r.center = (left, y)

            params = (0, slider_length) if func.__name__ == "ease_random_int_bounce" else None
            tweener.create_tween(r, "centerx", r.centerx, slider_length, duration, func, params)
            slider = Slider(r, left, left + slider_length, label)
            sliders.append(slider)
            trails[id(slider)] = [slider.box.center]

    import pygame

    pygame.init()
    screen_w = 1024
    screen_h = 900

    pygame.display.set_caption("tweening, press any key to reset")
    screen = pygame.display.set_mode((screen_w, screen_h))
    font = pygame.font.Font(None, 15)

    red = (255, 0, 0)
    white = (255, 255, 255)
    grey = (90, 90, 90)
    blue1 = (155, 155, 255)
    blue = (0, 0, 255)

    tile_w = 100
    tile_h = 50
    spacing = 5
    margin = 10

    tween_objects = []
    boxes = []
    trails = {}  # {id : [points]]
    sliders = []
    duration = 2
    tweener = Tweener()
    reset(screen_w, tile_w, tile_h, spacing, margin, ease_functions, duration, tween_objects, boxes, tweener, trails,
          sliders)

    # instructions
    font30 = pygame.font.Font(None, 30)
    instructions_label = font30.render("Press any key to re-start. Escape or close the window to quit.", 1, white, grey)
    instr_rect = instructions_label.get_rect()
    instr_rect.centerx = screen_w * 0.5
    instr_rect.bottom = screen_h - margin

    clock = pygame.time.Clock()
    running = True

    # main loop
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
                else:
                    reset(screen_w, tile_w, tile_h, spacing, margin, ease_functions, duration, tween_objects, boxes,
                          tweener, trails, sliders)

        # update
        dt = clock.tick(60) * 0.001  # convert to seconds
        tweener.update(dt)

        # record current position for drawing the trail
        for r in tween_objects:
            trails[id(r)].append(r.center)
        for s in sliders:
            trails[id(s)].append(s.box.center)

        # draw
        screen.fill((0, 0, 0))

        # draw boxes and sliders
        for idx, box in enumerate(boxes):
            box.draw(screen, blue1)
            sliders[idx].draw(screen)

        # draw trail of tweened objects
        for t in trails.values():
            # pygame.draw.lines(screen, grey, 0, t, 1)
            pygame.draw.lines(screen, blue, 0, t, 1)
            for p in t:
                screen.set_at(p, white)

        # draw tweened objects
        for r in tween_objects:
            # pygame.draw.rect(screen, (255, 0, 0), r, 0)
            pygame.draw.circle(screen, red, r.center, 2)

        # draw instructions
        screen.blit(instructions_label, instr_rect)

        pygame.display.flip()

logger.debug("imported")


if __name__ == '__main__':  # pragma: nocover

    show_tweens_demo()
