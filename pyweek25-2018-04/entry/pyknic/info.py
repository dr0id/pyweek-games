# -*- coding: utf-8 -*-

"""
This module should provide information about the pyknic module.

besides of:

__version__
__author__

__copyright__
__credits__
__license__
__maintainer__

Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

    For instance:
        * 1.2.0.1 instead of 1.2-a
        * 1.2.1.2 instead of 1.2-b2 (beta with some bug fixes)
        * 1.2.2.3 instead of 1.2-rc (release candidate)
        * 1.2.3.0 instead of 1.2-r (commercial distribution)
        * 1.2.3.5 instead of 1.2-r5 (commercial distribution with many bug fixes)


.. versionchanged:: 4.0.2.0
    renamed test to tests
    many improvements in the timing module: introduced Timer, FrameCap and made classes compatible in update signature
    moved pyknicpygame to pyknic.pygame, needs to be imported separately
    added more helper classes: simplestatemachine, animation, tweening, ...


.. versionchanged:: 3.0.1.1
    removed: VERSION, VERSIONNUMBER
    added: version, version_info, __versionnumber__
    __versionnumber__ is a tuple containing ints according to the versioning scheme.


"""
from __future__ import print_function, division

import logging
import sys
import warnings

logger = logging.getLogger(__name__)
logger.debug("importing...")

__versionnumber__ = (4, 0, 2, 1)
__version__ = ".".join([str(num) for num in __versionnumber__])
__version_info__ = __versionnumber__

__author__ = "DR0ID (C) 2011-2017"

__copyright__ = "Copyright 2011-2017, DR0ID"
__credits__ = ["DR0ID"]
__license__ = "New BSD license"
__maintainer__ = "DR0ID"

__all__ = ["version", "version_info"]

version = __version__
version_info = __versionnumber__


class VersionMismatchError(Exception):
    pass


# check python version
def check_version(library_name, lower_version, actual_version, upper_version, excluded_versions=()):
    """
    Check version compatibility.

    ::

        # check python version
        check_version('python', (2, 7), tuple(sys.version_info[:2]), (3, 7))

    :param library_name: the name of the library to check
    :param lower_version: the lowest supported version
    :param actual_version: the actual found version
    :param upper_version: the next, upper unsupported version
    :param excluded_versions: excluded version in the range of the lower to upper supported version
    """
    logger.debug("checking version: {0}: {1} <= {2} <= {3}", library_name, lower_version, actual_version, upper_version)
    if not upper_version > actual_version:
        msg_string = "checking version: {0}: {1} <= {2} <= {3}! The lib has not been tested with the version you are " \
                     "trying to use. It might work or not. Try at your own risk"
        msg = msg_string.format(library_name, lower_version, actual_version, upper_version)
        logger.error(msg)
        warnings.warn(msg_string)

    if not actual_version >= lower_version:
        msg = "unsupported version {0} for '{1}', should be > {2} ".format(actual_version, library_name, lower_version)
        version_exception = VersionMismatchError(msg)
        logger.error(version_exception)
        raise version_exception

    for exclude_version in excluded_versions:
        if not exclude_version >= lower_version or not exclude_version <= upper_version:
            out_or_range_msg = "excluded version should be in version range of [{0}, {1}]".format(lower_version,
                                                                                                  upper_version)
            raise ValueError(out_or_range_msg)
        if not actual_version != exclude_version:
            msg_string = "unsupported version {0} for '{1}', should be != {2}"
            msg = msg_string.format(actual_version, library_name, exclude_version)
            version_exception = VersionMismatchError(msg)
            logger.error(version_exception)
            raise version_exception


def log_info(log_separator_width=50):
    logger.info("*" * log_separator_width)
    logger.info("*" * log_separator_width)
    logger.info("*" * log_separator_width)
    _format_string = "%-" + str(log_separator_width) + "s"
    logger.info(_format_string % ("pyknic v" + version + " (c) DR0ID 2011-2016"))
    logger.info("using python version: " + sys.version)


logger.debug("imported")
