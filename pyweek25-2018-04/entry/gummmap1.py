# -*- coding: utf-8 -*-
"""gummmap1.py - map demo and re-learning for Gumm

Keys:
    left, right, up, down       Move
    tab                         cycle gui area
    escape                      quit
"""

import pygame
from pygame import KEYDOWN, KEYUP, QUIT, K_ESCAPE, K_RIGHT, K_LEFT, K_DOWN, K_UP, K_TAB

from pyknic.pygame_wrapper.transform import to_grey_scale
from pytmxloader import pytmxloader
from pytmxloader import TILE_LAYER_TYPE


def get_cached_image(img_cache, ti):
    key = (ti.tileset.image_path_rel_to_cwd, ti.spritesheet_x, ti.spritesheet_y, ti.flip_x, ti.flip_y, ti.angle)
    img = img_cache.get(key, None)
    if img is None:
        spritesheet = img_cache.get(ti.tileset.image_path_rel_to_cwd, None)
        if spritesheet is None:
            # noinspection PyUnresolvedReferences
            spritesheet = pygame.image.load(ti.tileset.image_path_rel_to_cwd).convert_alpha()
            # spritesheet = to_grey_scale(spritesheet)  # this is slower than just converting the single images
            img_cache[ti.tileset.image_path_rel_to_cwd] = spritesheet
        area = pygame.Rect(ti.spritesheet_x, ti.spritesheet_y, ti.tileset.tile_width, ti.tileset.tile_height)
        img = pygame.Surface(area.size, spritesheet.get_flags(), spritesheet)
        img.blit(spritesheet, (0, 0), area)
        img = pygame.transform.flip(img, ti.flip_x, ti.flip_y)
        if ti.angle != 0:
            img = pygame.transform.rotate(img, ti.angle)
        ip = ti.tileset.image_path_rel_to_cwd
        if 'space1_0_4x_smooth.png' in ip:
            img = img.convert()
        # img = to_grey_scale(img)
        img_cache[key] = img
    return img


def load_map(name):
    img_cache = {}
    sprites = {}

    map_info = pytmxloader.load_map_from_file_path(name)
    for idx, layer in enumerate(map_info.layers):
        if not layer.visible:
            continue
        if layer.layer_type == TILE_LAYER_TYPE:
            for tile_y in range(layer.height):
                for tile_x in range(layer.width):
                    pygame.event.pump()
                    tile = layer.data_yx[tile_y][tile_x]
                    if tile is None:
                        continue
                    pos = tile.tile_x + tile.offset_x, tile.tile_y + tile.offset_y
                    ti = tile.tile_info
                    img = get_cached_image(img_cache, ti)

                    kind = ti.properties.get("kind", None)
                    if kind != "background":
                        sprites[pos] = img

        elif layer.layer_type == pytmxloader.OBJECT_LAYER_TYPE:
            for ot in layer.tiles:
                pass

    return sprites, map_info


def main():
    pygame.init()
    clock = pygame.time.Clock()
    size_pick = 0

    def calc_screen():
        screen = pygame.display.set_mode([(1024, 768), (736, 768), (640, 768), (488, 768)][size_pick])
        screen_rect = screen.get_rect()
        if screen_rect.w > 488:
            gui_screen = screen.subsurface([(0, 0, 576, 768), (0, 0, 288, 768), (0, 0, 192, 768)][size_pick])
            scene_screen = screen.subsurface([(576, 0, 448, 768), (288, 0, 448, 768), (192, 0, 448, 768)][size_pick])
            gui_rect = gui_screen.get_rect()
            scene_rect = scene_screen.get_rect(x=gui_rect.w)
        else:
            gui_screen = None
            gui_rect = None
            scene_screen = screen
            scene_rect = screen_rect
        pygame.display.set_caption('Size_Pick={} Gui_Area={}'.format(
            size_pick, None if gui_rect is None else tuple(gui_rect)))
        return screen, screen_rect, gui_screen, gui_rect, scene_screen, scene_rect

    screen, screen_rect, gui_screen, gui_rect, scene_screen, scene_rect = calc_screen()
    sprites, map_info = load_map(['data/map/gumm1.json', 'data/map/game_map.json'][1])
    camera = screen_rect.copy()
    layer0 = map_info.layers[0]
    mw, mh, tw, th = layer0.width, layer0.height, layer0.tile_w, layer0.tile_h
    camera.bottom = mh * th
    move_x = 0
    move_y = 0

    while 1:
        clock.tick(120)
        for e in pygame.event.get():
            if e.type == KEYDOWN:
                if e.key == K_ESCAPE:
                    quit()
                elif e.key == K_RIGHT:
                    move_x += 1
                elif e.key == K_LEFT:
                    move_x -= 1
                elif e.key == K_DOWN:
                    move_y += 1
                elif e.key == K_UP:
                    move_y -= 1
                elif e.key == K_TAB:
                    size_pick = (size_pick + 1) % 4
                    screen, screen_rect, gui_screen, gui_rect, scene_screen, scene_rect = calc_screen()
            elif e.type == KEYUP:
                if e.key == K_RIGHT:
                    move_x -= 1
                elif e.key == K_LEFT:
                    move_x += 1
                elif e.key == K_DOWN:
                    move_y -= 1
                elif e.key == K_UP:
                    move_y += 1
            elif e.type == QUIT:
                quit()

        if move_x or move_y:
            camera.x += move_x
            camera.y += move_y

        scene_screen.fill(pygame.Color('skyblue'))
        for x in range(camera.x, camera.x + scene_rect.w + 64, 64):
            for y in range(camera.y, camera.y + scene_rect.h + 64, 64):
                get_x = x // 64 * 64
                get_y = y // 64 * 64
                image = sprites.get((get_x, get_y))
                if image:
                    scene_screen.blit(image, (get_x - camera.x, get_y - camera.y))
        pygame.display.flip()



if __name__ == '__main__':
    main()
