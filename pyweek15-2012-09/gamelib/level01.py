#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
TODO: docstring
"""

__version__ = '$Id: __init__.py 27 2009-05-06 21:37:20Z dr0iddr0id $'

import random

import pygame

import pyknicpygame
from pyknicpygame.pyknic.mathematics import Point2
from pyknicpygame.pyknic.mathematics import Vec2


import model
import levels
import settings

from pyknicpygame.pyknic.timing import Scheduler

import firesprite

class Player(object):
    
    def __init__(self, model):
        self.position = Vec2(0, 0)
        self.dir = Vec2(0, 0)
        self.vel = Vec2(0, 0)
        self.speed = settings.player_speed
        self.ignition = settings.player_ignition
        self.model = model
        self.left_pressed = 0
        self.right_pressed = 0
        self.up_pressed = 0
        self.down_pressed = 0
        self.health = 100
        self.temp = 0
        
    def look_at(self, x, y):
        self.dir = Vec2(x, y) - self.position
        if self.dir.length_sq > 40*40:
            self.dir.length = 40
        
    def set_flooding(self, yes_or_no):
        self.flooding = yes_or_no
        
    def update(self, dt):
        if self.flooding:
            p = self.position + self.dir
            px, py = p.as_tuple()
            self.model.cool_down(px, py, 2, 2)
            if self.position.get_distance(p) < 2:
                self.temp -= 2
                if self.temp <= 0:
                    self.temp = 0
            
        if self.temp > 0:
            self.health -= self.temp * 0.01
            
        self.position += self.vel * self.speed * dt
        cell = self.model.cells.get((self.position.x // self.model.cell_size_x, 
                                    self.position.y // self.model.cell_size_y), None)
        if cell and cell.temp > self.ignition and self.temp < self.ignition:
            self.temp = cell.temp
            
    def on_event(self, logical_event, event):
        if settings.player_move_left == logical_event:
            self.left_pressed = 1
        elif settings.player_stop_left == logical_event:
            self.left_pressed = 0
            
        elif settings.player_move_right == logical_event:
            self.right_pressed = 1
        elif settings.player_stop_right == logical_event:
            self.right_pressed = 0
            
        elif settings.player_move_up == logical_event:
            self.up_pressed = 1
        elif settings.player_stop_up == logical_event:
            self.up_pressed = 0
            
        elif settings.player_move_down == logical_event:
            self.down_pressed = 1
        elif settings.player_stop_down == logical_event:
            self.down_pressed = 0
            
        elif settings.player_dir == logical_event:
            ex, ey = event.pos
            self.look_at(ex, ey)
            
        elif settings.player_stop_flooding == logical_event:
            self.set_flooding(False)
        elif settings.player_start_flooding == logical_event:
            self.set_flooding(True)
            
        self.vel = Vec2(self.right_pressed - self.left_pressed, self.down_pressed - self.up_pressed).normalized
        # print('??? palyer event', logical_event, event, self.position, self.vel)
        # print(logical_event, self.vel)

class GridHeatSource(firesprite.HeatSource):

    def __init__(self, model):
        firesprite.HeatSource.__init__(self)
        self.model = model
        
        self.sources = []
        # todo: density!!
        for i in range(10):
            rsurf = pygame.Surface((self.model.cell_size_x, self.model.cell_size_y), pygame.SRCALPHA)
            self.sources.append(rsurf)
            rsurf.fill((0,0,0,0))
            rw = self.model.cell_size_x
            y = self.model.cell_size_y // 2
            for x in range(rw):
                # color = (max(0, min(255, int(self.model.cells[(x, y)].temp * 10))), )*3
                color = self.palette.get_at((random.randint(0, self.palette.get_size()[0] - 1), 0))
                # base = 150
                # alpha = int((255-base) * ratio) + base
                # # alpha = 255
                # color = (color[0], color[1], color[2], alpha)
                # surf.set_at((sx, sy + random.randint(-rh//2, rh//2)), color)
                rsurf.set_at((x, y + random.randint(-y, y)), color)
            
    def apply(self, surf):
        for pos, cell in self.model.cells.items():
            if cell.temp > cell.ignition and cell.ignition > 0:
                cx, cy = pos
                src = pygame.transform.flip(self.sources[random.randint(0, 9)], random.randint(0,1), random.randint(0,1))
                # surf.blit(src, (cx * self.model.cell_size_x, cy * self.model.cell_size_y), None, pygame.BLEND_RGBA_ADD))
                surf.blit(src, (cx * self.model.cell_size_x, cy * self.model.cell_size_y))
    
    
    def __apply(self, surf):
        # todo
        rw = rh = 10
        rect = pygame.Rect(0, 0, rw, rh)
        # surf.lock()
        for pos, cell in self.model.cells.items():
                cx, cy = pos
        # for cy in range(self.model.cells_count_y):
            # for cx in range(self.model.cells_count_x):
                # cell = self.model.cells[(cx, cy)]
                # if cell.has_fire():
                if cell.temp > cell.ignition and cell.ignition > 0:
                    # return
                    ratio = cell.temp / self.model.max_temp
                    for i in range(rw):
                        sx = cx * rw + i
                        sy = cy * rh - rh//2
                        
                        # color = (max(0, min(255, int(self.model.cells[(x, y)].temp * 10))), )*3
                        color = self.palette.get_at((random.randint(0, self.palette.get_size()[0] - 1), 0))
                        base = 150
                        alpha = int((255-base) * ratio) + base
                        # alpha = 255
                        color = (color[0], color[1], color[2], alpha)
                        # surf.set_at((sx, sy + random.randint(-rh//2, rh//2)), color)
                        surf.set_at((sx, sy + random.randint(-2, 2)), color)
        # surf.unlock()
                

        
        

        
class Level01(pyknicpygame.pyknic.context.Context):


    def __init__(self):
        pass

    def enter(self):
        """Called when this context is pushed onto the stack."""
        
        pygame.event.clear()
        self.scheduler = Scheduler()
        self.model = model.Model(self.scheduler)
        
        
        sw, sh = settings.resolution
        
        self.renderer = pyknicpygame.spritesystem.DefaultRenderer()
        self.cam = pyknicpygame.spritesystem.Camera(pygame.Rect(0, 0, sw, sh), position=Point2(sw/2, sh/2))
        
        self.info = pyknicpygame.spritesystem.TextSprite("", Point2(0, 0), anchor='bottomleft', color=(100, 100, 100))
        self.renderer.add_sprite(self.info)
        
        self.player = Player(self.model)
        
        surf = pygame.Surface((10, 10))
        surf.fill((255, 255, 0))
        p = Point2()
        p.copy_values(self.player.position)
        self.player_spr = pyknicpygame.spritesystem.Sprite(surf, p, anchor='center')
        self.renderer.add_sprite(self.player_spr)
        
        surf = pygame.Surface((5, 5))
        surf.fill((0, 0, 255))
        p = Point2()
        p.copy_values(self.player.position + self.player.dir)
        self.water_spr = pyknicpygame.spritesystem.Sprite(surf, p, anchor='center')
        self.renderer.add_sprite(self.water_spr)
        
        
        self._load_level(levels.level04)
        self.player.set_flooding(False)
        
        self.win_cond_id = self.scheduler.schedule(self.check_win_condition, 1.0)
        
        # self.firesprite = firesprite.FireSprite(settings.resolution, (0, 0))
        self.firesprite = firesprite.FireSprite((200, 200), (0, 0))
        self.firesprite.add_heat_source(GridHeatSource(self.model))
        
    def check_win_condition(self):
        print('checking win conditions:')
        print('     health: ', self.player.health)
        print('player temp: ', self.player.temp)
        print('     fires : ', len(self.model.fires))
        if self.player.health <= 0:
            print('LOOOSEEE!!')
        if not self.model.is_burning():
            print('WWIIIIIINN!')
        return 1.0
        
    def _load_level(self, level):
        self.model.load_level(levels.level04)
        px, py = levels.level04["player_pos"]
        self.player.position = Vec2(px, py)

    def exit(self):
        """Called when this context is popped off the stack."""
        # self.scheduler.remove(self.win_cond_id)
        self.scheduler.clear()
        
    def suspend(self):
        """Called when another context is pushed on top of this one."""
        pass
    def resume(self):
        """Called when another context is popped off the top of this one."""
        self.player.set_flooding(False)
        
    def think(self, delta_time):
        """Called once per frame"""
        # self.model.update(delta_time)
        self.scheduler.update(delta_time)
        self.player.update(delta_time)
        self.player_spr.position.copy_values(self.player.position)
        self.water_spr.position.copy_values(self.player.position + self.player.dir)
        self.process_events()
        self.firesprite.update(delta_time)
        
    def process_events(self):
        for event in pygame.event.get():
            logical_event = None
            # if pygame.MOUSEBUTTONDOWN == event.type:
            if pygame.MOUSEBUTTONDOWN == event.type:
                # self.player.set_flooding(True)
                # print(event)
                logical_event = settings.key_map.get((event.type, event.button), None)
            elif pygame.MOUSEBUTTONUP == event.type:
                logical_event = settings.key_map.get((event.type, event.button), None)
                # self.player.set_flooding(False)
            elif pygame.MOUSEMOTION == event.type:
                logical_event = settings.key_map.get((event.type, None), None)
                
                x, y = event.pos
                # self.player.look_at(x, y)
                # if pygame.mouse.get_pressed() != (0, 0, 0):
                    # amount = 2
                    # self.model.cool_down(x, y, 2, amount)
                self.info.position.x = x
                self.info.position.y = y
                cx = x // 10
                cy = y // 10
                cell = self.model.cells.get((cx, cy), None)
                if cell:
                    self.info.text = "{0}: {1}".format((cx, cy), str(cell.temp))
                else:
                    self.info.text = "None"
                
            elif pygame.QUIT == event.type:
                pyknicpygame.pyknic.context.pop()
            elif event.type in [pygame.KEYDOWN, pygame.KEYUP]:
                logical_event = settings.key_map.get((event.type, event.key), None)
                
            if logical_event:
                self.player.on_event(logical_event, event)
                
        
    def draw(self, screen):
        """Refresh the screen"""
        # print('----------')
        # for y in range(self.model.cells_count_y):
            # s = ''
            # for x in range(self.model.cells_count_x):
                # s +=  "{0}, ".format(str(self.model.cells[(x, y)].temp))
            # print(s)
        screen.fill((0, 0, 0))
        # rw = rh = 10
        # rect = pygame.Rect(0, 0, rw, rh)
        # for y in range(self.model.cells_count_y):
            # for x in range(self.model.cells_count_x):
                # rect.topleft = (x*rw, y*rh)
                # color = (max(0, min(255, int(self.model.cells[(x, y)].temp * 10))), )*3
                # screen.fill(color, rect)
        self.renderer.draw(screen, self.cam)
        screen.blit(self.firesprite.image, self.firesprite.rect)
        pygame.display.flip()