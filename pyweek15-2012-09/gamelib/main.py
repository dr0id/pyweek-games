#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
TODO: docstring
"""

__version__ = '$Id: main.py 239 2009-07-27 20:41:19Z dr0iddr0id $'

# do not use __file__ because it is not set if using py2exe

# put your imports here

import logging
# logging.getLogger().setLevel(logging.DEBUG)
logging.getLogger().setLevel(logging.INFO)

import pygame

import pyknicpygame
# call init before using any of the pyknicpygame/pyknic modules!!
pyknicpygame.init()

import menu
import settings

def main():
    # put here your code
    
    pygame.init()
    pygame.display.set_caption(settings.caption)
    
    screen = pygame.display.set_mode(settings.resolution, 0, 32)
    
    co = menu.Menu()
    pyknicpygame.pyknic.context.push(co)
    
    lock_stepper = pyknicpygame.pyknic.timing.LockStepper()

    clock = pygame.time.Clock()
    context_len = pyknicpygame.pyknic.context.length
    context_top = pyknicpygame.pyknic.context.top
    
    lock_stepper.event_integrate.add(lambda ls, dt, simt: context_top().think(dt) if context_top() else None)
    
    while context_len():
        # limit the fps
        dt = clock.tick() / 1000.0 # convert to seconds
        context_top().draw(screen)
        alpha = lock_stepper.update(dt, timestep_seconds=settings.sim_time_step)
        if __debug__:
            fps = clock.get_fps()
            # if fps < 15 or fps > 500:
            print('fps: ', str(fps), alpha)
        
    pygame.quit()
    
    

        

# this is needed in order to work with py2exe
if __name__ == '__main__':
    main()
