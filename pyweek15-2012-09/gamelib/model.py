#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
TODO: docstring
"""

__version__ = '$Id: __init__.py 27 2009-05-06 21:37:20Z dr0iddr0id $'

import random
import logging

log = logging.getLogger('model')

import pyknicpygame.pyknic.timing


class Fire(object):
    
    def __init__(self, x, y, size, max_size, burn_rate, update_interval):
        self.x = x
        self.y = y
        self.size = size
        self.burn_rate = burn_rate
        self.max_size = max_size
        self.update_interval = update_interval


class Cell(object):

    def __init__(self):
        self.fire = None
        self.temp = 0
        self.ignition = 0 # 0 disabled, no fire possible
        
    def has_fire(self):
        return (self.fire is not None)


class Model(object):

    def __init__(self, scheduler):
        self.cells = {} # {(x,y) : Cell}
        self.scheduler = scheduler
        self.cells_count_x = 0
        self.cells_count_y = 0
        self.max_temp = 100
        self.cell_size_x = 0
        self.cell_size_y = 0
        self.fires = []
        
    def is_burning(self):
        for cell in self.cells.values():
            if cell.temp > cell.ignition:
                return True
        return False
        
        
    def update(self, dt):
        # self.scheduler.update(dt)
        pass
        
    def cool_down(self, x, y, size, amount):
        cx = int(x // self.cell_size_x)
        cy = int(y // self.cell_size_y)
        for y in range(cy - size, cy + size, 1):
            for x in range(cx - size, cx + size, 1):
                if x < 0 or x >= self.cells_count_x:
                    continue
                if y < 0 or y >= self.cells_count_y:
                    continue
                cell = self.cells[(x, y)]
                dist_inv = 1.0 / (((cx - x)**2 + (cy - y)**2 )**0.5 + 1)
                cell.temp -= amount * dist_inv
                cell.temp = 0 if cell.temp <= 0 else cell.temp
                if cell.has_fire():
                    cell.fire.size -= 2
                    if cell.temp < cell.ignition:
                        # cell.fire = None
                        # cell.temp = cell.ignition
                        self.remove_fire(cell)
                        # print("removing fire: {0}, {1}".format(x, y))
                        
    def remove_fire(self, cell):
        if cell.has_fire():
            self.scheduler.remove(cell.fire.id)
            self.fires.remove(cell.fire)
            cell.fire = None

    def create_fire(self, x, y, size, max_size, burn_rate, update_interval):
        # log.info("creating fire: {0}, {1}, {2}, {3}, {4}, {5}".format(x, y, size, max_size, burn_rate, update_interval))
        # print("creating fire: {0}, {1}, {2}, {3}, {4}, {5}".format(x, y, size, max_size, burn_rate, update_interval))
        fire = Fire(x, y, size, max_size, burn_rate, update_interval)
        fire.id = self.scheduler.schedule(self._update_fire, update_interval, random.random() * update_interval, fire)
        cell = self.cells.get((x, y), Cell())
        cell.fire = fire
        cell.temp = cell.ignition
        self.fires.append(fire)

    def _update_fire(self, fire):
        # update grid
        for y in range(fire.y - fire.size, fire.y + fire.size, 1):
            for x in range(fire.x - fire.size, fire.x + fire.size, 1):
                if x < 0 or x >= self.cells_count_x:
                    continue
                if y < 0 or y >= self.cells_count_y:
                    continue
                cell = self.cells[(x, y)]
                if cell.ignition > 0:
                    dist_inv = 1.0 / (((fire.x - x)**2 + (fire.y - y)**2 )**0.5 + 1)
                    cell.temp = cell.temp + fire.burn_rate * dist_inv
                    cell.temp = cell.temp if cell.temp < self.max_temp else self.max_temp
                    # ignite additional fires
                    if cell.temp > cell.ignition:
                        if not cell.has_fire() and (x + y) % 2 == 0:
                            self.create_fire(x, y, 1, fire.max_size, fire.burn_rate, fire.update_interval)
                            # pass
        
        # todo: fire size growth independent of burn_rate?
        # todo: fire size growth depending on cell temp?
        if fire.size < fire.max_size:
            fire.size += int(1.5 - self.cells[(fire.x, fire.y)].temp / self.max_temp)
        
        if self.cells[(fire.x, fire.y)].has_fire() > 0:
            return fire.update_interval
        return Scheduler.STOPREPEAT
        
    def load_level(self, data):
        self.cells_count_x = data["cells_count_x"]
        self.cells_count_y = data["cells_count_y"]
        self.cell_size_x = data["cell_size_x"]
        self.cell_size_y = data["cell_size_y"]
        self.max_temp = data["max_temp"]
        for y in range(self.cells_count_y):
            for x in range(self.cells_count_x):
                cell = self.cells.get((x, y), Cell())
                cell.temp = data["temp"][y * self.cells_count_x + x]
                cell.ignition = data["ignition"][y * self.cells_count_x + x]
                self.cells[(x, y)] = cell
                
        for x, y, size, max_size, burn_rate, update_interval in data["fires"]:
            self.create_fire(x, y, size, max_size, burn_rate, update_interval)
