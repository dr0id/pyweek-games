#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
TODO: docstring
"""

__version__ = '$Id: main.py 239 2009-07-27 20:41:19Z dr0iddr0id $'

import pygame

import pyknicpygame
from pyknicpygame.pyknic.mathematics import Point2
from pyknicpygame.pyknic.mathematics import Vec2

import settings

import level01

class Item(pyknicpygame.spritesystem.TextSprite):
    font = None
    def __init__(self, key, text, action, pos):
        pyknicpygame.spritesystem.TextSprite.__init__(self, text, pos, anchor="center")
        self.key = key
        self.action = action
        self.screen = None
        
    def focus(self, focus=False):
        if focus:
            self._image.fill((255, 255, 0, 255), None, pygame.BLEND_RGBA_MULT)
            self._update()
        else:
            self.text = self.text



class Menu(pyknicpygame.pyknic.context.Context):


    def __init__(self):
        self.texts = [  ("'s'    Start   " , pygame.K_s, self.go_game), 
                        ("'t'    Tutorial" , pygame.K_t, self.go_tutorial), 
                        ("'c'    Credits " , pygame.K_c, self.go_credits), 
                        ("'e'    Exit    ", pygame.K_e, self.go_exit)]
        self.renderer = None
        self.cam = None
        self.items = []

    def go_game(self):
        lvl = level01.Level01()
        pyknicpygame.pyknic.context.push(lvl)
        
    def go_tutorial(self):
        pass
        
    def go_credits(self):
        pass
        
    def go_exit(self):
        pyknicpygame.pyknic.context.pop()
        
    def enter(self):
        pygame.event.clear()        
        
        sw, sh = settings.resolution
        
        Item.font = pygame.font.Font(None, 30)
        
        self.renderer = pyknicpygame.spritesystem.DefaultRenderer()
        self.cam = pyknicpygame.spritesystem.Camera(pygame.Rect(0, 0, sw, sh), position=Point2(sw/2, sh/2))
        
        y = dy = sh / (len(self.texts) + 1)
        dx = sw / 2
        self.items = []
        for text, key, action in self.texts:
            self.items.append(Item(key, text, action, Point2(dx, y)))
            y += dy # Item.font.get_linesize()
        
        self.renderer.add_sprites(self.items)
        
    def exit(self):
        print('exit')
    def suspend(self):
        print('suspend')
    def resume(self):
        self.enter()
        
    def think(self, delta_time):
        selected_item = None

        #todo: use even.wait() here
        # for event in pygame.event.get():
        for event in [pygame.event.wait()]:
            if event.type == pygame.QUIT:
                self.go_exit()
            elif event.type == pygame.KEYDOWN:
                for item in self.items:
                    if item.key == event.key:
                        selected_item = item
                        break
            elif event.type == pygame.MOUSEBUTTONDOWN:
                hit_items = self.renderer.get_sprites_in_rect(pygame.Rect(event.pos, (1, 1)))
                if hit_items:
                    selected_item = hit_items[0]
            elif event.type == pygame.MOUSEMOTION:
                for item in self.items:
                    item.focus(False)
                hit_items = self.renderer.get_sprites_in_rect(pygame.Rect(event.pos, (1, 1)))
                if hit_items:
                    hit_items[0].focus(True)
        if selected_item:
            selected_item.action()
        
        
    def draw(self, screen):
        self.renderer.draw(screen, self.cam, fill_color=(0,0,0), do_flip=True)

        
       
    
    