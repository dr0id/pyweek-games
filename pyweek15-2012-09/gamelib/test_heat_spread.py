#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
TODO: docstring
"""

__version__ = '$Id: __init__.py 27 2009-05-06 21:37:20Z dr0iddr0id $'

import pygame

class Fire(object):

    def __init__(self, x, y, init_size, burn_rate=1):
        self.x = x
        self.y = y
        self.size = init_size
        self.rate = burn_rate
        

def main():
    
    cells_width = 100
    cells_height = 100
    ignition_temp = {}
    cells = {}
    has_fire = {}
    for y in range(cells_height):
        for x in range(cells_width):
            cells[(x, y)] = 0
            has_fire[(x, y)] = 0
            ignition_temp[(x,y)] = 0
            

    # fires = [   Fire(15, 15, 1, 10), 
                # # Fire(18, 15, 1, 10), 
                # Fire(25, 15, 1, 10), 
                # Fire(35, 15, 1, 10), 
                # Fire(45, 15, 1, 10), 
                # Fire(45, 45, 1, 20)]
                
    has_fire = {(15, 15) : Fire(15, 15, 1, 10), 
                # Fire(18, 15, 1, 10), 
                (25, 15) : Fire(25, 15, 1, 10), 
                (35, 15) : Fire(35, 15, 1, 10), 
                (45, 15) : Fire(45, 15, 1, 10), 
                (45, 45) : Fire(45, 45, 1, 20)
    }
                
    for x in range(5, 10, 1):
        ignition_temp[(x,5)] = 20
    
    counter = 0
    running = True
    
    pygame.init()
    screen = pygame.display.set_mode((800, 600))
    
    while running:
        counter += 1
        # for fire in fires:
        for fire in has_fire.values():
            for y in range(fire.y - fire.size, fire.y + fire.size, 1):
                for x in range(fire.x - fire.size, fire.x + fire.size, 1):
                    # x = x if x < cells_width else cells_width
                    # x = x if x > 0 else 0
                    # y = y if y < cells_height else cells_height
                    # y = y if y > 0 else 0
                    # just ignore the outside cells
                    if x < 0 or x >= cells_width:
                        continue
                    if y < 0 or y >= cells_height:
                        continue
                    # dist_inv = 1.0 / (abs(fire.x - x) + abs(fire.y - y) + 1.0)
                    dist_inv = 1.0 / (((fire.x - x)**2 + (fire.y - y)**2 )**0.5 + 1)
                    cells[(x, y)] = cells[(x, y)] + fire.rate * dist_inv
                    # if has_fire.get((x, y), 0) == 0:
                        # if ignition_temp[(x, y)] > 0 and ignition_temp[(x, y)] <= cells[(x, y)]:
                            # has_fire[(x, y)] = Fire(x, y, 1, 10)
                
            fire.size += 1
        
        # for y in range(cells_height):
            # s = ''
            # for x in range(cells_width):
                # s += "{0}, ".format(str(cells[(x, y)]))
            # print(s)
            
        rw = rh = 5
        rect = pygame.Rect(0, 0, rw, rh)
        for y in range(cells_height):
            for x in range(cells_width):
                rect.topleft = (x*rw, y*rh)
                color = (min(255, int(cells[(x, y)] * 2)), )*3
                screen.fill(color, rect)
        pygame.display.flip()
            
        event = pygame.event.wait()
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                running = False
            elif event.key == pygame.K_r:
                for y in range(cells_height):
                    for x in range(cells_width):
                        cells[(x, y)] = 0
                for fire in has_fire.values():
                    fire.size = 1
            
        


if __name__ == '__main__':
    main()
