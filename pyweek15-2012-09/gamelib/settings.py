#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
TODO: docstring
"""

__version__ = '$Id: __init__.py 27 2009-05-06 21:37:20Z dr0iddr0id $'

import pygame


resolution = (800, 600)
caption = "pyweek15 - The Fire"  # todo


sim_time_step = 0.025

player_speed = 20
player_ignition = 4


# 1<<30  == 1073741824  <- biggest non 'L' int
player_move_left        = 1000
player_move_right       = 1001
player_move_up          = 1002
player_move_down        = 1003
player_stop_left        = 1004
player_stop_right       = 1005
player_stop_up          = 1006
player_stop_down        = 1007
player_dir              = 1008
player_start_flooding   = 1009
player_stop_flooding    = 1010

key_map = {
            (pygame.KEYDOWN, pygame.K_a) : player_move_left, 
            (pygame.KEYDOWN, pygame.K_d) : player_move_right,
            (pygame.KEYDOWN, pygame.K_w) : player_move_up,
            (pygame.KEYDOWN, pygame.K_s) : player_move_down,
            (pygame.KEYUP, pygame.K_a) : player_stop_left, 
            (pygame.KEYUP, pygame.K_d) : player_stop_right,
            (pygame.KEYUP, pygame.K_w) : player_stop_up,
            (pygame.KEYUP, pygame.K_s) : player_stop_down,
            (pygame.KEYDOWN, pygame.K_LEFT) : player_move_left, 
            (pygame.KEYDOWN, pygame.K_RIGHT) : player_move_right,
            (pygame.KEYDOWN, pygame.K_UP) : player_move_up,
            (pygame.KEYDOWN, pygame.K_DOWN) : player_move_down,
            (pygame.KEYUP, pygame.K_LEFT) : player_stop_left, 
            (pygame.KEYUP, pygame.K_RIGHT) : player_stop_right,
            (pygame.KEYUP, pygame.K_UP) : player_stop_up,
            (pygame.KEYUP, pygame.K_DOWN) : player_stop_down,
            (pygame.MOUSEBUTTONDOWN, 1) : player_start_flooding,
            (pygame.MOUSEBUTTONUP, 1) : player_stop_flooding,
            (pygame.MOUSEMOTION, None) : player_dir,
            }


