# -*- coding: utf-8 -*-
import logging

logger = logging.getLogger(__name__)
logger.debug("importing...")

from pygame import Surface
from pygame.math import Vector3 as Vector
from pygame import Rect
from pygame import display
from pygame import event
from pygame.time import Clock
from pygame.font import Font
from pygame import mouse
# from pygame import KEYDOWN, K_0, K_1, K_2, K_3, K_4, K_5, K_6, K_7, K_8, K_9
from pygame import draw
from pygame.locals import *
from pygame import key
from pygame import mixer
from pygame import init
from pygame import image
from pygame import transform
from pygame import font
from pygame import time
from pygame.math import Vector2

logger.debug("imported")
