import random

import pygame
from pygame.locals import *


class Pipe(object):

    def __init__(self, length=5):
        self.queue = [None] * length
        self.opipe = None
        self.rcv = None

    def put(self, data):
        # Try to put a packet on the pipe.
        # If data is None then who cares!
        # If my rcv queue is clear I can take the packet.
        # Else I cannot take it.
        if data is None:
            return True
        if self.rcv is None:
            self.rcv = data
            return True
        return False

    def _shift(self):
        # Shift all the packets along the pipe. Take from rcv queue, which may be None.
        for i in range(len(self.queue) - 1, 0, -1):
            self.queue[i] = self.queue[i - 1]
        self.queue[0] = self.rcv
        self.rcv = None

    def update(self):
        # Take data off the end.
        data = self.queue[-1]
        # If data is not None and the next pipe can take it, then shift my data.
        # Else, just shift it (because the last slot was None and who cares!)
        if data is not None:
            if self.opipe.put(data):
                self._shift()
        else:
            self._shift()


pygame.init()

resolution = 1024, 768
screen = pygame.display.set_mode(resolution)
screen_rect = screen.get_rect()

clock = pygame.time.Clock()
max_fps = 4

pygame.display.set_caption('Space: queue packet | X: drop random packet')

colors = [Color(c) for c in 'red green blue yellow orange grey brown violet brown yellowgreen turquoise'.split()]

# This simulates the Internet feed. A packet waits here until pipe1 is not full.
buf = None

# make and link pipes
pipe1 = Pipe()
pipe2 = Pipe()
pipe3 = Pipe()
pipe1.opipe = pipe2
pipe2.opipe = pipe3
pipe3.opipe = pipe1

size = 40  # size of rendered rects

running = True
while running:
    screen.fill((0, 0, 0))
    rect = Rect(0, 0, size, size)
    if buf:
        screen.fill(buf, rect)
    pygame.draw.rect(screen, Color('white'), rect, 1)
    pipe_row = 0
    for pipe in pipe1, pipe2, pipe3:
        pipe_row += 1
        for i in range(len(pipe.queue)):
            rect = Rect(i * size, pipe_row * size, size, size)
            if pipe.queue[i]:
                screen.fill(pipe.queue[i], rect)
            pygame.draw.rect(screen, Color('white'), rect, 1)
    pygame.display.flip()

    for e in pygame.event.get():
        if e.type == KEYDOWN:
            if e.key == K_ESCAPE:
                running = False
            elif e.key == K_SPACE:
                # queue a packet
                buf = random.choice(colors)
            elif e.key == K_x:
                # drop a random packet
                pipe = random.choice([pipe1, pipe2, pipe3])
                i = random.randrange(len(pipe.queue))
                pipe.queue[i] = None
        elif e.type == QUIT:
            running = False

    pipe1.update()
    pipe2.update()
    pipe3.update()
    if buf and pipe1.put(buf):
        buf = None

    dt = clock.tick(max_fps) / 1000.0
