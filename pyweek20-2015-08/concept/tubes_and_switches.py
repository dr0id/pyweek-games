# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pyweek_games
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function
from collections import defaultdict
from itertools import groupby
import random
import pygame
from concept import tweening

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 

SCREENSIZE = 800, 600

class Node(object):

    def __init__(self, name, message_ids):
        self.name = name
        self.rect = pygame.Rect(25, 25, 20, 20)
        self.packages = []
        self.message_ids = message_ids

    def update(self, graph):
        in_edges = graph.get_edges_to_node(self)
        for current_in_edge in in_edges:
            package = current_in_edge.peek()
            if package:
                # inspect package
                if package.destination == self.name:
                    self.packages.append(package)
                    current_in_edge.remove(package)
                    return

                # send it back
                out_edges = graph.get_edges_from_node(self)
                if out_edges:
                    current_out_edge = random.choice(out_edges)
                    if current_out_edge.put(package):
                        current_in_edge.remove(package)

    def check_messages(self):
        # check if we have all message parts
        current_message_ids = []
        self.packages.sort(key=lambda x: x.message_id)
        groups = groupby(self.packages,key= lambda x: x.message_id)
        for key, group_iter in groups:
            group = list(group_iter)
            p = group[0]
            if p.total != len(group):
                return False
            current_message_ids.append(key)
            
        # check if we have all messages
        current_message_ids.sort()
        self.message_ids.sort()
        if current_message_ids != self.message_ids:
            return False
        
        return True




class Edge(object):

    def __init__(self, node_from, node_to, name=None):
        self.node_from = node_from
        self.node_to = node_to
        self.name = name
        if name is None:
            self.name = "edge_{0}->{1}".format(self.node_from.name, self.node_to.name)


class Tube(Edge):

    def __init__(self, node_from, node_to, capacity=5, name=None):
        Edge.__init__(self, node_from, node_to, name)
        self.queue = [None] * capacity
        self.rect_in = pygame.Rect(20, 20, 15, 15)
        self.rect_out = pygame.Rect(30, 30, 15, 15)
        self.p_per_sec = 1.0  # actually time used to travel
        self.last_p = 0
        self.now = 0
        self.allowed = False

    def peek(self):
        if not self.allowed:
            return None
        self.allowed = False
        for p in reversed(self.queue):
            if p is not None and p.state == Package.State.EdgeEnd:
                return p
        return None

    def remove(self, package):
        idx = self.queue.index(package)
        self.queue[idx] = None
        # self.last_p = self.now

    def put(self, package):
        if len([item for item in self.queue if item is not None]) == len(self.queue):
            return False

        package.set_edge(self)
        self.queue.insert(0, package)
        idx = self.queue.index(None)
        del self.queue[idx]
        return True

    def update(self, dt):
        self.now += dt
        if self.now - self.last_p > self.p_per_sec / len(self.queue):
            self.allowed = True
            self.last_p = self.now



class Graph(object):

    def __init__(self, is_digraph=False):
        self.edges = defaultdict(list)  # {node: [edges]}
        self.is_digraph = is_digraph

    def add_node(self, node):
        self.edges[node] = []

    def remove_node(self, node):
        if node in self.edges:
            del self.edges[node]

    def add_edge(self, edge, bi_directional=True):
        self.edges[edge.node_from].append(edge)
        if self.is_digraph and bi_directional:
            self.edges[edge.node_to].append(Tube(edge.node_to, edge.node_from, len(edge.queue)))

    def remove_edge(self, edge):
        edges = self.edges[edge.node_from]
        if edge in edges:
            edges.remove(edge)
        if self.is_digraph:
            edges = self.edges[edge.node_to]
            if edge in edges:
                edges.remove(edge)

    def get_edges_from_node(self, node):
        """
        Returns all edges departing from node.
        :param node:
        :return:
        """
        return self.edges[node]

    def get_edges_to_node(self, node):
        """
        Returns all edges currently pointing to node.
        :param node:
        :return:
        """
        edges = []
        for node_edges in self.edges.values():
            for edge in node_edges:
                if edge.node_to == node:
                    edges.append(edge)
        return edges

    def add_edge_betwee_nodes(self, node_from, node_to, edge):
        edge.node_from = node_from
        edge.from_to = node_to
        self.add_edge(edge)

    def exists_node(self, name):
        for node in self.edges.keys():
            if node.name == name:
                return node

    def init_switches(self, switches):
        for switch in switches:
            switch.edges_in = self.get_edges_to_node(switch)
            switch.edges_out = self.get_edges_from_node(switch)
            # TODO: randomize
            if switch.edges_in:
                switch.current_in_edge = switch.edges_in[0]
            if switch.edges_out:
                switch.current_out_edge = switch.edges_out[0]

    def get_nodes_of_edge(self, edge):
        s_node = None
        e_node = None
        for node, edges_from in self.edges.items():
            if edge in edges_from:
                s_node = node
            edges_to = self.get_edges_to_node(node)
            if edge in edges_to:
                e_node = node
        return s_node, e_node

class Switch(Node):

    def __init__(self, name):
        Node.__init__(self, name, [])
        self.edges_in = []
        self.edges_out = []
        self.current_in_edge = None
        self.current_out_edge = None

    def switch(self, edge_in, edge_out):
        if edge_in not in self.edges_in:
            # raise Exception("edge_in {0} not found in switch {1}".format(edge_in.name, self.name))
            return
        if edge_out not in self.edges_out:
            # raise Exception("edge_out {0} not found in switch {1}".format(edge_out.name, self.name))
            return

        self.current_in_edge = edge_in
        self.current_out_edge = edge_out

    def update(self, graph):
        if self.current_in_edge:
            package = self.current_in_edge.peek()
            if package and self.current_out_edge:
                if self.current_out_edge.put(package):
                    self.current_in_edge.remove(package)



class Package(object):

    class State:
        Initial = 0
        EdgeStart = 1
        EdgeEnd = 2

    tweener = None

    _color_cache = {}  # {(dest, message_id): color}

    def __init__(self, color, destination, number, total, message_id):
        self.message_id = message_id
        self.total = total
        self.number = number
        self.destination = destination
        self.color = Package._color_cache.get((destination, message_id), None)
        if self.color is None:
            self.color = color
            Package._color_cache[(destination, message_id)] = color
        self.edge = None
        self.state = self.State.Initial
        self.rect = pygame.Rect(0, 0, 10, 10)

    def set_edge(self, edge):
        self.edge = edge
        self.state = self.State.EdgeStart
        # create_tween(self, obj, attr_name, begin, change, duration, tween_function=LINEAR, params=None, cb_end=None,
        self.tweener.create_tween(self.rect, 'x', edge.rect_in.x, edge.rect_out.x - edge.rect_in.x, edge.p_per_sec, cb_end=self.tween_cb)
        self.tweener.create_tween(self.rect, 'y', edge.rect_in.y, edge.rect_out.y - edge.rect_in.y, edge.p_per_sec)

    def tween_cb(self, *args):
        self.state = self.State.EdgeEnd


def rc():
    return random.randint(0, 254)

_node_colors = {
    "A": (rc(), rc(), rc()),
    "B": (rc(), rc(), rc()),
    "C": (rc(), rc(), rc()),
    "D": (rc(), rc(), rc()),
    "E": (rc(), rc(), rc()),
    "F": (rc(), rc(), rc()),
    "G": (rc(), rc(), rc()),
    "H": (rc(), rc(), rc()),
}

def get_node_color(destination):
    color = _node_colors.get(destination, None)
    if color is None:
        _node_colors[destination] = (rc(), rc(), rc())

    return _node_colors[destination]

messages = {
    "C" : [(1, 5), (2, 10), (3, 3), (4, 4)],
    "D" : [(10, 1), (20, 2), (30, 3), (40, 3)]
}

packages = []
for dest, msgs in messages.items():
    for msg_id, total in msgs:
        for i in range(total):
            packages.append(Package(get_node_color(dest), dest, i, total, msg_id))

def create_packages(destination, message_id, total):
    packages = []

    for i in range(total):
        packages.append(Package(get_node_color(destination), destination, i, total, message_id))
    return packages

def get_edge_at(graph, pos):
    c_rect = pygame.Rect(pos, (1, 1))
    edges = []
    for e in graph.edges.values():
        edges += e
    in_idx = c_rect.collidelist([ie.rect_in for ie in edges])
    if in_idx > -1:
        return edges[in_idx], 0
    out_idx = c_rect.collidelist([oe.rect_out for oe in edges])
    if out_idx > -1:
        return edges[out_idx], 1
    return None, -1


def main():
    pygame.init()
    screen = pygame.display.set_mode(SCREENSIZE, pygame.RESIZABLE | pygame.SRCALPHA, 32)
    # screen = pygame.display.set_mode(SCREENSIZE)

    tweener = tweening.Tweener()
    Package.tweener = tweener

    graph = Graph(True)
    nodes = [Node("A", [m[0] for m in messages.get("A", [])]),
             Node("B", [m[0] for m in messages.get("B", [])]),
             Node("C", [m[0] for m in messages.get("C", [])]),
             Node("D", [m[0] for m in messages.get("D", [])]),]
             # Node("E", [m[0] for m in messages.get("E", [])]),
             # Node("F", [m[0] for m in messages.get("F", [])]),
             # Node("G", [m[0] for m in messages.get("G", [])]),
             # Node("H", [m[0] for m in messages.get("H", [])])]

    switches = [Switch("0"),
                Switch("1"),
                Switch("2"),
                Switch("3"),
                Switch("4"),
                Switch("5"),
                Switch("6")]
    tubes = [
        Tube(nodes[0], switches[0]),  # A -> 0
        Tube(nodes[1], switches[0]),  # B -> 0
        Tube(switches[6], nodes[2]),  # 6 -> C
        Tube(switches[6], nodes[3]),  # 6 -> D
        Tube(switches[0], switches[1]),  # 0 -> 1
        Tube(switches[1], switches[6], 2),  # 1 -> 6
        Tube(switches[0], switches[2]),  # 0 -> 2
        Tube(switches[2], switches[6]),  # 2 -> 6
             ]

    # for t in tubes:
    #     t.put(Package((rc(), rc(), rc()), tweener))
    while packages:
        package = random.choice(packages)
        packages.remove(package)
        ok = False
        while not ok:
            tube = random.choice(tubes)
            ok = tube.put(package)

    nodes[0].rect.topleft = (450, 3)
    nodes[1].rect.topleft = (219, 1)
    nodes[2].rect.topleft = (486, 562)
    nodes[3].rect.topleft = (244, 560)
    # nodes[4].rect.topleft = (100, 300)
    # nodes[5].rect.topleft = (100, 300)
    # nodes[6].rect.topleft = (100, 300)
    # nodes[7].rect.topleft = (100, 300)

    switches[0].rect.topleft = (373, 143)
    switches[1].rect.topleft = (449, 241)
    switches[2].rect.topleft = (304, 247)
    switches[3].rect.topleft = (109, 328)
    switches[4].rect.topleft = (114, 376)
    switches[5].rect.topleft = (100, 364)
    switches[6].rect.topleft = (381, 345)

    for edge in tubes:
        graph.add_edge(edge)

    graph.init_switches(switches)
    for node in nodes + switches:
        move_edges_points(graph, node)

    fps = 60
    clock = pygame.time.Clock()

    selected_node = None
    selected_edge = None
    place_p_idx = 0

    running = True
    while running:
        dt = clock.tick(fps) / 1000.0  # convert to seconds

        # events
        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
                elif event.key == pygame.K_SPACE:
                    # idx = random.randint(0, len(tubes) * 1000) % len(tubes)
                    # color = (rc(), rc(), rc())
                    # package = Package(color, tweener)
                    if packages:
                        package = random.choice(packages)
                        packages.remove(package)
                        if tubes[place_p_idx].put(package):
                            place_p_idx = (place_p_idx + 1) % len(tubes)
                elif event.key == pygame.K_p:
                    for n in nodes + switches:
                        print(n.name, n.rect)

            elif event.type == pygame.MOUSEBUTTONDOWN:
                selected_node = None
                mouse_r = pygame.Rect(event.pos, (1, 1))
                _nodes = list(graph.edges.keys())
                idx = mouse_r.collidelist([n.rect for n in _nodes])
                if idx > -1:
                    selected_node = _nodes[idx]
                selected_edge = get_edge_at(graph, event.pos)
            elif event.type == pygame.MOUSEBUTTONUP:
                if selected_node:
                    selected_node.rect.center = event.pos
                    move_edges_points(graph, selected_node)
                other_edge, ot = get_edge_at(graph, event.pos)

                if selected_edge[0] is not None and other_edge is not None and selected_edge[0] is not other_edge:
                    for ns in graph.get_nodes_of_edge(selected_edge[0]):
                        for no in graph.get_nodes_of_edge(other_edge):
                            if ns == no and ot != selected_edge[1]:
                                try:
                                    if ot != 0:
                                        no.switch(other_edge, selected_edge[0])
                                    else:
                                        no.switch(selected_edge[0], other_edge)
                                except AttributeError:
                                    pass

        # draw
        for tt in graph.edges.values():
            for tube in tt:
                pygame.draw.line(screen, (0, 200, 0), tube.rect_in.center, tube.rect_out.center, 3)
                pygame.draw.rect(screen, (255, 255, 255), tube.rect_in, 0)
                pygame.draw.rect(screen, (255, 255, 0), tube.rect_out, 0)
                div = 5.0 / 4.0
                r_size = 7
                rect = pygame.Rect(tube.rect_in.x + (tube.rect_out.x - tube.rect_in.x) // div, tube.rect_in.y + (tube.rect_out.y - tube.rect_in.y) // div, r_size, r_size)
                for p in tube.queue:
                    rect.move_ip(r_size, 0)
                    if p is not None:
                        screen.fill(p.color, rect)
                    pygame.draw.rect(screen, (255, 255, 255), rect, 1)
                tube.update(dt)
                for p in tube.queue:
                    if p is not None:
                        pygame.draw.rect(screen, p.color, p.rect)

        # update
        tweener.update(dt)
        for node in graph.edges.keys():
            # pygame.draw.circle(screen, (100, 100, 100), node.rect.topleft, node.rect.width, 1)
            pygame.draw.rect(screen, (100, 100, 100), node.rect, 1)
            node.update(graph)

        for switch in switches:
            if switch.current_in_edge is not None and switch.current_out_edge is not None:
                pygame.draw.line(screen, (255, 0, 0), switch.current_in_edge.rect_out.center, switch.current_out_edge.rect_in.center, 3)
            # switch.update(graph)

        for node in nodes:
            pygame.draw.rect(screen, get_node_color(node.name), node.rect, 0)
            if node.check_messages():
                pygame.draw.rect(screen, (0, 255, 0), node.rect, 3)
            else:
                pygame.draw.rect(screen, (255, 0, 0), node.rect, 3)

        pygame.display.flip()
        screen.fill((255, 0, 255))

    pygame.quit()

def align_edge_between_nodes(idx, s_node, e_node, edge):
    ex, ey = e_node.rect.center
    sx, sy = s_node.rect.center
    dx = ex - sx
    dy = ey - sy
    dl = (dx ** 2 + dy ** 2) ** 0.5

    r = 25

    edge.rect_out.centerx = ex - dx/dl * r
    edge.rect_out.centery = ey - dy/dl * r

    edge.rect_in.centerx = sx + dx/dl * r
    edge.rect_in.centery = sy + dy/dl * r

    edx = edge.rect_out.centerx - edge.rect_in.centerx
    edy = edge.rect_out.centery - edge.rect_in.centery
    edl = (edx**2 + edy**2)**0.5
    edx /= edl
    edy /= edl

    dir = 1 if (edx * dx/dl + edy * dy/dl) > 0 else -1
    # dir = 1 if idx % 2 == 0 else -1

    dist = 20

    edge.rect_out.centerx += dir * edy * dist
    edge.rect_out.centery -= dir * edx * dist

    edge.rect_in.centerx += dir * edx * dist
    edge.rect_in.centery -= dir * edx * dist


def move_edges_points(graph, node):
    edges = graph.get_edges_from_node(node)
    for idx, edge in enumerate(edges):
        s_node, e_node = graph.get_nodes_of_edge(edge)
        align_edge_between_nodes(idx, s_node, e_node, edge)

    edges = graph.get_edges_to_node(node)
    for idx, edge in enumerate(edges):
        s_node, e_node = graph.get_nodes_of_edge(edge)
        align_edge_between_nodes(idx, s_node, e_node, edge)



if __name__ == '__main__':
    main()
