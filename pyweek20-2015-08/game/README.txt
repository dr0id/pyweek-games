﻿Data Blasters
=============

A game written for pyweek 20 (www.pyweek.org)

The theme was 'data data data'

Team: DRummb0ID
Members: DR0ID, Gummbum
https://www.pyweek.org/e/DRummb0ID/

CREDITS:
Coding: DR0ID, Gummbum
Artists: DR0ID, Gummbum
Musicians: https://soundcloud.com/douseewhateyesee-nicholson
SFX: see LICENSES
Story: Gummbum
Special Effects: DR0ID
Technical Advisor: DR0ID
Casting and Wardrobe: --
Public Relations: --


DEPENDENCIES:
=============

python (tested with: 2.7 and 3.4)
pygame (tested with: 1.9.1 and 1.9.2)
numpy (optional; tested with: 1.9.2; if you don't have numpy you won't see the nice gradient color font)

You might need to install these before running the game:

  Python:     http://www.python.org/
  PyGame:     http://www.pygame.org/


GAMEPLAY:
=========


RUNNING THE GAME:

On Windows or Mac OS X, locate the "run_game.pyw" file and double-click it.

Otherwise open a terminal / console and "cd" to the game directory and run:

  python run_game.py



HOW TO PLAY THE GAME:
=====================

Aim with the mouse. You will need a mouse!

'Left mouse button' or 'k' -- shoot left tower
'Right mouse button' or 'l' -- shoot right tower
'Ctrl' or 'space' -- secondary fire mode of towers
'Esc' -- pause/unpause game
'Enter' -- advance to next screen

additional Keys:

F2 - toggle enable/disable alarm sound
F3 - toggle enable/disable scan sound
F4 - toggle enable/disable power scan sound (scan at cursor)
F5 - toggle enable/disable suck in sound


cheat Keys:

F9 - toggle debug render
F10 - exit level during gameplay
F11 - restart level
F12 - next level

KEYMAP: All keys can be changed in gamelib/settings.py, look for KEY_*!



LICENSES:
=========

siren.ogg
    modified version of
    http://soundbible.com/1377-Woop-Woop.html
    Uploaded: 02.22.10 | License: Attribution 3.0 | Recorded by Mike Koenig

suckin.ogg
    modified version of
    http://soundbible.com/801-Suck-Up.html
    Uploaded: 08.03.09 | License: Attribution 3.0 | Recorded by Mike Koenig
