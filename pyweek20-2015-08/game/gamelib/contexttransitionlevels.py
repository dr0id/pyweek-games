# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek20-2015-08
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import pygame

import ptext
from pyknicpygame.pyknic.context import Context
from pyknicpygame.pyknic.mathematics import Point2
from pyknicpygame.pyknic.timing import Scheduler
from pyknicpygame.pyknic.tweening import Tweener, OUT_SINE, IN_SINE
from pyknicpygame.spritesystem import DefaultRenderer, Camera, Sprite
import settings

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 


class ContextTransitionLevels(Context):
    def __init__(self, level):
        self.level = level
        self.score = level.score
        self.scheduler = Scheduler()
        self.tweener = Tweener()
        self.spritesystem = DefaultRenderer()
        screen_rect = pygame.display.get_surface().get_rect()
        self.cam = Camera(screen_rect)

        self.font_args = settings.LOLIGHT_FONT_COLOR

        if self.level.end_messages:
            # font_args = dict(color=pygame.Color('tomato'), scolor=pygame.Color('gold'), shadow=(1, 1), owidth=1)
            y = 200
            x = 0
            duration = settings.LEVEL_END_MESSAGE_DURATION
            self.show_messages(self.font_args, self.level.end_messages, x, y, duration=duration)
            self.scheduler.schedule(self.show_scores, duration)

        self.done = False
        self.use_wait_events = False

    def show_scores(self):
        self.spritesystem.clear()
        texts = [self.score.message]
        texts.append("")
        texts.append("Packages destroyed:")
        texts.append("")
        for p, count in self.score.kills.items():
            texts.append("{0}    {1} / {2}".format(p.__name__, count, self.level.get_totals([p])))
        texts.append("")
        texts.append("Packages reached core:")
        for p, count in self.score.core_package_count.items():
            texts.append("{0}    {1} / {2}".format(p.__name__, count, self.level.get_totals([p])))
        texts.append("")
        texts.append("")
        texts.append("")
        # texts.append("Press [{0}] to continue...".format(settings.key_to_text[settings.KEY_MOVE_NEXT][1].upper()))
        key_text = '{0}'.format([settings.key_to_text[k][1].upper() for k in settings.KEY_MOVE_NEXT]).replace("'", '')
        texts.append("Press {0} to continue...".format(key_text.upper()))
        # font_args = dict(color=pygame.Color('tomato'), scolor=pygame.Color('gold'), shadow=(1, 1), owidth=1)
        distance = settings.SCORE_LINE_DISTANCE
        y = -300
        direction = -1
        for text in texts:
            if len(text) > 0:
                image = ptext.getsurf(text, 'BIOST', 42, width=settings.SCREEN_WIDTH * 2 / 3, **self.font_args)
                sprite = Sprite(image, Point2(0, y), z_layer=settings.LAYER_OVERLAY_TEXT)
                # sprite = TextSprite(text, Point2(0, y), font=font, color=(0, 0, 0), backcolor=(200, 200, 255))
                self.tweener.create_tween(sprite.position, "x", 900 * direction, -1 * 900 * direction,
                                          settings.SCORE_SWEEP_IN_DURATION,
                                          tween_function=OUT_SINE, cb_end=lambda *args: self.flip_wait())
                self.spritesystem.add_sprite(sprite)
            y += distance
            direction *= -1

    def show_messages(self, font_args, messages, x, y, duration=5.0, end_cb=None):
        if end_cb is None:
            end_cb = lambda tweener, obj, attr_name, *args: self.tweener.remove_tween(obj, attr_name)
        for msg in messages:
            image = ptext.getsurf(msg, 'BIOST', 80, width=settings.SCREEN_WIDTH * 2 / 3, **font_args)
            sprite = Sprite(image, Point2(x, y), z_layer=settings.LAYER_OVERLAY_TEXT)
            y += settings.LEVEL_MESSAGES_DISTANCE
            self.tweener.create_tween(sprite, "alpha", 255.0, -255.0, duration, IN_SINE, cb_end=end_cb)
            self.spritesystem.add_sprite(sprite)

    def enter(self):
        pygame.event.clear()

    def think(self, delta_time):
        """
        Any logic that needs to be updated for the effect.

        :Parameters:
            delta_time : float
                delta time passed since last frame

        :returns: True if it is done (finished, time is up or whatever),
                    otherwise False.
        """
        self.scheduler.update(delta_time)
        self.tweener.update(delta_time)
        for event in [pygame.event.wait()] if self.use_wait_events else pygame.event.get():
            if event.type == pygame.KEYUP:
                if event.key in settings.KEY_MOVE_NEXT:
                    self.move_to_next_context()
            # if event.type == pygame.MOUSEBUTTONUP:
            #     if event.button < 4:  # exclude wheel
            #         self.move_to_next_context()
            elif event.type == pygame.QUIT:
                self.pop(Context._stack_length(), False)

    def draw(self, screen, do_flip):
        """
        Draws the effect on the screen surface.

        :Parameters:
            screen : pygame.Surface
                the screen surface, the finished drawing
            old_context : Context
                the old context, the one that where already here
            new_context : Context
                the new context, may eventually replace the old_context

        """
        self.top(1).draw(screen, False)
        self.spritesystem.draw(screen, self.cam, fill_color=(128, 128,  128, 255),  do_flip=True, fill_flags=pygame.BLEND_RGBA_MULT)

    def move_to_next_context(self, *args):
        self.pop()
        self.pop()

    def flip_wait(self):
        self.use_wait_events = True
