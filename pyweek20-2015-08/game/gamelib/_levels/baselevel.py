from entities.entitytypes import PackageTypeClean, PackageTypeCleanUnsure, PackageTypeBaddyDetected, \
    PackageTypeBaddyUnsure, PackageTypeBaddyUndetected
import settings


class Score(object):
    def __init__(self):
        self.has_won = False
        self.message = ""
        self.kills = {}  # { package_type : count}
        self.core_package_count = {}

    def add_core_package(self, package):
        count = self.core_package_count.get(package.package_type.__class__, 0)
        self.core_package_count[package.package_type.__class__] = count + 1

    def add_kill_package(self, package):
        count = self.kills.get(package.package_type.__class__, 0)
        self.kills[package.package_type.__class__] = count + 1


class BaseLevel(object):
    def __init__(self, *args):
        self.args = args
        self.score = Score()

        self.start_messages = [self.__class__.__name__, "Connecting..."]
        self.end_messages = ["Shutting down!"]

        self.img_res_map = {
            settings.RES_IMG_BULLET1: "data/bullet.png",
            settings.RES_IMG_CURSOR: "data/cursor.png",
            settings.RES_IMG_PORT: "data/port.png",
            settings.RES_IMG_WALL: "data/wall.png",
            settings.RES_IMG_INSIDE_AREA: "data/inside.png",
            settings.RES_IMG_TURRET1: ("data/turret.png", 0.8),
            settings.RES_IMG_TURRET1_BASE: ("data/turretbase.png", 0.8),
            settings.RES_IMG_BULLET2: "data/bullet2.png",
            settings.RES_IMG_CORE1: "data/chip.png",
            settings.RES_IMG_CORE2: "data/chip2.png",
            settings.RES_IMG_PACKAGE: ("data/mail.png", 0.4),
            settings.RES_IMG_LASER: "data/laser.png",
            settings.RES_IMG_PACKAGE_FACE_BADDY : ("data/Angry-Face.png", 0.4),
            settings.RES_IMG_PACKAGE_FACE_FRIENDLY : ("data/Happy-Face.png", 0.4),
            settings.RES_IMG_CHECKMARK: ("data/Checkmark.png", 0.3),
            settings.RES_IMG_CROSS: ("data/x.png", 0.3),
            settings.RES_IMG_UNSURE: ("data/warning-triangle.png", 0.3),
        }

        self.sfx_res_map = {
            settings.RES_SFX_FIRE1: ('data/sfx/laser.ogg', 0.04),
            settings.RES_SFX_FIRE2: ('data/sfx/laser_01375.ogg', 0.50),
            # settings.RES_SFX_SCANNER_WARN: ('data/sfx/low_health.ogg', 1.0),
            settings.RES_SFX_SCANNER_WARN: ('data/sfx/siren.ogg', 0.05),
            settings.RES_SFX_SCAN: ('data/sfx/scan.ogg', 0.03),
            settings.RES_SFX_POWERSCAN: ('data/sfx/power_scan.ogg', 0.1),
            settings.RES_SFX_SUCK_IN_PACKAGE: ('data/sfx/suckin.ogg', 0.3),
        }

        self.music_res_map = {
            # {RES_ID : filename}
            settings.RES_MUS_1: ('Rise of an Empire', 'data/music/Rise_of_an_Empire_by_DoUseewhatEYEc.mp3'),
            settings.RES_MUS_2: ('The Shape Im In', 'data/music/The_Shape_Im_In_by_DoUseewhatEYEc.mp3'),
            settings.RES_MUS_3: ('DeJaVU Here We Go Again', 'data/music/DeJaVU_Here_We_Go_Again_by_DoUseewhatEYEc.mp3'),
            settings.RES_MUS_4: ('The Wrong Way', 'data/music/The_Wrong_Way_by_DoUseewhatEYEc.mp3'),
            settings.RES_MUS_5: ('One Way Out', 'data/music/One_Way_Out_by_DoUseewhatEYEc.mp3'),
        }

        self.font_res_map = {
            # {RES_ID : (filename, size)}
        }

        self.animation_res_map = {
            # {RES_ID : filename}
            settings.RES_ANIM_ALARM_SOUND: ("data/RSS.png.sdef", 0.6)
        }

        self.goal_kill_baddies_min = 1  # n of m where m >= n
        self.goal_kill_friendly_max = 1  # n of m where m >= n

        self.wave_areas = []
        self.waves = []
        self.turrets = []

    def check_waves(self, level_name):
        _name = " in '{0}'".format(level_name)
        total_baddies_count = 0
        total_friendly_count = 0
        for wave_idx, wave in enumerate(self.waves):
            name = _name + "wave idx " + str(wave_idx)
            used_areas = [gen[0] for gen in wave["generators"]]
            distinct_areas = set(used_areas)
            assert len(used_areas) == len(distinct_areas), "an area has been used twice!" + name

            for gen in wave["generators"]:
                friendly_count = gen[3].get(PackageTypeClean, 0) + gen[3].get(PackageTypeCleanUnsure, 0)
                total_friendly_count += friendly_count
                total_baddies_count += sum(gen[3].values()) - friendly_count
                assert gen[0] < len(self.wave_areas), "wave area index out of bound" + name
        assert total_baddies_count >= self.goal_kill_baddies_min, "not enough baddies to fulfill goal min kill" + _name
        assert total_friendly_count >= self.goal_kill_friendly_max, "not enough friendly to fail goal max friendly kill" + _name
        assert total_friendly_count == self.get_totals([PackageTypeClean, PackageTypeCleanUnsure])
        assert total_baddies_count == self.get_totals([PackageTypeBaddyDetected, PackageTypeBaddyUnsure, PackageTypeBaddyUndetected])


        for area in self.wave_areas:
            x, y, w, h = area
            assert w > 0, "wave area width should be > 0" + _name
            assert h > 0, "wave area height should be > 0" + _name

        assert 2 >= len(self.turrets) >= 1, "not enough or too many turrets: " + str(len(self.turrets)) + " in " + _name

    def get_totals(self, types):
        total = 0
        for wave in self.waves:
            for gen in wave["generators"]:
                for t in types:
                    total += gen[3].get(t, 0)
        return total