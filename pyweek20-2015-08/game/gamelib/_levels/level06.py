from _levels.baselevel import BaseLevel
from entities.entitytypes import ScanType, TurretType, PackageTypeClean, PackageTypeBaddyDetected, \
    PackageTypeCleanUnsure, PackageTypeBaddyUnsure, PackageTypeBaddyUndetected
from pyknicpygame.pyknic.mathematics import Point2
import settings

__author__ = 'DRummb0ID'


class Level06(BaseLevel):
    def __init__(self):
        BaseLevel.__init__(self)

        self.start_messages = ["Level 6", "Screen Crooked"]
        self.end_messages = ["Fixed - Don't hit the monitor..."]

        # tuning
        self.package_interval = 4.0  # seconds

        # area that is considered to be inside
        self.wall_width = 10
        # self.inside_area = (30, 140, 994 - 30, 628 - 30)
        w, h = 800, 500
        x = (settings.SCREEN_WIDTH - w) / 2
        y = (settings.SCREEN_HEIGHT - h) / 2
        self.inside_area = (x, y, w, h)
        self.inside_area_image = settings.RES_IMG_INSIDE_AREA  # "data/inside.png"
        self.color_outside = (255, 255, 255)
        # port = (wall_idx[0-3], dist in %, w, h, scanner_type, integrity in %)
        self.ports = [
            (1, 0.75, 100, 100, ScanType(3.0), 1.0),
            (3, 0.25, 100, 200, ScanType(3.0), 1.0),
        ]
        # turrets, (position, config) where position relative to inside_are and config all tune values
        self.turrets = [
            (self.inside_area[2] / 2 - 300, self.inside_area[3] / 2, TurretType()),
            (self.inside_area[2] / 2 + 300, self.inside_area[3] / 2, TurretType()),
        ]

        self.cores = [
            (Point2(self.inside_area[2] // 2, self.inside_area[3] // 2), 1.0)
        ]

        self.goal_kill_baddies_min = 6  # n of m where m >= n
        self.goal_kill_friendly_max = 3  # n of m where m >= n

        self.wave_areas = [(-50, 100, 50, 900), (1024 + 50, 100, 50, 900)]
        self.waves = [
            {
                "generators": [
                    (0, 4.0, 5.0,  # wave_areas index, min delay, max delay between packages
                     {
                         PackageTypeClean: 3,
                         PackageTypeBaddyDetected: 2,
                         PackageTypeCleanUnsure: 0,
                         PackageTypeBaddyUnsure: 2,
                         PackageTypeBaddyUndetected: 0,
                     }),
                    (1, 4.0, 5.0,  # wave_areas index, min delay, max delay between packages
                     {
                         PackageTypeClean: 3,
                         PackageTypeBaddyDetected: 0,
                         PackageTypeCleanUnsure: 2,
                         PackageTypeBaddyUnsure: 0,
                         PackageTypeBaddyUndetected: 2,
                     }),
                ],
                "delay": 1.0,
            },
        ]
