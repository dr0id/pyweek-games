from _levels.baselevel import BaseLevel
from entities.entitytypes import ScanType, TurretType, PackageTypeClean, PackageTypeBaddyDetected, \
    PackageTypeCleanUnsure, PackageTypeBaddyUnsure, PackageTypeBaddyUndetected
from pyknicpygame.pyknic.mathematics import Point2
import settings

__author__ = 'DRummb0ID'


class Level09(BaseLevel):
    def __init__(self):
        BaseLevel.__init__(self)

        self.start_messages = ["Free For All", "SPARKS + SMOKE!!!"]
        self.end_messages = ["Joe douses you in Mountain Dew..."]

        # tuning
        self.package_interval = 4.0  # seconds

        # area that is considered to be inside
        self.wall_width = 10
        # self.inside_area = (30, 140, 994 - 30, 628 - 30)
        w, h = 800, 600
        x = (settings.SCREEN_WIDTH - w) / 2
        y = (settings.SCREEN_HEIGHT - h) / 2
        self.inside_area = (x, y, w, h)
        self.inside_area_image = settings.RES_IMG_INSIDE_AREA  # "data/inside.png"
        self.color_outside = (255, 255, 255)
        # port = (wall_idx[0-3], dist in %, w, h, scanner_type, integrity in %)
        self.ports = [
            (0, 1.0 / 6.0, 100, 100, ScanType(3.0), 1.0),
            (0, 2.0 / 6.0, 100, 100, ScanType(3.0), 1.0),
            (0, 3.0 / 6.0, 100, 100, ScanType(3.0), 1.0),
            (0, 4.0 / 6.0, 100, 100, ScanType(3.0), 1.0),
            (0, 5.0 / 6.0, 100, 100, ScanType(3.0), 1.0),
        ]
        # turrets, (position, config) where position relative to inside_are and config all tune values
        self.turrets = [
            (self.inside_area[2] // 2 - 40, self.inside_area[3] * 13 // 20, TurretType()),
            (self.inside_area[2] // 2 + 40, self.inside_area[3] * 13 // 20, TurretType()),
        ]

        self.cores = [
            (Point2(self.inside_area[2] * 1 // 4, self.inside_area[3] * 7 // 8), 0.75),
            (Point2(self.inside_area[2] * 3 // 4, self.inside_area[3] * 7 // 8), 0.75),
        ]

        self.goal_kill_baddies_min = 30  # n of m where m >= n
        self.goal_kill_friendly_max = 50  # n of m where m >= n

        w = 1024 // 6
        self.wave_areas = [
            (w * 1, -100, w, 100),
            (w * 2, -100, w, 100),
            (w * 3, -100, w, 100),
            (w * 4, -100, w, 100),
            (w * 5, -100, w, 100),
        ]
        self.waves = [
            {
                "generators": [
                    (0, 4.0, 5.0,  # wave_areas index, min delay, max delay between packages
                     {
                         PackageTypeClean: 10,
                         PackageTypeBaddyDetected: 5,
                         PackageTypeCleanUnsure: 5,
                         PackageTypeBaddyUnsure: 5,
                         PackageTypeBaddyUndetected: 5,
                     }),
                ],
                "delay": 1.0,
            },
            {
                "generators": [
                    (1, 4.0, 5.0,  # wave_areas index, min delay, max delay between packages
                     {
                         PackageTypeClean: 10,
                         PackageTypeBaddyDetected: 5,
                         PackageTypeCleanUnsure: 5,
                         PackageTypeBaddyUnsure: 5,
                         PackageTypeBaddyUndetected: 5,
                     }),
                ],
                "delay": 1.0,
            },
            {
                "generators": [
                    (2, 4.0, 5.0,  # wave_areas index, min delay, max delay between packages
                     {
                         PackageTypeClean: 10,
                         PackageTypeBaddyDetected: 5,
                         PackageTypeCleanUnsure: 5,
                         PackageTypeBaddyUnsure: 5,
                         PackageTypeBaddyUndetected: 5,
                     }),
                ],
                "delay": 1.0,
            },
            {
                "generators": [
                    (3, 4.0, 5.0,  # wave_areas index, min delay, max delay between packages
                     {
                         PackageTypeClean: 10,
                         PackageTypeBaddyDetected: 5,
                         PackageTypeCleanUnsure: 5,
                         PackageTypeBaddyUnsure: 5,
                         PackageTypeBaddyUndetected: 5,
                     }),
                ],
                "delay": 1.0,
            },
            {
                "generators": [
                    (4, 4.0, 5.0,  # wave_areas index, min delay, max delay between packages
                     {
                         PackageTypeClean: 10,
                         PackageTypeBaddyDetected: 5,
                         PackageTypeCleanUnsure: 5,
                         PackageTypeBaddyUnsure: 5,
                         PackageTypeBaddyUndetected: 5,
                     }),
                ],
                "delay": 1.0,
            },
        ]
