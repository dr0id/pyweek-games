from _levels.baselevel import BaseLevel
from entities.entitytypes import ScanType, TurretType, PackageTypeClean, PackageTypeBaddyDetected, \
    PackageTypeCleanUnsure, PackageTypeBaddyUnsure, PackageTypeBaddyUndetected
from pyknicpygame.pyknic.mathematics import Point2
import settings

__author__ = 'DRummb0ID'


class Level01(BaseLevel):
    def __init__(self):
        BaseLevel.__init__(self)

        self.start_messages = ["Level 1", "Reconnect"]
        self.end_messages = ["Grandblaster replacement arrived..."]

        # tuning
        self.package_interval = 3.0  # seconds

        # area that is considered to be inside
        self.wall_width = 10
        w, h = 850, 450
        x = (settings.SCREEN_WIDTH - w) / 2
        y = (settings.SCREEN_HEIGHT - h) / 2
        self.inside_area = (x, y, w, h)
        self.inside_area_image = settings.RES_IMG_INSIDE_AREA  # "data/inside.png"
        self.color_outside = (255, 255, 255)
        # port = (wall_idx[0-3], dist in %, w, h, scanner_type, integrity in %)
        self.ports = [
            (0, 0.33, 100, 200, ScanType(), 1.0),
        ]
        # turrets, (position, config) where position relative to inside_are and config all tune values
        self.turrets = [
            (self.inside_area[2] / 2 - 300, self.inside_area[3] / 2, TurretType()),
            # (self.inside_area[2] / 2 + 300, self.inside_area[3] / 2, TurretType()),
        ]

        self.cores = [
            (Point2(self.inside_area[2] // 2, self.inside_area[3] // 2), 1.0)
        ]

        self.goal_kill_baddies_min = 9  # n of m where m >= n
        self.goal_kill_friendly_max = 6  # n of m where m >= n

        self.wave_areas = [(0, -100, 1024, 100)]
        self.waves = [
            {
                "generators": [
                    (0, 0.2, 5.0,  # wave_areas index, min delay, max delay between packages
                     {
                         PackageTypeClean: 6,
                         PackageTypeBaddyDetected: 3,
                         PackageTypeCleanUnsure: 3,
                         PackageTypeBaddyUnsure: 3,
                         PackageTypeBaddyUndetected: 4,
                     }),
                ],
                "delay": 1.0,
            },
        ]
