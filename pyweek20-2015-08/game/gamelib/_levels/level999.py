from _levels.baselevel import BaseLevel
from entities.entitytypes import ScanType, TurretType, PackageTypeClean, PackageTypeBaddyDetected, \
    PackageTypeCleanUnsure, PackageTypeBaddyUnsure, PackageTypeBaddyUndetected
from pyknicpygame.pyknic.mathematics import Point2
import settings

__author__ = 'bw'


class Level999(BaseLevel):
    def __init__(self):
        BaseLevel.__init__(self)

        self.start_messages = ["Level 0", "Connected"]
        self.end_messages = ["Disconnected..."]

        # tuning
        self.package_interval = 0.1  # seconds

        # area that is considered to be inside
        self.wall_width = 10
        self.inside_area = (0, 400, 1024, 380)
        self.inside_area = (30, 140, 900, 380)
        self.inside_area_image = settings.RES_IMG_INSIDE_AREA  # "data/inside.png"
        self.color_outside = (255, 255, 255)
        # port = (wall_idx[0-3], dist in %, w, h, scanner_type, integrity in %)
        self.ports = [
            (0, 0.33, 50, 200, ScanType(), 1.0),
            (1, 0.75, 100, 100, ScanType(0.1), 1.0),
            (2, 0.75, 100, 200, ScanType(1.0), 1.0),
            (3, 0.25, 100, 200, ScanType(0.4), 1.0),
        ]
        # turrets, (position, config) where position relative to inside_are and config all tune values
        self.turrets = [
            (self.inside_area[2] / 2 - 300, self.inside_area[3] / 2, TurretType()),
            (self.inside_area[2] / 2 + 300, self.inside_area[3] / 2, TurretType()),
        ]

        self.cores = [
            (Point2(self.inside_area[2] // 3 * 2, self.inside_area[3] // 2), 0.5),
            (Point2(self.inside_area[2] // 3, self.inside_area[3] // 2), 0.5),
        ]

        self.goal_kill_baddies_min = 20  # n of m where m >= n
        self.goal_kill_friendly_max = 30  # n of m where m >= n

        self.wave_areas = [(0, -100, 1024, 100), (-50, 100, 50, 900), (0, 1000, 1024, 100), (1024 + 50, 100, 50, 900)]
        self.waves = [
            {
                "generators": [
                    (0, 0.1, 0.5,  # wave_areas index, min delay, max delay between packages
                     {
                         PackageTypeClean: 3,
                         PackageTypeBaddyDetected: 30,
                         PackageTypeCleanUnsure: 0,
                         PackageTypeBaddyUnsure: 0,
                         PackageTypeBaddyUndetected: 0,
                     }),
                    (1, 0.2, 5.0,  # wave_areas index, min delay, max delay between packages
                     {
                         PackageTypeClean: 30,
                     }),
                ],
                "delay": 1.0,
            },
            {
                "generators": [
                    (2, 0.2, 5.0,  # wave_areas index, min delay, max delay between packages
                     {
                         PackageTypeClean: 3,
                         PackageTypeBaddyDetected: 3,
                         PackageTypeCleanUnsure: 3,
                         PackageTypeBaddyUnsure: 3,
                         PackageTypeBaddyUndetected: 3,
                     }),
                    (3, 0.1, 1.0,  # min, max
                     {
                         PackageTypeClean: 3,
                     }),
                ],
                "delay": 20.0,  # initial wave delay
            }
        ]
