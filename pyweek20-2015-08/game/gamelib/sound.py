# -*- coding: utf-8 -*-

import pygame

import settings


class Song(object):

    master_volume = 1.0

    def __init__(self, name, file_name, volume=1.0):
        self.name = name
        self.file_name = file_name
        self.volume = volume

    def play(self):
        pygame.mixer.music.load(self.file_name)
        self.set_volume(self.master_volume)
        pygame.mixer.music.play()

    def queue(self):
        pygame.mixer.music.queue(self.file_name)

    @staticmethod
    def fadeout(time=1000):
        pygame.mixer.music.fadeout(time)

    @staticmethod
    def stop():
        pygame.mixer.music.stop()

    @staticmethod
    def pause():
        pygame.mixer.music.pause()

    @staticmethod
    def unpause():
        pygame.mixer.music.unpause()

    @staticmethod
    def stop():
        pygame.mixer.music.stop()

    @staticmethod
    def get_volume():
        return pygame.mixer.music.get_volume()

    @staticmethod
    def set_volume(value):
        Song.master_volume = value
        pygame.mixer.music.set_volume(value)

    @staticmethod
    def get_busy():
        return pygame.mixer.music.get_busy()


songs = []


def roll_songs(intro=False):
    if songs:
        song = songs[0]
        if not song.get_busy():
            if not intro:
                songs.pop(0)
                songs.append(song)
                song = songs[0]
            song.play()


def remove_intro():
    global songs
    songs = [s for s in songs if s.name != 'Rise of an Empire']
