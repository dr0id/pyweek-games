# -*- coding: utf-8 -*-

"""
TODO: docstring

equations based on:

http://wiki.python-ogre.org/index.php?title=CodeSnippits_pyTweener
http://hosted.zeh.com.br/tweener/docs/en-us/misc/transitions.html
http://code.google.com/p/tweener/source/browse/trunk/as3/caurina/transitions/Equations.as

tweens based on:

http://www.robertpenner.com/easing/penner_chapter7_tweening.pdf

"""

import math
from math import cos
from math import sin
from math import pow
from math import asin
from math import sqrt
from math import isnan
from math import pi as PI
TWO_PI = PI * 2
HALF_PI = PI / 2.0


def LINEAR(t, b, c, d, p):
    return c * t / d + b

    
def IN_QUAD(t, b, c, d, p):
    t /= d
    return c * (t) * t + b


def OUT_QUAD(t, b, c, d, p):
    t /= d
    return -c * (t) * (t - 2) + b


def IN_OUT_QUAD(t, b, c, d, p):
    t /= d / 2.0
    if ((t) < 1):
        return c / 2.0 * t * t + b
    t -= 1
    return -c / 2.0 * ((t) * (t - 2) - 1) + b


def OUT_IN_QUAD(t, b, c, d, p):
    if (t < d / 2):
        return OUT_QUAD(t * 2, b, c / 2, d, p)
    return IN_QUAD((t * 2) - d, b + c / 2, c / 2, d, p)


def IN_CUBIC(t, b, c, d, p):
    t /= d
    return c * (t) * t * t + b


def OUT_CUBIC(t, b, c, d, p):
    t = t / d - 1
    return c * ((t) * t * t + 1) + b


def IN_OUT_CUBIC(t, b, c, d, p):
    t /= d / 2
    if ((t) < 1):
        return c / 2 * t * t * t + b
    t -= 2
    return c / 2 * ((t) * t * t + 2) + b


def OUT_IN_CUBIC(t, b, c, d, p):
    if t < d / 2:
        return OUT_CUBIC(t * 2, b, c / 2, d, p)
    return IN_CUBIC((t * 2) - d, b + c / 2, c / 2, d, p)


def IN_QUART(t, b, c, d, p):
    t /= d
    return c * (t) * t * t * t + b


def OUT_QUART(t, b, c, d, p):
    t = t / d - 1
    return -c * ((t) * t * t * t - 1) + b


def IN_OUT_QUART(t, b, c, d, p):
    t /= d / 2
    if (t < 1):
        return c / 2 * t * t * t * t + b
    t -= 2
    return -c / 2 * ((t) * t * t * t - 2) + b


def OUT_IN_QUART(t, b, c, d, p):
    if t < d / 2:
        return OUT_QUART(t * 2, b, c / 2, d, p)
    return IN_QUART((t * 2) - d, b + c / 2, c / 2, d, p)


def IN_QUINT(t, b, c, d, p):
    t = t / d
    return c * t * t * t * t * t + b


def OUT_QUINT(t, b, c, d, p):
    t = t / d - 1
    return c * (t * t * t * t * t + 1) + b


def IN_OUT_QUINT(t, b, c, d, p):
    t = t / (d / 2)
    if t < 1:
        return c / 2 * t * t * t * t * t + b
    t = t - 2
    return c / 2 * (t * t * t * t * t + 2) + b;


def OUT_IN_QUINT(t, b, c, d, p):
    c = c / 2
    if t < d / 2:
        return OUT_QUINT (t * 2, b, c, d, p)
    return IN_QUINT(t * 2 - d, b + c, c, d, p)


def IN_SINE(t, b, c, d, p):
    return -c * cos(t / d * HALF_PI) + c + b


def OUT_SINE(t, b, c, d, p):
    return c * sin(t / d * HALF_PI) + b


def IN_OUT_SINE(t, b, c, d, p):
    return -c / 2 * (cos(PI * t / d) - 1) + b


def OUT_IN_SINE(t, b, c, d, p):
    if t < d / 2:
        return OUT_SINE(t * 2, b, c / 2, d, p)
    return IN_SINE((t * 2) - d, b + c / 2, c / 2, d, p)


def IN_CIRC(t, b, c, d, p):
    t /= d + 0.0005
    return -c * (sqrt(1 - t * t) - 1) + b


def OUT_CIRC(t, b, c, d, p):
    t = t / d - 1 
    return c * sqrt(1 - t * t) + b


def IN_OUT_CIRC(t, b, c, d, p):
    t /= d / 2
    if t < 1:
        return -c / 2 * (sqrt(1 - t*t) - 1) + b
    t -= 2
    return c / 2 * (sqrt(1 - t * t) + 1) + b


def OUT_IN_CIRC(t, b, c, d, p):
    if t < d/2:
        return OUT_CIRC(t * 2, b, c / 2, d, p)
    return IN_CIRC(t * 2 - d, b + c / 2, c / 2, d, p)


def IN_EXPO(t, b, c, d, p):
    return b if t == 0 else c * (2 ** (10 * (t / d - 1))) + b - c * 0.001


def OUT_EXPO(t, b, c, d, p):
    return b + c if (t == d) else c * (-2 ** (-10 * t / d) + 1) + b


def IN_OUT_EXPO(t, b, c, d, p):
    if t==0:
        return b
    if t==d:
        return b + c
    t /= d / 2
    if t < 1:
        return c / 2 * pow(2, 10 * (t - 1)) + b - c * 0.0005
    return c / 2 * 1.0005 * (-pow(2, -10 * (t - 1)) + 2) + b


def OUT_IN_EXPO(t, b, c, d, p):
    if t < d / 2:
        return OUT_EXPO(t * 2, b, c / 2, d, p)
    return IN_EXPO((t * 2) - d, b + c / 2, c / 2, d, p)


def IN_ELASTIC(t, b, c, d, params):
    if t==0:
        return b
    t /= d
    if t==1:
        return b + c
    p = d * 0.3 if params is None or isnan(params.period) else params.period
    a = 0 if params is None or isnan(params.amplitude) else params.amplitude
    if a == 0 or a < abs(c):
        a = c
        s = p / 4
    else:
        s = p / TWO_PI * asin (c / a)
    t -= 1
    return -(a * pow(2, 10 * t) * sin( (t * d - s) * TWO_PI / p )) + b


def OUT_ELASTIC(t, b, c, d, params):
    if (t == 0):
        return b
    t /= d
    if t == 1:
        return b + c
    p = d * 0.3 if params is None or isnan(params.period) else params.period
    a = 1.0 if params is None or isnan(params.amplitude) else params.amplitude
    if a == 0 or a < abs(c):
        a = c
        s = p / 4
    else:
        s = p / TWO_PI * asin(c / a)

    return (a * pow(2, -10 * t) * sin((t * d - s) * TWO_PI / p) + c + b)


def IN_OUT_ELASTIC(t, b, c, d, params):
    if t==0:
        return b
    t /= d / 2
    if t==2:
        return b + c
    p = d * 0.3 * 1.5 if params is None or isnan(params.period) else params.period
    a = 0 if params is None or isnan(params.amplitude) else params.amplitude
    if a == 0 or  a < abs(c):
        a = c
        s = p / 4
    else:
        s = p / TWO_PI * asin (c / a)
    if t < 1:
        t -= 1
        return -0.5 * (a * pow(2, 10 * t) * sin( (t * d - s) * TWO_PI / p )) + b
    t -= 1
    return a * pow(2, -10 * t) * sin( (t * d - s) * TWO_PI / p ) * 0.5 + c + b


def OUT_IN_ELASTIC(t, b, c, d, p):
    if t < d / 2:
        return OUT_ELASTIC(t * 2, b, c / 2, d, p)
    return IN_ELASTIC((t * 2) - d, b + c / 2, c / 2, d, p)


def IN_BACK(t, b, c, d, p):
    s = 1.70158 if p is None or isnan(p.overshoot) else p.overshoot
    t /= d
    return c * t * t * ((s + 1) * t - s) + b


def OUT_BACK(t, b, c, d, p):
    s = 1.70158 if p is None or isnan(p.overshoot) else p.overshoot
    t = t / d - 1
    return c * (t * t * ((s + 1) * t + s) + 1) + b


def IN_OUT_BACK(t, b, c, d, p):
    s = 1.70158 if p is None or isnan(p.overshoot) else p.overshoot
    t /= d / 2
    if t < 1:
        s *= 1.525
        return c / 2 * (t * t * ((s + 1) * t - s)) + b
    s *= 1.525
    t -= 2
    return c / 2 * (t * t * ((s + 1) * t + s) + 2) + b


def OUT_IN_BACK(t, b, c, d, p):
    if t < d / 2:
        return OUT_BACK (t * 2, b, c / 2, d, p)
    return IN_BACK((t * 2) - d, b + c / 2, c / 2, d, p)


def IN_BOUNCE(t, b, c, d, p=None):
    return c - OUT_BOUNCE(d - t, 0, c, d) + b


def OUT_BOUNCE(t, b, c, d, p=None):
    t /= d
    if t < (1 / 2.75):
        return c * (7.5625 * t * t) + b
    elif t < (2 / 2.75):
        t -= (1.5 / 2.75)
        return c * (7.5625 * t * t + 0.75) + b
    elif t < (2.5 / 2.75):
        t-=(2.25 / 2.75)
        return c * (7.5625 * t * t + 0.9375) + b
    else:
        t -= (2.625 / 2.75)
        return c * (7.5625 * t * t + 0.984375) + b


def IN_OUT_BOUNCE(t, b, c, d, p):
    if (t < d / 2):
        return IN_BOUNCE(t * 2, 0, c, d) * 0.5 + b
    else:
        return OUT_BOUNCE(t * 2 - d, 0, c, d) * 0.5 + c * 0.5 + b


def OUT_IN_BOUNCE(t, b, c, d, p):
    if t < d / 2:
        return OUT_BOUNCE(t * 2, b, c / 2, d, p)
    return IN_BOUNCE((t * 2) - d, b + c / 2, c / 2, d, p)



ease_functions = [
                    LINEAR,
                    LINEAR,
                    LINEAR,
                    LINEAR,
                    LINEAR,
                    LINEAR,
                    LINEAR,
                    LINEAR,
                    
                    IN_SINE,
                    OUT_SINE,
                    IN_OUT_SINE,
                    OUT_IN_SINE,
                    
                    IN_QUAD,
                    OUT_QUAD,
                    IN_OUT_QUAD,
                    OUT_IN_QUAD,

                    IN_CUBIC,
                    OUT_CUBIC,
                    IN_OUT_CUBIC,
                    OUT_IN_CUBIC,

                    IN_QUART,
                    OUT_QUART,
                    IN_OUT_QUART,
                    OUT_IN_QUART,

                    IN_QUINT,
                    OUT_QUINT,
                    IN_OUT_QUINT,
                    OUT_IN_QUINT,
                    
                    IN_EXPO,
                    OUT_EXPO,
                    IN_OUT_EXPO,
                    OUT_IN_EXPO,
                    
                    IN_CIRC,
                    OUT_CIRC,
                    IN_OUT_CIRC,
                    OUT_IN_CIRC,
                    
                    IN_ELASTIC,
                    OUT_ELASTIC,
                    IN_OUT_ELASTIC,
                    OUT_IN_ELASTIC,
                    
                    IN_BACK,
                    OUT_BACK,
                    IN_OUT_BACK,
                    OUT_IN_BACK,

                    IN_BOUNCE,
                    OUT_BOUNCE,
                    IN_OUT_BOUNCE,
                    OUT_IN_BOUNCE,
                    
]

class Parameters(object):
    def __init__(self, period=float("nan"), amplitude=float("nan"), overshoot=float("nan")):
        self.period = float(period)
        self.amplitude = float(amplitude)
        self.overshoot = float(overshoot)


class _Tween(object):
    def __init__(self, ctrl, obj, attr_name, begin, change, duration, tween_function, params, delay, cb_end, *cb_args):
        self.ctrl = ctrl  # the controlling Tweener instance
        self.o = obj
        self.a = attr_name
        self.cb = cb_end  # TODO: use a signal here to support multiple listeners
        self.cb_args = cb_args
        
        # tween variables
        self.t = delay
        self.f = tween_function
        
        # tween parameters
        self.b = float(begin)
        self.c = float(change)
        self.d = float(duration)
        self.p = params


class Tweener(object):

    # TODO: implement pausing of tweens
    def __init__(self):
        self._active_tweens = []  # ? maybe a dict? {entity: [tween]}
        self._paused_tweens = []

    def update(self, delta_time):
        ended_tweens = []
        for tween in self._active_tweens:
            tween.t += delta_time
            if tween.t >= 0.0:
                if tween.t > tween.d:
                    ended_tweens.append(tween)
                    tween.t = tween.d
                value = tween.f(tween.t, tween.b, tween.c, tween.d, tween.p)
                setattr(tween.o, tween.a, value)
        for ended in ended_tweens:
            self._active_tweens.remove(ended)
            if ended.cb is not None:
                # TODO: this cb call seems strange with that many args
                ended.cb(self, ended.o, ended.a, ended.b, ended.c, ended.d, ended.f, ended.p, ended.cb, ended.cb_args)

    def create_tween(self, obj, attr_name, begin, change, duration, tween_function=LINEAR, params=None, cb_end=None,
                     cb_args=[], immediate=False, delay=0.0):
        # TODO: document the arguments, especially change
        # TODO: return the tween object? and add a  remove_tween(tween) method
        tween = _Tween(self, obj, attr_name, begin, change, duration, tween_function, params, -delay, cb_end, cb_args)
        self._active_tweens.append(tween)
        if immediate and tween.t >= 0.0:
            value = tween.f(tween.t, tween.b, tween.c, tween.d, tween.p)
            setattr(tween.o, tween.a, value)
        return tween

    def remove_tween(self, obj, attr_name):
        to_remove = []
        for tween in self._active_tweens:
            if tween.o == obj and tween.a == attr_name:
                to_remove.append(tween)
        for tween in to_remove:
            self._active_tweens.remove(tween)
            
    def clear(self):
        self._active_tweens[:] = []

    #TODO: this is hacked in
    def pause_tweens(self, objs):
        for obj in objs:
            to_remove = []
            for tween in self._active_tweens:
                if tween.o == obj:
                    to_remove.append(tween)
            for tween in to_remove:
                self._active_tweens.remove(tween)

            self._paused_tweens.extend(to_remove)

    #TODO: this is hacked in
    def resume_tweens(self, objs):
        for obj in objs:
            to_activate = []
            for tween in self._paused_tweens:
                if tween.o == obj:
                    to_activate.append(tween)
            for t in to_activate:
                self._paused_tweens.remove(t)
            self._active_tweens.extend(to_activate)


if __name__ == '__main__':

    def restart(tweener, *args):
        args[0].left = 10
        tweener.create_tween(*args)
        
    import pygame

    pygame.init()
    screen_w = 1024
    screen_h = 900

    pygame.display.set_caption("tweening, press any key to reset")
    screen = pygame.display.set_mode((screen_w, screen_h))

    tweener = Tweener()

    duration = 20
    time = 0
    rects = []
    tween_ended = False
    def stop_tween_time(tweener, *args):
        global tween_ended
        tween_ended = True
    
    def reset():
        global rects
        global tween_ended
        global time
        time = 0
        rects = []
        x = 0
        # d = 10
        w = h = 2
        tween_ended = False
        tweener.clear()
        for i in range(len(ease_functions)):
            # rect = pygame.Rect(x, i * d + d, w, h)
            rect = pygame.Rect(x, x, w, h)
            rects.append(rect)
            tweener.create_tween(rect, 'top', x, -50, duration, ease_functions[i], cb_end=stop_tween_time)
        screen.fill((0, 0, 0))
        pygame.display.flip()

    reset()
    
    if not pygame.font.get_init():
        pygame.font.init()
    font = pygame.font.Font(None, 15)
    
    clear_rect = pygame.Rect(850, 0, 300, screen_h)
    clock = pygame.time.Clock()
    running = True
    
    # main loop
    while running:
        clock.tick(60)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
                else:
                    reset()

        # draw
        # for idx, rect in enumerate(rects):
        for idx, tween in enumerate(tweener._active_tweens):
            rect = tween.o
            distance = 105
            # n_width = screen_w/distance
            n_width = 900 / distance
            offset_x = (idx % n_width) * distance + 10
            offset_y = 80 * (idx // n_width) + 80
            tt = 0
            if not tween_ended:
                tt = time * 5
            pygame.draw.rect(screen, (255, 255, 255), rect.move(offset_x + tt, offset_y), 0)
            aabb = pygame.Rect(0, 0, 100, 50)
            aabb.bottomleft = (offset_x, offset_y)
            pygame.draw.rect(screen, (155, 155, 255), aabb, 1)

            text = tween.f.__name__
            text_image = font.render(text, 1, (255, 255, 255, 255))
            screen.blit(text_image, (offset_x, offset_y + 10))
            
            
        other_list = list(tweener._active_tweens)
        # other_list.sort(key=lambda x: x.f.__name__)
        for idx, tween in enumerate(other_list):
            rect = tween.o
            text = tween.f.__name__
            text_image = font.render(text, 1, (255, 255, 255, 255))
            
            y_dist = 16
            y = (idx + 1) * y_dist
            screen.blit(text_image, (clear_rect.left, y))
            y += y_dist // 3
            x = clear_rect.left + 100
            pygame.draw.line(screen, (255, 255, 255), (x, y), (x+50, y), 1)
            # pygame.draw.rect(screen, (255, 0, 0), (x - rect.top, y, 4, 4), 0)
            pygame.draw.circle(screen, (255, 0, 0), (x - rect.top, y), 4, 0)
            
            
        # update
        delta = 0.1
        time += delta
        pygame.display.flip()
        tweener.update(delta)
        if len(tweener._active_tweens) > 0:
            screen.fill((0, 0, 0), clear_rect)

