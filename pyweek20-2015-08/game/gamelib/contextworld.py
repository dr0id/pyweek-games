# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek20-2015-08
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import random
import time
import logging

import pygame

from animation import Animator
from contextpause import ContextPause
from contexttransitionlevels import ContextTransitionLevels
from entities.entities import Wall, Turret, Cursor, Core
from entities.entitytypes import PackageTypeClean, PackageTypeCleanUnsure
from entities.port import Port
from entities.package import Package, StateMoveToCore
import ptext
import pyknicpygame
from pyknicpygame.pyknic.context import Context
from pyknicpygame.pyknic.mathematics import Point2, Vec2
from pyknicpygame.pyknic.timing import Scheduler
from pyknicpygame.pyknic.tweening import Tweener, IN_SINE
from pyknicpygame.spritesystem import Camera, Sprite
from resourceloader import ResourcesLoader
import settings

logger = logging.getLogger("pyknic.world")  # pyknic is needed otherwise its not logged into the pyknic.log file

random.seed(settings.RANDOM_SEED)

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 


class Generator(object):
    def __init__(self, world, wave_areas, initial_delay, gen_data):
        self.world = world
        area_idx, self.delay_min, self.delay_max, data = gen_data
        self.config = []

        for p_type, count in data.items():
            for i in range(count):
                self.config.append(p_type)

        random.shuffle(self.config)

        x, y, w, h = wave_areas[area_idx]
        self.area_w = (x, x + w)
        self.area_h = (y, y + h)
        self.finished = False

        self.world.scheduler.schedule(self.schedule_package, initial_delay)

    def schedule_package(self):
        if self.config:
            x = random.randint(*self.area_w)
            y = random.randint(*self.area_h)
            package_type = self.config.pop()

            package = Package(self.world, Point2(x, y), package_type())
            self.world.packages.append(package)

            delay = random.random() * (self.delay_max - self.delay_min) + self.delay_min
            return delay
        self.finished = True


class World(Context):
    def __init__(self, level):
        Context.__init__(self)

        self.collision_functions = {}  # {TYPE1|TYPE2: func}
        self.scheduler = Scheduler()
        self.tweener = Tweener()
        self.spritesystem = pyknicpygame.spritesystem.DefaultRenderer()
        self.animator = Animator()
        self.resources = ResourcesLoader()

        self.level = level

        screen_rect = pygame.display.get_surface().get_rect()
        self.cam = Camera(screen_rect)
        self.cam.position = Point2(screen_rect.w // 2, screen_rect.h // 2)

        self.inside_rect = pygame.Rect(level.inside_area)
        self.fire_walls = []
        self.ports = []
        self.turrets = []
        self.bullets = []
        self.cores = []
        self.packages = []
        self.cursor = None
        self._generators = []

    def create_inside_area_sprite(self, level):
        surface = self.resources.get_image(level.inside_area_image)
        surface = self.resources.scale_to_size(surface, self.inside_rect.size)

        # surface = pygame.Surface(self.inside_area.size)
        # surface.fill((200, 200, 200))

        inside_sprite = self.animator.create_animation(surface,
                                                       Point2(self.inside_rect.centerx, self.inside_rect.centery),
                                                       z_layer=settings.LAYER_BOTTOM)
        self.spritesystem.add_sprite(inside_sprite)

    def create_port(self, dist, w, h, integrity, scan_type, wall_idx):
        wall = self.fire_walls[wall_idx]
        pw = w if wall.nx == 0 else h
        ph = h if wall.nx == 0 else w
        area = pygame.Rect(0, 0, pw, ph)
        area.normalize()
        area.centerx = wall.sx + wall.vx * dist
        area.centery = wall.sy + wall.vy * dist
        angle = -Vec2(wall.vx, wall.vy).angle
        direction = Vec2(wall.nx, wall.ny)
        port = Port(self, area, angle, scan_type, integrity, direction)
        return port

    def enter(self):
        """Called when this context is pushed onto the stack."""
        pygame.mouse.set_visible(False)
        pygame.event.set_grab(True)

        # generate seed so it seems random and plays every time different
        seed = int(time.time())
        random.seed(seed)

        # seed = settings.RANDOM_SEED  # uncomment this two lines to play the levels always the same random order
        # random.seed()
        logger.info("SEED: {0}", seed)  # need to log the seed, so in case of a bug it can be reproduced

        self.resources.load_resources(self.level.img_res_map, self.level.sfx_res_map, self.level.music_res_map,
                                      self.level.font_res_map, self.level.animation_res_map)

        self.create_inside_area_sprite(self.level)

        self.fire_walls = [
            Wall(self, self.inside_rect.topleft, self.inside_rect.topright, self.level.wall_width),
            Wall(self, self.inside_rect.topright, self.inside_rect.bottomright, self.level.wall_width),
            Wall(self, self.inside_rect.bottomright, self.inside_rect.bottomleft, self.level.wall_width),
            Wall(self, self.inside_rect.bottomleft, self.inside_rect.topleft, self.level.wall_width),
        ]

        self.ports = []
        for wall_idx, dist, w, h, scan_type, integrity in self.level.ports:
            port = self.create_port(dist, w, h, integrity, scan_type, wall_idx)
            self.ports.append(port)

        inside_pos = Vec2(self.inside_rect.left, self.inside_rect.top)

        self.turrets = []
        for turret_config in self.level.turrets:
            if turret_config is None:
                continue
            px, py, config = turret_config
            turret = Turret(self, px + inside_pos.x, py + inside_pos.y, config)
            self.turrets.append(turret)

        self.turret2_available = len(self.turrets) == 2

        # bullets do register and remove themselves
        self.bullets = []

        self.cursor = Cursor(self)

        self.cores = []
        for core_pos, size in self.level.cores:
            self.cores.append(Core(self, inside_pos + core_pos, size))

        for wave in self.level.waves:
            delay = wave["delay"]
            for gen_config in wave["generators"]:
                gen = Generator(self, self.level.wave_areas, delay, gen_config)
                self._generators.append(gen)

        self.collision_functions[settings.BULLET_TYPE | settings.TYPE_PACKAGE] = self.collide_bullet_and_package
        self.collision_functions[settings.BULLET_TYPE | settings.PORT_TYPE] = self.collide_bullet_and_port

        font_args = settings.LOLIGHT_FONT_COLOR
        y = 200
        x = settings.SCREEN_WIDTH / 2
        messages = self.level.start_messages
        self.show_messages(font_args, messages, x, y, settings.LEVEL_START_MESSAGE_DURATION)

        if settings.FEATURE_ADD_URLS:
            self.add_urls()

    def show_messages(self, font_args, messages, x, y, duration, end_cb=None):
        if end_cb is None:
            end_cb = self.remove_text
        for msg in messages:
            image = ptext.getsurf(msg, 'BIOST', 80, width=settings.SCREEN_WIDTH * 2 / 3, **font_args)
            sprite = Sprite(image, Point2(x, y), z_layer=settings.LAYER_OVERLAY_TEXT)
            y += settings.LEVEL_MESSAGES_DISTANCE
            self.tweener.create_tween(sprite, "alpha", 255.0, -255.0, duration, IN_SINE, cb_end=end_cb)
            self.spritesystem.add_sprite(sprite)

    def remove_text(self, tweener, obj, attr_name, *args):
        self.tweener.remove_tween(obj, attr_name)

    def remove_package(self, package):
        self.spritesystem.remove_sprites(package.sprites)
        self.packages.remove(package)
        package.selected_port.remove_package_from_queue(package)

    def exit(self):
        """Called when this context is popped off the stack."""
        pygame.mouse.set_visible(True)
        pygame.event.set_grab(False)

    def suspend(self):
        """Called when another context is pushed on top of this one."""
        pass
        pygame.event.set_grab(False)

    def resume(self):
        """Called when another context is popped off the top of this one."""
        pygame.mouse.set_visible(False)
        pygame.event.set_grab(True)

    def think(self, delta_time):
        """Called once per frame"""
        self.handle_events()

        self.tweener.update(delta_time)
        self.scheduler.update(delta_time)
        self.animator.update(delta_time)

        for bullet in self.bullets:
            bullet.update(delta_time)

        for p in self.packages:
            p.update(delta_time)

        for port in self.ports:
            port.update(delta_time)

        self.check_collisions()

        self.check_level_completed()

    def handle_events(self):
        key_mods = pygame.key.get_mods()
        pressed = pygame.key.get_pressed()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.pop(Context._stack_length(), False)
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == settings.MOUSE_BUTTON_TURRET1_FIRE:
                    self._trigger_turret_fire(key_mods, pressed, self.turrets[0])
                elif event.button == settings.MOUSE_BUTTON_TURRET2_FIRE:
                    # right button, right turret primary fire
                    if self.turret2_available:
                        self._trigger_turret_fire(key_mods, pressed, self.turrets[1])

            elif event.type == pygame.MOUSEBUTTONUP:
                if event.button == settings.MOUSE_BUTTON_TURRET1_FIRE:
                    self._trigger_turret_fire_release(key_mods, pressed, self.turrets[0])
                elif event.button == settings.MOUSE_BUTTON_TURRET2_FIRE:
                    # right button, right turret primary fire
                    if self.turret2_available:
                        self._trigger_turret_fire_release(key_mods, pressed, self.turrets[1])

            elif event.type == pygame.MOUSEMOTION:
                for turret in self.turrets:
                    turret.aim_at(*event.pos)
                self.cursor.aim_at(*event.pos)

            elif event.type == pygame.KEYDOWN:
                # cheat keys
                if event.key == settings.KEY_CHEAT_WIN:
                    self.level.score.has_won = True
                    self.level.score.message = "CHEAT WIN!"
                    # self.pop()
                    # self.push(ScoreContext(self.level))
                    self.show_level_end_messages()
                elif event.key == settings.KEY_CHEAT_LOOSE:
                    self.level.score.has_won = False
                    self.level.score.message = "CHEAT LOSE!"
                    # self.pop()
                    # self.push(ScoreContext(self.level))
                    self.show_level_end_messages()
                elif event.key == settings.KEY_CHEAT_EXIT:
                    self.pop(2, do_resume=False)

                # volumes
                elif event.key == settings.KEY_INCREASE_MUSIC_VOLUME:
                    music = self.resources.get_music(settings.RES_MUS_1)
                    self.change_volume(music, settings.VOLUME_CHANGE)
                elif event.key == settings.KEY_DECREASE_MUSIC_VOLUME:
                    music = self.resources.get_music(settings.RES_MUS_1)
                    self.change_volume(music, -settings.VOLUME_CHANGE)
                elif event.key == settings.KEY_INCREASE_SFX_VOLUME:
                    sounds = self.resources.get_all_sounds()
                    for sound in sounds:
                        self.change_volume(sound, settings.VOLUME_CHANGE)
                elif event.key == settings.KEY_DECREASE_SFX_VOLUME:
                    sounds = self.resources.get_all_sounds()
                    for sound in sounds:
                        self.change_volume(sound, -settings.VOLUME_CHANGE)

                # toggles
                elif event.key == settings.KEY_TOGGLE_POWER_SCAN_SFX_ENABLED:
                    settings.POWER_SCAN_SFX_ENABLED = not settings.POWER_SCAN_SFX_ENABLED
                elif event.key == settings.KEY_TOGGLE_SCAN_SFX_ENABLED:
                    settings.SCAN_SFX_ENABLED = not settings.SCAN_SFX_ENABLED
                elif event.key == settings.KEY_TOGGLE_SIREN_SFX_ENABLED:
                    settings.SIREN_SFX_ENABLED = not settings.SIREN_SFX_ENABLED
                elif event.key == settings.KEY_TOGGLE_PACKAGE_SUCK_SFX_ENABLED:
                    settings.PACKAGE_SUCK_SFX_ENABLED = not settings.PACKAGE_SUCK_SFX_ENABLED
                elif event.key == settings.KEY_PAUSE:
                    self.push(ContextPause())

                # game keys
                elif event.key == settings.KEY_TOGGLE_DEBUG_RENDER:
                    settings.DEBUG_RENDER = not settings.DEBUG_RENDER
                elif event.key == settings.KEY_TURRET1_FIRE:
                    self._trigger_turret_fire(key_mods, pressed, self.turrets[0])
                elif event.key == settings.KEY_TURRET2_FIRE:
                    if self.turret2_available:
                        self._trigger_turret_fire(key_mods, pressed, self.turrets[1])

            elif event.type == pygame.KEYUP:
                if event.key == settings.KEY_TURRET1_FIRE:
                    self._trigger_turret_fire_release(key_mods, pressed, self.turrets[0])
                elif event.key == settings.KEY_TURRET2_FIRE:
                    if self.turret2_available:
                        self._trigger_turret_fire_release(key_mods, pressed, self.turrets[1])

    def change_volume(self, audio, change):
        old_volume = audio.get_volume()
        new_volume = old_volume + change
        if 0.0 < new_volume <= 1.0:
            logger.info("set volume from {0} + {3} to {1} on object {2}".format(old_volume, new_volume, audio, change))
            audio.set_volume(new_volume)

    def _trigger_turret_fire(self, key_mods, pressed, turret_):
        if key_mods & settings.MOD_KEY_SECONDARY_FIRE_MODE or pressed[settings.KEY_SECONDARY_FIRE_MODE]:
            # right button, left turret secondary fire
            turret_.fire_secondary_press()
        else:
            # right button, left turret primary fire
            turret_.fire_press()

    def _trigger_turret_fire_release(self, key_mods, pressed, turret_):
        if key_mods & settings.MOD_KEY_SECONDARY_FIRE_MODE or pressed[settings.KEY_SECONDARY_FIRE_MODE]:
            # left button, left turret secondary fire
            turret_.fire_secondary_release()
        else:
            # left button, left turret primary fire
            turret_.fire_release()

    def draw(self, screen, do_flip):
        """Refresh the screen"""
        # self.spritesystem.draw(self, surf, cam, fill_color=None, do_flip=False, interpolation_factor=1.0):
        self.spritesystem.draw(screen, self.cam, fill_color=self.level.color_outside, do_flip=False)

        if settings.DEBUG_RENDER:
            for p in self.packages:
                if p.package_type.__class__ == PackageTypeClean or p.package_type.__class__ == PackageTypeCleanUnsure:
                    color = (0, 255, 0)
                else:
                    color = (255, 0, 0)
                pygame.draw.rect(screen, color, p.sprite.rect, 3)
            for port in self.ports:
                pygame.draw.rect(screen, (255, 0, 0), port.rect, 2)
                pygame.draw.circle(screen, (255, 0, 0), port.position.rounded(int).as_tuple(), 4, 2)
                for p in port.queue:
                    pygame.draw.rect(screen, (255, 255, 0), p.sprite.rect, 1)
            for wall in self.fire_walls:
                mx = wall.sx + wall.vx / 2
                my = wall.sy + wall.vy / 2
                pygame.draw.line(screen, (0, 0, 255), (mx, my), (mx + wall.nx * 10, my + wall.ny * 10), 1)

        if do_flip:
            pygame.display.flip()

    def check_collisions(self):
        func_bullet_package = self.collision_functions[settings.BULLET_TYPE | settings.TYPE_PACKAGE]
        # func_bullet_port = self.collision_functions[settings.BULLET_TYPE | settings.PORT_TYPE]
        for b in self.bullets:
            # for p in self.ports:
            #     if func_bullet_port(b, p):
            #         break
            for p in self.packages:
                if func_bullet_package(b, p):  # I know that settings.BULLET_TYPE < settings.TYPE_PACKAGE
                    break

        for package in self.packages:
            if package.current_state == StateMoveToCore and self.cursor.position.get_distance_sq(
                    package.position) < 30 * 30:
                self.cursor.add_to_tracked_sprites(package)
            else:
                self.cursor.remove_from_tracked_sprites(package)

    def collide_bullet_and_port(self, bullet, port):
        if port.rect.collidepoint(bullet.position.as_tuple()):
            bullet.destroy_bullet()
            return True
        return False

    def collide_bullet_and_package(self, bullet, package):
        if package.position.get_distance_sq(bullet.position) < settings.PACKAGE_BULLET_COLLISION_RADIUS_SQ:
            bullet.destroy_bullet()

            # calculate damage
            if package.calculate_damage(bullet.bullet_type.damage):
                self.remove_package(package)
                # record score
                self.level.score.add_kill_package(package)
            return True
        return False

    def check_level_completed(self):
        friendy_kill_count = self.level.score.kills.get(PackageTypeClean, 0) + self.level.score.kills.get(
            PackageTypeCleanUnsure, 0)
        if friendy_kill_count > self.level.goal_kill_friendly_max:
            self.level.score.message = "LOSE, data loss it too high! Try again!"
            self.level.score.has_won = False

            self.show_level_end_messages()
            # self.push(ScoreContext(self.level))

        if len([g for g in self._generators if not g.finished]) == 0 and len(self.packages) == 0:
            if sum(self.level.score.kills.values()) - friendy_kill_count < self.level.goal_kill_baddies_min:
                self.level.score.message = "LOSE, too many corrupted packages made it to the cores! Try again!"
                self.level.score.has_won = False
            else:
                self.level.score.message = "WIN, you protected the cores well!"
                self.level.score.has_won = True

            self.show_level_end_messages()
            # self.push(ScoreContext(self.level))

    def show_level_end_messages(self):
        self.push(ContextTransitionLevels(self.level))

    def add_urls(self):
        screen_rect = self.cam.rect
        border = 50
        surf = pygame.Surface((screen_rect.w, screen_rect.h))
        surf.fill(self.level.color_outside)
        rect = self.inside_rect.copy()
        rect.width = rect.width * 3 / 4
        prefixes = ["https://", "http://", ""]
        for i in range(settings.URL_SHOWN_COUNT):
            text = random.choice(prefixes) + random.choice(settings.list_of_urls)
            x = random.randint(-2 * border, screen_rect.width + border)
            y = random.randint(-2 * border, screen_rect.height + border)

            font_args = dict(color=pygame.Color('gray93'), scolor=pygame.Color('darkgrey'), owidth=1)

            fontsize = random.randint(10, 35)
            img = ptext.getsurf(text, 'BIOST', fontsize, **font_args)
            surf.blit(img, (x, y))
        spr = Sprite(surf, Point2(screen_rect.centerx, screen_rect.centery), z_layer=settings.LAYER_INTERNET)
        self.spritesystem.add_sprite(spr)
