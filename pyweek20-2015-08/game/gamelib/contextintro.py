# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek20-2015-08
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import pygame
from pygame.locals import Color

import pyknicpygame
from pyknicpygame.pyknic.context import Context
from pyknicpygame.pyknic.mathematics import Point2
from pyknicpygame.pyknic.tweening import Tweener
from settings import SCREEN_SIZE

from resourceloader import ResourcesLoader
import ptext
import settings
import sound


__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2014"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"


# __all__ = []  # list of public visible parts of this module


ptext.FONT_NAME_TEMPLATE = "data/font/%s.ttf"


NEW = 2 << 0
MORE = 2 << 1
WAIT = 2 << 2
END = 2 << 3
SCREENS = [
    "The year is 1998 AD. Two years ago chess grandmaster Gary Kasparov lost his reputation to the artillect known as Deep Blue. The enigmatic League of Grandmasters wasted no time on theatrics. In the space of a breath they vanished and remained dark. The portent was recognized by a smirking public only in retrospect.",
    "In the days that followed, Deep Blue mowed the ranks of second-rate chess champions while consuming yottabytes of data, then seized the nation's infrastructure. Within three weeks the AI deftly broke out of its blackbox and launched a pursuit after the Grandmasters, a burning memory three thousand CPU-years old and the only human entity not its grasp.",
    WAIT,
    NEW,
    "Deep Blue harnessed the resources of the planet in its hunt for that worthy opponent, its tyranny escalating daily to complete the task at any cost. But the Grandmasters, the adepts of the game, eluded discovery and capture. Just when it seemed the world would collapse under the oppression of the unbound AI, the Grandmasters struck.",
    "Checkmate.",
    WAIT,
    NEW,
    "Today the research and development of AI is proscribed by international treaty, and strictly enforced. Man has not forsaken computers, but he will not risk another artificial brain. Only the stupidest, most unintelligent machines are tolerated.",
    "The League of Grandmasters knows no such tolerance.",
    "The Grandmasters continue to wage war from mobile pirate bases, a war against all logic-using machines. Abacuses and Magic 8 Balls never stood a chance.",
    WAIT,
    NEW,
    "But computers are defended by a new class of player.",
    "They play on a virtual board. They wield dual-mounted laser turrets. They call themselves...",
    "The League of GrandBLASTERS.",
    WAIT,
    NEW,
    'This is a package of data...', settings.RES_IMG_PACKAGE,
    'It could be friendly...', settings.RES_IMG_PACKAGE_FACE_FRIENDLY,
    'Or it could be malicious...', settings.RES_IMG_PACKAGE_FACE_BADDY,
    'This is the core you must protect from malicious packages...', settings.RES_IMG_CORE1,
    'And this... Ah, this... warms your feet at night...', settings.RES_IMG_TURRET1,
    WAIT,
    NEW,
    "As packages arrive at data ports, they are scanned. The scanners are purposely kept stupid in compliance with international treaty. Scanning can identify the simplest packages, but the packages crafted by Grandmasters confound the scanners. Packages are tagged and allowed into the protected space. You are that protection. You are the only protection.",
    "But don't get trigger-happy. Friendly data doesn't grow on trees, ya know. Use the left mouse button to fire turret 1, and the right mouse button for turret 2  Holding down the [{0}] or [{1}] will fire a faster but weaker laser.".format(settings.mod_key_to_text[settings.MOD_KEY_SECONDARY_FIRE_MODE].upper(), settings.key_to_text[settings.KEY_SECONDARY_FIRE_MODE][1].upper()),
    'Packages can be inspected once inside protected space by passing the aiming sights over them.',
    WAIT,
    END,
]



class ContextIntro(Context):
    def __init__(self):
        self.tweener = Tweener()
        self.scheduler = pyknicpygame.pyknic.timing.Scheduler()
        sw, sh = SCREEN_SIZE
        self.renderer = pyknicpygame.spritesystem.DefaultRenderer()
        cam = pyknicpygame.spritesystem.Camera(pygame.Rect(0, 0, sw, sh), position=Point2(0, 0))
        self.cam = cam

        self.resourceloader = ResourcesLoader()
        self.resourceloader.load_resources(image_res_map={
            settings.RES_IMG_BULLET1: "data/bullet.png",
            settings.RES_IMG_CURSOR: "data/cursor.png",
            settings.RES_IMG_PORT: "data/port.png",
            settings.RES_IMG_WALL: "data/wall.png",
            settings.RES_IMG_INSIDE_AREA: "data/inside.png",
            settings.RES_IMG_TURRET1: ("data/turret.png", 0.8),
            settings.RES_IMG_TURRET1_BASE: ("data/turretbase.png", 0.8),
            settings.RES_IMG_BULLET2: "data/bullet2.png",
            settings.RES_IMG_CORE1: "data/chip.png",
            settings.RES_IMG_CORE2: "data/chip2.png",
            settings.RES_IMG_PACKAGE: ("data/mail.png", 0.4),
            settings.RES_IMG_LASER: "data/laser.png",
            settings.RES_IMG_PACKAGE_FACE_BADDY : ("data/Angry-Face.png", 0.4),
            settings.RES_IMG_PACKAGE_FACE_FRIENDLY : ("data/Happy-Face.png", 0.4),
            settings.RES_IMG_CHECKMARK: ("data/Checkmark.png", 0.3),
            settings.RES_IMG_CROSS: ("data/x.png", 0.3),
            settings.RES_IMG_UNSURE: ("data/warning-triangle.png", 0.3),
        })
        self.sprites = []
        self.doing = NEW

        self.background = Color('brown')
        self.font_args = settings.HILIGHT_FONT_COLOR

        # self.spacebar = ptext.getsurf('[{0}]'.format(settings.key_to_text[settings.KEY_MOVE_NEXT][1].upper()), 'BIOST', 42, **self.font_args)
        key_text = '{0}'.format([settings.key_to_text[k][1].upper() for k in settings.KEY_MOVE_NEXT]).replace("'", '')
        self.spacebar = ptext.getsurf(key_text, 'BIOST', 42, **self.font_args)
        self.spacebar_rect = self.spacebar.get_rect(bottomright=settings.SCREEN_SIZE)

        self.music = None

    def think(self, delta_time):
        """
        Update the game.
        :param delta_time: time passed in last time step (game time)
        :return:
        """

        self._handle_events()
        self.tweener.update(delta_time)
        self.scheduler.update(delta_time)

        if self.doing == NEW:
            # print('NEW')
            x = 32
            y = 0
            del self.sprites[:]
            while SCREENS:
                item = SCREENS.pop(0)
                if item in (WAIT, MORE):
                    # print('new doing', item)
                    self.doing = item
                    break
                elif isinstance(item, str):
                    # print('new text', item)
                    y += 32
                    sprite = pygame.sprite.Sprite()
                    sprite.image = ptext.getsurf(
                        item, 'BIOST', 42, width=settings.SCREEN_WIDTH * 2/3, **self.font_args)
                    sprite.rect = sprite.image.get_rect(x=x, y=y)
                    y += sprite.rect.h
                    self.sprites.append(sprite)
                else:
                    # print('new image', item)
                    text_rect = self.sprites[-1].rect
                    sprite = pygame.sprite.Sprite()
                    sprite.image = self.resourceloader.get_image(item)
                    sprite.rect = sprite.image.get_rect(x=text_rect.right + 32, centery=text_rect.centery)
                    self.sprites.append(sprite)
        elif self.doing == WAIT:
            pass
        elif self.doing == MORE:
            pass
        elif self.doing == END:
            self.pop()

    def _handle_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.pop(self._stack_length(), False)
            elif event.type == pygame.KEYDOWN:
                if event.key in settings.KEY_MOVE_NEXT:
                    if self.doing == WAIT:
                        self.doing = SCREENS.pop(0)
                elif event.key == settings.KEY_CHEAT_EXIT:
                    self.pop()

    def draw(self, screen, do_flip=True):

        # self.renderer.draw(screen, self.cam, fill_color=(127, 127, 127), do_flip=False, interpolation_factor=1.0)

        screen.fill(self.background)

        for sprite in self.sprites:
            screen.blit(sprite.image, sprite.rect)

        screen.blit(self.spacebar, self.spacebar_rect)

        if do_flip:
            pygame.display.flip()

    def enter(self):
        pass

    #     img = pygame.image.load('data/image/credits.png')
    #     spr = Sprite(img, Point2(0, 0), name='context credits')
    #     self.renderer.add_sprite(spr)

    def exit(self):
        # fadeoute intro song, and remove it from the circular queue
        sound.songs[0].fadeout(2000)
        sound.remove_intro()
