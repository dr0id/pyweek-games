# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek20-2015-08
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function
import pygame

import settings
import sound
import spritesheetlib

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module


class ResourcesLoader(object):
    def __init__(self):
        self._sound_map = {}  # {res_id: sound}
        self._img_map = {}  # {res_id: surface}
        self._music_map = {}  # {res_id: music}
        self._font_map = {}  # {res_id: font}
        self._animation_map = {}

    def load_resources(self, image_res_map=None, sound_res_map=None, music_res_map=None, font_res_map=None,
                       animation_res_map=None):
        """
        Pass in the res_id to filename mapping to load the resources.
        :param image_res_map: {res_id : filename}
        :param sound_res_map: {res_id : (filename, volume)}
        :param music_res_map: {res_id : filename}
        :param font_res_map: {res_id : (filename, size)}
        :param animation_res_map: {res_id : filename}
        """
        if image_res_map:
            self._load_resource(image_res_map, self._load_image, self._img_map)

        if sound_res_map:
            self._load_resource(sound_res_map, self._load_sound, self._sound_map)

        if music_res_map:
            self._load_resource(music_res_map, self._load_music, self._music_map)

        if font_res_map:
            self._load_resource(font_res_map, self._load_font, self._font_map)

        if animation_res_map:
            self._load_resource(animation_res_map, self._load_animation, self._animation_map)

    @staticmethod
    def _load_resource(res_id_map, func, resource_map):
        for res_id, data in res_id_map.items():
            if isinstance(data, str):
                res = func(data)
            else:
                res = func(*data)
            resource_map[res_id] = res

    @staticmethod
    def scale_to_size(surface, size):
        return pygame.transform.scale(surface, size)

    def get_image(self, res_id):
        return self._img_map[res_id]

    def get_sound(self, res_id):
        return self._sound_map[res_id]

    def get_music(self, res_id):
        return self._music_map[res_id]

    def get_font(self, res_id):
        return self._font_map[res_id]

    def get_animation(self, res_id):
        return self._animation_map[res_id]

    def get_all_sounds(self):
        return list(self._sound_map.values())

    def _load_image(self, filename, scale=None, angle=0):
        img = pygame.image.load(filename).convert_alpha()
        if scale and scale > 0.0:
            img = pygame.transform.rotozoom(img, angle, scale)
        return img

    def _load_sound(self, filename, volume):
        sfx = pygame.mixer.Sound(filename)
        sfx.set_volume(volume)
        return sfx

    def _load_music(self, name, filename):
        song = sound.Song(name, filename, settings.MUSIC_VOLUME)
        song.set_volume(settings.MUSIC_VOLUME)
        if song not in sound.songs:
            sound.songs.append(song)
        if settings.PLAY_MUSIC:
            sound.roll_songs()
        return song

    def _load_font(self, filename):
        name, size = filename
        return pygame.font.Font(name, size)

    def _load_animation(self, filename, zoom=1.0):
        sprites = spritesheetlib.SpritesheetLib10().load_spritesheet(spritesheetlib.FileInfo(filename))
        if zoom != 1.0:
            for spr in sprites:
                spr.image = pygame.transform.rotozoom(spr.image, 0.0, zoom)
        return sprites
