# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek20-2015-08
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import pygame

from pyknicpygame.pyknic.mathematics import Point2, Vec2
from pyknicpygame.pyknic.tweening import LINEAR
import settings

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module


class Wall(object):
    def __init__(self, world, start, end, width):
        self.world = world
        sx, sy = start
        ex, ey = end
        self.sx = sx
        self.sy = sy
        self.ex = ex
        self.ey = ey

        self.vx = ex - sx
        self.vy = ey - sy
        self.vl = (self.vx ** 2 + self.vy ** 2) ** 0.5
        self.nx = self.vy / self.vl
        self.ny = -self.vx / self.vl

        w = width
        rot = 0
        if self.nx == 0:
            pw = w
            ph = self.vl
            self.rect = pygame.Rect(0, 0, ph, pw)
            self.rect.normalize()
            if self.ny > 0:
                self.rect.midright = (sx, sy)
            else:
                self.rect.midleft = (sx, sy)

        else:
            pw = self.vl
            ph = w
            self.rect = pygame.Rect(0, 0, ph, pw)
            self.rect.normalize()
            rot = 90
            if self.nx > 0:
                self.rect.midtop = (sx, sy)
            else:
                self.rect.midbottom = (sx, sy)

        img = self.world.resources.get_image(settings.RES_IMG_WALL)  # "data/wall.png")
        if rot:
            img = self.world.resources.scale_to_size(img, (self.rect.h, self.rect.w))
        else:
            img = self.world.resources.scale_to_size(img, self.rect.size)
        sprite = self.world.animator.create_animation(img, Point2(self.rect.centerx, self.rect.centery),
                                                      z_layer=settings.LAYER_WALL)
        sprite.rotation = -Vec2(self.vx, self.vy).angle
        self.world.spritesystem.add_sprite(sprite)


class Bullet(object):
    def __init__(self, world, pos, target_pos, direction, bullet_type, turret):
        self.turret = turret
        self.max_distance_sq = (turret.position - target_pos).length_sq
        self.world = world
        self.position = pos
        self.direction = direction
        self.type = bullet_type.type

        self.bullet_type = bullet_type

        self.v = Vec2(direction.x, direction.y) * bullet_type.speed

        img = self.world.resources.get_image(bullet_type.image)
        # self.sprite = Sprite(img, self.position, z_layer=settings.LAYER_BULLET)
        self.sprite = self.world.animator.create_animation(img, self.position, z_layer=settings.LAYER_BULLET)
        self.sprite.rotation = -self.direction.angle
        self.world.spritesystem.add_sprite(self.sprite)

        # self.world.scheduler.schedule(self.destroy_bullet, bullet_type.time_to_live)
        self.world.bullets.append(self)
        turret.inc_bullet(self)

    def update(self, dt):
        self.position += self.v * dt
        if (self.position - self.turret.position).length_sq > self.max_distance_sq:
            self.destroy_bullet()

    def destroy_bullet(self, *args):
        if self.world.spritesystem.remove_sprite(self.sprite):
            self.world.bullets.remove(self)
            self.turret.dec_bullet(self)
        return None


class Turret(object):
    def __init__(self, world, x, y, turret_type):
        self.world = world
        self.position = Point2(x, y)
        self.turret_type = turret_type
        self.aim_point = Point2(0.0, 0.0)
        self.direction = Vec2(1.0, 0.0)
        self.bullets = []

        base_img = self.world.resources.get_image(turret_type.image_base)
        self.base_spr = self.world.animator.create_animation(base_img, Point2(x, y), z_layer=settings.LAYER_TURRET_BASE)
        self.world.spritesystem.add_sprite(self.base_spr)

        t_image = self.world.resources.get_image(turret_type.image_turret)
        self.sprite = self.world.animator.create_animation(t_image, Point2(x, y), anchor=Point2(-25, 0.0),
                                                           z_layer=settings.LAYER_TURRET)
        self.world.spritesystem.add_sprite(self.sprite)

    def inc_bullet(self, bullet):
        self.bullets.append(bullet)

    def dec_bullet(self, bullet):
        try:
            self.bullets.remove(bullet)
        except ValueError:
            pass

    def fire_secondary_press(self):
        self.create_bullet(self.turret_type.bullet_type_secondary)

    def fire_secondary_release(self):
        pass

    def fire_press(self):
        self.create_bullet(self.turret_type.bullet_type)

    def create_bullet(self, bullet_type):
        if len(self.bullets) < self.turret_type.bullet_max_fired:
            turret_length = self.turret_type.turret_length
            if self.position.get_distance(self.aim_point) > turret_length:
                turret_dir = self.direction.normalized
                pos = self.position + turret_dir * turret_length
                Bullet(self.world, pos, Point2(self.aim_point.x, self.aim_point.y), turret_dir,
                       bullet_type, self)
                self.world.resources.get_sound(bullet_type.sound).play()

    def fire_release(self):
        pass

    def aim_at(self, x, y):
        self.aim_point.x = x
        self.aim_point.y = y

        self.direction.x = x - self.position.x
        self.direction.y = y - self.position.y
        # self.direction.normalize()

        self.sprite.rotation = -self.direction.angle


class Cursor(object):
    def __init__(self, world):
        self.position = Point2(0, 0)
        self.world = world

        img = self.world.resources.get_image(settings.RES_IMG_CURSOR)  # "data/cursor.png")
        self.sprite = self.world.animator.create_animation(img, self.position, z_layer=settings.LAYER_CURSOR)
        self.world.spritesystem.add_sprite(self.sprite)
        self.tracked_sprites = []

    def aim_at(self, x, y):
        self.position.x = x
        self.position.y = y

    def remove_from_tracked_sprites(self, package):
        try:
            self.tracked_sprites.remove(package.sprite_face)
            self.world.spritesystem.remove_sprite(package.sprite_face)
        except ValueError as ve:
            pass

    def add_to_tracked_sprites(self, package):
        sprite = package.sprite_face
        if sprite not in self.tracked_sprites:
            self.tracked_sprites.append(sprite)
            self.world.spritesystem.add_sprite(sprite)
            self.world.tweener.create_tween(sprite, "alpha", 0, 200, settings.POWER_SCAN_DURATION,
                                            tween_function=LINEAR, immediate=True)
            self.start_scan(package)

    def start_scan(self, package):
        sprite = package.sprite
        img = self.world.resources.get_image(settings.RES_IMG_LASER)
        pos = Point2(0, 0)
        pos.copy_values(sprite.position)

        img_get_size = img.get_size()
        func = LINEAR
        duration = settings.POWER_SCAN_DURATION
        travel_dist_x = package.direction.x * package.speed * duration
        travel_dist_y = package.direction.y * package.speed * duration

        ls = self.world.animator.create_animation(img, pos, z_layer=settings.LAYER_LASER)
        ls.anchor = "topleft"
        ls.zoom = float(sprite.rect.height) / img_get_size[0]
        ls.rotation = 90
        ls.surf_flags = pygame.BLEND_RGB_ADD
        self.world.spritesystem.add_sprite(ls)
        change_x = sprite.rect.width + travel_dist_x
        self.world.tweener.create_tween(ls.position, "x", pos.x - sprite.rect.width / 2.0, change_x, duration,
                                        tween_function=func,
                                        cb_end=lambda *args: self.world.spritesystem.remove_sprite(args[-1][0]),
                                        cb_args=ls)
        change_y = travel_dist_y
        self.world.tweener.create_tween(ls.position, "y", pos.y, change_y, duration, tween_function=func)

        pos2 = Point2(pos.x, pos.y)
        ls = self.world.animator.create_animation(img, pos2, z_layer=settings.LAYER_LASER)
        ls.zoom = float(sprite.rect.width) / img_get_size[0]
        ls.anchor = "topleft"
        # ls.rotation = 1
        ls.surf_flags = pygame.BLEND_RGB_ADD
        self.world.spritesystem.add_sprite(ls)

        change_x = travel_dist_x
        self.world.tweener.create_tween(ls.position, "x", pos.x, change_x, duration, tween_function=func)

        change_y = sprite.rect.height + travel_dist_y
        self.world.tweener.create_tween(ls.position, "y", pos2.y - sprite.rect.height / 2.0, change_y, duration,
                                        tween_function=func,
                                        cb_end=lambda *args: self.world.spritesystem.remove_sprite(args[-1][0]),
                                        cb_args=ls)
        if settings.POWER_SCAN_SFX_ENABLED:
            self.world.resources.get_sound(settings.RES_SFX_POWERSCAN).play()


class Core(object):
    def __init__(self, world, position, size):
        self.world = world
        self.position = position
        self.size = size

        img = self.world.resources.get_image(settings.RES_IMG_CORE1)
        self.sprite = self.world.animator.create_animation(img, position, z_layer=settings.LAYER_CORE)
        self.sprite.zoom = size
        self.world.spritesystem.add_sprite(self.sprite)

        self.rect = img.get_rect(center=position.as_tuple())

    def receive_package(self, package):
        self.world.remove_package(package)
        self.world.level.score.add_core_package(package)

        self.world.spritesystem.add_sprite(package.sprite)
        duration = settings.PACKAGE_SUCK_IN_DURATION
        self.world.tweener.create_tween(package.sprite, "zoom", package.sprite.zoom, -package.sprite.zoom, duration,
                                        cb_end=lambda *args: self.world.spritesystem.remove_sprite(args[-1][0]),
                                        cb_args=package.sprite)
        if settings.PACKAGE_SUCK_SFX_ENABLED:
            self.world.resources.get_sound(settings.RES_SFX_SUCK_IN_PACKAGE).play()
