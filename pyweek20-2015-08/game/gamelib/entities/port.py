# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek20-2015-08
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import pygame
from .entitytypes import ThreadLevels

from pyknicpygame.pyknic import tweening
from pyknicpygame.pyknic.mathematics import Point2
import settings

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module


class Port(object):
    def __init__(self, world, area, angle, scan_type, integrity, direction):
        self.current_package = None
        self.direction = direction  # point to the outside
        self.world = world
        self.scan_type = scan_type
        self.integrity = integrity
        self.angle = angle
        self.rect = area
        self.type = settings.PORT_TYPE
        self.position = Point2(self.rect.centerx, self.rect.centery)

        img = self.world.resources.get_image(settings.RES_IMG_PORT)
        img_laser = self.world.resources.get_image(settings.RES_IMG_LASER)
        if area.w > area.h:
            img = self.world.resources.scale_to_size(img, (area.h, area.w))
            laser_w = area.h
        else:
            img = self.world.resources.scale_to_size(img, area.size)
            laser_w = area.w
        self.sprite = self.world.animator.create_animation(img, self.position, z_layer=settings.LAYER_PORT)
        self.sprite.rotation = angle

        self.world.spritesystem.add_sprite(self.sprite)

        self.laser_sprite = self.world.animator.create_animation(img_laser,
                                                                 self.position - self.direction * settings.PORT_LASER_OFFSET,
                                                                 z_layer=settings.LAYER_LASER)
        self.laser_sprite.surf_flags = pygame.BLEND_RGB_ADD
        self.laser_sprite.zoom = float(laser_w) / img_laser.get_size()[0] * settings.PORT_LASER_WIDTH_FACTOR
        self.laser_sprite.rotation = angle

        self.queue = []
        self.packages_moving_to_queue = []

        self.current_state = 0  # 0 fetch, 1 scan, 2 wait for scan completion

    def queue_package(self, package):
        # calculate slot position
        package.sprite.z_layer = settings.LAYER_PACKAGE_IN_PORT_QUEUE
        pos = self.position + self.direction * settings.PORT_QUEUE_OFFSET
        if self.queue:
            pos = self.queue[-1].position + self.direction * (package.sprite.rect.h + settings.PORT_QUEUE_SPACE)
        package.position.copy_values(pos)
        self.queue.append(package)

    def update(self, dt):
        queue_move_duration = settings.PORT_MOVE_QUEUE_DURATION
        if self.current_state == 0:  # fetch
            if self.queue:

                # current package already removed from queue
                self.current_package = self.queue.pop(0)
                self.current_package.position.copy_values(self.position)
                self.current_package.z_layer = settings.LAYER_PACKAGE_IN_PORT_SCAN

                if self.queue:
                    # move packages in queue
                    pos = self.position + self.direction * settings.PORT_QUEUE_OFFSET
                    change = pos - self.queue[0].position
                    for p in self.queue:

                        self.world.tweener.create_tween(p.position, "x", p.position.x, change.x,
                                                        queue_move_duration)
                        self.world.tweener.create_tween(p.position, "y", p.position.y, change.y,
                                                        queue_move_duration)
                # update state
                self.current_state = 1
        elif self.current_state == 1:

            # scanning
            scan_duration = self.scan_type.scan_duration
            if settings.FEATURE_SCAN_DURATION_DEPENDS_ON_PACKAGE_SIZE:
                scan_duration *= self.current_package.size
            self.world.scheduler.schedule(self._end_scan, scan_duration)  # self.scan_type.scan_duration)
            self.current_state = 2  # wait for scan completion

            self.start_laser_scan(scan_duration)
            # TODO: scan result
        else:
            pass

    def start_laser_scan(self, scan_duration):
        # laser sprite
        ls = self.laser_sprite
        self.world.spritesystem.add_sprite(ls)
        change = self.direction * settings.PORT_LASER_DISTANCE
        ls.position = self.position - self.direction * settings.PORT_LASER_OFFSET
        func = tweening.IN_OUT_SINE
        half_scan_duration = scan_duration / 2.0
        self.world.tweener.create_tween(ls.position, "x", ls.position.x, change.x, half_scan_duration,
                                        tween_function=func, cb_end=self._half_scanned, cb_args=[ls, change])
        self.world.tweener.create_tween(ls.position, "y", ls.position.y, change.y, half_scan_duration,
                                        tween_function=func)

        # face sprite
        face_sprite = self.current_package.sprite_face
        face_sprite.position = Point2(self.current_package.position.x, self.current_package.position.y)
        face_sprite.position = ls.position + self.direction * 24

        rect = face_sprite._orig_image.get_rect()
        face_sprite.area = rect.copy()
        self.world.tweener.create_tween(self.current_package.sprite_face, "alpha", 0.0, 255.0, half_scan_duration,
                                        tween_function=func)
        if self.direction.x == 0:
            if self.direction.y < 0:
                # up
                self.world.tweener.create_tween(face_sprite.area, "h", 0, rect.h, half_scan_duration,
                                                tween_function=func, immediate=True)
                self.world.tweener.create_tween(face_sprite.area, "y", float(rect.h), -float(rect.h),
                                                half_scan_duration, tween_function=func, immediate=True)
                self.world.tweener.create_tween(face_sprite.position, "y", face_sprite.position.y + rect.h, -rect.h,
                                                half_scan_duration, tween_function=func, immediate=True)
            else:
                # down
                self.world.tweener.create_tween(face_sprite.area, "h", 0, rect.h, half_scan_duration,
                                                tween_function=func, immediate=True)
        elif self.direction.x > 0:
            # right
            self.world.tweener.create_tween(face_sprite.area, "w", 0, rect.h, half_scan_duration, tween_function=func,
                                            immediate=True)
        else:
            # left
            self.world.tweener.create_tween(face_sprite.area, "w", 0, rect.w, half_scan_duration, tween_function=func,
                                            immediate=True)
            self.world.tweener.create_tween(face_sprite.area, "x", float(rect.w), -float(rect.w), half_scan_duration,
                                            tween_function=func, immediate=True)
            self.world.tweener.create_tween(face_sprite.position, "x", face_sprite.position.x + rect.w, -rect.w,
                                            half_scan_duration, tween_function=func, immediate=True)
        self.world.spritesystem.add_sprite(face_sprite)
        self.sound_siren()

    def _half_scanned(self, tweener, o, a, s, c, d, f, p, cb, cba):
        ls, change = cba[0]
        func = tweening.IN_OUT_SINE
        self.world.tweener.create_tween(ls.position, "x", ls.position.x, -change.x, d, tween_function=func)
        self.world.tweener.create_tween(ls.position, "y", ls.position.y, -change.y, d, tween_function=func)
        self.world.tweener.create_tween(self.current_package.sprite_face, "alpha", 255.0, -255.0, d,
                                        tween_function=tweening.IN_SINE)
        self.sound_siren()

    def sound_siren(self):
        if settings.SCAN_SFX_ENABLED:
            self.world.resources.get_sound(settings.RES_SFX_SCAN).play()

    def _end_scan(self, *args):
        self.current_state = 0
        # push the package a bit away from port
        self.current_package.position -= self.direction * settings.PORT_LEAVE_DISTANCE
        self.current_package.released_from_scan()

        self.world.spritesystem.remove_sprite(self.laser_sprite)

        # make sure the sprite and the package share the position vector again
        self.current_package.sprite_face.position = self.current_package.position
        self.world.spritesystem.remove_sprite(self.current_package.sprite_face)

        img = self.world.resources.get_image(settings.RES_IMG_CHECKMARK)
        thread_level = self.current_package.package_type.thread_level
        if thread_level >= ThreadLevels.STEALTH:
            pass
        elif thread_level >= ThreadLevels.DETECTED:
            img = self.world.resources.get_image(settings.RES_IMG_CROSS)
            self.sound_alarm()
        elif thread_level >= ThreadLevels.UNSURE_DETECTED:
            img = self.world.resources.get_image(settings.RES_IMG_UNSURE)
        elif thread_level >= ThreadLevels.UNSURE_CLEAN:
            img = self.world.resources.get_image(settings.RES_IMG_UNSURE)
        elif thread_level >= ThreadLevels.CLEAN:
            pass

        # share the position
        scan_mark = self.world.animator.create_animation(img, self.current_package.position, anchor="bottomleft",
                                                         z_layer=settings.LAYER_PACKAGE_MARK)
        self.current_package.sprites.append(scan_mark)
        self.world.spritesystem.add_sprite(scan_mark)

    def remove_package_from_queue(self, package):
        try:
            self.queue.remove(package)
            if package == self.current_package:
                self.current_state = 0  # fetch
        except ValueError:
            pass

    def sound_alarm(self):
        anim = self.world.resources.get_animation(settings.RES_ANIM_ALARM_SOUND)
        pos = self.position - self.direction * settings.PORT_ALARM_DISTANCE
        layer = settings.LAYER_ALARM
        repeat = self.scan_type.alarm_repeats
        duration = self.scan_type.alarm_duration / repeat
        spr = self.world.animator.create_animation(anim, pos, duration=duration, end_cb=self._end_alarm_anim,
                                                   z_layer=layer, end_cb_args=repeat)
        spr.rotation = self.angle - 45 - 90
        spr.start()
        self.world.spritesystem.add_sprite(spr)
        if settings.SIREN_SFX_ENABLED:
            self.world.resources.get_sound(settings.RES_SFX_SCANNER_WARN).play()

    def _end_alarm_anim(self, anim):
        repeat = anim.end_cb_args - 1
        if repeat > 0:
            anim.end_cb_args = repeat
            anim.start()
        else:
            self.world.spritesystem.remove_sprite(anim)
