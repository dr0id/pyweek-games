# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek20-2015-08
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function
import random

from pyknicpygame.pyknic.statemachines import SimpleStateMachine
import settings

random.seed(settings.RANDOM_SEED)

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module


class State(object):
    @staticmethod
    def enter(owner):
        pass

    @staticmethod
    def exit(owner):
        pass

    @staticmethod
    def update(owner, dt):
        pass


class StatePortScan(State):
    @staticmethod
    def enter(owner):
        owner.selected_port.queue_package(owner)


class StateGoToPort(State):
    @staticmethod
    def enter(owner):
        if owner.selected_port is None:
            # find nearest port
            nearest = owner.world.ports[0]
            nearest_dist = owner.position.get_distance_sq(nearest.position)
            for port in owner.world.ports:
                distance_sq = owner.position.get_distance_sq(port.position)
                if distance_sq < nearest_dist:
                    nearest = port
                    nearest_dist = distance_sq
            owner.selected_port = nearest
            owner.direction = (nearest.position - owner.position).normalized

    @staticmethod
    def update(owner, dt):
        owner.position += owner.speed * owner.direction * dt
        if owner.position.get_distance_sq(owner.selected_port.position) < settings.PACKAGE_TO_PORT_DIST_SQ:
            owner.switch_state(StatePortScan)


class StateMoveToCore(State):
    @staticmethod
    def enter(owner):
        owner.selected_core = random.choice(owner.world.cores)
        owner.direction = owner.selected_core.position - owner.position
        owner.direction.normalize()

    @staticmethod
    def update(owner, dt):
        owner.position += owner.direction * owner.speed * dt
        if owner.position.get_distance_sq(
                owner.selected_core.position) < settings.PACKAGE_TO_CORE_DIST_SQ * owner.selected_core.size:
            owner.selected_core.receive_package(owner)


class Package(SimpleStateMachine):
    def __init__(self, world, position, package_type):
        SimpleStateMachine.__init__(self)  # gives us 'current_state' and 'switch_state(new_state)'
        self.package_type = package_type
        self.world = world
        self.position = position
        self.type = type
        self.selected_port = None

        self.size = 1.0
        if settings.FEATURE_SIZED_PACKAGES:
            self.size = package_type.size + (random.random() - 0.5)

        self.speed = self.package_type.speed * 1.0 / self.size

        self.selected_core = None

        self.switch_state(StateGoToPort)
        img_face = self.world.resources.get_image(package_type.image_face)
        self.sprite_face = self.world.animator.create_animation(img_face, position, z_layer=settings.LAYER_PACKAGE_FACE)

        img_envelop = self.world.resources.get_image(package_type.image)
        self.sprite = self.world.animator.create_animation(img_envelop, position, z_layer=settings.LAYER_PACKAGE)
        self.sprite.zoom = self.size
        self.sprites = [self.sprite]
        self.world.spritesystem.add_sprites(self.sprites)
        self.sprites.append(self.sprite_face)

    def update(self, dt):
        self.current_state.update(self, dt)

    def is_inside(self):
        if self.world.inside_rect.collidepoint(self.position.as_tuple()):
            # inside
            return True
        else:
            # outside
            return False

    def released_from_scan(self):
        self.switch_state(StateMoveToCore)
        self.update_layers()

    def calculate_damage(self, damage):
        # can't shoot packages while beeing scanned
        if self.current_state == StatePortScan:
            return False

        self.size -= damage
        if self.size <= settings.PACKAGE_MINIMAL_SIZE:
            # dead
            return True

        # # not sure if either all sprites should shrink
        # for spr in self.sprites:
        #     spr.zoom = self.size
        self.sprite.zoom = self.size
        self.update_layers()

    def update_layers(self):
        self.sprites.sort(key=lambda x: x.z_layer)
        for idx, spr in enumerate(self.sprites):
            spr.z_layer = settings.LAYER_PACKAGE_AFTER_PORT_SCAN + (10.0 - self.size) / 10.0 + idx
        # self.speed = self.package_type.speed * 1.0 / self.size
