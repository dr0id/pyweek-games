# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek20-2015-08
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function
import settings

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module


class BulletType(object):
    def __init__(self):
        self.speed = 500.0
        self.image = settings.RES_IMG_BULLET1  # "data/bullet.png"
        self.type = settings.BULLET_TYPE
        self.sound = settings.RES_SFX_FIRE1
        self.damage = 1.0


class BulletType2(object):
    def __init__(self):
        self.speed = 1000.0
        self.image = settings.RES_IMG_BULLET2  # "data/bullet.png"
        self.type = settings.BULLET_TYPE
        self.sound = settings.RES_SFX_FIRE2
        self.damage = 0.5


class TurretType(object):
    def __init__(self):
        # save here the weapons properties
        # image
        # where bullet leave
        # type: laser, rocket, etc
        # projectile max lifetime (if not dead before)
        # reload speed
        # etc
        self.image_turret = settings.RES_IMG_TURRET1  # "data/turret.png"
        self.image_base = settings.RES_IMG_TURRET1_BASE  # "data/turretbase.png"
        self.turret_length = 70
        self.bullet_type = BulletType()
        self.bullet_type_secondary = BulletType2()
        self.bullet_max_fired = 2

class ThreadLevels:
    CLEAN = 0
    UNSURE_CLEAN = 10
    UNSURE_DETECTED = 20
    DETECTED = 30
    STEALTH = 40

class PackageTypeClean(object):
    def __init__(self, size=1.0):
        self.speed = 40
        self.image = settings.RES_IMG_PACKAGE
        self.image_face = settings.RES_IMG_PACKAGE_FACE_FRIENDLY
        self.size = size
        self.thread_level = ThreadLevels.CLEAN


class PackageTypeBaddyDetected(PackageTypeClean):
    def __init__(self, size=1.0):
        PackageTypeClean.__init__(self, size)
        self.image_face = settings.RES_IMG_PACKAGE_FACE_BADDY
        self.thread_level = ThreadLevels.DETECTED

class PackageTypeCleanUnsure(PackageTypeClean):
    def __init__(self, size=1.0):
        PackageTypeClean.__init__(self, size)
        self.thread_level = ThreadLevels.UNSURE_CLEAN


class PackageTypeBaddyUnsure(PackageTypeClean):
    def __init__(self, size=1.0):
        PackageTypeClean.__init__(self, size)
        self.image_face = settings.RES_IMG_PACKAGE_FACE_BADDY
        self.thread_level = ThreadLevels.UNSURE_DETECTED


class PackageTypeBaddyUndetected(PackageTypeClean):
    def __init__(self, size=1.0):
        PackageTypeClean.__init__(self, size)
        self.image_face = settings.RES_IMG_PACKAGE_FACE_BADDY
        self.thread_level = ThreadLevels.STEALTH


class ScanType(object):
    def __init__(self, duration=2.5):
        self.scan_duration = duration  # seconds
        self.scan_level = 50
        self.alarm_duration = 1.0
        self.alarm_repeats = 2
