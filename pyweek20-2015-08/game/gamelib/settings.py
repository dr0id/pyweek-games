# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek20-2015-08
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function
import random

__version_number__ = (0, 0, 0, 0)
__version__ = ".".join([str(num) for num in __version_number__])

__author__ = 'dr0iddr0id {at} gmail [dot] com'
__copyright__ = "DR0ID @ 2014"
__credits__ = ["DR0ID", "Gummbum"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

import os

import pygame

DOMAIN_NAME = "pyweek20-DataDataData"  # used for translation files

###############################################################################
#
# for tuning
#
###############################################################################

# touch game/nomusic.txt to disable music locally without distributing the setting
PLAY_MUSIC = not os.access('nomusic.txt', os.F_OK)

#                           1    left mouse button
#                           2    middle mouse button
#                           3    right mouse button
#                           4    scroll wheel up
#                           5    scroll wheel down
MOUSE_BUTTON_TURRET1_FIRE = 1
MOUSE_BUTTON_TURRET2_FIRE = 3

MOD_KEY_SECONDARY_FIRE_MODE = pygame.KMOD_CTRL
KEY_SECONDARY_FIRE_MODE = pygame.K_a

KEY_TURRET1_FIRE = pygame.K_k
KEY_TURRET2_FIRE = pygame.K_l

KEY_MOVE_NEXT = (pygame.K_RETURN, pygame.K_SPACE)
KEY_PAUSE = pygame.K_ESCAPE

KEY_TOGGLE_SIREN_SFX_ENABLED = pygame.K_F2
KEY_TOGGLE_SCAN_SFX_ENABLED = pygame.K_F3
KEY_TOGGLE_POWER_SCAN_SFX_ENABLED = pygame.K_F4
KEY_TOGGLE_PACKAGE_SUCK_SFX_ENABLED = pygame.K_F5

KEY_INCREASE_MUSIC_VOLUME = pygame.K_1
KEY_DECREASE_MUSIC_VOLUME = pygame.K_2
KEY_INCREASE_SFX_VOLUME = pygame.K_3
KEY_DECREASE_SFX_VOLUME = pygame.K_4

KEY_CHEAT_EXIT = pygame.K_F10
KEY_CHEAT_WIN = pygame.K_F12
KEY_CHEAT_LOOSE = pygame.K_F11
KEY_TOGGLE_DEBUG_RENDER = pygame.K_F9

# Smaller screen may give better performance; but the world isn't scalable
# and having a wide view helps in choosing your paths.
SCREEN_WIDTH = 1024
SCREEN_HEIGHT = 768

SIREN_SFX_ENABLED = True
POWER_SCAN_SFX_ENABLED = True
SCAN_SFX_ENABLED = True
PACKAGE_SUCK_SFX_ENABLED = True

URL_SHOWN_COUNT = 200
FEATURE_ADD_URLS = True

###############################################################################
#
# end tuning
#
###############################################################################

SCREEN_SIZE = (SCREEN_WIDTH, SCREEN_HEIGHT)
CAPTION = "Data Blasters"
FLAGS = pygame.DOUBLEBUF
BIT_DEPTH = 32

try:
    import numpy
    HILIGHT_FONT_COLOR = dict(color='gold', scolor='black', shadow=(1, 1), owidth=1, gcolor='tomato')
    LOLIGHT_FONT_COLOR = dict(color='tomato', scolor='black', shadow=(1, 1), owidth=1, gcolor='gold')
except:
    HILIGHT_FONT_COLOR = dict(color='gold', scolor='black', shadow=(1, 1), owidth=1)
    LOLIGHT_FONT_COLOR = dict(color='tomato', scolor='black', shadow=(1, 1), owidth=1)

MIXER_FREQUENCY = 0
MIXER_BUFFER_SIZE = 128

MUSIC_VOLUME = 0.3
VOLUME_CHANGE = 0.005

SIM_TIME_STEP = 0.02  # 50 fps

FEATURE_SIZED_PACKAGES = True
FEATURE_SCAN_DURATION_DEPENDS_ON_PACKAGE_SIZE = True

LEVEL_END_MESSAGE_DURATION = 4.0
SCORE_SWEEP_IN_DURATION = 0.5
LEVEL_START_MESSAGE_DURATION = 5.0
LEVEL_MESSAGES_DISTANCE = 50
SCORE_LINE_DISTANCE = 30
POWER_SCAN_DURATION = 0.5
PACKAGE_SUCK_IN_DURATION = 0.2

# increase this better rotation caching; but clearing the cache makes the game hiccup;
# lower it if the game periodically freezes for a split second for no apparent reason
# SPRITE_MAX_CACHE_IMAGE_COUNT = 10000
SPRITE_CACHE_MAX_MEMORY = 1.5 * 2 ** 30
SPRITE_CACHE_DEFAULT_EXPIRATION = 15.0
SPRITE_CACHE_AT_MOST = 50
SPRITE_CACHE_AUTO_TUNE_AGING = False
SPRITE_CACHE_TUNE_MIN_PERCENT = 94.0
SPRITE_CACHE_TUNE_MAX_PERCENT = 95.0
SPRITE_CACHE_TUNE_STEP = 0.01


# type definitions, use bit shift to make sure, that one bit position is used for each type
# one could define groups of types by using the bitwise and, e.g. group = t1 & t2
NO_TYPE = 0
BULLET_TYPE = 1 << 0
TYPE_PACKAGE = 1 << 1
BADDY2_TYPE = 1 << 2
BADDY3_TYPE = 1 << 3
PORT_TYPE = 1 << 4
TYPE3 = 1 << 5
TYPE4 = 1 << 6

RANDOM_SEED = 908709870987  # set to None for real randomness, this seed will produce the same result over and over
random.seed(RANDOM_SEED)

PACKAGE_BULLET_COLLISION_RADIUS_SQ = 30 * 30
PACKAGE_MINIMAL_SIZE = 0.2

PACKAGE_TO_CORE_DIST_SQ = 40 * 40
PACKAGE_TO_PORT_DIST_SQ = 100 * 100
PORT_MOVE_P_IN_QUEUE_DURATION = 1.0
PORT_QUEUE_SPACE = 2
PORT_QUEUE_OFFSET = 64 + PORT_QUEUE_SPACE
PORT_MOVE_QUEUE_DURATION = 0.1
PORT_LEAVE_DISTANCE = 30
PORT_ALARM_DISTANCE = 80
PORT_LASER_WIDTH_FACTOR = 0.6
PORT_LASER_OFFSET = 30
PORT_LASER_DISTANCE = 38

DEBUG_RENDER = False

LAYER_INTERNET = -1
LAYER_BOTTOM = 0
LAYER_CORE = 10
LAYER_TURRET_BASE = 100
LAYER_WALL = 500
LAYER_PORT = 600
LAYER_PACKAGE = 615
LAYER_PACKAGE_IN_PORT_QUEUE = 616
LAYER_PACKAGE_IN_PORT_SCAN = 617
LAYER_PACKAGE_AFTER_PORT_SCAN = 617
LAYER_PACKAGE_FACE = 620
LAYER_PACKAGE_MARK = 621
LAYER_LASER = 700
LAYER_BULLET = 705
LAYER_CURSOR = 750
LAYER_TURRET = 810
LAYER_ALARM = 1000
LAYER_OVERLAY_TEXT = 10000

# images 0 - 999
RES_IMG_BULLET1 = 1
RES_IMG_CURSOR = 2
RES_IMG_PORT = 3
RES_IMG_WALL = 4
RES_IMG_INSIDE_AREA = 5
RES_IMG_TURRET1 = 6
RES_IMG_TURRET1_BASE = 7
RES_IMG_BULLET2 = 8
RES_IMG_CORE1 = 9
RES_IMG_CORE2 = 10
RES_IMG_PACKAGE = 11
RES_IMG_LASER = 12
RES_IMG_PACKAGE_FACE_BADDY = 13
RES_IMG_PACKAGE_FACE_FRIENDLY = 14
RES_IMG_CHECKMARK = 15
RES_IMG_CROSS = 16
RES_IMG_UNSURE = 17

# sound 1000 - 1999
RES_SFX_FIRE1 = 1000
RES_SFX_FIRE2 = 1001
RES_SFX_SCANNER_WARN = 1002
RES_SFX_SCAN = 1003
RES_SFX_POWERSCAN = 1004
RES_SFX_SUCK_IN_PACKAGE = 1005

# music 2000 - 2999
RES_MUS_1 = 2000
RES_MUS_2 = 2001
RES_MUS_3 = 2002
RES_MUS_4 = 2003
RES_MUS_5 = 2004

# fonts 3000 - 3999


# animations 4000 - 4999
RES_ANIM_ALARM_SOUND = 4000

key_to_text = {
    pygame.K_BACKSPACE: ("\\b", "backspace"),
    pygame.K_TAB: ("\\t", "tab"),
    pygame.K_CLEAR: ("", "clear"),
    pygame.K_RETURN: ("\\r", "return"),
    pygame.K_PAUSE: ("", "pause"),
    pygame.K_ESCAPE: ("^[", "escape"),
    pygame.K_SPACE: ("", "space"),
    pygame.K_EXCLAIM: ("!", "exclaim"),
    pygame.K_QUOTEDBL: ("\"", "quotedbl"),
    pygame.K_HASH: ("#", "hash"),
    pygame.K_DOLLAR: ("$", "dollar"),
    pygame.K_AMPERSAND: ("&", "ampersand"),
    pygame.K_QUOTE: ("", "quote"),
    pygame.K_LEFTPAREN: ("(", "left parenthesis"),
    pygame.K_RIGHTPAREN: ("),", "right parenthesis"),
    pygame.K_ASTERISK: ("*", "asterisk"),
    pygame.K_PLUS: ("+", "plus sign"),
    pygame.K_COMMA: (",", "comma"),
    pygame.K_MINUS: ("-", "minus sign"),
    pygame.K_PERIOD: (".", "period"),
    pygame.K_SLASH: ("/", "forward slash"),
    pygame.K_0: ("0", "0"),
    pygame.K_1: ("1", "1"),
    pygame.K_2: ("2", "2"),
    pygame.K_3: ("3", "3"),
    pygame.K_4: ("4", "4"),
    pygame.K_5: ("5", "5"),
    pygame.K_6: ("6", "6"),
    pygame.K_7: ("7", "7"),
    pygame.K_8: ("8", "8"),
    pygame.K_9: ("9", "9"),
    pygame.K_COLON: (":", "colon"),
    pygame.K_SEMICOLON: (";", "semicolon"),
    pygame.K_LESS: ("<", "less-than sign"),
    pygame.K_EQUALS: ("=", "equals sign"),
    pygame.K_GREATER: (">", "greater-than sign"),
    pygame.K_QUESTION: ("?", "question mark"),
    pygame.K_AT: ("@", "at"),
    pygame.K_LEFTBRACKET: ("[", "left bracket"),
    pygame.K_BACKSLASH: ("\\", "backslash"),
    pygame.K_RIGHTBRACKET: ("]", "right bracket"),
    pygame.K_CARET: ("^", "caret"),
    pygame.K_UNDERSCORE: ("_", "underscore"),
    pygame.K_BACKQUOTE: ("`", "grave"),
    pygame.K_a: ("a", "a"),
    pygame.K_b: ("b", "b"),
    pygame.K_c: ("c", "c"),
    pygame.K_d: ("d", "d"),
    pygame.K_e: ("e", "e"),
    pygame.K_f: ("f", "f"),
    pygame.K_g: ("g", "g"),
    pygame.K_h: ("h", "h"),
    pygame.K_i: ("i", "i"),
    pygame.K_j: ("j", "j"),
    pygame.K_k: ("k", "k"),
    pygame.K_l: ("l", "l"),
    pygame.K_m: ("m", "m"),
    pygame.K_n: ("n", "n"),
    pygame.K_o: ("o", "o"),
    pygame.K_p: ("p", "p"),
    pygame.K_q: ("q", "q"),
    pygame.K_r: ("r", "r"),
    pygame.K_s: ("s", "s"),
    pygame.K_t: ("t", "t"),
    pygame.K_u: ("u", "u"),
    pygame.K_v: ("v", "v"),
    pygame.K_w: ("w", "w"),
    pygame.K_x: ("x", "x"),
    pygame.K_y: ("y", "y"),
    pygame.K_z: ("z", "z"),
    pygame.K_DELETE: ("", "delete"),
    pygame.K_KP0: ("", "keypad 0"),
    pygame.K_KP1: ("", "keypad 1"),
    pygame.K_KP2: ("", "keypad 2"),
    pygame.K_KP3: ("", "keypad 3"),
    pygame.K_KP4: ("", "keypad 4"),
    pygame.K_KP5: ("", "keypad 5"),
    pygame.K_KP6: ("", "keypad 6"),
    pygame.K_KP7: ("", "keypad 7"),
    pygame.K_KP8: ("", "keypad 8"),
    pygame.K_KP9: ("", "keypad 9"),
    pygame.K_KP_PERIOD: (".", "keypad period"),
    pygame.K_KP_DIVIDE: ("/", "keypad divide"),
    pygame.K_KP_MULTIPLY: ("*", "keypad multiply"),
    pygame.K_KP_MINUS: ("-", "keypad minus"),
    pygame.K_KP_PLUS: ("+", "keypad plus"),
    pygame.K_KP_ENTER: ("\\r", "keypad enter"),
    pygame.K_KP_EQUALS: ("=", "keypad equals"),
    pygame.K_UP: ("", "up arrow"),
    pygame.K_DOWN: ("", "down arrow"),
    pygame.K_RIGHT: ("", "right arrow"),
    pygame.K_LEFT: ("", "left arrow"),
    pygame.K_INSERT: ("", "insert"),
    pygame.K_HOME: ("", "home"),
    pygame.K_END: ("", "end"),
    pygame.K_PAGEUP: ("", "page up"),
    pygame.K_PAGEDOWN: ("", "page down"),
    pygame.K_F1: ("", "F1"),
    pygame.K_F2: ("", "F2"),
    pygame.K_F3: ("", "F3"),
    pygame.K_F4: ("", "F4"),
    pygame.K_F5: ("", "F5"),
    pygame.K_F6: ("", "F6"),
    pygame.K_F7: ("", "F7"),
    pygame.K_F8: ("", "F8"),
    pygame.K_F9: ("", "F9"),
    pygame.K_F10: ("", "F10"),
    pygame.K_F11: ("", "F11"),
    pygame.K_F12: ("", "F12"),
    pygame.K_F13: ("", "F13"),
    pygame.K_F14: ("", "F14"),
    pygame.K_F15: ("", "F15"),
    pygame.K_NUMLOCK: ("", "numlock"),
    pygame.K_CAPSLOCK: ("", "capslock"),
    pygame.K_SCROLLOCK: ("", "scrollock"),
    pygame.K_RSHIFT: ("", "right shift"),
    pygame.K_LSHIFT: ("", "left shift"),
    pygame.K_RCTRL: ("", "right ctrl"),
    pygame.K_LCTRL: ("", "left ctrl"),
    pygame.K_RALT: ("", "right alt"),
    pygame.K_LALT: ("", "left alt"),
    pygame.K_RMETA: ("", "right meta"),
    pygame.K_LMETA: ("", "left meta"),
    pygame.K_LSUPER: ("", "left windows key"),
    pygame.K_RSUPER: ("", "right windows key"),
    pygame.K_MODE: ("", "mode shift"),
    pygame.K_HELP: ("", "help"),
    pygame.K_PRINT: ("", "print screen"),
    pygame.K_SYSREQ: ("", "sysrq"),
    pygame.K_BREAK: ("", "break"),
    pygame.K_MENU: ("", "menu"),
    pygame.K_POWER: ("", "power"),
    pygame.K_EURO: ("", "euro"),
}

mod_key_to_text = {
    pygame.KMOD_NONE: "",
    pygame.KMOD_LSHIFT: "left shift",
    pygame.KMOD_RSHIFT: "right shift",
    pygame.KMOD_SHIFT: "shift",
    pygame.KMOD_CAPS: "capslock",
    pygame.KMOD_LCTRL: "left ctrl",
    pygame.KMOD_RCTRL: "right ctrl",
    pygame.KMOD_CTRL: "ctrl",
    pygame.KMOD_LALT: "left alt",
    pygame.KMOD_RALT: "right alt",
    pygame.KMOD_ALT: "alt",
    pygame.KMOD_LMETA: "left meta",
    pygame.KMOD_RMETA: "right meta",
    pygame.KMOD_META: "meta",
    pygame.KMOD_NUM: "num",
    pygame.KMOD_MODE: "mode",
}

list_of_urls = [
    "wordpress.org/",
    "get.adobe.com/flashplayer",
    "www.google.com/",
    "www.miibeian.gov.cn/",
    "get.adobe.com/reader/",
    "get.adobe.com/flashpla...",
    "www.facebook.com/",
    "validator.w3.org/check...",
    "www.baidu.com/",
    "www.facebook.com/share...",
    "jigsaw.w3.org/css-vali...",
    "twitter.com/",
    "statcounter.com/",
    "www.wordpress.org/",
    "www.blogger.com/",
    "www.joomla.org/",
    "vimeo.com/",
    "www.yahoo.com/",
    "wordpress.com/signup/?...",
    "www.youtube.com/",
    "wordpress.com/?ref=footer",
    "www.adobe.com/products...",
    "www.networkadvertising...",
    "www.networkadvertising...",
    "www.phpbb.com/",
    "www.twitter.com/",
    "en.wordpress.com/about...",
    "www.phoca.cz/",
    "www.miitbeian.gov.cn/",
    "www.godaddy.com/hostin...",
    "www.google.com/policie...",
    "www.weebly.com/",
    "wordpress.com/",
    "www.statcounter.com/",
    "www.histats.com/",
    "www.google.com/privacy...",
    "www.e-recht24.de/muste...",
    "wordpress.org/support/",
    "facebook.com/",
    "tools.google.com/dlpag...",
    "www.homestead.com/",
    "codex.wordpress.org/",
    "www.addthis.com/bookma...",
    "www.phoca.cz/phocagallery",
    "cpanel.net/",
    "www.weebly.com/?footer",
    "www.cpanel.net/",
    "www.discuz.net/",
    "planet.wordpress.org/",
    "www.dedecms.com/",
    "wordpress.org/themes/",
    "www.addtoany.com/share...",
    "wordpress.org/extend/t...",
    "www.comsenz.com/",
    "wordpress.org/plugins/",
    "wordpress.org/extend/p...",
    "www.amazon.com/",
    "wordpress.org/news/",
    "google.com/",
    "docs.cpanel.net/twiki/...",
    "www.cpanel.net/docs/dn...",
    "www.linkedin.com/",
    "www.e-recht24.de/",
    "twitter.com/home",
    "ja.wordpress.org/",
    "www.php.net/",
    "wordpress.org/ideas/",
    "wordpress.org/extend/i...",
    "www.cloudflare.com/ema...",
    "www.bing.com/",
    "www.oaic.gov.au/",
    "www.adobe.com/",
    "www.privacy.gov.au/",
    "www.bluehost.com/",
    "gmpg.org/xfn/",
    "sphinn.com/index.php?c...",
    "lifestream.aol.com/",
    "www.gnu.org/licenses/g...",
    "www.163.com/",
    "www.flickr.com/",
    "sites.google.com/",
    "whoisprivacyprotect.co...",
    "www.mysql.com/",
    "www.mozilla.org/firefox/",
    "www.microsoft.com/",
    "wordpress.com/signup/?...",
    "de-de.facebook.com/pol...",
    "www.elegantthemes.com/",
    "www.bluehost.com/cgi/i...",
    "www.bluehost.com/cgi/help",
    "www.bluehost.com/cgi/t...",
    "www.bluehost.com/cgi/i...",
    "www.bluehost.com/cgi-b...",
    "drupal.org/",
    "www.pinterest.com/",
    "www.desdev.cn/",
    "opensource.org/license...",
    "www.wikipedia.org/",
    "www.parallels.com/prod...",
    "statcounter.com/tumblr/",
    "www.dagondesign.com/",
    "www.redcross.org/",
    "www.sina.com.cn/",
    "www.paypal.com/",
    "top100.rambler.ru/top100/",
    "www.cnn.com/",
    "jigsaw.w3.org/css-vali...",
    "www.nytimes.com/",
    "www.domainmarket.com/",
    "www.apple.com/",
    "www.deliciousdays.com/...",
    "netscape.aol.com/",
    "smallbusiness.yahoo.co...",
    "www.deliciousdays.com/...",
    "www.facebook.com/home.php",
    "hibu.com/",
    "windows.microsoft.com/...",
    "www.chronoengine.com/",
    "www.ebay.com/",
    "disqus.com/",
    "www.sohu.com/",
    "www.paginegialle.it/",
    "www.yahoo.co.jp/",
    "www.yellowbook.com/pri...",
    "www.e-recht24.de/artik...",
    "www.parallels.com/prod...",
    "www.parallels.com/prod...",
    "www.parallels.com/prod...",
    "www.1und1.de/",
    "yahoo.com/",
    "www.facebook.com/busin...",
    "feedburner.google.com/",
    "twitter.com/privacy",
    "www.opera.com/",
    "www.simplemachines.org/",
    "1und1.de/",
    "www.shinystat.com/it",
    "delicious.com/post",
    "www.qq.com/",
    "www.omniture.com/en/",
    "www.omniture.com/",
    "whoisprivacyprotect.co...",
    "maps.yahoo.com/",
    "www.shinystat.com/it/",
    "pinterest.com/",
    "connect.mail.ru/share",
    "www.jevents.net/",
    "www.apple.com/safari/",
    "digg.com/",
    "de.jimdo.com/",
    "del.icio.us/post",
    "www.vistaprint.com/web...",
    "es.wordpress.org/",
    "www.taobao.com/",
    "visualsite.paginegiall...",
    "www.aboutads.info/",
    "www.linkwithin.com/",
    "joomla.org/",
    "listings.homestead.com/",
    "www.mapquest.com/",
    "www.joomlatune.com/",
    "mail.google.com/mail/",
    "www.freecsstemplates.org/",
    "vinaora.com/",
    "www.stumbleupon.com/su...",
    "www.unicef.org/",
    "jalbum.net/",
    "www.123-reg.co.uk/ecom...",
    "www.123-reg.co.uk/?_$j...",
    "www.123-reg.co.uk/ssl-...",
    "www.123-reg.co.uk/doma...",
    "www.123-reg.co.uk/web-...",
    "www.123-reg.co.uk/emai...",
    "www.123-reg.co.uk/make...",
    "www.123-reg.co.uk/vps-...",
    "www.123-reg.co.uk/webs...",
    "www.123-reg.co.uk/inst...",
    "www.123-reg.co.uk/secu...",
    "www.123-reg.co.uk/web-...",
    "www.123-reg.co.uk/webs...",
    "www.123-reg.co.uk/secu...",
    "www.123-reg.co.uk/doma...",
    "www.123-reg.co.uk/serv...",
    "jalbum.net/en/",
    "disqus.com/?ref_noscript",
    "validator.w3.org/check",
    "maps.google.com/",
    "www.studiopress.com/",
    "www.apache.org/",
    "www.pagesjaunes.fr/",
    "www.linezing.com/",
    "www.google.com/intl/de...",
    "www.irs.gov/",
    "twitter.com/account/se...",
    "www.google.com/intl/de...",
    "www.skype.com/",
    "delicious.com/",
    "www.liveinternet.ru/click",
    "www.google.co.jp/",
    "youtube.com/",
    "www.myspace.com/",
    "wordpress.org/developm...",
    "www.w3.org/",
    "www.roytanck.com/",
    "www.example.com/",
    "www.adobe.com/products...",
    "www.addthis.com/bookma...",
    "www.prestashop.com/",
    "www.woothemes.com/",
    "www.google.com/chrome",
    "www.google.com/analytics/",
    "www.whitehouse.gov/",
    "www.apple.com/mac",
    "www.slideshare.net/",
    "apple.com/mac",
    "www.google.cn/",
    "www.cdc.gov/",
    "www.wordpress.com/",
    "www.parallels.com/plesk/",
    "www.weather.com/",
    "validator.w3.org/",
    "instagram.com/",
    "translate.google.com/",
    "www.parallels.com/",
    "www.gnu.org/copyleft/g...",
    "www.google.de/",
    "www.google.com/doublec...",
    "en.wikipedia.org/wiki/...",
    "www.msn.com/",
    "moodle.org/",
    "www.toplist.cz/",
    "www.freeprivacypolicy....",
    "cyberchimps.com/respon...",
    "www.mozilla.com/firefox/",
    "www.fox.ra.it/",
    "wordpress.org/plugins/...",
    "wordpress.org/extend/p...",
    "www.adobe.com/jp/produ...",
    "www.jiathis.com/share",
    "www.vistaprint.com/vp/...",
    "automattic.com/",
    "www.vistaprint.com/",
    "www.addthis.com/bookma...",
    "www.imdb.com/",
    "204.232.149.59/shutdow...",
    "issuu.com/",
    "www.mapy.cz/",
    "www.xml-sitemaps.com/",
    "www.usatoday.com/",
    "www.hostmonster.com/",
    "www.people.com.cn/",
    "cn.wordpress.org/",
    "www.xinhuanet.com/",
    "www.wordpress-fr.net/",
    "www.google.fr/",
    "www.ucoz.ru/",
    "www.rockettheme.com/",
    "www.widgetbox.com/",
    "www.lycos.com/",
    "www.e-recht24.de/artik...",
    "www.stumbleupon.com/",
    "gtranslate.net/",
    "www.ustream.tv/",
    "statcounter.com/free_h...",
    "www.apple.com/quicktim...",
    "jigsaw.w3.org/css-vali...",
    "www.adobe.com/products...",
    "www.bbc.co.uk/",
    "www.businessweek.com/",
    "imageshack.us/",
    "www.hotmail.com/",
    "www.mediawiki.org/wiki...",
    "www.shinystat.com/",
    "www.google.com/intl/de...",
    "extensions.joomla.org/",
    "www.tumblr.com/",
    "technorati.com/",
    "get.adobe.com/jp/reader/",
    "statcounter.com/free-h...",
    "www.mediawiki.org/",
    "discuz.qq.com/service/...",
    "www.allaboutcookies.org/",
    "www.reuters.com/",
    "www.mp3.tv/",
    "oxide.com/",
    "del.icio.us/",
    "www.aol.com/",
    "jquery.com/",
    "feedjit.com/",
    "www.fma.com/",
    "ru.wordpress.org/",
    "www.parallels.com/prod...",
    "www.explore.com/",
    "fed.com/",
    "www.dj.net/",
    "www.multimedia.com/",
    "www.i.net/",
    "www.miki.com/",
    "www.falcons.com/",
    "www.jackass.com/",
    "www.impact.com/",
    "www.ifeng.com/",
    "www.ibiza.com/",
    "www.monsters.com/",
    "www.party.com/",
    "www.efx.com/",
    "www.dgx.com/",
    "www.osk.com/",
    "www.jogging.com/",
    "www.palette.com/",
    "www.whynot.com/",
    "www.arabs.com/",
    "www.liquidmedia.com/",
    "www.domaindomain.com/",
    "www.nly.com/",
    "nameclub.com/",
    "www.gotv.com/",
    "www.rockcool.com/",
    "www.cooperators.com/",
    "jogging.com/",
    "www.bola.com/",
    "www.newyear.com/",
    "www.axj.net/",
    "www.squeeze.com/",
    "www.dotparis.com/",
    "www.officeworks.com/",
    "finaza.com/",
    "www.traveltv.com/",
    "www.kyr.net/",
    "www.ajmal.com/",
    "www.watanionline.com/",
    "twitter.com/#!/@fma",
    "www.houseofmedia.com/",
    "www.mailtv.com/",
    "www.unaddressed.com/",
    "www.authorize.net/",
    "www.hp.com/",
    "www.nameclub.com/",
    "www.oxide.com/",
    "mp3.tv/",
    "go.microsoft.com/fwlin...",
    "www.mozilla.org/en-US/",
    "www.ibm.com/",
    "www.adobe.com/products...",
    "www.ted.com/",
    "area51.phpbb.com/",
    "www.artisteer.com/?p=j...",
    "www.opencart.com/",
    "www.hostmonster.com/cg...",
    "www.hostmonster.com/cg...",
    "www.hostmonster.com/cg...",
    "www.hostmonster.com/cg...",
    "www.hostmonster.com/cg...",
    "www.hostmonster.com/cg...",
    "www.hostmonster.com/cg...",
    "www.pagelines.com/",
    "www.epa.gov/",
    "www.telnic.org/",
    "www.google.com.hk/",
    "www.networksolutions.com/",
    "forum.bytesforall.com/",
    "forum.joomla.org/",
    "www.gimp.org/",
    "lazaworx.com/",
    "www.theguardian.com/",
    "china.alibaba.com/",
    "www.ipage.com/ipage/in...",
    "www.ipage.com/",
    "www.statcounter.com/fr...",
    "www.fda.gov/",
    "creativecommons.org/li...",
    "support.widgetbox.com/",
    "online.wsj.com/",
    "www.sixapart.jp/movabl...",
    "www.gmail.com/",
    "coppermine-gallery.net/",
    "www.aboutcookies.org/",
    "www.bigmir.net/",
    "www.gnu.org/licenses/g...",
    "www.constantcontact.co...",
    "www.parallels.com/prod...",
    "linkedin.com/",
    "www.simplemachines.org...",
    "bbs.dedecms.com/",
    "www.phoca.cz/phocadown...",
    "creativecommons.org/li...",
    "gallery.sourceforge.net/",
    "plus.google.com/",
    "www.e-recht24.de/impre...",
    "www.oracle.com/technet...",
    "www.ssa.gov/",
    "www.washingtonpost.com/",
    "wowslider.com/",
    "www.nasa.gov/",
    "en.wikipedia.org/wiki/...",
    "marketing.hibu.co.uk/p...",
    "twitter.com/onecom",
    "www.mozilla.org/",
    "www.loc.gov/index.html",
    "www.facebook.com/Onecom",
    "statcounter.com/joomla/",
    "www.one.com/sv/?utm_so...",
    "webstats.motigo.com/",
    "get.adobe.com/de/reader/",
    "www.hao123.com/",
    "www.ubuntu.com/",
    "www.dmoz.org/",
    "www.biblegateway.com/",
    "reviews.justhost.com/",
    "www.gov.cn/",
    "www.parallels.com/prod...",
    "www.parallels.com/virt...",
    "www.parallels.com/auto...",
    "www.openoffice.org/",
    "en.gravatar.com/",
    "www.usa.gov/",
    "themeid.com/responsive...",
    "www.senate.gov/",
    "www.etsy.com/",
    "www.kunena.org/",
    "www.oscommerce.com/",
    "www.house.gov/",
    "creativecommons.org/li...",
    "diythemes.com/thesis/",
    "aquoid.com/news/themes...",
    "search.yahoo.com/",
    "www.gstatic.com/domain...",
    "www.cancer.org/",
    "www.youku.com/",
    "www.guardian.co.uk/",
    "www.ask.com/",
    "cargocollective.com/",
    "www.siteground.com/",
    "www.one.com/pt/?utm_so...",
    "www.simplemachines.org...",
    "www.simplemachines.org...",
    "www.cisco.com/",
    "www.1and1.com/",
    "creativecommons.org/li...",
    "www.loc.gov/",
    "earth.google.com/",
    "www.bloomberg.com/",
    "www.zen-cart.com/",
    "www.barnesandnoble.com/",
    "www.one.com/en/?utm_so...",
    "www.tucows.com/",
    "www.godaddy.com/",
    "bufferapp.com/add",
    "www.one.com/es/?utm_so...",
    "www.alipay.com/",
    "www.artisteer.com/?p=w...",
    "www.etracker.com/",
    "www.etracker.com/en.html",
    "www.live.com/",
    "www.accuweather.com/en...",
    "www.huffingtonpost.com/",
    "statcounter.com/free-w...",
    "www.oracle.com/us/sun/...",
    "www.technorati.com/",
    "www.sogou.com/",
    "www.apple.com/iphone/",
    "www.stumbleupon.com/su...",
    "reddit.com/submit",
    "www.one.com/no/?utm_so...",
    "www.joomlashine.com/",
    "www.one.com/nl/?utm_so...",
    "error.hostinger.eu/403...",
    "www.rainbowsoft.org/",
    "www.sba.gov/",
    "www.xt-commerce.com/",
    "www.webmd.com/",
    "www.bahn.de/p/view/ind...",
    "www.hupso.com/share/",
    "www.foxnews.com/",
    "commons.wikimedia.org/...",
    "creativecommons.org/",
    "www.netscape.com/",
    "statcounter.com/blogger/",
    "www.setup.ru/",
    "setup.ru/",
    "www.alexa.com/",
    "www.squarespace.com/",
    "visuallightbox.com/",
    "www.cmbchina.com/",
    "www.adobe.com/products...",
    "www.icbc.com.cn/",
    "wiki.simplemachines.or...",
    "wiki.simplemachines.or...",
    "example.com/",
    "www.findlaw.com/",
    "www.constantcontact.co...",
    "dictionary.reference.com/",
    "www.stumbleupon.com/su...",
    "www.providesupport.com/",
    "1and1.com/",
    "flickr.com/",
    "www.att.com/",
    "mashable.com/",
    "api.yandex.ru/maps/too...",
    "www.accuweather.com/ma...",
]
