# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek20-2015-08
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function
import heapq
from pyknicpygame.spritesystem import Sprite
import spritesheetlib

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 


class _Animation(Sprite):

    def __init__(self, animator, images, position, seconds_per_frame=1.0 / 20.0, end_cb=None, end_cb_args=None,
                 anchor=None, z_layer=None, parallax_factors=None, do_init=True, name='_NO_NAME_'):
        if isinstance(images[0], spritesheetlib.Sprite):
            images = [s.image for s in images]
        Sprite.__init__(self, images[0], position, anchor, z_layer, parallax_factors, do_init, name)
        self.images = images
        self.seconds_per_frame = seconds_per_frame
        self.end_cb = end_cb
        self.end_cb_args = end_cb_args
        self.animator = animator
        self.count = len(images)
        self.idx = 0
        self._playing = False

    def start(self):
        if self._playing:
            return

        self.animator.start_animation(self)

    def stop(self):
        self.animator.stop_animation(self)

_sort_func = lambda x: x[0]

class Animator(object):

    def __init__(self):
        self._current_time = 0.0
        self._timeline = []
        self._dirty = False

    def create_animation(self, images, position, fps=None, duration=None, end_cb=None, end_cb_args=None, anchor=None,
                         z_layer=None, parallax_factors=None, do_init=True, name='_NO_NAME_', start=False):
        if fps is not None and duration is not None:
            raise Exception("set either fps or duration to None otherwise they are conflicting")

        if fps is None and duration is None:
            fps = 20.0

        sec_per_frame = 1.0
        if duration is not None:
            sec_per_frame = duration / float(len(images) + 1)

        if fps is not None:
            sec_per_frame = 1.0 / fps

        # make sure it is a list of images
        try:
            img = images[0]
        except:
            images = [images]

        animation = _Animation(self, images, position, sec_per_frame, end_cb, end_cb_args, anchor, z_layer,
                               parallax_factors, do_init, name)
        if start:
            animation.start()
        return animation

    def start_animation(self, animation):
        if animation._playing:
            return
        if animation.count > 1:
            animation._playing = True
            animation.idx %= animation.count
            animation.set_image(animation.images[animation.idx])
            next_time_point = self._current_time + animation.seconds_per_frame
            self._timeline.append((next_time_point, animation))
            self._dirty = True

    def stop_animation(self, animation):
        animation._playing = False
        for a in self._timeline:
            if a == animation:
                self._timeline.remove(a)
                return

    def update(self, dt):
        # sort timeline
        if self._dirty is True:
            self._timeline.sort(key=_sort_func)
            self._dirty = False

        # update time
        self._current_time += dt
        while self._timeline:
            time_point, anim = self._timeline[0]
        # for time_point, anim in self._timeline:
            if time_point > self._current_time:
                break

            self._timeline.pop(0)

            anim.idx += 1
            if anim.idx == anim.count:
                anim.stop()
                if anim.end_cb:
                    anim.end_cb(anim)
            else:
                anim.idx %= anim.count
                anim.set_image(anim.images[anim.idx])
                next_time_point = time_point + anim.seconds_per_frame
                self._timeline.append((next_time_point, anim))
                self._dirty = True


