# from context import Context
from contextworld import World
import pyknicpygame
from pyknicpygame.pyknic.context import Context
# from levels import Level0, Level1, Score
import contextcredits

__author__ = 'DRummb0ID'


class ContextLevelRouter(Context):
    def __init__(self, level_order):
        self.level_order = level_order
        for level in level_order:
            level.check_waves(level.__class__.__name__)
        self.current_level_idx = 0
        self.current_level = None

    def enter(self):
        """Called when this context is pushed onto the stack."""
        self.current_level = self.level_order[self.current_level_idx]
        self.push(World(self.current_level))

    def exit(self):
        """Called when this context is popped off the stack."""
        context = contextcredits.ContextCredits()
        pyknicpygame.pyknic.context.push(context)

    def suspend(self):
        """Called when another context is pushed on top of this one."""
        pass

    def resume(self):
        """Called when another context is popped off the top of this one."""
        if self.current_level.score.has_won:
            self.current_level_idx += 1
            if self.current_level_idx < len(self.level_order):
                self.current_level = self.level_order[self.current_level_idx]
                self.push(World(self.current_level))
        else:
            self.current_level = self.current_level.__class__(*self.current_level.args)
            self.push(World(self.current_level))

    def think(self, delta_time):
        """Called once per frame"""
        if self.current_level_idx == len(self.level_order):
            self.pop()

    def draw(self, screen, do_flip=False):
        """Refresh the screen"""
        pass
