from pyglet.window.key import *
from pyglet.gl import *
import pyglet.text

import lib.engine


class SingleLineCaret(pyglet.text.caret.Caret):

    def __init__(self, layout, limit = None, callback = None):
        self.limit = limit
        self.callback = callback
        super(SingleLineCaret, self).__init__(layout)

    def on_text(self, text):
        if ord(text) == 13 or ord(text) == 10 or text == ' ':
            if self.callback:
                self.callback( self._layout.document.text )
        else:
            if self.limit and text[0] not in self.limit:
                return
            super(SingleLineCaret, self).on_text(text)
            
class TextEntry(object):
    label_margin = 5
    label_height = 30
    opacity = 0.75
    def __init__(self, font = 'Qubix', limit = None, callback = None, label = " Text entry:"):
        # get font
        lib.engine.fonts.get(font, font.lower() + '.ttf')
        # init document
        self.doc = pyglet.text.document.UnformattedDocument()
        self.doc.set_style(0, 0, {'color': (0, 0, 0, 255), 'font_name':font, 'font_size':36})
        # init layout, all text inputs are centered
        width, height = lib.engine.width, 56
        self.layout = pyglet.text.layout.IncrementalTextLayout(self.doc, width, height, multiline = True)
        self.layout.x = (lib.engine.width - width) //  2
        self.layout.y = (lib.engine.height - height) // 2
        # init single line caret with callback on return or enter
        self.caret = SingleLineCaret(self.layout, limit = limit, callback = self.clear)
        self.caretcolor = (128, 128, 128)
        # init label
        lx = self.layout.x
        ly = self.layout.y + height + self.label_margin
        self.label = pyglet.text.Label(text=label,
                font_name='Times New Roman',
                font_size=20,
                x=lx, y=ly,
                halign='left')
        
        self.visible = False
        self.callback = callback
        
    def clear(self, text):
        self.visible = False
        self.doc.text = ''
        self.caret.position = 0
        lib.engine.pop_handlers()
        if self.callback and text.strip():
            self.callback(text)
        
    def show(self):
        self.visible = True
        self.doc.text = ''
        self.caret.position = 0
        lib.engine.push_handlers(self.caret)
    
    def draw(self):
        if self.visible:
            width, height = self.layout.width, self.layout.height
            glTranslatef(self.layout.x, self.layout.y, 0.0)
            glBegin(GL_QUADS)
            # Label BG
            glColor4f(0.0, 0.0, 0.0, self.opacity)
            glVertex3f(0.0, height, 0.0)
            glVertex3f(width, height, 0.0)
            glVertex3f(width, height + self.label_height, 0.0)
            glVertex3f(0.0, height + self.label_height, 0.0)
            # Entry BG
            glColor4f(1.0, 1.0, 1.0, self.opacity)
            glVertex3f(0.0, 0.0, 0.0)
            glVertex3f(width, 0.0, 0.0)
            glVertex3f(width, height, 0.0)
            glVertex3f(0.0, height, 0.0)
            glEnd()
            glBegin(GL_LINES)
            glColor4f(0.0, 0.0, 0.0, self.opacity)
            glVertex3f(0.0, 0.0, 0.0)
            glVertex3f(width, 0.0, 0.0)
            glEnd()
            glTranslatef(-self.layout.x, -self.layout.y, 0.0)
            self.layout.draw()
            self.label.draw()