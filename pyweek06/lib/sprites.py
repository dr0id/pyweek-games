import lib.engine
import pyglet.sprite, pyglet.graphics

class SpriteManager(object):

    def __init__(self):
        super(SpriteManager, self).__init__()
        
    def get(self, filename, x = 0, y = 0, **kwargs):
        img = lib.engine.images.get(filename)
        spr = pyglet.sprite.Sprite(img, x=x, y=y)
        for key, val in kwargs.items():
            if hasattr(spr, key) and key in 'rotation scale opacity visible'.split():
                setattr(spr, key, val)

        return spr