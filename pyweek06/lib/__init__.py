#---- requirements ----#
def _requires_pyglet_version(version):
    try:
        import pyglet
        req = [int(i) for i in version if i in '1234567890']
        have = [int(i) for i in pyglet.version if i in '1234567890']
        if tuple(req) > tuple(have):
            raise ImportError('Celldoku requires Pyglet %s or later' % version)
    except ImportError:
        raise ImportError('Celldoku requires Pyglet %s or later' % version)


_requires_pyglet_version('1.1')

def _get_mod(modulePath):
    return __import__(modulePath, globals(), locals(), ['*'])

def _get_attr(fullAttrName):
    """Retrieve a module attribute from a full dotted-package name."""
    # Parse out the path, module, and function
    lastDot = fullAttrName.rfind(u".")
    attrName = fullAttrName[lastDot + 1:]
    modPath = fullAttrName[:lastDot]
    aMod = _get_mod(modPath)
    aAttr = getattr(aMod, attrName)
    # Return a reference to the function itself,
    # not the results of the function.
    return aAttr
    
def _get_func(fullFuncName):
    aFunc = _get_attr(fullFuncName)
    assert callable(aFunc), "%s is not callable." % fullFuncName
    return aFunc

def _get_class(fullClassName, parentClass=None):
    """Load a module and retrieve a class (NOT an instance).
    
    If the parentClass is supplied, className must be of parentClass
    or a subclass of parentClass (or None is returned).
    """
    aClass = _get_func(fullClassName)
    
    # Assert that the class is a subclass of parentClass.
    if parentClass is not None:
        if not issubclass(aClass, parentClass):
            raise TypeError(u"%s is not a subclass of %s" %
                            (fullClassName, parentClass))
    
    # Return a reference to the class itself, not an instantiated object.
    return aClass
    
import lib, lib.engine, lib.progress
lib.engine = lib.engine.EngineClass(width = 480, height = 640, visible = False, caption = "Celldoku // Pyweek 6 - Dustin Lacewell and Chris Hopp")
lib.progress.load()
