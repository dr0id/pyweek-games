import pyglet.image
import pyglet.resource
import os
class ImageManager(object):
    __default_path = 'data/art'
    
    def __init__(self):
        super(ImageManager, self).__init__()
        self.__cache = {}
        pyglet.resource.path.append( self.__default_path )
        pyglet.resource.reindex()
        
    def get(self, filename):
        if filename in self.__cache:
            return self.__cache[ filename ]
        self.__cache[ filename ] = pyglet.resource.image( filename )
        
        return self.__cache[ filename ]