from pyglet.gl import *
import pyglet
from pyglet.image import ImageGrid
from pyglet.sprite import Sprite
import random
import lib.engine, lib.levels

class Flower (object):
    def __init__ (self, *args, **kwargs):
        super(Flower, self).__init__(None, *args,**kwargs)
        self.seq = lib.engine.images.get('flower-anim.png')
        self.seq = ImageGrid(self.seq, 1, 16)
        self.fps = 25.0
        self.scale = 1.0
        self.rotationstep = 0.0
        self.rotation = 0.0
        self.current_frame = 0
        self.update_animation(0.0)
 
    def update_animation (self, dt):
        self.image = Sprite(self.seq[self.current_frame])
        self.image.image.anchor_x = self.image.image.width / 2
        self.image.image.anchor_y = self.image.image.height / 2
        self.image.rotation = self.rotation
        self.rotation += dt * self.rotationstep
        self.image.scale = self.scale
        if self.current_frame < len(self.seq) - 1:
            self.current_frame += 1
            
        pyglet.clock.schedule_once(self.update_animation, (1000/self.fps)/1000.0)
 
    
 
    def reset(self):
        self.current_frame = 0
 
    def draw (self):
        self.image.draw()
      
class Field(object):
    ALIVE = (0.0, 0.0, 0.0, .95)
    TARGET = (1.0, 0.0, 0.0, 0.35 )
    POINT  = (0.10, 0.05, 0.05, .95)

    def __init__(self, pxsize = 384, thickness = 5.0, opacity = 0.25):
        self.pxsize = pxsize
        self.thickness = thickness
        self.opacity = opacity
        self.cellsize = None # CALCULATED
        # Position - AUTOMATIC    
        self.x = (lib.engine.width - self.pxsize) / 2
        self.y = ((lib.engine.height - 170) - self.pxsize) / 2
        # Level Attributes
        self.level_name = None
        self.level_size = None
        self.level_gens = None
        self.current_gen = None
        
        self.seed = lib.engine.sprites.get('seed.png')
        
        self.flower = Flower()
        self.showflowers = False
        # Floor display list
        self.__gen_floor_display()
        self__grid_display_id = None

        self.field = {}
        self.load_level( lib.levels.Level( 'blank' ) )
        
            
    def load_level(self, level, gen = 0):
        self.cellsize = self.pxsize / level.size
        self.seed.scale = self.cellsize / 128.0
        self.flower.scale =  self.seed.scale
        self.showflowers = False
        self.level_name = level.name
        self.level_size = level.size
        self.__gen_grid_display()
        self.current_gen = gen
        self.level_gens = level.generations
        self.load_generation()
        
    def save_level(self, name = None):
        self.commit_generation()
        if not name:
            name = self.level_name
        newlevel = lib.levels.Level(name, size = self.level_size, generations = self.level_gens)
        return newlevel

    def apply_cell(self, x, y):
        if not self.level_name:
            return False
        alive = 0
        # Get neighbor count
        for dx in range(-1, 2):
            for dy in range(-1, 2):
                try:
                    if self.field[(x +dx, y+dy)]:
                        alive += 1
                except KeyError:
                    pass
        try :
            if self.field[(x, y)]:
                alive -= 1
                if alive not in [2, 3]:
                    return False
                else: 
                    return True
        except KeyError:
            if alive == 3:
                return True
            else: return False
            
    def calc_next(self):
        newfield = {}
        for x in range(self.level_size):
            for y in range(self.level_size):
                if self.apply_cell(x, y):
                    newfield[ (x, y) ] = True
        return newfield
        
    def pick_cell(self, x, y):
        if not self.level_name:
            return None
        if x >= self.x and x <= self.x + self.pxsize - 1:
            if y  >= self.y and y <= self.y + self.pxsize - 1:
                x = x - self.x
                y =y - self.y
                cx = int(x / self.cellsize)
                cy = int(y / self.cellsize)
                return (cx, cy)
        return None
    
    def commit_generation(self):
        self.level_gens[ self.current_gen ] = self.field
            
    def load_generation(self, idx = None):
        if idx != None:
            self.field = dict(self.level_gens[ idx ])
            self.current_generation = idx
        else:
            self.field = dict(self.level_gens[ self.current_gen ])
    
    def next_generation(self):
        if len(self.level_gens) >= self.current_gen + 2:
            return self.current_gen + 1
        else:
            return None
    
    def load_next(self):
        self.current_gen += 1
        try:
            self.load_generation()
            return self.current_gen
        except IndexError:
            print "NO GENERATION"
            self.current_gen -= 1
            return None
        
    
    def edit_first(self):
        if self.level_name:
            self.commit_generation()
            self.current_gen = 0
            self.load_generation()
            
                    
    def edit_next(self, step = False):
        if self.level_name:
            # Create new
            if self.current_gen == len(self.level_gens) - 1:
                if step:
                    self.edit_new( field = self.calc_next() )
                else:
                    return
            else:
                if step:
                    self.level_gens = self.level_gens[:self.current_gen+1]
                    self.edit_new( field = self.calc_next())
                else:            
                    self.commit_generation()   
                    self.current_gen += 1
                    self.load_generation()
                
    def edit_previous(self):
        if self.level_name:
            self.commit_generation()
            self.current_gen -= 1
            self.current_gen = max(0, self.current_gen)
            self.load_generation()
                
    def edit_new(self, field = None):
        if self.level_name:
            self.commit_generation()
            if field:
                self.level_gens.append(field)
            else:
                self.level_gens.append( {} )
                
            self.current_gen += 1
            self.load_generation()
            
        
    # DISPLAY RUNS
    def __floor_display_run(self):
        size = self.pxsize
        glTranslatef(self.x, self.y, 0.0)
        glBegin(GL_QUADS)
        glColor4f(1.0, 1.0, 1.0, self.opacity)
        glVertex3f(0.0, 0.0, 0.0)
        glVertex3f(size, 0.0, 0.0)
        glVertex3f(size, size, 0.0)
        glVertex3f(0.0, size, 0.0)
        glEnd()
        glTranslatef(-self.x, -self.y, 0.0)
        
    def __gen_floor_display(self):
        self.__floor_display_id = glGenLists(1)
        glNewList(self.__floor_display_id, GL_COMPILE)
        self.__floor_display_run()
        glEndList()
        
    def __grid_display_run(self):
        glTranslatef(self.x, self.y, 0.0)
        glLineWidth( self.thickness )
        glBegin(GL_LINES)
        glColor3f(0.0, 0.0, 0.0)
        for n in range(self.level_size + 1):
            offset = n * self.cellsize
            hx = 0.0
            hx2 = self.pxsize
            vx = offset
            vx2 = offset
            hy = offset
            hy2 = offset
            vy = 0.0
            vy2 = self.pxsize
            glVertex3f(hx, hy, 0.0)
            glVertex3f(hx2, hy2, 0.0)
            glVertex3f(vx, vy, 0.0)
            glVertex3f(vx2, vy2, 0.0) 
        glEnd()
        glTranslatef(-self.x, -self.y, 0.0)
        
    def __gen_grid_display(self):
        self.__grid_display_id = glGenLists(1)
        glNewList(self.__grid_display_id, GL_COMPILE)
        self.__grid_display_run()
        glEndList()
        

        
    def draw_cells(self, drawtarget = False):
        glTranslatef(self.x, self.y, 0.0)
        for x in range(self.level_size):
            for y in range(self.level_size):
                pos = (x, y)
                state, target = None, None
                try:
                    state = self.field[pos]
                except: pass
                try:
                    nextfield = self.level_gens[ self.current_gen + 1 ]
                    target = nextfield[ pos ]               
                except: pass
                if state or target:
                    px = x * self.cellsize
                    py = y * self.cellsize 
                    glTranslatef(px, py, 0.0)
                    
                    if target and drawtarget:
                        glEnable(GL_BLEND)
                        glBegin(GL_QUADS)
                        glColor4f(*self.TARGET)
                        glVertex3f(0.0, 0.0, 0.0)
                        glVertex3f(self.cellsize, 0.0, 0.0)
                        glVertex3f(self.cellsize, self.cellsize, 0.0)
                        glVertex3f(0.0, self.cellsize, 0.0)
                        glEnd()
                    if state:
                        
                        if self.showflowers:
                            ox = self.cellsize / 2.0
                            oy = self.cellsize / 2.0
                            glTranslatef(ox, oy, 0.0)
                            self.flower.draw()
                            glTranslatef(-ox, -oy, 0.0)
                        else:
                            self.seed.draw()
                        
                    glTranslatef(-px, -py, 0.0)    
        glTranslatef(-self.x, -self.y, 0.0)
        
    def draw(self, drawtarget = False):
        if self.level_name:
            glCallList(self.__floor_display_id)
            self.draw_cells(drawtarget = drawtarget)
            glCallList(self.__grid_display_id)
                        
                    
        
        
        
        
      
    
    