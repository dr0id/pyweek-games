import pyglet.font
import pyglet.resource
import os

class FontManager(object):
    __default_path = 'data/fnt'
    
    def __init__(self):
        super(FontManager, self).__init__()
        self.__cache = {}
        
    def get(self, name, filename):
        if name in self.__cache:
            return self.__cache[ name ]
        path = os.path.join(self.__default_path, filename)
        pyglet.font.add_file(path)
        self.__cache[ name ] = pyglet.font.load(name)
        return self.__cache[ name ]