import cPickle
import os.path

class Level(object):
    def __init__(self, name, size = 8, generations = [{}]):
        self.name = name
        self.size = size
        self.generations = generations
        
class LevelManager(object):
    __default_path = 'data/lvl'
    
    def __init__(self):
        super(LevelManager, self).__init__()
    
    def get(self, name):
        name = name.replace(' ', '')
        filename = name + '.lvl'
        path = os.path.join(self.__default_path, filename)
        path = os.path.abspath( path )
        file = open(path, 'rb')
        level = cPickle.load(file)
        file.close()
        return level
        
    
    def save(self, level_obj ):
        name = level_obj.name.replace(' ', '')
        filename = name + '.lvl'
        path = os.path.join(self.__default_path, filename)
        path = os.path.abspath(path)
        print path
        f = open(path, 'wb')
        cPickle.dump(level_obj, f, -1)
        f.close()