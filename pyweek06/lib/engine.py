import pyglet
pyglet.options['audio'] = ( 'directsound','openal')
import pyglet.resource
import pyglet.media
import pyglet.font
import pyglet.window
import pyglet.clock
import pyglet.app
from pyglet.gl import glEnable, GL_BLEND

import lib.images
import lib.fonts
import lib.levels
import lib.progress
import lib.field
import lib.sprites
import os
class EngineClass(pyglet.window.Window):

    def __init__(self, *args, **named):
        super(EngineClass, self).__init__(**named)
        # Event handlers
        self.register_event_type('on_scene_freeze')
        self.register_event_type('on_scene_unfreeze')
        self.register_event_type('on_scene_enter')
        self.register_event_type('on_scene_leave')
        self.__event_loop = pyglet.app.EventLoop()
        self.__event_loop.push_handlers(self)
        # Scene attributes
        self.__scenes = []
        self.__current_scene = None
        self.__first_scene = None
        # Opengl initialization
        glEnable(GL_BLEND)
        # Game attributes
        self.field = None
        # Clock attributes
        self.__fps_display = pyglet.clock.ClockDisplay()
        ### MANAGERS ###
        self.images = lib.images.ImageManager()
        self.levels = lib.levels.LevelManager()
        self.sprites = lib.sprites.SpriteManager()
        self.fonts = lib.fonts.FontManager()
        ### SOUND ###

    def update_sounds(self, dt):
        for snd in self.soundlist.values():
            pass#snd.dispatch_events(dt)
        
    def play_sound(self, sound):
        return
        print self.soundlist[sound]
        snd = self.soundlist[sound].play()
        print snd.playing
        
    def draw_fps(self):
        pass#self.__fps_display.draw()
        
    def get_current_scene(self):
        return self.__current_scene
        
    def push_scene(self, scene):
        if self.__current_scene:
            self.__scenes.append(self.__current_scene)
            self.dispatch_event('on_scene_freeze')
            self.pop_handlers()
        self.__current_scene = scene
        self.push_handlers(self.__current_scene)
        self.dispatch_event('on_scene_enter')

    def swap_scene(self, scene):
        if self.__current_scene:
            self.dispatch_event('on_scene_leave')
            self.pop_handlers()
        self.__current_scene = scene
        self.push_handlers(self.__current_scene)
        self.dispatch_event('on_scene_enter')
        
    def pop_scene(self):
        if self.__current_scene:
            self.dispatch_event('on_scene_leave')
            self.pop_handlers()
            try:
                self.__current_scene = self.__scenes.pop()
                self.push_handlers( self.__current_scene )
                self.dispatch_event('on_scene_unfreeze')
            except Exception, e:
                self.on_close()
                
    def scene_count(self):
        return len(self.__scenes) + (1 if self.__current_scene else 0)
        
    def clear_scenes(self):
        for i in range(self.scene_count()):
            pass#self.pop_scene()
        self.__current_scene = None
        
    def run(self):
        self.__event_loop.run()
    
    def first_scene(self, scene):
        self.__first_scene = scene
    
    def on_enter(self):
        self.push_scene( self.__first_scene )
        self.set_visible()
        self.__first_scene = None
    
    def on_close(self):
        lib.progress.save()
        self.clear_scenes()
        self.__event_loop.exit()