import cPickle
import os.path
import lib.engine
_path = 'data/lvl'

class Progress(object):
    def __init__(self):
        self.easy = ['first', 'second', 'third', 'fourth', 'fifth']
        self.moderate = ['sixth', 'seventh', 'eighth', 'ninth', 'tenth']
        self.hard = ['eleventh', 'twelfth', 'thirteenth', 'fourteenth', 'fifteenth']
        
        self.progression = [self.easy, self.moderate, self.hard]
        
        self.difficulty = 0
        self.level = 0
        self.generation = 0
        self.win = 0
        self.total_time = 0
        

def reset():
    lib.engine.progress = Progress()
        
def load():
    global _path
    name = 'prog'
    path = os.path.join(_path, name)
    path = os.path.abspath( path )
    try:
        file = open(path, 'rb')
        lib.engine.progress = cPickle.load(file)
        file.close()
    except:
        lib.engine.progress = Progress()
    
    
def save():
    global _path
    name = 'prog'
    path = os.path.join(_path, name)
    path = os.path.abspath( path )
    file = open(path, 'wb')
    cPickle.dump(lib.engine.progress, file, -1)
    
    