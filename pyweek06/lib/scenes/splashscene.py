from lib import engine, scenes, levels, field, entry, progress
from pyglet.gl import *
from pyglet.window.key import *

import pyglet.sprite
import pyglet.text
import pyglet.clock
import string

class SplashScene(scenes.Scene):
    UPDATESPEED = 1 / 30.0
    LOGOSPEED = 65.0
    LOGOFADESPEED = 50.0
    BGFADESPEED = 70.0
    BYLINEFADESPEED = 65.0
    FADEOUTSPEED = 120.0
    HURRYSPEED = 2.0
    def on_scene_enter(self):
        # Load Background
        self.bg = engine.sprites.get('splashbg.png', opacity = 0)
        
        # Load Sprites
        self.logo = engine.sprites.get('mainlogo.png', opacity = 0)
        self.logo.x = (engine.width - self.logo.image.width) / 2
        self.logo.y = (engine.height - self.logo.image.height) / 2
        
        self.byline = engine.sprites.get('byline.png', opacity = 0)
        self.byline.x = (engine.width - self.byline.image.width) / 2
        self.byline.y = (engine.height - self.byline.image.height) / 2
        
        if engine.progress.level == 0 and engine.progress.difficulty == 0:
            gofile = 'gobutton.png', 'gobuttonover.png'
        else:
            gofile = 'continuebutton.png', 'continuebuttonover.png'
        
        self.gobutton = engine.sprites.get(gofile[0], opacity = 0)
        self.gobutton.x = (engine.width - self.gobutton.image.width) / 2
        self.gobutton.y = 250
        self.gobuttonover = engine.sprites.get(gofile[1], opacity = 0)
        self.gobuttonover.x = self.gobutton.x
        self.gobuttonover.y = self.gobutton.y
        
        self.howbutton = engine.sprites.get('howbutton.png', opacity = 0)
        self.howbutton.x = (engine.width - self.howbutton.image.width) / 2
        self.howbutton.y = self.gobutton.y - (self.howbutton.image.height + 15)
        self.howbuttonover = engine.sprites.get('howbuttonover.png', opacity = 0)
        self.howbuttonover.x = self.howbutton.x
        self.howbuttonover.y = self.howbutton.y
        
        self.exitbutton = engine.sprites.get('exitbutton.png', opacity = 0)
        self.exitbutton.x = (engine.width - self.exitbutton.image.width) / 2
        self.exitbutton.y = self.howbutton.y - (self.exitbutton.image.height * 2 + 15)
        self.exitbuttonover = engine.sprites.get('exitbuttonover.png', opacity = 0)
        self.exitbuttonover.x = self.exitbutton.x
        self.exitbuttonover.y = self.exitbutton.y
        
        self.startbutton = self.gobutton
        self.rulesbutton = self.howbutton
        self.quitbutton = self.exitbutton
        
        self.menuselection = None
        
        self.hurry = 1.0
        self.clickable = False
        self.clicked = False
        
        pyglet.clock.schedule_interval( self.first_trans, self.UPDATESPEED)
        
    def pick_start_button(self, x, y):        
        bx, bx2 = self.startbutton.x, self.startbutton.x + self.startbutton.image.width
        by, by2 = self.startbutton.y, self.startbutton.y + self.startbutton.image.height
        
        if x >= bx and x <= bx2:
            if y >= by and y <= by2:
                return True
        return False
        
    def pick_rules_button(self, x, y):        
        bx, bx2 = self.rulesbutton.x, self.rulesbutton.x + self.rulesbutton.image.width
        by, by2 = self.rulesbutton.y, self.rulesbutton.y + self.rulesbutton.image.height
        
        if x >= bx and x <= bx2:
            if y >= by and y <= by2:
                return True
        return False   
        
    def pick_quit_button(self, x, y):        
        bx, bx2 = self.exitbutton.x, self.exitbutton.x + self.exitbutton.image.width
        by, by2 = self.exitbutton.y, self.exitbutton.y + self.exitbutton.image.height
        
        if x >= bx and x <= bx2:
            if y >= by and y <= by2:
                return True
        return False 

    def first_trans(self, dt):
        """ Main logo fades in """
        self.logo.opacity = min(255, self.logo.opacity + dt * self.LOGOFADESPEED * self.hurry )
        if self.logo.opacity >= 255:
            pyglet.clock.unschedule( self.first_trans )
            pyglet.clock.schedule_interval( self.second_trans, self.UPDATESPEED)
    
    def second_trans(self, dt):
        """ Background fades in """
        
        self.bg.opacity = min(255, self.bg.opacity + dt * self.BGFADESPEED * self.hurry )
        if self.bg.opacity >= 255:
            pyglet.clock.unschedule( self.second_trans )
            pyglet.clock.schedule_interval( self.third_trans, self.UPDATESPEED)
    
    def third_trans(self, dt):
        """ Logo moves up as byline fades in """
        self.clickable = True
        self.logo.y = min(engine.height - self.logo.image.height, self.logo.y + dt * self.LOGOSPEED * self.hurry)
        if self.logo.y >= (engine.height / 2) + 15 and self.logo.y <= engine.height - self.logo.image.height:
            self.byline.y += dt * self.LOGOSPEED * self.hurry
        self.byline.opacity = min(255, self.byline.opacity + dt * self.BYLINEFADESPEED * self.hurry)
        self.gobutton.opacity = self.gobuttonover.opacity = self.byline.opacity
        self.howbutton.opacity = self.howbuttonover.opacity = self.byline.opacity
        self.exitbutton.opacity = self.exitbuttonover.opacity = self.byline.opacity
        if self.logo.y >= engine.height - self.logo.image.height and self.byline.opacity >= 255:
            pyglet.clock.unschedule( self.third_trans )
            
    def fourth_trans(self, dt):
        """ Start fade out """
        pyglet.clock.schedule_interval( self.fifth_trans, self.UPDATESPEED)
            
    def fifth_trans(self, dt):
        """ All but logo fade out """
        self.bg.opacity = self.byline.opacity = max(0, self.bg.opacity - dt * self.FADEOUTSPEED * self.hurry)
        self.gobutton.opacity = self.gobuttonover.opacity = self.bg.opacity
        self.howbutton.opacity = self.howbuttonover.opacity = self.bg.opacity
        self.exitbutton.opacity = self.exitbuttonover.opacity = self.bg.opacity
        if self.bg.opacity <= 0:
            pyglet.clock.unschedule( self.fifth_trans )
            if self.menuselection:
                pyglet.clock.schedule_once( self.menuselection, 1.0 )
            else:
                engine.pop_scene()
            
    def start_game(self, dt):
        #engine.pop_scene()
        engine.swap_scene( scenes.get('storybegin')() )
    
    def start_rules(self, dt):
        engine.push_scene( scenes.get('howtoscene')() )
    
    def on_mouse_motion(self, x, y, dx, dy):
        if self.clickable:
            # Start button hover
            if self.pick_start_button(x, y):
                self.startbutton = self.gobuttonover
            else:
                self.startbutton = self.gobutton
            # Rules button hover
            if self.pick_rules_button(x, y):
                self.rulesbutton = self.howbuttonover
            else:
                self.rulesbutton = self.howbutton
            # Exit button hover   
            if self.pick_quit_button(x, y):
                self.quitbutton = self.exitbuttonover
            else:
                self.quitbutton = self.exitbutton
            
    def on_mouse_press(self, x, y, button, mod):
        if not self.clicked and self.clickable:
            if self.pick_start_button(x, y):
                self.clicked = True
                self.menuselection = self.start_game
                self.fourth_trans(None)
            elif self.pick_rules_button(x, y):
                self.clicked = True
                engine.push_scene( scenes.get('howtoscene')() )
            elif self.pick_quit_button(x, y):
                self.clicked = True
                engine.pop_scene()
                self.fourth_trans(None)
    
    def on_scene_unfreeze(self):
        self.clicked = False
         
    def on_draw(self):
        engine.clear()
        self.bg.draw()
        self.logo.draw()
        self.byline.draw()
        self.startbutton.draw()
        self.rulesbutton.draw()
        self.quitbutton.draw()
        engine.draw_fps()
        
scene_class = SplashScene