from lib import engine, scenes, levels, field, entry, progress
from pyglet.gl import *
from pyglet.window.key import *

import pyglet.sprite
import pyglet.text
import pyglet.clock
import string
import random

class ProgressScene(scenes.Scene):
    UPDATESPEED = 1 / 30.0
    FADEINSPEED = 70.0
    FADEOUTSPEED = 80.0
    STAGES = ['easy', 'moderate', 'hard']
    
    def on_scene_enter(self):
        diff = self.STAGES[engine.progress.difficulty] 
        level = engine.progress.level + 1
        # Set BG
        self.set_bg()
        # Set Logo
        self.logo = engine.sprites.get('mainlogo.png')
        self.logo.x = (engine.width - self.logo.image.width) / 2
        self.logo.y = (engine.height - self.logo.image.height)
        # Set Info label
        self.info_label = pyglet.text.Label('Current Difficulty: %s\nCurrent Level: %d' % (diff.capitalize(), level),
                    font_size = 20, font_name = 'System', halign = 'center', multiline = True, width = 350, color = (0, 0, 0, 255) )
        self.info_label.x = engine.width  / 2
        self.info_label.y = engine.height - 150
        # Set Enter label
        self.enter_label = pyglet.text.Label('[ click to continue ]',
                    font_size = 9, font_name = 'System', halign = 'center', multiline = True, width = 150, color = (0, 0, 0, 255) )
        self.enter_label.x = engine.width  / 2
        self.enter_label.y = engine.height - 200
        # Create field
        engine.field = field.Field(thickness = 1.0)
        # Create flowers
        self.flowers = []
        pyglet.clock.schedule_interval(self.create_flower, 1.0)
        pyglet.clock.schedule_interval(self.update_flowers, 1/30.0)
        
    def create_flower(self, dt):
        newflower = field.Flower()
        newflower.x = random.randint(0, engine.width - 1)
        newflower.y = engine.height + newflower.image.image.height
        newflower.scale =  random.random() + .1
        newflower.rotationstep = 180 - (newflower.scale * 90)
        newflower.dx = -((random.random()*2.0) - 1.0)
        newflower.dy = -5.0 * newflower.scale
        self.flowers.append( newflower )
        
    def update_flowers(self, dt):
        dellist = list()
        for f in self.flowers:
            f.x += f.dx
            f.y += f.dy
            
            if f.y <= - f.image.image.height:
                dellist.append(f)
        for f in dellist:
            self.flowers.remove(f)
        del dellist
        
        
    def set_bg(self):
        diff = self.STAGES[engine.progress.difficulty] 
        self.bg = engine.sprites.get(diff + 'bg.png')    
        
    def change_level(self):
        engine.progress.level += 1
        prog = engine.progress.progression[ engine.progress.difficulty ]
        if engine.progress.level + 1 >= len( prog ):
            engine.progress.level = 0
            engine.progress.generation = 0
            engine.progress.difficulty += 1
            if engine.progress.difficulty >= len( engine.progress.progression ):
                return False
                
                
        return True
    
    def update_splash(self, *args):
            newgen = engine.field.calc_next()
            engine.field.field = newgen
        
    def start_gameplay(self):
        pyglet.clock.unschedule(self.update_splash)

        diff = engine.progress.difficulty
        lvl = engine.progress.level
        levelname = engine.progress.progression[diff][lvl]
        generation = engine.progress.generation
        engine.field.load_level( engine.levels.get(levelname), gen = generation )
        
        pyglet.clock.unschedule(self.create_flower)
        pyglet.clock.unschedule(self.update_flowers)
        
        engine.push_scene( scenes.get('gameplayscene')() )
    
    def on_scene_unfreeze(self):
        self.flowers = []
        pyglet.clock.schedule_interval(self.create_flower, 1.0)
        pyglet.clock.schedule_interval(self.update_flowers, 1/30.0)
        
        if not self.change_level():
            cls = scenes.get('storyend')
            scene = cls()
            engine.push_scene( scene )
            return True
        self.set_bg()
        
        diff = self.STAGES[engine.progress.difficulty] 
        level = engine.progress.level + 1
        
        self.info_label.text = 'Current Difficulty: %s\nCurrent Level: %d' % (diff.capitalize(), level)
        engine.field.load_level( engine.levels.get('splash'))
        

        
        
        
    def on_draw(self):
        engine.clear()
        self.bg.draw()
        for f in self.flowers:
            glTranslatef(f.x, f.y, 0.0)
            f.draw()
            glTranslatef(-f.x, -f.y, 0.0)
        self.logo.draw()
        self.enter_label.draw()
        self.info_label.draw()
        engine.draw_fps()
    
    def on_mouse_press(self, x, y, button, mod):
        self.start_gameplay()
        
    def on_key_press(self, key, mod):
        if key == ENTER:
            self.start_gameplay()
        
scene_class = ProgressScene        