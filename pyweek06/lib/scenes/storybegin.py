from lib import engine, scenes, levels, field, entry
from pyglet.gl import *
from pyglet.window.key import *
import pyglet.sprite
import pyglet.text
import pyglet.clock
import string

class StoryBeginScene(scenes.Scene):
    UPDATESPEED = 1 / 30.0
    FADEINSPEED = 100.0
    FADEOUTSPEED = 100.0
    PAUSETIME = 5.0
    HURRYSPEED = 2.0
    def on_scene_enter(self):
        self.hurry = 1.0
        
        if engine.progress.level == 0 and engine.progress.difficulty == 0:
            self.bg = engine.sprites.get('storybegin.png', opacity = 0)

            self.logo = engine.sprites.get('mainlogo.png')
            self.logo.x = (engine.width - self.logo.image.width) / 2
            self.logo.y = (engine.height - self.logo.image.height)
            
            pyglet.clock.schedule_interval( self.first_trans, self.UPDATESPEED)
        else:
            engine.push_scene( scenes.get('progressscene')() )
            
    def first_trans(self, dt):
        """ Fade in BG """
        self.bg.opacity = min(255, self.bg.opacity + dt * self.FADEINSPEED * self.hurry)
        if self.bg.opacity >= 255:
            pyglet.clock.unschedule( self.first_trans )
            pyglet.clock.schedule_once( self.second_trans, self.PAUSETIME / self.hurry )
            
    def second_trans(self, dt):
        """ Start fade out """
        pyglet.clock.schedule_interval( self.third_trans, self.UPDATESPEED )
            
    def third_trans(self, dt):
        self.bg.opacity = max(0, self.bg.opacity - dt * self.FADEOUTSPEED * self.hurry)
        if self.bg.opacity <= 0:
            pyglet.clock.unschedule( self.third_trans )
            engine.swap_scene( scenes.get('progressscene')() )
            #engine.pop_scene()
           
    def on_key_press(self, key, mod):
        self.hurry = self.HURRYSPEED
           
    def on_mouse_press(self, x, y, button, mod):
        self.hurry *= self.HURRYSPEED
        
    def on_draw(self):
        engine.clear()
        self.bg.draw()
        self.logo.draw()
        engine.draw_fps()
        
        
scene_class = StoryBeginScene