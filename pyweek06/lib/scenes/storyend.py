from lib import engine, scenes, progress
from pyglet.window.key import *
import pyglet.sprite
import pyglet.clock

class StoryEndScene(scenes.Scene):
    UPDATESPEED = 1 / 30.0
    FADEINSPEED = 100.0
    FADEOUTSPEED = 100.0
    PAUSETIME = 8.0
    LOGOPAUSETIME = 1.5
    LOGOFADESPEED = 130.0
    HURRYSPEED = 4.0
    
    def on_scene_enter(self):
        engine.progress = progress.Progress()
        self.bg = engine.sprites.get('storyend.png', opacity = 0)

        self.logo = engine.sprites.get('mainlogo.png', opacity = 0)
        self.logo.x = (engine.width - self.logo.image.width) / 2
        self.logo.y = (engine.height - self.logo.image.height) / 2
        
        pyglet.clock.schedule_interval(self.first_trans, self.UPDATESPEED)
     
    def first_trans(self, dt):
        """ Fade in BG """
        self.bg.opacity = min(255, self.bg.opacity + dt * self.FADEINSPEED)
        if self.bg.opacity >= 255:
            pyglet.clock.unschedule( self.first_trans )
            pyglet.clock.schedule_once( self.second_trans, self.PAUSETIME)
            
    def second_trans(self, dt):
        """ Start fade out """
        pyglet.clock.schedule_interval( self.third_trans, self.UPDATESPEED )
            
    def third_trans(self, dt):
        """ Fade out BG, Fade in logo """
        self.bg.opacity = max(0, self.bg.opacity - dt * self.FADEOUTSPEED)
        self.logo.opacity = min(255, self.logo.opacity + dt * self.FADEINSPEED)
        if self.bg.opacity <= 0 and self.logo.opacity >= 255:
            pyglet.clock.unschedule( self.third_trans )
            pyglet.clock.schedule_once( self.fourth_trans, self.LOGOPAUSETIME )
            
    def fourth_trans(self, dt):
        """ Start logo fade out """
        pyglet.clock.schedule_interval( self.fifth_trans, self.UPDATESPEED )
        
    def fifth_trans(self, dt):
        """ Fade out logo and exit """
        self.logo.opacity = max(0, self.logo.opacity - dt * self.LOGOFADESPEED)
        if self.logo.opacity <= 0:
            pyglet.clock.unschedule( self.fifth_trans )
            engine.on_close()             
            
    def on_draw(self):
        engine.clear()
        self.bg.draw()
        self.logo.draw()
        
         
scene_class = StoryEndScene