from lib import engine, scenes
from pyglet.gl import *
from pyglet.window.key import *

import pyglet.clock

class HowToScene(scenes.Scene):
    UPDATESPEED = 1 / 30.0
    FADEINSPEED = 180.0
    FADEOUTSPEED = 180.0
    
    def on_scene_enter(self):
        self.bg = engine.sprites.get('gamerules.png', opacity = 0)
        self.fading = False
        pyglet.clock.schedule_interval( self.first_trans, self.UPDATESPEED)
        
    def first_trans(self, dt):
        """ fade in """
        self.bg.opacity = min(255, self.bg.opacity + dt * self.FADEINSPEED)
        if self.bg.opacity >= 255:
            pyglet.clock.unschedule( self.first_trans )
            
    def second_trans(self, dt):
        """ fade out, exit """
        self.bg.opacity = max(0, self.bg.opacity - dt * self.FADEOUTSPEED)
        if self.bg.opacity <= 0:
            pyglet.clock.unschedule( self.second_trans )
            engine.pop_scene()
        
    def do_fadeout(self):
        if not self.fading:
            self.fading = True
            pyglet.clock.unschedule( self.first_trans )
            pyglet.clock.schedule_interval( self.second_trans, self.UPDATESPEED )
        
    def on_key_press(self, key, mod):
        self.do_fadeout()
        
    def on_mouse_press(self, x, y, button, mod):
        self.do_fadeout()
        
    def on_draw(self):
        engine.clear()
        self.bg.draw()
        
scene_class = HowToScene
    