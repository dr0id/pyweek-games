from lib import engine, scenes, levels, field, entry
from pyglet.gl import *
from pyglet.window.key import *
import pyglet.sprite
import pyglet.text
import pyglet.clock
import string

class EditorScene(scenes.Scene):
    
    def on_scene_enter(self):
        # Setup pyglet resources
        self.bg = engine.sprites.get('splashbg.png')
        
        self.logo = engine.sprites.get('mainlogo.png')
        self.logo.x = (engine.width - self.logo.image.width) / 2
        self.logo.y = (engine.height - self.logo.image.height)
        
        self.saveentry = entry.TextEntry( limit=string.ascii_lowercase, callback=self.save, label=" Save name:")
        self.loadentry = entry.TextEntry( limit=string.ascii_lowercase, callback=self.load, label=" Load name:")
        self.sizeentry = entry.TextEntry( limit=string.digits, callback=self.new, label=" Level size:")
        
            
    
    def on_draw(self):
        self.bg.draw()
        engine.field.draw()
        self.logo.draw()
        self.saveentry.draw()
        self.loadentry.draw()
        self.sizeentry.draw()
        engine.draw_fps()
    
    def new(self, text):
        engine.field.load_level( levels.Level("FirstLevel", size = int(text), generations= [{}]))
    
    def save(self, text):
        engine.levels.save( engine.field.save_level( name = text ) )
        
    def load(self, text):
        engine.field.load_level( engine.levels.get( text) )
        
    def on_key_press(self, key, mod):
        if mod & MOD_CTRL:
            if key == Q:
                engine.pop_scene()
            if key == N:
                self.sizeentry.show()
            if key == S:
                self.saveentry.show()
            if key == L:
                self.loadentry.show()
            if key == RIGHT:
                engine.field.edit_next()
            if key == LEFT:
                engine.field.edit_previous()
            if key == UP: engine.field.edit_first()
            if key == DOWN:
                newgen = engine.field.edit_next(step=True)
            if key == EQUAL:
                print "current:", engine.field.current_gen
                print "count:", len(engine.field.level_gens)
                for x in range(0, len(engine.field.level_gens)):
                    print x, engine.field.level_gens[x]
            

            
    
    def on_mouse_press(self, x, y, button, mod):
        pos = engine.field.pick_cell(x, y)
        if pos in engine.field.field:
            del engine.field.field[pos]
        else:
            engine.field.field[pos] = True
        
    
    def on_update(self, dt):
        pass#print dt
scene_class = EditorScene