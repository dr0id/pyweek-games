from lib import engine, scenes, levels, field, entry
from pyglet.gl import *
from pyglet.window.key import *

import pyglet.sprite
import pyglet.text
import pyglet.clock
import string

class GameplayScene(scenes.Scene):
    STAGES = ['easy', 'moderate', 'hard']
    
    def set_bg(self):
        diff = self.STAGES[engine.progress.difficulty] 
        self.bg = engine.sprites.get(diff + 'bg.png')    
    
    def on_scene_enter(self):
        self.resetting = self.advancing = self.winning = None
        self.set_bg()
        
        self.logo = engine.sprites.get('mainlogo.png')
        self.logo.x = (engine.width - self.logo.image.width) / 2
        self.logo.y = (engine.height - self.logo.image.height)
        
        self.buttonreg = engine.sprites.get('button.png')
        self.buttonreg.x = (engine.width - self.buttonreg.image.width) / 2
        self.buttonreg.y = (engine.height - self.buttonreg.image.height * 4)
        
        self.buttonover = engine.sprites.get('buttonover.png')
        self.buttonover.x = self.buttonreg.x
        self.buttonover.y = self.buttonreg.y
        
        self.button = self.buttonreg
        
    def on_draw(self):
        engine.clear()
        self.bg.draw()
        engine.field.draw( drawtarget = True )
        self.logo.draw()
        self.button.draw()
        engine.draw_fps()
        
    def reset_level(self, *args):
        if not self.resetting:
            self.resetting = True
            engine.field.flower.reset()
            engine.field.showflowers = True
            self.bg.opacity = 155
            pyglet.clock.schedule_once(self.reset_level, 4.0)
        else:
            self.resetting = False
            engine.field.showflowers = False
            self.bg.opacity = 255
            engine.field.load_generation( idx = 0 )
    
    def next_gen(self, *args):
        if not self.advancing:
            self.advancing = True
            self.bg.opacity = 200
            engine.field.flower.reset()
            engine.field.showflowers = True
            pyglet.clock.schedule_once(self.next_gen, 2.0)
        else:
            self.advancing = False
            engine.field.showflowers = False
            self.bg.opacity = 255
            engine.field.current_gen += 1
            nextgen = engine.field.next_generation()
            if nextgen is not None:
                engine.field.load_generation( idx = nextgen)
            else:            
                engine.pop_scene()
                
            
    
    def on_update(self, dt):
        pass
    
    def on_key_press(self, key, mod):
        if not self.resetting or self.advancing:
            if key == ENTER:
                newgen = engine.field.calc_next()
                levelgen = engine.field.level_gens[ engine.field.next_generation() ]
                engine.field.field = dict(newgen)
                if newgen == levelgen:
                    self.next_gen()
                else:
                    self.reset_level()
                    
            elif key == F1:
                engine.push_scene( scenes.get('editorscene')() )
    
    def pick_button(self, x, y):
        if self.resetting or self.advancing: return False
        
        bx, bx2 = self.button.x, self.button.x + self.button.image.width
        by, by2 = self.button.y, self.button.y + self.button.image.height
        
        if x >= bx and x <= bx2:
            if y >= by and y <= by2:
                return True
        return False
    
    def on_mouse_motion(self, x, y, dx, dy):
        if self.pick_button(x, y):
            self.button = self.buttonover
        else:
            self.button = self.buttonreg
    
    
    def on_mouse_press(self, x, y, button, mod):
        if self.resetting or self.advancing: return False
        
        if self.pick_button(x, y):
            self.on_key_press(ENTER, None)
        else:
            pos = engine.field.pick_cell(x, y)
            if pos in engine.field.field:
                del engine.field.field[pos]
            else:
                engine.field.field[pos] = True

            
scene_class = GameplayScene