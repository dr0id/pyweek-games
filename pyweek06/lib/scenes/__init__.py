import lib

_loaded_scene_classes = {}

class Scene(object):
    def __init__(self):
        super(Scene, self).__init__()     

    def on_update(self, dt):
        pass

def get( scene_name ):
    if scene_name in _loaded_scene_classes:
        return _loaded_scene_classes[ scene_name ]
    fullpath = '.'.join( [__name__, scene_name, 'scene_class'] )
    cls = lib._get_class(fullpath, parentClass = Scene)
    if cls:
        _loaded_scene_classes[ scene_name ] = cls
        return cls
    else:
        return None