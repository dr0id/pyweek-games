Celldoku
===============

Entry in PyWeek #6  <http://www.pyweek.org/6/>
Team: fydleroid
Members: fydo, ldlework, DR0ID_


DEPENDENCIES:

You might need to install some of these before running the game:

  Python 2.5.* >:     http://www.python.org/
  Pyglet 1.1 > :    http://www.pyglet.org/



RUNNING THE GAME:

On Windows or Mac OS X, locate the "run_game.pyw" file and double-click it.

Othewise open a terminal / console and "cd" to the game directory and run:

  python run_game.py



CONTROLS:

Mouse click: Toggle cell
Enter: Submit board
Escape: Exit

SUMMARY:

Each level is a progression of a Cellular Automata. At each generation, the board indicates a "target configuration" in which the player's submitted board must resolve to. After the player submits the board, the Rules of Life are applied to his submission and if the resultant board which is generated matches the target, he passes the level.

The game is played as a progression of levels. One after the other, just like a Cellular Automata. If you exit, the game will resume where you left off.

THEORY:

Celldoku is based around a area of complex systems research called Cellular Automata. The interest in these systems is the emergent complex behavior that arises from the extremely simple rules that control it.

The system exists as a two-dimensional grid of cells. Each cell has two states, either alive or dead, on or off. The simple rules that control the system are applied to the whole one cell at a time until a new generation or configuration is created.

The rules are applied to each cell individually, and the result of the rule determines whether or not the cell of the same position in the new generation, is alive or dead. Each rule considers the state of the cell which being checked and the state of its eight neightbors. Based on what these values are the rule might determine this cell to either live or die in the new configuration.

Different rulesets cause different behavior to be displayed in the system. One of the most interesting and well known rulesets was created by John Conway in 1970. Despite doing all of his automations by hand on paper he aimed for a truely interesting ruleset and his hardwork resulted in the following three rules:

* A dead cell will birth if it has EXACTLY three neighbors (Nurture)
* A live cell will die if it has 4 or more neighbors (Overcrowding)
* A live cell will die if it has 2 or less neighbors (Starvation)

How does this work in Celldoku?

In Celldoku, each level exists as a broken progression of generations in a Cellular Automata. The player is given an initial configuration on the board and a target configuration is indicated on the board in red. It is up to the player to "fix" the configuration so that when submitted it results in the generation of the target.

In this screenshot the live cells in the player's board are indicated by the dark areas. The target configuration is indicated by redness on the empty cells. If a live cell is currently occupies the target its indicated by a green shading of the cell. A legend:

Clear : Dead cell
Black : Alive cell
Black-Red : Alive cell occupying target
Red : Target

[screenshot] http://vector.ics-il.com/theory.png [/screenshot]

STRATEGY:

A general strategy for Celldoku is either have synthesisia or become a cyborg. If you can't manage those then maybe following will help:

Consider this - All the red-shaded target cells need to be alive when your board resolves. So ask yourself, "What three neighbor cells need to be alive, so this cell becomes alive?" This is because for a cell to become alive, it needs exactly three alive neighbors. Note however sometimes the cell or cells you need to change include cells in the target, so this piece of advice is only one of the things you need to consider for each cell.

"But I can't I just turn all the red cells on, and all the clear cells off?"

This will work for some target configurations. There are boards which are "stable" and do not change when the rules of Life are applied to them. Celldoku doesn't contain any target configurations like this. There are a few levels which contain generations where you don't have to change anything, watch out for these :)

LICENSE:

This game skellington is placed in the Public Domain.

