#!/usr/bin/python

import pygame, os, sys
from pygame.locals import *

pygame.init()

size = width, height = 128, 128
screen = pygame.display.set_mode(size, HWSURFACE|DOUBLEBUF)

flower = pygame.image.load('flower-anim.png').convert_alpha()

frame = 0

while 1:
	for event in pygame.event.get():
		if event.type == pygame.QUIT: 
			sys.exit()
		elif event.type == KEYDOWN:
			if (event.key == K_SPACE):
				frame = 0
			elif (event.key == K_ESCAPE):
				sys.exit()
	
	if frame < 15:
		frame = frame + 1
	
	screen.fill( (0, 0, 0))
	
	screen.blit(flower, (0,0), (frame * 128, 0, 128, 128))
	
	pygame.display.flip()
	pygame.time.wait(50)