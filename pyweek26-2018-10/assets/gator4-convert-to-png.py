import pygame
from pygame.locals import *
pygame.init()
clock = pygame.time.Clock()
screen = pygame.display.set_mode((350, 350))
screen_rect = screen.get_rect()

load_image = 'gator4-64x128.bmp'
save_image = 'gator4-64x128.png'
if load_image == save_image:
    raise Exception('error: loaded and saved images cannot be same file')

# load image
img = pygame.image.load(load_image)

# scale image
if False:  # scale me
    scale = float(128) / img.get_width()
    scale = 0.5
    img = pygame.transform.rotozoom(img, 0.0, scale)

# make dest surface
w, h = img.get_size()
img2 = pygame.Surface((w, h + 1), SRCALPHA)
img2.fill((0, 0, 0, 0), None, BLEND_MULT)
px1 = Rect(0, 0, 1, 1)

# copy pixels w/ alpha conversion
for x in range(img.get_width()):
    for y in range(img.get_height()):
        c = img.get_at((x, y))
        if c == (192, 192, 192, 255):
            c.a = 0
            c.r = c.g = c.b = 255
        px1.topleft = x, y
        img2.fill(c, px1, BLEND_RGBA_ADD)

# save dest image
pygame.image.save(img2, save_image)

# water blue = 0, 134, 139
bg_color = 0, 134, 139

while 1:
    clock.tick(30)
    screen.fill(bg_color)
    [quit() for e in pygame.event.get() if e.type == KEYDOWN]
    screen.blit(img2, (0, 0))
    pygame.display.flip()
