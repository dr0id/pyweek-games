import random
import pygame
from pygame.locals import *


random_seed = 1
random.seed(random_seed)
color_names = [k for k in pygame.color.THECOLORS.keys() if 'gray' not in k and 'grey' not in k]


class Game(object):
    def __init__(self):
        self.screen = pygame.display.get_surface()
        self.screen_rect = self.screen.get_rect()
        self.clear_color = Color('lightblue')

        self.max_ticks_per_second = 30
        self.max_caption_per_second = 1
        self.clock = pygame.time.Clock()

        # set these...
        self.tile_width = 32    # pixels
        self.tile_height = 32   # pixels
        self.tile_numx = 8      # num tiles
        self.tile_numy = 7      # num tiles
        self.surf_depth = 32    # depth arg
        self.surf_flags = 0     # flags arg

        # these are calculated
        self.filename = ('dummy_seed_{}.png'.format(random_seed))
        self.surf_width = self.tile_width * self.tile_numx
        self.surf_height = self.tile_height * self.tile_numy
        self.surf_size = self.surf_width, self.surf_height
        self.surf = pygame.Surface(self.surf_size, self.surf_flags, self.surf_depth)
        self.gen_surface()

        pygame.display.set_caption('SurfSize={} Filename={}'.format(self.surf_size, self.filename))

        self.running = False

    def gen_surface(self):
        def key(c):
            r, g, b, a = c
            return r + g + b  #, r * g * b, r * g, r
        colors = []
        chosen_colors = set()
        for i in range(self.tile_numx * self.tile_numy):
            color_name = random.choice(color_names)
            while color_name in chosen_colors:
                color_name = random.choice(color_names)
            chosen_colors.add(color_name)
        for name in sorted(chosen_colors):
            print(name)
            colors.append(tuple(Color(name)))
        # for c in colors:
        #     print(c)
        # quit()

        rect = Rect(0, 0, self.tile_width, self.tile_height)
        for iy in range(0, self.tile_numy):
            rect.y = iy * self.tile_height
            for ix in range(0, self.tile_numx):
                rect.x = ix * self.tile_width
                color = colors.pop(0)
                self.surf.fill(color, rect)

        pygame.image.save(self.surf, self.filename)

    def run(self):
        self.running = True
        while self.running:
            self.clock.tick(self.max_ticks_per_second)
            self.update(1000 / self.max_ticks_per_second)
            self.draw()

    def update(self, dt):
        self.handle_events()

    def draw(self):
        self.screen.fill(self.clear_color)
        self.screen.blit(self.surf, (0, 0))
        pygame.display.flip()

    def update_caption(self, dt):
        pygame.display.set_caption('{} fps'.format(int(self.draw_sched.per_second())))

    def handle_events(self):
        for e in pygame.event.get():
            if e.type == KEYDOWN:
                if e.key == K_ESCAPE:
                    self.running = False
            elif e.type == QUIT:
                self.running = False


if __name__ == '__main__':
    pygame.init()
    resolution = 800, 600
    pygame.display.set_mode(resolution)
    Game().run()
