<?xml version="1.0" encoding="UTF-8"?>
<tileset name="branch2" tilewidth="48" tileheight="81" tilecount="1" columns="1">
 <properties>
  <property name="kind" value="branch"/>
 </properties>
 <image source="../graphics/branch2.png" width="48" height="81"/>
 <tile id="0" type="branch">
  <properties>
   <property name="kind" value="branch"/>
  </properties>
 </tile>
</tileset>
