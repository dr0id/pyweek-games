<?xml version="1.0" encoding="UTF-8"?>
<tileset name="kayak" tilewidth="40" tileheight="40" tilecount="64" columns="8">
 <image source="../graphics/kayak.png" width="320" height="320"/>
 <tile id="48">
  <properties>
   <property name="kind" value="heroflipped"/>
  </properties>
  <animation>
   <frame tileid="20" duration="300"/>
   <frame tileid="21" duration="300"/>
  </animation>
 </tile>
 <tile id="49">
  <properties>
   <property name="kind" value="heroflipup"/>
  </properties>
  <animation>
   <frame tileid="19" duration="100"/>
   <frame tileid="18" duration="100"/>
   <frame tileid="17" duration="100"/>
   <frame tileid="16" duration="100"/>
   <frame tileid="13" duration="100"/>
  </animation>
 </tile>
 <tile id="50">
  <properties>
   <property name="kind" value="herosink"/>
  </properties>
  <animation>
   <frame tileid="24" duration="100"/>
   <frame tileid="25" duration="100"/>
   <frame tileid="26" duration="100"/>
   <frame tileid="27" duration="100"/>
   <frame tileid="27" duration="100"/>
  </animation>
 </tile>
 <tile id="51">
  <properties>
   <property name="kind" value="herosunken"/>
  </properties>
  <animation>
   <frame tileid="27" duration="100"/>
  </animation>
 </tile>
 <tile id="56">
  <properties>
   <property name="kind" value="hero"/>
  </properties>
  <animation>
   <frame tileid="0" duration="100"/>
   <frame tileid="1" duration="100"/>
   <frame tileid="2" duration="100"/>
   <frame tileid="3" duration="100"/>
   <frame tileid="4" duration="100"/>
   <frame tileid="5" duration="100"/>
   <frame tileid="6" duration="100"/>
   <frame tileid="7" duration="100"/>
  </animation>
 </tile>
 <tile id="57">
  <properties>
   <property name="kind" value="herojump"/>
  </properties>
  <animation>
   <frame tileid="8" duration="100"/>
   <frame tileid="9" duration="100"/>
   <frame tileid="10" duration="100"/>
   <frame tileid="10" duration="100"/>
   <frame tileid="14" duration="100"/>
   <frame tileid="14" duration="100"/>
   <frame tileid="11" duration="100"/>
   <frame tileid="12" duration="100"/>
   <frame tileid="13" duration="100"/>
  </animation>
 </tile>
 <tile id="58">
  <properties>
   <property name="kind" value="heroflip"/>
  </properties>
  <animation>
   <frame tileid="16" duration="100"/>
   <frame tileid="17" duration="100"/>
   <frame tileid="18" duration="100"/>
   <frame tileid="19" duration="100"/>
  </animation>
 </tile>
</tileset>
