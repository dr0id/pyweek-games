<?xml version="1.0" encoding="UTF-8"?>
<tileset name="bridges" tilewidth="32" tileheight="320" tilecount="6" columns="6">
 <image source="../graphics/logs.png" width="192" height="320"/>
 <tile id="0">
  <properties>
   <property name="kind" value="bridge"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="kind" value="bridge"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="kind" value="bridge"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="kind" value="log"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="kind" value="log"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="kind" value="pipeline"/>
  </properties>
 </tile>
</tileset>
