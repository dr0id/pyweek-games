<?xml version="1.0" encoding="UTF-8"?>
<tileset name="dummy_seed_1" tilewidth="32" tileheight="32" tilecount="56" columns="8">
 <image source="dummy_seed_1.png" width="256" height="224"/>
 <tile id="0">
  <properties>
   <property name="xyz" value="what is this?"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="kind" value="water"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="kind" value="water"/>
  </properties>
 </tile>
 <tile id="16">
  <properties>
   <property name="kind" value="water"/>
  </properties>
 </tile>
 <tile id="22">
  <properties>
   <property name="kind" value="border"/>
  </properties>
 </tile>
 <tile id="31">
  <properties>
   <property name="kind" value="border"/>
  </properties>
 </tile>
 <tile id="32">
  <properties>
   <property name="kind" value="grass"/>
  </properties>
 </tile>
 <tile id="39">
  <properties>
   <property name="kind" value="grass"/>
  </properties>
 </tile>
 <tile id="40">
  <properties>
   <property name="kind" value="grass"/>
  </properties>
 </tile>
 <tile id="43">
  <properties>
   <property name="kind" value="water"/>
  </properties>
 </tile>
 <tile id="49">
  <properties>
   <property name="kind" value="border"/>
  </properties>
 </tile>
 <tile id="52">
  <properties>
   <property name="kind" value="water"/>
  </properties>
 </tile>
</tileset>
