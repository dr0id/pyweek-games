# -*- coding: utf-8 -*-
"""
This it the main module. This is the main entry point of your code (put in the main() method).
"""
from __future__ import division, print_function

import logging

from gamelib.context_intro import Intro
from gamelib.context_game import Game
from gamelib.context_init_pygame import PygameInitContext
from pyknic import context

from gamelib import settings


logger = logging.getLogger(__name__)


def main():
    # put here your code
    import pyknic
    pyknic.logs.log_environment()
    pyknic.logs.print_logging_hierarchy()

    logging.getLogger().setLevel(logging.DEBUG)
    logging.getLogger().setLevel(settings.log_level)

    context.push(PygameInitContext())
    if settings.startup_context == 0:
        context.push(Intro())
    elif settings.startup_context == 1:
        raise NotImplemented
    elif settings.startup_context == 2:
        context.push(Game(level_num=settings.startup_level, damage_factor=1.0))

    while context.length():
        top = context.top()
        if top:
            top.update(0, 0)

    logger.debug('Finished. Exiting.')


# this is needed in order to work with py2exe
if __name__ == '__main__':
    main()
