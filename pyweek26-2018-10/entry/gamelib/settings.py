# # -*- coding: utf-8 -*-
import logging

import pygame

from gamelib.eventing import event_dispatcher
from pyknic.generators import GlobalIdGenerator, FlagGenerator
from pyknic.mathematics import Vec3
from pyknic.pyknic_pygame.eventmapper import ANY_MOD, EventMapper
from gamelib.skel import imagecache
from gamelib.skel import pygametext

#
#
# # ------------------------------------------------------------------------------
# # Tunables
# # ------------------------------------------------------------------------------
#
# print_fps = False

max_fps = 60
master_volume = 1.0             # min: 0.0; max: 1.0
log_level = [logging.WARNING, logging.DEBUG][0]

# ambient_color = 96, 96, 96      # pygame color values; higher for more visibility in dark levels; None for OFF
#
#
# # ------------------------------------------------------------------------------
# # CPU
# # ------------------------------------------------------------------------------
#
# ticks_per_second = 30
# frames_per_second = 150
# time_step = 1.0 / ticks_per_second
# sim_time_step = time_step
# frame_step = 1.0 / frames_per_second
sfx_step = 1.0
#
#
# # ------------------------------------------------------------------------------
# # Display
# # ------------------------------------------------------------------------------
#
# os.environ['SDL_VIDEO_CENTERED'] = '1'
#
screen_width = 1024
screen_height = 768
# screen_size = screen_width, screen_height
screen_flags_presets = (
    0,
    pygame.FULLSCREEN | pygame.DOUBLEBUF,
)
screen_flags = screen_flags_presets[0]
default_fill_color = 105, 139, 34
title = "Pyweek 26 - CoDoGuPywk26"
path_to_icon = "./data/icon.png"
screen = None

LAYER_HERO = 100
LAYER_LOG = 200

#
# # ------------------------------------------------------------------------------
# # Game
# # ------------------------------------------------------------------------------
#
# 0: context_intro
# 1: context_mainmenu
# 2: context_gameplay
# 3: context_credits
# 4: context_cutscene
startup_context = 0
startup_level = 1  # valid: 0 through 10

# Map
map_dir = './data/map'
map_names = (
    'test_map_002.json',  # there is no level 0, only a test map!
    'level01.json',       # game level 1: rocks: GAME START
    'level02.json',       # game level 2: swirls
    'level03.json',       # game level 3: branches
    'level04.json',       # game level 4: logs
    'level05.json',       # game level 5: waterfalls
    'level06.json',       # game level 6: gators
    'level07.json',       # game level 7: swirls and logs: CHALLENGE START
    'level08.json',       # game level 8: rapids
    'level09.json',       # game level 8: rocks, tubes and logs
    'level10.json',       # game level 10: DEFENDER OF THE REALM
)
map_difficulty = {
    0: 'fun',  # test map
    1: 'Whelp',
    2: 'Whelp',
    3: 'Whelp',
    4: 'Whelp',
    5: 'Whelp',
    6: 'Whelp',
    7: 'Adventurer',
    8: 'Adventurer',
    9: 'Adventurer',
    10: 'Defender Of The Realm',
    11: 'Defender Of The Realm',
    12: 'Defender Of The Realm',
}

# actions
_global_id_generator = GlobalIdGenerator(0)

ACTION_MOVE_LEFT = _global_id_generator.next()
ACTION_MOVE_RIGHT = _global_id_generator.next()
ACTION_JUMP = _global_id_generator.next()
ACTION_FLIP = _global_id_generator.next()

ACTION_TOGGLE_DEBUG_RENDER = _global_id_generator.next()

gameplay_event_map = EventMapper({
    # #     pygame.QUIT: {None: ACTION_QUIT},
    # #     pygame.MOUSEBUTTONDOWN: {
    # #         1: ACTION_FIRE,
    # #         3: ACTION_USE_ITEM,
    # #     },
    # #     pygame.MOUSEBUTTONUP: {
    # #         1: ACTION_HOLD_FIRE,
    # #     },
    # #     pygame.MOUSEMOTION: {
    # #         None: ACTION_MOUSE_MOTION,
    # #     },
    pygame.KEYDOWN: {
        (pygame.K_a, ANY_MOD): (0, ACTION_MOVE_LEFT),  # 0 is the index of the canoes list in Game for player 1
        (pygame.K_d, ANY_MOD): (0, ACTION_MOVE_RIGHT),
        (pygame.K_s, ANY_MOD): (0, ACTION_FLIP),
        (pygame.K_w, ANY_MOD): (0, ACTION_JUMP),

        (pygame.K_LEFT, ANY_MOD): (1, ACTION_MOVE_LEFT),  # 1 is the index of the canoes list in Game for player 2
        (pygame.K_RIGHT, ANY_MOD): (1, ACTION_MOVE_RIGHT),
        (pygame.K_DOWN, ANY_MOD): (1, ACTION_FLIP),
        (pygame.K_UP, ANY_MOD): (1, ACTION_JUMP),

        (pygame.K_F1, ANY_MOD): ACTION_TOGGLE_DEBUG_RENDER,
    },
    # #     pygame.KEYUP: {
    # #         (pygame.K_ESCAPE, ANY_MOD): ACTION_WIN_LEVEL,
    # #         (pygame.K_a, ANY_MOD): ACTION_STOP_LEFT,
    # #         (pygame.K_d, ANY_MOD): ACTION_STOP_RIGHT,
    # #         (pygame.K_w, ANY_MOD): ACTION_STOP_UP,
    # #         (pygame.K_s, ANY_MOD): ACTION_STOP_DOWN,
    # #     },
})

# # example for reference
# # __event_map = EventMapper({
# #     pygame.QUIT: {None: ACTION_QUIT},
# #     pygame.KEYDOWN: {
# #         (pygame.K_ESCAPE, ANY_MOD): ACTION_ESCAPE,
# #         (pygame.K_F4, pygame.KMOD_ALT): ACTION_QUIT,
# #         # cheat keys
# #         (pygame.K_F10, ANY_MOD): ACTION_WIN_LEVEL,
# #         (pygame.K_F9, ANY_MOD): ACTION_LOOSE_LEVEL,
# #         (pygame.K_F1, ANY_MOD): ACTION_TOGGLE_DEBUG_RENDER,
# #         (pygame.K_F2, ANY_MOD): ACTION_FULL_STOP,
# #         (pygame.K_F3, ANY_MOD): ACTION_HUD_SHOW_DEBUGS,
# #         # movement (arrows, joystick?, ...)
# #         (pygame.K_UP, ANY_MOD): ACTION_MOVE_UP,
# #         (pygame.K_DOWN, ANY_MOD): ACTION_MOVE_DOWN,
# #         (pygame.K_LEFT, ANY_MOD): ACTION_MOVE_LEFT,
# #         (pygame.K_RIGHT, ANY_MOD): ACTION_MOVE_RIGHT,
# #         (pygame.K_w, ANY_MOD): ACTION_MOVE_UP,
# #         (pygame.K_s, ANY_MOD): ACTION_MOVE_DOWN,
# #         (pygame.K_a, ANY_MOD): ACTION_MOVE_LEFT,
# #         (pygame.K_SPACE, ANY_MOD): ACTION_FIRE,
# #         (pygame.K_LCTRL, ANY_MOD): ACTION_FIRE,
# #         (pygame.K_m, ANY_MOD): ACTION_SECONDARY,
# #         (pygame.K_d, ANY_MOD): ACTION_MOVE_RIGHT,
# #     },
# #     pygame.KEYUP: {
# #         (pygame.K_SPACE, ANY_MOD): ACTION_HOLD_FIRE,
# #         (pygame.K_LCTRL, ANY_MOD): ACTION_HOLD_FIRE,
# #         (pygame.K_UP, ANY_MOD): ACTION_STOP_UP,
# #         (pygame.K_DOWN, ANY_MOD): ACTION_STOP_DOWN,
# #         (pygame.K_LEFT, ANY_MOD): ACTION_STOP_LEFT,
# #         (pygame.K_RIGHT, ANY_MOD): ACTION_STOP_RIGHT,
# #         (pygame.K_w, ANY_MOD): ACTION_STOP_UP,
# #         (pygame.K_s, ANY_MOD): ACTION_STOP_DOWN,
# #         (pygame.K_a, ANY_MOD): ACTION_STOP_LEFT,
# #         (pygame.K_d, ANY_MOD): ACTION_STOP_RIGHT,
# #     },
# # })
#
#
# ------------------------------------------------------------------------------
# messages
# ------------------------------------------------------------------------------
EVENT_COL_C_WALL = _global_id_generator.next()
EVENT_COL_C_WALL_ROT = _global_id_generator.next()
EVENT_COL_C_WATERFALL = _global_id_generator.next()
EVENT_COL_C_BRIDGE = _global_id_generator.next()
EVENT_COL_C_SWIRL = _global_id_generator.next()
EVENT_COL_C_GATOR_HEAD = _global_id_generator.next()  # gator only
EVENT_COL_GATOR_TAIL = _global_id_generator.next()  # -> gator only
EVENT_GATOR_BITE = _global_id_generator.next()  # gator -> canoe


event_dispatcher.register_event_type(EVENT_COL_C_WALL)
event_dispatcher.register_event_type(EVENT_COL_C_WALL_ROT)
event_dispatcher.register_event_type(EVENT_COL_C_WATERFALL)
event_dispatcher.register_event_type(EVENT_COL_C_BRIDGE)
event_dispatcher.register_event_type(EVENT_COL_C_SWIRL)
event_dispatcher.register_event_type(EVENT_COL_C_GATOR_HEAD)
event_dispatcher.register_event_type(EVENT_COL_GATOR_TAIL)
event_dispatcher.register_event_type(EVENT_GATOR_BITE)


# ------------------------------------------------------------------------------
# kinds
# ------------------------------------------------------------------------------
_flag_gen = FlagGenerator()
KIND_CANOE = _flag_gen.next()
KIND_START_EDGE = _flag_gen.next()
KIND_END_EDGE = _flag_gen.next()
KIND_WALL_EDGE = _flag_gen.next()
KIND_ROCK_EDGE = _flag_gen.next()
KIND_LOG_EDGE = _flag_gen.next()
KIND_WATERFALL = _flag_gen.next()
KIND_FINISH = _flag_gen.next()

KIND_MESSAGE = _flag_gen.next()
KIND_CHECKPOINT_EDGE = _flag_gen.next()
KIND_GATOR_JAW_EDGE = _flag_gen.next()  # todo: remove obsolete
KIND_GATOR_TAIL_EDGE = _flag_gen.next()
KIND_SWIRL = _flag_gen.next()
KIND_TREE_BRANCH_EDGE = _flag_gen.next()
KIND_BRIDGE_EDGE = _flag_gen.next()
KIND_SEA_MONSTER_EDGE = _flag_gen.next()
KIND_VELOCITY = _flag_gen.next()
KIND_GATOR = _flag_gen.next()

KIND_SFX = _flag_gen.next()
KIND_MUSIC = _flag_gen.next()


# # ------------------------------------------------------------------------------
# # Graphics
# # ------------------------------------------------------------------------------

# queue holds the texts
system_messages_queue = []
system_messages_used = []
system_message_y = screen_height * 0.2

# cached surfaces (required by spritefx)
# Note: pygametext does its own caching. No need to cache surfaces from pygametext.getsurf() unless you transform them.
# Note: these settings are not tunable on the fly. If you want to change the image cache settings during runtime, you'll
# have to call the image_cache instance's methods.
# Note: If you rely on aging or memory cap, remember to call image_cache.update(), or purge_memory() or purge_old().
image_cache_expire = 1 * 60          # expire unused textures after N seconds; recommend 1 min (1 * 60)
image_cache_memory = 5 * 2 ** 20     # force expiration if memory is exceeded; recommend 5 MB (5 * 2**20)
image_cache = imagecache.ImageCache(image_cache_expire, image_cache_memory)
image_cache.enable_age_cap(True)

pygametext.FONT_NAME_TEMPLATE = 'data/font/%s'

# font_themes is used with the functions in module pygametext
# Example call to render some text using a theme:
#   image = pygametext.getsurf('some text', **settings.font_themes['intro'])
# font_themes can be accessed by nested dict keys:
#   font_theme['intro']['fontname']
font_themes = dict(
    intro=dict(
        fontname='Boogaloo.ttf',
        fontsize=52,
        color='red2',
        gcolor='deepskyblue',
        ocolor='yellow',
        owidth=0.5,
        scolor=None,
        shadow=None,
    ),
    mainmenu=dict(
        fontname='Bubblegum_Sans.ttf',
        fontsize=80,
        color='red2',
        gcolor='orange1',
        ocolor='yellow',
        owidth=0.5,
        scolor=None,
        shadow=None,
    ),
    game=dict(
        fontname='VeraBD.ttf',
        fontsize=38,
        color='yellow',
        gcolor='orange3',
        ocolor=None,
        owidth=0.5,
        scolor='grey20',
        shadow=(-1, 1),
        italic=True,
    ),
    gamehud=dict(
        fontname='VeraBd.ttf',
        fontsize=25,
        color='yellow',
        gcolor='orange2',
        ocolor=None,
        owidth=0.5,
        scolor='grey20',
        shadow=(1, 1),
    ),
    floatingtext=dict(
        fontname='Vera.ttf',
        fontsize=18,
        color='red',
        gcolor='lightblue4',
        ocolor='yellow',
        owidth=0.5,
        scolor=None,
        shadow=None,
    ),
)

# ------------------------------------------------------------------------------
# Sound
# ------------------------------------------------------------------------------

# Mixer
MIXER_NUM_CHANNELS = 24
MIXER_FREQUENCY = 0
MIXER_BUFFER_SIZE = 128
MIXER_RESERVED_CHANNELS = 6

# Player
sfx_path = 'data/sfx'
music_path = 'data/music'
(
    channel_paddle,
    channel_hit,
    channel_gator_growl,
    channel_gator_splash,
    channel_waterfall,
    channel_splashes,
) = range(MIXER_RESERVED_CHANNELS)
sfx_handler = None  # this will be set from sfx.py, py 2.7, avoid circular dependencies!


# key_to_text = {
#     pygame.K_BACKSPACE: ("\\b", "backspace"),
#     pygame.K_TAB: ("\\t", "tab"),
#     pygame.K_CLEAR: ("", "clear"),
#     pygame.K_RETURN: ("\\r", "return"),
#     pygame.K_PAUSE: ("", "pause"),
#     pygame.K_ESCAPE: ("^[", "escape"),
#     pygame.K_SPACE: ("", "space"),
#     pygame.K_EXCLAIM: ("!", "exclaim"),
#     pygame.K_QUOTEDBL: ("\"", "quotedbl"),
#     pygame.K_HASH: ("#", "hash"),
#     pygame.K_DOLLAR: ("$", "dollar"),
#     pygame.K_AMPERSAND: ("&", "ampersand"),
#     pygame.K_QUOTE: ("", "quote"),
#     pygame.K_LEFTPAREN: ("(", "left parenthesis"),
#     pygame.K_RIGHTPAREN: ("),", "right parenthesis"),
#     pygame.K_ASTERISK: ("*", "asterisk"),
#     pygame.K_PLUS: ("+", "plus sign"),
#     pygame.K_COMMA: (",", "comma"),
#     pygame.K_MINUS: ("-", "minus sign"),
#     pygame.K_PERIOD: (".", "period"),
#     pygame.K_SLASH: ("/", "forward slash"),
#     pygame.K_0: ("0", "0"),
#     pygame.K_1: ("1", "1"),
#     pygame.K_2: ("2", "2"),
#     pygame.K_3: ("3", "3"),
#     pygame.K_4: ("4", "4"),
#     pygame.K_5: ("5", "5"),
#     pygame.K_6: ("6", "6"),
#     pygame.K_7: ("7", "7"),
#     pygame.K_8: ("8", "8"),
#     pygame.K_9: ("9", "9"),
#     pygame.K_COLON: (":", "colon"),
#     pygame.K_SEMICOLON: (";", "semicolon"),
#     pygame.K_LESS: ("<", "less-than sign"),
#     pygame.K_EQUALS: ("=", "equals sign"),
#     pygame.K_GREATER: (">", "greater-than sign"),
#     pygame.K_QUESTION: ("?", "question mark"),
#     pygame.K_AT: ("@", "at"),
#     pygame.K_LEFTBRACKET: ("[", "left bracket"),
#     pygame.K_BACKSLASH: ("\\", "backslash"),
#     pygame.K_RIGHTBRACKET: ("]", "right bracket"),
#     pygame.K_CARET: ("^", "caret"),
#     pygame.K_UNDERSCORE: ("_", "underscore"),
#     pygame.K_BACKQUOTE: ("`", "grave"),
#     pygame.K_a: ("a", "a"),
#     pygame.K_b: ("b", "b"),
#     pygame.K_c: ("c", "c"),
#     pygame.K_d: ("d", "d"),
#     pygame.K_e: ("e", "e"),
#     pygame.K_f: ("f", "f"),
#     pygame.K_g: ("g", "g"),
#     pygame.K_h: ("h", "h"),
#     pygame.K_i: ("i", "i"),
#     pygame.K_j: ("j", "j"),
#     pygame.K_k: ("k", "k"),
#     pygame.K_l: ("l", "l"),
#     pygame.K_m: ("m", "m"),
#     pygame.K_n: ("n", "n"),
#     pygame.K_o: ("o", "o"),
#     pygame.K_p: ("p", "p"),
#     pygame.K_q: ("q", "q"),
#     pygame.K_r: ("r", "r"),
#     pygame.K_s: ("s", "s"),
#     pygame.K_t: ("t", "t"),
#     pygame.K_u: ("u", "u"),
#     pygame.K_v: ("v", "v"),
#     pygame.K_w: ("w", "w"),
#     pygame.K_x: ("x", "x"),
#     pygame.K_y: ("y", "y"),
#     pygame.K_z: ("z", "z"),
#     pygame.K_DELETE: ("", "delete"),
#     pygame.K_KP0: ("", "keypad 0"),
#     pygame.K_KP1: ("", "keypad 1"),
#     pygame.K_KP2: ("", "keypad 2"),
#     pygame.K_KP3: ("", "keypad 3"),
#     pygame.K_KP4: ("", "keypad 4"),
#     pygame.K_KP5: ("", "keypad 5"),
#     pygame.K_KP6: ("", "keypad 6"),
#     pygame.K_KP7: ("", "keypad 7"),
#     pygame.K_KP8: ("", "keypad 8"),
#     pygame.K_KP9: ("", "keypad 9"),
#     pygame.K_KP_PERIOD: (".", "keypad period"),
#     pygame.K_KP_DIVIDE: ("/", "keypad divide"),
#     pygame.K_KP_MULTIPLY: ("*", "keypad multiply"),
#     pygame.K_KP_MINUS: ("-", "keypad minus"),
#     pygame.K_KP_PLUS: ("+", "keypad plus"),
#     pygame.K_KP_ENTER: ("\\r", "keypad enter"),
#     pygame.K_KP_EQUALS: ("=", "keypad equals"),
#     pygame.K_UP: ("", "up arrow"),
#     pygame.K_DOWN: ("", "down arrow"),
#     pygame.K_RIGHT: ("", "right arrow"),
#     pygame.K_LEFT: ("", "left arrow"),
#     pygame.K_INSERT: ("", "insert"),
#     pygame.K_HOME: ("", "home"),
#     pygame.K_END: ("", "end"),
#     pygame.K_PAGEUP: ("", "page up"),
#     pygame.K_PAGEDOWN: ("", "page down"),
#     pygame.K_F1: ("", "F1"),
#     pygame.K_F2: ("", "F2"),
#     pygame.K_F3: ("", "F3"),
#     pygame.K_F4: ("", "F4"),
#     pygame.K_F5: ("", "F5"),
#     pygame.K_F6: ("", "F6"),
#     pygame.K_F7: ("", "F7"),
#     pygame.K_F8: ("", "F8"),
#     pygame.K_F9: ("", "F9"),
#     pygame.K_F10: ("", "F10"),
#     pygame.K_F11: ("", "F11"),
#     pygame.K_F12: ("", "F12"),
#     pygame.K_F13: ("", "F13"),
#     pygame.K_F14: ("", "F14"),
#     pygame.K_F15: ("", "F15"),
#     pygame.K_NUMLOCK: ("", "numlock"),
#     pygame.K_CAPSLOCK: ("", "capslock"),
#     pygame.K_SCROLLOCK: ("", "scrollock"),
#     pygame.K_RSHIFT: ("", "right shift"),
#     pygame.K_LSHIFT: ("", "left shift"),
#     pygame.K_RCTRL: ("", "right ctrl"),
#     pygame.K_LCTRL: ("", "left ctrl"),
#     pygame.K_RALT: ("", "right alt"),
#     pygame.K_LALT: ("", "left alt"),
#     pygame.K_RMETA: ("", "right meta"),
#     pygame.K_LMETA: ("", "left meta"),
#     pygame.K_LSUPER: ("", "left windows key"),
#     pygame.K_RSUPER: ("", "right windows key"),
#     pygame.K_MODE: ("", "mode shift"),
#     pygame.K_HELP: ("", "help"),
#     pygame.K_PRINT: ("", "print screen"),
#     pygame.K_SYSREQ: ("", "sysrq"),
#     pygame.K_BREAK: ("", "break"),
#     pygame.K_MENU: ("", "menu"),
#     pygame.K_POWER: ("", "power"),
#     pygame.K_EURO: ("", "euro"),
# }
#
# mod_key_to_text = {
#     pygame.KMOD_NONE: "",
#     pygame.KMOD_LSHIFT: "left shift",
#     pygame.KMOD_RSHIFT: "right shift",
#     pygame.KMOD_SHIFT: "shift",
#     pygame.KMOD_CAPS: "capslock",
#     pygame.KMOD_LCTRL: "left ctrl",
#     pygame.KMOD_RCTRL: "right ctrl",
#     pygame.KMOD_CTRL: "ctrl",
#     pygame.KMOD_LALT: "left alt",
#     pygame.KMOD_RALT: "right alt",
#     pygame.KMOD_ALT: "alt",
#     pygame.KMOD_LMETA: "left meta",
#     pygame.KMOD_RMETA: "right meta",
#     pygame.KMOD_META: "meta",
#     pygame.KMOD_NUM: "num",
#     pygame.KMOD_MODE: "mode",
# }
#
PIXEL_PPU = 1.0
PPU = 10.0 * PIXEL_PPU  # [pixels/m]
max_under_water_time_span_in_seconds = 3  # [s]
under_water_update_interval = 1  # [s]
rotation_angle = 15  # [degrees]
gravitation = 9.81  # [m/s**2]
water_velocity = Vec3(1.0, 0.0, 0.0)  # flow velocity of the water in [m/s]
canoe_speed = 0.8  # relative to the water in [m/s]
canoe_velocity = Vec3(canoe_speed, 0.0, 0.0)  # relative to the water in [m/s]
canoe_length = 3.0  # [m]
canoe_collision_box_size = 50  # [pixels]
canoe_push_wall_distance = 1.0  # [m]  distance from the wall after a collision
canoe_jump_height = 1.0  # [m] jump height
tumble_time_span = 0.5  # [s] time that is will be tumbling
gator_body_radius = 16  # [pixels]
gator_jaw_length = 15  # [pixels]
gator_head_distance = 20  # [pixels]
MAX_DT = 0.5  # [s]

STEP_DT = 1.0 / 20.0  # max dt for lock stepper, 1.0 / fps

# setting needed by spritefx module
time_step = STEP_DT

DAMAGE_CANOE_WALL = 10
DAMAGE_CANOE_WALL_ROTATE = 5
DAMAGE_CANOE_WATERFALL_CRASH = 20
DAMAGE_CANOE_LOG = 5
DAMAGE_GATOR_BITE = 10

try:
    from gamelib._gummbum_custom import *
except:
    pass
