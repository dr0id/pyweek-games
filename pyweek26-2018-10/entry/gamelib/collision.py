# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'collission.py' is part of codogupywk26
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Module containing (most of) the collision code and the collider.


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division

import logging
from itertools import groupby
import random

import pygame
from pygame.rect import Rect

from gamelib import settings
from gamelib import resource_sound
from gamelib.entities.canoe import Airborne
from gamelib.eventing import event_dispatcher
from gamelib.settings import canoe_length, PPU, canoe_push_wall_distance, rotation_angle, canoe_speed, EVENT_COL_C_WALL, \
    EVENT_COL_C_WALL_ROT, EVENT_COL_C_WATERFALL, EVENT_COL_C_BRIDGE, EVENT_COL_C_SWIRL, gator_body_radius, gator_jaw_length
from pyknic.mathematics import Vec3 as Vec, sign, distance_from_line

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2018"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["Collider"]  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


def collide_canoe_vs_wall(canoe, walls, kinds):
    if canoe.state_machine.current_state == Airborne:
        return

    # get nearest and process only this one
    canoe_pos = canoe.position * PPU
    walls = list(sorted(walls, key=lambda o: canoe_pos.get_distance_sq(o.position)))
    wall = walls[0]

    canoe_dir = Vec(canoe.v.x, canoe.v.y).normalized
    front = (canoe.position + canoe_dir * canoe_length / 2.0) * PPU

    wall_dir = wall.p1 - wall.p0

    _collide_rotate(canoe, canoe_dir, front, wall, wall_dir, kinds)

    # is canoe in wall range?
    wall2canoe = canoe.position * PPU - wall.p0
    if wall_dir.dot(wall2canoe) < 0:
        return

    if (-wall_dir).dot(canoe.position * PPU - wall.p1) < 0:
        return

    wall2old = canoe.old_position * PPU - wall.p0
    # is canoe in back of the wall?
    if wall.normal.dot(wall2canoe) < 0 < wall.normal.dot(wall2old) and wall.normal.dot(canoe.v + canoe.water_v) < 0:
        # collision!
        depth = distance_from_line(wall.p0, wall_dir.normalized, canoe.position * PPU)
        depth = wall.normal * (depth + canoe_push_wall_distance * PPU)
        canoe.position += depth / PPU
        event_dispatcher.fire(EVENT_COL_C_WALL, canoe, kinds, wall)


def collide_canoe_vs_rock(canoe, rocks, kinds):
    collide_canoe_vs_wall(canoe, rocks, kinds)


def collide_canoe_vs_branch(canoe, branches, kinds):
    collide_canoe_vs_wall(canoe, branches, kinds)


def _collide_rotate(canoe, direction, front, wall, wall_dir, kinds):
    # is it in wall range
    wall2front = front - wall.p0
    if wall_dir.dot(wall2front) < 0:
        return
    if (-wall_dir).dot(front - wall.p1) < 0:
        return
    # is front of canoe in back of the wall?
    if wall2front.dot(wall.normal) < 0:
        # is the front near enough
        dist = distance_from_line(wall.p0, wall_dir.normalized, front)
        if dist < canoe_length:  # hack, wont work well for small angles between edges!

            # collision!
            angle = wall.normal.get_angle_between(direction)
            cross = wall.normal.cross(direction)
            if cross.dot(Vec(0, 0, -1)) < 0:
                angle = -angle
            # logger.debug("collision wall2front {0} vs wall normal{1} angle -90 {2}", wall2front, wall.normal, angle)
            canoe.v.rotate(angle)
            canoe.v.rotate(sign(angle) * -90)

            angle = canoe.v.angle
            steps, modulo = divmod(angle, rotation_angle)
            if modulo > rotation_angle / 2.0:
                steps += 1
            vv = Vec(1, 0, 0)
            vv.length = canoe_speed
            vv.rotate(steps * rotation_angle)
            canoe.v.copy_values(vv)
            event_dispatcher.fire(EVENT_COL_C_WALL_ROT, canoe, kinds, wall)


def collide_canoe_vs_waterfall(canoe, waterfalls, kinds):
    canoe_pos = canoe.position * PPU
    for waterfall in waterfalls:
        r = Rect(waterfall.aabb)
        if r.collidepoint(canoe_pos.as_xy_tuple(int)):
            event_dispatcher.fire(EVENT_COL_C_WATERFALL, canoe, kinds, waterfall)


def collide_canoe_bridge(canoe, log_edges, kinds):
    # find first
    log_edges = set(log_edges)

    while log_edges:
        first = log_edges.pop().first

        # remove chained edges from list if present to not process them twice
        log_edges.difference_update(first.edges)

        canoe_pos = canoe.position * PPU
        inside = True
        for edge in first.edges:
            edge2canoe = canoe_pos - edge.p0
            if edge.normal.dot(edge2canoe) > 0:
                inside = False
                break
        if inside:
            event_dispatcher.fire(EVENT_COL_C_BRIDGE, canoe, kinds, first)


def collide_canoe_vs_swirl(canoe, swirls, kinds):
    canoe_pos = canoe.position * PPU
    for swirl in swirls:
        if canoe_pos.get_distance_sq(swirl.position) < swirl.radius ** 2:
            # collision
            event_dispatcher.fire(EVENT_COL_C_SWIRL, canoe, swirl)
            settings.sfx_handler.handle_message(random.choice(resource_sound.SFX_GATOR_SPLASHES), 'mutex')


def collide_canoe_vs_velocity(canoe, vels, kinds):
    canoe_pos = canoe.position * PPU
    for vel in vels:
        if pygame.Rect(*vel.aabb).collidepoint(canoe_pos.as_xy_tuple(int)):
            canoe.water_v.copy_values(vel.velocity)
            break


def collide_canoe_vs_messaging(canoe, walls, kinds):
    x = canoe.position.x * settings.PPU
    y = canoe.position.y * settings.PPU
    sys_queue = settings.system_messages_queue
    sys_used = settings.system_messages_used

    for ent in walls:
        r = pygame.Rect(ent.aabb)
        if not r.collidepoint(x, y):
            continue

        text = getattr(ent, 'collision_text', None)
        if text is not None:
            if text in sys_queue or text in sys_used:
                # kludge to only show text once
                ent.collision_text = None
            else:
                r = Rect(ent.aabb)
                if r.collidepoint(x, y):
                    sys_queue.append(text)
                    ent.collision_text = None

        sfx_play = getattr(ent, 'sfx_play', None)
        if sfx_play is not None:
            if sfx_play == 'waterfall':
                settings.sfx_handler.handle_message(resource_sound.SFX_WATERFALL)
                # kludge to only play sfx once
                ent.sfx_play = None

        sfx_fadeout = getattr(ent, 'sfx_fadeout', None)
        if sfx_fadeout is not None:
            if sfx_fadeout == 'waterfall':
                settings.sfx_handler.fadeout(resource_sound.SFX_WATERFALL, 1000)
                # kludge to only fadeout once
                ent.sfx_fadeout = None


def collide_canoe_vs_gator(canoe, gators, kinds):
    canoe_pos = canoe.position * PPU
    for gator in gators:
        # is it in in front or at the tail?
        head = gator.position + gator.direction
        gator2canoe = canoe_pos - head
        if gator.direction.dot(gator2canoe) > 0:
            # front, is distance to canoe small enough to bite?
            dist_canoe_sq = canoe_pos.get_distance_sq(head)
            dist_front_sq = head.get_distance_sq(
                (canoe.position + canoe.v.normalized * settings.canoe_length / 2) * PPU)
            if dist_canoe_sq < gator_jaw_length ** 2 or dist_front_sq < gator_jaw_length ** 2:
                event_dispatcher.fire(settings.EVENT_COL_C_GATOR_HEAD, gator, kinds, canoe)
        else:
            # tail, does it touch the body?
            if gator.position.get_distance_sq(canoe_pos) < gator_body_radius ** 2:
                event_dispatcher.fire(settings.EVENT_COL_GATOR_TAIL, gator, kinds, canoe)


class Collider(object):

    def __init__(self):
        self._callbacks = {}  # {(kind1, kind2): callback}

    def register(self, kinds, callback):
        k1, k2 = kinds
        if k2 < k1:
            raise Exception("inverse kinds {0}".format(kinds))

        self._callbacks[kinds] = callback

    def collide_one2many(self, one, others):
        one_kind = one.kind
        for other_kind, grouped in groupby(others, key=lambda o: o.kind):

            kinds = (one_kind, other_kind)
            if other_kind < one_kind:
                kinds = (other_kind, one_kind)

            coll_func = self._callbacks.get(kinds, None)
            if coll_func is None:
                logger.warning("no coll function found for kinds: {0}", kinds)
                continue

            coll_func(one, grouped, kinds)

            # for other in others:
            #     other_kind = other.kind
            #     if other_kind < one_kind:
            #         one_kind, other_kind = other_kind, one_kind
            #     kinds = (one_kind, other_kind)
            #     coll_func = self._callbacks.get(kinds, None)
            #     if coll_func is None:
            #         logger.warning("no coll function found for kinds: {0}", kinds)
            #         continue
            #
            #     coll_func(one, other)


logger.debug("imported")
