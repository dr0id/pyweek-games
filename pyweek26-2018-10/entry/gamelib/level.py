# -*- coding: utf-8 -*-

import logging

import pygame

import pytmxloader
from gamelib.animation import SpriteAnimation, SpecialAnimation
from gamelib.entities.gator import Gator
from gamelib.entities.walledge import *
from gamelib.settings import (KIND_FINISH, KIND_START_EDGE, KIND_CHECKPOINT_EDGE, KIND_VELOCITY, KIND_MESSAGE,
                              KIND_WATERFALL, KIND_SWIRL, KIND_GATOR, KIND_ROCK_EDGE,
                              gator_head_distance, LAYER_HERO, LAYER_LOG, PIXEL_PPU)
from pyknic import tweening
from pyknic.mathematics import Point3 as Point, Vec3 as Vec, sign
from pyknic.mathematics.geometry.binspace import AabbBinSpace2
from pyknic.pyknic_pygame.spritesystem import Sprite, TextSprite
from pyknic.timing import Timer
from pytmxloader import TILE_LAYER_TYPE

logger = logging.getLogger(__name__)


def _log_tiled_sprite_needed(layer_type, layer_name, obj_kind, obj_id):
    logger.warning("TODO: Tiled {} object needs a sprite:  layer={} kind={} id={}",
                   layer_type, layer_name, obj_kind, obj_id)
    raise Exception("TODO: Tiled {} object needs a sprite:  layer={} kind={} id={}".format(
        layer_type, layer_name, obj_kind, obj_id))


class Level(object):
    def __init__(self, renderer):
        self.renderer = renderer
        self.space = AabbBinSpace2()
        self.map_file_name = "unknown"
        self.start_area = None
        self.tweener = tweening.Tweener()
        self.hero_anim_normal = []
        self.hero_anim_jump = []
        self.hero_anim_flip = []
        self.hero_anim_flipped = []
        self.hero_anim_flip_up = []
        self.hero_anim_sink = []
        self.hero_anim_sunken = []

    def load_map(self, name, scheduler):
        img_cache = {}
        sprites = []
        has_background = False

        logger.info(">>> LOADING MAP {0}", name)

        def make_obj_rect_spr(obj, rect, idx, color, colorkey=(0, 0, 0)):
            if not isinstance(color, pygame.Color):
                color = pygame.Color(color)
            x, y, w, h = ot.pixel_rect
            pos = Point(x, y) + Vec(w // 2, h // 2)
            img = pygame.Surface(rect.size, pygame.SRCALPHA)
            # img.set_colorkey(colorkey)
            img.fill(color)
            spr = Sprite(img, pos, anchor="center", z_layer=idx, name=(obj.name, obj.object_id))
            spr.rotation = -obj.rotation
            spr.use_sub_pixel = True
            sprites.append(spr)
            return spr

        def make_ent_data(obj):
            p_last = obj.pixel_points[0]
            normal = obj.properties.get("normal", "left")
            points = []
            for p in obj.pixel_points[1:]:
                points.append([p_last, p])
                p_last = p
            points.append([p_last, obj.pixel_points[0]])
            return points, normal

        self.map_file_name = name
        map_info = pytmxloader.load_map_from_file_path(name)
        # TODO: he is broke
        # Sprite.configure_global_image_cache(True, max_cache_entry_count=100000)
        # Sprite._image_cache.max_memory = 2 * 2 ** 30  # 1*2**30 = 1 GB
        for idx, layer in enumerate(map_info.layers):
            if not layer.visible:
                continue
            pxm = layer.properties.get("parallax_x_min", 1.0)
            pxa = layer.properties.get("parallax_x_max", 1.0)
            if layer.layer_type == TILE_LAYER_TYPE:
                logger.debug("layer {0}", layer.name)
                for tile_y in range(layer.height):
                    logger.debug("line {0}", tile_y)
                    for tile_x in range(layer.width):
                        pygame.event.pump()
                        tile = layer.data_yx[tile_y][tile_x]
                        if tile is None:
                            continue
                        pos = Point(tile.tile_x + tile.offset_x, tile.tile_y + tile.offset_y)
                        ti = tile.tile_info
                        img = self._get_cached_image(img_cache, ti)

                        kind = ti.properties.get("kind", None)
                        if kind == "grass":
                            spr = Sprite(img, pos, anchor="topleft", z_layer=idx,
                                         name=(ti.tileset.image_path_rel_to_cwd, ti.spritesheet_x, ti.spritesheet_y))
                            sprites.append(spr)
                        elif kind == "water":
                            spr = Sprite(img, pos, anchor="topleft", z_layer=idx,
                                         name=(ti.tileset.image_path_rel_to_cwd, ti.spritesheet_x, ti.spritesheet_y))
                            sprites.append(spr)
                        elif kind == "border":
                            spr = Sprite(img, pos, anchor="topleft", z_layer=idx,
                                         name=(ti.tileset.image_path_rel_to_cwd, ti.spritesheet_x, ti.spritesheet_y))
                            sprites.append(spr)
                        elif kind == "waterfall top":
                            spr = Sprite(img, pos, anchor="topleft", z_layer=idx,
                                         name=(ti.tileset.image_path_rel_to_cwd, ti.spritesheet_x, ti.spritesheet_y))
                            sprites.append(spr)
                        elif kind == "waterfall bottom":
                            spr = Sprite(img, pos, anchor="topleft", z_layer=idx,
                                         name=(ti.tileset.image_path_rel_to_cwd, ti.spritesheet_x, ti.spritesheet_y))
                            sprites.append(spr)
                        elif kind == "rock":
                            _log_tiled_sprite_needed("Map", layer.name, kind, ti.gid)
                        elif kind == "log":
                            spr.z_layer = LAYER_LOG
                            _log_tiled_sprite_needed("Map", layer.name, kind, ti.gid)
                        elif kind == "branch":
                            _log_tiled_sprite_needed("Map", layer.name, kind, ti.gid)
                        elif kind == "bridge":
                            _log_tiled_sprite_needed("Map", layer.name, kind, ti.gid)
                        elif kind == "gator back":
                            _log_tiled_sprite_needed("Map", layer.name, kind, ti.gid)
                        elif kind == "gator against":
                            _log_tiled_sprite_needed("Map", layer.name, kind, ti.gid)
                        elif kind == "swirl":
                            _log_tiled_sprite_needed("Map", layer.name, kind, ti.gid)
                        elif kind == "vine":
                            _log_tiled_sprite_needed("Map", layer.name, kind, ti.gid)
                        elif kind == "sea monster":
                            _log_tiled_sprite_needed("Map", layer.name, kind, ti.gid)
                        elif kind == "velocity":
                            spr = None
                            # spr = Sprite(img, pos, anchor="topleft", z_layer=idx,
                            #              name=(ti.tileset.image_path_rel_to_cwd, ti.spritesheet_x, ti.spritesheet_y))
                            # sprites.append(spr)
                            ent = BaseEntity(pos, KIND_VELOCITY, ti.tileset.tile_width, ti.tileset.tile_height, spr)
                            ent.position = ent.position + ent.half_bounds
                            v = Vec(float(ti.properties["speed"]), 0.0)
                            v.rotate(float(ti.properties["angle"]))
                            ent.velocity = v
                            self.space.add(ent)
                        elif kind == "hero":
                            spr = self._load_special_animation_sprite(idx, img_cache, pos, ti, scheduler)
                            spr.visible = True
                            sprites.append(spr)
                            self.hero_anim_normal.append(spr)
                        elif kind == "herojump":
                            spr = self._load_special_animation_sprite(idx, img_cache, pos, ti, scheduler)
                            spr.visible = False
                            sprites.append(spr)
                            self.hero_anim_jump.append(spr)
                        elif kind == "heroflip":
                            spr = self._load_special_animation_sprite(idx, img_cache, pos, ti, scheduler)
                            spr.visible = False
                            sprites.append(spr)
                            self.hero_anim_flip.append(spr)
                        elif kind == "heroflipped":
                            spr = self._load_special_animation_sprite(idx, img_cache, pos, ti, scheduler)
                            spr.visible = False
                            sprites.append(spr)
                            self.hero_anim_flipped.append(spr)
                        elif kind == "heroflipup":
                            spr = self._load_special_animation_sprite(idx, img_cache, pos, ti, scheduler, True)
                            # spr.flipped_y = True
                            spr.visible = False
                            sprites.append(spr)
                            self.hero_anim_flip_up.append(spr)
                        elif kind == "herosink":
                            spr = self._load_special_animation_sprite(idx, img_cache, pos, ti, scheduler, True)
                            # spr.flipped_y = True
                            spr.visible = False
                            sprites.append(spr)
                            self.hero_anim_sink.append(spr)
                        elif kind == "herosunken":
                            spr = self._load_special_animation_sprite(idx, img_cache, pos, ti, scheduler, True)
                            # spr.flipped_y = True
                            spr.visible = False
                            sprites.append(spr)
                            self.hero_anim_sunken.append(spr)
                        else:
                            raise Exception("unexpected kind={} in tile layer={}, gid={}".format(
                                kind, layer.name, ti.gid - 1))

                        text = ti.properties.get("text", None)
                        text_pos = pos + Vec(ti.tileset.tile_width / 2, ti.tileset.tile_height + 5)
                        self._create_text_sprite(sprites, text, text_pos)

            elif layer.layer_type == pytmxloader.OBJECT_LAYER_TYPE:
                for ot in layer.tiles:
                    ti = ot.tile_info
                    kind = ti.properties.get("kind", None)
                    if kind is None:
                        kind = ot.properties.get("kind", None)

                    x, y, w, h = ot.pixel_rect
                    pos = Point(x, y) + Vec(w // 2, h // 2)
                    if ti.animation:
                        spr = self._load_animation_sprite(idx, img_cache, pos, ti, scheduler)
                        spr.anchor = "center"
                        spr.start()
                    else:
                        img = self._get_cached_image(img_cache, ti)
                        # pf = random.uniform(pxm, pxa)
                        # parallax_factors = Vec2(pf, pf)
                        spr = Sprite(img, pos, anchor="center", z_layer=idx,
                                     name=(ti.tileset.image_path_rel_to_cwd, ti.spritesheet_x, ti.spritesheet_y))
                    spr.rotation = -ot.rotation
                    spr.use_sub_pixel = True
                    spr.kind = kind
                    sprites.append(spr)

                    if kind == "swirl":
                        spr.zoom = w / spr.image.get_size()[0]
                        swirl = BaseEntity(spr.position, KIND_SWIRL, w, h, spr)
                        swirl.radius = w / 2.0
                        duration = ot.properties.get("duration", 1.0)
                        swirl.duration = duration
                        self.space.add(swirl)

                        # animate sprite
                        self.tweener.create_tween(spr, "rotation", 0, sign(duration) * 360, abs(duration),
                                                  delta_update=True,
                                                  do_start=False).repeat(0).start()
                    elif kind == "gator":
                        direction = Vec(gator_head_distance, 0, 0)
                        direction.rotate(-spr.rotation)
                        gator = Gator(spr.position, direction, KIND_GATOR, w, h, spr, Timer(scheduler=scheduler))
                        gator.ccw_prob = ot.properties.get("ccwprob", 0.2)
                        gator.cw_prob = ot.properties.get("cwprob", 0.5)
                        self.space.add(gator)
                    elif kind == "pipeline":
                        spr.z_layer = LAYER_LOG
                    elif kind == "bridge":
                        spr.z_layer = LAYER_LOG
                    elif kind == "log":
                        spr.zoom = ot.width / 32.0
                        spr.z_layer = LAYER_LOG

                        points, normal = make_ent_data(ot)
                        edges = [LogEdge(pp, normal) for pp in points]
                        self._group_edges(edges)
                        self.space.adds(edges)
                    elif kind == "rocktile":
                        points, normal = make_ent_data(ot)
                        edges = [RockEdge(pp, normal) for pp in points]
                        self._group_edges(edges)
                        self.space.adds(edges)
                    else:
                        # if ot.name == 'ROCK':
                        #     logger.error('>>>>> GUMM {} {} {} {}', kind, layer.name, ot.name, ot.id)
                        #     logger.error('>>>>> {}', ot.properties.get('kind', None))
                        #     logger.error('>>>>> {}', ti.properties.get('kind', None))
                        #     quit()
                        pass
                        # raise Exception("unknown layer tile kind " + str(kind))

                for ot in layer.rectangles:
                    r = pygame.Rect(ot.x, ot.y, ot.width, ot.height)
                    # x, y, w, h = ot.pixel_rect
                    # pos = Point(x, y) + Vec(w//2, h//2)
                    kind = ot.properties.get("kind", None)
                    spr = None

                    if kind == "border":
                        logger.warning("TODO: Tiled Map object needs a sprite:  kind={}", kind)
                    elif kind == "start":
                        self.start_area = r
                        start_entity = BaseEntity(Point(r.centerx, r.centery), KIND_START_EDGE, r.w, r.h, None)
                        start_entity.collision_text = ot.properties.get("collision_text", None)
                        self.space.add(start_entity)
                    elif kind == "finish":
                        finish_entity = BaseEntity(Point(r.centerx, r.centery), KIND_FINISH, r.w, r.h, None)
                        finish_entity.collision_text = ot.properties.get("collision_text", None)
                        self.space.add(finish_entity)
                    elif kind == "waterfall":
                        ent = BaseEntity(Point(r.centerx, r.centery), KIND_WATERFALL, r.w, r.h, None)
                        ent.free_fall = int(ot.properties["height"])
                        ent.done = set()
                        self.space.add(ent)
                    elif kind == "waterfall top":
                        _log_tiled_sprite_needed("Rect", layer.name, kind, ot.object_id)
                    elif kind == "waterfall bottom":
                        _log_tiled_sprite_needed("Rect", layer.name, kind, ot.object_id)
                    elif kind == "rock":
                        _log_tiled_sprite_needed("Rect", layer.name, kind, ot.object_id)
                    elif kind == "rockedge":
                        pass
                    elif kind == "log":
                        spr = make_obj_rect_spr(ot, r, idx, "brown")
                        spr.z_layer = LAYER_LOG
                        points, normal = make_ent_data(ot)
                        edges = [LogEdge(pp, normal) for pp in points]
                        self._group_edges(edges)
                        self.space.adds(edges)
                    elif kind == "branch":
                        _log_tiled_sprite_needed("Rect", layer.name, kind, ot.object_id)
                    elif kind == "bridge":
                        _log_tiled_sprite_needed("Rect", layer.name, kind, ot.object_id)
                    elif kind == "checkpoint":
                        ent = BaseEntity(Point(r.centerx, r.centery), KIND_CHECKPOINT_EDGE, r.w, r.h, None)
                        ent.collision_text = ot.properties.get("collision_text", None)
                        self.space.add(ent)
                    elif kind == "message":
                        ent = BaseEntity(Point(r.centerx, r.centery), KIND_MESSAGE, r.w, r.h, None)
                        ent.collision_text = ot.properties.get("collision_text", None)
                        ent.sfx_play = ot.properties.get("sfx_play", None)
                        ent.sfx_fadeout = ot.properties.get("sfx_fadeout", None)
                        ent.rect = pygame.Rect(ent.aabb)
                        ent.rect.center = ent.position.x, ent.position.y
                        self.space.add(ent)
                    elif kind == "gatortailedge":
                        _log_tiled_sprite_needed("Rect", layer.name, kind, ot.object_id)
                    elif kind == "gatorjawedge":
                        _log_tiled_sprite_needed("Rect", layer.name, kind, ot.object_id)
                    elif kind == "swirl":
                        _log_tiled_sprite_needed("Rect", layer.name, kind, ot.object_id)
                    elif kind == "vine":
                        _log_tiled_sprite_needed("Rect", layer.name, kind, ot.object_id)
                    elif kind == "sea monster":
                        _log_tiled_sprite_needed("Rect", layer.name, kind, ot.object_id)
                    else:
                        # FIXME: bug? ot.id != Tiled ID  <<= What up with that?
                        raise Exception("unexpected kind={} in object layer={} name='{}' id={}".format(
                            kind, layer.name, ot.name, ot.id))

                    if spr is not None:
                        text = ot.properties.get("text", None)
                        text_pos = spr.position + Vec(0, ti.tileset.tile_height / 2 + 5)
                        self._create_text_sprite(sprites, text, text_pos)

                for ot in layer.poly_lines:
                    p_last = ot.pixel_points[0]
                    kind = ot.properties.get("kind", None)
                    normal = ot.properties.get("normal", "left")
                    for p in ot.pixel_points[1:]:
                        if kind == "border":
                            ent = WallEdge([p_last, p], normal)
                        elif kind == "rockedge":
                            ent = RockEdge([p_last, p], normal)
                        elif kind == "treebranchedge":
                            ent = TreeBranchEdge([p_last, p], normal)
                        else:
                            # FIXME: bug? ot.id != Tiled ID  <<= What up with that?
                            raise Exception("unexpected kind={} in object layer={} name='{}' id={}".format(
                                kind, layer.name, ot.name, ot.id))
                        self.space.add(ent)
                        p_last = p

        self.renderer.add_sprites(sprites)  # this is many times faster than adding single sprites (only one z sort)!

        _space_entities = self.space._entity_to_cells.keys()
        if len([ent for ent in _space_entities if ent.kind == KIND_FINISH]) != 1:
            raise Exception("no or too many finish(es) found!")

        if self.start_area is None:
            raise Exception("no start area found!")

        if not self.hero_anim_normal:
            raise Exception("Hero anim not present!")
        if not self.hero_anim_jump:
            raise Exception("Hero anim not present!")
        if not self.hero_anim_flip:
            raise Exception("Hero anim not present!")
        if not self.hero_anim_flipped:
            raise Exception("Hero anim not present!")
        if not self.hero_anim_flip_up:
            raise Exception("Hero anim not present!")
        if not self.hero_anim_sink:
            raise Exception("Hero anim not present!")
        if not self.hero_anim_sunken:
            raise Exception("Hero anim not present!")

        logger.info(">>> LOADING MAP {0} DONE", name)

    def _load_animation_sprite(self, layer, img_cache, position, ti, scheduler):
        frames = []
        fps = 1.0
        for af in ti.animation:
            img = self._get_cached_image(img_cache, af.tile_info)
            fps = 1.0 / (af.duration / 1000.0)
            frames.append(img)
        spr = SpriteAnimation(position, layer, frames, fps, scheduler)
        return spr

    def _load_special_animation_sprite(self, layer, img_cache, position, ti, scheduler, flip_y=False):
        frames = []
        fps = 1.0
        for af in ti.animation:
            img = self._get_cached_image(img_cache, af.tile_info)
            if flip_y:
                img = pygame.transform.flip(img, False, True)
            fps = 1.0 / (af.duration / 1000.0)
            frames.append(img)
        spr = SpecialAnimation(position, LAYER_HERO, frames, fps, scheduler)
        return spr

    def _group_edges(self, edges):
        first = edges[0]
        first.edges = set()
        for edge in edges:
            edge.first = first
            first.edges.add(edge)

    def _get_cached_image(self, img_cache, ti):
        key = (ti.tileset.image_path_rel_to_cwd, ti.spritesheet_x, ti.spritesheet_y, ti.flip_x, ti.flip_y, ti.angle)
        img = img_cache.get(key, None)
        if img is None:
            spritesheet = img_cache.get(ti.tileset.image_path_rel_to_cwd, None)
            if spritesheet is None:
                # noinspection PyUnresolvedReferences
                spritesheet = pygame.image.load(ti.tileset.image_path_rel_to_cwd).convert_alpha()
                img_cache[ti.tileset.image_path_rel_to_cwd] = spritesheet
            area = pygame.Rect(ti.spritesheet_x, ti.spritesheet_y, ti.tileset.tile_width, ti.tileset.tile_height)
            img = pygame.Surface(area.size, spritesheet.get_flags(), spritesheet)
            img.blit(spritesheet, (0, 0), area)
            img = pygame.transform.flip(img, ti.flip_x, ti.flip_y)
            if ti.angle != 0:
                img = pygame.transform.rotate(img, ti.angle)
            ip = ti.tileset.image_path_rel_to_cwd
            if "space1_0_4x_smooth.png" in ip:
                img = img.convert()

            if PIXEL_PPU != 1.0:
                if PIXEL_PPU == 2.0:
                    img = pygame.transform.scale2x(img)
                else:
                    _w, _h = img.get_size()
                    img = pygame.transform.smoothscale(img, (int(_w * PIXEL_PPU), int(_h * PIXEL_PPU)))

            img_cache[key] = img
        return img

    def _create_text_sprite(self, sprites, text, text_pos):
        if text is not None:
            ts = TextSprite(text, text_pos, z_layer=99, anchor="midtop")
            ts.name = str(id(ts))
            sprites.append(ts)
