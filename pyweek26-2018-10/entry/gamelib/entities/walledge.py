# -*- coding: utf-8 -*-

import logging

from gamelib import settings
from pyknic.generators import GlobalIdGenerator
from pyknic.mathematics import Point3 as Point, Vec3 as Vec

logger = logging.getLogger(__name__)
logger.debug("importing...")

_id_gen = GlobalIdGenerator()

__all__ = [
    "BaseEntity", "EdgeEntity",
    "StartEdge", "EndEdge", "WallEdge", "RockEdge", "LogEdge",
    "CheckpointEdge", "GatorJawEdge", "GatorTailEdge", "TreeBranchEdge", "BridgeEdge", "SeaMonsterEdge",
]


class BaseEntity(object):
    def __init__(self, pos_point2, kind, width, height, spr, unique_id=None):
        self.sprite = spr
        self.id = unique_id
        self.kind = kind
        self.position = pos_point2
        if self.id is None:
            self.id = _id_gen.next()
        self.half_bounds = Vec(width / 2, height / 2)
        self.width = width
        self.height = height

    def _get_aabb(self):
        x, y = self.position.as_xy_tuple(int)
        w = int(self.width)
        h = int(self.height)
        return x - w // 2, y - h // 2, w, h

    aabb = property(_get_aabb)

    def update(self, dt, simt):
        pass


class EdgeEntity(BaseEntity):
    kind = None

    def __init__(self, pixel_points, normal, spr=None, unique_id=None):
        assert len(pixel_points) == 2, "wrong number of pixel points: 2 != {0}".format(len(pixel_points))
        self.p0 = Point(*pixel_points[0])
        self.p1 = Point(*pixel_points[1])
        if normal == "left":
            self.normal = (self.p1 - self.p0).normal_right.normalized
        else:
            self.normal = (self.p1 - self.p0).normal_left.normalized
        xx = sorted(p[0] for p in pixel_points)
        yy = sorted(p[1] for p in pixel_points)
        width = xx[1] - xx[0]
        height = yy[1] - yy[0]
        pos_point2 = Point(xx[0] + width / 2, yy[0] + height / 2)
        BaseEntity.__init__(self, pos_point2, self.kind, width, height, spr, unique_id)


class StartEdge(EdgeEntity):
    kind = settings.KIND_START_EDGE


class EndEdge(EdgeEntity):
    kind = settings.KIND_END_EDGE


class WallEdge(EdgeEntity):
    kind = settings.KIND_WALL_EDGE


class RockEdge(EdgeEntity):
    kind = settings.KIND_ROCK_EDGE


class LogEdge(EdgeEntity):
    kind = settings.KIND_LOG_EDGE


class CheckpointEdge(EdgeEntity):
    kind = settings.KIND_CHECKPOINT_EDGE


class GatorJawEdge(EdgeEntity):
    kind = settings.KIND_GATOR_JAW_EDGE


class GatorTailEdge(EdgeEntity):
    kind = settings.KIND_GATOR_TAIL_EDGE


class TreeBranchEdge(EdgeEntity):
    kind = settings.KIND_TREE_BRANCH_EDGE


class BridgeEdge(EdgeEntity):
    kind = settings.KIND_BRIDGE_EDGE


class SeaMonsterEdge(EdgeEntity):
    kind = settings.KIND_SEA_MONSTER_EDGE
