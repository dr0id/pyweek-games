# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'gator.py' is part of codogupywk26
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Gator implementation.


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division

import logging
import random

from gamelib.eventing import event_dispatcher
from gamelib.settings import EVENT_COL_C_GATOR_HEAD, EVENT_COL_GATOR_TAIL, EVENT_GATOR_BITE, ACTION_JUMP
from pyknic.ai.statemachines import StateDrivenAgentBase, BaseState
from pyknic.generators import GlobalIdGenerator
from pyknic.mathematics import Vec3 as Vec

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2018"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["Gator"]  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")

_id_gen = GlobalIdGenerator()


class GatorBaseState(BaseState):

    @staticmethod
    def on_timer(owner):
        logger.error("gator on timer error!")

    @staticmethod
    def on_anim_end(owner):
        logger.error("gator anim end")


class Floating(GatorBaseState):

    @staticmethod
    def enter(owner):
        owner.timer.start(1.0, True)

    @staticmethod
    def exit(owner):
        owner.timer.stop()

    @staticmethod
    def on_timer(owner):
        z = random.random()
        if z < 0.33:
            if owner.sprite.current_index > 0:
                owner.sprite.set_current_index(owner.sprite.current_index - 1)
        elif z > 1 - 0.33:
            if owner.sprite.current_index < 2:
                owner.sprite.set_current_index(owner.sprite.current_index + 1)
        else:
            pass
        z = random.random()
        if z < owner.ccw_prob:  #0.2:
            owner.direction.rotate(15)
            owner.sprite.rotation -= 15
        elif z > 1 - owner.cw_prob:  #0.5:
            owner.direction.rotate(-15)
            owner.sprite.rotation += 15
        else:
            pass

    @staticmethod
    def handle_event(owner, event, *args):
        if event == EVENT_COL_C_GATOR_HEAD:
            owner.state_machine.switch_state(FloatUp)
        elif event == EVENT_COL_GATOR_TAIL:
            if not owner.bitten:
                owner.state_machine.switch_state(Dead)
                gator, kinds, canoe = args
                canoe.handle_event(ACTION_JUMP, canoe)


class Dead(GatorBaseState):

    @staticmethod
    def exit(owner):
        logger.error("gator is dead!")

    @staticmethod
    def enter(owner):
        owner.sprite.frames = list(reversed(owner.sprite.frames))
        owner.sprite.current_index = len(owner.sprite.frames) - 1 - owner.sprite.current_index
        owner.sprite.start()
        owner.sprite.event_animation_end += owner.on_anim_end

    @staticmethod
    def on_anim_end(owner):
        owner.sprite.visible = False
        owner.sprite.stop()
        owner.sprite.event_animation_end -= owner.on_anim_end


class FloatUp(GatorBaseState):

    @staticmethod
    def enter(owner):
        owner.sprite.event_animation_end += owner.on_anim_end
        owner.sprite.start()

    @staticmethod
    def exit(owner):
        owner.sprite.stop()
        owner.sprite.event_animation_end -= owner.on_anim_end

    @staticmethod
    def on_anim_end(owner):
        owner.state_machine.switch_state(Bite)
        owner.sprite.stop()


class Bite(GatorBaseState):

    @staticmethod
    def enter(owner):
        owner.timer.start(2.0)

    @staticmethod
    def exit(owner):
        owner.timer.stop()

    @staticmethod
    def on_timer(owner):
        owner.state_machine.switch_state(Sink)

    @staticmethod
    def handle_event(owner, event, *args):
        gator, kinds, canoe = args
        if event == EVENT_COL_C_GATOR_HEAD:
            event_dispatcher.fire(EVENT_GATOR_BITE, canoe, kinds, owner)
            owner.state_machine.switch_state(Sink)
            owner.bitten = True


class Sink(GatorBaseState):

    @staticmethod
    def enter(owner):
        owner.sprite.frames = list(reversed(owner.sprite.frames))
        owner.sprite.current_index = 0
        owner.sprite.start()
        owner.sprite.event_animation_end += owner.on_anim_end
        owner.timer.start(5)

    @staticmethod
    def exit(owner):
        owner.sprite.frames = list(reversed(owner.sprite.frames))
        owner.sprite.current_index = 0
        owner.sprite.event_animation_end -= owner.on_anim_end
        owner.sprite.stop()
        owner.timer.stop()

    @staticmethod
    def on_anim_end(owner):
        owner.sprite.stop()

    @staticmethod
    def handle_event(owner, event, *args):
        if not owner.sprite.is_running:
            if event == EVENT_COL_C_GATOR_HEAD:
                owner.state_machine.switch_state(FloatUp)

    @staticmethod
    def on_timer(owner):
        owner.state_machine.switch_state(Floating)


class Gator(StateDrivenAgentBase):

    def __init__(self, position, direction, kind, width, height, spr, timer, unique_id=None):
        StateDrivenAgentBase.__init__(self, Floating, self)
        self.direction = direction
        spr.stop()
        self.timer = timer
        self.timer.event_elapsed += self._on_timer
        self.timer.start(1.0, True)

        self.sprite = spr
        self.id = unique_id
        self.kind = kind
        self.position = position
        if self.id is None:
            self.id = _id_gen.next()
        self.half_bounds = Vec(width / 2, height / 2)
        self.width = width
        self.height = height
        self.bitten = False  # hack
        self.state_machine.event_switched_state += self._on_state_switched
        event_dispatcher.add_listener(EVENT_COL_C_GATOR_HEAD, self.handle_event)
        event_dispatcher.add_listener(EVENT_COL_GATOR_TAIL, self.handle_event)
        self.ccw_prob = 0.2
        self.cw_prob = 0.5

    def _get_aabb(self):
        r = self.sprite.rect.copy()
        r.center = self.position.as_xy_tuple()
        return r.left, r.top, r.w, r.h

    aabb = property(_get_aabb)

    def _on_timer(self, *args):
        self.state_machine.current_state.on_timer(self)

    def handle_event(self, event, *args):
        gator = args[0]
        assert isinstance(gator, Gator)
        if gator == self:
            self.state_machine.current_state.handle_event(self, event, *args)

    def on_anim_end(self, *args):
        self.state_machine.current_state.on_anim_end(self)

    def _on_state_switched(self, owner, prev_state, *args):
        logger.info("{2} gator state switch: {0} -> {1}", prev_state, self.state_machine.current_state, id(self))


logger.debug("imported")
