# -*- coding: utf-8 -*-
import pygame

from gamelib import pygametext, settings
from gamelib.context_game import Game
from gamelib.timestep_context import TimeSteppedContext
from pyknic.mathematics import Point3 as Point
from pyknic.pyknic_pygame.spritesystem import TextSprite, DefaultRenderer, Camera, Sprite


class Intro(TimeSteppedContext):

    def __init__(self):
        TimeSteppedContext.__init__(self)
        self.renderer = DefaultRenderer()
        self.cam = None
        self.sprites = []
        self.spr_idx = 0

    def enter(self):
        TimeSteppedContext.enter(self)
        pygametext.FONT_NAME_TEMPLATE = 'data/font/%s'
        img_easy = pygametext.getsurf("easy", **settings.font_themes['intro'])
        img_med = pygametext.getsurf("medium", **settings.font_themes['intro'])
        img_hard = pygametext.getsurf("hard", **settings.font_themes['intro'])
        img_quit = pygametext.getsurf("quit", **settings.font_themes['intro'])
        screen_rect = pygame.display.get_surface().get_rect()
        self.cam = Camera(screen_rect)
        images = [img_easy, img_med, img_hard, img_quit]
        d = screen_rect.h // (len(images) + 1)
        c = screen_rect.w // 2
        for i, t in enumerate(images):
            spr = Sprite(t, Point(c, d * i + d))
            self.renderer.add_sprite(spr)
            self.sprites.append(spr)
            spr.difficulty = i + 1
        self.cam.position = Point(screen_rect.centerx, screen_rect.centery)
        self.sprites[0].zoom = 2.0


    def update_step(self, delta, sim_time, *args):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.pop(10)
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self.pop(10)
                elif event.key in (pygame.K_SPACE, pygame.K_RETURN):
                    idx = self.spr_idx
                    self.start_game(idx)
                else:
                    self.sprites[self.spr_idx].zoom = 1.0
                    self.spr_idx += 1
                    self.spr_idx %= len(self.sprites)
                    self.sprites[self.spr_idx].zoom = 2.0
            elif event.type == pygame.MOUSEBUTTONDOWN:
                found = self.renderer.get_sprites_at_tuple(event.pos)
                if found:
                    self.start_game(self.sprites.index(found[0]))
            elif event.type == pygame.MOUSEMOTION:
                found = self.renderer.get_sprites_at_tuple(event.pos)
                if found:
                    self.sprites[self.spr_idx].zoom = 1.0
                    self.spr_idx = self.sprites.index(found[0])
                    self.sprites[self.spr_idx].zoom = 2.0


    def start_game(self, idx):
        self.pop()
        difficulty = self.sprites[idx].difficulty
        if difficulty < 4:
            self.push(Game(settings.startup_level, difficulty))
        else:
            self.pop(10)

    def draw(self, screen, do_flip=True, interpolation_factor=1.0):
        self.renderer.draw(screen, self.cam, fill_color=(0, 0, 0), do_flip=True)
