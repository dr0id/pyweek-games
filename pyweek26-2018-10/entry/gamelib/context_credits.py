# -*- coding: utf-8 -*-
import pygame

from pyknic.context import Context


class Credits(Context):

    def __init__(self):
        Context.__init__(self)

    def update(self, delta_time, sim_time):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.pop()
