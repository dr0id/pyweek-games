# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The skellington package contains the skellington execution code along with a compatibility module.

"""
from __future__ import print_function, division

# NOTE: no imports on this level to minimize the possibilities of exceptions, all imports are done in a try...except

__version__ = '2.0.3.1'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2016"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# list of public visible parts of this module
__all__ = [
    "run_main"
]

# NOTE: no imports on this level to minimize the possibilities of exceptions, all imports are done in a try...except

record_format = "%(relativeCreated)d [%(processName)s %(process)d %(threadName)-10s %(thread)d]: " \
                    "%(levelname)-8s %(message)s [%(name)s: %(funcName)s in %(filename)s(%(lineno)d)]"
date_format = None
stream_std_handler_name = 'std_out_stream'


def configure_logging(log_file_name):
    import os
    import logging.config
    path = os.getcwd()
    file_mode = 'a'
    max_bytes = 5 * 1024 * 1024  # 5 MB
    backup_count = 5
    encoding = 'utf8'
    delayed_logging = 1  # delayed open of stream to when first time it is written to, the file is opened here already

    file_handler_name = 'skellington_file'
    formatter_name = 'record_format'
    log_config = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            formatter_name: {
                'format': record_format,
                'datefmt': date_format
            },
        },
        'handlers': {
            file_handler_name: {
                'level': logging.NOTSET,
                'class': 'logging.handlers.RotatingFileHandler',
                'formatter': formatter_name,
                'filename': os.path.join(path, log_file_name),
                'mode': file_mode,
                'maxBytes': max_bytes,
                'backupCount': backup_count,
                'encoding': encoding,
                'delay': delayed_logging,
            },
        },
        'loggers': {
            '': {
                'level': logging.DEBUG,
                'handlers': [
                    file_handler_name,
                ]
            },
        }
    }

    logging.config.dictConfig(log_config)
    logging.captureWarnings(True)  # redirect all warnings to the logs!
    return log_config


def log_log_config(log_config):
    import logging
    logger = logging.getLogger(__name__)
    logger.info("Logging configuration used:\n")
    import pprint
    from .compatibility import StringIO
    str_buffer = StringIO()
    pprint.pprint(dict(log_config), stream=str_buffer)
    logger.info(str_buffer.getvalue())


class StreamToLogger(object):

    def __init__(self, logger_name, level_to_use):
        import logging
        self._logger = logging.getLogger(logger_name)
        # self._logger.setLevel(level_to_use)
        self._level = level_to_use

    def write(self, message):
        self._logger.log(self._level, message.rstrip('\r\n'))

    def writelines(self, sequence_of_strings):
        for message in sequence_of_strings:
            self.write(message)

    def flush(self, *args, **kwargs):
        pass


class ChainedException(Exception):

    def __init__(self, handling_ex, inner_exception=None):
        import sys
        Exception.__init__(self, repr(handling_ex), handling_ex)
        # self.message = repr(handling_ex)
        self.handling_ex = handling_ex
        self.inner_exception = inner_exception
        self.traceback = sys.exc_info()[2]

    def __str__(self):
        import traceback
        from .compatibility import StringIO

        output = StringIO()

        new_line = "\n"
        current = self
        while current is not None:
            # output.write(new_line)
            output.write(repr(current) + new_line)
            output.write(new_line)
            traceback.print_tb(current.traceback, file=output)
            if isinstance(current.inner_exception, ChainedException):
                output.write("----- inner exception -----")
                current = current.inner_exception
            else:
                current = None

        return output.getvalue()


def run_main(function_to_run, log_to_console, fallback_file_name, process_name=None):
    sys = None
    traceback = None
    try:
        import sys
        import traceback

        try:
            from . import compatibility  # checks for compatible python version
            import logging
            log_config = configure_logging(fallback_file_name)
            logger = logging.getLogger(__name__)

            try:
                try:
                    setup_std_stream_logger()
                    if log_to_console:
                        setup_std_stream_handler()

                    if process_name is not None:
                        import multiprocessing
                        multiprocessing.current_process().name = process_name

                    log_header(logger)
                    log_log_config(log_config)

                    function_to_run()

                except Exception as ex:
                    raise ChainedException(ex)
            except Exception as _original:
                try:
                    if not logging.getLogger().handlers:
                        raise ChainedException(Exception("no logging handlers found!"), _original)

                    logger.error(str(_original))
                    sys.exit(1)
                except Exception as _while_handling:
                    raise ChainedException(_while_handling, _original)

        except Exception as log_ex:
            try:
                with open(fallback_file_name, 'a') as log_file:
                    log_file.write(str(log_ex))
                sys.exit(1)
            except Exception as _log_ex:
                raise ChainedException(_log_ex, log_ex)

    except Exception as sub_ex:
        # trying to catch any exceptions and print them out in a way that the user will see them!
        new_line = '\n'
        sys.__stderr__.write(new_line)
        if hasattr(sub_ex, 'inner_exception'):
            sys.__stderr__.write(str(sub_ex) + new_line)
        else:
            traceback.print_exc(file=sys.__stderr__)
        sys.__stderr__.flush()
        sys.exit(1)


def log_header(logger):
    import sys
    log_separator_width = 50
    logger.info("-+" * log_separator_width)
    logger.info("using python version: " + sys.version)


def setup_std_stream_logger():
    import sys
    import logging
    sys.stdout = StreamToLogger("STDOUT", logging.INFO)  # redirect stdout to STDOUT logger
    sys.stderr = StreamToLogger("STDERR", logging.ERROR)  # redirect stdout to STDERR logger


def setup_std_stream_handler():
    import logging
    import sys

    class StdFilter(logging.Filter):

        def __init__(self, name, max_log_level=logging.WARNING):
            logging.Filter.__init__(self, name)
            self._max_log_level = max_log_level
            self.level = logging.NOTSET

        def filter(self, record):

            if record.levelno <= self._max_log_level:
                return 1
            return 0  # don't log record

    std_out_filter = StdFilter("std_out_filter")
    formatter = logging.Formatter(record_format, date_format)
    root_logger = logging.getLogger()

    stream_std_handler = logging.StreamHandler(sys.__stdout__)
    stream_std_handler.setFormatter(formatter)
    stream_std_handler.name = stream_std_handler_name
    stream_std_handler.level = logging.NOTSET  # log everything...
    stream_std_handler.addFilter(std_out_filter)  # ...up to warning
    root_logger.addHandler(stream_std_handler)  # log to stdout

    stream_err_handler = logging.StreamHandler(sys.__stderr__)
    stream_err_handler.setFormatter(formatter)
    stream_err_handler.name = "err_out_stream"
    stream_err_handler.level = logging.ERROR  # log error and above
    root_logger.addHandler(stream_err_handler)  # log to stderr
