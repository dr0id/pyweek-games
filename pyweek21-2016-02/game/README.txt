﻿Detention: The Aftermath
========================


This game has been written for pyweek 21: https://www.pyweek.org/21/

The theme was 'Aftermath' and here it is.


CONTACT:

Homepage: https://bitbucket.org/dr0id/pyweek-games/src/1b28dfcffc4d25b2ab672effed02836ace9a2d86/pyweek21-2016-02/?at=default
Name: Detention: The Aftermath
Team: DRummb0ID21
Members: Gummbum, DR0ID


DEPENDENCIES:

python 2.7+ - python 3.4    (python 3.5 does not work!)
pygame 1.9.2a+ (pygame 1.9.1 might work, not sure)
numpy (tested with 1.8.1)

You might need to install some of these before running the game:

  Python:     http://www.python.org/
  PyGame:     http://www.pygame.org/


RUNNING THE GAME:

On Windows or Mac OS X, locate the "run_game.pyw" file and double-click it.

Othewise open a terminal / console and "cd" to the game directory and run:

  python run_game.py



HOW TO PLAY THE GAME:

Move the cursor around the screen with the mouse.

Press the left mouse button to fire the ducks.



LICENSE:

This game skellington has same license as pyknic.

Fonts:

    DJB Chalk It Up.ttf         http://www.fontspace.com/category/chalkboard?p=2


========= remove from here ===================================================

FOR DEVELOPERS:

- put your code into the gamelib/main.py
- put your data (images, sound, etc.) into data/
- the working directory is set to the directory where the run_game.py is
- fill out this readme.txt so it describes your game
- remove the unwanted run_game* files (run_game_debug.py should be removed also the py2exe script)
- use the make_exe.py (uses py2exe) script at own risk
  - fill in the fields in the make_exe.py
  - run it (it will include the pygame *.dll files and default font file)


