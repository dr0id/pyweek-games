#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
TODO: docstring
"""

import sys
import os
import subprocess


# run in right directory
import traceback

if not sys.argv[0]:
    app_dir = os.path.abspath(os.path.dirname(__file__))
else:
    app_dir = os.path.abspath(os.path.dirname(sys.argv[0]))
os.chdir(app_dir)
# make sure that sub modules in gamelib are imported correctly
app_dir = os.path.join(app_dir, 'gamelib')
if app_dir not in sys.path:
    sys.path.insert(0, app_dir)

mode = 'a'


def run_optimized():
    if __debug__:
            sys.stdout.write("see out.txt!")
            with open('out.txt', mode) as out_file:
                try:
                    sys.stderr = out_file
                    sys.stdout = out_file
                    # start subprocess
                    sys.stdout.write("argv: " + str(sys.executable) + '\n')
                    sub_process_args = [str(sys.executable)+"", "-OO", str(__file__)]
                    for arg in sys.argv[1:]:
                        sub_process_args.append(arg)
                    sys.stdout.write("exec subprocess: " + str(sub_process_args) + '\n')
                    sys.stdout.flush()
                    sys.stderr.flush()
                    subprocess.call(sub_process_args)
                except Exception as ex:
                    # trying to catch any exceptions and print them out in a way that the user will see them!
                    sys.stderr.write("Error:" + '\n')
                    sys.stderr.write(str(ex))
                    sys.stderr.write('\n')
                    exc_type, exc_value, exc_traceback = sys.exc_info()
                    traceback.print_tb(exc_traceback, file=sys.stderr)
                    sys.stderr.write('\n')
                    raise
    else:
        # running optimized
        with open("opt-out.txt", mode) as o_out_file:
            sys.stderr = o_out_file
            sys.stdout = o_out_file
            # noinspection PyUnresolvedReferences
            import main
            try:
                main.main()
            except Exception as ex:
                sys.stderr.write("Error:" + '\n')
                sys.stderr.write(str(ex))
                sys.stderr.write('\n')
                exc_type, exc_value, exc_traceback = sys.exc_info()
                traceback.print_tb(exc_traceback, file=sys.stderr)
                sys.stderr.write('\n')


if __name__ == '__main__':
    run_optimized()
