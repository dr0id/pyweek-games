This font was created using scans of a friend's handwriting. She liked it so much I ended up making light and bold versions, and now it's gotta go up on the site!

This font is completely free. All I ask is that you come back to FontParty for all your font needs!

Enjoy!

SaraRachelle & Shayne aka ZebraFlames


Visit FontParty: http://www.FontParty.com
And Shayne's site: http://www.geocities.com/silver_flame67
