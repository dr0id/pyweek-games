# -*- coding: utf-8 -*-
"""
TODO: docstring
"""
from __future__ import division, print_function

import gettext
import logging
import os
import sys

import pygame

import pyknicpygame

import contextintro
import contextlevelselection
import contextcredits
import settings
import gameresource
import sound

# do not use __file__ because it is not set if using py2exe

# TODO: write an application class that has a run method, register subsystem like music... hmm subsystem registry?

# call init before using any of the pyknicpygame/pyknic modules!!
# TODO:  put here the configuration values, otherwise you have to always look it up
# TODO: add pygame configuration values too to the config
pyknicpygame.init()

# import first context
# import sound
# import gameplay

logger = logging.getLogger("pyknic.main")
logger.setLevel(pyknicpygame.pyknic.settings.log_logger_level)
# if __debug__:
#     if 'log_console_handler' in pyknicpygame.pyknic.settings:
#         logger.addHandler(pyknicpygame.pyknic.settings['log_console_handler'])

# TODO: move this path to application class, check first what this actually does or is noeeded for!?
pyknicpygame.pyknic.settings.appdir = sys.path[0]
gettext.install(settings.DOMAIN_NAME, pyknicpygame.pyknic.settings.appdir, 1)


def main():
    try:
        _main()
    except Exception as e:
        # TODO: log error as well!
        logger.exception("Oh no! Something unexpected happened. We are extremely sorry!")
        logger.fatal("previous error was fatal, terminating...")


def _main():
    # TODO: this should go into config
    logging.basicConfig(level=logging.DEBUG)

    # TODO: this should go into config
    os.environ['SDL_VIDEO_CENTERED'] = '1'
    # TODO: this should go into config
    pygame.mixer.pre_init(frequency=settings.MIXER_FREQUENCY, buffer=settings.MIXER_BUFFER_SIZE)
    pygame.init()
    # TODO: this should go into param of init method
    pygame.display.set_caption(settings.CAPTION)
    # TODO: this should go into param of init method
    icon = pygame.image.load("icon.png")
    # TODO: this should go into pyknicpygame init method
    pygame.display.set_icon(icon)
    pygame.mixer.set_reserved(2)

    # TODO: this should go into config or param values
    screen = pygame.display.set_mode(settings.SCREEN_SIZE, settings.FLAGS, settings.BIT_DEPTH)

    clock = pygame.time.Clock()
    settings.clock = clock

    # TODO: should this be done by the context entry/exit (unload)?
    # Load default resources
    gameresource.GameResource.load()

    # TODO: this should go into param of init method (first context)
    # Load overarching game flow
    context = contextcredits.CreditsContext()
    pyknicpygame.pyknic.context.push(context)
    context = contextlevelselection.LevelSelectionContext()
    pyknicpygame.pyknic.context.push(context)
    context_intro = contextintro.ContextIntro()
    pyknicpygame.pyknic.context.push(context_intro)

    # TODO: this should go into application class with a run() method
    lock_stepper = pyknicpygame.pyknic.timing.LockStepper()

    pyknicpygame.pyknic.context.set_deferred_mode(True)  # avoid routing problems, do stack operations only at update
    context_len = pyknicpygame.pyknic.context.length
    context_top = pyknicpygame.pyknic.context.top
    context_update = pyknicpygame.pyknic.context.update

    lock_stepper.event_integrate.add(lambda ls, dt, simt: context_top().update(dt) if context_top() else None)

    # if __debug__:
    scheduler = pyknicpygame.pyknic.timing.Scheduler()
    scheduler.schedule(_print_fps, 2, 0, clock)
    lock_stepper.event_integrate.add(lambda ls, dt, simt: scheduler.update(dt))
    alpha = 1.0

    while context_len():
        # limit the fps
        dt = clock.tick() / 1000.0  # convert to seconds
        # if settings.PLAY_MUSIC:
        #     # loop intro song while playing intro
        #     sound.roll_songs(context_top() is context_intro)
        context_top().draw(screen, do_flip=True, interpolation_factor=alpha)
        # alpha = lock_stepper.update(dt, timestep_seconds=settings.SIM_TIME_STEP)
        lock_stepper.update(dt, timestep_seconds=settings.SIM_TIME_STEP)
        context_update()
        # if __debug__:
        #     fps = clock.get_fps()
        #     # if fps < 60:
        #     logger.debug('fps: %s, %s', str(fps), alpha)

    try:
        pygame.mixer.music.fadeout(2000)
        while pygame.mixer.music.get_busy():
            pygame.time.wait(100)
    except IndexError:
        pass

    pygame.quit()


def _print_fps(clock):
    if settings.PRINT_FPS:
        fps = clock.get_fps()
        logger.debug('fps: %s', str(fps))
    return 1  # return next interval


# this is needed in order to work with py2exe
if __name__ == '__main__':
    main()
