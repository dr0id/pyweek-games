# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek21-2016-02
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function
import os
import random

import pygame

import ptext

__version_number__ = (0, 0, 0, 0)
__version__ = ".".join([str(num) for num in __version_number__])

__author__ = 'dr0iddr0id {at} gmail [dot] com'
__copyright__ = "DR0ID @ 2016"
__credits__ = ["DR0ID", "Gummbum"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

DOMAIN_NAME = "pyweek21-The Aftermath"  # used for translation files

###############################################################################
#
# for tuning
#
###############################################################################

# Some people have trouble with numpy. It will fail gracefully if not found.
use_numpy = True

# touch game/nomusic.txt to disable music locally without distributing the setting
PLAY_MUSIC = not os.access('nomusic.txt', os.F_OK)
MUSIC_VOLUME = 0.4
SFX_VOLUME = 1.0

# Adjust the delay for timed dialogue to accommodate your reading speed. The value
# is seconds per word. A higher value causes dialogue to display longer.
# The recommended general value is 0.7 (out of respect for those whose first
# language is not English). For native English speakers 0.6 might be a good value.
DIALOGUE_SECONDS_PER_WORD = 0.7

# Note: graphics are not scaled, so you probably don't want to change these.
SCREEN_WIDTH = 1024
SCREEN_HEIGHT = 700  # 768

PRINT_FPS = False

CHEATS_ENABLED = os.access('cheats_enabled.txt', os.F_OK)

###############################################################################
#
# end tuning
#
###############################################################################

ptext.FONT_NAME_TEMPLATE = "data/font/%s.ttf"
FONT_SAY = 'Chalkboard by Marta van Eck'  # 'raygun'
FONT_SPLASH = 'CFMarieEve'

SCREEN_SIZE = (SCREEN_WIDTH, SCREEN_HEIGHT)
CAPTION = "Detention: The Aftermath"
FLAGS = pygame.DOUBLEBUF
BIT_DEPTH = 32

if FLAGS & pygame.FULLSCREEN:
    SCREEN_HEIGHT = 768

numpy = None
if use_numpy:
    try:
        import numpy
    except ImportError:
        pass

if numpy:
    HILIGHT_FONT_COLOR = dict(color='gold', scolor='black', shadow=(1, 1), owidth=1, gcolor='tomato')
    LOLIGHT_FONT_COLOR = dict(color='tomato', scolor='black', shadow=(1, 1), owidth=1, gcolor='gold')
    # Not sure which one I like better! - Gumm
    SPLASH_FONT_COLOR = dict(color='mediumslateblue', scolor='slateblue4', gcolor='slateblue4', shadow=(1, 1))
    # SPLASH_FONT_COLOR = dict(color='mediumslateblue', ocolor='slateblue4', gcolor='white', owidth=1)
else:
    HILIGHT_FONT_COLOR = dict(color='gold', scolor='black', shadow=(1, 1), owidth=1)
    LOLIGHT_FONT_COLOR = dict(color='tomato', scolor='black', shadow=(1, 1), owidth=1)
    # Not sure which one I like better! - Gumm
    SPLASH_FONT_COLOR = dict(color='mediumslateblue', scolor='slateblue4', shadow=(1, 1))
    # SPLASH_FONT_COLOR = dict(color='mediumslateblue', ocolor='slateblue4', owidth=1)
SAY_FONT_COLOR = dict(color='white')
BLACKBOARD_TITLE_FONT_COLOR = dict(color='white', scolor='mediumslateblue', shadow=(1, 1), owidth=1)
SAY_CHOICES_FONT_COLOR = dict(color='palevioletred', owidth=0.25, ocolor='palevioletred')

MIXER_FREQUENCY = 0
MIXER_BUFFER_SIZE = 128

VOLUME_CHANGE = 0.005

SIM_TIME_STEP = 0.02  # 50 fps

DIALOGUE_AREA = pygame.Rect(
    SCREEN_WIDTH // 2 - 768 // 2 + 140, SCREEN_HEIGHT - 402 + 60,
    768 - 280, 432 - 60
)
DIALOGUE_LEFT_AREA = pygame.Rect(
    DIALOGUE_AREA.x, DIALOGUE_AREA.y,
    DIALOGUE_AREA.w * 7 / 10, DIALOGUE_AREA.h
)
DIALOGUE_RIGHT_AREA = pygame.Rect(
    DIALOGUE_AREA.x + DIALOGUE_AREA.w * 3 / 10, DIALOGUE_AREA.y,
    DIALOGUE_AREA.w * 7 / 10, DIALOGUE_AREA.h
)

COMBAT_AREA_BORDER = 30
_border = 50
COMBAT_AREA = pygame.Rect(SCREEN_WIDTH // 8, SCREEN_HEIGHT // 8 + _border, SCREEN_WIDTH // 4 * 3,
                          SCREEN_HEIGHT // 4 * 3 - _border)
COMBAT_AREA_ROW_DISTANCE = 10
COMBAT_AREA_CHOICE_DISTANCE = 3
COMBAT_GUY_HEIGHT = 140
COMBAT_AREA_CHOICES_HIGHLIGHT_ZOOM = 1.5
COMBAT_AREA_CHOICES_DEFAULT_ZOOM = 1.0
COMBAT_AREA_TEXT_COLOR = (242, 242, 242)
COMBAT_AREA_DEBUG_RENDER_ON = True and CHEATS_ENABLED
COMBAT_LEAVE_DELAY = 2  # seconds
COMBAT_CHOICE_TRAVEL_DURATION = 0.5  # seconds
COMBAT_BAD_ATTACK_SHAKE_DURATION = 0.5  # seconds
COMBAT_SHAKE_DISTANCE_RANGE = (-10, 10)

DIALOGUE_AREA_CHOICES_SPACING = 4
DIALOGUE_AREA_CHOICES_HIGHLIGHT_ZOOM = 1.0
DIALOGUE_AREA_CHOICES_DEFAULT_ZOOM = 0.9
DIALOGUE_ADD_NAME = False

LEFT_MOUSE_BUTTON = 1

PLAYER_SPEED = 120  # kmh!!
PLAYER_IMAGE_CHANGE_DELAY = 0.3  # seconds
PLAYER_RADIUS = 25
PLAYER_DIST_TO_TARGET = 50.0

ZOMBIE_ANIMATION_INTERVAL = 0.3
ZOMBIE_SPEED = 100
ZOMBIE_ROTATION_SPEED = 90
ZOMBIE_WAIT_STATE_TO_MOVE_STATE_CHANCE = 0.05
ZOMBIE_DIRECTION_STATE_TO_WAIT_STATE_CHANCE = 0.1
ZOMBIE_MOVE_STATE_TO_WAIT_STATE_CHANCE = 0.1
ZOMBIE_MOVE_RANGE = (10, 300)
ZOMBIE_ROTATION_RANGE = (-90, 90)  # angle in degrees
ZOMBIE_WAIT_DURATION_RANGE = (1, 4)  # in seconds

CLASS_ROOM_BORDER = 10

ERASE_EFFECT_DURATION = 1.0

# increase this better rotation caching; but clearing the cache makes the game hiccup;
# lower it if the game periodically freezes for a split second for no apparent reason
# SPRITE_MAX_CACHE_IMAGE_COUNT = 10000
SPRITE_CACHE_MAX_MEMORY = 1.5 * 2 ** 30
SPRITE_CACHE_DEFAULT_EXPIRATION = 15.0
SPRITE_CACHE_AT_MOST = 50
SPRITE_CACHE_AUTO_TUNE_AGING = False
SPRITE_CACHE_TUNE_MIN_PERCENT = 94.0
SPRITE_CACHE_TUNE_MAX_PERCENT = 95.0
SPRITE_CACHE_TUNE_STEP = 0.01

UUID_FOR_RANDOM = '1e5c96a9-7bb9-49f1-8c4c-2c9ee9e63f0f'
random.seed(UUID_FOR_RANDOM)  # make random repeatable

# Resources
FILENAME_GAME_PROGRESS = 'progress.json'

# Keys

if CHEATS_ENABLED:
    KEY_CHEAT_EXIT = pygame.K_F10
    KEY_CHEAT_COMBAT_TEST = pygame.K_F9
    KEY_CHEAT_LEVEL_SELECTION = pygame.K_F8
    KEY_CHEAT_DIALOGUE_SELECTION = pygame.K_1, pygame.K_2, pygame.K_3, pygame.K_4, pygame.K_5
else:
    KEY_CHEAT_EXIT = ''
    KEY_CHEAT_COMBAT_TEST = ''
    KEY_CHEAT_LEVEL_SELECTION = ''
    KEY_CHEAT_DIALOGUE_SELECTION = ['']
KEY_SKIP_DIALOGUE = pygame.K_SPACE, pygame.K_RETURN
