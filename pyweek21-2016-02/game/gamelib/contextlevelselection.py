﻿# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek21-2016-02
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import pygame

import gameprogress
import script
import settings
import sound
from contextalarm import AlarmContext
from contextgameplay import GamePlayContext
from effecterase import EraseEffect
from gameresource import GameResource
from levels import PlayerMathSanityData
from pyknicpygame import spritesystem
from pyknicpygame.pyknic.context import Context
from pyknicpygame.pyknic.context.transitions import PopTransition
from pyknicpygame.pyknic.mathematics import Point2

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2016"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 


class SelectableSprite(spritesystem.Sprite):

    door_open_channel = None
    door_close_channel = None

    def __init__(self, image, hover_image, action, door_id, is_locked, position, is_repeat, anchor=None):
        spritesystem.Sprite.__init__(self, image, position, anchor)

        self._is_repeat = is_repeat
        self._hover_image = hover_image
        self._normal_image = image
        self._action = action
        self._is_locked = is_locked
        self._is_hovered = False
        self._door_id = door_id

        if SelectableSprite.door_open_channel is None:
            SelectableSprite.door_open_channel = pygame.mixer.Channel(0)
            SelectableSprite.door_close_channel = pygame.mixer.Channel(1)

    def mouse_over(self, is_mouse_over):
        if self._is_hovered != is_mouse_over:
            self._is_hovered = is_mouse_over
            if self._is_hovered:
                self._mouse_enter()
            else:
                self._mouse_leave()

    def do_action(self):
        if not self._is_locked:
            self._action(self._door_id, self._is_repeat)
        else:
            # GameResource.get(GameResource.SOUND_DOOR_LOCKED).play()
            GameResource.get(GameResource.SOUND_DOOR_CLOSED).play()

    def _mouse_enter(self):
        if not self._is_locked:
            self.set_image(self._hover_image)
            # print('==== mouse enter ====')
            if not SelectableSprite.door_open_channel.get_busy():
                # print('---- mouse enter (door open) ----')
                SelectableSprite.door_open_channel.play(GameResource.get(GameResource.SOUND_DOOR_OPENED))
        else:
            # GameResource.get(GameResource.SOUND_DOOR_LOCKED).play()
            if not SelectableSprite.door_close_channel.get_busy():
                # print('---- mouse enter (door closed) ----')
                SelectableSprite.door_close_channel.play(GameResource.get(GameResource.SOUND_DOOR_CLOSED))

    def _mouse_leave(self):
        self.set_image(self._normal_image)
        if not self._is_locked:
            if not SelectableSprite.door_close_channel.get_busy():
                # print('---- mouse leave (door close) ----')
                SelectableSprite.door_close_channel.play(GameResource.get(GameResource.SOUND_DOOR_CLOSED))


class LevelSelectionContext(Context):
    def __init__(self):
        self.renderer = spritesystem.DefaultRenderer()
        screen_rect = pygame.Rect(0, 0, settings.SCREEN_WIDTH, settings.SCREEN_HEIGHT)
        self.cam = spritesystem.Camera(screen_rect)
        self.cam.position = Point2(settings.SCREEN_WIDTH // 2, settings.SCREEN_HEIGHT // 2)

        self._over_sprite = None
        self._selected_sprite = None
        self.level = None
        self.player_sanity_data = PlayerMathSanityData()

    def enter(self):
        self._load_sprites_according_to_progress()

        progress = gameprogress.progress.current_level
        if progress < 7:
            intro_script = script.winner_script(progress - 1)
            self.push(intro_script)

    def _load_sprites_according_to_progress(self):
        self.renderer.clear()

        gameprogress.progress.load()
        img_hallway = GameResource.get(GameResource.IMG_HALLWAY)
        spr = spritesystem.Sprite(img_hallway, Point2(0, 0), anchor='topleft', z_layer=-5)
        self.renderer.add_sprite(spr)
        img_blackboard = GameResource.get(GameResource.IMG_HALLWAY_BLACKBOARD)
        spr = spritesystem.Sprite(img_blackboard, Point2(0, 0), anchor='topleft', z_layer=-10)
        self.renderer.add_sprite(spr)
        for rect_tuple, res_ids in GameResource.hallway_door_positions.items():
            img_closed = GameResource.get(res_ids[0])
            img_opened = GameResource.get(res_ids[1])
            position = Point2(rect_tuple[0], rect_tuple[1])
            door_id = res_ids[2]
            is_repeat = False
            if door_id == 0:
                is_level_locked = False
            else:
                is_level_locked = False
                if door_id > gameprogress.progress.current_level:
                    # locked doors
                    img_opened = img_closed
                    is_level_locked = True
                elif door_id < gameprogress.progress.current_level:
                    # already beaten level, repeating it
                    img_closed = img_opened
                    is_repeat = True
                else:
                    # current door
                    pass

            spr = SelectableSprite(img_closed, img_opened, self._handle_click_action, door_id, is_level_locked,
                                   position, is_repeat, anchor='topleft')
            self.renderer.add_sprite(spr)

    def _handle_click_action(self, door_id, is_repeat):
        # TODO: sfx enter or leave room
        if door_id == 0:  # exit door
            self.push(PopTransition(EraseEffect()))
        else:
            self.level = GamePlayContext(door_id, self.player_sanity_data, is_repeat)
            if door_id == 6:  # last level
                self.push(AlarmContext(self.level))
            self.push(self.level)
            self.replaying_beaten_level = self.was_level_repeated()

    def resume(self):
        if self.level:
            self._play_after_level_script(self.level.won)
            if self.level.won:
                self._load_sprites_according_to_progress()
            self.level = None

    def was_level_repeated(self):
        if self.level:
            return self.level.is_repeat
        return False

    def _play_after_level_script(self, has_won):
        if self.replaying_beaten_level:
            script_obj = script.replay_script(self.level.level_index)
        elif has_won:
            script_obj = script.winner_script(self.level.level_index)
        else:
            script_obj = script.loser_script(self.level.level_index)
        self.push(script_obj)

    def exit(self):
        gameprogress.progress.save()

    def draw(self, screen, do_flip=True, interpolation_factor=1.0):
        self.renderer.draw(screen, self.cam, do_flip=do_flip)

    def update(self, delta_time):
        sound.play_intermission()
        self._update_events()

    def _update_events(self):
        for event in pygame.event.get():
            if event.type == pygame.MOUSEMOTION:
                sprites = self.renderer.get_sprites_at_tuple(event.pos)
                if sprites:
                    new_sprite = sprites[0]
                    if new_sprite != self._over_sprite:
                        self._set_mouse_over_to_sprite(False)
                        self._over_sprite = new_sprite
                        self._set_mouse_over_to_sprite(True)
            elif event.type == pygame.MOUSEBUTTONDOWN and event.button == settings.LEFT_MOUSE_BUTTON:
                sprites = self.renderer.get_sprites_at_tuple(event.pos)
                if sprites:
                    self._selected_sprite = sprites[0]
                else:
                    self._selected_sprite = None
            elif event.type == pygame.MOUSEBUTTONUP and event.button == settings.LEFT_MOUSE_BUTTON:
                sprites = self.renderer.get_sprites_at_tuple(event.pos)
                if sprites:
                    sprite = sprites[0]
                    if sprite == self._selected_sprite:
                        try:
                            sprite.do_action()
                        except Exception as e:
                            pass

    def _set_mouse_over_to_sprite(self, is_mouse_over):
        if self._over_sprite:
            try:
                self._over_sprite.mouse_over(is_mouse_over)
            except AttributeError:
                pass
