from __future__ import print_function

import pygame
from pygame.locals import Color

import pyknicpygame
from pyknicpygame.pyknic.context import Context
from pyknicpygame.pyknic.mathematics import Point2, Vec2
from pyknicpygame.pyknic.tweening import Tweener
from pyknicpygame.spritesystem import Sprite

import settings
import ptext
import sound


class CreditsContext(Context):
    def __init__(self):
        self.tweener = Tweener()
        self.scheduler = pyknicpygame.pyknic.timing.Scheduler()
        self.renderer = pyknicpygame.spritesystem.DefaultRenderer()
        w, h = settings.SCREEN_SIZE
        self.cam = pyknicpygame.spritesystem.Camera(pygame.Rect(0, 0, w, h), position=Point2(w // 2, h // 2))
        self.background = Color('lightblue')
        self.font_args = settings.LOLIGHT_FONT_COLOR

        # self.speed = 180, 120  # pixels per second
        direction = (-Vec2(180.0, 120)).normalized
        self.sprites = []

        self._add_text_sprite(h, w, Vec2(0.0, 0.0), 72, 'Credits')
        distance = 100
        self._add_text_sprite(h, w, direction * distance * 1, 40, settings.CAPTION)
        self._add_text_sprite(h, w, direction * distance * 2, 40, "pyweek 21 'aftermath'")
        self._add_text_sprite(h, w, direction * distance * 3, 40, 'feb 2016')
        self._add_text_sprite(h, w, direction * distance * 4, 40, 'Gummbum')
        self._add_text_sprite(h, w, direction * distance * 5, 40, 'DR0ID')

    def _add_text_sprite(self, h, w, offset, size, text):
        img = ptext.getsurf(text, 'vera', size, **settings.HILIGHT_FONT_COLOR)
        sprite = Sprite(img, Point2(w // 4 * 3, h // 4 * 3) + offset)
        sprite.speed = 180 * 0.7, 120 * 0.7
        self.renderer.add_sprite(sprite)
        self.sprites.append(sprite)

    def update(self, delta_time):
        sound.play_intermission()
        self._handle_events()
        self.tweener.update(delta_time)
        self.scheduler.update(delta_time)
        for sprite in self.sprites:
            self.move_label(delta_time, sprite)

    def move_label(self, delta_time, sprite):
        dx, dy = sprite.speed
        mx = dx * delta_time
        my = dy * delta_time
        x, y, w = sprite.position
        # r = self.sprite.image.get_rect(center=self.sprite.position)
        r = sprite.rect
        x += mx
        y += my
        r.center = x, y
        if r.left < 0:
            r.x = 0
            dx = -dx
        elif r.right > settings.SCREEN_WIDTH:
            r.right = settings.SCREEN_WIDTH
            dx = -dx
        if r.y < 0:
            r.y = 0
            dy = -dy
        elif r.bottom > settings.SCREEN_HEIGHT:
            r.bottom = settings.SCREEN_HEIGHT
            dy = -dy
        sprite.speed = dx, dy
        sprite.old_position.copy_values(sprite.position)
        sprite.position.x = x
        sprite.position.y = y

    def _handle_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.pop(self.get_stack_length(), False)
            elif event.type == pygame.MOUSEBUTTONDOWN:
                self.pop()
            elif event.type == pygame.KEYDOWN:
                if event.key == settings.KEY_CHEAT_EXIT:
                    self.pop()
                elif event.key == settings.KEY_CHEAT_COMBAT_TEST:
                    self.pop()
                    self.push(CreditsContext())

    def draw(self, screen, do_flip=True, interpolation_factor=1.0):
        self.renderer.draw(screen, self.cam, fill_color=self.background, do_flip=do_flip,
                           interpolation_factor=interpolation_factor)

    def enter(self):
        pass

    def exit(self):
        pass
