# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek21-2016-02
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import settings
from gameresource import GameResource
# from mathematics import Vec2
from pyknicpygame.pyknic.mathematics import Vec2

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2016"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 


class Player(object):

    def __init__(self, sanity_data, position, scheduler):
        self.scheduler = scheduler
        self.radius = settings.PLAYER_RADIUS
        self.sanity_data = sanity_data
        self.position = position
        self.target = position.clone()
        self.direction = Vec2(1.0, 0.0)
        self.sprite = None
        self._images = [
            GameResource.get(GameResource.IMG_PLAYER_00),
            GameResource.get(GameResource.IMG_PLAYER_01),
            GameResource.get(GameResource.IMG_PLAYER_02),
            GameResource.get(GameResource.IMG_PLAYER_03),
        ]
        self._img_idx = 1
        self._time_id = 0
        self._image_change_rate = settings.PLAYER_IMAGE_CHANGE_DELAY
        self._is_moving = False
        self.speed = settings.PLAYER_SPEED

    def update(self, delta_time):
        self.direction = self.target - self.position
        dist = self.direction.normalize()

        if dist > settings.PLAYER_DIST_TO_TARGET:
            self.position += self.direction * self.speed * delta_time
            self.set_moving(True)
        else:
            self.set_moving(False)

        self.sprite.rotation = -self.direction.angle
        self.sanity_data.decrease_hypnosis_level(self.sanity_data.hypnosis_decrease_rate_per_second * delta_time)

    def update_target(self, point):
        self.target.copy_values(point)

    def set_moving(self, is_moving):
        if self._is_moving != is_moving:
            self._is_moving = is_moving
            if is_moving:
                self._time_id = self.scheduler.schedule(self._update_img, self._image_change_rate)
            else:
                self.scheduler.remove(self._time_id)

    def _update_img(self):
        self.sprite.set_image(self._images[self._img_idx])
        self._img_idx += 1
        self._img_idx %= len(self._images)
        return self._image_change_rate
