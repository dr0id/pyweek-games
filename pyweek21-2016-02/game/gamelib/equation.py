# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek21-2016-02
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import random

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2016"
__credits__ = ["DR0ID"]     # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []    # list of public visible parts of this module 


class Equation(object):

    def __init__(self, equation, missing, choices, separator=' '):
        self.separator = separator
        self.missing = self.check_for_spaces(missing)
        self.choices = choices + list(missing.values())
        random.shuffle(self.choices)
        self._choices_orig = choices
        self.done = {}
        self.equation_as_chars = [self.to_quoted_string_if_spaces_exist(x) for x in equation.split(separator)]
        # check that there are enough missing pieces
        missing_symbol = '?'
        assert len(self.choices) <= 7  # not enough space on screen otherwise
        assert len(self.missing) == len([x for x in self.equation_as_chars if x == missing_symbol])
        # check that for each missing piece there is an corresponding entry in missing dict
        for idx, char in enumerate(self.equation_as_chars):
            if char == missing_symbol:
                assert idx in self.missing or (idx - len(self.equation_as_chars)) in self.missing
        self.try_count = 0

    def clone(self):
        return Equation(self.equation, dict(self.missing), list(self._choices_orig), self.separator)

    @property
    def equation(self):
        return ' '.join(self.equation_as_chars)

    def check(self, choice):
        choice = self.to_quoted_string_if_spaces_exist(choice)
        if choice in self.missing.values():
            entry_key = None
            for key, val in self.missing.items():
                if val == choice:
                    entry_key = key
            assert entry_key is not None
            self.done[entry_key] = choice
            del self.missing[entry_key]
            self.equation_as_chars[entry_key] = choice
            return True
        self.try_count += 1
        return False

    def is_done(self):
        return len(self.missing) == 0

    def check_for_spaces(self, missing):
        values_to_change = {}
        for k, v in missing.items():
            values_to_change[k] = self.to_quoted_string_if_spaces_exist(v)

        missing.update(values_to_change)
        return missing

    def to_quoted_string_if_spaces_exist(self, v):
        if ' ' in v:
            return "'{0}'".format(v)
        return v
