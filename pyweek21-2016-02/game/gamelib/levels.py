﻿# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek21-2016-02
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import settings
from equation import Equation
from pyknicpygame.pyknic.mathematics import Point2

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2016"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 


class MathZombieCombatData(object):
    def __init__(self, h_rate=0.5, pos=Point2(300, 300), angle=45, radius=25, equations=None):
        self.equations = equations
        if self.equations is None:
            self.equations = [
                Equation("1 + 1 = ?", {-1: '2'}, ['1', '3', '4', '6']),
                Equation("1 + 1 ? 3 = ?", {-1: '-1', 3: '-'}, ['1', '3', '4', '6']),
                Equation("4 ? 1 = ? + 2", {-3: '3', 1: '+'}, ['0', '3', '4', '6', '5']),
                Equation("tomato + mixer = ?", {-1: 'ketchup'}, ['bla', 'blu', 'tea', 'soup']),
            ]
        self.hypnosis_increase_rate_per_second = h_rate
        self.position = pos
        self.angle = angle
        self.radius = radius
        if pos.x < settings.CLASS_ROOM_BORDER:
            pos.x += settings.CLASS_ROOM_BORDER
        if pos.y < settings.CLASS_ROOM_BORDER:
            pos.y += settings.CLASS_ROOM_BORDER
        assert pos.x > settings.CLASS_ROOM_BORDER
        assert pos.x < settings.SCREEN_WIDTH - settings.CLASS_ROOM_BORDER
        assert pos.y > settings.CLASS_ROOM_BORDER
        assert pos.y < settings.SCREEN_HEIGHT - settings.CLASS_ROOM_BORDER

    def clone(self):
        return MathZombieCombatData(self.hypnosis_increase_rate_per_second, self.position.clone(), self.angle,
                                    self.radius, [eq.clone() for eq in self.equations])

sw = settings.SCREEN_WIDTH - settings.CLASS_ROOM_BORDER
sh = settings.SCREEN_HEIGHT - settings.CLASS_ROOM_BORDER


level_configs = {
    1:
        {
            'enemies': [
                MathZombieCombatData(h_rate=1.0, pos=Point2(sw * 0.5, sh * 0.5), angle=45, radius=25, equations=[
                    Equation("1 + 1 = ?", {-1: '2'}, ['1', '3', '4', '6']),
                    Equation("3 + ? = 8", {2: '5'}, ['1', '3', '4', '6', '8']),
                ]),
                MathZombieCombatData(h_rate=1.0, pos=Point2(sw * 0.25, sh * 0.75), angle=-45, radius=25, equations=[
                                         Equation("3 * 3 = ?", {-1: '9'}, ['3', '33', '5', '6']),
                                         Equation("20 / ? = 5", {2: '4'}, ['1', '3', '5', '6']),
                                     ]),
                MathZombieCombatData(h_rate=1.0, pos=Point2(sw * 0.8, sh * 0.5), angle=0, radius=25, equations=[
                                         Equation("3 ? 3 = 0", {1: '-'}, ['+', '/', '=', '~']),
                                         Equation("20 / ? = 5", {2: '4'}, ['1', '3', '5', '6']),
                                     ]),
            ],
            'time': 10,  # seconds
            'door_position': Point2(settings.SCREEN_WIDTH - settings.COMBAT_AREA_BORDER, settings.SCREEN_HEIGHT // 2),
        },
    2:
        {
            'enemies': [
                MathZombieCombatData(h_rate=1.0, pos=Point2(sw * 0.5, sh * 0.5), angle=45, radius=25, equations=[
                    Equation("1 + 1 + 1 = ?", {-1: '3'}, ['1', '4', '6']),
                    Equation("13 + ? = 28", {2: '15'}, ['3', '13', '+', '17']),
                ]),
                MathZombieCombatData(h_rate=0.9, pos=Point2(sw * 0.5, sh * 0.5), angle=45, radius=25, equations=[
                    Equation("2 ? 2 = 1", {1: '/'}, ['+', '-', '*']),
                    Equation("-2 ? 10 = 8", {1: '+'}, ['/', '=', '~']),
                ]),
                MathZombieCombatData(h_rate=0.9, pos=Point2(sw * 0.5, sh * 0.5), angle=45, radius=25, equations=[
                    Equation("21 ? 7 + 1 = 4", {1: '/'}, ['+', '=', '~']),
                    Equation("10 - 20 = ?", {-1: '-10'}, ['-1', '-20', '10', '-11']),
                ]),
            ],
            'time': 18,  # seconds
            'door_position': Point2(settings.COMBAT_AREA_BORDER, settings.SCREEN_HEIGHT // 2),
        },
    3:
        {
            'enemies': [
                MathZombieCombatData(h_rate=0.6, pos=Point2(sw * 0.25, sh * 0.75), angle=-45, radius=25, equations=[
                    Equation("E = M * ? ** 2", {4: 'C'}, ['c', 'A', 'G', 'h']),
                    Equation("m * ? = f", {2: 'a'}, ['A', 'n', 'C', 'p']),
                    Equation("4 ? 1 = ? + 2", {-3: '3', 1: '+'}, ['0', '-', '4', '5']),
                ]),
                MathZombieCombatData(h_rate=0.5, pos=Point2(sw * 0.25, sh * 0.25), angle=45, radius=25, equations=[
                    Equation("4 ? 1 = ? + 2", {-3: '3', 1: '+'}, ['0', '-', '4', '6']),
                    Equation("a**2 ? b**2 = c**2", {1: '+'}, ['~', '-', '*', '**', '&']),
                    Equation("2 + ? ? 1 = 3", {3: '-', 2: '2'}, ['+', '1', '3']),
                ]),
                MathZombieCombatData(h_rate=0.6, pos=Point2(sw * 0.25, sh * 0.25), angle=45, radius=25, equations=[
                    Equation("4 + 1 ? 5 - 0", {3: '='}, ['<', '>', '<=']),
                    Equation("9 ** ? = 3", {2: '0.5'}, ['0.25', '0.75', '5.0']),
                ]),
            ],
            'time': 20,  # seconds
            'door_position': Point2(settings.SCREEN_WIDTH - settings.COMBAT_AREA_BORDER, settings.SCREEN_HEIGHT // 2),
        },
    4:
        {
            'enemies': [
                MathZombieCombatData(h_rate=0.7, pos=Point2(sw * 0.5, sh * 0.5), angle=45, radius=25, equations=[
                    Equation("mouse + cat = ?", {-1: 'squeak'}, ['woof', 'roar', 'moo']),
                    Equation("cat + ? = ?", {2: 'water', -1: 'soggy'}, ['happy', 'catnip', 'dog']),
                ]),
                MathZombieCombatData(h_rate=1.0, pos=Point2(sw * 0.5, sh * 0.5), angle=45, radius=25, equations=[
                    Equation("mouse * ? = mice", {2: '2'}, ['1', '0', 'cat']),
                    Equation("blue + yellow = ?", {-1: 'green'}, ['blueyellow', 'yellowblue', "I don't know!"]),
                ]),
                MathZombieCombatData(h_rate=0.8, pos=Point2(sw * 0.5, sh * 0.5), angle=45, radius=25, equations=[
                    Equation("ice ** ? = ?", {2: '3', -1: 'ice_cubes'}, ['crushed_ice', 'black_ice', 'pick', '2']),
                    Equation("ice * 2 = ?", {-1: 'twice'}, ['iceice', 'ices', 'ice2']),
                ]),
            ],
            'time': 20,  # seconds
            'door_position': Point2(settings.COMBAT_AREA_BORDER, settings.SCREEN_HEIGHT // 2),
        },
    5:
        {
            'enemies': [
                MathZombieCombatData(h_rate=0.5, pos=Point2(sw * 0.5, sh * 0.5), angle=45, radius=25, equations=[
                    Equation("tortoise > ? + ?", {2: 'hare', -1: 'nap'}, ['turtle']),
                    Equation("700 + 200 - ? = ?", {-3: '100', -1: '800'}, ['1000', '500']),
                ]),
                MathZombieCombatData(h_rate=1.0, pos=Point2(sw * 0.5, sh * 0.5), angle=45, radius=25, equations=[
                    Equation("1 + 1 = ?", {-1: '2'}, ['1', '3', '4', '6']),
                    Equation("2 + ? = 3", {2: '1'}, ['3', '4', '8']),
                    Equation("3 + ? = 5", {2: '2'}, ['1', '3', '4', '6']),
                ]),
                MathZombieCombatData(h_rate=0.5, pos=Point2(sw * 0.5, sh * 0.5), angle=45, radius=25, equations=[
                    Equation("7 * 20 = ?", {-1: '140'}, ['142', '131']),
                    Equation("7 / 2 = ?", {-1: '3.5'}, ['35', '53', '5.3']),
                    Equation("7 ** ? = 1", {2: '0'}, ['1', '3', '4', '6']),
                    Equation("7 ** ? = 49", {2: '2'}, ['1', '3', '4', '6']),
                ]),
                MathZombieCombatData(h_rate=0.5, pos=Point2(sw * 0.5, sh * 0.5), angle=45, radius=25, equations=[
                    Equation("toe * 5 = ?", {-1: 'foot'}, ['5', '-2.5', 'toe_jam', '3']),
                    Equation("foot / ? = toe", {2: '5'}, ['1001', '3', '4', '6']),
                    Equation("feet - shoes = ?", {-1: 'smelly_feet'}, ['foots', '-2.5', '3']),
                    Equation("toe + finger = ?", {-1: 'smelly_finger'}, ['tickle', '3', '4', '6']),
                ]),
            ],
            'time': 20,  # seconds
            'door_position': Point2(settings.SCREEN_WIDTH - settings.COMBAT_AREA_BORDER, settings.SCREEN_HEIGHT // 2),
        },
    6:
        {
            'enemies': [
                # here only one because its the professor!!
                MathZombieCombatData(h_rate=1.0, pos=Point2(sw * 0.9, sh * 0.25), angle=45, radius=25, equations=[
                    Equation("5 / 2 = ?", {-1: '2.5'}, ['2', '-2.5', '3']),
                    Equation("1 ? 2 = 4", {1: '<<'}, ['<', '<=', '>>', '>']),
                    Equation("5 // 2 = ?", {-1: '2'}, ['2.5', '-2.5', '3']),
                    Equation("1 + 1 = 2 ? 1", {-2: 'Minus!'}, ['Plus!', 'Mult!', 'Divide!', 'Root!']),  # not sure if people will remember it...
                ]),
            ],
            'time': 25,  # seconds
            'door_position': Point2(settings.COMBAT_AREA_BORDER, settings.SCREEN_HEIGHT // 2),
        },
}


class PlayerMathSanityData(object):
    def __init__(self):
        self.hypnosis_level = 0
        self.hypnosis_threshold = 10
        self.hypnosis_decrease_rate_per_second = 1.0

    def decrease_hypnosis_level(self, amount):
        self.hypnosis_level -= amount
        # clamp to lowest possible level
        if self.hypnosis_level < 0:
            self.hypnosis_level = 0
