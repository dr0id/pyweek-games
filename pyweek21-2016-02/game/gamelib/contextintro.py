from __future__ import print_function

import random
from math import radians, cos, sin, atan2, pi

import pygame
from pygame.locals import Color

import pyknicpygame
from pyknicpygame.pyknic.context import Context
from pyknicpygame.pyknic.mathematics import Point2, Vec2
from pyknicpygame.pyknic.tweening import Tweener
from pyknicpygame.spritesystem import Sprite

import settings
import ptext
import sound


def distance(a, b):
    """Calculate the distance between points a and b. Returns distance as a float.

    The a argument is a sequence representing one end point (x1,y1).

    The b argument is a sequence representing the other end point (x2,y2).
    """
    x1, y1 = a
    x2, y2 = b
    diffx = x1 - x2
    diffy = y1 - y2
    return (diffx * diffx + diffy * diffy) ** 0.5


def angle_of(origin, end_point):
    """Calculate the angle between the vector defined by end points (origin,point)
    and the Y axis. All input and output values are in terms of pygame screen
    space. Returns degrees as a float.

    The origin argument is a sequence of two numbers representing the origin
    point.

    The point argument is a sequence of two numbers representing the end point.

    The angle 0 and 360 are oriented at the top of the screen, and increase
    clockwise.
    """
    x1, y1 = origin
    x2, y2 = end_point
    return (atan2(y2 - y1, x2 - x1) * 180.0 / pi + 90.0) % 360.0


def point_on_circumference(center, radius, degrees_):
    """Calculate the point on the circumference of a circle defined by center and
    radius along the given angle. Returns a tuple (x,y).

    The center argument is a representing the origin of the circle.

    The radius argument is a number representing the length of the radius.

    The degrees_ argument is a number representing the angle of radius from
    origin. The angles 0 and 360 are at the top of the screen, with values
    increasing clockwise.
    """
    radians_ = radians(degrees_ - 90)
    x = center[0] + radius * cos(radians_)
    y = center[1] + radius * sin(radians_)
    return [x, y]


class Struct(object):
    """A class with arbitrary members. Construct it like a dict, then access the
    keys as instance attributes.

    s = Struct(x=2, y=4)
    print s.x,s.y
    """

    def __init__(self, **kwargs):
        self.__dict__.update(**kwargs)


class Circle(object):
    def __init__(self, origin, radius, angle, speed):
        self.origin = origin
        self.radius = radius
        self.angle = angle
        self.speed = speed
        self.position = origin
        self.image = None
        self.rect = None
        self.update(0)
        self.tvalx = 0
        self.tvaly = 0

    def update(self, delta_time):
        self.angle += self.speed * delta_time
        self.angle %= 360.0
        self.position = point_on_circumference(self.origin, self.radius, self.angle)


class Orbits(object):
    def __init__(self, letters):
        self.tval = 0
        self.letters = letters
        self.origin = settings.SCREEN_WIDTH // 2, settings.SCREEN_HEIGHT // 2
        self.radius = 250
        self.angle = 45.0
        self.speed = 150
        self.position = 0, 0
        self.circles = []
        self.rels = []
        w, h = settings.SCREEN_SIZE
        for c in self.letters:
            origin = random.randrange(200, w - 200), random.randrange(100, h - 100)
            radius = random.randrange(50, 150)
            angle = random.randrange(360)
            speed = random.randrange(100, 400)
            circle = Circle(origin, radius, angle, speed)
            circle.char = c
            circle.image = ptext.getsurf(c, settings.FONT_SPLASH, 84, **settings.SPLASH_FONT_COLOR)
            circle.rect = circle.image.get_rect()
            self.circles.append(circle)
            self.rels.append(dict(
                angle=random.randrange(360),
                radius=random.randrange(33, 99),
                speed=random.randrange(50, 100),
            ))
        self.update(0)

    def update(self, delta_time):
        self.angle += self.speed * delta_time
        self.angle %= 360.0
        self.position = point_on_circumference(self.origin, self.radius, self.angle)
        for i, c in enumerate(self.circles):
            rel = self.rels[i]
            angle = rel['angle']
            radius = rel['radius']
            speed = rel['speed']
            angle += speed * delta_time
            angle %= 360.0
            c.origin = point_on_circumference(self.position, radius, angle)
            rel['angle'] = angle
            rel['radius'] = radius
            rel['speed'] = speed
            c.update(delta_time)


class ContextIntro(Context):
    def __init__(self):
        self.tweener = Tweener()
        w, h = settings.SCREEN_SIZE
        self.background = Color('lightblue')
        self.font_args = settings.LOLIGHT_FONT_COLOR

        self.orbits = Orbits('DETENTION:TheAftermath')
        self.doing = self.spinning
        self.tween = self.tweener.create_tween(self.orbits, 'tval', 0, 10, 9.0, cb_end=self.done_spinning)
        self.tobj = None
        self.writings = []
        self.screen_capture = None
        self.screen_capture_pos = [0, 0]

        self.align_left = 100
        self.current_pos = [self.align_left, 200]

    def update(self, delta_time):
        sound.play_intermission()
        self._handle_events()
        self.tweener.update(delta_time)
        self.doing(delta_time)

    def spinning(self, delta_time):
        self.orbits.update(delta_time)

    def done_spinning(self, *args):
        # print('---- done spinning ----')
        self.doing = self.writing1
        self.tobj = Struct(tval=0)
        self.tween = self.tweener.create_tween(self.tobj, 'tval', 1, -1, 0.2)

    def update_writings(self, delta_time):
        self.orbits.update(delta_time)
        for c in self.writings:
            c.rect.x = c.tvalx
            c.rect.y = c.tvaly

    def writing1(self, delta_time):
        self.update_writings(delta_time)
        if self.tobj.tval == 0:
            # print('---- next letter ----')
            circle = self.orbits.circles.pop(0)
            self.orbits.rels.pop(0)
            fx, fy = circle.position
            tx, ty = self.next_pos(circle)
            self.tweener.create_tween(circle, 'tvalx', fx, tx - fx, 0.4)
            self.tweener.create_tween(circle, 'tvaly', fy, ty - fy, 0.4)
            self.writings.append(circle)
            if circle.char == ':':
                self.doing = self.pausing
                self.tween = self.tweener.create_tween(self.tobj, 'tval', 2, -2, 6.0)
            else:
                self.tween = self.tweener.create_tween(self.tobj, 'tval', 1, -1, 0.2)

    def pausing(self, delta_time):
        self.update_writings(delta_time)
        if self.tobj.tval == 0:
            self.doing = self.writing2
            self.current_pos = [self.align_left, 350]
            for i, circle in enumerate(self.orbits.circles):
                if i == 3:
                    # add a space after "THE"
                    self.current_pos[0] += 30
                fx, fy = circle.position
                tx, ty = self.next_pos(circle)
                self.tweener.create_tween(circle, 'tvalx', fx, tx - fx, 0.4)
                self.tweener.create_tween(circle, 'tvaly', fy, ty - fy, 0.4)
                self.writings.append(circle)
            del self.orbits.circles[:]
            del self.orbits.rels[:]
            self.tween = self.tweener.create_tween(self.tobj, 'tval', 4, -4, 4)

    def writing2(self, delta_time):
        self.update_writings(delta_time)
        if self.tobj.tval == 0:
            self.doing = self.slide_left
            self.tween = self.tweener.create_tween(
                self.tobj, 'tval', 0, -settings.SCREEN_WIDTH, 0.5, cb_end=self.all_done)
            self.screen_capture = pygame.display.get_surface().copy()

    def slide_left(self, delta_time):
        self.screen_capture_pos[0] = self.tobj.tval

    def all_done(self, *args):
        for i in range(10):
            pygame.time.wait(100)
            pygame.event.clear()
        self.pop()

    def next_pos(self, circle):
        pos = self.current_pos
        retval = pos[:]
        pos[0] += circle.image.get_width() + 3
        # print('---- to pos {} {}:{}----'.format(retval, circle.char, circle.image.get_width()))
        return retval

    def _handle_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.pop(self.get_stack_length(), False)
            elif event.type == pygame.MOUSEBUTTONDOWN:
                # self.pop()
                # if self.doing == self.spinning:
                #     self.tweener.remove_tween(self.orbits, 'tval')
                #     self.done_spinning()
                # elif settings.CHEATS_ENABLED:
                if settings.CHEATS_ENABLED:
                    if self.top() == self:
                        self.pop()
            elif event.type == pygame.KEYDOWN:
                if event.key == settings.KEY_CHEAT_EXIT:
                    self.pop()

    def draw(self, screen, do_flip=True, interpolation_factor=1.0):
        screen.fill(self.background)
        if self.screen_capture is None:
            for c in self.orbits.circles:
                c.rect.center = c.position
                screen.blit(c.image, c.rect)
            for c in self.writings:
                screen.blit(c.image, c.rect)
        else:
            screen.blit(self.screen_capture, self.screen_capture_pos)
        if do_flip:
            pygame.display.flip()

    def enter(self):
        pass

    def exit(self):
        pass
