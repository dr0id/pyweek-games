﻿# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek21-2016-02
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

import pygame

import gameprogress
import levels
import pyknicpygame
import settings
import sound
# import tweening
from pyknicpygame.pyknic import tweening
from pyknicpygame.pyknic.context.transitions import PopTransition
from contextcombat import CombatContext
from effecterase import EraseEffect
from gameresource import GameResource
# from mathematics import Point2
from pyknicpygame.pyknic.mathematics import Point2
from player import Player
from pyknicpygame import spritesystem
from pyknicpygame.pyknic import timing
from pyknicpygame.pyknic.context import Context
# from spritesystem import Sprite
from pyknicpygame.spritesystem import Sprite
# from tweening import Tweener
from pyknicpygame.pyknic.tweening import Tweener
from zombie import Zombie

logger = logging.getLogger("pyknic.main")
logger.setLevel(pyknicpygame.pyknic.settings.log_logger_level)

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2016"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 


class SelectableSprite(spritesystem.Sprite):
    def __init__(self, image, hover_image, action, door_id, position, anchor=None):
        spritesystem.Sprite.__init__(self, image, position, anchor)

        self._hover_image = hover_image
        self._normal_image = image
        self._action = action
        self._is_hovered = False
        self._door_id = door_id

    def mouse_over(self, is_mouse_over):
        if self._is_hovered:
            if not is_mouse_over:
                # mouse exit
                self._mouse_leave()
        else:
            if is_mouse_over:
                # mouse enter
                self._mouse_enter()

    def do_action(self):
        self._action(self._door_id)

    def _mouse_enter(self):
        self.set_image(self._hover_image)
        self._is_hovered = True

    def _mouse_leave(self):
        self._is_hovered = False
        self.set_image(self._normal_image)


class GamePlayContext(Context):
    def __init__(self, level_idx, player_sanity_data, is_repeat):
        # dependencies, should be provided by arguments
        self.is_repeat = is_repeat
        self._time_is_up = False
        self.renderer = spritesystem.DefaultRenderer()
        screen_rect = pygame.Rect(0, 0, settings.SCREEN_WIDTH, settings.SCREEN_HEIGHT)
        self.cam = spritesystem.Camera(screen_rect)
        self.cam.position = Point2(settings.SCREEN_WIDTH // 2, settings.SCREEN_HEIGHT // 2)
        self.scheduler = timing.Scheduler()
        self.tweener = Tweener()

        # variables
        self.player_sanity_data = player_sanity_data
        self._over_sprite = None
        self._selected_sprite = None
        self._level_idx = level_idx
        self._enemies = []
        self._player = None

        self.won = False
        self.time_for_5_min = 0
        self._combat = None
        self.update = self._update

        self.tock = True

    @property
    def level_index(self):
        return self._level_idx

    def enter(self):
        self._setup_level()
        self._add_background()
        self._add_debug_help_buttons()

    def _setup_level(self):
        level_config = levels.level_configs[self._level_idx]
        self._setup_bell(level_config)
        self._setup_player(level_config)
        self._setup_enemies(level_config)

    def _setup_player(self, level_config):
        self.player_sanity_data.hypnosis_level = 0  # reset for each level
        self._player = Player(self.player_sanity_data, level_config['door_position'].clone(), self.scheduler)
        spr = Sprite(GameResource.get(GameResource.IMG_PLAYER_00), self._player.position)
        self._player.sprite = spr
        self.renderer.add_sprite(self._player.sprite)

    def _setup_enemies(self, level_config):
        enemies_ = level_config['enemies']
        for enemy_combat_data in enemies_:

            enemy = Zombie(enemy_combat_data.clone(), level_config['door_position'])
            img = GameResource.get(GameResource.IMG_ZOMBIE_00)
            spr = Sprite(img, enemy.position)
            enemy.sprite = spr
            self._enemies.append(enemy)
            self.renderer.add_sprite(spr)

    def _add_debug_help_buttons(self):
        if settings.COMBAT_AREA_DEBUG_RENDER_ON:
            img_closed = pygame.Surface((100, 100))
            img_closed.fill((255, 0, 0))
            img_opened = img_closed.copy()
            img_opened.fill((0, 255, 0))
            position = Point2(50, 50)
            door_id = -1
            spr = SelectableSprite(img_closed, img_opened, self._handle_click_action, door_id, position, anchor='topleft')
            self.renderer.add_sprite(spr)
            img_opened = img_closed.copy()
            img_opened.fill((0, 0, 255))
            position = Point2(150, 50)
            spr = SelectableSprite(img_closed, img_opened, self._handle_click_action, 1, position, anchor='topleft')
            self.renderer.add_sprite(spr)
            img_opened = img_closed.copy()
            img_opened.fill((255, 0, 255))
            position = Point2(250, 50)
            spr = SelectableSprite(img_closed, img_opened, self._handle_click_action, 0, position, anchor='topleft')
            self.renderer.add_sprite(spr)

    def _add_background(self):
        img_blackboard = GameResource.get(GameResource.IMG_HALLWAY_BLACKBOARD)
        spr = spritesystem.Sprite(img_blackboard, Point2(0, 0), anchor='topleft', z_layer=-10)
        self.renderer.add_sprite(spr)

        room_img = GameResource.get(GameResource.IMG_CLASSROOM)
        if self.level_index % 2 == 1:
            room_img = pygame.transform.flip(room_img, True, self.level_index in (3, 4))
        spr = spritesystem.Sprite(room_img, Point2(0, 0), anchor='topleft', z_layer=-9)
        self.renderer.add_sprite(spr)

    def draw(self, screen, do_flip=True, interpolation_factor=1.0):
        self.renderer.draw(screen, self.cam, fill_color=(255, 0, 255), do_flip=do_flip)

    def _update(self, delta_time):
        sound.play_intermission()
        self._update_events()
        self._update_timed(delta_time)
        self._check_for_collisions()
        self._check_win_condition()
        self._check_loose_condition()

    def _check_loose_condition(self):
        if self._time_is_up:
            logger.info("level " + str(self._level_idx) + " LOST!")
            self.won = False
            self._leave_level()

    def _check_win_condition(self):
        active_zombie_count = 0
        for enemy in self._enemies:
            if not enemy.is_converted():
                active_zombie_count += 1

        if active_zombie_count > 0:  # there is still a math zombie around!
            return

        self.won = True
        logger.info("level " + str(self._level_idx) + " WON!")
        self._leave_level()

    def _leave_level(self):
        self.update = self._update_timed  # prevent user input and win/loose conditions check
        self.scheduler.schedule(self._pop_level, 2)

    def _pop_level(self):
        # self.pop()
        self.push(PopTransition(EraseEffect(), do_resume_new=False))
        return 0

    def _update_events(self):
        for event in pygame.event.get():
            if event.type == pygame.MOUSEMOTION:
                if settings.COMBAT_AREA_DEBUG_RENDER_ON:
                    # TODO: remove ################

                    sprites = self.renderer.get_sprites_at_tuple(event.pos)
                    if sprites:
                        new_sprite = sprites[0]
                        if new_sprite != self._over_sprite:
                            self._set_mouse_over_to_sprite(False)
                            self._over_sprite = new_sprite
                            self._set_mouse_over_to_sprite(True)
                    # TODO: end remove ############
            elif event.type == pygame.MOUSEBUTTONDOWN and event.button == settings.LEFT_MOUSE_BUTTON:
                if settings.COMBAT_AREA_DEBUG_RENDER_ON:
                    # TODO: remove ################
                    sprites = self.renderer.get_sprites_at_tuple(event.pos)
                    if sprites:
                        self._selected_sprite = sprites[0]
                    else:
                        self._selected_sprite = None
                    # TODO: end remove ############
                # update player
                x, y, = event.pos
                self._player.update_target(Point2(x, y))
            elif event.type == pygame.MOUSEBUTTONUP and event.button == settings.LEFT_MOUSE_BUTTON:
                if settings.COMBAT_AREA_DEBUG_RENDER_ON:
                    # TODO: remove ################
                    sprites = self.renderer.get_sprites_at_tuple(event.pos)
                    if sprites:
                        sprite = sprites[0]
                        if sprite == self._selected_sprite:
                            try:
                                sprite.do_action()
                            except Exception as e:
                                pass
                    # TODO: end remove ############

    def _set_mouse_over_to_sprite(self, is_mouse_over):
        if self._over_sprite:
            try:
                self._over_sprite.mouse_over(is_mouse_over)
            except AttributeError:
                pass

    def _handle_click_action(self, btn_id):
        # TODO: remove ################
        if btn_id > 0:
            # win
            self._enemies.clear()
        elif btn_id == 0:
            # combat
            # combat = CombatContext(MathZombieCombatData(), PlayerMathSanityData())
            # self.push(combat)
            pass
        else:
            # loose
            self._time_is_up = True
            # self.pop()

    def _update_enemies(self, delta_time):
        Zombie.scheduler.update(delta_time)
        for enemy in self._enemies:
            enemy.update(delta_time)
            enemy.sprite.rotation = -enemy.direction.angle

    def _setup_bell(self, level_config):
        time_span = level_config['time']
        self.time_for_5_min = time_span / 12.0
        self.scheduler.schedule(self._on_second_passed, self.time_for_5_min)

        clock_position = Point2(settings.SCREEN_WIDTH - settings.COMBAT_AREA_BORDER - 40, settings.COMBAT_AREA_BORDER + 40)
        self._bell_sprites = [
            Sprite(GameResource.get(GameResource.IMG_CLOCK_00), clock_position, z_layer=10),
            Sprite(GameResource.get(GameResource.IMG_CLOCK_05), clock_position, z_layer=10),
            Sprite(GameResource.get(GameResource.IMG_CLOCK_10), clock_position, z_layer=10),
            Sprite(GameResource.get(GameResource.IMG_CLOCK_15), clock_position, z_layer=10),
            Sprite(GameResource.get(GameResource.IMG_CLOCK_20), clock_position, z_layer=10),
            Sprite(GameResource.get(GameResource.IMG_CLOCK_25), clock_position, z_layer=10),
            Sprite(GameResource.get(GameResource.IMG_CLOCK_30), clock_position, z_layer=10),
            Sprite(GameResource.get(GameResource.IMG_CLOCK_35), clock_position, z_layer=10),
            Sprite(GameResource.get(GameResource.IMG_CLOCK_40), clock_position, z_layer=10),
            Sprite(GameResource.get(GameResource.IMG_CLOCK_45), clock_position, z_layer=10),
            Sprite(GameResource.get(GameResource.IMG_CLOCK_50), clock_position, z_layer=10),
            Sprite(GameResource.get(GameResource.IMG_CLOCK_55), clock_position, z_layer=10),
            Sprite(GameResource.get(GameResource.IMG_CLOCK_60), clock_position, z_layer=10),
        ]

        self.renderer.add_sprite(self._bell_sprites[0])

    def _on_second_passed(self):
        if len(self._bell_sprites) > 1:
            self.tock = not self.tock
            if self.tock:
                GameResource.get(GameResource.SOUND_CLOCK_TOCK_DISTINTIVE).play()
            else:
                GameResource.get(GameResource.SOUND_CLOCK_TICK_DISTINTIVE).play()
            old_spr = self._bell_sprites.pop(0)
            self.renderer.remove_sprite(old_spr)
            self.renderer.add_sprite(self._bell_sprites[0])
            return self.time_for_5_min
        else:
            GameResource.get(GameResource.SOUND_CLASS_BELL).play()
            spr = self._bell_sprites[-1]
            self.tweener.create_tween(spr.position, 'x', spr.position.x, 0, 10, tweening.RANDOM_INT_BOUNCE, (-10, 10))
            self.tweener.create_tween(spr.position, 'y', spr.position.y, 0, 10, tweening.RANDOM_INT_BOUNCE, (-10, 10))
            self._time_is_up = True
            return 0

    def _update_timed(self, delta_time):
        self._update_enemies(delta_time)
        self.scheduler.update(delta_time)
        self.tweener.update(delta_time)
        self._player.update(delta_time)

    def exit(self):
        pygame.event.clear()  # no events should leak
        if self.won:
            if self.level_index == gameprogress.progress.current_level:
                gameprogress.progress.current_level += 1
                gameprogress.progress.save()

    def _check_for_collisions(self):
        for enemy in self._enemies:
            diff = enemy.position - self._player.position
            distance = diff.length
            if distance < enemy.radius + self._player.radius and self._player.direction.dot(diff.normalized) > 0 and not enemy.is_converted():
                self._combat = CombatContext(enemy, self._player.sanity_data)
                self.push(self._combat)
                break

    def resume(self):
        if self._combat:
            self.player_sanity_data.hypnosis_level = 0
            if self._combat.has_player_won():
                zombie = self._combat.zombie
                zombie.convert()
            else:
                pass

            # separate zombie and player so they do not collide again
            self._player.direction = -self._player.direction
            dist = 50
            self._player.target = self._player.position + self._player.direction * dist
            self.tweener.create_tween(self._player.position, 'x', self._player.position.x, self._player.direction.x * dist, 0.5, tweening.OUT_SINE)
            self.tweener.create_tween(self._player.position, 'y', self._player.position.y, self._player.direction.y * dist, 0.5, tweening.OUT_SINE)
            self._combat = None

