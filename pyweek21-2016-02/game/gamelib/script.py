import logging
import random
import sys

import pygame

import pyknicpygame
from pyknicpygame.pyknic.context import Context
from pyknicpygame.pyknic.mathematics import Point2
from pyknicpygame.pyknic.tweening import Tweener
from pyknicpygame.spritesystem import Sprite

import settings
import ptext
from gameresource import GameResource
import sound
import gradients


logger = logging.getLogger("pyknic.main")
logger.setLevel(pyknicpygame.pyknic.settings.log_logger_level)


class _Timer(object):
    def __init__(self, start, end, seconds, callback_end, tweener):
        self.value = 0.0
        self.callback_end = callback_end
        self.tweener = tweener
        tweener.create_tween(self, 'value', start, end - start, seconds, cb_end=self.done)
        # logger.debug('_Timer: start {}'.format(self.value))

    def remove(self):
        self.tweener.remove_tween(self, 'value')

    def done(self, *args):
        self.callback_end()
        # logger.debug('_Timer: end {}'.format(self.value))


SPRITE_LAYER_BACKGROUND = 9
SPRITE_LAYER_DIALOGUE = 10
SPRITE_LAYER_CHARACTER = 11
SPRITE_LAYER_TITLE = 12
SPRITE_LAYER_SAY = 13
SPRITE_LAYER_ASK = 14


class Globals:
    # a container to look up the speaker by side
    character = {
        '<': '',
        'l': '',
        'left': '',
        '>': '',
        'r': '',
        'right': '',
    }
    renderer = None


class _Command(object):
    def think(self, delta_time, events):
        pass

    def is_finished(self):
        return False


class Dialogue(_Command):
    def __init__(self, level, screen_title, chalk_board_id=None):
        self.level = level
        self.screen_title = screen_title
        self.tweener = Tweener()
        self.sprites = []

        # Blackboard canvas
        w, h = settings.SCREEN_SIZE
        self.fully_opened_y = h - settings.DIALOGUE_AREA.h
        self.fully_closed_y = h
        if chalk_board_id is None:
            chalk_board_id = GameResource.IMG_CHALKBOARD_768x432
        surf = GameResource.get(chalk_board_id)
        surf.set_colorkey(pygame.Color('white'))
        self.fully_opened_y = h - surf.get_height()
        self.sprite = Sprite(surf, Point2(*pygame.display.get_surface().get_rect().center),
                             z_layer=SPRITE_LAYER_DIALOGUE)
        self.sprite.anchor = 'midtop'
        self.sprites.append(self.sprite)

        # Blackboard title
        surf = ptext.getsurf(self.screen_title, settings.FONT_SAY, 60, **settings.BLACKBOARD_TITLE_FONT_COLOR )
        sprite = Sprite(surf, Point2(w // 2, 0), z_layer=SPRITE_LAYER_TITLE)
        sprite.rect.bottom = settings.DIALOGUE_AREA.y
        sprite.position.y = sprite.rect.centery
        self.sprites.append(sprite)
        self.title_sprite = sprite

        # Background decorations
        surf_room = GameResource.get(GameResource.IMG_CLASSROOM)
        surf = pygame.Surface(surf_room.get_size())
        surf.fill((77, 77, 77))
        surf.blit(surf_room, (0, 0))
        sprite = Sprite(surf, Point2(w // 2, 160), z_layer=SPRITE_LAYER_BACKGROUND)
        sprite.zoom = 0.42
        sprite.rotation = 5.0
        # sprite.alpha = 144
        self.sprites.append(sprite)
        surf_clock = GameResource.get(GameResource.IMG_CLOCK_10)
        rect = surf_clock.get_rect()
        rect.w += 6
        rect.h += 6
        surf = pygame.Surface(rect.size)
        surf.fill((0, 0, 0))
        surf.set_colorkey((0, 0, 0))
        pygame.draw.circle(surf, (77, 77, 77), rect.center, rect.width // 2, 0)
        surf.blit(surf_clock, (3, 3))
        sprite = Sprite(surf, Point2(w * 4 // 5, 80), z_layer=SPRITE_LAYER_BACKGROUND)
        # sprite.zoom = 1.0
        sprite.rotation = -5.0
        # sprite.alpha = 144
        self.sprites.append(sprite)

        self.running = True
        self.blocking = True
        self.doing = self.opening
        self.tween = _Timer(self.fully_closed_y, self.fully_opened_y, 0.25, self.done_opening, self.tweener)

    def close(self):
        self.tween = _Timer(self.fully_opened_y, self.fully_closed_y, 0.25, self.done_closing, self.tweener)
        self.doing = self.closing
        self.blocking = True

    def think(self, delta_time, events):
        self.tweener.update(delta_time)
        self.doing(delta_time)

    def opening(self, delta_time):
        # animate opening the dialogue area
        self.sprite.position.y = self.tween.value
        self.title_sprite.position.y = self.tween.value + 50

    def nothing(self, delta_time):
        pass

    def closing(self, delta_time):
        # animate closing the dialogue area
        self.sprite.position.y = self.tween.value
        self.title_sprite.position.y = self.tween.value + 50

    def done_opening(self, *args):
        self.tween.remove()
        self.sprite.position.y = self.fully_opened_y
        self.doing = self.nothing
        self.blocking = False

    def done_closing(self, *args):
        self.tween.remove()
        del self.sprites[:]
        self.doing = self.nothing
        self.running = False
        self.blocking = False

    def is_finished(self):
        return self.running


class Character(_Command):
    def __init__(self, side, name, image_id, flipped=False):
        assert side in ('left', 'right', 'l', 'r', '<', ">")
        Globals.character[side] = self
        self.name = name
        self.image_id = image_id
        self.side = side
        self.sprites = []

        img = GameResource.get(image_id)
        sprite = Sprite(img, Point2(0, 0), z_layer=SPRITE_LAYER_CHARACTER)
        screen_rect = pygame.display.get_surface().get_rect()
        chalkboard_rect = GameResource.get(GameResource.IMG_CHALKBOARD2).get_rect(
            centerx=screen_rect.centerx, bottom=screen_rect.bottom)
        if side in '<left':
            anchor = 'topright'
            x = chalkboard_rect.x - sprite.offset.x + 30
            y = chalkboard_rect.y - sprite.offset.y + 120
        else:
            anchor = 'topleft'
            x = chalkboard_rect.right - sprite.offset.x - 30
            y = chalkboard_rect.y - sprite.offset.y + 120
        setattr(sprite.rect, anchor, (x, y))
        sprite.position.x = sprite.rect.centerx
        sprite.position.y = sprite.rect.centery
        if flipped:
            sprite.flipped_x = True
        self.sprite = sprite
        self.sprites.append(sprite)

        self.tweener = Tweener()
        self.tween = _Timer(0.1, 1.0, 0.15, self.done_walking_on, self.tweener)
        self.doing = self.zooming

        self.running = True
        self.blocking = True

    def say(self, text, choices=None):
        speech = Say(self.side, text, choices)
        return speech

    def ask(self, text, choices=None):
        return self.say(text, choices)

    def close(self):
        self.tween = _Timer(1.0, 0.1, 0.15, self.done_closing, self.tweener)
        self.doing = self.zooming
        self.blocking = True

    def think(self, delta_time, events):
        self.tweener.update(delta_time)
        self.doing()

    def zooming(self):
        self.sprite.zoom = self.tween.value

    def nothing(self):
        pass

    def done_walking_on(self, *args):
        self.tween.remove()
        self.zooming()
        self.doing = self.nothing
        self.blocking = False

    def done_closing(self, *args):
        self.tween.remove()
        self.running = False
        self.blocking = False

    def is_finished(self):
        return self.running


class Choice(Sprite):
    # just a subclass so I can detect the type
    pass


class ChoiceFrame(Sprite):
    # just a subclass so I can detect the type
    pass


class Say(_Command):
    def __init__(self, side, text, choices=None):
        """
        side: one of 'left', 'right', 'l', 'r', '<', ">"
        text: str to use with ptext
        prompt: text to put in [prompt] (e.g. what keys to press, or list of choices)
        choices: varargs of pygame.KEYDOWN keys
        """
        assert side in ('left', 'right', 'l', 'r', '<', ">")
        self.side = side
        self.text = text
        self.sprites = []
        self.choices = choices
        self.choice_sprites = []
        self.answer = None
        self.elapsed = 0.0

        # Render left-right text...
        if settings.DIALOGUE_ADD_NAME:
            self.text = '{}:\n{}'.format(Globals.character[side].name, self.text)
        if side in '<left':
            img = ptext.getsurf(self.text, settings.FONT_SAY, 40, width=settings.DIALOGUE_LEFT_AREA.w, align='left',
                                **settings.SAY_FONT_COLOR)
            x, y = settings.DIALOGUE_LEFT_AREA.topleft
            x += 5
            y += 55
            sprite = Sprite(img, Point2(x, y), anchor='topleft', z_layer=SPRITE_LAYER_SAY)
        else:
            img = ptext.getsurf(self.text, settings.FONT_SAY, 40, width=settings.DIALOGUE_RIGHT_AREA.w, align='left',
                                **settings.SAY_FONT_COLOR)
            x, y = settings.DIALOGUE_RIGHT_AREA.topright
            x -= 5
            y += 55
            sprite = Sprite(img, Point2(x, y), anchor='topright', z_layer=SPRITE_LAYER_SAY)
        self.sprites.append(sprite)
        self.text_sprite = sprite

        # Render choices...
        if choices is None:
            choices = ['Skip:', 'SPACE', 'ENTER']
        total_width = 0
        for text in ['['] + list(choices) + [']']:
            img = ptext.getsurf(text, settings.FONT_SAY, 40, **settings.SAY_CHOICES_FONT_COLOR)
            total_width += img.get_width()
            x = settings.DIALOGUE_AREA.centerx
            y = settings.DIALOGUE_AREA.bottom - 40
            if text in '[]':
                sprite = ChoiceFrame(img, Point2(x, y), anchor='bottomleft', z_layer=SPRITE_LAYER_SAY)
            else:
                sprite = Choice(img, Point2(x, y), anchor='bottomleft', z_layer=SPRITE_LAYER_SAY)
                self.choice_sprites.append(sprite)
            sprite.user_text = text
            self.sprites.append(sprite)
        # Justify choices on center
        choice_sprites = [s for s in self.sprites if isinstance(s, (Choice, ChoiceFrame))]
        spacing = settings.DIALOGUE_AREA_CHOICES_SPACING
        x = settings.DIALOGUE_AREA.centerx - total_width // 2 + spacing * len(choice_sprites) - spacing
        for sprite in choice_sprites:
            sprite.position.x = x
            x += sprite.image.get_width() + spacing
        mouse_pos = pygame.mouse.get_pos()
        for sprite in self.choice_sprites:
            sprite.rect.center = sprite.position.x - sprite.offset.x, sprite.position.y - sprite.offset.y
            sprite.anchor = 'center'
            sprite.position.x = sprite.rect.centerx
            sprite.position.y = sprite.rect.centery
            if sprite.rect.collidepoint(mouse_pos):
                sprite.zoom = settings.DIALOGUE_AREA_CHOICES_HIGHLIGHT_ZOOM
            else:
                sprite.zoom = settings.DIALOGUE_AREA_CHOICES_DEFAULT_ZOOM

        self.tweener = Tweener()
        # self.wait = len(self.text.split(' ')) / 2.0 + 2.0  # seconds
        self.wait = len(self.text) / 3.0 * settings.DIALOGUE_SECONDS_PER_WORD  # seconds
        if self.wait < 1.5:
            self.wait = 1.5
        if self.choices is not None:
            self.wait = 0.1
        self.running = True
        self.blocking = True
        self.doing = self.zooming
        self.tween = _Timer(0.0, 1.0, 0.15, self.done_opening, self.tweener)
        # I don't much like this chalk sound over and over. - Gumm
        self.sound_channel = None
        # self.sound_channel = GameResource.get(GameResource.SOUND_CHALK_PARAGRAPH).play()
        # self.sound_channel.set_volume(0.5)

    def close(self, *args):
        self.tween = _Timer(1.0, 0.1, 0.15, self.done_closing, self.tweener)
        self.doing = self.zooming
        self.blocking = True

    def think(self, delta_time, events):
        self.elapsed += delta_time
        self.do_mouseover(events)
        self.tweener.update(delta_time)
        self.doing(delta_time, events)

    def do_mouseover(self, events):
        for event in events:
            if event.type == pygame.MOUSEMOTION:
                # reset zooms first
                for sprite in self.sprites:
                    if sprite in self.choice_sprites:
                        sprite.zoom = settings.DIALOGUE_AREA_CHOICES_DEFAULT_ZOOM
                # inflate the hover ones
                sprites = [s for s in self.choice_sprites if s.rect.collidepoint(event.pos)]
                for sprite in sprites:
                    sprite.zoom = settings.DIALOGUE_AREA_CHOICES_HIGHLIGHT_ZOOM
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if self.elapsed < 1.0:
                    return
                if event.button == 1:
                    for sprite in [s for s in self.choice_sprites if s.rect.collidepoint(event.pos)]:
                        self.answer = sprite.user_text

    def zooming(self, delta_time, events):
        self.text_sprite.zoom = self.tween.value

    def waiting_on_time(self, delta_time, events):
        if self.elapsed > 1.0 and self.choices is None:
            for e in events:
                if e.type == pygame.KEYDOWN:
                    if e.key in settings.KEY_SKIP_DIALOGUE:
                        self.done_waiting_on_time()
                        self.running = False
                        self.blocking = False
                        # print("----- SKIP KEY -----")
        self.waiting_on_input(delta_time, events)

    def waiting_on_input(self, delta_time, events):
        if self.answer is not None:
            self.doing = self.zooming
            self.tween = _Timer(1.0, 0.1, 0.15, self.done_closing, self.tweener)

    def done_opening(self, *args):
        self.tween.remove()
        self.doing = self.waiting_on_time
        # print('----- WAIT {} -----'.format(self.wait))
        self.tween = _Timer(0.0, 1.0, self.wait, self.done_waiting_on_time, self.tweener)

    def done_waiting_on_time(self, *args):
        self.tween.remove()
        if self.sound_channel and self.sound_channel.get_busy():
            self.sound_channel.fadeout(150)
        if self.choices is None:
            # print('----- DONE WAITING ON TIME -----')
            self.doing = self.zooming
            self.tween = _Timer(1.0, 0.1, 0.15, self.done_closing, self.tweener)
        else:
            self.doing = self.waiting_on_input

    def done_closing(self, *args):
        self.tween.remove()
        self.running = False
        self.blocking = False

    def is_finished(self):
        return False


class Ask(Say):
    """Just an alias to help script readability"""
    pass


class DramaticPause(_Command):
    def __init__(self, seconds):
        self.running = True
        self.blocking = True
        self.tweener = Tweener()
        self.tween = _Timer(0.0, 1.0, seconds, self.done_waiting, self.tweener)

    def done_waiting(self, *args):
        self.tween.remove()
        self.running = False
        self.blocking = False

    def think(self, delta_time, events):
        self.tweener.update(delta_time)

    def is_finished(self):
        return self.running


class Script(Context):
    def __init__(self, level, screen_title):
        self.level = level
        self.screen_title = screen_title

        w, h = settings.SCREEN_SIZE
        self.scheduler = pyknicpygame.pyknic.timing.Scheduler()
        self.cam = pyknicpygame.spritesystem.Camera(pygame.Rect(0, 0, w, h), position=Point2(w // 2, h // 2))
        self.renderer = pyknicpygame.spritesystem.DefaultRenderer()
        Globals.renderer = self.renderer

        # self.background = pygame.Color('lightblue2')
        self.background = pygame.display.get_surface().copy()
        order = ['r', 'g', 'b']
        random.seed()
        random.shuffle(order)
        from_color = pygame.Color('lightblue2')
        # setattr(from_color, order[0], random.randrange(180, 200))
        # setattr(from_color, order[1], random.randrange(120, 150))
        # setattr(from_color, order[2], random.randrange(120, 150))
        #  lightblue: 178, 223, 238
        to_color = pygame.Color(
            from_color.r - random.randrange(30, 40),
            from_color.g - random.randrange(30, 40),
            from_color.b - random.randrange(30, 40),
        )
        # setattr(to_color, order[0], getattr(from_color, order[0]) + random.randrange(50) - 25)
        # setattr(to_color, order[1], getattr(from_color, order[1]) + random.randrange(40) - 20)
        # setattr(to_color, order[2], getattr(from_color, order[2]) + random.randrange(40) - 20)
        x, y = settings.SCREEN_SIZE
        sys.stdout.flush()
        gradients.draw_gradient(self.background, (0, 0), (x, y), from_color, to_color)

        self._commands = self._gen_dialogue()
        self.elements = []
        self.unhandled_events = []
        self.running = True

    def update(self, delta_time):
        sound.play_intermission()
        self._handle_events()
        try:
            any_blocking = False
            dead = []
            for e in self.elements:
                if e.blocking:
                    any_blocking = True
                if e.running:
                    e.think(delta_time, self.unhandled_events)
                else:
                    dead.append(e)
            if not any_blocking:
                command = next(self._commands)
                if command:
                    command.think(delta_time, [])
                    self.elements.append(command)
                    if hasattr(command, 'sprites'):
                        self.renderer.add_sprites(command.sprites)
            for e in dead:
                try:
                    self.elements.remove(e)
                except ValueError:
                    pass
                if hasattr(e, 'sprites'):
                    self.renderer.remove_sprites(e.sprites)
        except StopIteration:
            self.running = False
            self.pop()

    def draw(self, screen, do_flip=True, interpolation_factor=1.0):
        screen.blit(self.background, (0, 0))
        self.renderer.draw(screen, self.cam, do_flip=False, interpolation_factor=interpolation_factor)
        if False:
            # Debug: left-right dialogue rects
            pygame.draw.rect(screen, pygame.Color('red'), settings.DIALOGUE_LEFT_AREA, 1)
            pygame.draw.rect(screen, pygame.Color('green'), settings.DIALOGUE_RIGHT_AREA, 1)
        if do_flip:
            pygame.display.flip()

    def _handle_events(self):
        del self.unhandled_events[:]
        for e in pygame.event.get():
            handled = False
            if e.type == pygame.KEYDOWN:
                if e.key == settings.KEY_CHEAT_EXIT:
                    self.pop()
                    handled = True
                elif e.key in settings.KEY_CHEAT_DIALOGUE_SELECTION:
                    script_class = {
                        pygame.K_1: Script1,
                        pygame.K_2: Script2,
                        pygame.K_3: Script3,
                        pygame.K_4: Script4,
                        pygame.K_5: Script5,
                        pygame.K_6: Script6,
                    }[e.key]
                    self.pop()
                    self.push(winner_script(e.key - pygame.K_1 + 1))
                elif e.key == settings.KEY_CHEAT_EXIT:
                    self.pop()
            elif e.type == pygame.QUIT:
                self.pop(self.get_stack_length())
            if not handled:
                self.unhandled_events.append(e)

    def _gen_dialogue(self):
        pass


# Doesn't work anymore
#
# class ScriptIntroTest(Script):
#     """Script for intro
#     """
#     def _gen_dialogue(self):
#         logger.debug('Script1: start ScriptIntro')
#         dialogue = Dialogue()
#         yield dialogue
#         hero = Character('<', 'Hero', GameResource.IMG_MATH_HERO, flipped=True)
#         yield hero
#         yield Say('<', 'Say, I say!')
#         joe = Character('>', 'Joe', GameResource.IMG_MATH_ZOMBIE, flipped=True)
#         yield joe
#         yield Say('>', 'Hellooooo over there!')
#         ask = Ask('<', 'Do you have any bananas?', choices=('Yes', 'No'))
#         yield ask
#         if ask.answer == 'No':
#             yield Say('>', 'No.')
#             yield Say('<', "I wasn't expecting that!", choices=('Goodbye',))
#         else:
#             yield Say('>', 'Yes.')
#             yield Say('<', "Ha! I knew it, banana breath!", choices=('Goodbye',))
#
#         # Close hero and joe concurrently. Allow hero and joe to block while animating closure (yield None).
#         hero.close()
#         joe.close()
#         yield None
#
#         dialogue.close()
#         logger.debug('Script1: end ScriptIntro')
#         yield None


class ScriptIntro(Script):
    """Script for intro

    Professor punishes you with detention
    """
    def _gen_dialogue(self):
        logger.debug('Script1: start ScriptIntro')
        dialogue = Dialogue(self.level, 'INTRODUCTION')
        yield dialogue

        professor = Character('<', 'Professor', GameResource.IMG_MATH_PROFESSOR, flipped=True)
        yield professor

        yield professor.say('Happy Friday, class.', choices=['Happy Friday, Professor'])
        yield professor.say('Yesterday we learned that 1 + 1 = (_) ?')

        joe = Character('>', 'Joe', GameResource.IMG_MATH_ZOMBIE, flipped=True)
        yield joe
        yield joe.say('2!')
        yield joe.close()

        yield professor.say('Very good!')
        yield professor.say('And 1 + 1 = 2 (_) 1 ?')

        joe = Character('>', 'Joe', GameResource.IMG_MATH_ZOMBIE, flipped=True)
        yield joe
        yield joe.say('Minus!')
        yield joe.close()

        yield professor.say('VERY good!')
        yield professor.say('And 2 + 2 = (_) ?')

        hero = Character('>', 'Me', GameResource.IMG_MATH_HERO, flipped=False)
        yield hero
        yield hero.say('Oo! Oo!', choices=["It's 3!!"])
        yield hero.say("No, I mean...", choices=["It's 1...", "No, 2!", "What was the question?"])

        yield DramaticPause(1.0)
        # yield professor.say('Young man, stop embarrassing us.')
        yield professor.say('Young man, stop embarrassing us. If only you would do the assignments...')
        yield professor.say('Very well. I shall insure you do them.')
        yield professor.say('In DETENTION!')

        yield hero.say('NOOOO!!!!')
        yield professor.close()
        yield hero.close()

        yield DramaticPause(1.0)
        joe = Character('>', 'Joe', GameResource.IMG_MATH_ZOMBIE, flipped=True)
        yield joe
        yield joe.say('*GLOATING*')
        yield joe.close()

        dialogue.close()
        logger.debug('Script1: end ScriptIntro')
        yield None


class Script1(Script):
    """Script to play after completing game room 1

    Professor sees ex-zombie
    """
    def _gen_dialogue(self):
        dialogue = Dialogue(self.level, self.screen_title)
        yield dialogue

        professor = Character('<', 'Professor', GameResource.IMG_MATH_PROFESSOR, flipped=True)
        yield professor
        yield professor.say('You, boy. I want to speak to you.')

        joe = Character('>', 'Joe', GameResource.IMG_MATH_EX_ZOMBIE, flipped=True)
        yield joe
        yield joe.say('Yes, professor?')

        yield professor.say('Come closer. You look...')
        yield professor.say('...different.')
        yield professor.say('A light in your eye.')
        yield professor.say('Is there anything you want to tell me?')

        yield joe.say('We had a good tutoring session today.')
        yield professor.say('You may go.')
        yield joe.close()

        yield DramaticPause(2.0)
        yield professor.say(""""Good tutoring?" You don't say...""")
        yield DramaticPause(1.0)
        yield professor.say("The tutor defeated my hypnosis!?")
        yield professor.close()

        yield dialogue.close()


class Script2(Script):
    """Script to play after completing game room 2

    Professor and hero question their suspicions
    """
    def _gen_dialogue(self):
        dialogue = Dialogue(self.level, self.screen_title)
        yield dialogue

        professor = Character('<', 'Professor', GameResource.IMG_MATH_PROFESSOR, flipped=True)
        yield professor
        yield professor.say('What is up with all these boys?')
        yield professor.say('They are forgetting their lessons.')
        yield professor.say('I wonder...')
        yield professor.say('Is the tutor doing it?')
        yield professor.close()

        hero = Character('>', 'Me', GameResource.IMG_MATH_HERO, flipped=False)
        yield hero
        yield hero.say('What is up with all these boys?')
        yield hero.say('They bully me with math.')
        yield hero.say('And then they change their minds.')
        yield hero.say('I wonder...')
        yield hero.say('Is the professor doing it?')
        yield hero.close()

        yield dialogue.close()


class Script3(Script):
    """Script to play after completing game room 3

    Professor investigates his suspicion
    """
    def _gen_dialogue(self):
        dialogue = Dialogue(self.level, self.screen_title)
        yield dialogue

        professor = Character('<', 'Professor', GameResource.IMG_MATH_PROFESSOR, flipped=True)
        yield professor
        yield professor.say('You, boy. I want to speak to you.')

        hero = Character('>', 'Me', GameResource.IMG_MATH_HERO, flipped=False)
        yield hero
        yield hero.say('Yes, professor?')

        yield professor.say('What is 1 + 1 ?')
        yield hero.say('*chewing lip*', choices=['1?', 'Is this a trick question?'])
        yield professor.say("Don't get wise with me.")
        yield hero.say("Please don't give me any more detention!")
        yield professor.say("Who is your tutor?")
        yield hero.say("Huh? I don't have a tutor.")
        yield professor.say("1 * 2 !")
        yield hero.say("It's...", choices=['3?'])
        yield professor.say("2 * 1 !")
        yield hero.say("It's...", choices=['4?'])
        yield professor.say("2 - 2 !")
        yield hero.say("It's...", choices=['...detention?'])
        yield professor.say("Yes.")
        yield professor.say("Now.")

        yield hero.close()

        yield DramaticPause(4.0)
        yield professor.say("Impossible. He is thick as a brick. But who is sabotaging my work...?")
        yield professor.close()

        yield dialogue.close()


class Script4(Script):
    """Script to play after completing game room 4

    Professor and hero throw down
    """
    def _gen_dialogue(self):
        dialogue = Dialogue(self.level, self.screen_title)
        yield dialogue

        professor = Character('<', 'Professor', GameResource.IMG_MATH_PROFESSOR, flipped=True)
        yield professor
        joe = Character('>', 'Joe', GameResource.IMG_MATH_ZOMBIE, flipped=True)
        yield joe

        yield professor.say("You will use math every day of your life.")
        yield professor.say("You love Math. It is your friend.")
        yield joe.say("Yes, Master.")
        yield professor.say('132 + 41 + 739 = 1035 - 123.')
        yield joe.say('132 + 41 + 739 = 1035 - 123.')
        yield professor.say('1022 / 511 = 8848 / 4424.')
        yield joe.say('1022 / 511 = 8848 / 4424.')
        yield joe.close()

        hero = Character('>', 'Me', GameResource.IMG_MATH_HERO, flipped=False)
        yield hero
        yield DramaticPause(1.0)
        yield hero.say("It's you!")
        yield professor.say("It's... YOU?")
        yield hero.say("I will stop you!")
        yield professor.say("1 + 1 !")
        yield professor.close()
        yield DramaticPause(1.0)
        yield hero.say("Rats...!", choices=['2!'])
        yield hero.close()

        yield dialogue.close()


class Script5(Script):
    """Script to play after completing game room 5

    Boss encounter
    """
    def _gen_dialogue(self):
        dialogue = Dialogue(self.level, self.screen_title)
        yield dialogue

        hero = Character('<', 'Me', GameResource.IMG_MATH_HERO, flipped=True)
        yield hero
        yield hero.say('I WIN!')

        professor = Character('>', 'Professor', GameResource.IMG_MATH_PROFESSOR, flipped=False)
        yield professor
        yield professor.say("You fool!! You've won nothing! I will go to a new school! Haha-HA-ha-HA-hahaaa!!!!!")

        yield hero.say('22 / 7 !')
        yield professor.say("Wait...")
        yield professor.say("I know this one...")
        yield professor.say("3.142857142857143...")

        yield hero.say("I don't need to know your dark arts. You are the one who is bound by them.")
        yield professor.say("...carry the remainder and divide by 7 again...")

        yield hero.say("Sing with me! Oh it's the song that never ends...")
        yield professor.say("Grrr. En garde!")
        hero.close()
        professor.close()
        yield None

        yield dialogue.close()


class Script6(Script):
    """Script to play after completing game room 6

    Boss encounter
    """
    def _gen_dialogue(self):
        dialogue = Dialogue(self.level, self.screen_title)
        yield dialogue

        hero = Character('<', 'Me', GameResource.IMG_GUY, flipped=True)
        yield hero
        yield hero.say('Zzz-zz-zzz - *SNORT* Riposte! What!?')
        yield hero.say('Phew. It was only a dream. :)')

        yield dialogue.close()


loser_phrases = [
    "I ran away. What would Mommy say? I have to go back to classroom {}!",
    "Mummieee, I got a booboo... Try, try again. To classroom {}!",
    "Oh, I'm a LOSER. I have to try classroom {} again.",
    "You dirty rat! You'll pay for that. Classroom {} will be mine!",
    "I will be the boss of detention! Back to classroom {}.",
]


class ScriptLoser(Script):
    """Script to play after completing game room 5 (optional)

    Player loses a combat and flees the classroom
    """
    def _gen_dialogue(self):
        dialogue = Dialogue(self.level, self.screen_title)
        yield dialogue

        hero = Character('<', 'Me', GameResource.IMG_MATH_HERO, flipped=True)
        yield hero

        phrase = loser_phrases.pop(0)
        loser_phrases.append(phrase)
        yield hero.say(phrase.format(self.level))
        yield hero.say("Maybe I should go to all my detention-mates and greet them before the end of class.")

        yield hero.close()

        yield dialogue.close()


class ScriptReplay(Script):
    """Script to play after completing game room 5 (optional)

    Player loses a combat and flees the classroom
    """
    def _gen_dialogue(self):
        dialogue = Dialogue(self.level, self.screen_title)
        yield dialogue

        hero = Character('<', 'Me', GameResource.IMG_MATH_HERO, flipped=True)
        yield hero

        yield hero.say("Yeeha! That was fun for old time's sake!")

        yield hero.close()

        yield dialogue.close()


def winner_script(level_id):
    scripts = [
        ScriptIntro,
        Script1,
        Script2,
        Script3,
        Script4,
        Script5,
        Script6,
    ]
    script_class = scripts[level_id]
    if level_id == 6:
        return script_class(level_id, 'CLASSROOM {} GAME WON'.format(level_id))
    else:
        return script_class(level_id, 'CLASSROOM {} BEATEN'.format(level_id))


def loser_script(level_id):
    script_class = ScriptLoser
    return script_class(level_id, 'CLASSROOM {} BEAT YOU'.format(level_id))


def replay_script(level_id):
    script_class = ScriptReplay
    return script_class(level_id, 'CLASSROOM {} REPLAYED'.format(level_id))
