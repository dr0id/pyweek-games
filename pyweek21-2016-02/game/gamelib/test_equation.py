# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek21-2016-02
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function
import unittest
import equation as mut  # module under test

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id [at] gmail [dot] com"
__copyright__ = "DR0ID @ 2016"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

if mut.__version_info__ != __version_info__:
    raise Exception("version numbers do not match!")


class EquationTests(unittest.TestCase):
    def test_equation_return_value_False(self):
        # arrange
        equation = mut.Equation('1 + 1 = ?', {-1: '2'}, ['3', '4', '5'])

        # act
        actual = equation.check('3')

        # verify
        self.assertFalse(actual)

    def test_equation_return_value_True(self):
        # arrange
        equation = mut.Equation('1 + 1 = ?', {-1: '2'}, ['3', '4', '5'])

        # act
        actual = equation.check('2')

        # verify
        self.assertTrue(actual)

    def test_equation_check_True_removes_missing(self):
        # arrange
        equation = mut.Equation('1 + 1 = ?', {-1: '2'}, ['3', '4', '5'])

        # act
        equation.check('2')

        # verify
        self.assertEqual(0, len(equation.missing))

    def test_equation_check_True_adds_to_done(self):
        # arrange
        equation = mut.Equation('1 + 1 = ?', {-1: '2'}, ['3', '4', '5'])

        # act
        equation.check('2')

        # verify
        self.assertEqual(1, len(equation.done))

    def test_equation_check_with_unknown_choice_fails(self):
        # arrange
        equation = mut.Equation('1 + 1 = ?', {-1: '2'}, ['3', '4', '5'])

        # act
        actual = equation.check('99')

        # verify
        self.assertFalse(actual)

    def test_equation_missing_multiple_check_first(self):
        # arrange
        equation = mut.Equation("1 + 1 ? 3 = ?", {-1: '-1', 3: '-'}, ['1', '3', '4', '6', '+'])

        # act
        actual = equation.check('-')

        # verify
        self.assertTrue(actual)

    def test_equation_missing_multiple_check_is_done_False(self):
        # arrange
        equation = mut.Equation("1 + 1 ? 3 = ?", {-1: '-1', 3: '-'}, ['1', '3', '4', '6', '+'])

        # act
        equation.check('-')

        # verify
        self.assertFalse(equation.is_done())

    def test_equation_missing_multiple_check_is_done_True(self):
        # arrange
        equation = mut.Equation("1 + 1 ? 3 = ?", {-1: '-1', 3: '-'}, ['1', '3', '4', '6', '+'])

        # act
        equation.check('-')
        equation.check('-1')

        # verify
        self.assertTrue(equation.is_done())

    def test_equation_missing_multiple_check_replaces_term(self):
        # arrange
        equation = mut.Equation("1 + 1 ? 3 = ?", {-1: '-1', 3: '-'}, ['1', '3', '4', '6', '+'])
        expected = '1 + 1 ? 3 = -1'.split()

        # act
        equation.check('-1')

        # verify
        self.assertEqual(expected, equation.equation_as_chars)

    def test_equation_property_is_combined_correctly(self):
        # arrange
        equation = mut.Equation("feet - shoes = ?", {-1: 'smellyFeet'}, ['1', '3', '4', '6', '+'])
        expected = 'feet - shoes = smellyFeet'

        # act
        actual = equation.check("smellyFeet")

        # verify
        self.assertEqual(expected, equation.equation)

    def test_equation_property_is_combined_correctly_with_spaces_in_missing(self):
        # arrange
        # equation = mut.Equation("feet;-;shoes;=;?", {-1: "smelly Feet"}, ['1', '3', '4', '6', '+'], separator=';')
        equation = mut.Equation("feet - shoes = ?", {-1: "smelly Feet"}, ['1', '3', '4', '6', '+'])
        expected = "feet - shoes = 'smelly Feet'"

        # act
        equation.check("smelly Feet")

        # verify
        self.assertEqual(expected, equation.equation)

    def test_equation_property_is_combined_correctly_with_spaces_in_equation(self):
        # arrange
        # equation = mut.Equation("feet;-;shoes;=;?", {-1: "smelly Feet"}, ['1', '3', '4', '6', '+'], separator=';')
        equation = mut.Equation("feet - ? = 'smelly feet'", {2: "shoes"}, ['1', '3', '4', '6', '+'], separator=' ')
        expected = "feet - shoes = 'smelly feet'"

        # act
        equation.check("shoes")

        # verify
        self.assertEqual(expected, equation.equation)

    def test_equation_property_is_combined_correctly_with_spaces_in_equation_and_using_separator(self):
        # arrange
        # equation = mut.Equation("feet;-;shoes;=;?", {-1: "smelly Feet"}, ['1', '3', '4', '6', '+'], separator=';')
        equation = mut.Equation("feet;-;?;=;smelly feet", {2: "shoes"}, ['1', '3', '4', '6', '+'], separator=';')
        expected = "feet - shoes = 'smelly feet'"

        # act
        equation.check("shoes")

        # verify
        self.assertEqual(expected, equation.equation)

    # def test_equation_property_is_combined_correctly_with_spaces_in_equation_and_using_separator2(self):
    #     # arrange
    #     # equation = mut.Equation("feet;-;shoes;=;?", {-1: "smelly Feet"}, ['1', '3', '4', '6', '+'], separator=';')
    #     equation = mut.Equation("feet;-;shoes;=;?", {-1: 'smelly feet'}, ['feet', '-2.5', '', '3'], separator=";")
    #     expected = "feet - shoes = 'smelly feet'"
    #
    #     # act
    #     equation.check("'smelly feet'")
    #
    #     # verify
    #     self.assertEqual(expected, equation.equation)

    def test_equation_with_missing_pieces_on_both_sides(self):
        # arrange
        equation = mut.Equation("4 ? 1 = ? + 2", {-3: '3', 1: '+'}, ['0', '3', '4', '6', '5'])

        # act
        actual1 = equation.check('+')
        actual2 = equation.check('3')

        # verify
        self.assertTrue(actual1)
        self.assertTrue(actual2)
        self.assertTrue(equation.is_done())


if __name__ == '__main__':
    unittest.main()
