# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek21-2016-02
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import random

import settings
from gameresource import GameResource
from pyknicpygame.pyknic.mathematics import Point2, Vec2
from pyknicpygame.pyknic.statemachines import SimpleStateMachine
from pyknicpygame.pyknic.timing import Scheduler

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2016"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 

random.seed(settings.UUID_FOR_RANDOM)


class BaseState(object):
    @staticmethod
    def enter(sm):
        pass

    @staticmethod
    def exit(sm):
        sm.stop_timer()

    @staticmethod
    def update(sm, delta_time):
        pass

    @staticmethod
    def on_time_elapsed(sm):
        return 0


class MoveState(BaseState):
    @staticmethod
    def enter(sm):
        length = random.randint(*settings.ZOMBIE_MOVE_RANGE)
        sm.target = sm.position + sm.direction * length
        border = settings.CLASS_ROOM_BORDER
        if sm.target.x < 0 + border:
            sm.switch_state(DirectionState)
            # sm.target.x = border
        if sm.target.x > settings.SCREEN_WIDTH - border:
            sm.switch_state(DirectionState)
            # sm.target.x = settings.SCREEN_WIDTH - border
        if sm.target.y < 0 + border:
            sm.switch_state(DirectionState)
            # sm.target.y = border
        if sm.target.y > settings.SCREEN_HEIGHT - border:
            sm.switch_state(DirectionState)
            # sm.target.y = settings.SCREEN_HEIGHT - border
        sm.start_animation()

    @staticmethod
    def exit(sm):
        sm.stop_animation()

    @staticmethod
    def update(sm, delta_time):
        diff = sm.target - sm.position
        diff_length = diff.length
        if diff_length > 5:
            sm.position += diff * sm.speed * delta_time * 1.0 / (diff_length + 1)
        else:
            if random.random() < settings.ZOMBIE_MOVE_STATE_TO_WAIT_STATE_CHANCE:
                sm.switch_state(WaitState)
            else:
                sm.switch_state(DirectionState)


class DirectionState(BaseState):
    @staticmethod
    def enter(sm):
        diff = random.randint(*settings.ZOMBIE_ROTATION_RANGE)
        sm.target_angle = sm.direction.angle + diff
        sm.target_angle %= 360

    @staticmethod
    def update(sm, delta_time):
        diff = sm.target_angle - sm.direction.angle % 360
        delta_angle = diff * delta_time * sm.rotation_speed
        if abs(diff) > 0.5 and abs(delta_angle) < 360:
            sm.direction.rotate(delta_angle)
        else:
            if random.random() < settings.ZOMBIE_DIRECTION_STATE_TO_WAIT_STATE_CHANCE:
                sm.switch_state(WaitState)
            else:
                sm.switch_state(MoveState)


class WaitState(BaseState):
    @staticmethod
    def enter(sm):
        sm.start_timer(random.randint(*settings.ZOMBIE_WAIT_DURATION_RANGE))

    @staticmethod
    def on_time_elapsed(sm):
        if random.random() < settings.ZOMBIE_WAIT_STATE_TO_MOVE_STATE_CHANCE:
            sm.switch_state(MoveState)
        else:
            sm.switch_state(DirectionState)
        return 0


class FleeState(BaseState):
    @staticmethod
    def enter(sm):
        sm.sprite.set_image(GameResource.get(GameResource.IMG_PLAYER_00))
        diff = sm.door_position - sm.position
        sm.direction = diff.normalized
        sm.start_animation()

    @staticmethod
    def update(sm, delta_time):
        sm.position += sm.direction * sm.speed * delta_time  # TODO: run speed??


class Zombie(SimpleStateMachine):
    scheduler = Scheduler()
    _animation_interval = settings.ZOMBIE_ANIMATION_INTERVAL
    _zombie_images = None
    _converted_images = None

    def __init__(self, combat_data, door_position):
        if Zombie._zombie_images is None:
            Zombie._zombie_images = [
                GameResource.get(GameResource.IMG_ZOMBIE_00),
                GameResource.get(GameResource.IMG_ZOMBIE_01),
                GameResource.get(GameResource.IMG_ZOMBIE_02),
                GameResource.get(GameResource.IMG_ZOMBIE_03),
            ]

            Zombie._converted_images = [
                GameResource.get(GameResource.IMG_PLAYER_00),
                GameResource.get(GameResource.IMG_PLAYER_01),
                GameResource.get(GameResource.IMG_PLAYER_02),
                GameResource.get(GameResource.IMG_PLAYER_03),
            ]

        self._images = Zombie._zombie_images

        self.radius = combat_data.radius
        self.combat_data = combat_data
        self.position = combat_data.position.clone()
        self.direction = Vec2(1.0, 0.0)
        self.direction.rotate_to(combat_data.angle)
        self.timer_id = -1
        self._animation_timer_id = -1
        self._animation_idx = 1

        self.sprite = None
        self.target = Point2(0, 0)
        self.speed = settings.ZOMBIE_SPEED
        self.target_angle = combat_data.angle
        self.rotation_speed = settings.ZOMBIE_ROTATION_SPEED
        self.door_position = door_position
        SimpleStateMachine.__init__(self, MoveState)

    def update(self, delta_time):
        self.current_state.update(self, delta_time)

    def start_timer(self, delay):
        self.timer_id = Zombie.scheduler.schedule(self.on_time_elapsed, delay)

    def on_time_elapsed(self):
        self.current_state.on_time_elapsed(self)

    def stop_timer(self):
        try:
            Zombie.scheduler.remove(self.timer_id)
        except:
            pass

    def convert(self):
        self.switch_state(FleeState)
        self._images = Zombie._converted_images

    def is_converted(self):
        return self.current_state == FleeState

    def start_animation(self):
        self._animation_timer_id = Zombie.scheduler.schedule(self.update_animation, self._animation_interval)

    def stop_animation(self):
        Zombie.scheduler.remove(self._animation_timer_id)

    def update_animation(self):
        self.sprite.set_image(self._images[self._animation_idx])
        self._animation_idx += 1
        self._animation_idx %= len(self._images)
        return self._animation_interval
