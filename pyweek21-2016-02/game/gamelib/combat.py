# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek21-2016-02
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import random

from levels import MathZombieCombatData, PlayerMathSanityData

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2016"
__credits__ = ["DR0ID"]     # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []    # list of public visible parts of this module


# Done: could be better with hypnosis popups, but there is the bar indicating
# there is something missing, somehow the player needs to feel he is loosing if providing the wrong answer....

# I think it should be in a way, that if you start a combat and do nothing, then after some time you should be
# hypnotized and you have to recover first (this leads to a time constrained solution as the only solution)
#
# option 1 (time constraint): your hypnosis resistance is decreasing over time, when reaching critical level you get
# hypnotized for some amount of time (spiraling background!? MUUUUUAHAHAHA!)
#
# option 2 (punish and reward): for each error a (health-?)point is taken, for each or two equations you
# solve correctly you get a (health-?)point back  (would probably need some balancing!)
#
# option 3 (????)


class Combat(object):

    def __init__(self, combat_data, player_data):
        self.combat_data = combat_data
        self._current_equation_idx = 0
        for eq in combat_data.equations:
            if eq.is_done():
                self._current_equation_idx += 1
        self.player_data = player_data
        self._state = None

    @property
    def equations(self):
        return self.combat_data.equations

    @property
    def current_equation(self):
        return self.current_equation_object.equation

    @property
    def current_equation_object(self):
        return self.combat_data.equations[self._current_equation_idx]

    @property
    def current_choices(self):
        return self.current_equation_object.choices

    def attack_hypnotic_zombie(self, choice):
        if self.current_equation_object.check(choice):
            # we have a match
            # update choices and equation
            if self.current_equation_object.is_done():
                # move to next equation
                self._current_equation_idx += 1
                if self._current_equation_idx == len(self.combat_data.equations):
                    self._current_equation_idx = len(self.combat_data.equations) - 1
                    self._state = True if self._state is None else self._state
            else:
                # still on same equation, maybe remove that choice?
                pass

            self.player_data.decrease_hypnosis_level(self.player_data.hypnosis_decrease_rate_per_second)
            return True  # attack was successful
        else:
            # failure
            random.shuffle(self.current_equation_object.choices)
            # self.player_data.hypnosis_level += 1
            return False  # attack was unsuccessful

    def update(self, delta_time):
        # no clamping here, the higher this gets the longer it takes to get to normal state
        self.player_data.hypnosis_level += self.combat_data.hypnosis_increase_rate_per_second * delta_time

    def player_has_won(self):
        return self._state is True

    def is_in_progress(self):
        if self.player_data.hypnosis_level >= self.player_data.hypnosis_threshold:
            self._state = False if self._state is None else self._state
        return self._state is None

if __name__ == '__main__':
    combat = Combat(MathZombieCombatData(), PlayerMathSanityData())

    while combat.is_in_progress():
        print()
        print("hypnosis: {0}/{1}".format(combat.player_data.hypnosis_level, combat.player_data.hypnosis_threshold))
        print(combat.current_equation)
        print('choices:', combat.current_choices)
        reading = input()
        attack_result = combat.attack_hypnotic_zombie(reading)
        if attack_result:
            # won!
            print("aaarg, I'm getting weaker! What is happening?")
        elif attack_result is False:
            print('harr harr! This was wrong. I will get you!')

    if combat.player_has_won():
        print("Uh! Oh! Where am I? I go home now.")
    else:
        print("MMUUUAHAHA! I got you!")
