# -*- coding: utf-8 -*-

import pygame

import settings
from gameresource import GameResource


class Song(object):
    now_playing = None


def play_intermission():
    if not settings.PLAY_MUSIC:
        return
    song_id = GameResource.SONG_INTERMISSION
    if Song.now_playing not in (None, song_id):
        # print('Fading action song: returning')
        Song.now_playing = None
        pygame.mixer.music.fadeout(1000)
        return
    if pygame.mixer.music.get_busy():
        # print('Busy: returning')
        return
    Song.now_playing = song_id
    song = GameResource.get(song_id)
    # print('Loading song: {}'.format(song.name))
    pygame.mixer.music.load(song.file_name)
    pygame.mixer.music.play()
    pygame.mixer.music.set_volume(song.volume)


def play_action_song():
    if not settings.PLAY_MUSIC:
        return
    if Song.now_playing == GameResource.SONG_INTERMISSION and pygame.mixer.music.get_busy():
        # print('Fading intermission song: returning')
        Song.now_playing = None
        pygame.mixer.music.fadeout(250)
        return
    if pygame.mixer.music.get_busy():
        # print('Busy: returning')
        return
    song_id = Song.now_playing
    if song_id is None:
        song_id = GameResource.song_queue.pop(0)
        GameResource.song_queue.append(song_id)
        Song.now_playing = song_id
    song = GameResource.get(song_id)
    # print('Loading song: {}'.format(song.name))
    pygame.mixer.music.load(song.file_name)
    pygame.mixer.music.play()
    pygame.mixer.music.set_volume(song.volume)


def remove_intro():
    global songs
    songs = [s for s in songs if s.name != 'Rise of an Empire']
