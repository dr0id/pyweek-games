# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek21-2016-02
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import pygame

import pyknicpygame.pyknic.context as context
import pyknicpygame.pyknic.timing
import pyknicpygame.resource
import settings
# import spritesystem
# import tweening
from pyknicpygame import spritesystem
from pyknicpygame.pyknic import tweening
from combat import Combat
from gameresource import GameResource
# from mathematics import Point2
from pyknicpygame.pyknic.mathematics import Point2
from pyknicpygame.pyknic.context import Context
import sound

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2016"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"


# __all__ = []  # list of public visible parts of this module


def _render_string(text, use_big_font=False):
    id__ = GameResource.FONT_CHALK_IT_UP_BIG_ID if use_big_font else GameResource.FONT_CHALK_IT_UP_ID
    font = pyknicpygame.resource.resources.get_resource(id__)
    surf = font.render(text.replace('_', ' '), 0, settings.COMBAT_AREA_TEXT_COLOR)
    return surf


def _render_equations(equation, is_current_equation):
    return _render_string(equation.equation, is_current_equation)


class CombatContext(Context):
    def __init__(self, zombie, player_sanity_data):
        self._equation_sprites = []
        self._choice_sprites = []
        self._selected_sprite = None
        self._current_equation_sprite = None
        self._bar_sprite = None
        self.zombie = zombie

        # dependencies, should be provided by the constructor
        self.combat = Combat(zombie.combat_data, player_sanity_data)
        screen_rect = pygame.Rect(0, 0, settings.SCREEN_WIDTH, settings.SCREEN_HEIGHT)
        self.cam = spritesystem.Camera(screen_rect, position=Point2(screen_rect.centerx, screen_rect.centery))
        self.scheduler = pyknicpygame.pyknic.timing.Scheduler()
        self.renderer = spritesystem.DefaultRenderer()
        self.tweener = tweening.Tweener()
        self.update = self._update

    def enter(self):
        self._update_equations_and_choices()
        self._add_background()
        self._add_character_sprites()
        self._add_bar()
        self._start_update_timers()

        if settings.COMBAT_AREA_DEBUG_RENDER_ON:
            self._add_area_debug_render_sprite()

    # noinspection PyArgumentList
    def _add_area_debug_render_sprite(self):
        surf = pygame.Surface(settings.COMBAT_AREA.size).convert_alpha()
        surf.fill((255, 255, 0, 50))
        position = Point2(settings.COMBAT_AREA.left, settings.COMBAT_AREA.top)
        spr = spritesystem.Sprite(surf, position, anchor='topleft', z_layer=-1)
        self.renderer.add_sprite(spr)

    def _add_character_sprites(self):
        _border = settings.COMBAT_AREA_BORDER
        _hero_img = GameResource.get(GameResource.IMG_MATH_HERO).convert_alpha()
        # _hero_img.fill((255, 255, 0, 50))
        _hero_position = Point2(settings.COMBAT_AREA.right - _border, settings.COMBAT_AREA.top + _border)
        _hero_spr = spritesystem.Sprite(_hero_img, _hero_position, anchor='topright')
        self.renderer.add_sprite(_hero_spr)
        _zombie_img = GameResource.get(GameResource.IMG_MATH_ZOMBIE).convert_alpha()
        # _zombie_img.fill((255, 255, 0, 50))
        _zombie_position = Point2(settings.COMBAT_AREA.left + _border, settings.COMBAT_AREA.top + _border)
        _zombie_spr = spritesystem.Sprite(_zombie_img, _zombie_position, anchor='topleft')
        self.renderer.add_sprite(_zombie_spr)

    def _add_background(self):
        background_img = GameResource.get(GameResource.IMG_CHALKBOARD).convert_alpha()
        position = Point2(settings.COMBAT_AREA.centerx, settings.COMBAT_AREA.centery)
        background = spritesystem.Sprite(background_img, position, anchor='center', z_layer=-10)
        background.zoom = 1.6
        self.renderer.add_sprite(background)

    def draw(self, screen, do_flip=True, interpolation_factor=1.0):
        # assume there is yet another context on the stack
        next_context = context.top(1)
        next_context.draw(screen, do_flip=False, interpolation_factor=interpolation_factor)

        # draw here this context
        self.renderer.draw(screen, self.cam, fill_color=None, do_flip=True,
                           interpolation_factor=interpolation_factor)

    def _update(self, delta_time):
        sound.play_action_song()
        self._update_timed(delta_time)

        if self.combat.is_in_progress():
            self.check_events_for_attack()

            self._update_hypnosis_bar()

        else:
            if self.combat.player_has_won():
                # TODO: convert snap zombie out of hypnosis
                GameResource.get(GameResource.SOUND_NO_HYPNO).play()
            else:
                # TODO: player lost, let him in hypnosis
                GameResource.get(GameResource.SOUND_HYPNO).play()

            # start end timer and change update method to _update_timed to prevent doing win/loose action more than once
            self.scheduler.schedule(self._timer_leave_combat, settings.COMBAT_LEAVE_DELAY)

            # noinspection PyAttributeOutsideInit
            self.update = self._update_timed

    def has_player_won(self):
        return self.combat.player_has_won()

    # noinspection PyUnusedLocal
    def _timer_leave_combat(self, *args, **kwargs):
        self.pop()  # pop itself
        return 0  # stop scheduling this method in timer

    def check_events_for_attack(self):
        choice = None
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONUP and event.button == settings.LEFT_MOUSE_BUTTON:
                sprites = self.renderer.get_sprites_at_tuple(event.pos)
                if sprites:
                    sprite = sprites[0]  # topmost sprite
                    if sprite in self._choice_sprites and sprite == self._selected_sprite:
                        choice = sprite.term
            elif event.type == pygame.MOUSEBUTTONDOWN and event.button == settings.LEFT_MOUSE_BUTTON:
                sprites = self.renderer.get_sprites_at_tuple(event.pos)
                if sprites:
                    sprite = sprites[0]  # topmost sprite
                    if sprite in self._choice_sprites:
                        self._selected_sprite = sprite
            elif event.type == pygame.MOUSEMOTION:
                # reset zooms first
                for sprite in self.renderer.get_sprites():
                    if sprite in self._choice_sprites:
                        sprite.zoom = settings.COMBAT_AREA_CHOICES_DEFAULT_ZOOM
                # inflate the hove ones
                sprites = self.renderer.get_sprites_at_tuple(event.pos)
                for sprite in sprites:
                    if sprite in self._choice_sprites:
                        sprite.zoom = settings.COMBAT_AREA_CHOICES_HIGHLIGHT_ZOOM

        if choice:
            attack_result = self.combat.attack_hypnotic_zombie(choice)
            GameResource.get(GameResource.SOUND_CHALK_WORD).play()
            if attack_result:
                self._move_choice_to_equation(self._update_equations_and_choices)
            else:
                # bad, wrong choice
                self._move_choice_to_equation(self._tween_bad_attack_action_penalty)

    # noinspection PyUnusedLocal
    def _tween_bad_attack_action_penalty(self, *args, **kwargs):
        change = 0.0
        duration = settings.COMBAT_BAD_ATTACK_SHAKE_DURATION
        range_tuple = settings.COMBAT_SHAKE_DISTANCE_RANGE
        self.tweener.create_tween(self.cam.position, 'x', self.cam.position.x, change, duration,
                                  tweening.RANDOM_INT_BOUNCE,
                                  range_tuple, cb_end=self._tween_shutter_end)
        self.tweener.create_tween(self.cam.position, 'y', self.cam.position.y, change, duration,
                                  tweening.RANDOM_INT_BOUNCE,
                                  range_tuple)
        self._update_hypnosis_bar()
        self._update_equations_and_choices()

    def _update_hypnosis_bar(self):
        factor = 1.0 / self.combat.player_data.hypnosis_threshold * self.combat.player_data.hypnosis_level
        self._bar_sprite.area.width = int(factor * self._bar_sprite.bar_width)

    # noinspection PyUnusedLocal
    def _tween_shutter_end(self, *args, **kwargs):
        self._reset_cam_position()

    # noinspection PyUnusedLocal
    def _update_equations_and_choices(self, *args, **kwargs):
        self._update_equations()
        self._update_choices()

    def _update_equations(self):
        self.renderer.remove_sprites(self._equation_sprites)
        del self._equation_sprites[:]

        _distance = settings.COMBAT_AREA_ROW_DISTANCE
        _border = settings.COMBAT_AREA_BORDER
        _cur_y_pos = settings.COMBAT_AREA.top + settings.COMBAT_GUY_HEIGHT + _border

        for idx, equation in enumerate(self.combat.equations):
            equation_surf = _render_equations(equation, equation == self.combat.current_equation_object)
            _position = Point2(settings.COMBAT_AREA.left + _border, _cur_y_pos + _distance)
            _cur_y_pos += equation_surf.get_height()
            spr = spritesystem.Sprite(equation_surf, _position, anchor='topleft', z_layer=7)
            self._equation_sprites.append(spr)
            if equation.equation == self.combat.current_equation:
                self._current_equation_sprite = spr

        self.renderer.add_sprites(self._equation_sprites)

    def _update_choices(self):
        self.renderer.remove_sprites(self._choice_sprites)
        del self._choice_sprites[:]

        _border = settings.COMBAT_AREA_BORDER
        _distance = settings.COMBAT_AREA_CHOICE_DISTANCE
        # 1024x768 vs 1024x700 hack
        if settings.SCREEN_HEIGHT == 768:
            _cur_y_pos = settings.COMBAT_AREA.top + settings.COMBAT_GUY_HEIGHT + _border
        else:
            _cur_y_pos = settings.COMBAT_AREA.top + settings.COMBAT_GUY_HEIGHT - 30

        for choice in self.combat.current_choices:
            choice_surf = _render_string(choice, True)
            _position = Point2(settings.COMBAT_AREA.centerx + 4 * _border, _cur_y_pos + _distance)
            _cur_y_pos += choice_surf.get_height()
            spr = spritesystem.Sprite(choice_surf, _position, z_layer=10)
            spr.term = choice
            self._choice_sprites.append(spr)

        self.renderer.add_sprites(self._choice_sprites)

    def _start_update_timers(self):
        pass

    def _reset_cam_position(self):
        self.cam.position.x = settings.SCREEN_WIDTH // 2
        self.cam.position.y = settings.SCREEN_HEIGHT // 2

    def _move_choice_to_equation(self, end_callback):
        change = self._current_equation_sprite.position - self._selected_sprite.position
        change.x += self._current_equation_sprite.rect.width + settings.COMBAT_AREA_BORDER
        change.y += self._current_equation_sprite.rect.height // 2
        duration = settings.COMBAT_CHOICE_TRAVEL_DURATION
        self.tweener.create_tween(self._selected_sprite.position, 'x', self._selected_sprite.position.x, change.x,
                                  duration,
                                  cb_end=end_callback)
        self.tweener.create_tween(self._selected_sprite.position, 'y', self._selected_sprite.position.y, change.y,
                                  duration, cb_end=self._tween_reset_selected_sprite)

    def _add_bar(self):
        img = GameResource.get(GameResource.IMG_MATH_BAR)
        position = Point2(settings.COMBAT_AREA.centerx,
                          settings.COMBAT_AREA.bottom - settings.COMBAT_AREA_BORDER)
        self._bar_sprite = spritesystem.Sprite(img, position, z_layer=8)
        self._bar_sprite.bar_width = img.get_width()
        self._bar_sprite.area = pygame.Rect(0, 0, 0, img.get_height())
        self.renderer.add_sprite(self._bar_sprite)

    def _update_timed(self, delta_time):
        self.scheduler.update(delta_time)
        self.tweener.update(delta_time)
        self.combat.update(delta_time)

    # noinspection PyUnusedLocal
    def _tween_reset_selected_sprite(self, *args, **kwargs):
        # self._selected_sprite = None
        pass
