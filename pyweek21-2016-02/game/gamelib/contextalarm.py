# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek21-2016-02
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import pygame

import settings
from gameresource import GameResource
from pyknicpygame import spritesystem
from pyknicpygame.pyknic.context import Context
from pyknicpygame.pyknic.mathematics import Point2
from pyknicpygame.pyknic.tweening import Tweener, RANDOM_INT_BOUNCE
from effecterase import EraseEffect
from pyknicpygame.pyknic.context import Context
from pyknicpygame.pyknic.context.transitions import PopTransition

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2016"
__credits__ = ["DR0ID"]     # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []    # list of public visible parts of this module 


class AlarmContext(Context):

    def __init__(self, level):
        self.level = level
        self.tweener = Tweener()
        self.renderer = spritesystem.DefaultRenderer()
        screen_rect = pygame.Rect(0, 0, settings.SCREEN_WIDTH, settings.SCREEN_HEIGHT)
        self.cam = spritesystem.Camera(screen_rect)
        self.cam.position = Point2(settings.SCREEN_WIDTH // 2, settings.SCREEN_HEIGHT // 2)
        self.spr = None

        self.repeated = 0

    def resume(self):
        if not self.level.won or self.level.is_repeat:
            self.pop()
            return

        duration = 10.0
        bounce_range = (-5, 5)
        self.tweener.create_tween(self.spr.position, 'x', self.spr.position.x, 0, duration, RANDOM_INT_BOUNCE,
                                  bounce_range, cb_end=self._on_done)
        self.tweener.create_tween(self.spr.position, 'y', self.spr.position.y, 0, duration, RANDOM_INT_BOUNCE,
                                  bounce_range)
        self.tweener.create_tween(self.spr, 'zoom', 1.0, 4.0, duration)
        if self.repeated == 0:
            GameResource.get(GameResource.SOUND_CLASS_BELL_10).play()
        self.repeated += 1

    def enter(self):
        # load images
        img = GameResource.get(GameResource.IMG_CLOCK_60)
        self.spr = spritesystem.Sprite(img, self.cam.position.clone())
        self.renderer.add_sprite(self.spr)

    def update(self, delta_time):
        self.tweener.update(delta_time)
        pygame.event.clear()

    def draw(self, screen, do_flip=True, interpolation_factor=1.0):
        self.renderer.draw(screen, self.cam, (0, 0, 0), do_flip)

    # noinspection PyUnusedLocal
    def _on_done(self, *args, **kwargs):
        self.push(PopTransition(EraseEffect()))
