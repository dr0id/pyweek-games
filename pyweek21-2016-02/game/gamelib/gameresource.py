import os

from pyknicpygame.resource import resources, FONT, IMAGE, SOUND, SONG

import settings


class GameResource(object):
    FONT_CHALK_IT_UP_ID = -1
    FONT_CHALK_IT_UP_BIG_ID = -1
    IMG_CHALKBOARD = -1
    IMG_CHALKBOARD2 = -1
    IMG_CHALKBOARD_768x432 = -1
    IMG_MATH_PROFESSOR = -1
    IMG_MATH_ZOMBIE = -1
    IMG_MATH_EX_ZOMBIE = -1
    IMG_MATH_HERO = -1
    IMG_MATH_BAR = -1
    IMG_DOOR_0_CLOSED = -1
    IMG_DOOR_1_CLOSED = -1
    IMG_DOOR_2_CLOSED = -1
    IMG_DOOR_3_CLOSED = -1
    IMG_DOOR_4_CLOSED = -1
    IMG_DOOR_5_CLOSED = -1
    IMG_DOOR_666_CLOSED = -1
    IMG_DOOR_0_OPENED = -1
    IMG_DOOR_1_OPENED = -1
    IMG_DOOR_2_OPENED = -1
    IMG_DOOR_3_OPENED = -1
    IMG_DOOR_4_OPENED = -1
    IMG_DOOR_5_OPENED = -1
    IMG_DOOR_666_OPENED = -1
    hallway_door_positions = {}
    IMG_HALLWAY = -1
    IMG_HALLWAY_BLACKBOARD = -1
    SOUND_DOOR_OPENED = -1
    SOUND_DOOR_CLOSED = -1
    SOUND_DOOR_LOCKED = -1
    SOUND_DOOR_CREAKY = -1
    SOUND_CHALK_WORD = -1
    SOUND_CHALK_PARAGRAPH = -1
    SOUND_DUM_DUM = -1
    SOUND_HYPNO = -1
    SOUND_NO_HYPNO = -1
    SOUND_CLASS_BELL = -1
    SOUND_CLASS_BELL_10 = -1
    SOUND_CLOCK_TICK_HEAVY = -1
    SOUND_CLOCK_TOCK_HEAVY = -1
    SOUND_CLOCK_TICK_DISTINTIVE = -1
    SOUND_CLOCK_TOCK_DISTINTIVE = -1

    SONG_INTERMISSION = -1
    SONG_ACTION1 = -1
    SONG_ACTION2 = -1
    SONG_ACTION3 = -1
    song_queue = []

    IMG_PLAYER_00 = -1
    IMG_PLAYER_01 = -1
    IMG_PLAYER_02 = -1
    IMG_PLAYER_03 = -1
    IMG_ZOMBIE_00 = -1
    IMG_ZOMBIE_01 = -1
    IMG_ZOMBIE_02 = -1
    IMG_ZOMBIE_03 = -1
    IMG_CLOCK_00 = -1
    IMG_CLOCK_05 = -1
    IMG_CLOCK_10 = -1
    IMG_CLOCK_15 = -1
    IMG_CLOCK_20 = -1
    IMG_CLOCK_25 = -1
    IMG_CLOCK_30 = -1
    IMG_CLOCK_35 = -1
    IMG_CLOCK_40 = -1
    IMG_CLOCK_45 = -1
    IMG_CLOCK_50 = -1
    IMG_CLOCK_55 = -1
    IMG_CLOCK_60 = -1
    IMG_CLASSROOM = -1

    IMG_BOARD_ERASE_01 = -1
    IMG_BOARD_ERASE_02 = -1
    IMG_BOARD_ERASE_03 = -1
    IMG_BOARD_ERASE_04 = -1
    IMG_BOARD_ERASE_05 = -1

    IMG_GUY = -1

    @staticmethod
    def load():
        # fonts
        __chalk_it_up_font_path = "data/font/darcy-baldwin-fonts_djb-chalk-it-up/DJB Chalk It Up.ttf"
        GameResource.FONT_CHALK_IT_UP_ID = resources.load(FONT, (__chalk_it_up_font_path, 21))
        GameResource.FONT_CHALK_IT_UP_BIG_ID = resources.load(FONT, (__chalk_it_up_font_path, 40))

        # images
        GameResource.IMG_CHALKBOARD = resources.load(IMAGE, 'data/image/chalkboard.png', 1.0, 0, 0, 0)
        GameResource.IMG_CHALKBOARD2 = resources.load(IMAGE, 'data/image/chalkboard2-upscaled.png', 1.0, 0, 0, 0)
        GameResource.IMG_CHALKBOARD_768x432 = resources.load(IMAGE, 'data/image/chalkboard_768x432.png', 1.0, 0, 0, 0)
        GameResource.IMG_MATH_PROFESSOR = resources.load(IMAGE, 'data/image/math-professor.png')
        GameResource.IMG_MATH_ZOMBIE = resources.load(IMAGE, 'data/image/math-zombie.png')
        GameResource.IMG_MATH_EX_ZOMBIE = resources.load(IMAGE, 'data/image/math-ex-zombie.png')
        GameResource.IMG_MATH_HERO = resources.load(IMAGE, 'data/image/math-hero.png')
        GameResource.IMG_MATH_BAR = resources.load(IMAGE, 'data/image/math-hypnosis-bar.png')

        GameResource.IMG_DOOR_0_CLOSED = resources.load(IMAGE, 'data/image/hallway-door0-closed.png')
        GameResource.IMG_DOOR_1_CLOSED = resources.load(IMAGE, 'data/image/hallway-door1-closed.png')
        GameResource.IMG_DOOR_2_CLOSED = resources.load(IMAGE, 'data/image/hallway-door2-closed.png')
        GameResource.IMG_DOOR_3_CLOSED = resources.load(IMAGE, 'data/image/hallway-door3-closed.png')
        GameResource.IMG_DOOR_4_CLOSED = resources.load(IMAGE, 'data/image/hallway-door4-closed.png')
        GameResource.IMG_DOOR_5_CLOSED = resources.load(IMAGE, 'data/image/hallway-door5-closed.png')
        GameResource.IMG_DOOR_666_CLOSED = resources.load(IMAGE, 'data/image/hallway-door666-closed.png')
        GameResource.IMG_DOOR_0_OPENED = resources.load(IMAGE, 'data/image/hallway-door0-opened.png')
        GameResource.IMG_DOOR_1_OPENED = resources.load(IMAGE, 'data/image/hallway-door1-opened.png')
        GameResource.IMG_DOOR_2_OPENED = resources.load(IMAGE, 'data/image/hallway-door2-opened.png')
        GameResource.IMG_DOOR_3_OPENED = resources.load(IMAGE, 'data/image/hallway-door3-opened.png')
        GameResource.IMG_DOOR_4_OPENED = resources.load(IMAGE, 'data/image/hallway-door4-opened.png')
        GameResource.IMG_DOOR_5_OPENED = resources.load(IMAGE, 'data/image/hallway-door5-opened.png')
        GameResource.IMG_DOOR_666_OPENED = resources.load(IMAGE, 'data/image/hallway-door666-opened.png')
        GameResource.IMG_HALLWAY = resources.load(IMAGE, 'data/image/hallway.png')
        GameResource.IMG_HALLWAY_BLACKBOARD = resources.load(IMAGE, 'data/image/chalkboard_1024x768.png')

        GameResource.IMG_PLAYER_00 = resources.load(IMAGE, 'data/image/player_00.png')
        GameResource.IMG_PLAYER_01 = resources.load(IMAGE, 'data/image/player_01.png')
        GameResource.IMG_PLAYER_02 = resources.load(IMAGE, 'data/image/player_02.png')
        GameResource.IMG_PLAYER_03 = resources.load(IMAGE, 'data/image/player_03.png')

        GameResource.IMG_ZOMBIE_00 = resources.load(IMAGE, 'data/image/zombie_00.png')
        GameResource.IMG_ZOMBIE_01 = resources.load(IMAGE, 'data/image/zombie_01.png')
        GameResource.IMG_ZOMBIE_02 = resources.load(IMAGE, 'data/image/zombie_02.png')
        GameResource.IMG_ZOMBIE_03 = resources.load(IMAGE, 'data/image/zombie_03.png')

        GameResource.IMG_CLOCK_00 = resources.load(IMAGE, 'data/image/clock_00.png')
        GameResource.IMG_CLOCK_05 = resources.load(IMAGE, 'data/image/clock_05.png')
        GameResource.IMG_CLOCK_10 = resources.load(IMAGE, 'data/image/clock_10.png')
        GameResource.IMG_CLOCK_15 = resources.load(IMAGE, 'data/image/clock_15.png')
        GameResource.IMG_CLOCK_20 = resources.load(IMAGE, 'data/image/clock_20.png')
        GameResource.IMG_CLOCK_25 = resources.load(IMAGE, 'data/image/clock_25.png')
        GameResource.IMG_CLOCK_30 = resources.load(IMAGE, 'data/image/clock_30.png')
        GameResource.IMG_CLOCK_35 = resources.load(IMAGE, 'data/image/clock_35.png')
        GameResource.IMG_CLOCK_40 = resources.load(IMAGE, 'data/image/clock_40.png')
        GameResource.IMG_CLOCK_45 = resources.load(IMAGE, 'data/image/clock_45.png')
        GameResource.IMG_CLOCK_50 = resources.load(IMAGE, 'data/image/clock_50.png')
        GameResource.IMG_CLOCK_55 = resources.load(IMAGE, 'data/image/clock_55.png')
        GameResource.IMG_CLOCK_60 = resources.load(IMAGE, 'data/image/clock_60.png')

        GameResource.IMG_CLASSROOM = resources.load(IMAGE, 'data/image/classroom.png')
        GameResource.IMG_GUY = resources.load(IMAGE, 'data/image/guy.png')

        GameResource.IMG_BOARD_ERASE_01 = resources.load(IMAGE, 'data/image/chalkboard_erase_01.png')
        GameResource.IMG_BOARD_ERASE_02 = resources.load(IMAGE, 'data/image/chalkboard_erase_02.png')
        GameResource.IMG_BOARD_ERASE_03 = resources.load(IMAGE, 'data/image/chalkboard_erase_03.png')
        GameResource.IMG_BOARD_ERASE_04 = resources.load(IMAGE, 'data/image/chalkboard_erase_04.png')
        GameResource.IMG_BOARD_ERASE_05 = resources.load(IMAGE, 'data/image/chalkboard_erase_05.png')

        GameResource.hallway_door_positions = {
            (420, 258, 185, 216): [GameResource.IMG_DOOR_0_CLOSED, GameResource.IMG_DOOR_0_OPENED, 0],
            (340, 239, 48, 254): [GameResource.IMG_DOOR_1_CLOSED, GameResource.IMG_DOOR_1_OPENED, 1],
            (724, 159, 87, 426): [GameResource.IMG_DOOR_2_CLOSED, GameResource.IMG_DOOR_2_OPENED, 2],
            (243, 191, 82, 385): [GameResource.IMG_DOOR_3_CLOSED, GameResource.IMG_DOOR_3_OPENED, 3],
            (858, 52, 124, 678): [GameResource.IMG_DOOR_4_CLOSED, GameResource.IMG_DOOR_4_OPENED, 4],
            (15, 30, 161, 722): [GameResource.IMG_DOOR_5_CLOSED, GameResource.IMG_DOOR_5_OPENED, 5],
            (636, 227, 67, 279): [GameResource.IMG_DOOR_666_CLOSED, GameResource.IMG_DOOR_666_OPENED, 6]
        }

        # sounds
        vol = settings.SFX_VOLUME
        GameResource.SOUND_DOOR_OPENED = resources.load(SOUND, 'data/sfx/open_door.ogg', vol * 0.5)
        GameResource.SOUND_DOOR_CLOSED = resources.load(SOUND, 'data/sfx/close_door_1.ogg', vol * 0.5)
        GameResource.SOUND_DOOR_LOCKED = resources.load(SOUND, 'data/sfx/door_lock.ogg', vol * 0.5)
        GameResource.SOUND_DOOR_CREAKY = resources.load(SOUND, 'data/sfx/open_creaky_door.ogg', vol * 0.5)
        GameResource.SOUND_CHALK_WORD = resources.load(
            SOUND, 'data/sfx/chalk_stick_single_stroke_on_chalk_board_005.ogg', vol * 1.0)
        GameResource.SOUND_CHALK_PARAGRAPH = resources.load(
            SOUND, 'data/sfx/chalk_stick_write_on_chalk_board.ogg', vol * 1.0)
        GameResource.SOUND_DUM_DUM = resources.load(SOUND, 'data/sfx/dum_dum.ogg', vol * 1.0)
        GameResource.SOUND_HYPNO = resources.load(
            SOUND, 'data/sfx/145434__soughtaftersounds__old-music-box-1.ogg', vol * 1.0)
        GameResource.SOUND_NO_HYPNO = resources.load(
            SOUND, 'data/sfx/145435__soughtaftersounds__old-music-box-2.ogg', vol * 1.0)
        GameResource.SOUND_CLASS_BELL = resources.load(SOUND, 'data/sfx/church_bells_5_secs.ogg', vol * 1.0)
        GameResource.SOUND_CLASS_BELL_10 = resources.load(SOUND, 'data/sfx/church_bells.ogg', vol * 1.0)
        GameResource.SOUND_CLOCK_TICK_HEAVY = resources.load(
            SOUND, 'data/sfx/mantle_clock_heavy_low_tick.ogg', vol * 0.5)
        GameResource.SOUND_CLOCK_TOCK_HEAVY = resources.load(
            SOUND, 'data/sfx/mantle_clock_heavy_low_tock.ogg', vol * 0.5)
        GameResource.SOUND_CLOCK_TICK_DISTINTIVE = resources.load(
            SOUND, 'data/sfx/mantle_clock_distinctive_tick.ogg', vol * 0.5)
        GameResource.SOUND_CLOCK_TOCK_DISTINTIVE = resources.load(
            SOUND, 'data/sfx/mantle_clock_distinctive_tock.ogg', vol * 0.5)

        # songs
        vol = settings.MUSIC_VOLUME
        GameResource.SONG_INTERMISSION = resources.load(
            SONG, 'Intermission', 'data/music/_dark_tension_eerie_dark_tense_and_dramatic_underscore.ogg', vol * 1.0)
        GameResource.SONG_ACTION1 = resources.load(SONG, 'Black Hole', 'data/music/black_hole.ogg', vol * 1.0)
        GameResource.SONG_ACTION2 = resources.load(SONG, 'Escape', 'data/music/escape.ogg', vol * 1.0)
        GameResource.SONG_ACTION3 = resources.load(SONG, 'Sports Card', 'data/music/sports_card.ogg', vol * 1.0)
        GameResource.song_queue = [
            GameResource.SONG_ACTION1,
            GameResource.SONG_ACTION2,
            GameResource.SONG_ACTION3,
        ]
    @staticmethod
    def get(resource_id):
        return resources.get_resource(resource_id)
