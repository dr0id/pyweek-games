from __future__ import print_function

import pygame

import levels
import pyknicpygame
import settings
from contextcombat import CombatContext
from contextlevelselection import LevelSelectionContext
from pyknicpygame.pyknic.context import Context
from pyknicpygame.pyknic.mathematics import Point2
from script import Script, ScriptIntro, Script1, Script2, Script3, Script4


class ContextStory(Context):
    """Current flow:
    1 main.py pushes ContextStory
    2 main.py pushes Intro splash screen
    3 Splash screen completes and pops itself
    4 ContextStory resumes
    5 ContextStory.resume() loads next dialogue in sequence from self.dialogues
    6 Dialogue script proceeds until it is done
    7 When dialogue script is done, level-selector Hallway is pushed
    8 Hallway pushes a selected combat room
    9 When player wins a combat, combat room pops itself and the hallway
    10 If more combat rooms to beat, return to step 4
    11 End game!
    12 Credits

    Alternative flow:
    1 main.py pushes LevelSelectionContext
    2 main.py pushes Intro splash screen
    3 Splash screen completes and pops itself
    4 LevelSelectionContext resumes
    5 LevelSelectionContext loads next dialogue in sequence from self.dialogues
    6 Dialogue script proceeds until it is done
    7 When dialogue script is done, it pops itself
    8 LevelSelectionContext resumes
    9 LevelSelectionContext pushes a selected combat room
    10 When player wins a combat, combat room pops itself
    11 If more combat rooms to beat, return to step 4
    12 End game!
    13 Credits
    """

    dialogues = [
        ScriptIntro,

        # TODO: add dialogue script for each combat room
        Script1,
        Script2,
        Script3,
        Script4,
        # Script5,

        # TODO: add GameWon
        # GameWon,
    ]

    def __init__(self):
        self.doing = None
        self.background = pygame.Color('lightblue2')

        w, h = settings.SCREEN_SIZE
        self.scheduler = pyknicpygame.pyknic.timing.Scheduler()
        self.cam = pyknicpygame.spritesystem.Camera(pygame.Rect(0, 0, w, h), position=Point2(w // 2, h // 2))
        self.renderer = pyknicpygame.spritesystem.DefaultRenderer()

        self.unhandled_events = []

    def enter(self):
        pass

    def resume(self):
        try:
            dialogue_class = self.dialogues.pop(0)
            self.doing = dialogue_class()
        except IndexError:
            # No more dialogues, game over
            self.pop()

    def update(self, delta_time):
        self._handle_events()
        if isinstance(self.doing, Script):
            # interact with dialogue script
            add_sprites = []
            remove_sprites = []

            self.doing.think(delta_time, add_sprites, remove_sprites, self.unhandled_events)

            for sprite in remove_sprites:
                self.renderer.remove_sprite(sprite)

            for sprite in add_sprites:
                self.renderer.add_sprite(sprite)

            # HALLWAY
            if not self.doing.running:
                if self.dialogues:
                    self.push(LevelSelectionContext())
                else:
                    # no more dialogues
                    self.pop()
        else:
            # TODO: interact with generic class TBD
            self.doing.update(delta_time)

    def draw(self, screen, do_flip=True, interpolation_factor=1.0):
        self.renderer.draw(screen, self.cam, fill_color=self.background, do_flip=False,
                           interpolation_factor=interpolation_factor)
        if False:
            # Debug: left-right dialogue rects
            pygame.draw.rect(screen, pygame.Color('red'), settings.DIALOGUE_LEFT_AREA, 1)
            pygame.draw.rect(screen, pygame.Color('green'), settings.DIALOGUE_RIGHT_AREA, 1)
        if do_flip:
            pygame.display.flip()

    def _handle_events(self):
        del self.unhandled_events[:]
        for e in pygame.event.get():
            handled = False
            if e.type == pygame.KEYDOWN:
                if e.key == settings.KEY_CHEAT_EXIT:
                    self.pop()
                    handled = True
                # elif e.key == settings.KEY_CHEAT_COMBAT_TEST:
                #     self.push(CombatContext(levels.MathZombieCombatData(), levels.PlayerMathSanityData()))
                elif e.key == settings.KEY_CHEAT_LEVEL_SELECTION:
                    self.push(LevelSelectionContext())
            elif e.type == pygame.QUIT:
                self.pop()
            if not handled:
                self.unhandled_events.append(e)
