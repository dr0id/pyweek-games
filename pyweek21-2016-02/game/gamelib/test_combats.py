# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek21-2016-02
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function
import unittest
import combat as mut  # module under test
from equation import Equation

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id [at] gmail [dot] com"
__copyright__ = "DR0ID @ 2016"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

if mut.__version_info__ != __version_info__:
    raise Exception("version numbers do not match!")


class TestZombieData(object):

    def __init__(self):
        self.equations = [Equation("1 + 1 = ?", {-1: '2'}, ['1', '3', '4', '6'])]
        self.hypnosis_increase_rate_per_second = 1.0


class TestPlayerMathSanityData(object):

    def __init__(self):
        self.hypnosis_level = 0
        self.hypnosis_threshold = 10
        self.hypnosis_decrease_rate_per_second = 1.0

    def decrease_hypnosis_level(self, amount):
        self.hypnosis_level -= amount
        # clamp to lowest possible level
        if self.hypnosis_level < 0:
            self.hypnosis_level = 0


class CombatTestCases(unittest.TestCase):
    def test_combat_in_progress_unsuccessful_attack(self):
        # arrange
        combat = mut.Combat(TestZombieData(), TestPlayerMathSanityData())

        # act
        actual = combat.attack_hypnotic_zombie('6')

        # verify
        self.assertFalse(actual)
        self.assertFalse(combat.player_has_won())
        self.assertTrue(combat.is_in_progress())

    def test_combat_in_progress_successful_attack(self):
        # arrange
        combat = mut.Combat(TestZombieData(), TestPlayerMathSanityData())

        # act
        actual = combat.attack_hypnotic_zombie('2')

        # verify
        self.assertTrue(actual)
        self.assertTrue(combat.player_has_won())
        self.assertFalse(combat.is_in_progress())

    def test_player_looses(self):
        # arrange
        combat = mut.Combat(TestZombieData(), TestPlayerMathSanityData())

        # act
        combat.attack_hypnotic_zombie('4')
        combat.attack_hypnotic_zombie('4')
        combat.update(2.0)
        self.assertTrue(combat.is_in_progress())
        combat.attack_hypnotic_zombie('4')
        combat.attack_hypnotic_zombie('4')
        combat.update(2.0)
        self.assertTrue(combat.is_in_progress())
        combat.attack_hypnotic_zombie('4')
        combat.attack_hypnotic_zombie('4')
        combat.update(2.0)
        self.assertTrue(combat.is_in_progress())
        combat.attack_hypnotic_zombie('4')
        combat.attack_hypnotic_zombie('4')
        combat.update(2.0)
        self.assertTrue(combat.is_in_progress())
        combat.attack_hypnotic_zombie('4')
        combat.attack_hypnotic_zombie('4')
        combat.update(2.0)
        self.assertFalse(combat.is_in_progress())
        combat.attack_hypnotic_zombie('4')
        actual = combat.attack_hypnotic_zombie('4')
        combat.update(2.0)

        # verify
        self.assertFalse(actual)
        self.assertFalse(combat.player_has_won())
        self.assertFalse(combat.is_in_progress())



if __name__ == '__main__':
    unittest.main()
