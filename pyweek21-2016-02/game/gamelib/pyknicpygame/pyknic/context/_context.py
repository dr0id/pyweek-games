# -*- coding: utf-8 -*-

"""
Context managment.

Limitations:
    pop can be called from anywhere, also from within a update method of an
    active (topmost) context. After poping it the active context is not the
    topmost context anymore, but can still modify the context stack (since
    it is still in the update method).

.. todo::
    new API, maybe better or not???::

        # should top() be in __all__ ??
        __all__ = ["update", "draw", "init", "length"]

        def init(initial_context):
            _push(initial_context)

        def update(dt):
            action = _CONTEXT_STACK[-1].update(dt) # -> None  : do nothing
                                                  #    False : pop top context
                                                  #NewContext: push new context
            if action:
                _push(action)
            elif action is False:
                _pop()

        def draw(screen):
            if _CONTEXT_STACK:
                _CONTEXT_STACK[-1].draw(screen)

.. TODO::
    pass around a 'info' object (through push/pop)?? <- could make scene management easier
"""

import logging
logger = logging.getLogger(__name__)


__all__ = ["Context", "push", "pop", "top", "length", "print_stack", "set_deferred_mode", "is_deferred_mode_active",
           "update"]


class ContextStack(object):
    """
    The ContextStack class. It holds the stack of contexts and has convenient methods to work with the stack.
    """

    def __init__(self):
        self._stack = []
        self._is_deferred_mode_active = False
        self._deferred_commands = []

    def push(self, cont):
        """
        Pushes a new context on the stack. The enter method of the new context
        will be called when pushing it. If the stack is not empty, suspend will
        be called on the topmost context on the stack before pushing the new
        context on top of it.
        """
        if self._is_deferred_mode_active:
            self._append_deferred_command((self._push, (cont,)))
        else:
            self._push(cont)

    def _append_deferred_command(self, cmd):
        """
        Appends a command to the deferred command list.
        :param cmd: A command as a tuple of (cmd, args) where cmd either _push or _pop, args a tuple or list of args.
        """
        logger.debug("CONTEXT deferred: appending command: " + str(cmd))
        if cmd[0] != self._push and cmd[0] != self._pop:
            raise ValueError("command should be either _push or _pop but is " + str(cmd[0].__name__))

        self._deferred_commands.append(cmd)

    def _push(self, cont):
        """
        Pushes a new context on the stack. The enter method of the new context
        will be called when pushing it. If the stack is not empty, suspend will
        be called on the topmost context on the stack before pushing the new
        context on top of it.
        """
        cont.process_stack_push(self._stack)
        if __debug__:
            logger.debug("CONTEXT: pushed " + str(cont))
            print_stack()

    def pop(self, count=1, do_resume=True):
        """
        Pops a context from the stack. The exit method of the removed context will
        be called. If a there is another context on the stack, the resume method of
        it will be called.

        :Parameters:
            count : int
                defaults to 1, number of contextes to pop
            do_resume : bool
                (default: True) This has only an effect if poping more than one context at once (count>1).
                If set to False then only the last context is resumed.
        """
        if self._is_deferred_mode_active:
            self._append_deferred_command((self._pop, (count, do_resume)))
        else:
            self._pop(count, do_resume)

    def _pop(self, count, do_resume):
        """
        Pops a context from the stack. The exit method of the removed context will
        be called. If a there is another context on the stack, the resume method of
        it will be called.

        :Parameters:
            count : int
                defaults to 1, number of contextes to pop
            do_resume : bool
                (default: True) This has only an effect if poping more than one context at once (count>1).
                If set to False then only the last context is resumed.
        """
        while count:
            count -= 1
            cont = None
            if self._stack:
                cont = self._stack.pop(-1)
                cont.process_stack_pop(self._stack, (do_resume or count == 0))
                if __debug__:
                    logger.debug("CONTEXT: poped " + str(cont))
                    print_stack()

    def top(self, idx=0):
        """
        The active context.

        :Parameters:
            idx : int
                default: 0 (topmost), the item at index position idx of the stack.

        :returns:
            default: the topmost context of the stack or None
            otherwise the the item at index position idx or None if idx > length()
        """
        idx += 1
        if idx > len(self._stack):
            return None
        return self._stack[-1 * idx]

    def length(self):
        """
        The number of items in the stack.

        :returns:
            number of items, integer >= 0
        """
        return len(self._stack)

    def set_deferred_mode(self, is_deferred):
        """
        Set or un-set the deferred mode. In deferred mode the pop() and push() methods are executed when the
        update() method is called. It is essential to call update(), otherwise nothing will happen.

        When disabling the deferred mode all operations still awaiting the update() call will be executed
        immediately. Afterwards calls to update() will do nothing.

        :param is_deferred: Boolean to indicate if deferred mode should be activated or not.
        """
        if not is_deferred:
            logger.debug("CONTEXT: executing any deferred commands before disabling deferred mode!")
            self.update()

        logger.debug("CONTEXT: setting deferred mode to: " + str(is_deferred))
        self._is_deferred_mode_active = is_deferred

    def is_deferred_mode_active(self):
        """
        Check if deferred mode is active or not.

        :return: Boolean indicating if deferred mode is active.
        """
        return self._is_deferred_mode_active

    def update(self):
        """
        Update method to execute all deferred pop() and push() operations. This method has to be called
        in deferred mode. When the deferred mode is inactive this method does nothing.
        """
        if self._is_deferred_mode_active:
            for cmd, args in self._deferred_commands:
                logger.debug("CONTEXT deferred mode: executing command: {0}({1})".format(cmd, args))
                cmd(*args)
            # logger.debug("CONTEXT deferred mode: clearing deferred commands")
            self._deferred_commands = []

    def print_stack(self):
        """
        Prints the current stack to the console. Should only be used for
        debugging purposes.
        """
        if __debug__:
            logger.debug("CONTEXT STACK:")
            for idx in range(0, self.length()):
                logger.debug("    %s %s" % (idx, self.top(idx)))
            logger.debug("")

# TODO: implement exchange(int idx, B): exchange context A with B from stack (any position) <- maybe as transition?
# TODO: implement remove(int idx): remove context A from stack (any position) <- maybe as transition?
_CONTEXT_STACK = ContextStack()
pop = _CONTEXT_STACK.pop
push = _CONTEXT_STACK.push
length = _CONTEXT_STACK.length
top = _CONTEXT_STACK.top
print_stack = _CONTEXT_STACK.print_stack
set_deferred_mode = _CONTEXT_STACK.set_deferred_mode
is_deferred_mode_active = _CONTEXT_STACK.is_deferred_mode_active
update = _CONTEXT_STACK.update


class Context(object):
    """
    The context class.
    """

    def enter(self):
        """Called when this context is pushed onto the stack."""
        pass

    def exit(self):
        """Called when this context is popped off the stack."""
        pass

    def suspend(self):
        """Called when another context is pushed on top of this one."""
        pass

    def resume(self):
        """Called when another context is popped off the top of this one."""
        pass

    def update(self, delta_time):
        """Called once per frame"""
        pass

    def draw(self, screen, do_flip=True, interpolation_factor=1.0):
        """
        Refresh the screen

        :param screen: the screen surface
        :param do_flip: if set to true, the it should flip the screen, otherwise not. Default: True
        :param interpolation_factor: interpolation factor for drawing between sim updates, default: 1.0
        """
        pass

    def process_stack_push(self, context_stack):
        """
        Processes the stack push for instances of this class.
        :param context_stack: the stack to push the context on (append it to the list).
        """
        if context_stack:
            context_stack[-1].suspend()
            if __debug__:
                logger.debug("CONTEXT: suspended " + str(context_stack[-1].__class__.__name__))
        context_stack.append(self)
        if __debug__:
            logger.debug("CONTEXT: pushed " + str(self.__class__.__name__))
        self.enter()
        if __debug__:
            logger.debug("CONTEXT: entered " + str(self.__class__.__name__))

    def process_stack_pop(self, context_stack, do_resume):
        """
        Processes the stack push for instances of this class.
        """
        self.exit()
        if __debug__:
            logger.debug("CONTEXT: exited " + str(self.__class__.__name__))
            logger.debug("CONTEXT: resume=" + str(do_resume))
        if context_stack and do_resume:
            # log first, because resume might pop a context from stack
            if __debug__:
                logger.debug("CONTEXT: resumed " + str(context_stack[-1].__class__.__name__))
            context_stack[-1].resume()

    push = staticmethod(push)
    pop = staticmethod(pop)
    top = staticmethod(top)
    get_stack_length = staticmethod(length)
