# -*- coding: utf-8 -*-
"""
.. todo:: initial pyknic doc
.. todo:: explain how logging works and is configured

"""
import sys
import logging
# local imports
from . import info  # pylint: disable=W0404
from . import logs
from . import settings


class VersionMismatchError(Exception):
    pass


__all__ = ["context", "cache", "events", "timing", "info", "mathematics"]


# check python version
def check_version(upper_version, actual_version, lower_version):
    if not upper_version >= actual_version:
        raise VersionMismatchError("expected version: {0} >= {1}!".format(upper_version, actual_version))

    if not actual_version >= lower_version:
        raise VersionMismatchError("expected version: {0} >= {1}!".format(actual_version, lower_version))


check_version((3, 5), tuple(sys.version_info), (2, 7))

# read only
is_initialized = False


def init(log_config=logs.get_default_logger_config()):
    """
    Call this to initialize pyknic after you have changed its settings.
    After calling this method changes in settings may not have effect 
    (depends on the setting).
    
    .. todo:: explain logging options (maybe in settings?) and how it works
    .. todo:: replace settings with options using optparse!!
    """
    global is_initialized  # pylint: disable=W0603
    if not is_initialized:
        is_initialized = True

        # logging
        logs.setup_loggers(log_config)
        logger = logging.getLogger(__name__)
        logger.addHandler(logging.NullHandler())  # prevent handler not configured warning

        # mark the start in the log
        logger.info("*" * settings.log_separator_width)
        logger.info("*" * settings.log_separator_width)
        logger.info("*" * settings.log_separator_width)
        _format_string = "%-" + str(settings.log_separator_width) + "s"
        logger.info(_format_string % ("pyknic v" + info.version + " (c) DR0ID 2011"))
        logger.info("*" * settings.log_separator_width)
        logger.info("using python version: " + sys.version)

        logs.log_log_config(log_config)
        # logs.log_dict(settings, "Settings at startup:")
        logs.log_settings()
        logs.log_environment()

        # TODO: is this lazy import really needed?
        # make other modules available
        from . import context
        from . import cache
        from . import events
        from . import timing
        from . import mathematics
    else:
        logging.warning("pyknic already initialized, should only called once")

# -----------------------------------------------------------------------------

# TODO: write a predefined main loop -> subsystem management? (because of e.g.  music fadeout)
# TODO: logging to another process...?
