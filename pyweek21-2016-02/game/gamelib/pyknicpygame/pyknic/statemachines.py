# -*- coding: utf-8 -*-

"""
This module contains different state machine implementations.
"""

import logging
logger = logging.getLogger(__name__)

class SimpleStateMachine(object):
    """
    A simple StateMachine.
    
    Usage::
    
        state1 = State1()
        sm = StateMachine()
        #sm.switch_state(state1) # set the first state
        
        # in main loop
        sm.current_state.update(dt)
        

        Never ever set current_state directly, ALWAYS use switch_state
    
    """

    def __init__(self, initial_state):
        self.current_state = initial_state
        initial_state.enter(self)

    # TODO: idea: transition should be a instance of a class like
    # transition.guard() -> bool
    # transition.enter(self.context) <= used for hirachical sm
    # transition.exit(self.context) <= used for hirachical sm
    # def switch_state(self, new_state, transition=None):
    def switch_state(self, new_state):
        """
        Change to the next state. First exit is called on the current state,
        then the new state is made the current state and finally enter is called
        on the new state.
        
        :Parameters:
            new_state : object
                Can be any object that has following methods: enter(sm) and 
                exit(sm), can not be None (use a end state instead).
        
        """

        assert new_state is not None
        assert hasattr(new_state, "enter")
        assert hasattr(getattr(new_state, "enter"), '__call__')
        assert hasattr(new_state, "exit")
        assert hasattr(getattr(new_state, "exit"), '__call__')

        self.current_state.exit(self)
        self.current_state = new_state
        self.current_state.enter(self)



class StackStateMachine(SimpleStateMachine):
    """
    This state machine has a stack of states, instead of just switching between
    states, a state can be pushed on top of the current state.
    
    
    """

    _stack = None
    
    def push_state(self, new_state):
        """
        Pushes a state on top of the current state.
        
        :Parameters:
            new_state : object
                It can be any object that has following methods:
                enter, exit, deactivate, activate

        """
        assert new_state is not None
        assert hasattr(new_state, "deactivate")
        assert hasattr(getattr(new_state, "deactivate"), '__call__')
        assert hasattr(new_state, "activate")
        assert hasattr(getattr(new_state, "activate"), '__call__')
        if __debug__:
            if self.current_state is not None:
                assert hasattr(self.current_state, "deactivate")
                assert hasattr(getattr(self.current_state, "deactivate"), '__call__')
                assert hasattr(self.current_state, "activate")
                assert hasattr(getattr(self.current_state, "activate"), '__call__')
        
        if self._stack is None:
            self._stack = []
        
        if self.current_state is not None:
            self.current_state.deactivate(self)
        self._stack.append(self.current_state)
        self.current_state = new_state
        self.current_state.enter(self)
    
    def pop_state(self):
        """
        Removes the top state from the stack, set the current_state to 
        the next state in the stack and returns the removed state.
        
        :Returns:
            Removed state:
        :rtype:
            object
        """
    
        assert self._stack is not None
        assert len(self._stack) > 0
        
        self.current_state.exit(self)
        removed = self.current_state
        self.current_state = self._stack.pop(-1)
        self.current_state.activate(self)
        return removed
    

