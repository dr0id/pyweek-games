# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function
import os
import logging
import logging.handlers  # pylint: disable=W0404
import logging.config
import atexit
from . import settings

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 



# settings = settings.settings

# logging string format support extension both '%' formatting and '{}' formatting are supported
try:
    # noinspection PyStatementEffect
    str
    _unicode = True
except NameError:
    _unicode = False

_orig_get_message = logging.LogRecord.getMessage


def _get_format_message(self):
    """
    LogRecord formatting method that supports '%' formatting and '{}' formatting in strings.
    :return: formatted string
    """
    try:
        return _orig_get_message(self)
    except TypeError as te:
        if not _unicode:  # if no unicode support...
            msg = str(self.msg)
        else:
            msg = self.msg
            if not isinstance(msg, str):
                try:
                    msg = str(self.msg)
                except UnicodeError:
                    msg = self.msg  # Defer encoding till later
        if self.args:
            args = self.args
            msg = msg.format(*args)
        return msg

logging.LogRecord.getMessage = _get_format_message

logger = logging.getLogger(__name__)

# shut down logging properly
atexit.register(logging.shutdown)


NULL_LOGGING_CONFIGURATION = {
    'version': 1,
    'disable_existing_loggers': settings.log_disable_existing_loggers,
    'formatters': {
        'record_format': {
            'format': settings.log_record_format,
            'datefmt': settings.log_date_format
        },
    },
    'handlers': {
    },
    'loggers': {
        settings.log_logger_name: {
            'level': settings.log_logger_level,
            'handlers': []
        },
    }
}


def get_default_logger_config(log_to_console=True, log_to_file=True):
    configuration = dict(NULL_LOGGING_CONFIGURATION)
    if log_to_console:
        _add_console_handler(configuration)
    if log_to_file:
        _add_file_handler(configuration)
    return configuration

# TODO: console or file handler are slow, use a in memory queue and write from a secondary thread to console and file!
def _add_console_handler(config, logger_name=settings.log_logger_name):
    config['handlers']['console'] = {
        'class': 'logging.StreamHandler',
        'formatter': 'record_format',
        'level': logging.NOTSET,
    }
    config['loggers'][logger_name]['handlers'].append('console')


def _add_file_handler(config):
    config['handlers']['file'] = {
        'level': logging.NOTSET,
        'class': 'logging.handlers.RotatingFileHandler',
        'formatter': 'record_format',
        'filename': settings.log_path_and_filename,
        'mode': settings.log_file_mode,
        'maxBytes': settings.log_max_bytes,
        'backupCount': settings.log_backup_count,
        'encoding': settings.log_encoding,
        'delay': settings.log_delayed_logging,
    }
    config['loggers']['pyknic']['handlers'].append('file')


def get_logger_names():
    # noinspection PyUnresolvedReferences
    return logging.Logger.manager.loggerDict.keys()


def get_logger_parent_tuples():
    flat_hierarchy = []
    for name in get_logger_names():
        flat_hierarchy.append((name, logging.getLogger(name).parent.name))
    return flat_hierarchy


def is_logging_configured():
    root_logger = logging.getLogger()
    return len(root_logger.handlers) > 0


def setup_loggers(log_config):
    if log_config:
        logging.config.dictConfig(log_config)
    # TODO: else case?


def log_log_config(log_config):
    logger.info("LogConfiguration")
    import pprint
    try:
        from StringIO import StringIO
    except ImportError:
        # py3k compatibility
        from io import StringIO
    str_buffer = StringIO()
    pp = pprint.PrettyPrinter(stream=str_buffer)
    pp.pprint(log_config)
    logger.info(str_buffer.getvalue())


def log_dict(dict_to_log, title):
    logger.info("-" * settings.log_separator_width)
    logger.info(title)
    # TODO: maybe use the PrettyPrinter object for output
    for key in dict_to_log.keys():
        logger.info("  %-20s: %s" % (str(key), str(dict_to_log[key])))
    logger.info("-" * settings.log_separator_width)


def log_environment():
    env = os.environ
    log_dict(env, "Environment:")


# TODO: rewrite as log_module_interface ??
def log_settings():
    for name in sorted(dir(settings)):
        attribute_value = getattr(settings, name, None)
        if attribute_value and not callable(attribute_value) and not name.startswith('_'):  # and name in settings.__all__:
            logger.info("settings.{0}: {1}".format(name, attribute_value))
