# -*- coding: utf-8 -*-

"""
This module should provide information about the pyknic module.

Currently there is:

VERSION
VERSIONNUMBER

besides of:

__version__
__author__

__copyright__
__credits__
__license__
__maintainer__

Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

    For instance:
        * 1.2.0.1 instead of 1.2-a
        * 1.2.1.2 instead of 1.2-b2 (beta with some bug fixes)
        * 1.2.2.3 instead of 1.2-rc (release candidate)
        * 1.2.3.0 instead of 1.2-r (commercial distribution)
        * 1.2.3.5 instead of 1.2-r5 (commercial distribution with many bug fixes)


.. versionchanged:: 3.0.1.1
    removed: VERSION, VERSIONNUMBER
    added: version, version_info, __versionnumber__
    __versionnumber__ is a tuple containing ints according to the versioning scheme.


"""
import logging
logger = logging.getLogger(__name__)

__versionnumber__ = (3, 0, 1, 1)
__version__ = ".".join([str(num) for num in __versionnumber__])
__version_info__ = __versionnumber__

__author__ = "DR0ID (C) 2011-2012"

__copyright__ = "Copyright 2011-2012, DR0ID"
__credits__ = ["DR0ID"]
__license__ = "GPL3"
__maintainer__ = "DR0ID"

__all__ = ["version", "version_info"]

version = __version__
version_info = __versionnumber__

logger.debug("loaded.")

# -----------------------------------------------------------------------------
