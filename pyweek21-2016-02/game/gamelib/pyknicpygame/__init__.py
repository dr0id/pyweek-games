# -*- coding: utf-8 -*-

"""
.. todo:: docstring
.. todo:: make the relative import failure a warning?

"""

import logging

# Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage
#
#   +-- api change, probably incompatible with older versions
#   |     +-- enhancements but no api change
#   |     |
# major.minor[.build[.revision]]
#                |
#                +-|* 0 for alpha (status)
#                  |* 1 for beta (status)
#                  |* 2 for release candidate
#                  |* 3 for (public) release
#
# For instance:
#     * 1.2.0.1 instead of 1.2-a
#     * 1.2.1.2 instead of 1.2-b2 (beta with some bug fixes)
#     * 1.2.2.3 instead of 1.2-rc (release candidate)
#     * 1.2.3.0 instead of 1.2-r (commercial distribution)
#     * 1.2.3.5 instead of 1.2-r5 (commercial distribution with many bug fixes)

__version__ = "0.0.0.0"
__author__ = "DR0ID (C) 2011"

__copyright__ = "Copyright 2011, pyknicpygame"
__credits__ = ["DR0ID"]
__license__ = "GPL3"
__maintainer__ = "DR0ID"

__all__ = ["context", "pyknic", "transform", "spritesystem"]

# import pyknic, either relative of absolute
try:
    from . import pyknic
except ImportError as e:
    if __debug__:
        debug_logger = logging.getLogger(__name__)
        handler = logging.StreamHandler()
        debug_logger.addHandler(handler)
        debug_logger.error('IMPORT: relative import failed, trying absolute import! ' + str(e))
        import sys
        import traceback

        # import StringIO
        try:
            # python 2.x
            import StringIO
            from StringIO import StringIO

            exc_traceback = sys.exc_traceback
        except:
            # python 3.x
            from io import StringIO

            exc_type, exc_value, exc_traceback = sys.exc_info()
        strIO = StringIO()
        traceback.print_tb(exc_traceback, None, strIO)
        debug_logger.debug(strIO.getvalue())
        debug_logger.handlers.remove(handler)  # remove it again to not mess up real setup
    import pyknic

# shortcut for pyknic.settings
settings = pyknic.settings

# add pyknicpygame specific settings
settings.resolution = (800, 600)


# check pygame version
import pygame

pyknic.check_version((1, 9, 3), pygame.version.vernum, (1, 9, 0))

# check pyknic version
pyknic.check_version((4, 0, 0, 0), pyknic.info.version_info, (3, 0, 0, 0))

logger = None

# read only
is_initialized = False


def init(log_config=pyknic.logs.get_default_logger_config()):
    global is_initialized
    # TODO: invert if
    if not is_initialized:
        is_initialized = True

        if log_config == pyknic.logs.get_default_logger_config():
            # setup handlers here, reuse whatever loggers pyknic has
            _logger_config = log_config['loggers'].get(__name__, {})
            _logger_config['handlers'] = log_config['loggers'][settings.log_logger_name]['handlers']
            _logger_config['level'] = log_config['loggers'][settings.log_logger_name]['level']
            log_config['loggers'][__name__] = _logger_config

        pyknic.init(log_config=log_config)

        _logger = logging.getLogger(__name__)
        _logger.addHandler(logging.NullHandler())  # prevent handler not configured warning

        _logger.info("*" * settings.log_separator_width)
        _format_string = "%-" + str(settings.log_separator_width) + "s"
        _logger.info(_format_string % ("pyknicpygame v" + str(__version__) + " (c) DR0ID 2011"))
        _logger.info("*" * settings.log_separator_width)

        _logger.info("using pygame version: " + str(pygame.version.ver) + \
                     " (SDL: " + str(pygame.get_sdl_version()) + ")")

        # local imports
        from . import context
        from . import transform
        from . import spritesystem
        from . import surface
        from . import resource

    else:
        logging.warning("pyknicpygame already initialized, should only called once")
