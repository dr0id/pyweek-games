Game intro
==========
Given. You learned the basics: a single equation that weakens a zombie: x = x - 1. You can spam this at a zombie to get
him to zero, and he turns back to a civilian.


New scene
=========
Intro. You remember your dream in class, which teaches you a new equation. This gets added to your notebook, so you
have two now.

1. You can use a = F x m to slow a zombie down for a couple seconds.
2. You can spam x = x - 1 to get his hitpoints down to zero.

You snap out of it and see...

1: zombie chasing you.

You can lead him around and between rows of chairs. Slow him down and spam him. Then run away around chairs a few
seconds. Then slow, spam, ...


New scene
=========
Intro. You remember your dream in class, which teaches you a new equation. This gets added to your notebook, so you
have three now.

1. You can use a = F x m to slow a zombie down for a couple seconds. (This works on singles.)
2. You can spam x = x - 1 to get his hitpoints down to zero.
3. You can knock him back. (Interrupt zombie attack. This works on groups.)

You snap out of it and see...

1: zombie vs man
2: zombie vs man
3: zombie x 2 vs man

You have to save #3 first, because 2 on 1 will be converted twice as fast.

But you have to switch between #1 and #2 to weaken them so they will not be at full strength.

So you knock back #2. Then slow one of them. Then run between #1, #2, and #3 spamming as many as you can: but mostly
spam #3. You win #3.

Then go to #2. Knock him back and slow him. Spam him. You win.

But #1 has had time to turn his victim. So you have to manage two chasing you.
