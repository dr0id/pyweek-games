import os
from random import randrange
import pygame
from pygame.locals import *


class Sprite(object):
    def __init__(self, image, pos, anchor='topleft'):
        self.image = image
        self.rect = image.get_rect()
        self.anchor = anchor

        self._pos = [0, 0]
        self.pos = pos

    def _get_pos(self):
        return self._pos

    def _set_pos(self, val):
        self._pos[:] = val
        setattr(self.rect, self.anchor, val)
    pos = property(_get_pos, _set_pos)


class Game(object):
    def __init__(self):
        self.screen = pygame.display.get_surface()
        self.screen_rect = self.screen.get_rect()
        self.clear_color = Color('lightblue')

        self.clock = pygame.time.Clock()
        self.max_fps = 60
        self.dt = 1.0 / self.max_fps
        self.timer_reset = 1.0
        self.timer = self.timer_reset

        self.running = False

        self.bgsprites = []
        self.sprites = []
        self.mouses = []
        self.selected_furniture = None
        self.selected_offset = 0, 0
        self.mouse_pos = 0, 0

        sr = self.screen_rect
        color = Color('beige')
        image = pygame.Surface(sr.size)
        image.fill(color)
        pygame.draw.line(image, Color('black'), sr.center, (sr.left, sr.bottom))
        pygame.draw.line(image, Color('black'), sr.center, (sr.right, sr.centery))
        self.bgsprites.append(Sprite(image, (0, 0)))

        color = [c * 0.95 for c in color]
        image = pygame.Surface(sr.size)
        image.fill(color)
        pygame.draw.line(image, Color('black'), (sr.left, sr.centery), (sr.right, sr.centery))
        self.bgsprites.append(Sprite(image, (sr.right, 0)))

        color = [c * 0.95 for c in color]
        image = pygame.Surface(sr.size)
        image.fill(color)
        pygame.draw.line(image, Color('black'), sr.center, (sr.right, sr.bottom))
        pygame.draw.line(image, Color('black'), sr.center, (sr.left, sr.centery))
        self.bgsprites.append(Sprite(image, (sr.right * 2, 0)))

        self.world_rect = self.screen_rect.copy()
        self.world_rect.w *= len(self.bgsprites)
        self.camera = self.screen_rect.copy()
        self.camera.centerx = self.screen_rect.right

        xbound = 0, self.world_rect.w - 64
        ybound = self.world_rect.centery, self.world_rect.h - 64
        for i in range(3):
            image = pygame.Surface((64, 64))
            image.fill(Color('blue'))
            self.sprites.append(Sprite(image, (randrange(*xbound), randrange(*ybound))))

        image = pygame.Surface((64, 64))
        image.fill(Color('grey'))
        self.mouses.append(Sprite(image, (0, 0), 'center'))

    def run(self):
        self.running = True
        pygame.event.set_grab(True)
        while self.running:
            self.update_timer()
            self.handle_events()
            self.update(self.dt)
            self.draw()
        pygame.event.set_grab(False)

    def update(self, dt):
        self.mouses[0].pos = self.screen_to_world(self.mouse_pos)
        pygame.display.set_caption('{} {} {}'.format(
            self.mouse_pos, self.mouses[0].rect.center, self.selected_furniture))
        if self.selected_furniture:
            x, y = self.screen_to_world(self.mouse_pos)
            self.selected_furniture.rect.x = x - self.selected_offset[0]
            self.selected_furniture.rect.y = y - self.selected_offset[1]

    def draw(self):
        self.screen.fill(self.clear_color)
        cx, cy = self.camera.topleft
        for g in (self.bgsprites, self.sprites, self.mouses):
            for s in g:
                self.screen.blit(s.image, s.rect.move(-cx, -cy))
        pygame.display.flip()

    def update_timer(self):
        self.clock.tick(self.max_fps) / 1000.0
        self.timer -= self.dt
        if self.timer <= 0.0:
            self.timer = self.timer_reset
            pygame.display.set_caption('{} fps'.format(int(self.clock.get_fps())))

    def screen_to_world(self, pos):
        sr = self.screen_rect
        wr = self.world_rect
        fx = float(pos[0]) / sr.w
        fy = float(pos[1]) / sr.h
        return wr.w * fx, wr.h * fy

    def pan_to_pos(self, pos):
        sr = self.screen_rect
        wr = self.world_rect
        fx = float(pos[0]) / sr.w
        fy = float(pos[1]) / sr.h
        return (wr.w - sr.w) * fx, (wr.h - sr.h) * fy

    def handle_events(self):
        for e in pygame.event.get():
            if e.type == KEYDOWN:
                if e.key == K_ESCAPE:
                    self.running = False
            elif e.type == QUIT:
                self.running = False
            elif e.type == MOUSEMOTION:
                self.mouse_pos = e.pos
                x, y = self.pan_to_pos(e.pos)
                self.camera.x = x
            elif e.type == MOUSEBUTTONDOWN:
                self.mouse_pos = e.pos
                pos = self.screen_to_world(e.pos)
                for s in self.sprites:
                    if s.rect.collidepoint(pos):
                        self.selected_furniture = s
                        self.selected_offset = pos[0] - s.rect.x, pos[1] - s.rect.y
                        break
            elif e.type == MOUSEBUTTONUP:
                self.selected_furniture = None


if __name__ == '__main__':
    os.environ['SDL_VIDEO_CENTERED'] = '1'
    pygame.init()
    resolution = 800, 600
    pygame.display.set_mode(resolution)
    Game().run()
