import pygame
from pygame.locals import *


class Sprite(object):
    def __init__(self, image, pos, anchor='topleft'):
        self.image = image
        self.rect = image.get_rect()
        self.anchor = anchor

        self._pos = [0, 0]
        self.pos = pos

    def _get_pos(self):
        return self._pos

    def _set_pos(self, val):
        self._pos[:] = val
        setattr(self.rect, self.anchor, val)
    pos = property(_get_pos, _set_pos)


class Game(object):
    def __init__(self):
        self.screen = pygame.display.get_surface()
        self.screen_rect = self.screen.get_rect()
        self.clear_color = Color('lightblue')

        self.clock = pygame.time.Clock()
        self.max_fps = 60
        self.dt = 1.0 / self.max_fps
        self.timer_reset = 1.0
        self.timer = self.timer_reset

        self.running = False

        self.bgsprites = []
        self.sprites = []

        sr = self.screen_rect
        color = Color('beige')
        image = pygame.Surface(sr.size)
        image.fill(color)
        pygame.draw.line(image, Color('black'), sr.center, (sr.left, sr.bottom))
        pygame.draw.line(image, Color('black'), sr.center, (sr.right, sr.centery))
        self.bgsprites.append(Sprite(image, (0, 0)))

        color = [c * 0.95 for c in color]
        image = pygame.Surface(sr.size)
        image.fill(color)
        pygame.draw.line(image, Color('black'), (sr.left, sr.centery), (sr.right, sr.centery))
        self.bgsprites.append(Sprite(image, (sr.right, 0)))

        color = [c * 0.95 for c in color]
        image = pygame.Surface(sr.size)
        image.fill(color)
        pygame.draw.line(image, Color('black'), sr.center, (sr.right, sr.bottom))
        pygame.draw.line(image, Color('black'), sr.center, (sr.left, sr.centery))
        self.bgsprites.append(Sprite(image, (sr.right * 2, 0)))

        self.world_rect = self.screen_rect.copy()
        self.world_rect.w *= len(self.bgsprites)
        self.camera = self.screen_rect.copy()
        self.camera.centerx = self.screen_rect.right

    def run(self):
        self.running = True
        pygame.event.set_grab(True)
        while self.running:
            self.update_timer()
            self.handle_events()
            self.update(self.dt)
            self.draw()
        pygame.event.set_grab(False)

    def update(self, dt):
        pass

    def draw(self):
        self.screen.fill(self.clear_color)
        cx, cy = self.camera.topleft
        for s in self.bgsprites:
            self.screen.blit(s.image, s.rect.move(-cx, -cy))
        pygame.display.flip()

    def update_timer(self):
        self.clock.tick(self.max_fps) / 1000.0
        self.timer -= self.dt
        if self.timer <= 0.0:
            self.timer = self.timer_reset
            pygame.display.set_caption('{} fps'.format(int(self.clock.get_fps())))

    def screen_to_world(self, pos):
        sr = self.screen_rect
        wr = self.world_rect
        fx = float(pos[0]) / sr.w
        fy = float(pos[1]) / sr.h
        return wr.w * fx, wr.h * fy

    def pan_to_pos(self, pos):
        sr = self.screen_rect
        wr = self.world_rect
        fx = float(pos[0]) / sr.w
        fy = float(pos[1]) / sr.h
        return (wr.w - sr.w) * fx, (wr.h - sr.h) * fy

    def handle_events(self):
        for e in pygame.event.get():
            if e.type == KEYDOWN:
                if e.key == K_ESCAPE:
                    self.running = False
            elif e.type == QUIT:
                self.running = False
            elif e.type == MOUSEMOTION:
                pos = self.pan_to_pos(e.pos)
                self.camera.x = pos[0]


if __name__ == '__main__':
    pygame.init()
    resolution = 1024, 768
    pygame.display.set_mode(resolution)
    Game().run()
