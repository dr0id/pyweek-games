# -*- coding: utf-8 -*-
import glob
import json

file_names = glob.glob("*.bmp")

sprites = []
for idx, fn in enumerate(sorted(file_names)):
    if fn.startswith("greeting") or fn.startswith("hit") or fn.startswith("tip") or fn.startswith("running"):
        continue


    ffront = fn.split(".")[0]
    sn = ffront[-4:]
    if fn.startswith("stopped"):
        n = 0
        a = "stopped"
        d = ['s', 'sw', 'w', 'nw', 'n', 'ne', 'e', 'se'][int(sn)]
    else:
        n = int(sn)
        a, d = ffront[:-4].rsplit(" ", 1)
        if " " in a:
            a = a.split(" ")[1]

    sprite = {
        "filename": fn,
        "gid": idx,
        "points": [[0, 0], [96, 0], [96, 96], [0, 96]],
        "anchor": [47, 73],
        "properties": {
            "action": a,
            "facing": d,
            "col": 0,
            "row": 0,
        }
    }
    sprites.append(sprite)

sprite_def = {
    "filename": "shooting e0000.bmp.sdef",
    "backgroundcolor": [110 - 1, 80 - 1, 52 - 1],  # (109, 79, 51, 254)
    "sprites": sprites,
    "version": "1.0"
}

# json.dumps(sprite_def)
with open("shooting e0000.bmp.sdef", "w") as fp:
    json.dump(sprite_def, fp, indent=4, separators=(',', ': '), sort_keys=True)
