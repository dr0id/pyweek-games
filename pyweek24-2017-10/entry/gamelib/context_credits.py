# -*- coding: utf-8 -*-

from __future__ import print_function
import logging

import pygame
from pygame.locals import Color

import pyknic
from pyknic.mathematics import Point2
from pyknic.pygame_wrapper.resource import ImageLoader, SongLoader
from pyknic.pygame_wrapper.spritesystem import Camera, DefaultRenderer, Sprite
from gamelib import settings
from gamelib.util import pygametext


logger = logging.getLogger(__name__)

image_loader = ImageLoader()
song_loader = SongLoader()


class Credits(pyknic.context.Context):
    def __init__(self, *args, **kwargs):
        self.image = pygametext.getsurf('Credits pyweek 24, 2017', **settings.font_themes['intro_title'])
        self.dr0id = pygametext.getsurf('DR0ID', **settings.font_themes['intro_title'])
        self.dr0id = pygame.transform.rotate(self.dr0id, -90)
        self.dr0id_spr = Sprite(self.dr0id, Point2(-10, 0), z_layer=0.5)

        self.gumm = pygametext.getsurf('Gummbum', **settings.font_themes['intro_title'])
        self.gumm = pygame.transform.rotate(self.gumm, -90)
        self.gumm_spr = Sprite(self.gumm, Point2(20, 400), z_layer=0.5)

        self.rect = self.image.get_rect(center=settings.screen_rect.center)
        self.cam = Camera(pygame.Rect((0, 0), settings.screen_size))
        self.renderer = DefaultRenderer()

    def enter(self):
        pygame.event.clear()
        crack1 = pygame.image.load("./data/gfx/cracks/crack1.png")
        spr1 = Sprite(crack1, Point2(0, 0), z_layer=0)
        crack2 = pygame.image.load("./data/gfx/cracks/crack2.png")
        spr2 = Sprite(crack2, Point2(0, 0), z_layer=1)
        self.renderer.add_sprite(spr1)
        self.renderer.add_sprite(spr2)
        spr = Sprite(self.image, Point2(0, 350), z_layer=1.1)
        self.renderer.add_sprite(spr)
        self.renderer.add_sprite(self.gumm_spr)
        self.renderer.add_sprite(self.dr0id_spr)
        img = pygame.Surface((300, 500))
        img.fill((160, 160, 160))
        self.renderer.add_sprite(Sprite(img, Point2(0, 400), z_layer=1))

    def exit(self):
        pass

    def suspend(self):
        pass

    def resume(self):
        pygame.event.clear()

    def update(self, dt, sim_time):
        self._handle_events()
        self.update_spr(self.gumm_spr, dt)
        self.update_spr(self.dr0id_spr, dt)

    def update_spr(self, spr, dt):
        spr.position.y -= 300 * dt
        if spr.position.y < -500:
            spr.position.y = 400
        Sprite._dirty_sprites.add(spr)

    def draw(self, screen, do_flip=False, interpolation_factor=1.0):
        screen.fill((160, 160, 160))

        self.renderer.update_z_layers()
        self.renderer.draw(screen, self.cam)
        if do_flip:
            pygame.display.flip()

    def _handle_events(self):
        actions, extras = settings.gameplay_event_map.get_actions(pygame.event.get())
        if settings.debug_event_handlers and (actions or extras):
            logger.debug('actions={} extras={}'.format(actions, extras))
        for action, extra in actions:
            if action in (settings.ACTION_QUIT, settings.ACTION_WIN_LEVEL):
                self.pop()
