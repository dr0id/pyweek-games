# -*- coding: utf-8 -*-
"""
TODO: docstring
"""
from __future__ import division, print_function

import logging

import pygame
from pyknic import context
import pyknic
from gamelib import settings
from gamelib import sfx
from gamelib import gfx
from gamelib import resource_sound
from gamelib import context_intro, context_mainmenu, context_gameplay, context_credits, context_cutscene


logger = logging.getLogger(__name__)


def init_timing():
    lock_stepper = pyknic.timing.LockStepper(timestep=settings.sim_time_step, max_steps=8, max_dt=settings.sim_time_step * 10)

    pyknic.context.set_deferred_mode(True)  # avoid routing problems, due stack operations only at update
    context_top = pyknic.context.top
    context_update = pyknic.context.update

    def _update_top_context(dt, simt, ls):
        top_context = context_top()
        if top_context:
            top_context.update(dt, simt)
        context_update()

    lock_stepper.event_integrate += _update_top_context

    def _do_draw(dt, simt, *args):
        top_context = context_top()
        if top_context:
            top_context.draw(settings.screen, do_flip=True, interpolation_factor=lock_stepper.alpha)

    frame_cap = pyknic.timing.FrameCap()
    frame_cap.event_update += _do_draw

    game_time = pyknic.timing.GameTime()
    game_time.event_update += frame_cap.update
    game_time.event_update += lock_stepper.update

    return game_time


def _print_fps(clock, draw_clock, sim_clock):
    if settings.print_fps:
        settings.FPS = fps = clock.get_fps()
        settings.GFX_FPS = gfx_fps = draw_clock.get_fps()
        settings.SIM_FPS = sim_fps = sim_clock.get_fps()
        logger.debug('main loop fps: %s   draw fps: %s   sim fps: %s', str(fps), str(gfx_fps), str(sim_fps))
    return 1  # return next interval


def main():
    pyknic.logs.log_environment()
    pyknic.logs.print_logging_hierarchy()

    pygame.mixer.pre_init(22050, -16, 2, 64)
    pygame.init()

    settings.screen = pygame.display.set_mode(settings.screen_size, settings.screen_flags)
    settings.screen_rect = settings.screen.get_rect()
    icon = pygame.image.load("./data/gfx/misc/icon.png")
    pygame.display.set_icon(icon)
    pygame.display.set_caption(settings.title)

    pygame.mixer.set_num_channels(settings.num_channels)
    settings.sfx_handler = sfx.Sound()
    settings.sfx_handler.load(resource_sound.sfx_data)
    # reserved mixer channels
    settings.channel_drama = pygame.mixer.Channel(settings.channel_drama)
    settings.channel_gun_fire = pygame.mixer.Channel(settings.channel_gun_fire)
    settings.channel_explosions = pygame.mixer.Channel(settings.channel_explosions)
    settings.channel_hero_speaking = pygame.mixer.Channel(settings.channel_hero_speaking)
    settings.channel_hero_moving = pygame.mixer.Channel(settings.channel_hero_moving)
    settings.channel_ants_speaking = pygame.mixer.Channel(settings.channel_ants_speaking)
    settings.channel_ants_moving = pygame.mixer.Channel(settings.channel_ants_moving)

    settings.screen.fill(settings.default_fill_color)
    settings.gfx_handler = gfx.Graphics()
    settings.gfx_handler.load()

    # Using the dev-debug startup cases.
    #
    # 1. For convenience, put the following in your gamelib/_YOURNAME_custom.py
    #
    # 0: context_intro
    # 1: context_mainmenu
    # 2: context_gameplay
    # 3: context_credits
    # 4: context_cutscene
    #
    # (startup_context, startup_level) = (
    #     (0, 1),     # default: beginning
    #     (1, 3),     # mainmenu
    #     (2, 1),     # gameplay
    #     (3, 1),     # credits
    #     (4, 1),     # cutscene
    # )[1]
    #
    # 2. And put this at the end of settings.py
    #
    # try:
    #   import _YOURNAME_custom
    # except:
    #   pass
    #
    ctx = None
    if settings.startup_context == 0:
        ctx = context_intro.Intro()
    elif settings.startup_context == 1:
        ctx = context_mainmenu.MainMenu(level=settings.startup_level)
    elif settings.startup_context == 2:
        ctx = context_gameplay.Gameplay(level=settings.startup_level)
    elif settings.startup_context == 3:
        ctx = context_credits.Credits()
    elif settings.startup_context == 4:
        ctx = context_cutscene.Cutscene()
    else:
        raise pygame.error('invalid startup context: {}'.format(settings.startup_context))
    context.push(ctx)

    game_time = init_timing()
    context_len = context.length
    clock = pygame.time.Clock()  # this is the only clock we need
    pyknic.timing.Timer.scheduler.schedule(
        settings.sfx_handler.update, settings.sfx_step, 0.0, settings.sfx_step, settings.sfx_step)
    while context_len():
        dt = clock.tick() / 1000.0  # convert to seconds
        game_time.update(dt)

    logger.debug('Finished. Exiting.')


# this is needed in order to work with py2exe
if __name__ == '__main__':
    main()
