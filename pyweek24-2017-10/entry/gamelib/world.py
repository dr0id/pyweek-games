# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'level.py' is part of CoDoGuPywk24
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]                    # For instance:
                   |                                  * 1.2.0.1 instead of 1.2-a
                   +-|* 0 for alpha (status)          * 1.2.1.2 instead of 1.2-b2 (beta with some bug fixes)
                     |* 1 for beta (status)           * 1.2.2.3 instead of 1.2-rc (release candidate)
                     |* 2 for release candidate       * 1.2.3.0 instead of 1.2-r (commercial distribution)
                     |* 3 for (public) release        * 1.2.3.5 instead of 1.2-r5 (commercial dist with many bug fixes)


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging
import random

import pygame
from gamelib.entities.grenade import Grenade, GrenadeBlast
from gamelib import settings, level_data, resource_sound
from gamelib.entities.bullet import Bullet
from gamelib.entities.cursor import Cursor
from gamelib.entities.thing import Thing
from gamelib.entities.wall import Wall
from gamelib.entities.ant_spigot import AntSpigot
from gamelib.util import spritefx
from pyknic.events import Signal
from pyknic.mathematics import Vec2
from gamelib.entities.player import Player
from gamelib.entities.tower import Tower
from gamelib.entities.ant import Ant
from pyknic.mathematics import Point2, sign
from pyknic.timing import Timer
from pyknic import tweening

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2017"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 

logger = logging.getLogger(__name__)
logger.debug("importing...")


class World(object):

    def __init__(self, level_num, the_registry, collision_detector, message_dispatcher, the_scheduler, graphics, sound):
        self.level_num = level_num
        self.collision_pairs = None
        self.messaging = message_dispatcher
        self.registry = the_registry
        self.coll_handlers = collision_detector
        self.scheduler = the_scheduler
        self.event_entity_created = Signal("entity created")
        self.gfx = graphics
        self.sfx = sound
        self.registry.register_entity(self.gfx)
        self.registry.register_entity(self.sfx)
        self.initialize_collision_system()

        self.ant_spigots = []
        self.screen_messages = []
        self.screen_message = None
        self.player = None
        self.cracks = []

        self.level_status = settings.LEVEL_RUNNING

    def end_level(self):
        if self.level_status == settings.LEVEL_WIN_PENDING:
            self.level_status = settings.LEVEL_WON
        elif self.level_status == settings.LEVEL_LOSS_PENDING:
            self.level_status = settings.LEVEL_LOST
        else:
            # Probably a dev-debug case
            self.level_status = settings.LEVEL_QUIT

        # This may be called from a Timer, so return 0.0
        return 0.0

    def create_player(self, position):
        player = Player(self, position)
        if self.level_num >= 5:
            player.health = settings.boss_level_hero_health
        # added: data sets location
        # player_data = [o for o in self.unhandled_objects if o.kind == settings.KIND_HERO][0]
        # player.position = Point2(*player_data.pos)
        self.registry.register_entity(player)
        self._create_anim_sprite(player, 1)  # layer 1 will be sorted specially for 2.5D
        return player

    def create_tower(self, position):
        tower = Tower(position)
        self.registry.register_entity(tower)
        self._create_anim_sprite(tower, 1)  # layer 1 will be sorted specially for 2.5D
        return tower

    def create_cursor(self):
        cursor = Cursor()
        self.registry.register_entity(cursor)
        self._create_sprite(cursor, 200)
        return cursor

    def create_ant(self, position, radius):
        ant = Ant(position, radius, self)
        if self.level_num >= 5:
            ant.eat_per_sec *= settings.boss_level_ant_eater
        self.registry.register_entity(ant)
        self._create_anim_sprite(ant, 1)  # layer 1 will be sorted specially for 2.5D
        return ant

    def create_bullet(self, position, direction, cursor_pos):
        bullet = Bullet(position, direction, cursor_pos, self)
        self.registry.register_entity(bullet)
        self._create_anim_sprite(bullet, 1)  # layer 1 will be sorted specially for 2.5D
        return bullet

    def _create_sprite(self, entity, layer):
        self.gfx.create_sprite(entity.kind, entity.id, entity.position, layer)

    def _create_anim_sprite(self, entity, layer, fps=None):
        self.gfx.create_animations(entity, layer, fps)

    def create_grenade(self, position, direction, cursor_pos):
        gn = Grenade(position, direction, cursor_pos, self)
        self.registry.register_entity(gn)
        self._create_anim_sprite(gn, 2, 10)  # layer 1 will be sorted specially for 2.5D
        self.sfx.handle_message(None, None, resource_sound.SFX_GRENADE_TOSS, None)
        return gn

    def create_blast(self, position):
        bl = GrenadeBlast(position.clone(), self)
        self.registry.register_entity(bl)
        # self._create_anim_sprite(bl, 1)
        self._create_anim_sprite(bl, 2)
        self.sfx.handle_message(None, None, resource_sound.SFX_GRENADE_EXPLODE, None)

    def update(self, dt, sim_t):
        self.scheduler.update(dt, sim_t)
        Timer.scheduler.update(dt, sim_t)
        self.gfx.update(dt, sim_t)
        self.sfx.update(dt, sim_t)

        # post any queued up screen messages
        if self.screen_message is not None and not self.screen_message.is_alive():
            self.screen_message = None
        if self.screen_messages and self.screen_message is None:
            ent_data = self.screen_messages.pop(0)
            self.screen_message = ent_data
            Timer.scheduler.schedule(ent_data.make_sprite, 0.0)

        group = (settings.KIND_ANT | settings.KIND_BULLET | settings.KIND_HERO | settings.KIND_GRENADE |
                 settings.KIND_ANT_SPIGOT | settings.KIND_LEVEL_CONTROL)
        entities = self.registry.get_by_bit_kind(group)
        for ent in entities:
            ent.update(dt, sim_t)

        # collision detection
        get_ent_by_kind = self.registry.get_by_kind
        get_coll_func = self.coll_handlers.get_by_kind
        for k1, k2 in self.collision_pairs:
            func = get_coll_func(k1, k2)
            func(get_ent_by_kind(k1), get_ent_by_kind(k2))  # do the heavy looping in the method!

        # TODO: I'm sure we don't want to start this dirty snowball
        # Level 1: win-level check for shrubs.
        if self.level_num == 1 and self.level_status == settings.LEVEL_RUNNING:
            all_trimmed = True
            for ent in self.registry.get_by_bit_kind(settings.KIND_SHRUB1):
                if ent.scale > 0.75:
                    all_trimmed = False
                    break
            if all_trimmed:
                self.sfx.handle_message(None, None, resource_sound.SFX_MUNCH_2, 'queue')
                Timer.scheduler.schedule(self.end_level, 5.0)
                self.level_status = settings.LEVEL_WIN_PENDING

    def remove_entity(self, entity):
        self.registry.save_remove_by_id(entity.id)
        self.gfx.remove_by_id(entity.id)

    def initialize_collision_system(self):
        self.coll_handlers.register(settings.KIND_BULLET, settings.KIND_ANT, self._coll_bullet_ant)
        self.coll_handlers.register(settings.KIND_HERO, settings.KIND_TOWER, self._coll_player_walk)
        self.coll_handlers.register(settings.KIND_HERO, settings.KIND_ANT, self._coll_player_walk_over_ant)
        self.coll_handlers.register(settings.KIND_HERO, settings.KIND_THINGS, self._coll_player_walk)
        # self.coll_handlers.register(settings.KIND_HERO, settings.KIND_YARD, self._coll_player_walk)
        # self.coll_handlers.register(settings.KIND_HERO, settings.KIND_SOFA, self._coll_player_walk)
        self.coll_handlers.register(settings.KIND_HERO, settings.KIND_SHRUB1, self._coll_player_shrub)
        # self.coll_handlers.register(settings.KIND_HERO, settings.KIND_CHAIR1, self._coll_player_walk)
        # self.coll_handlers.register(settings.KIND_HERO, settings.KIND_CHAIR2, self._coll_player_walk)
        # self.coll_handlers.register(settings.KIND_HERO, settings.KIND, self._coll_player_walk)
        self.coll_handlers.register(settings.KIND_HERO, settings.KIND_WALL, self._coll_player_wall)
        self.coll_handlers.register(settings.KIND_ANT, settings.KIND_BLAST, self._coll_grenade_ant)
        self.coll_handlers.register(settings.KIND_TOWER, settings.KIND_BLAST, self._coll_grenade_ant)

        self.coll_handlers.register(settings.KIND_BULLET, settings.KIND_SHRUB1, self._coll_bullet_shrub)
        self.coll_handlers.register(settings.KIND_SHRUB1, settings.KIND_BLAST, self._coll_grenade_shrub)

        self.collision_pairs = list(self.coll_handlers.collision_handlers.keys())  # [(settings.KIND_BULLET, settings.KIND_ANT)]

    def _coll_grenade_ant(self, ants, blasts):
        for g in blasts:
            for ant in ants:
                if g.position.get_distance_sq(ant.position) > g.blast_radius_sq:
                    continue
                ant.handle_message(g.id, ant.id, settings.MSG_HIT, g)

    def _coll_player_wall(self, players, walls):
        for p in players:
            for w in walls:
                # check if p is moving away
                if p.v.dot(w.normal) > 0:
                    continue

                # check it is within wall range
                ap = p.position - w.position
                bp = p.position - w.end

                if w.ab.dot(ap) < 0 or w.ab.dot(bp) > 0:
                    # not in range
                    continue

                # check if p passed the wall
                if w.normal.dot(ap) > 0:
                    continue

                # # check if movement crossed the wall or not
                # op = p.old_position - w.position
                # if w.normal.dot(op) < 0:
                #     continue

                # behind the wall, push back
                projection = ap.project_onto(w.normal)

                if projection.length_sq > p.radius**2:
                    # too far away
                    continue

                p.position += -projection
                p.v += -p.v.project_onto(w.normal)

    def _coll_player_shrub(self, players, scrubs):
        for p in players:
            for o in scrubs:
                is_behind = p.position.y < o.position.y
                was_behind = p.old_position.y < o.position.y
                if is_behind != was_behind:
                    if o.position.x - o.radius < p.position.x < o.position.x + o.radius:
                        p.position.y = p.old_position.y + 0 * sign(p.old_position.y - p.position.y)

    def _coll_player_walk_over_ant(self, players, ants):
        for p in players:
            for a in ants:
                if a.health <= 0 or self.level_status == settings.LEVEL_LOSS_PENDING:
                    continue
                radius_sum = p.radius + a.radius
                if p.position.get_distance_sq(a.position) < (radius_sum)**2:
                    a.handle_message(p.id, a.id, settings.MSG_STOMP, p)

    def _coll_player_walk(self, players, others):
        for p in players:
            for o in others:
                radius_sum = p.radius + o.radius
                if p.position.get_distance_sq(o.position) < (radius_sum)**2:
                    way = p.position - o.position
                    way.length = radius_sum
                    p.position.copy_values(o.position + way)

    def _coll_bullet_ant(self, bullets, ants):
        for b in bullets:
            for a in ants:
                if a.health > 0 and b.height <= a.height and a.position.get_distance_sq(b.position) < (a.radius + b.radius)**2:
                    a.handle_message(None, None, settings.MSG_HIT, b)
                    self.remove_entity(b)

    def _coll_bullet_shrub(self, bullets, shrubs):
        dead = set()
        for b in bullets:
            for s in shrubs:
                if s.radius > 2:
                    r2 = s.radius + b.radius
                    pos = Vec2(*s.position) - Vec2(0, s.radius)
                    if pos.get_distance_sq(b.position) < r2 * r2:
                        s.scale *= 0.999    # <<< this will take all day, so player has to use grenade :)
                        s.radius = s.radius_orig * s.scale
                        spr = self.gfx.sprites[s.id]
                        spr.zoom = s.scale
                        dead.add(b)
        for e in dead:
            self.remove_entity(e)

    def _coll_grenade_shrub(self, shrubs, blasts):
        for b in blasts:
            for s in shrubs:
                if s.radius > 2:
                    r2 = s.radius + b.radius
                    pos = Vec2(*s.position) - Vec2(0, s.radius)
                    if pos.get_distance_sq(b.position) < r2 * r2:
                        s.scale *= 0.99     # <<< this damage repeats for the duration of the blast entity
                        s.radius = s.radius_orig * s.scale
                        spr = self.gfx.sprites[s.id]
                        spr.zoom = s.scale

    def load_level(self, level_num):
        self.gfx.cam.ambient_is_on = False
        self.registry.clear()
        self.gfx.clear()

        make_sprite_for_kinds = (
            settings.KIND_YARD, settings.KIND_SHRUB1,
            settings.KIND_ROOM,
            settings.KIND_SOFA, settings.KIND_CHAIR1, settings.KIND_CHAIR2, settings.KIND_THINGS)

        _level_data = level_data.levels[level_num]
        # level_title = _level_data[0]
        # pygame.display.set_caption(settings.title + ": " + level_title, settings.title)
        # x, y = settings.screen_rect.center
        # y *= 0.6
        # spritefx.TextSprite(level_title, (x, y), expire=5.0, anchor='center', theme_name='gameplay_prompt').fade_effect(
        #     duration=5.0, tween_function=tweening.ease_in_quint)
        level_title = None
        for ent_data in _level_data:
            kind = ent_data.kind
            if kind in make_sprite_for_kinds:
                logger.info('World.load_level: creating entity for map object {}', ent_data.__class__.__name__)
                radius = min(*self.gfx.image_cache[kind].get_rect().center) if not hasattr(ent_data, 'radius') else ent_data.radius
                self.create_thing(ent_data.kind, Point2(*ent_data.pos), ent_data.z_layer, radius, ent_data.anchor)
            elif kind == settings.KIND_TOWER:
                logger.info('World.load_level: creating entity for map object {}', ent_data.__class__.__name__)
                self.create_tower(Point2(*ent_data.pos))
            elif kind == settings.KIND_HERO:
                logger.info('World.load_level: creating entity for map object {}', ent_data.__class__.__name__)
                self.player = self.create_player(Point2(*ent_data.pos))
            elif kind == settings.KIND_WALL:
                logger.info('World.load_level: creating entity for map object {}', ent_data.__class__.__name__)
                wall = Wall(Point2(*ent_data.pos), Point2(*ent_data.end), ent_data.height)
                self.registry.register_entity(wall)
            elif kind == settings.KIND_ANT_SPIGOT:
                logger.info('World.load_level: creating entity for map object {}', ent_data.__class__.__name__)
                ant_spigot = AntSpigot(
                    Point2(*ent_data.pos), ent_data.frequency, ent_data.per_pulse, ent_data.count, self)
                self.registry.register_entity(ant_spigot)
                self.ant_spigots.append(ant_spigot)
                w, h = settings.screen_rect.size
                w2 = w / 2
                h2 = h / 2
                x, y = ent_data.pos
                x += w2
                y += h2
                self.cracks.append(spritefx.SpriteFX(self.gfx.image_cache[settings.KIND_CRACK],
                                                     (x, y), angle=360 * random.random(), scale=0.5,
                                                     anchor='center'))
            elif kind == settings.KIND_LEVEL_CONTROL:
                if ent_data.state != settings.TOWER_WATCHER_SPAWNED:
                    try:
                        ent_data.__init__(ent_data._towers_needed)
                    except:
                        ent_data.__init__()
                ent_data.world = self
                self.registry.register_entity(ent_data)
            elif kind == settings.KIND_GAME_TEXT:
                self.screen_messages.append(ent_data)
                if level_title is None:
                    level_title = ent_data.text
                    pygame.display.set_caption(settings.title + ": " + level_title)
            elif kind == settings.KIND_LIGHT:
                assert self.player is not None, "make sure light is after hero in level_data"
                self.gfx.create_light(ent_data.ambient_color, self.player)
            else:
                logger.error('World.load_level: unhandled map object {}', ent_data.__class__.__name__)
                # unhandled_objects.append(ent_data)

    def create_thing(self, kind, position, layer, radius=50, anchor="midbottom"):
        thing = Thing(kind, position, radius)
        self.registry.register_entity(thing)
        self.gfx.create_sprite(kind, thing.id, thing.position, layer, anchor)


logger.debug("imported")
