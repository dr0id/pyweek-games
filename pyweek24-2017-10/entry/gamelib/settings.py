# -*- coding: utf-8 -*-
import os
import pygame
from pyknic.pygame_wrapper.eventmapper import EventMapper, ANY_KEY, ANY_MOD
from pyknic.generators import GlobalIdGenerator, FlagGenerator, IdGenerator
from gamelib.util import pygametext, imagecache


# ------------------------------------------------------------------------------
# Tunables
# ------------------------------------------------------------------------------

print_fps = False
master_volume = 1.0             # min: 0.0; max: 1.0

boss_level_difficulty = 0.8     # 1.0 is probably impossible; 0.5 is about as hard as level 4
boss_level_hero_health = 300    # for normal levels see PlayerTemplate.player_health
boss_level_ant_eater = 0.5      # ant damage slowed a bit for boss level
ambient_color = 96, 96, 96      # pygame color values; higher for more visibility in dark levels; None for OFF
player_is_strafe_while_shooting_allowed = False


# ------------------------------------------------------------------------------
# CPU
# ------------------------------------------------------------------------------

ticks_per_second = 30
frames_per_second = 150
time_step = 1.0 / ticks_per_second
sim_time_step = time_step
frame_step = 1.0 / frames_per_second
sfx_step = 1.0


# ------------------------------------------------------------------------------
# Display
# ------------------------------------------------------------------------------

os.environ['SDL_VIDEO_CENTERED'] = '1'

screen_width = 1024
screen_height = 768
screen_size = screen_width, screen_height
screen_flags_presets = (
    0,
    pygame.FULLSCREEN | pygame.DOUBLEBUF,
)
screen_flags = screen_flags_presets[0]
default_fill_color = 0, 0, 64
screen = None
screen_rect = None
title = "cANT2"


# ------------------------------------------------------------------------------
# Game
# ------------------------------------------------------------------------------

# 0: context_intro
# 1: context_mainmenu
# 2: context_gameplay
# 3: context_credits
# 4: context_cutscene
startup_context = 0
startup_level = 1

# EventMappers (events -> actions)
debug_event_handlers = False

_action_generator = GlobalIdGenerator(0)
ACTION_QUIT = _action_generator.next()
ACTION_ESCAPE = _action_generator.next()
ACTION_WIN_LEVEL = _action_generator.next()
ACTION_LOOSE_LEVEL = _action_generator.next()
ACTION_FULL_STOP = _action_generator.next()

ACTION_MOVE_UP = _action_generator.next()
ACTION_MOVE_DOWN = _action_generator.next()
ACTION_MOVE_LEFT = _action_generator.next()
ACTION_MOVE_RIGHT = _action_generator.next()
ACTION_STOP_UP = _action_generator.next()
ACTION_STOP_DOWN = _action_generator.next()
ACTION_STOP_LEFT = _action_generator.next()
ACTION_STOP_RIGHT = _action_generator.next()
ACTION_FIRE = _action_generator.next()
ACTION_HOLD_FIRE = _action_generator.next()
ACTION_USE_ITEM = _action_generator.next()
ACTION_MOUSE_MOTION = _action_generator.next()

ACTION_SECONDARY = _action_generator.next()

ACTION_TOGGLE_DEBUG_RENDER = _action_generator.next()
ACTION_HUD_SHOW_DEBUGS = _action_generator.next()
ACTION_EAT_CHIPS = _action_generator.next()

# context_intro
intro_event_map = EventMapper({
    pygame.QUIT: {None: ACTION_QUIT},
    pygame.MOUSEBUTTONUP: {
        1: ACTION_WIN_LEVEL,
        2: ACTION_WIN_LEVEL,
        3: ACTION_WIN_LEVEL,
    },
    pygame.KEYUP: {
        (ANY_KEY, ANY_MOD): ACTION_WIN_LEVEL,
    },
})

# context_mainmenu
menu_event_map = EventMapper({
    pygame.QUIT: {None: ACTION_QUIT},
    # pygame.MOUSEBUTTONUP: {
    #     1: ACTION_WIN_LEVEL,
    #     2: ACTION_WIN_LEVEL,
    #     3: ACTION_WIN_LEVEL,
    # },
    pygame.KEYUP: {
        (pygame.K_RETURN, ANY_MOD): ACTION_WIN_LEVEL,
        (pygame.K_SPACE, ANY_MOD): ACTION_WIN_LEVEL,
    },
})

# context_gameplay
gameplay_event_map = EventMapper({
    pygame.QUIT: {None: ACTION_QUIT},
    pygame.MOUSEBUTTONDOWN: {
        1: ACTION_FIRE,
        3: ACTION_USE_ITEM,
    },
    pygame.MOUSEBUTTONUP: {
        1: ACTION_HOLD_FIRE,
    },
    pygame.MOUSEMOTION: {
        None: ACTION_MOUSE_MOTION,
    },
    pygame.KEYDOWN: {
        (pygame.K_a, ANY_MOD): ACTION_MOVE_LEFT,
        (pygame.K_d, ANY_MOD): ACTION_MOVE_RIGHT,
        (pygame.K_w, ANY_MOD): ACTION_MOVE_UP,
        (pygame.K_s, ANY_MOD): ACTION_MOVE_DOWN,
        (pygame.K_q, ANY_MOD): ACTION_USE_ITEM,
        (pygame.K_F1, ANY_MOD): ACTION_TOGGLE_DEBUG_RENDER,
        (pygame.K_F2, ANY_MOD): ACTION_EAT_CHIPS,
    },
    pygame.KEYUP: {
        (pygame.K_ESCAPE, ANY_MOD): ACTION_WIN_LEVEL,
        (pygame.K_a, ANY_MOD): ACTION_STOP_LEFT,
        (pygame.K_d, ANY_MOD): ACTION_STOP_RIGHT,
        (pygame.K_w, ANY_MOD): ACTION_STOP_UP,
        (pygame.K_s, ANY_MOD): ACTION_STOP_DOWN,
    },
})

# example for reference
__event_map = EventMapper({
    pygame.QUIT: {None: ACTION_QUIT},
    pygame.KEYDOWN: {
        (pygame.K_ESCAPE, ANY_MOD): ACTION_ESCAPE,
        (pygame.K_F4, pygame.KMOD_ALT): ACTION_QUIT,
        # cheat keys
        (pygame.K_F10, ANY_MOD): ACTION_WIN_LEVEL,
        (pygame.K_F9, ANY_MOD): ACTION_LOOSE_LEVEL,
        (pygame.K_F1, ANY_MOD): ACTION_TOGGLE_DEBUG_RENDER,
        (pygame.K_F2, ANY_MOD): ACTION_FULL_STOP,
        (pygame.K_F3, ANY_MOD): ACTION_HUD_SHOW_DEBUGS,
        # movement (arrows, joystick?, ...)
        (pygame.K_UP, ANY_MOD): ACTION_MOVE_UP,
        (pygame.K_DOWN, ANY_MOD): ACTION_MOVE_DOWN,
        (pygame.K_LEFT, ANY_MOD): ACTION_MOVE_LEFT,
        (pygame.K_RIGHT, ANY_MOD): ACTION_MOVE_RIGHT,
        (pygame.K_w, ANY_MOD): ACTION_MOVE_UP,
        (pygame.K_s, ANY_MOD): ACTION_MOVE_DOWN,
        (pygame.K_a, ANY_MOD): ACTION_MOVE_LEFT,
        (pygame.K_SPACE, ANY_MOD): ACTION_FIRE,
        (pygame.K_LCTRL, ANY_MOD): ACTION_FIRE,
        (pygame.K_m, ANY_MOD): ACTION_SECONDARY,
        (pygame.K_d, ANY_MOD): ACTION_MOVE_RIGHT,
    },
    pygame.KEYUP: {
        (pygame.K_SPACE, ANY_MOD): ACTION_HOLD_FIRE,
        (pygame.K_LCTRL, ANY_MOD): ACTION_HOLD_FIRE,
        (pygame.K_UP, ANY_MOD): ACTION_STOP_UP,
        (pygame.K_DOWN, ANY_MOD): ACTION_STOP_DOWN,
        (pygame.K_LEFT, ANY_MOD): ACTION_STOP_LEFT,
        (pygame.K_RIGHT, ANY_MOD): ACTION_STOP_RIGHT,
        (pygame.K_w, ANY_MOD): ACTION_STOP_UP,
        (pygame.K_s, ANY_MOD): ACTION_STOP_DOWN,
        (pygame.K_a, ANY_MOD): ACTION_STOP_LEFT,
        (pygame.K_d, ANY_MOD): ACTION_STOP_RIGHT,
    },
})


# ------------------------------------------------------------------------------
# messages
# ------------------------------------------------------------------------------
MSG_ANIM_END = _action_generator.next()
MSG_SFX_DRAMA_BUILD_UP = _action_generator.next()
MSG_HIT = _action_generator.next()
MSG_EAT = _action_generator.next()
MSG_RETREAT = _action_generator.next()
MSG_STOMP = _action_generator.next()


# ------------------------------------------------------------------------------
# kinds
# ------------------------------------------------------------------------------
_flag_gen = FlagGenerator()
KIND_HERO = _flag_gen.next()
KIND_BULLET = _flag_gen.next()
KIND_ANT = _flag_gen.next()
KIND_TOWER = _flag_gen.next()
KIND_TRIGGER = _flag_gen.next()
KIND_GRENADE = _flag_gen.next()
KIND_CRACK = _flag_gen.next()
KIND_SODA = _flag_gen.next()
KIND_THINGS = _flag_gen.next()
KIND_GFX = _flag_gen.next()
KIND_SFX = _flag_gen.next()
KIND_CURSOR = _flag_gen.next()
KIND_YARD = _flag_gen.next()
KIND_ROOM = _flag_gen.next()
KIND_SHRUB1 = _flag_gen.next()
KIND_CHAIR1 = _flag_gen.next()
KIND_CHAIR2 = _flag_gen.next()
KIND_SOFA = _flag_gen.next()
KIND_SUNNY_BIRD = _flag_gen.next()
KIND_WALL = _flag_gen.next()
KIND_BLAST = _flag_gen.next()
KIND_LEVEL_CONTROL = _flag_gen.next()
KIND_ANT_SPIGOT = _flag_gen.next()
KIND_GAME_TEXT = _flag_gen.next()
KIND_LIGHT = _flag_gen.next()

# make sure those two have this values because of the hack in the sprite module!
assert KIND_ANT == 4
assert KIND_TOWER == 8
assert KIND_SOFA == 131072


# Level control
_id_gen = IdGenerator(1)
LEVEL_RUNNING = _id_gen.next()
LEVEL_WIN_PENDING = _id_gen.next()
LEVEL_LOSS_PENDING = _id_gen.next()
LEVEL_WON = _id_gen.next()
LEVEL_LOST = _id_gen.next()
LEVEL_QUIT = _id_gen.next()


# Tower watcher control
_id_gen = IdGenerator(1)
TOWER_WATCHER_SPAWNED = _id_gen.next()
TOWER_WATCHER_RACING_ANTS = _id_gen.next()
TOWER_WATCHER_WON = _id_gen.next()
TOWER_WATCHER_LOST = _id_gen.next()


class TowerTemplate(object):
    health = 100


class GrenadeTemplate(object):
    grenade_blast_radius = 50
    grenade_damage = 1000  # might be too much!
    max_throw_distance_sq = 300**2


class PlayerTemplate(object):
    player_speed = 80
    player_height = 60
    player_throw_delay = 3  # in seconds
    player_gun_fire_rate = 0.1  # every n seconds a bullet is created
    player_radius = 10
    player_eat_per_sec = 50
    player_health = 50
    is_strafe_on = player_is_strafe_while_shooting_allowed


class BulletTemplate(object):
    bullet_speed = 850
    bullet_radius = 3
    bullet_accuracy_distance = 10  # means up to distance x it is accurate
    bullet_height = 30
    bullet_damage = 5  # might be too less


class AntTemplate(object):
    health = 100
    height = 40
    eat_per_sec = 10
    flee_speed = 60
    walk_speed = 30
    follow_timeout = 10  # seconds


# ------------------------------------------------------------------------------
# Graphics
# ------------------------------------------------------------------------------

gfx_handler = None
image_cache_expire = 1 * 60          # expire unused textures after N seconds; recommend 1 min (1 * 60)
image_cache_memory = 5 * 2 ** 20     # force expiration if memory is exceeded; recommend 5 MB (5 * 2**20)
image_cache = imagecache.ImageCache(image_cache_expire, image_cache_memory)
image_cache.enable_age_cap(True)

# ------------------------------------------------------------------------------
# Sound
# ------------------------------------------------------------------------------

sfx_path = 'data/sfx'
music_path = 'data/music'
num_channels = 16

sfx_handler = None

# reserved channels
num_reserved_channels = 7
(channel_drama,
 channel_gun_fire,
 channel_explosions,
 channel_hero_speaking,
 channel_hero_moving,
 channel_ants_speaking,
 channel_ants_moving) = tuple(range(num_reserved_channels))


# ------------------------------------------------------------------------------
# Fonts
# ------------------------------------------------------------------------------

pygametext.FONT_NAME_TEMPLATE = 'data/fonts/%s'
font_themes = dict(
    intro_title=dict(
        fontname='VeraBd.ttf',
        fontsize=50,
        color='red2',
        gcolor='gold',
        ocolor=None,
        owidth=0.5,
        scolor='grey10',
        shadow=(2, 2),
    ),
    intro_subtitle=dict(
        fontname='Vera.ttf',
        fontsize=38,
        color='lightblue',
        gcolor='green4',
        ocolor=None,
        owidth=0.5,
        scolor='grey10',
        shadow=(1, 1),
    ),
    mainmenu=dict(
        fontname='BIOST.TTF',
        fontsize=42,
        color='gold',
        gcolor='tomato',
        ocolor=None,
        owidth=0.5,
        scolor='black',
        shadow=(1, 1),
    ),
    gameplay_prompt=dict(
        fontname='Boogaloo.ttf',
        fontsize=55,
        color='yellow',
        gcolor='orange',
        ocolor='black',
        owidth=0.5,
        scolor=None,
        shadow=None,
    ),
)
font_themes['default'] = font_themes['intro_subtitle']

key_to_text = {
    pygame.K_BACKSPACE: ("\\b", "backspace"),
    pygame.K_TAB: ("\\t", "tab"),
    pygame.K_CLEAR: ("", "clear"),
    pygame.K_RETURN: ("\\r", "return"),
    pygame.K_PAUSE: ("", "pause"),
    pygame.K_ESCAPE: ("^[", "escape"),
    pygame.K_SPACE: ("", "space"),
    pygame.K_EXCLAIM: ("!", "exclaim"),
    pygame.K_QUOTEDBL: ("\"", "quotedbl"),
    pygame.K_HASH: ("#", "hash"),
    pygame.K_DOLLAR: ("$", "dollar"),
    pygame.K_AMPERSAND: ("&", "ampersand"),
    pygame.K_QUOTE: ("", "quote"),
    pygame.K_LEFTPAREN: ("(", "left parenthesis"),
    pygame.K_RIGHTPAREN: ("),", "right parenthesis"),
    pygame.K_ASTERISK: ("*", "asterisk"),
    pygame.K_PLUS: ("+", "plus sign"),
    pygame.K_COMMA: (",", "comma"),
    pygame.K_MINUS: ("-", "minus sign"),
    pygame.K_PERIOD: (".", "period"),
    pygame.K_SLASH: ("/", "forward slash"),
    pygame.K_0: ("0", "0"),
    pygame.K_1: ("1", "1"),
    pygame.K_2: ("2", "2"),
    pygame.K_3: ("3", "3"),
    pygame.K_4: ("4", "4"),
    pygame.K_5: ("5", "5"),
    pygame.K_6: ("6", "6"),
    pygame.K_7: ("7", "7"),
    pygame.K_8: ("8", "8"),
    pygame.K_9: ("9", "9"),
    pygame.K_COLON: (":", "colon"),
    pygame.K_SEMICOLON: (";", "semicolon"),
    pygame.K_LESS: ("<", "less-than sign"),
    pygame.K_EQUALS: ("=", "equals sign"),
    pygame.K_GREATER: (">", "greater-than sign"),
    pygame.K_QUESTION: ("?", "question mark"),
    pygame.K_AT: ("@", "at"),
    pygame.K_LEFTBRACKET: ("[", "left bracket"),
    pygame.K_BACKSLASH: ("\\", "backslash"),
    pygame.K_RIGHTBRACKET: ("]", "right bracket"),
    pygame.K_CARET: ("^", "caret"),
    pygame.K_UNDERSCORE: ("_", "underscore"),
    pygame.K_BACKQUOTE: ("`", "grave"),
    pygame.K_a: ("a", "a"),
    pygame.K_b: ("b", "b"),
    pygame.K_c: ("c", "c"),
    pygame.K_d: ("d", "d"),
    pygame.K_e: ("e", "e"),
    pygame.K_f: ("f", "f"),
    pygame.K_g: ("g", "g"),
    pygame.K_h: ("h", "h"),
    pygame.K_i: ("i", "i"),
    pygame.K_j: ("j", "j"),
    pygame.K_k: ("k", "k"),
    pygame.K_l: ("l", "l"),
    pygame.K_m: ("m", "m"),
    pygame.K_n: ("n", "n"),
    pygame.K_o: ("o", "o"),
    pygame.K_p: ("p", "p"),
    pygame.K_q: ("q", "q"),
    pygame.K_r: ("r", "r"),
    pygame.K_s: ("s", "s"),
    pygame.K_t: ("t", "t"),
    pygame.K_u: ("u", "u"),
    pygame.K_v: ("v", "v"),
    pygame.K_w: ("w", "w"),
    pygame.K_x: ("x", "x"),
    pygame.K_y: ("y", "y"),
    pygame.K_z: ("z", "z"),
    pygame.K_DELETE: ("", "delete"),
    pygame.K_KP0: ("", "keypad 0"),
    pygame.K_KP1: ("", "keypad 1"),
    pygame.K_KP2: ("", "keypad 2"),
    pygame.K_KP3: ("", "keypad 3"),
    pygame.K_KP4: ("", "keypad 4"),
    pygame.K_KP5: ("", "keypad 5"),
    pygame.K_KP6: ("", "keypad 6"),
    pygame.K_KP7: ("", "keypad 7"),
    pygame.K_KP8: ("", "keypad 8"),
    pygame.K_KP9: ("", "keypad 9"),
    pygame.K_KP_PERIOD: (".", "keypad period"),
    pygame.K_KP_DIVIDE: ("/", "keypad divide"),
    pygame.K_KP_MULTIPLY: ("*", "keypad multiply"),
    pygame.K_KP_MINUS: ("-", "keypad minus"),
    pygame.K_KP_PLUS: ("+", "keypad plus"),
    pygame.K_KP_ENTER: ("\\r", "keypad enter"),
    pygame.K_KP_EQUALS: ("=", "keypad equals"),
    pygame.K_UP: ("", "up arrow"),
    pygame.K_DOWN: ("", "down arrow"),
    pygame.K_RIGHT: ("", "right arrow"),
    pygame.K_LEFT: ("", "left arrow"),
    pygame.K_INSERT: ("", "insert"),
    pygame.K_HOME: ("", "home"),
    pygame.K_END: ("", "end"),
    pygame.K_PAGEUP: ("", "page up"),
    pygame.K_PAGEDOWN: ("", "page down"),
    pygame.K_F1: ("", "F1"),
    pygame.K_F2: ("", "F2"),
    pygame.K_F3: ("", "F3"),
    pygame.K_F4: ("", "F4"),
    pygame.K_F5: ("", "F5"),
    pygame.K_F6: ("", "F6"),
    pygame.K_F7: ("", "F7"),
    pygame.K_F8: ("", "F8"),
    pygame.K_F9: ("", "F9"),
    pygame.K_F10: ("", "F10"),
    pygame.K_F11: ("", "F11"),
    pygame.K_F12: ("", "F12"),
    pygame.K_F13: ("", "F13"),
    pygame.K_F14: ("", "F14"),
    pygame.K_F15: ("", "F15"),
    pygame.K_NUMLOCK: ("", "numlock"),
    pygame.K_CAPSLOCK: ("", "capslock"),
    pygame.K_SCROLLOCK: ("", "scrollock"),
    pygame.K_RSHIFT: ("", "right shift"),
    pygame.K_LSHIFT: ("", "left shift"),
    pygame.K_RCTRL: ("", "right ctrl"),
    pygame.K_LCTRL: ("", "left ctrl"),
    pygame.K_RALT: ("", "right alt"),
    pygame.K_LALT: ("", "left alt"),
    pygame.K_RMETA: ("", "right meta"),
    pygame.K_LMETA: ("", "left meta"),
    pygame.K_LSUPER: ("", "left windows key"),
    pygame.K_RSUPER: ("", "right windows key"),
    pygame.K_MODE: ("", "mode shift"),
    pygame.K_HELP: ("", "help"),
    pygame.K_PRINT: ("", "print screen"),
    pygame.K_SYSREQ: ("", "sysrq"),
    pygame.K_BREAK: ("", "break"),
    pygame.K_MENU: ("", "menu"),
    pygame.K_POWER: ("", "power"),
    pygame.K_EURO: ("", "euro"),
}

mod_key_to_text = {
    pygame.KMOD_NONE: "",
    pygame.KMOD_LSHIFT: "left shift",
    pygame.KMOD_RSHIFT: "right shift",
    pygame.KMOD_SHIFT: "shift",
    pygame.KMOD_CAPS: "capslock",
    pygame.KMOD_LCTRL: "left ctrl",
    pygame.KMOD_RCTRL: "right ctrl",
    pygame.KMOD_CTRL: "ctrl",
    pygame.KMOD_LALT: "left alt",
    pygame.KMOD_RALT: "right alt",
    pygame.KMOD_ALT: "alt",
    pygame.KMOD_LMETA: "left meta",
    pygame.KMOD_RMETA: "right meta",
    pygame.KMOD_META: "meta",
    pygame.KMOD_NUM: "num",
    pygame.KMOD_MODE: "mode",
}

try:
    from gamelib._dr0id_custom import *
except:
    pass

try:
    from gamelib._gummbum_custom import *
except:
    pass
