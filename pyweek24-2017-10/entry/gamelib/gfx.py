# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'gfx.py' is part of CoDoGuPywk24
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]                    # For instance:
                   |                                  * 1.2.0.1 instead of 1.2-a
                   +-|* 0 for alpha (status)          * 1.2.1.2 instead of 1.2-b2 (beta with some bug fixes)
                     |* 1 for beta (status)           * 1.2.2.3 instead of 1.2-rc (release candidate)
                     |* 2 for release candidate       * 1.2.3.0 instead of 1.2-r (commercial distribution)
                     |* 3 for (public) release        * 1.2.3.5 instead of 1.2-r5 (commercial dist with many bug fixes)


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging
import random

import pygame

from gamelib.util import pygametext
from pyknic.pygame_wrapper.spritesystem import Sprite, DefaultRenderer, Camera

from pyknic.mathematics import Vec2 as Vec, Point2
from gamelib.util.spritesheetlib import SpritesheetLib10, DummyLogger, FileInfo
from gamelib.util.spritesheetlib import Sprite as SpriteSheetSprite
from pyknic.animation import Animation
from gamelib import settings
from pyknic.entity import TypedEntity
from gamelib import level_data

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2017"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 

logger = logging.getLogger(__name__)
logger.debug("importing...")


class SdefGfxAnimation(Animation):

    def __init__(self, action, facing, frames, fps, loop=True, start_index=0):
        Animation.__init__(self, start_index, len(frames), fps)
        # Sprite.__init__(self, frames[0], None)
        self.action = action
        self.facing = facing
        self.frames = frames
        self.do_loop = loop
        if not loop:
            self.event_animation_end.add(self._stop_after_one_loop)

    def _stop_after_one_loop(self, *args):
        self.stop()  # animation will stay on it last frame, user reset to more it to the first frame is desired

    def clone(self):
        return SdefGfxAnimation(self.action, self.facing, self.frames, self.fps, self.do_loop)


class AnimationSprite(Sprite):

    def __init__(self, entity, animations_dict, layer, fps=None):
        img = pygame.Surface((0, 0))
        Sprite.__init__(self, img, entity.position, z_layer=layer)
        self.fps = fps
        self.animations = animations_dict
        entity.event_animation_changed.add(self._change_animation)
        self._current_animation = animations_dict[entity.action][entity.facing]
        self._change_animation(entity)
        self.entity = entity

    def _change_animation(self, ent):
        self._current_animation.stop()
        self._current_animation.event_index_changed.remove(self._change_image)
        self._current_animation.event_animation_end.remove(self._anim_ended)
        cur_idx = self._current_animation.current_index

        self._current_animation = self.animations[ent.action][ent.facing].clone()
        if self.fps is not None:
            self._current_animation.fps = self.fps
        self._current_animation.set_current_index(cur_idx)
        self._current_animation.event_index_changed.add(self._change_image)
        self._current_animation.event_animation_end.add(self._anim_ended)
        self._change_image()
        self._current_animation.start()

    def _change_image(self, *args):
        spr = self._current_animation.frames[self._current_animation.current_index]
        self.set_image(spr.image)
        iw, ih = spr.image.get_size()
        if spr.anchor is not None:
            if isinstance(spr.anchor, list):
                self.anchor = Vec(spr.anchor[0] - iw / 2, spr.anchor[1] - ih / 2)
            else:
                self.anchor = spr.anchor

    def _anim_ended(self, *args):
        self.entity.handle_message(-1, -1, settings.MSG_ANIM_END, self)

    def stop(self):
        self._current_animation.stop()


class Graphics(TypedEntity):

    def __init__(self):
        TypedEntity.__init__(self, settings.KIND_GFX)
        self.cam = Camera(pygame.Rect((0, 0), settings.screen_size))
        self.animations = {}  # {kind : animations}
        self.image_cache = {}  # {ident: surface}
        self.active_anims = []
        self.renderer = DefaultRenderer()
        self.sprites = {}  # {id: spr}

    def clear(self):
        self.sprites.clear()
        self.renderer.clear()
        del self.active_anims[:]

    def set_layer(self, ent, layer):
        self.sprites[ent.id].z_layer = layer

    def handle_message(self, sender, receiver_id, msg_type, extra):
        pass

    def _sort_func(self, e):
        # if e.internal_z == 1:
        #     return e.internal_z, e.position.y
        # return e.internal_z, None
        if isinstance(e, AnimationSprite):
            spr = self.sprites.get(e.entity._id, None)
            if spr is None:
                return 0, 0
            if spr.kind == settings.KIND_ANT:
                return e.z_layer - 1, 0
        elif e.kind == settings.KIND_SOFA:
            return e.z_layer, e.rect.centery
        return e.z_layer, e.rect.bottom

    def draw(self, screen, interpolation_factor=1.0):
        self.renderer.update_z_layers(self._sort_func)
        self.renderer.draw(screen, self.cam)

    def update(self, dt, sim_t):
        Animation.scheduler.update(dt, sim_t)
        pass

    def _add_sprite(self, spr, _id):
        self.renderer.add_sprite(spr)
        self.sprites[_id] = spr

    def load(self):
        # pygame.event.set_allowed(None)
        pygame.event.clear()

        self._show_loading()

        ssl = SpritesheetLib10(DummyLogger())

        # load hero
        frames = ssl.load_spritesheet(FileInfo("./data/gfx/soldier/shooting e0000.bmp.sdef"))
        grouped = frames.get_grouped_by_action_and_facing()
        self.animations.setdefault(settings.KIND_HERO, grouped)
        pygame.event.clear()

        # load tower
        anims = {}
        for i in range(6):
            facing = "e"
            anims.setdefault(i, {}).setdefault(facing, {})
            img = pygame.image.load("./data/gfx/chips/tower{0}.png".format(i)).convert_alpha()
            anims[i][facing] = [SpriteSheetSprite(img, [38, 99], i, {})]
        self.animations.setdefault(settings.KIND_TOWER, anims)
        pygame.event.clear()

        # load images
        for kind, filename in ((settings.KIND_CURSOR, "./data/gfx/cross/cross.png"),
                               (settings.KIND_YARD, 'data/gfx/misc/yard2_color_lensflare_vangogh.jpg'),
                               (settings.KIND_SHRUB1, 'data/gfx/misc/shrub1.png'),
                               (settings.KIND_ROOM, 'data/gfx/misc/room1_colorized_cartoon.jpg'),
                               (settings.KIND_SOFA, 'data/gfx/misc/sofa1_cleaned_cartoon.png'),
                               (settings.KIND_CHAIR1, 'data/gfx/misc/chair1_color_cartoon.png'),
                               (settings.KIND_CHAIR2, 'data/gfx/misc/chair2_color_cartoon.png'),
                               (settings.KIND_SUNNY_BIRD, 'data/gfx/misc/sunny_birds_singing.jpg'),
                               (settings.KIND_CRACK, 'data/gfx/cracks/crack.png'),
                               (settings.KIND_LIGHT, './data/gfx/misc/light.png')):
            self.image_cache[kind] = pygame.image.load(filename).convert_alpha()
        # im = self.image_cache[settings.KIND_CRACK]
        # self.image_cache[settings.KIND_CRACK] = im.convert()
        # im.set_colorkey(im.get_at((0, 0)))
        pygame.event.clear()

        imgs = []
        for filename in (
            "./data/gfx/misc/button.png",
            "./data/gfx/misc/dice.png",
            "./data/gfx/misc/hazelnut.png",
            "./data/gfx/misc/nut.png"):
            imgs.append(pygame.image.load(filename).convert_alpha())
        self.image_cache[settings.KIND_THINGS] = imgs
        pygame.event.clear()

        # load bullet
        anims = {}
        anims.setdefault(0, {})
        img = pygame.image.load("./data/gfx/misc/bullet.png").convert_alpha()
        for i in range(360):
            rot = pygame.transform.rotozoom(img, -i, 1.0).convert_alpha()
            anims[0][i] = [SpriteSheetSprite(rot, "center", 0, {})]
        self.animations.setdefault(settings.KIND_BULLET, anims)
        pygame.event.clear()

        # grenade
        anims = {}
        anims.setdefault(0, {}).setdefault(0, [])
        img = pygame.image.load("./data/gfx/grenade/cartoon_grenade_pin_pulled_small.png").convert_alpha()
        for i in [0.75, 1.0, 1.25, 1.0, 0.75, 0.5]:
            angle = random.randint(-5, 5)
            rot = pygame.transform.rotozoom(img, angle, i).convert_alpha()
            anims[0][0].append(SpriteSheetSprite(rot, "center", 0, {}))

        # explosion animation
        rect = pygame.Rect(0, 0, 128, 128)
        frames = []
        img_sheet = pygame.image.load("./data/gfx/explosion/explosion-4.png")
        for i in range(12):
            i = pygame.Surface((128, 128), pygame.SRCALPHA, 32)
            i.fill((0, 0, 0, 0), None, pygame.BLEND_RGBA_MULT)  # make transparent
            i.blit(img_sheet, (0, 0), rect)
            frames.append(SpriteSheetSprite(i.convert_alpha(), "midbottom", 1, {}))
            rect.move_ip(128, 0)
        anims.setdefault(1, {})[0] = frames

        self.animations.setdefault(settings.KIND_GRENADE, anims)
        self.animations.setdefault(settings.KIND_BLAST, anims)
        pygame.event.clear()

        # ant
        fi_sdef = FileInfo("./data/gfx/antlion/antlion_0.png.sdef")
        sdef = ssl.load_sdef_from_file(fi_sdef)

        fi_sheet = FileInfo("./data/gfx/antlion/antlion_0.png")
        frames = ssl.load_spritesheet_from_sdef(sdef, fi_sheet)
        grouped = frames.get_grouped_by_action_and_facing()
        self.animations.setdefault(settings.KIND_ANT, {"_normal": grouped})
        pygame.event.clear()

        # fi_sheet = FileInfo("./data/gfx/antlion/fire_ant.png")
        # frames = ssl.load_spritesheet_from_sdef(sdef, fi_sheet)
        # grouped = frames.get_grouped_by_action_and_facing()
        # self.animations[settings.KIND_ANT]["_fire"] = grouped
        # pygame.event.clear()
        #
        # fi_sheet = FileInfo("./data/gfx/antlion/ice_ant.png")
        # frames = ssl.load_spritesheet_from_sdef(sdef, fi_sheet)
        # grouped = frames.get_grouped_by_action_and_facing()
        # self.animations[settings.KIND_ANT]["_ice"] = grouped
        # pygame.event.clear()

        for kind in self.animations.keys():
            self.animations[kind] = self._create_animations(kind)
        pygame.event.clear()

        # BUG: the following should allow all events; it does not
        # pygame.event.set_blocked(None)

        # load levels

    def _load_image(self, kind, image_file):
        anims = {}
        anims.setdefault(0, {})
        img = pygame.image.load(image_file).convert_alpha()
        anims[0]["e"] = [SpriteSheetSprite(img, "center", 0, {})]
        self.animations.setdefault(kind, anims)

    def create_light(self, ambient_color, player):
        self.cam.ambient_is_on = True
        self.cam.ambient_color = ambient_color
        img = self.image_cache[settings.KIND_LIGHT]
        spr = Sprite(img, player.position)
        spr.is_light = True
        self._add_sprite(spr, id(spr))

    def create_animations(self, entity, layer, fps):
        kind = entity.kind
        anims = self.animations[kind]
        spr = AnimationSprite(entity, anims, layer, fps)
        spr.kind = kind
        self._add_sprite(spr, entity.id)

        return self.animations[kind]

    def create_sprite(self, kind, _id, position, layer, anchor="center"):
        img = self.image_cache[kind]
        if kind == settings.KIND_THINGS:
            img = random.choice(img)
        spr = Sprite(img, position, anchor, z_layer=layer)
        spr.kind = kind
        self._add_sprite(spr, _id)

    def _create_animations(self, kind):
        facings = {
            "e": 0,
            "se": 1,
            "s": 2,
            "sw": 3,
            "w": 4,
            "nw": 5,
            "n": 6,
            "ne": 7,
        }

        result = {}
        if kind == settings.KIND_ANT:
            modes = self.animations[kind]
            for mode in modes.keys():
                anims = modes[mode]
                for action in anims.keys():
                    action_mode = action + mode
                    result.setdefault(action_mode, {})
                    for facing in anims[action].keys():
                        d = facings[facing.lower()]
                        frames = anims[action][facing]
                        anim = SdefGfxAnimation(action_mode, d, frames, 20)
                        result[action_mode][d] = anim
        else:
            anims = self.animations[kind]
            for action in anims.keys():
                result.setdefault(action, {})
                for facing in anims[action].keys():
                    facing_lower = facing
                    if isinstance(facing, str):
                        facing_lower = facing.lower()
                    d = facings.get(facing_lower, facing)
                    frames = anims[action][facing]
                    anim = SdefGfxAnimation(action, d, frames, 20)
                    result[action][d] = anim

        return result

    # noinspection PyMethodMayBeStatic
    def _show_loading(self):
        img = pygametext.getsurf('Loading...', **settings.font_themes['intro_title'])
        screen = pygame.display.get_surface()
        screen.fill(settings.default_fill_color)
        rect = img.get_rect(center=screen.get_rect().center)
        screen.blit(img, rect)
        pygame.display.flip()

    def remove_by_id(self, _id):
        spr = self.sprites.pop(_id, None)
        if spr is not None:
            self.renderer.remove_sprite(spr)

logger.debug("imported")
