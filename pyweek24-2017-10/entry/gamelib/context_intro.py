# -*- coding: utf-8 -*-
import logging
import pygame
from pyknic.context import Context
from pyknic.pygame_wrapper.context.effects import FadeOutEffect
from pyknic.tweening import Tweener, ease_out_quad
from gamelib import context_mainmenu
from gamelib import settings
from gamelib import resource_sound
from gamelib.util import interpolation
from gamelib.util import pygametext


logger = logging.getLogger(__name__)


class Sprite(object):
    def __init__(self, image, pos, anchor='topleft'):
        self.image = image
        self.rect = image.get_rect()
        self.anchor = anchor

        self._pos = [0, 0]
        self.pos = pos

        self.interpolator = interpolation.Interpolator(*self.rect.topleft)

    def _get_pos(self):
        return self._pos

    def _set_pos(self, val):
        self._pos[:] = val
        setattr(self.rect, self.anchor, val)
    pos = property(_get_pos, _set_pos)

    def update(self, dt):
        self.interpolator.update(*self.pos)


class Title1(Sprite):
    def __init__(self, image, pos, anchor='topleft'):
        super(Title1, self).__init__(image, pos, anchor)
        self._image_orig = self.image
        self._alpha = 0
        self._prev_alpha = 0
        self.mod_alpha()

    def mod_alpha(self):
        self.image = self._image_orig.copy()
        self.image.fill((255, 255, 255, self._alpha), None, pygame.BLEND_RGBA_MULT)
        self._prev_alpha = int(self._alpha)

    def update(self, dt):
        if int(self._alpha) != self._prev_alpha:
            self.mod_alpha()


class Title2(Sprite):
    def __init__(self, image, pos, anchor='topleft'):
        super(Title2, self).__init__(image, pos, anchor)
        self.x = self.rect.x

    def update(self, dt):
        self._pos[0] = self.x
        self.pos = self._pos
        self.interpolator.update(*self.rect.topleft)


class Intro(Context):
    def __init__(self):
        self.sprites = []
        self.tweener = Tweener(logger)
        self.title1 = None

    def enter(self):
        sr = settings.screen_rect
        title1 = pygametext.getsurf('cANT2: The Hidden Threat', **settings.font_themes['intro_title'])
        sprite = Title1(title1, sr.center, 'midbottom')
        self.tweener.create_tween(sprite, '_alpha', 0, 255, 3.5, cb_end=self.title1_ended, ease_function=ease_out_quad)
        self.sprites.append(sprite)
        self.title1 = sprite

        settings.sfx_handler.handle_message(None, None, resource_sound.SFX_DRAMA_BUILD_UP, None)

    def title1_ended(self, *args):
        sr = settings.screen_rect
        title2 = pygametext.getsurf("They're Behind Everything", **settings.font_themes['intro_subtitle'])
        sprite = Title2(title2, (sr.right, sr.centery), 'topleft')
        self.tweener.create_tween(sprite, 'x', sr.right, -(sr.w - self.title1.rect.left) + 230, 0.22)
        self.sprites.append(sprite)

        settings.sfx_handler.handle_message(None, None, resource_sound.SFX_DRAMA_WOOSH_INTRO, None)

    def update(self, delta_time, sim_time):
        self._handle_events()
        self.tweener.update(delta_time)
        for s in self.sprites:
            s.update(delta_time)

    def draw(self, screen, do_flip=False, interpolation_factor=1.0):
        screen.fill(settings.default_fill_color)
        for s in self.sprites:
            screen.blit(s.image, s.interpolator.from_start(interpolation_factor))
        if do_flip:
            pygame.display.flip()

    def win_level(self):
        self.exchange(context_mainmenu.MainMenu(), effect=FadeOutEffect(2.0, fade_in=False))
        settings.channel_drama.fadeout(2000)

    def _handle_events(self):
        actions, extras = settings.intro_event_map.get_actions(pygame.event.get())
        for action, extra in actions:
            if action == settings.ACTION_WIN_LEVEL:
                self.win_level()
                return
            elif action == settings.ACTION_QUIT:
                self.pop()
