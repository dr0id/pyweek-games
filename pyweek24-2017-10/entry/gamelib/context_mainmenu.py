# -*- coding: utf-8 -*-
from __future__ import print_function
import logging
import random

import pygame
from pygame.locals import Color

import pyknic
from pyknic.pygame_wrapper.context.effects import DirectionalSweep, ExplosionEffect, FadeOutEffect
from pyknic.timing import Timer
# from pyknic.tweening import Tweener
from pyknic.pygame_wrapper.resource import ImageLoader, SongLoader

from gamelib import settings
from gamelib.context_gameplay import Gameplay
from gamelib.context_credits import Credits
from gamelib import resource_sound
from gamelib.util import pygametext


logger = logging.getLogger(__name__)

image_loader = ImageLoader()
song_loader = SongLoader()


class ChapterData(object):
    def __init__(self, chapter_num):
        self.chapter_num = chapter_num


class TextData(object):
    def __init__(self, text):
        self.text = text

    def __str__(self):
        return '<TextData({})>'.format(self.text)


class ContextData(object):
    def __init__(self, context_class, *args, **kwargs):
        self.context_class = context_class
        self.args = list(args)
        self.kwargs = kwargs

    def make_context(self):
        return self.context_class(*self.args, **self.kwargs)

    def make_custom(self, *args, **kwargs):
        return self.context_class(*args, **kwargs)

    def __str__(self):
        return '<ContextData({})>'.format(self.context_class.__name__)


class ImageResourceData(object):
    def __init__(self, filename, *args, **kwargs):
        """*args and **kwargs are passed to ImageLoader.load(); see also SpriteFromImageResource

        If 'scale' is in kwargs then the resulting sprite's image will be scaled: example scale=0.5.
        """
        self.filename = filename
        self.args = list(args)
        self.kwargs = kwargs
        self.scale = kwargs.get('scale', 1.0)
        self.colorkey = kwargs.pop('colorkey', None)

    def __str__(self):
        return '<ImageResourceData({})>'.format(self.filename)


class SoundResourceData(object):
    def __init__(self, ident, delay=0.0):
        self.ident = ident
        self.delay = delay

    def __str__(self):
        return '<SoundResourceData({})>'.format(self.ident)


class SoundMunchData(object):
    def __str__(self):
        return '<SoundMunchData()>'

    def queue_another(self):
        ident = random.choice(resource_sound.munches)
        settings.sfx_handler.handle_message(None, None, ident, 'queue')


class SongResourceData(object):
    def __init__(self, ident, fadeout=0):
        self.ident = ident
        self.fadeout = fadeout

    def __str__(self):
        return '<SongResourceData({})>'.format(self.ident)


class Sprite(object):
    def __init__(self, image, rect):
        self.image = image
        self.rect = rect

    def __str__(self):
        return '<Sprite({}x{}) @ {}>'.format(self.rect.w, self.rect.h, self.rect.topleft)


class SpriteFromImageResource(Sprite):
    def __init__(self, filename, pos, *args, **kwargs):
        """make a resource-loaded sprite

        filename is the relative path to the resource file.

        pos is a dict containing rect {attr: value, ...}.

        *args and **kwargs are passed to ImageLoader.load().
        """
        image = image_loader.load(filename, *args, **kwargs)
        rect = image.get_rect(**pos)
        super(SpriteFromImageResource, self).__init__(image, rect)

        # if needed for debugging
        self.init_args = filename, pos, args, kwargs

    def __str__(self):
        return '<SpriteFromImageResource({}x{}) @ {} {}>'.format(
            self.rect.w, self.rect.h, self.rect.topleft, self.init_args[0])


# Sentinels
INIT = 'control', 1 << 0            # initial state set in __init__()
END_PAGE = 'control', 1 << 1        # use NEW_PAGE for multiple pages in a chapter

SCREENS = [
    ChapterData(1),
    TextData("CHAPTER 1"),
    TextData("The queen and her colony are gone. Invasion stopped, lives saved, order restored. The rest is, as my neighbors used to say, history. Just like them yella-belly chicken-liver pretend-neighbors are history, a dwindling dust cloud on the horizon. Adios, amigos. Ah well... At least they won't be talking with their mouths full! Har-har."),
    ImageResourceData('data/gfx/chips/tower1.png'),
    TextData("It's sunny. Listen, song birds. Maybe I'll trim the hedges tomorrow."),
    ImageResourceData('data/gfx/misc/sunny_birds_singing.jpg', scale=0.7),
    SongResourceData(resource_sound.SONG_WILLOW_WARBLER_BIRD_CALL, fadeout=30000),
    END_PAGE,
    ContextData(Gameplay, level=1),

    ChapterData(2),
    TextData("CHAPTER 2"),
    TextData("That sound ..."),
    TextData("It can't be ... !"),
    TextData("It's ... MY   C H I I I I I I I P S !!!!!"),
    ImageResourceData('data/gfx/chips/tower2.png'),
    TextData("Urgh. My ankle itches like mad. Oh, I feel sick and dizzy.... why is the room getting bigger?"),
    TextData("Huff. No time for self-pity. I must foil this plot."),
    SoundMunchData(),
    END_PAGE,
    ContextData(Gameplay, level=2, start_music_carousel=True),

    ChapterData(3),
    TextData("CHAPTER 3"),
    TextData("Ach. Did I just see that? Them bugs got smart and disappeared my chips behind the sofa!"),
    ImageResourceData('data/gfx/misc/sofa1_cleaned_cartoon.png', scale=0.6),
    TextData("Well, as my grandpappy used say. When the critters is smarter than you, there's one thing left to do. Show 'em who's boss. Mano a mandible, as it were."),
    TextData("Hey, look. My old change purse. Lemme strap this on my back, and I'm ready for a chip raid."),
    END_PAGE,
    ContextData(Gameplay, level=3),

    ChapterData(4),
    TextData("CHAPTER 4"),
    TextData("Them bugs are smart AND tough!"),
    SoundResourceData(resource_sound.SFX_EARTHQUAKE, delay=4.0),
    TextData("That sound. I'd know that rumble anywhere."),
    # ImageResourceData('data/gfx/cracks/crack.png', scale=0.6, colorkey=pygame.Color('red')),
    ImageResourceData('data/gfx/cracks/crack.png', scale=0.6),
    TextData("It came from behind the chair. In fact, it seems like..."),
    TextData("They're behind everything!"),
    ImageResourceData('data/gfx/misc/chair2_color_cartoon.png', scale=0.6),
    TextData("No more delays. We're at the Chair of the Abyss."),
    END_PAGE,
    ContextData(Gameplay, level=4),

    ChapterData(5),
    TextData("CHAPTER 5"),
    TextData("Normally I'd hit the library...",),
    TextData("But my wee legs would give out before finishing a page... every pregnant elipsis... draggin' it out... Yeah, I know. Any excuse will do."),
    TextData("I'm more of an exclamation point kinda guy! YEAH!!"),
    TextData("Let's get around to it, you, BBBUGS. My varmint-kickin' boots are just your size: double-ought twelve. That's one thousanths scale for you propeller hat wearing types."),
    TextData("No more delays. We're at the Wall of the Abyss."),
    END_PAGE,
    ChapterData(5),
    TextData("CHAPTER 5"),
    TextData("Shh-hh. Up ahead. I see a golden glow."),
    TextData("I hear skittering. It almost sounds like a whispering babble. Like... they're talkin' about... chips!"),
    TextData("Time to disrupt the repose! Har-har."),
    END_PAGE,
    ContextData(Gameplay, level=5),

    # ChapterData(6),
    # TextData("CHAPTER 6"),
    # ContextData(Gameplay, level=5),

    # TODO: WinGame or Outro?
    # ContextData(WinGame),

    ChapterData(99),
    ContextData(Credits),
]


class MainMenu(pyknic.context.Context):
    def __init__(self, *args, **kwargs):
        self.level = kwargs.get('level')
        logger.error('MainMenu level {}', self.level)

        # self.tweener = Tweener()
        # self.scheduler = pyknic.timing.Scheduler()

        self.sprites = []
        self.pending = INIT

        self.background = Color('brown')
        self.keys_image = None
        self.keys_rect = None

        self.chapter_num = None
        self.song_fadeout = 0

        self.annoying_munching_forever = None
        self.schedule_ids = []

        # Init page, generate the sprites.
        self.initialized = False

    def init(self):
        def delayed_sound(*args):
            settings.sfx_handler.handle_message(*args)
            return 0.0

        if self.level is not None:
            while SCREENS:
                if isinstance(SCREENS[0], ChapterData) and SCREENS[0].chapter_num == self.level:
                    break
                SCREENS.pop(0)

        # Load the top page.
        x = 32
        y = 0
        text_max_width = settings.screen_width * 5 / 8
        xpad = 64
        while SCREENS:
            item = SCREENS.pop(0)
            if isinstance(item, ChapterData):
                logger.debug('new chapter={}', item.chapter_num)
                self.chapter_num = item.chapter_num
            elif isinstance(item, TextData):
                logger.debug('new text={}', item.text)
                y += 32
                image = pygametext.getsurf(
                    item.text, align='left', width=text_max_width, **settings.font_themes['mainmenu'])
                rect = image.get_rect(x=x, y=y)
                sprite = Sprite(image, rect)
                self.sprites.append(sprite)
                y += sprite.image.get_height()
            elif isinstance(item, ImageResourceData):
                logger.debug('new image={}', item.filename)
                text_rect = self.sprites[-1].rect
                pos = dict(x=text_max_width + xpad, centery=text_rect.centery)
                sprite = SpriteFromImageResource(item.filename, pos, *item.args, **item.kwargs)
                # Note about the following. Probably better to scale the image down.
                # if text_rect.h < sprite.rect.h:
                #     y = y - text_rect.h + sprite.rect.h
                if item.scale != 1.0:
                    sprite.image = pygame.transform.rotozoom(sprite.image, 0, item.scale)
                    sprite.rect = sprite.image.get_rect(x=text_max_width + xpad, centery=text_rect.centery)
                if item.colorkey:
                    sprite.image = sprite.image.convert()
                    sprite.image.set_colorkey(item.colorkey)
                self.sprites.append(sprite)
            elif isinstance(item, ContextData):
                logger.debug('new context={}', item.context_class)
                self.load_context(item)
                break
            elif isinstance(item, SongResourceData):
                logger.debug('new song={}', item.ident)
                settings.sfx_handler.handle_message(None, None, item.ident, None)
                self.song_fadeout = item.fadeout
            elif isinstance(item, SoundResourceData):
                logger.debug('new sound={}', item.ident)
                self.schedule_ids.append(
                    Timer.scheduler.schedule(delayed_sound, item.delay, cb_args=(None, None, item.ident, None)))
            elif isinstance(item, SoundMunchData):
                self.annoying_munching_forever = item
                self.annoying_munching_forever.queue_another()
            elif item == END_PAGE:
                logger.debug('new pending={}', item)
                self.pending = item
                break

        # Create the prompt.
        keys = []
        mouses = []
        for type_num, events in settings.menu_event_map._event_to_action_map.items():
            logger.debug('type_name={} events={}'.format(pygame.event.event_name(type_num), events))
            for event_value, action in events.items():
                if settings.ACTION_WIN_LEVEL == action:
                    if type_num == pygame.KEYUP:
                        key_num, mod_num = event_value
                        keys.append(settings.key_to_text[key_num][1].upper())
                    elif type_num == pygame.MOUSEBUTTONUP:
                        mouses.append(('', 'LMB', 'MMB', 'RMB')[event_value])
        keys.sort()
        if {'LMB', 'MMB', 'RMB'} == set(mouses):
            keys.append('MOUSE')
        else:
            keys.extend(sorted(mouses))
        key_text = '[{}]'.format(', '.join(keys))
        image = pygametext.getsurf(key_text, **settings.font_themes['mainmenu'])
        rect = image.get_rect(bottomright=settings.screen_size)
        self.sprites.append(Sprite(image, rect))

        self.initialized = True

    def enter(self):
        pygame.event.clear()

    def exit(self):
        for ident in self.schedule_ids:
            Timer.scheduler.remove(ident)

    def suspend(self):
        pass

    def resume(self):
        pygame.event.clear()

    def update(self, delta_time, sim_time):
        """
        Update the game.
        :param delta_time: time passed in last time step (game time)
        :return:
        """
        if not self.initialized:
            self.init()
        self._handle_events()
        # self.tweener.update(delta_time)
        # self.scheduler.update(delta_time, sim_time)
        if self.annoying_munching_forever:
            self.annoying_munching_forever.queue_another()
        Timer.scheduler.update(delta_time, sim_time)
        # logger.info('>>> Timer.scheduler {}', Timer.scheduler._heap)

    def draw(self, screen, do_flip=False, interpolation_factor=1.0):
        screen.fill(self.background)
        for s in self.sprites:
            screen.blit(s.image, s.rect)
            # pygame.draw.rect(screen, Color('white'), s.rect, 1)

        if do_flip:
            pygame.display.flip()

    def load_context(self, data):
        pygame.event.clear()

        if SCREENS and isinstance(SCREENS[0], ChapterData):
            if SCREENS[0].chapter_num == 99:
                logger.info('loading Credits {}', self.chapter_num)
                data.kwargs.update(**dict(returning_context=Credits, returning_effect=ExplosionEffect))
            else:
                logger.info('loading Gameplay {}', self.chapter_num)
                data.kwargs.update(**dict(returning_context=MainMenu, returning_effect=FadeOutEffect))
        else:
            logger.error('lookahead failed: expecting ChapterData')
            logger.info('got: {}', SCREENS[0] if SCREENS else [])
            logger.info('using default: return to mainmenu')
            logger.info('loading Gameplay {}', self.chapter_num)
            data.kwargs.update(**dict(returning_context=MainMenu, returning_effect=FadeOutEffect))

        if data.context_class is Gameplay:
            context = data.make_context()
            self.exchange(context, DirectionalSweep(settings.screen_size))
        else:
            raise pygame.error('unknown data.context_class "{}"'.format(data.context_class))

        if self.song_fadeout > 0:
            pygame.mixer.music.fadeout(self.song_fadeout)

    def _handle_events(self):
        actions, extras = settings.menu_event_map.get_actions(pygame.event.get())
        if settings.debug_event_handlers and (actions or extras):
            logger.debug('actions={} extras={}'.format(actions, extras))
        for action, extra in actions:
            if action == settings.ACTION_QUIT:
                self.pop()
            elif action == settings.ACTION_WIN_LEVEL:
                if self.pending == END_PAGE:
                    if SCREENS and isinstance(SCREENS[0], ContextData):
                        ctx = SCREENS.pop(0)
                        start_music = ctx.kwargs.get('start_music_carousel', None)
                        if start_music:
                            settings.sfx_handler.fill_music_carousel(
                                [resource_sound.SONG_UNDAUNTED, resource_sound.SONG_URBAN_GAUNTLET])
                            settings.sfx_handler.start_music_carousel()
                        self.load_context(ctx)
                        return
                    elif SCREENS and isinstance(SCREENS[0], ChapterData):
                        pygame.event.clear()
                        self.exchange(MainMenu(), FadeOutEffect(1.0))
                        return
