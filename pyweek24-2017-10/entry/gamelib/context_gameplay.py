# -*- coding: utf-8 -*-

from __future__ import print_function
import logging
import random

import pygame
from pygame.locals import Color

from pyknic.mathematics import Point2 as Point
from pyknic.timing import Scheduler, Timer
from gamelib.collisionsystem import CollisionHandlerLookup
from pyknic.messaging import MessageDispatcher
from pyknic.registry import Registry
from gamelib.world import World
import pyknic
from pyknic.mathematics import Vec2
from pyknic.pygame_wrapper.context.effects import FadeOutEffect, ExplosionEffect, DirectionalSweep
from pyknic.pygame_wrapper.resource import ImageLoader, SongLoader
from gamelib import settings
from gamelib.util import pygametext
from gamelib.util import spritefx
from gamelib.util import hudlight, hudthemes

logger = logging.getLogger(__name__)

image_loader = ImageLoader()
song_loader = SongLoader()


class Gameplay(pyknic.context.Context):
    def __init__(self, *args, **kwargs):

        self.level = kwargs.get('level', 2)
        if self.level is None:
            logger.error('level not specified')
            raise pygame.error('level not specified')

        self.debug_render = False
        registry = Registry()
        self.world = World(self.level, registry, CollisionHandlerLookup(), MessageDispatcher(registry), Scheduler(),
                           settings.gfx_handler, settings.sfx_handler)
        self.player = None
        self.cursor = None
        self.returning_context = kwargs.get('returning_context', None)
        self.returning_effect = kwargs.get('returning_effect', None)
        self.image = pygametext.getsurf('Context: Gameplay', **settings.font_themes['intro_title'])
        self.rect = self.image.get_rect(center=settings.screen_rect.center)

        self.hud = None

    def enter(self):
        pygame.event.clear()

        self.world.load_level(self.level)

        # if self.player is None:
        #     self.player = self.world.create_player()
        self.player = self.world.player
        if self.cursor is None:
            self.cursor = self.world.create_cursor()

        if self.level >= 3:
            self.hud = hudlight.HUD(**hudthemes.HUD_THEMES['boogaloo'])
            self.hud.add('label1', 'Chipometer')
            self.hud.add('health', 'Chips {:0.0f}', self.player.health)
            self.hud.add('towers', 'Towers {}', '')

        if self.level > 1000:
            self.world.create_ant(Point(-100, 100), 20)
            self.world.create_ant(Point(200, 250), 20)
            self.world.create_ant(Point(300, 50), 20)
            self.world.create_ant(Point(-200, 40), 20)
            for i in range(100):
                self.world.create_ant(Point(random.randint(-200, 200), random.randint(-200, 200)), 20)

        pygame.mouse.set_visible(False)

    def exit(self):
        pygame.mouse.set_visible(True)
        spritefx.clear()
        pygame.display.set_caption(settings.title)
        Timer.scheduler.clear()

    def suspend(self):
        pass

    def resume(self):
        pygame.event.clear()

    def won_level(self):
        self.load_context(self.returning_context, self.returning_effect)

    def lost_level(self):
        ctx = Gameplay(level=self.level, returning_context=self.returning_context, returning_effect=self.returning_effect)
        # self.exchange(ctx, FadeOutEffect(2))
        self.exchange(ctx, DirectionalSweep(settings.screen_size))

    def quit_level(self):
        self.load_context(self.returning_context, self.returning_effect)

    def update(self, delta_time, sim_time):
        self._handle_events()
        self.world.update(delta_time, sim_time)
        spritefx.update_tweens(delta_time)
        if self.hud:
            try:
                chip_watcher = self.world.registry.get_by_kind(settings.KIND_LEVEL_CONTROL)[0]
                self.hud.update_item('health', self.player.health)
                self.hud.update_item('towers', chip_watcher._towers_report)
            except:
                pass

        world_status = self.world.level_status
        if world_status == settings.LEVEL_WON:
            self.won_level()
        elif world_status == settings.LEVEL_LOST:
            self.lost_level()
        elif world_status == settings.LEVEL_QUIT:
            self.quit_level()

    def draw(self, screen, do_flip=False, interpolation_factor=1.0):
        screen.fill(Color('darkgreen'))
        # screen.blit(self.image, self.rect)

        for s in self.world.cracks:
            s.draw(screen)

        self.world.gfx.draw(screen, interpolation_factor)

        if self.debug_render:

            for ent in self.world.registry.get_all():
                self._draw_boundng_radius(screen, ent)

            self.draw_vector(screen, self.player.position, self.player.direction, (0, 255, 0), 10)
            self.draw_vector(screen, self.player.position, self.player.v, (0, 0, 255), 1)

            for ant in self.world.registry.get_by_kind(settings.KIND_ANT):
                self.draw_vector(screen, ant.position, ant.direction, (0, 255, 0), 10)
                self.draw_vector(screen, ant.position, ant.v, (0, 0, 255), 1)

            for w in self.world.registry.get_by_kind(settings.KIND_WALL):
                a = w.position
                b = w.end
                a1 = a - Vec2(0, w.height)
                b1 = b - Vec2(0, w.height)
                self._draw_line(screen, a, b, (255, 255, 0))
                self._draw_line(screen, a1, b1, (255, 255, 0))
                self._draw_line(screen, a, a1, (255, 255, 0))
                self._draw_line(screen, b, b1, (255, 255, 0))

        for s in spritefx.texts:
            screen.blit(s.image, s.rect)

        if self.hud:
            self.hud.draw(screen)

        if do_flip:
            pygame.display.flip()

    def _draw_boundng_radius(self, screen, entity, color=(255, 0, 0)):
        if entity.kind == settings.KIND_SHRUB1:
            pos = Vec2(*self.world.gfx.cam.world_to_screen(entity.position))
            pos.y -= entity.radius
        else:
            pos = Vec2(*self.world.gfx.cam.world_to_screen(entity.position))
        pos = pos.as_tuple(int)
        radius = entity.radius if entity.kind != settings.KIND_GRENADE else entity.blast_radius_sq**0.5
        radius = radius if radius > 2 else 2
        pygame.draw.circle(screen, color, pos, int(radius), 2)
        pygame.draw.circle(screen, color, pos, int(2))

    def _draw_line(self, screen, a, b, color):
        a = self.world.gfx.cam.world_to_screen(a)
        b = self.world.gfx.cam.world_to_screen(b)
        pygame.draw.line(screen, color, a.as_tuple(int), b.as_tuple(int))

    def draw_vector(self, screen, pos, v, color, factor):
        start = self.world.gfx.cam.world_to_screen(pos).as_tuple(int)
        end = self.world.gfx.cam.world_to_screen(pos + v * factor).as_tuple(int)
        pygame.draw.line(screen, color, start, end)

    def load_context(self, context, effect=None):
        if effect is FadeOutEffect:
            self.exchange(context(), effect=FadeOutEffect(1.0))
        elif effect is ExplosionEffect:
            self.exchange(context(), effect=ExplosionEffect(settings.screen_rect))
        else:
            # Probably a dev-debug config...
            self.pop()
            if context:
                self.push(context())

        # pygame.mixer.music.fadeout(1000)

    def _handle_events(self):
        actions, extras = settings.gameplay_event_map.get_actions(pygame.event.get())
        if settings.debug_event_handlers and (actions or extras):
            logger.debug('actions={} extras={}'.format(actions, extras))

        for action, extra in actions:
            if action == settings.ACTION_MOUSE_MOTION:
                # convert to world coordinates
                _pos, _rel, _buttons = extra
                _world_pos = self.world.gfx.cam.screen_to_world(Point(_pos[0], _pos[1]))
                extra = _world_pos, _rel, _buttons

            if action in (settings.ACTION_QUIT, settings.ACTION_WIN_LEVEL):
                if self.returning_context:
                    self.load_context(self.returning_context, self.returning_effect)
                else:
                    self.pop()

            self.player.handle_action(action, extra)
            self.cursor.handle_action(action, extra)

            if settings.ACTION_TOGGLE_DEBUG_RENDER == action:
                self.debug_render = not self.debug_render
            elif settings.ACTION_EAT_CHIPS == action:
                self.world.registry.get_by_kind(settings.KIND_TOWER)[0].action += 1

