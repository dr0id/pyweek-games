# -*- coding: utf-8 -*-
import logging
from pyknic.generators import IdGenerator
from gamelib import settings
from gamelib.sfx import MusicData, SoundData


logger = logging.getLogger(__name__)

sound_loader = None
song_loader = None

_gen_id = IdGenerator(1).next

SFX_DRAMA_BUILD_UP = _gen_id()
SFX_DRAMA_WOOSH_INTRO = _gen_id()

SFX_EARTHQUAKE = _gen_id()

SFX_GUN_FIRE_MACHINE_GUN_BURST_1 = _gen_id()
SFX_GUN_FIRE_MACHINE_GUN_BURST_2 = _gen_id()

SFX_MUNCH_1 = _gen_id()
SFX_MUNCH_2 = _gen_id()
SFX_MUNCH_3 = _gen_id()
SFX_MUNCH_4 = _gen_id()
SFX_MUNCH_5 = _gen_id()
SFX_MUNCH_6 = _gen_id()

SFX_GRENADE_TOSS = _gen_id()
SFX_GRENADE_EXPLODE = _gen_id()

SONG_WILLOW_WARBLER_BIRD_CALL = _gen_id()
SONG_UNDAUNTED = _gen_id()
SONG_URBAN_GAUNTLET = _gen_id()

munches = (
    SFX_MUNCH_1,
    SFX_MUNCH_2,
    SFX_MUNCH_3,
    SFX_MUNCH_4,
    SFX_MUNCH_5,
    SFX_MUNCH_6,
)

sfx_data = {
    SFX_DRAMA_BUILD_UP: SoundData('Build up 01.ogg', 1.0, reserved_channel_id=settings.channel_drama),
    SFX_DRAMA_WOOSH_INTRO: SoundData('PA Woosh 27.ogg', 1.0, reserved_channel_id=None),

    SFX_EARTHQUAKE: SoundData('earthquake.ogg', 1.0, reserved_channel_id=settings.channel_drama),

    SFX_GUN_FIRE_MACHINE_GUN_BURST_1: SoundData('Machine_Gun_Burst_1.ogg', 1.0, reserved_channel_id=None),
    SFX_GUN_FIRE_MACHINE_GUN_BURST_2: SoundData('Machine_Gun_Burst_2.ogg', 1.0, reserved_channel_id=None),

    SFX_MUNCH_1: SoundData('munch1.ogg', 1.0, reserved_channel_id=settings.channel_ants_speaking),
    SFX_MUNCH_2: SoundData('munch2.ogg', 1.0, reserved_channel_id=settings.channel_ants_speaking),
    SFX_MUNCH_3: SoundData('munch3.ogg', 1.0, reserved_channel_id=settings.channel_ants_speaking),
    SFX_MUNCH_4: SoundData('munch4.ogg', 1.0, reserved_channel_id=settings.channel_ants_speaking),
    SFX_MUNCH_5: SoundData('munch5.ogg', 1.0, reserved_channel_id=settings.channel_ants_speaking),
    SFX_MUNCH_6: SoundData('munch6.ogg', 1.0, reserved_channel_id=settings.channel_ants_speaking),

    SFX_GRENADE_TOSS: SoundData('forcepulse.ogg', 1.0, reserved_channel_id=settings.channel_explosions),
    SFX_GRENADE_EXPLODE: SoundData('Explosion_Fast.ogg', 1.0, reserved_channel_id=settings.channel_explosions),

    SONG_WILLOW_WARBLER_BIRD_CALL: MusicData('Willow Warbler Bird call Bird song.ogg', 1.0),
    SONG_UNDAUNTED: MusicData('Undaunted.ogg', 0.3),
    SONG_URBAN_GAUNTLET: MusicData('Urban Gauntlet.ogg', 0.3),
}
