# -*- coding: utf-8 -*-

import logging

from gamelib import settings
from gamelib.util import spritefx


# z_layer < 1: background, ground items: always rendered under
# z_layer = 1: interactive items: rendered in sort order
# z_layer > 1: overhead items: always rendered on top
from gamelib.entities import GameBaseEntity
from pyknic import tweening
from pyknic.entity import BaseEntity
from pyknic.mathematics import Point2, Vec2
from pyknic.timing import Timer
from pyknic.tweening import Tweener
from gamelib.util import spritefx


logger = logging.getLogger(__name__)


class Object(object):
    """a misc object without an image"""
    kind = None
    z_layer = 0
    anchor = 'center'

    def __init__(self, pos, *args, **kwargs):
        self.pos = pos
        self.args = args
        self.kwargs = kwargs


class Yard(Object):
    kind = settings.KIND_YARD
    z_layer = -1


class Room(Object):
    kind = settings.KIND_ROOM
    z_layer = -1


class Shrub(Object):
    kind = settings.KIND_SHRUB1
    z_layer = 1
    anchor = "midbottom"  # from center image


class Sofa(Object):
    kind = settings.KIND_SOFA
    z_layer = 1


class Chair2(Object):
    kind = settings.KIND_CHAIR2
    z_layer = 1


class Chair1(Object):
    kind = settings.KIND_CHAIR1
    z_layer = 1
    anchor = "midbottom"  # from center image


class Hero(Object):
    kind = settings.KIND_HERO
    z_layer = 1


class Tower(Object):
    kind = settings.KIND_TOWER
    z_layer = 1


class Wall(Object):
    kind = settings.KIND_WALL

    def __init__(self, start, end, height):
        Object.__init__(self, start)
        self.end = end
        self.height = height


class Thing(Object):
    kind = settings.KIND_THINGS
    radius = 86
    anchor = Vec2(0, 13)
    z_layer = 1


class Light(Object):
    kind = settings.KIND_LIGHT

    def __init__(self, ambient_color):
        Object.__init__(self, (0, 0))
        self.ambient_color = ambient_color


class AntSpigot(Object):
    kind = settings.KIND_ANT_SPIGOT

    def __init__(self, pos, frequency=1.0, per_pulse=1, count=0):
        """make ants over time

        :param pos: x, y
        :param frequency: pulse every N seconds
        :param per_pulse: release N ants every pulse
        :param count: release N total (0=unlimited)
        """
        super(AntSpigot, self).__init__(pos)
        self.frequency = frequency
        self.per_pulse = per_pulse
        self.count = count


class DelayedText(Object):
    kind = settings.KIND_GAME_TEXT

    def __init__(self, text, delay, duration):
        Object.__init__(self, (0, 0))
        self.text = text
        self.delay = delay if delay > 0.0 else 0.00001
        self.duration = duration
        self.first_time = True
        self.sprite = None

    def is_alive(self):
        if self.sprite is None:
            return True
        return self.sprite.alive

    def make_sprite(self, *args, **kwargs):
        # Timer calls immediately, so use that call to repeat, i.e. "delay" sprite creation
        if self.first_time:
            self.first_time = False
            return self.delay
        x, y = settings.screen_rect.center
        y *= 0.6
        s = spritefx.TextSprite(self.text, (x, y), expire=self.duration, anchor='center', theme_name='gameplay_prompt')
        s.fade_effect(end_alpha=32, duration=self.duration, tween_function=tweening.ease_in_quint)
        self.sprite = s
        return 0.0


class ChipsWatcherLevel2(BaseEntity):
    def __init__(self, *args, **kwargs):
        BaseEntity.__init__(self, settings.KIND_LEVEL_CONTROL, Point2(0, 0))
        self.world = None
        self.tweener = Tweener()
        self.moving = False
        self.state = settings.TOWER_WATCHER_SPAWNED

    def update(self, dt, sim_t, *args):
        if self.state == settings.TOWER_WATCHER_LOST:
            return

        towers = self.world.registry.get_by_kind(settings.KIND_TOWER)
        factor = 0.5
        weak_towers = [_t for _t in towers if _t.health < settings.TowerTemplate.health * factor]
        self.tweener.update(dt)

        destroyed_towers = [_t for _t in towers if _t.destroyed]
        if destroyed_towers:
            self.state = settings.TOWER_WATCHER_LOST
            self.world.level_status = settings.LEVEL_LOSS_PENDING

            del self.world.screen_messages[:]
            self.world.screen_messages.append(DelayedText("You Bombed Your Chips to Smithereens!", 0, 2.5))
            self.world.screen_messages.append(DelayedText("Try Again", 0.3, 2))
            Timer.scheduler.schedule(self.world.end_level, 5.0)
            return

        if weak_towers:
            if not self.moving:
                self.moving = True
                chips = weak_towers[0]
                # 830, 500  end position
                dx = 830 - w2 - chips.position.x
                dy = 500 - h2 - chips.position.y
                duration = 4.0
                func = tweening.ease_in_out_expo
                self.world.gfx.set_layer(chips, 0.5)
                self.tweener.create_tween(chips.position, 'x', chips.position.x, dx, duration, func)
                self.tweener.create_tween(chips.position, 'y', chips.position.y, dy, duration, func)

                for spigot in self.world.registry.get_by_kind(settings.KIND_ANT_SPIGOT):
                    spigot.count = 0

                for ant in self.world.registry.get_by_kind(settings.KIND_ANT):
                    ant.handle_message(self.id, ant.id, settings.MSG_RETREAT, self.world)

                self.state = settings.TOWER_WATCHER_WON
                self.world.level_status = settings.LEVEL_WIN_PENDING
                Timer.scheduler.schedule(self.world.end_level, 5.0)
            else:
                if len([_a for _a in self.world.registry.get_by_kind(settings.KIND_ANT) if _a.health > 0]) == 0:
                    self.world.level_status = settings.LEVEL_WON


class ChipsWatcherLevel3(BaseEntity):
    def __init__(self, towers_needed=2):
        BaseEntity.__init__(self, settings.KIND_LEVEL_CONTROL, Point2(0, 0))
        self.world = None
        self.tweener = Tweener()
        self.moving = False
        self.state = settings.TOWER_WATCHER_SPAWNED

        self._hero_bite_interval = 1.0
        self._hero_bite_elapsed = 0

        self._towers_needed = towers_needed
        self._towers_taken = set()
        self._towers_lost = set()
        self._towers_report = ''

    def _won(self):
        self.state = settings.TOWER_WATCHER_WON
        self.world.level_status = settings.LEVEL_WIN_PENDING
        Timer.scheduler.schedule(self.world.end_level, 5.0)

    def _lost(self):
        self.state = settings.TOWER_WATCHER_LOST
        self.world.level_status = settings.LEVEL_LOSS_PENDING
        Timer.scheduler.schedule(self.world.end_level, 5.0)

    def update(self, dt, sim_t, *args):
        towers = self.world.registry.get_by_kind(settings.KIND_TOWER)
        destroyed_towers = [_t for _t in towers if _t.destroyed]

        for chips in destroyed_towers:
            if chips not in self._towers_lost:
                self.world.screen_messages.append(DelayedText("Don't Bomb Your Chips!", 0.3, 2))
                self._towers_lost.add(chips)
                self._towers_report += '-'

        for chips in towers:
            if chips.health <= 0 and chips not in self._towers_taken and chips not in self._towers_lost:
                self.world.screen_messages.append(DelayedText("Argh! They Got One!", 0.3, 2))
                self._towers_lost.add(chips)
                self._towers_report += '-'

        if self.state == settings.TOWER_WATCHER_SPAWNED:
            self.world.coll_handlers.register(settings.KIND_HERO, settings.KIND_TOWER, self._coll_hero_tower)
            self.state = settings.TOWER_WATCHER_RACING_ANTS

        elif self.state == settings.TOWER_WATCHER_RACING_ANTS:
            if len(self._towers_taken) == self._towers_needed:
                logger.info('WON: objective met')
                self.world.screen_messages.append(DelayedText('You Saved the Chips!', 0, 2))
                if self.world.level_num < 5:
                    self.world.screen_messages.append(DelayedText('Next Challenge', 0.5, 2))
                else:
                    self.world.screen_messages.append(DelayedText('You Win for Now (TO BE CONTINUED...)', 0.5, 3))
                self._won()

            elif len(towers) - len(self._towers_lost) < self._towers_needed:
                logger.info('LOST: too many towers taken by ants')
                self.world.screen_messages.append(DelayedText('You Lost Too Many Chips!', 0.3, 2))
                self.world.screen_messages.append(DelayedText('Try Again', 1, 2))
                self._lost()

            elif self.world.player.health <= 0:
                logger.info('LOST: player ran out of chips!')
                self.world.screen_messages.append(DelayedText("Ants Savaged Your Chip Pack!", 0.3, 2))
                self.world.screen_messages.append(DelayedText('Try Again', 1, 2))
                self._lost()

        elif self.state == settings.TOWER_WATCHER_WON:
            pass
        elif self.state == settings.TOWER_WATCHER_LOST:
            pass

    def _coll_hero_tower(self, heroes, towers):
        if self.state not in (settings.TOWER_WATCHER_RACING_ANTS, settings.TOWER_WATCHER_WON):
            return
        for hero in heroes:
            for chips in towers:
                # if chips.health > 0 and hero.height <= chips.height and \
                #                 chips.position.get_distance_sq(hero.position) < (chips.radius + hero.radius)**2:
                if chips.health > 0 and \
                                chips.position.get_distance_sq(hero.position) < (chips.radius + hero.radius)**2:

                    # hero elapse toward next bite
                    self._hero_bite_elapsed += settings.time_step
                    if self._hero_bite_elapsed >= self._hero_bite_interval:
                        self._hero_bite_elapsed %= self._hero_bite_interval

                        # hero take a bite
                        if chips.health > 0:
                            logger.info('hero takes a bite')
                            eat_per_sec = settings.PlayerTemplate.player_eat_per_sec
                            chips.handle_message(None, None, settings.MSG_EAT, eat_per_sec)
                            hero.health += eat_per_sec

                            # hero takes a tower?
                            if chips not in self._towers_taken:
                                logger.info('hero takes a tower')
                                self._towers_taken.add(chips)
                                self.world.screen_messages.append(DelayedText("You've Taken a Tower, Hero!", 0, 1))
                                self._towers_report += '+'


# NB: There is no level 0.

easy_boss = settings.boss_level_difficulty
if easy_boss <= 0:
    easy_boss = 0.1

w, h = settings.screen_size
w2 = w // 2
h2 = h // 2
# topleft = -w2, -h2
# topright = w2, -h2
# bottomright = w2, h2
# bottomleft = -w2, h2
top = -h2 + 50
right = w2 - 20
bottom = h2 - 10
left = -w2 + 10
topleft = left, top
topright = right, top
bottomright = right, bottom
bottomleft = left, bottom
levels = [
    # 0: Unused
    ['Unused', [], [], []],

    # 1: Trimming hedges in the yard
    [
        # DelayedText(text, delay, duration)
        DelayedText('Trimming the Hedges', 0, 3),
        DelayedText('Shoot Hedges (LMB)', 0.5, 3),
        DelayedText('Grenade Hedges (RMB or "q")', 0.5, 3),
        DelayedText('Trim Both Hedges Before the Sun Sets!', 0.5, 3),
        Yard((0, 0)),
        Shrub((-w * 0.3, h * 0.35 + 78)), Shrub((w * 0.3, h * 0.35 + 78)),
        Hero((0, h * 0.45)),
        # AntSpigot((w * 0.1, h * 0.4), 3.0, count=10),
        Wall((36 - w2, h - h2 + 1), (46 - w2, 715 - h2), 100),  # < +1 because I could sneak out the bottom left
        Wall((46 - w2, 715 - h2), (w - w2, 715 - h2), 100),
        Wall((w - w2, 715 - h2), (w - w2, h - h2), 100),
        Wall((w - w2, h - h2), (0 - w2, h - h2), 0),
    ],

    # 2: Protect the chips in the living room!
    [
        DelayedText('Protect The Chips', 0, 3),
        DelayedText("Don't Grenade Your Chips!", 0.5, 3),
        Room((0, 0)),
        Sofa((w * 0.32, h * 0.17)), Chair1((-w * 0.4, h * 0.4 + 121)), Chair2((-w * 0.25, h * 0.15)),
        Tower((w * 0.15, h * 0.25)),
        Tower((w * 0.16, h * 0.33)),
        Hero((-w * 0.25, h * 0.45)),

        AntSpigot((830 - w2, 500 - h2), 2.0, count=0),
        AntSpigot((330 - w2, 450 - h2), 2.0, count=1000),
        AntSpigot((90 - w2, 630 - h2), 2.0, count=1000),

        Wall((145 - w2, h - h2), (220 - w2, 750 - h2), 0),
        Wall((220 - w2, 750 - h2), (34 - w2, 620 - h2), 0),
        Wall((34 - w2, 620 - h2), (400 - w2, 500 - h2), 0),
        Wall((400 - w2, 500 - h2), (230 - w2, 470 - h2), 0),
        Wall((230 - w2, 470 - h2), (370 - w2, 400 - h2), 0),
        Wall((370 - w2, 400 - h2), (680 - w2, 400 - h2), 0),  # back wall
        Wall((680 - w2, 400 - h2), (950 - w2, 500 - h2), 0),
        Wall((950 - w2, 500 - h2), (639 - w2, 509 - h2), 0),

        # Wall((639 - w2, 509 - h2), (636 - w2, 537 - h2), 0),
        Wall((630 - w2, 405 - h2), (636 - w2, 537 - h2), 0),
        Wall((636 - w2, 537 - h2), (w - w2, 650 - h2), 0),
        Wall((w - w2, 650 - h2), (w - w2, h - h2), 0),
        Wall((w - w2, h - h2), (145 - w2, h - h2), 0),
        ChipsWatcherLevel2()
    ],

    # 3: Dark adventure under the sofa.
    [
        DelayedText('Beneath the Sofa', 0.5, 1.5),
        DelayedText('Recover All Three Piles of Chips', 0.5, 1.5),
        DelayedText("Hurry! Avoid Ants! ", 0.5, 1.5),
        Tower((0, -h2 * 0.25)),         # top
        Tower((-w * 0.35, h2 * 0.25)),  # left
        Tower((w * 0.35, h * 0.25)),    # right
        Hero((-w * 0.25, h * 0.45)),

        AntSpigot((-w2 * 0.25, h2 * 0.5), 15.0, count=2),    # left
        AntSpigot((w2 * 0.25, h2 * 0.5), 15.0, count=2),     # right
        AntSpigot((0, 0), 5.0, count=30),                   # center

        # room border
        Wall(topleft, topright, 0),    # top wall
        Wall(topright, bottomright, 0),    # right wall
        Wall(bottomright, bottomleft, 0),    # bottom wall
        Wall(bottomleft, topleft, 0),    # left wall
        ChipsWatcherLevel3(towers_needed=3),
    ],

    # 4: Dark adventure under the chair.
    [
        DelayedText('Beneath the Chair', 0, 4),
        DelayedText('Recover Seven Piles of Chips', 6, 4),
        DelayedText("Hurry! Avoid Ants! ", 0.5, 1.5),
        Tower((-w2 * 0.3, -h2 * 0.3)),  # upper left
        Tower((-w2 * 0.7, -h2 * 0.7)),   # upper left
        Tower((w2 * 0.3, -h2 * 0.3)),   # upper right
        Tower((w2 * 0.7, -h2 * 0.7)),   # upper right
        Tower((-w2 * 0.3, h2 * 0.3)),   # lower left
        Tower((-w2 * 0.7, h2 * 0.7)),   # lower left
        Tower((w2 * 0.3, h2 * 0.3)),   # lower right
        Tower((w2 * 0.7, h2 * 0.7)),   # lower right
        Hero((0, 0)),                   # center

        AntSpigot((-w2 * 0.1, -h2 * 0.1), 10.0, count=30),  # upper left
        AntSpigot((w2 * 0.1, -h2 * 0.1), 8.0, count=30),    # upper right
        AntSpigot((-w2 * 0.1, h2 * 0.1), 6.0, count=30),    # lower left
        AntSpigot((w2 * 0.1, h2 * 0.1), 4.0, count=30),     # lower left

        # room border
        Wall(topleft, topright, 0),    # top wall
        Wall(topright, bottomright, 0),    # right wall
        Wall(bottomright, bottomleft, 0),    # bottom wall
        Wall(bottomleft, topleft, 0),    # left wall
        ChipsWatcherLevel3(towers_needed=7),
    ],

    # 5: Dark adventure inside the wall.
    [
        DelayedText('> MANO A MANDIBLE <', 0, 4),
        DelayedText('Recover All the Piles of Chips', 6, 4),
        Tower((-w2 * 0.3, -h2 * 0.3)),  # upper left
        Tower((-w2 * 0.7, -h2 * 0.7)),   # upper left
        Tower((w2 * 0.3, -h2 * 0.3)),   # upper right
        Tower((w2 * 0.7, -h2 * 0.7)),   # upper right
        Tower((-w2 * 0.3, h2 * 0.3)),   # lower left
        Tower((-w2 * 0.7, h2 * 0.7)),   # lower left
        Tower((w2 * 0.3, h2 * 0.3)),   # lower right
        Tower((w2 * 0.7, h2 * 0.7)),   # lower right
        Hero((0, 0)),                   # center

        Thing(((0, -h2 * 0.7))),
        Thing(((0, h2 * 0.7))),
        Thing(((w2 * 0.7, 0))),
        Thing(((-w2 * 0.7, 0))),
        # Thing(((w * 0.11, h * 0.151))),
        # Thing(((w * 0.17, h * 0.22))),
        # Thing(((w * 0.23, h * 0.33))),
        # Thing(((w * 0.34, h * 0.54))),

        AntSpigot((-w2 * 0.1, -h2 * 0.1), 10.0 / easy_boss, count=30),  # upper left
        AntSpigot((w2 * 0.1, -h2 * 0.1), 8.0 / easy_boss, count=30),    # upper right
        AntSpigot((-w2 * 0.1, h2 * 0.1), 6.0 / easy_boss, count=30),    # lower left
        AntSpigot((w2 * 0.1, h2 * 0.1), 4.0 / easy_boss, count=30),     # lower left

        # room border
        Wall(topleft, topright, 0),    # top wall
        Wall(topright, bottomright, 0),    # right wall
        Wall(bottomright, bottomleft, 0),    # bottom wall
        Wall(bottomleft, topleft, 0),    # left wall
        ChipsWatcherLevel3(towers_needed=8),
    ],

    # 6: Boss battle!
    [
        DelayedText('Dev Sandbox Level', 0, 4),
        Tower((w * 0.15, h * 0.25)),
        Hero((-w * 0.25, h * 0.45)),
        AntSpigot((0, 0), 15, count=30),
        ChipsWatcherLevel3(towers_needed=1),
    ],
]

if settings.ambient_color is not None:
    levels[3].append(Light(settings.ambient_color))
    levels[4].append(Light(settings.ambient_color))
