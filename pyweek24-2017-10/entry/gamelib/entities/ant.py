# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'Ant.py' is part of CoDoGuPywk24
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]                    # For instance:
                   |                                  * 1.2.0.1 instead of 1.2-a
                   +-|* 0 for alpha (status)          * 1.2.1.2 instead of 1.2-b2 (beta with some bug fixes)
                     |* 1 for beta (status)           * 1.2.2.3 instead of 1.2-rc (release candidate)
                     |* 2 for release candidate       * 1.2.3.0 instead of 1.2-r (commercial distribution)
                     |* 3 for (public) release        * 1.2.3.5 instead of 1.2-r5 (commercial dist with many bug fixes)


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging
import random

from gamelib import settings
from gamelib.entities import GameBaseEntity, BaseState
from pyknic.ai.statemachines import StateDrivenAgentBase
from pyknic.mathematics import Vec2
from pyknic.timing import Timer

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2017"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 

logger = logging.getLogger(__name__)
logger.debug("importing...")


class _M(object):
    normal = "_normal"
    # fire = "_fire"
    # ice = "_ice"


class _A(object):
    attack = "attack"
    block = "block"
    die = "die"
    hit = "hit"
    stance = "stance"
    walk = "walk"


class _AntBaseState(BaseState):

    @staticmethod
    def enter(owner):
        pass

    @staticmethod
    def exit(owner):
        pass

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        pass

    @staticmethod
    def handle_event(owner, event):
        pass

    @staticmethod
    def handle_message(owner, msg_type, extra):
        if msg_type == settings.MSG_HIT:
            owner.health -= extra.damage
            if owner.health <= 0:
                owner.state_machine.switch_state(_Die)
            else:
                owner.direction = (owner.position - extra.position).normalized
                owner.direction.rotate(random.randint(-45, 45))
                owner.state_machine.switch_state(_Flee)
        elif msg_type == settings.MSG_RETREAT:
            if owner.health > 0:
                spigots = extra.registry.get_by_kind(settings.KIND_ANT_SPIGOT)
                owner.target = owner.get_nearest(spigots)
                owner.state_machine.switch_state(_MoveStraight)
                owner.stop = extra
        elif msg_type == settings.MSG_STOMP:
            owner.target = extra
            owner.follow_timer.start(owner.follow_timeout, False)
            owner.state_machine.switch_state(_Eat)


class _Wander(_AntBaseState):

    @staticmethod
    def enter(owner):
        owner.action_only = _A.stance
        owner.mode = _M.normal

    @staticmethod
    def handle_message(owner, msg, extra):
        if msg == settings.MSG_ANIM_END:
            extra.stop()
        else:
            _AntBaseState.handle_message(owner, msg, extra)


class _Stance(_AntBaseState):

    @staticmethod
    def enter(owner):
        owner.action_only = _A.stance

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        if owner.target is None:
            # towers = owner.world.registry.get_by_kind(settings.KIND_TOWER)
            towers = owner.world.registry.get_by_bit_kind(settings.KIND_TOWER | settings.KIND_HERO)
            if towers:
                healthy_towers = [t for t in towers if t.health > 0]
                if healthy_towers:
                    owner.target = random.choice(healthy_towers)
                else:
                    owner.state_machine.switch_state(_Wander)
            else:
                owner.state_machine.switch_state(_Wander)
        else:
            owner.state_machine.switch_state(_ChangeDir)

    @staticmethod
    def handle_event(owner, event):
        pass


class _Eat(_AntBaseState):

    @staticmethod
    def enter(owner):
        owner.action_only = _A.attack
        owner.timer.start(interval=0.5)
        owner.speed = 0
        # first bite... mmmh!
        owner.target.handle_message(owner.id, owner.target.id, settings.MSG_EAT, owner.eat_per_sec / 2.0)

    @staticmethod
    def exit(owner):
        owner.timer.stop()

    @staticmethod
    def on_timer(owner, *args, **kwargs):
        if owner.target and \
                        owner.target.health >= 0 and \
                        owner.position.get_distance_sq(owner.target.position) < (owner.radius + owner.target.radius)**2:
            owner.target.handle_message(owner.id, owner.target.id, settings.MSG_EAT, owner.eat_per_sec / 2.0)
        else:
            owner.target = None if owner.target and owner.target.kind != settings.KIND_HERO else owner.target
            owner.state_machine.switch_state(_Stance)


class _Walk(_AntBaseState):

    @staticmethod
    def enter(owner):
        owner.action_only = _A.walk
        duration = random.random() * 3
        owner.timer.start(interval=duration)
        owner.speed = owner.walk_speed

    @staticmethod
    def exit(owner):
        owner.timer.stop()

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        owner.v = owner.speed * owner.direction
        owner.position += owner.v * dt

        if owner.target is not None and  \
                        owner.position.get_distance_sq(owner.target.position) < (owner.radius + owner.target.radius)**2:
            owner.state_machine.switch_state(_Eat)

    @staticmethod
    def on_timer(owner, *args, **kwargs):
        owner.state_machine.switch_state(_ChangeDir)


class _ChangeDir(_AntBaseState):

    @staticmethod
    def enter(owner):
        owner.action_only = _A.stance
        r = random.random() * 1.0
        owner.timer.start(interval=r)

    @staticmethod
    def exit(owner):
        owner.timer.stop()

    @staticmethod
    def on_timer(owner, *args, **kwargs):
        if owner.target is not None:
            owner.direction = (owner.target.position - owner.position).normalized
        var_angle = 90
        owner.direction = owner.direction.rotated(random.randint(-1 * var_angle, var_angle))
        owner.state_machine.switch_state(_Walk)


class _Die(_AntBaseState):

    @staticmethod
    def enter(owner):
        owner.action_only = _A.die

    @staticmethod
    def handle_message(owner, msg, extra):
        if msg == settings.MSG_ANIM_END:
            extra.stop()
        else:
            _AntBaseState.handle_message(owner, msg, extra)


class _Flee(_AntBaseState):

    @staticmethod
    def enter(owner):
        owner.action_only = _A.walk
        r = random.random() * 1.0
        owner.timer.start(interval=r)
        owner.speed = owner.flee_speed

    @staticmethod
    def exit(owner):
        owner.timer.stop()

    @staticmethod
    def on_timer(owner, *args, **kwargs):
        owner.state_machine.switch_state(_Walk)

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        owner.v = owner.speed * owner.direction
        owner.position += owner.v * dt


class _MoveStraight(_AntBaseState):

    @staticmethod
    def enter(owner):
        owner.speed = owner.flee_speed
        owner.direction = (owner.target.position - owner.position).normalized

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        owner.v = owner.speed * owner.direction
        owner.position += owner.v * dt
        if owner.position.get_distance_sq(owner.target.position) < (owner.radius + owner.target.radius)**2:
            if owner.stop is not None:
                owner.speed = 0
                owner.stop.remove_entity(owner)
            else:
                owner.target = None
                owner.state_machine.switch_state(_Stance)

    @staticmethod
    def handle_message(owner, msg_type, extra):
        if msg_type == settings.MSG_HIT:
            owner.health -= extra.damage
            if owner.health <= 0:
                owner.state_machine.switch_state(_Die)
        else:
            _AntBaseState.handle_message(owner, msg_type, extra)


class _TargetHelp(object):

    def __init__(self, position):
        self.position = position
        self.radius = 20


class Ant(GameBaseEntity, StateDrivenAgentBase):

    def __init__(self, position, radius, world, _id=None, template=settings.AntTemplate):
        GameBaseEntity.__init__(self, settings.KIND_ANT, position, radius, init_id=_id)
        StateDrivenAgentBase.__init__(self, _MoveStraight)
        self.facing = 0
        self._mode = _M.normal
        self._action_only = ""
        self.action_only = _A.stance  # use this to set the action, only this
        # self.template = template
        self.health = template.health
        self.height = template.height
        self._direction = Vec2(1.0, 0.0)
        self.predator = None
        vec_ = Vec2(1, 0).rotated(random.randint(0, 360))
        vec_.length = random.randint(50, 150)
        self.target = _TargetHelp(self.position + vec_)
        self.world = world
        self.speed = 0
        self.v = Vec2(0, 0)
        self.timer = Timer()
        self.timer.repeat = True
        self.follow_timer = Timer()
        self.follow_timer.event_elapsed.add(self._on_follow_timer)
        self.eat_per_sec = template.eat_per_sec
        self.flee_speed = template.flee_speed
        self.walk_speed = template.walk_speed
        self.timer.event_elapsed.add(self._on_timer)
        self.stop = None
        self.state_machine.switch_state(_MoveStraight)
        self.follow_timeout = template.follow_timeout

    @property
    def direction(self):
        return self._direction

    @direction.setter
    def direction(self, value):
        self._direction = value
        new_value = (self._direction.angle + 22.5) % 360 // 45  # TODO: should be in gfx, there the # frames is known
        self.facing = new_value

    @property
    def action_only(self):
        return self._action_only

    @action_only.setter
    def action_only(self, value):
        """
        Use this here to set the action because if will combine it with the mode. Do not use the 'action'
        property directly.
        :return:
        """
        if self._action_only != value:
            self._action_only = value
            self._update_action()

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, value):
        if value != self._mode:
            self._mode = value
            self._update_action()

    def _update_action(self):
        self.action = self.action_only + self.mode

    def handle_message(self, sender, receiver, msg_type, extra):
        self.state_machine.current_state.handle_message(self, msg_type, extra)

    def get_nearest(self, others):
        nearest = others[0]
        nd_sq = self.position.get_distance_sq(nearest.position)
        for o in others[1:]:
            d_sq = self.position.get_distance_sq(o.position)
            if d_sq < nd_sq:
                nd_sq = d_sq
                nearest = o
        return nearest

    def _on_timer(self, *args, **kwargs):
        self.state_machine.current_state.on_timer(self, *args, **kwargs)

    def _on_follow_timer(self, *args, **kwargs):
        self.target = None
        self.state_machine.switch_state(_Stance)  # avoid problems

logger.debug("imported")
