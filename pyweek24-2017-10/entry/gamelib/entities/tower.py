# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'tower.py' is part of CoDoGuPywk24
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]                    # For instance:
                   |                                  * 1.2.0.1 instead of 1.2-a
                   +-|* 0 for alpha (status)          * 1.2.1.2 instead of 1.2-b2 (beta with some bug fixes)
                     |* 1 for beta (status)           * 1.2.2.3 instead of 1.2-rc (release candidate)
                     |* 2 for release candidate       * 1.2.3.0 instead of 1.2-r (commercial distribution)
                     |* 3 for (public) release        * 1.2.3.5 instead of 1.2-r5 (commercial dist with many bug fixes)


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division

import logging

from gamelib import settings
from gamelib.entities import GameBaseEntity
from pyknic.mathematics import Point2 as Point

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2017"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 

logger = logging.getLogger(__name__)
logger.debug("importing...")


class Tower(GameBaseEntity):

    def __init__(self, position=Point(0, 0), _id=None, template=settings.TowerTemplate):
        GameBaseEntity.__init__(self, settings.KIND_TOWER, position, 28, init_id=_id)
        self.action = 0  # actions 0-6, 6 has not chips left!!
        self.facing = 0
        self.health = template.health
        self._step = self.health // 6
        self.destroyed = False

    @property
    def height(self):
        return (6 - self.action) / 6.0 * 150  # hero is about 60 in height

    @height.setter
    def height(self, value):
        pass

    def handle_message(self, sender, receiver, msg, extra):
        if msg == settings.MSG_EAT:
            self.health -= extra
            extra = None
        elif msg == settings.MSG_HIT:
            self.health -= extra.damage

        new_action = 6 - (self.health // self._step)
        self.action = new_action if new_action < 6 else 5
        if self.health <= 0:
            if extra is not None and extra.kind == settings.KIND_BLAST:
                self.destroyed = True


logger.debug("imported")
