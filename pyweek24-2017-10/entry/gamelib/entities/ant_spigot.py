# -*- coding: utf-8 -*-

from __future__ import print_function

import logging
import time

from gamelib import settings
from gamelib.entities import GameBaseEntity


__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2017"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class AntSpigot(GameBaseEntity):

    def __init__(self, position, frequency, per_pulse, count, world, _id=None):
        GameBaseEntity.__init__(self, settings.KIND_ANT_SPIGOT, position, 3, init_id=_id)
        self.frequency = frequency
        self.per_pulse = per_pulse
        self.count = count
        self.world = world
        self._elapsed = frequency
        self._logged_once = False

    def update(self, delta_time, sim_time):
        if self.count > 0:
            self._elapsed += delta_time
            if self._elapsed >= self.frequency:
                    pulse = self.per_pulse
                    if self.frequency == 0:
                        pulse = self.count
                    else:
                        self._elapsed %= self.frequency
                    logger.debug('SPIGOT releasing {} ants at {:0.1f}', pulse, time.time())
                    for i in range(pulse):
                        self.world.create_ant(self.position.clone(), 20)
                    self.count -= pulse
        elif not self._logged_once:
            logger.debug('SPIGOT exhausted')
            self._logged_once = True
