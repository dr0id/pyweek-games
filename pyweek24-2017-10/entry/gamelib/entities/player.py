# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'player.py' is part of CoDoGuPywk24
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]                    # For instance:
                   |                                  * 1.2.0.1 instead of 1.2-a
                   +-|* 0 for alpha (status)          * 1.2.1.2 instead of 1.2-b2 (beta with some bug fixes)
                     |* 1 for beta (status)           * 1.2.2.3 instead of 1.2-rc (release candidate)
                     |* 2 for release candidate       * 1.2.3.0 instead of 1.2-r (commercial distribution)
                     |* 3 for (public) release        * 1.2.3.5 instead of 1.2-r5 (commercial dist with many bug fixes)


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

from pyknic.timing import Timer
from pyknic.generators import IdGenerator
from pyknic.mathematics import EPSILON
from pyknic.ai.statemachines import StateDrivenAgentBase
from gamelib.settings import ACTION_MOVE_UP, ACTION_MOVE_DOWN, ACTION_MOVE_LEFT, ACTION_MOVE_RIGHT, ACTION_STOP_UP, \
    ACTION_STOP_DOWN, ACTION_STOP_LEFT, ACTION_STOP_RIGHT, ACTION_FIRE, ACTION_HOLD_FIRE, ACTION_USE_ITEM, \
    ACTION_MOUSE_MOTION

from pyknic.mathematics import Vec2 as Vec
from pyknic.mathematics import Point2 as Point
from gamelib import settings
from gamelib import resource_sound
from gamelib.entities import GameBaseEntity, BaseState

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2017"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 

logger = logging.getLogger(__name__)
logger.debug("importing...")


class _A(object):
    # greeting = "greeting"
    # hit = "hit"
    # tip_over = "over"
    # running = "running"
    shooting = "shooting"
    stopped = "stopped"
    throwing = "throwing"
    walking = "walking"

_id_gen = IdGenerator(0)
_e_fire = _id_gen.next()
_e_hold_fire = _id_gen.next()
_e_use_item = _id_gen.next()
_e_anim_end = _id_gen.next()


class _PlayerBaseState(BaseState):
    @staticmethod
    def update_direction(owner, pos):
        owner.target = pos
        owner.direction = pos - owner.position
        owner.direction.normalize()

        owner.facing = (owner.direction.angle + 22.5) % 360 // 45  # TODO: should be in gfx, there the # frames is known

    @staticmethod
    def enter(owner):
        owner.action = _A.stopped

    @staticmethod
    def handle_event(owner, event):
        if event == _e_fire:
            owner.state_machine.switch_state(_Shooting)
        elif event == _e_hold_fire:
            if owner.v.length_sq > EPSILON:
                owner.state_machine.switch_state(_Walk)
            else:
                owner.state_machine.switch_state(_Stand)
        elif event == _e_use_item and owner.last_throw + owner.throw_delay < owner.sim_t:
            owner.state_machine.switch_state(_Throw)

    @staticmethod
    def exit(owner):
        pass


class _Throw(_PlayerBaseState):

    @staticmethod
    def enter(owner):
        owner.action = _A.throwing
        owner.create_grenade()
        owner.last_throw = owner.sim_t

    @staticmethod
    def handle_event(owner, event):
        if event == _e_anim_end:
            if owner.action_state[4]:
                owner.state_machine.switch_state(_Shooting)
            else:
                owner.state_machine.switch_state(_Stand)

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        if settings.PlayerTemplate.is_strafe_on:
            owner.old_position = owner.position.clone()
            owner.position += owner.v * dt
            _Walk.update_direction(owner, owner.target)


class _Shooting(_PlayerBaseState):

    @staticmethod
    def enter(owner):
        owner.action = _A.shooting
        owner.timer.start()
        owner._create_bullet()

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        # settings.sfx_handler.handle_message(None, None, resource_sound.SFX_GUN_FIRE_MACHINE_GUN_BURST_1, 'queue')
        pass

    @staticmethod
    def exit(owner):
        settings.sfx_handler.handle_message(None, None, resource_sound.SFX_GUN_FIRE_MACHINE_GUN_BURST_2, None)
        owner.timer.stop()

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        if settings.PlayerTemplate.is_strafe_on:
            owner.old_position = owner.position.clone()
            owner.position += owner.v * dt
            _Walk.update_direction(owner, owner.target)


class _Stand(_PlayerBaseState):

    @staticmethod
    def enter(owner):
        owner.action = _A.stopped

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        if owner.v.length_sq > 0:
            owner.state_machine.switch_state(_Walk)


class _Walk(_PlayerBaseState):

    @staticmethod
    def enter(owner):
        owner.action = _A.walking

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        if owner.v.length_sq <= EPSILON:
            owner.state_machine.switch_state(_Stand)
        owner.old_position = owner.position.clone()
        owner.position += owner.v * dt
        _Walk.update_direction(owner, owner.target)


class Player(GameBaseEntity, StateDrivenAgentBase):

    def __init__(self, world, position=Point(0.0, 0.0), init_id=None, template=settings.PlayerTemplate):
        GameBaseEntity.__init__(self, settings.KIND_HERO, position, template.player_radius, init_id=init_id)
        StateDrivenAgentBase.__init__(self, _Stand)
        self.direction = Vec(1.0, 0.0)
        self.speed = template.player_speed
        self.v = Vec(0.0, 0.0)
        self.facing = 0
        self.action = _A.stopped
        self.world = world
        self.timer = Timer(template.player_gun_fire_rate, True, scheduler=self.world.scheduler)
        self.timer.event_elapsed.add(self._create_bullet)
        self.height = template.player_height
        self.target = self.position.clone()
        self.action_state = [False, False, False, False, False]  # up, down, left, right, shoot
        self.sim_t = 0
        self.throw_delay = template.player_throw_delay
        self.last_throw = -template.player_throw_delay * 2  # first throw should be possible right away
        self.health = template.player_health

    def _create_bullet(self, *args):
        cursor = self.world.registry.get_by_kind(settings.KIND_CURSOR)[0]
        gun_pos = self.position + Vec(0, -35) + self.direction * 30 + self.direction.normal_left * 5
        self.world.create_bullet(gun_pos.clone(), self.direction.clone(), cursor.position.clone())
        settings.sfx_handler.handle_message(None, None, resource_sound.SFX_GUN_FIRE_MACHINE_GUN_BURST_1, 'queue')

    def create_grenade(self, *args):
        cursor = self.world.registry.get_by_kind(settings.KIND_CURSOR)[0]
        gun_pos = self.position + Vec(0, -42) + self.direction * 0 + self.direction.normal_left * 10
        self.world.create_grenade(gun_pos.clone(), self.direction.clone(), cursor.position.clone())

    def handle_action(self, action, extra):
        if action == ACTION_MOVE_DOWN:
            self.action_state[0] = True
            # dv.y += 1
        elif action == ACTION_STOP_DOWN:
            self.action_state[0] = False
            # dv.y -= 1

        elif action == ACTION_MOVE_UP:
            self.action_state[1] = True
            # dv.y -= 1
        elif action == ACTION_STOP_UP:
            self.action_state[1] = False
            # dv.y += 1

        elif action == ACTION_MOVE_RIGHT:
            self.action_state[2] = True
            # dv.x += 1
        elif action == ACTION_STOP_RIGHT:
            self.action_state[2] = False
            # dv.x -= 1

        elif action == ACTION_MOVE_LEFT:
            self.action_state[3] = True
            # dv.x -= 1
        elif action == ACTION_STOP_LEFT:
            self.action_state[3] = False
            # dv.x += 1

        elif action == ACTION_FIRE:
            self.handle_event(_e_fire)
            self.action_state[4] = True
        elif action == ACTION_HOLD_FIRE:
            self.handle_event(_e_hold_fire)
            self.action_state[4] = False

        elif action == ACTION_MOUSE_MOTION:
            pos, rel, buttons = extra
            self.state_machine.current_state.update_direction(self, pos)
        elif action == ACTION_USE_ITEM:
            self.handle_event(_e_use_item)

        dv = Vec(self.action_state[2] - self.action_state[3], self.action_state[0] - self.action_state[1])

        dv.length = self.speed
        self.v.copy_values(dv)

    def handle_message(self, sender, receiver, msg_type, extra):
        if msg_type == settings.MSG_ANIM_END:
            self.handle_event(_e_anim_end)
        elif msg_type == settings.MSG_EAT:
            self.health -= extra

    def update(self, dt, sim_t, *args, **kwargs):
        self.sim_t = sim_t
        StateDrivenAgentBase.update(self, dt, sim_t, *args, **kwargs)


logger.debug("imported")
