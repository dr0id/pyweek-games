﻿cANT2: The Hidden Threat
========================

CONTACT: DR0ID, Gummbum - #pygame @ freenode.net

Homepage: https://pyweek.org/e/codogupywk24/
Name: cANT2: The Hidden Threat
Team:  codogupywk24
Members: DR0ID, Gummbum


DEPENDENCIES:

You might need to install these before running the game:

  Python:     http://www.python.org/
  PyGame:     http://www.pygame.org/

Fully tested with Python 3.6.


RUNNING THE GAME:

On Windows or Mac OS X, locate the "run.pyw" file and double-click it.

Othewise open a terminal / console and "cd" to the game directory and run:

  python run.py



HOW TO PLAY THE GAME:

  *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *
Important note: Post-processing effects on most audio cards do not play well
with the SDL mixer. For the best listening experience, please disable all
effects, especially dynamic equalization, voice enhancement, and volume leveler.
These things are known to defeat the best efforts of game developers to achieve
a balance with music and sfx.
  *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *

Game Controls
- The splash screen accepts any mouse or keyboard press
- Narration screens prompt for input
- WASD: move the hero
- LMB: auto-fire machine gun
- RMB: lob a grenade

Hackers
- Feel free to play with some of the tunables in settings.py. The most useful
  may be master_volume to reduce the overall audio volume if you have things
  going on other windows. Please see the aforementioned caveat on the post-
  processing effects of sound cards.

There are fours entry points, three for quicker access to various parts of the
game. These are intended for the developers and testers, but can be used e.g.
for bigger challenges or cheating on the boss level.

The following code can be placed at the end of settings.py, and indexed for
convenience configuration. startup_context is the game area to jump to.
startup_level is the scene number: there are 5 scenes in the game, plus a
developer sandbox scene #6. The action (gameplay) levels can be edited in
level_data.py.

# 0: context_intro
# 1: context_mainmenu
# 2: context_gameplay
# 3: context_credits
(startup_context, startup_level) = (
    (0, 1),     # default: beginning
    (1, 3),     # mainmenu
    (2, 5),     # gameplay
    (3, 1),     # credits
)[0]


LICENSE:

This game "cANT2: The Hidden Threat" has the same license as pyknic.

Various licenses and credits for assets may be littered throughout the data/
directories.
