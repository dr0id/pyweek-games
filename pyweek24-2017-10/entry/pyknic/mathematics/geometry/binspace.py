﻿# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'binspace.py' is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]                    # For instance:
                   |                                  * 1.2.0.1 instead of 1.2-a
                   +-|* 0 for alpha (status)          * 1.2.1.2 instead of 1.2-b2 (beta with some bug fixes)
                     |* 1 for beta (status)           * 1.2.2.3 instead of 1.2-rc (release candidate)
                     |* 2 for release candidate       * 1.2.3.0 instead of 1.2-r (commercial distribution)
                     |* 3 for (public) release        * 1.2.3.5 instead of 1.2-r5 (commercial dist with many bug fixes)


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division

import collections
import logging
from collections import defaultdict

import itertools

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2017"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["BinSpace"]  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class BinSpace(object):

    def __init__(self, cell_size=50):
        self._cells = collections.defaultdict(set)
        self._entity_to_cells = collections.defaultdict(set)
        self._cell_size = cell_size  # TODO: make it adaptive? but then one need more info about the entity (size, pos)!

    def add_with_radius(self, e, x, y, r):
        self.adds_with_radius([(e, x, y, r)])

    def _adds_with_radius(self, circles, _int=int, _range=range, _product=itertools.product):
        self_cells = self._cells
        self__cell_size = self._cell_size
        self_entity_to_cells = self._entity_to_cells
        self_remove = self.remove
        # for e, x, y, r in circles:
        for c in circles:
            e = c[0]
            if e in self_entity_to_cells:
                self_remove(e)

            r = c[3]
            cx_min = _int((c[1] - r) // self__cell_size)
            cx_max = _int((c[1] + r) // self__cell_size)
            cy_min = _int((c[2] - r) // self__cell_size)
            cy_max = _int((c[2] + r) // self__cell_size)

            self_entity_to_cells_e__add = self_entity_to_cells[e].add

            # y_range = _range(cy_min, cy_max + 1)
            # for _x in _range(cx_min, cx_max + 1):
            #     for _y in y_range:
            for _cell_id in _product(_range(cx_min, cx_max + 1), _range(cy_min, cy_max + 1)):
                self_cells[_cell_id].add(e)
                self_entity_to_cells_e__add(_cell_id)

    def adds_with_radius(self, circles, _int=int, _range=range, _product=itertools.product):
        self_cells = self._cells
        self__cell_size = self._cell_size
        self_entity_to_cells = self._entity_to_cells
        for c in circles:
            # c = e, x, y, r
            e = c[0]
            if e in self_entity_to_cells:
                self.remove(e)
            r = _int(c[3])
            rr = 2 * r
            cx_min = _int((c[1] - r))
            cy_min = _int((c[2] - r))

            self_entity_to_cells_e__add = self_entity_to_cells[e].add
            for _cell_id in _product(_range(cx_min // self__cell_size, (cx_min + rr) // self__cell_size + 1),
                                     _range(cy_min // self__cell_size, (cy_min + rr) // self__cell_size + 1)):

                    self_cells[_cell_id].add(e)
                    self_entity_to_cells_e__add(_cell_id)

    def __len__(self):
        return len(self._entity_to_cells)

    def clear(self):
        self._cells.clear()
        self._entity_to_cells.clear()

    def collides(self, circles, do_self_collision=False, _int=int, _range=range, _set=set, _defaultdict=defaultdict, _product=itertools.product):
        result = _defaultdict(_set)
        self_cells = self._cells
        self__cell_size = self._cell_size

        others = _set()
        others_clear = others.clear
        others_discard = others.discard
        result_pop = result.pop

        for c in circles:
            # c = e, x, y, r
            r = _int(c[3])
            rr = 2 * r
            e = c[0]
            cx_min = _int((c[1] - r))
            cy_min = _int((c[2] - r))

            result_e = result[e]
            result_e__add = result_e.add

            others_clear()

            for cell_id in _product(_range(cx_min // self__cell_size, (cx_min + rr) // self__cell_size + 1),
                                    _range(cy_min // self__cell_size, (cy_min + rr) // self__cell_size + 1)):
                others |= self_cells[cell_id]

            if not do_self_collision:
                others_discard(e)

            for other in others:
                if other in result:
                    result[other].add(e)
                else:
                    result_e__add(other)
            if not result[e]:
                result_pop(e)

        return result

    def _collides(self, circles, do_self_collision=False, _int=int, _range=range, _set=set, _defaultdict=defaultdict, _product=itertools.product):
        result = _defaultdict(_set)
        self_cells = self._cells
        self__cell_size = self._cell_size

        result_pop = result.pop

        for c in circles:
            # c = e, x, y, r
            r = _int(c[3])
            rr = 2 * r
            e = c[0]

            cx_min = _int((c[1] - r))
            cy_min = _int((c[2] - r))

            result_e = result[e]
            result_e__add = result_e.add

            for cell_id in _product(_range(cx_min // self__cell_size, (cx_min + rr) // self__cell_size + 1),
                                    _range(cy_min // self__cell_size, (cy_min + rr) // self__cell_size + 1)):
                for other in self_cells[cell_id]:
                    if other in result:
                        result[other].add(e)
                    else:
                        result_e__add(other)

            if not do_self_collision:
                result_e.discard(e)
                if not result[e]:
                    result_pop(e)

        return result

    def __collides(self, circles, do_self_collision=False, _int=int, _ra=range, _set=set, _defaultdict=defaultdict, _pr=itertools.product):
        re = _defaultdict(_set)
        sc = self._cells
        ssi = self._cell_size
        ot = _set()
        otc = ot.clear
        otd = ot.discard
        rep = re.pop
        for c in circles:
            r = _int(c[3])
            rr = 2 * r
            e = c[0]
            cxi = _int((c[1] - r))
            cyi = _int((c[2] - r))

            ree = re[e]
            rea = ree.add
            otc()

            for ci in _pr(_ra(cxi // ssi, (cxi + rr) // ssi + 1), _ra(cyi // ssi, (cyi + rr) // ssi + 1)):
                ot |= sc[ci]

            if not do_self_collision:
                otd(e)

            for o in ot:
                if o in re:
                    re[o].add(e)
                else:
                    rea(o)
            if not re[e]:
                rep(e)

        return re

    def get_in_rect(self, x, y, w, h, _int=int):
        result = set()
        cx_min = _int(x // self._cell_size)
        cx_max = _int((x + w) // self._cell_size)
        cy_min = _int(y // self._cell_size)
        cy_max = _int((y + h) // self._cell_size)
        self_cells = self._cells
        result_update = result.update

        y_range = range(cy_min, cy_max + 1)
        for _x in range(cx_min, cx_max + 1):
            for _y in y_range:
                result_update(self_cells[(_x, _y)])

        # TODO: maybe return same result structure as collides
        return list(result)

    def remove(self, entity):
        self.removes([entity])

    def removes(self, entities):
        for e in entities:
            if e in self._entity_to_cells:
                for cell in self._entity_to_cells.pop(e, []):
                    self._cells[cell].remove(e)

logger.debug("imported")
