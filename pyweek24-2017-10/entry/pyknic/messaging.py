# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'messaging.py' is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]                    # For instance:
                   |                                  * 1.2.0.1 instead of 1.2-a
                   +-|* 0 for alpha (status)          * 1.2.1.2 instead of 1.2-b2 (beta with some bug fixes)
                     |* 1 for beta (status)           * 1.2.2.3 instead of 1.2-rc (release candidate)
                     |* 2 for release candidate       * 1.2.3.0 instead of 1.2-r (commercial distribution)
                     |* 3 for (public) release        * 1.2.3.5 instead of 1.2-r5 (commercial dist with many bug fixes)


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2017"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["MessageDispatcher"]  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class _DummyReceiver(object):
    """
    The dummy receiver. Used internally by the MessageDispatcher for all messages sent to an unknown receiver.
    Unknown by the entity registry. See: MessageDispatcher.
    """
    def __init__(self, logger_to_use):
        """
        Constructor.
        :param logger_to_use: The logger to use be used, can be None to disable logging.
        """
        self._logger = logger_to_use

    def handle_message(self, sender, receiver, msg_type, extra):
        """
        Handle the messages for this entity. All it does it log a warning if logging is enabled.
        :param sender: The sender id.
        :param receiver: The receiver id.
        :param msg_type: The message type.
        :param extra: Extra info for that msg type.
        :return: False, always.
        """
        if self._logger is not None:
            self._logger.warning("Unknown receiver: from %s to %s type %s extra %s", sender, receiver, msg_type, extra)
        return False


class MessageDispatcher(object):
    """
    The MessageDispatcher class. Used to send messages to other entities.
    """
    def __init__(self, registry, logger_to_use=None, dummy_receiver_to_use=None):
        """
        Constructor.
        :param registry: The entity registry to use to find the receiver entity.
        :param logger_to_use: The logger instance to use. If set to None the module logger is used. False disables it.
        :param dummy_receiver_to_use: The dummy receiver for unknown id to use. All messages with unknown receiver
            will be sent here.
        """
        self._registry = registry
        self._logger = logger if logger_to_use is None else (None if logger_to_use is False else logger_to_use)
        self._dummy_receiver = _DummyReceiver(self._logger) if dummy_receiver_to_use is None else dummy_receiver_to_use

    def send(self, sender, receiver_id, msg_type, extra=None):
        """
        The send method.
        :param sender: The sender id.
        :param receiver_id: The receiver id.
        :param msg_type: The message type.
        :param extra: Extra info for a message type.
        :return: True if the receiver has accepted or similar, otherwise False.
        """
        entity = self._registry.by_id.get(receiver_id, self._dummy_receiver)
        if self._logger is not None and self._logger.level <= logging.DEBUG:
            self._logger.debug("send <Message %s from %s to %s extra: %s>", msg_type, sender, receiver_id, extra)
        return_value = entity.handle_message(sender, receiver_id, msg_type, extra)
        assert return_value is not None, "Receiver should return True of False: <Message %s from %s to %s extra: %s>" % (msg_type, sender, receiver_id, extra)
        return return_value

logger.debug("imported")
