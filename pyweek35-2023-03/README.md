# README #


### *The Cove* ###

* This is is the entry for the [pyweek 35](https://pyweek.org/35/) competition.
* Challange theme: *In the shadows*

### How to run? ###

* extract the zip
* dependencies: 
  * python 3.11
  * pygame 2.3.0 -*or*- pygame-ce 2.2.0

* navigate to *run_game.py*
* How to run:
  * Linux and Unix: in the console type: __python -OO run_game.py__
  * Windows: in the console type: __py -3.11 -OO run_game.py__

### How to play ###

Use the keys 'a', 's', 'd', 'w' or the arrow keys to move and the mouse to point andn click. 
Take a picture by left click (will be in the pictures directory after the is finished).

### Who do I talk to? ###

* DR0ID on the official discord channel

