# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'textscene.py' is part of pw-35
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Scene showing some text.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2023"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

import pygame

from gamelib.scenemanager import Scene, SceneManager

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class TextScene(Scene):

    def __init__(self, event_provider, screen_provider, text, font, transition):
        self.event_provider = event_provider
        self._screen_provider = screen_provider
        self.text: str = text
        self.font: pygame.font.Font = font
        self._transition = transition

    def enter(self):
        pygame.event.clear()

    def exit(self):
        pygame.event.clear()

    def pause(self):
        pygame.event.clear()

    def resume(self):
        pygame.event.clear()

    def run(self):
        self.draw(self._screen_provider.get_surface())  # draw initially
        cmd = self.update(0)
        while not cmd:
            cmd = self.update(0)
            self.draw(self._screen_provider.get_surface())
        return cmd

    def update(self, dt_s):
        # for event in [self.event_provider.wait()]:
        for event in [self.event_provider.wait()]:
            if event.type == pygame.QUIT:
                return SceneManager.PopCmd(100, self._transition)
            elif event.type == pygame.KEYDOWN:
                return SceneManager.PopCmd(1, self._transition)

    def draw(self, screen, fill_color=(0, 0, 0), do_flip=True):
        lines = self.text.split('\n')
        labels = []
        total_height = 0
        for line in lines:
            label = self.font.render(line, True, (255, 255, 255), (0, 0, 0))
            labels.append(label)
            h = label.get_size()[1]
            total_height += h
        screen_rect = self._screen_provider.get_surface().get_rect()
        y = screen_rect.centery - total_height / 2
        if fill_color:
            screen.fill(fill_color)

        for label in labels:
            r = label.get_rect(midtop=(screen_rect.centerx, y))
            screen.blit(label, r)
            y += r.h

        if do_flip:
            self._screen_provider.flip()


logger.debug("imported")
