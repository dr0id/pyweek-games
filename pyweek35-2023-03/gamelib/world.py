# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'world.py' is part of pw-35
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
"""
from __future__ import print_function, division

import logging
import math
import os
from collections import deque
from contextlib import contextmanager

import pygame

from gamelib import featureswitches, settings, sound, spacez
from gamelib.clipping import clip_segment_against_aabb
from gamelib.gamedata import PictureInfo, GameData
from gamelib.geometry import Segment, Intersection, ViewPolygon
from gamelib.scenemanager import SceneManager, Scene
from gamelib.timing import Accumulator, Timer
from gamelib.transitions import FadeOutAndIn

logging.basicConfig()
logger = logging.getLogger(__name__)

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2023"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"


# __all__ = []  # list of public visible parts of this module


class PreWorld(Scene):

    def __init__(self, event_provider, world):
        self._event_provider = event_provider
        self._world = world

    def exit(self):
        logging.info("pre exit")

    def pause(self):
        logging.info("pre pause")

    def resume(self):
        logging.info("pre resume")

    def run(self):
        cmd = self.update(0)
        while not cmd:
            cmd = self.update(0)
            self.draw(self._screen_provider.get_surface())
        return cmd

    def update(self, dt_s):
        return self._handle_events()

    def draw(self, screen, fill_color=(0, 0, 0), do_flip=True):
        if fill_color:
            screen.fill(fill_color)

        screen.fill((255, 255, 255))

        if do_flip:
            pygame.display.flip()

    def enter(self):
        logging.info("pre enter")

    def _handle_events(self, ):
        next_scene = None
        for event in [self._event_provider.wait()]:
            if event.type == pygame.QUIT:
                next_scene = SceneManager.PopCmd(100, transition=FadeOutAndIn(pygame.display, pygame.time.Clock()))
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    next_scene = SceneManager.PopCmd(transition=FadeOutAndIn(pygame.display, pygame.time.Clock()))
                elif event.key == pygame.K_SPACE:
                    next_scene = SceneManager.PushCmd(self._world,
                                                      transition=FadeOutAndIn(pygame.display, pygame.time.Clock()))
        return next_scene


class World(Scene):

    def __init__(self, clock, event_provider, player, space, camera, game_data, timer, accumulator, font_provider,
                 draw_clock, update_clock):
        self._timer: Timer = timer
        self._accumulator: Accumulator = accumulator
        self._key_seq = deque(maxlen=2)
        self.player:Player = player
        self._event_provider = event_provider
        self._next_state = None
        self._clock: pygame.time.Clock = clock
        self._update_clock = update_clock
        self._draw_clock = draw_clock
        self._player_view_radius = 100
        self._segments: list[Segment] = []
        self._cam = camera
        self.space: spacez.SpacezTuple = space
        self.game_data: GameData = game_data
        self.light_cone_img = None
        self.background_img = None
        self.entry_img = None
        self.cursor_img = None
        self.recorded_points = []
        self.font = font_provider.Font(None, 18)

        self.debug_pos_idx = -1
        self.debug_positions = []
        # troublesome debug positions (disable update in eventhandler!) for those polygons
        self.debug_positions.append((220, 220))
        self.debug_positions.append((138, 275))
        self.debug_positions.append((219, 270))
        self.debug_positions.append((76, 140))
        self.debug_positions.append((151, 249))
        self.debug_positions.append((22, 138))
        self.debug_positions.append((194, 102))
        self.debug_positions.append((157, 435))
        self.debug_positions.append((595, 161))
        self.debug_positions.append((68, 368))
        self.debug_positions.append((113, 401))
        self.debug_positions.append((69, 830))
        self.debug_positions.append((1283, 253))

    def enter(self):
        logging.info("enter")
        pygame.mouse.set_visible(False)

        self._segments = []

        self.light_cone_img = self.load_image("lightcone2.png").convert_alpha()
        lr = self.light_cone_img.get_rect()
        self._player_view_radius = lr.width

        self.background_img = self.load_image("cove.png").convert()
        self.entry_img = self.load_image("entry.png").convert_alpha()
        self.entrylight_img = self.load_image("entrylight.png").convert_alpha()
        self.cursor_img = self.load_image("cursor.png").convert_alpha()
        # import random
        # count = random.randint(10, 100)
        # logging.info("segments generated: %s", count)
        # for i in range(count):
        #     p1 = pygame.Vector2(random.randint(0, 700), random.randint(0, 500))
        #     seg_len = 80
        #     p2 = pygame.Vector2(random.randint(p1.x - seg_len, p1.x + seg_len), random.randint(p1.y - seg_len, p1.y + seg_len))
        #     self._segments.append(Segment(p1, p2))

        # self._segments.append(Segment(pygame.Vector2(20, 20), pygame.Vector2(120, 20)))

        # # debug poly
        # self._segments.append(Segment(pygame.Vector2(661, 632), pygame.Vector2(526, 640)))
        # self._segments.append(Segment(pygame.Vector2(526, 640), pygame.Vector2(598, 589)))
        # self._segments.append(Segment(pygame.Vector2(598, 589), pygame.Vector2(545, 536)))
        # self._segments.append(Segment(pygame.Vector2(545, 536), pygame.Vector2(660, 523)))
        # self._segments.append(Segment(pygame.Vector2(660, 523), pygame.Vector2(661, 632)))
        # return

        # Border
        sw, sh = settings.SCREENSIZE
        sw *= 2
        sh *= 2
        self._segments.append(Segment(pygame.Vector2(sw + 1, 0 - 1), pygame.Vector2(0 - 1, 0 - 1)))
        self._segments.append(Segment(pygame.Vector2(sw + 1, sh + 1), pygame.Vector2(sw + 1, 0 - 1)))
        self._segments.append(Segment(pygame.Vector2(0 - 1, sh + 1), pygame.Vector2(sw + 1, sh + 1)))
        self._segments.append(Segment(pygame.Vector2(0 - 1, 0 - 1), pygame.Vector2(0 - 1, sh + 1)))
        # return

        # Polygon  # 1
        self._segments.append(Segment(pygame.Vector2(100, 150), pygame.Vector2(120, 50)))
        self._segments.append(Segment(pygame.Vector2(120, 50), pygame.Vector2(200, 80)))
        self._segments.append(Segment(pygame.Vector2(200, 80), pygame.Vector2(140, 210)))
        self._segments.append(Segment(pygame.Vector2(140, 210), pygame.Vector2(100, 150)))

        # Polygon  # 2
        self._segments.append(Segment(pygame.Vector2(100, 200), pygame.Vector2(120, 250)))
        self._segments.append(Segment(pygame.Vector2(120, 250), pygame.Vector2(60, 300)))
        self._segments.append(Segment(pygame.Vector2(60, 300), pygame.Vector2(100, 200)))

        # Polygon  # 3
        self._segments.append(Segment(pygame.Vector2(200, 260), pygame.Vector2(220, 150)))
        self._segments.append(Segment(pygame.Vector2(220, 150), pygame.Vector2(300, 200)))
        self._segments.append(Segment(pygame.Vector2(300, 200), pygame.Vector2(350, 320)))
        self._segments.append(Segment(pygame.Vector2(350, 320), pygame.Vector2(200, 260)))

        # Polygon  # 4
        self._segments.append(Segment(pygame.Vector2(340, 60), pygame.Vector2(360, 40)))
        self._segments.append(Segment(pygame.Vector2(360, 40), pygame.Vector2(370, 70)))
        self._segments.append(Segment(pygame.Vector2(370, 70), pygame.Vector2(340, 60)))

        # Polygon  # 5
        self._segments.append(Segment(pygame.Vector2(450, 190), pygame.Vector2(560, 170)))
        self._segments.append(Segment(pygame.Vector2(560, 170), pygame.Vector2(540, 270)))
        self._segments.append(Segment(pygame.Vector2(540, 270), pygame.Vector2(430, 290)))
        self._segments.append(Segment(pygame.Vector2(430, 290), pygame.Vector2(450, 190)))

        # self.space.addlist(self.space, self._segments)
        # return

        # Polygon  # 6
        self._segments.append(Segment(pygame.Vector2(400, 95), pygame.Vector2(580, 50)))
        self._segments.append(Segment(pygame.Vector2(580, 50), pygame.Vector2(480, 150)))
        self._segments.append(Segment(pygame.Vector2(480, 150), pygame.Vector2(400, 95)))

        # map
        self._segments.append(Segment(pygame.Vector2(617, 584), pygame.Vector2(599, 548)))
        self._segments.append(Segment(pygame.Vector2(599, 548), pygame.Vector2(647, 526)))
        self._segments.append(Segment(pygame.Vector2(647, 526), pygame.Vector2(653, 551)))
        self._segments.append(Segment(pygame.Vector2(653, 551), pygame.Vector2(617, 584)))
        ####################
        self._segments.append(Segment(pygame.Vector2(680, 595), pygame.Vector2(709, 544)))
        self._segments.append(Segment(pygame.Vector2(709, 544), pygame.Vector2(740, 547)))
        self._segments.append(Segment(pygame.Vector2(740, 547), pygame.Vector2(729, 574)))
        self._segments.append(Segment(pygame.Vector2(729, 574), pygame.Vector2(680, 595)))
        ####################
        self._segments.append(Segment(pygame.Vector2(580, 645), pygame.Vector2(624, 630)))
        self._segments.append(Segment(pygame.Vector2(624, 630), pygame.Vector2(653, 643)))
        self._segments.append(Segment(pygame.Vector2(653, 643), pygame.Vector2(661, 688)))
        self._segments.append(Segment(pygame.Vector2(661, 688), pygame.Vector2(616, 703)))
        self._segments.append(Segment(pygame.Vector2(616, 703), pygame.Vector2(580, 645)))
        ####################
        self._segments.append(Segment(pygame.Vector2(586, 489), pygame.Vector2(586, 396)))
        self._segments.append(Segment(pygame.Vector2(586, 396), pygame.Vector2(638, 331)))
        self._segments.append(Segment(pygame.Vector2(638, 331), pygame.Vector2(611, 269)))
        self._segments.append(Segment(pygame.Vector2(611, 269), pygame.Vector2(681, 280)))
        self._segments.append(Segment(pygame.Vector2(681, 280), pygame.Vector2(650, 417)))
        self._segments.append(Segment(pygame.Vector2(650, 417), pygame.Vector2(586, 489)))
        ####################
        self._segments.append(Segment(pygame.Vector2(532, 458), pygame.Vector2(440, 411)))
        self._segments.append(Segment(pygame.Vector2(440, 411), pygame.Vector2(419, 349)))
        self._segments.append(Segment(pygame.Vector2(419, 349), pygame.Vector2(518, 366)))
        self._segments.append(Segment(pygame.Vector2(518, 366), pygame.Vector2(532, 458)))
        ####################
        self._segments.append(Segment(pygame.Vector2(839, 495), pygame.Vector2(823, 448)))
        self._segments.append(Segment(pygame.Vector2(823, 448), pygame.Vector2(891, 405)))
        self._segments.append(Segment(pygame.Vector2(891, 405), pygame.Vector2(902, 459)))
        self._segments.append(Segment(pygame.Vector2(902, 459), pygame.Vector2(877, 493)))
        self._segments.append(Segment(pygame.Vector2(877, 493), pygame.Vector2(839, 495)))
        ####################
        self._segments.append(Segment(pygame.Vector2(370, 772), pygame.Vector2(358, 855)))
        self._segments.append(Segment(pygame.Vector2(358, 855), pygame.Vector2(317, 769)))
        self._segments.append(Segment(pygame.Vector2(317, 769), pygame.Vector2(252, 769)))
        self._segments.append(Segment(pygame.Vector2(252, 769), pygame.Vector2(205, 882)))
        self._segments.append(Segment(pygame.Vector2(205, 882), pygame.Vector2(176, 775)))
        self._segments.append(Segment(pygame.Vector2(176, 775), pygame.Vector2(100, 817)))
        self._segments.append(Segment(pygame.Vector2(100, 817), pygame.Vector2(106, 731)))
        self._segments.append(Segment(pygame.Vector2(106, 731), pygame.Vector2(55, 685)))
        self._segments.append(Segment(pygame.Vector2(55, 685), pygame.Vector2(94, 602)))
        self._segments.append(Segment(pygame.Vector2(94, 602), pygame.Vector2(75, 536)))
        self._segments.append(Segment(pygame.Vector2(75, 536), pygame.Vector2(133, 441)))
        self._segments.append(Segment(pygame.Vector2(133, 441), pygame.Vector2(214, 473)))
        self._segments.append(Segment(pygame.Vector2(214, 473), pygame.Vector2(235, 385)))
        self._segments.append(Segment(pygame.Vector2(235, 385), pygame.Vector2(178, 348)))
        self._segments.append(Segment(pygame.Vector2(178, 348), pygame.Vector2(137, 385)))
        self._segments.append(Segment(pygame.Vector2(137, 385), pygame.Vector2(90, 353)))
        self._segments.append(Segment(pygame.Vector2(90, 353), pygame.Vector2(164, 282)))
        self._segments.append(Segment(pygame.Vector2(164, 282), pygame.Vector2(193, 321)))
        self._segments.append(Segment(pygame.Vector2(193, 321), pygame.Vector2(277, 357)))
        self._segments.append(Segment(pygame.Vector2(277, 357), pygame.Vector2(311, 464)))
        self._segments.append(Segment(pygame.Vector2(311, 464), pygame.Vector2(405, 496)))
        self._segments.append(Segment(pygame.Vector2(405, 496), pygame.Vector2(469, 484)))
        self._segments.append(Segment(pygame.Vector2(469, 484), pygame.Vector2(530, 505)))
        self._segments.append(Segment(pygame.Vector2(530, 505), pygame.Vector2(508, 551)))
        self._segments.append(Segment(pygame.Vector2(508, 551), pygame.Vector2(397, 566)))
        self._segments.append(Segment(pygame.Vector2(397, 566), pygame.Vector2(307, 545)))
        self._segments.append(Segment(pygame.Vector2(307, 545), pygame.Vector2(250, 554)))
        self._segments.append(Segment(pygame.Vector2(250, 554), pygame.Vector2(247, 646)))
        self._segments.append(Segment(pygame.Vector2(247, 646), pygame.Vector2(346, 662)))
        self._segments.append(Segment(pygame.Vector2(346, 662), pygame.Vector2(437, 636)))
        self._segments.append(Segment(pygame.Vector2(437, 636), pygame.Vector2(497, 648)))
        self._segments.append(Segment(pygame.Vector2(497, 648), pygame.Vector2(511, 682)))
        self._segments.append(Segment(pygame.Vector2(511, 682), pygame.Vector2(456, 717)))
        self._segments.append(Segment(pygame.Vector2(456, 717), pygame.Vector2(371, 720)))
        self._segments.append(Segment(pygame.Vector2(371, 720), pygame.Vector2(370, 772)))
        ####################
        self._segments.append(Segment(pygame.Vector2(709, 480), pygame.Vector2(752, 348)))
        self._segments.append(Segment(pygame.Vector2(752, 348), pygame.Vector2(762, 256)))
        self._segments.append(Segment(pygame.Vector2(762, 256), pygame.Vector2(671, 186)))
        self._segments.append(Segment(pygame.Vector2(671, 186), pygame.Vector2(727, 109)))
        self._segments.append(Segment(pygame.Vector2(727, 109), pygame.Vector2(782, 195)))
        self._segments.append(Segment(pygame.Vector2(782, 195), pygame.Vector2(830, 197)))
        self._segments.append(Segment(pygame.Vector2(830, 197), pygame.Vector2(825, 125)))
        self._segments.append(Segment(pygame.Vector2(825, 125), pygame.Vector2(882, 95)))
        self._segments.append(Segment(pygame.Vector2(882, 95), pygame.Vector2(901, 197)))
        self._segments.append(Segment(pygame.Vector2(901, 197), pygame.Vector2(866, 304)))
        self._segments.append(Segment(pygame.Vector2(866, 304), pygame.Vector2(800, 390)))
        self._segments.append(Segment(pygame.Vector2(800, 390), pygame.Vector2(709, 480)))
        ###################
        self._segments.append(Segment(pygame.Vector2(11, 835), pygame.Vector2(137, 988)))
        self._segments.append(Segment(pygame.Vector2(137, 988), pygame.Vector2(206, 963)))
        self._segments.append(Segment(pygame.Vector2(206, 963), pygame.Vector2(234, 1026)))
        self._segments.append(Segment(pygame.Vector2(234, 1026), pygame.Vector2(306, 1011)))
        self._segments.append(Segment(pygame.Vector2(306, 1011), pygame.Vector2(295, 897)))
        self._segments.append(Segment(pygame.Vector2(295, 897), pygame.Vector2(368, 1003)))
        self._segments.append(Segment(pygame.Vector2(368, 1003), pygame.Vector2(440, 981)))
        self._segments.append(Segment(pygame.Vector2(440, 981), pygame.Vector2(450, 844)))
        self._segments.append(Segment(pygame.Vector2(450, 844), pygame.Vector2(527, 824)))
        self._segments.append(Segment(pygame.Vector2(527, 824), pygame.Vector2(594, 863)))
        self._segments.append(Segment(pygame.Vector2(594, 863), pygame.Vector2(594, 912)))
        self._segments.append(Segment(pygame.Vector2(594, 912), pygame.Vector2(698, 909)))
        self._segments.append(Segment(pygame.Vector2(698, 909), pygame.Vector2(756, 815)))
        self._segments.append(Segment(pygame.Vector2(756, 815), pygame.Vector2(783, 685)))
        self._segments.append(Segment(pygame.Vector2(783, 685), pygame.Vector2(717, 674)))
        self._segments.append(Segment(pygame.Vector2(717, 674), pygame.Vector2(729, 635)))
        self._segments.append(Segment(pygame.Vector2(729, 635), pygame.Vector2(817, 653)))
        self._segments.append(Segment(pygame.Vector2(817, 653), pygame.Vector2(856, 613)))
        self._segments.append(Segment(pygame.Vector2(856, 613), pygame.Vector2(858, 544)))
        self._segments.append(Segment(pygame.Vector2(858, 544), pygame.Vector2(949, 584)))
        self._segments.append(Segment(pygame.Vector2(949, 584), pygame.Vector2(1024, 559)))
        self._segments.append(Segment(pygame.Vector2(1024, 559), pygame.Vector2(1030, 488)))
        self._segments.append(Segment(pygame.Vector2(1030, 488), pygame.Vector2(977, 506)))
        self._segments.append(Segment(pygame.Vector2(977, 506), pygame.Vector2(942, 396)))
        self._segments.append(Segment(pygame.Vector2(942, 396), pygame.Vector2(1032, 418)))
        self._segments.append(Segment(pygame.Vector2(1032, 418), pygame.Vector2(1054, 348)))
        self._segments.append(Segment(pygame.Vector2(1054, 348), pygame.Vector2(1011, 294)))
        self._segments.append(Segment(pygame.Vector2(1011, 294), pygame.Vector2(1070, 237)))

        self._segments.append(Segment(pygame.Vector2(1070, 237), pygame.Vector2(1134, 334)))
        self._segments.append(Segment(pygame.Vector2(1134, 334), pygame.Vector2(1158, 466)))
        self._segments.append(Segment(pygame.Vector2(1158, 466), pygame.Vector2(1257, 521)))
        self._segments.append(Segment(pygame.Vector2(1257, 521), pygame.Vector2(1375, 415)))
        self._segments.append(Segment(pygame.Vector2(1375, 415), pygame.Vector2(1419, 279)))
        self._segments.append(Segment(pygame.Vector2(1419, 279), pygame.Vector2(1319, 167)))
        self._segments.append(Segment(pygame.Vector2(1319, 167), pygame.Vector2(1206, 244)))
        self._segments.append(Segment(pygame.Vector2(1206, 244), pygame.Vector2(1154, 244)))
        self._segments.append(Segment(pygame.Vector2(1154, 244), pygame.Vector2(1148, 165)))
        self._segments.append(Segment(pygame.Vector2(1148, 165), pygame.Vector2(1049, 181)))
        self._segments.append(Segment(pygame.Vector2(1049, 181), pygame.Vector2(934, 30)))

        self._segments.append(Segment(pygame.Vector2(934, 30), pygame.Vector2(824, 58)))
        self._segments.append(Segment(pygame.Vector2(824, 58), pygame.Vector2(752, 25)))
        self._segments.append(Segment(pygame.Vector2(752, 25), pygame.Vector2(693, 60)))
        self._segments.append(Segment(pygame.Vector2(693, 60), pygame.Vector2(635, 9)))
        self._segments.append(Segment(pygame.Vector2(635, 9), pygame.Vector2(468, 16)))
        self._segments.append(Segment(pygame.Vector2(468, 16), pygame.Vector2(353, 4)))
        self._segments.append(Segment(pygame.Vector2(353, 4), pygame.Vector2(141, 8)))
        self._segments.append(Segment(pygame.Vector2(141, 8), pygame.Vector2(62, 30)))
        self._segments.append(Segment(pygame.Vector2(62, 30), pygame.Vector2(68, 87)))
        self._segments.append(Segment(pygame.Vector2(68, 87), pygame.Vector2(7, 45)))
        self._segments.append(Segment(pygame.Vector2(7, 45), pygame.Vector2(9, 94)))
        self._segments.append(Segment(pygame.Vector2(9, 94), pygame.Vector2(53, 157)))
        self._segments.append(Segment(pygame.Vector2(53, 157), pygame.Vector2(8, 278)))
        self._segments.append(Segment(pygame.Vector2(8, 278), pygame.Vector2(57, 408)))
        self._segments.append(Segment(pygame.Vector2(57, 408), pygame.Vector2(11, 835)))
        ###################
        self._segments.append(Segment(pygame.Vector2(1225, 385), pygame.Vector2(1237, 315)))
        self._segments.append(Segment(pygame.Vector2(1237, 315), pygame.Vector2(1324, 266)))
        self._segments.append(Segment(pygame.Vector2(1324, 266), pygame.Vector2(1320, 343)))
        self._segments.append(Segment(pygame.Vector2(1320, 343), pygame.Vector2(1282, 390)))
        self._segments.append(Segment(pygame.Vector2(1282, 390), pygame.Vector2(1225, 385)))
        ###################

        self.space.addlist(self.space, self._segments)
        for idx, s in enumerate(self._segments):
            s.idx = idx

    def load_image(self, name):
        return pygame.image.load(os.path.join("resources", name))

    def exit(self):
        logging.info("exit")
        sound.now_playing().fadeout()
        pygame.mouse.set_visible(True)
        for idx, p in enumerate(self.game_data.pictures):
            name = f"{p.time_stamp}-{idx}.png"
            pygame.image.save(p.picture, os.path.join("pictures", name))

    def pause(self):
        logging.info("pause")

    def resume(self):
        logging.info("resume")
        self.enter()

    def _elapsed(self):
        self._draw_clock.tick()
        self.draw(pygame.display.get_surface())

    def run(self):
        # cmd = self.update(0)
        # while not cmd:
        #     cmd = self.update(0)
        #     self.draw(self._screen_provider.get_surface())
        # return cmd
        self._accumulator.call_back = self.update
        self._timer.elapsed = self._elapsed
        cmd = None
        while not cmd:
            raw_dt_s = self._clock.tick() / 1000
            cmd = self._accumulator.update(raw_dt_s)
            self._timer.update(self._accumulator.game_time_s)
            if cmd:
                return cmd

        # if raw_dt_s > settings.max_raw_dt_s:
        #     raw_dt_s = settings.max_raw_dt_s
        # accumulator += raw_dt_s
        # while accumulator > update_dt and scene:
        #     cmd = scene.update(update_dt)
        #     if cmd:
        #         scene = stack.update(cmd)
        #     game_time += update_dt
        #     accumulator -= update_dt
        #
        # if next_draw < game_time and scene:
        #     scene.draw(screen)
        #     next_draw = game_time + draw_dt

    def update(self, dt_s, game_time_s):
        self._update_clock.tick()
        sound.roll_songs()
        self.player.update(dt_s)
        self.collide(self._segments)
        next_scene = self._handle_events()
        return next_scene

    def collide(self, segments):
        if not featureswitches.collision_detection_on:
            return
        if featureswitches.use_space_z:
            side_length = self.player.radius * 3
            r = pygame.Rect(0, 0, side_length, side_length)
            r.center = self.player.position
            segments = self.space.entities_in_rect(self.space, r)
        contacts = []
        # for s in segments:
        #     logger.warning(f"{self.player.position - self.player.prev_position}!!!!!")
        #     player_pos = self.player.position - s.normal * self.player.radius
        #     player_prev_pos = self.player.prev_position - s.normal * self.player.radius
        #     p0_to_p = player_pos - s.p0
        #     p0_to_prev_p = player_prev_pos - s.p0
        #     p0_to_p_dot_n = p0_to_p.dot(s.normal)
        #     p0_to_prev_p_dot_n = p0_to_prev_p.dot(s.normal)
        #     if p0_to_p_dot_n > 0 and p0_to_prev_p_dot_n > 0:
        #         # both are on the good side
        #         logger.warning("good side!")
        #         continue
        #     p0_to_p = self.player.position - s.p0
        #     p1_to_p = self.player.position - s.p1
        #     front_p0 = s.direction.dot(p0_to_p)
        #     if (front_p0 > 0) == (s.direction.dot(p1_to_p) > 0):
        #         # outside of segment, check distance to points
        #         if p0_to_p.dot(s.normal) <= 0:  # behind ignore
        #             logger.warning("out segment, behind!")
        #             continue
        #         # check which point
        #         radius_sq = self.player.radius ** 2
        #         if front_p0 > 0:
        #             # check p1
        #             if p1_to_p.length_squared() > radius_sq:
        #                 continue
        #             # move_dir = player_pos - player_prev_pos
        #             # if move_dir.length_squared() > 0:
        #             #     move_n = move_dir.project(s.normal)
        #             #     move_t = move_dir - move_n
        #             #     self.player.collision_displacement += move_t - move_n.normalize() * (self.player.radius - p1_to_p.length())
        #
        #             # collision with circle region!
        #             n = p1_to_p
        #             contacts.append((s, n, 0))
        #             # p_new = s.p1 + self.player.radius * n
        #             # self.player.collision_displacement += p_new - self.player.position
        #             # logger.error(f"r {self.player.radius} ppos {self.player.position} prev {self.player.prev_position}: segment: {s.p0} -> {s.p1}  n {s.normal} disp: {p_new - self.player.position}")
        #             continue
        #         else:
        #             if p0_to_p.length_squared() > radius_sq:
        #                 continue
        #             # move_dir = player_pos - player_prev_pos
        #             # if move_dir.length_squared() > 0:
        #             #     move_n = move_dir.project(s.normal)
        #             #     move_t = move_dir - move_n
        #             #     if move_n.length_squared():
        #             #         move_n.normalize()
        #             #     self.player.collision_displacement += move_t - move_n * (self.player.radius - p0_to_p.length())
        #
        #             # collision with circle region!
        #             n = p0_to_p
        #             contacts.append((s, n, 0))
        #             # p_new = s.p0 + self.player.radius * n
        #             # self.player.collision_displacement += p_new - self.player.position
        #             # logger.error(f"r {self.player.radius} ppos {self.player.position} prev {self.player.prev_position}: segment: {s.p0} -> {s.p1}  n {s.normal} disp: {p_new - self.player.position}")
        #             continue
        #     else:
        #         if p0_to_p_dot_n < 0 < p0_to_prev_p_dot_n:
        #             # intersection
        #             contacts.append((s, s.normal, 1))
        #             # move_dir = player_pos - player_prev_pos
        #             # move_n = move_dir.project(s.normal)
        #             # move_t = move_dir.project(s.direction)
        #             # logger.error(
        #             #     f"r {self.player.radius} ppos {self.player.position} prev {self.player.prev_position}: segment: {s.p0} -> {s.p1}  n {s.normal} disp n: {move_n}, t: {move_t} : {move_t-move_n}")
        #             # self.player.collision_displacement += move_t - move_n
        #         else:
        #             logger.warning("no segment intersection!")

        # logger.error(f"self.player.collision_displacement: {self.player.collision_displacement}")
        # self.player.position += self.player.collision_displacement
        # self.player.collision_displacement.update(0, 0)

        player_radius_sq = self.player.radius ** 2
        for segment in segments:
            move_dir = self.player.position - self.player.prev_position
            # if move_dir.dot(s.normal) > 0:
            #     # moving away from collision side of segment
            #     continue

            r0 = self.player.position - segment.p0
            if r0.dot(segment.normal) >= 0 or (
                    r0.dot(segment.normal) < 0 <= (self.player.prev_position - segment.p0).dot(segment.normal)):
                if r0.dot(segment.direction) < 0:
                    # outside of segment at p0 side
                    r0_len_sq = r0.length_squared()
                    if r0_len_sq < player_radius_sq:
                        contacts.append((segment, r0, 0))
                    continue
                else:
                    r1 = self.player.position - segment.p1
                    if r1.dot(segment.direction) > 0:
                        # outside of segment at p1 side
                        r1_len_sq = r1.length_squared()
                        if r1_len_sq < player_radius_sq:
                            contacts.append((segment, r1, 0))
                        continue
                    else:
                        # segment region
                        dist_s_sq = r0.cross(segment.direction) ** 2 / segment.direction.length_squared()
                        if dist_s_sq < player_radius_sq:
                            contacts.append((segment, segment.normal, 1))

        if contacts:
            logger.debug(f"CONTACTS! {len(contacts)}")
            axis = sum((_n.normalize() for _, _n, _ in contacts), pygame.Vector2(0, 0))
            axis.normalize()
            # move_dir = self.player.position - self.player.prev_position
            depth = 0
            for segment, n, contact_type in contacts:
                if contact_type == 0:
                    # circle to point collision
                    d = (self.player.radius - n.length()) / axis.dot(n.normalize())
                    if d > depth:
                        depth = d
                else:
                    # circle to segment collision
                    d = (self.player.position - segment.p0 - self.player.radius * n).cross(segment.direction) / (
                            segment.direction.length() * axis.dot(n))
                    if d < 0:
                        d = -d
                    if d > depth:
                        depth = d

            pos = self.player.position.copy()
            # self.player.position += axis * depth
            self.player.axis.update(axis)
            self.player.move(axis * depth)
            logger.debug(f"correct {pos} -> {self.player.position}")

    def draw(self, screen: pygame.Surface, fill_color=(255, 0, 255), do_flip=True):
        with self._cam.clip(screen):
            if fill_color:
                screen.fill(fill_color)

            # self._cam.look_at(self.player.position)
            if featureswitches.lerp_cam_on:
                self._cam.lerp(self.player.position, settings.draw_dt_s)
            else:
                self._cam.look_at(self.player.position.copy())

            # background
            screen.blit(self.background_img, self._cam.to_screen(pygame.Vector2()))
            screen_cove_entry_position = self._cam.to_screen(pygame.Vector2(settings.cove_entry))
            screen.blit(self.entry_img, screen_cove_entry_position)

            if featureswitches.show_checker_board:
                view_bb = pygame.Rect(0, 0, 40, 40)
                for x in range(10):
                    for y in range(10):
                        if (x + y) % 2:
                            screen.fill((200, 200, 200),
                                        view_bb.move(self._cam.to_screen(pygame.Vector2(x * view_bb.w, y * view_bb.h))))

            # draw player
            pygame.draw.circle(screen, (255, 0, 0), self._cam.to_screen(self.player.position), 5)
            pygame.draw.circle(screen, (0, 255, 0), self._cam.to_screen(self.player.position),
                               self.player.radius, 1)

            size = 2
            view_bb = pygame.Rect(0, 0, self._player_view_radius * size, self._player_view_radius * size)
            view_bb.center = self.player.light_position
            segments_culled_by_spacez = self._segments

            if featureswitches.use_space_z:
                segments_culled_by_spacez = self.space.entities_in_rect(self.space, view_bb)

            view_segments = []
            if featureswitches.preprocess_view_segments:
                # collection of points on the view bounding box, need to split those to segments too
                sides = {1: [pygame.Vector2(view_bb.topright), pygame.Vector2(view_bb.topleft)],  # top
                         2: [pygame.Vector2(view_bb.topleft), pygame.Vector2(view_bb.bottomleft)],  # left
                         3: [pygame.Vector2(view_bb.bottomleft), pygame.Vector2(view_bb.bottomright)],  # bottom
                         4: [pygame.Vector2(view_bb.bottomright), pygame.Vector2(view_bb.topright)],  # right
                         None: []}
                for s in segments_culled_by_spacez:
                    hit, xx, xy, yx, yy, s1, s2 = clip_segment_against_aabb(s.p0.x, s.p0.y, s.p1.x, s.p1.y,
                                                                            view_bb.left,
                                                                            view_bb.top, view_bb.right, view_bb.bottom)
                    xx = int(xx)
                    xy = int(xy)
                    yx = int(yx)
                    yy = int(yy)
                    if hit and not (xx == yx and xy == yy):
                        sides[s1].append(pygame.Vector2(xx, xy))
                        sides[s2].append(pygame.Vector2(yx, yy))
                        view_segments.append(Segment(pygame.Vector2(xx, xy), pygame.Vector2(yx, yy), is_wall=True))
                    # else:
                    #     view_segments.append(s)

                # construct view bounding box segments
                sides[1].sort(key=lambda v: -v.x)
                sides[3].sort(key=lambda v: v.x)
                sides[2].sort(key=lambda v: v.y)
                sides[4].sort(key=lambda v: -v.y)
                for ps in [sides[1], sides[2], sides[3], sides[4]]:
                    for idx, p in enumerate(ps[:-1]):
                        pn = ps[idx + 1]
                        if p.x != pn.x or p.y != pn.y:
                            segment = Segment(p, pn, is_wall=False)
                            view_segments.append(segment)
                        else:
                            logger.debug(f"{p} == {pn}")
            else:
                view_segments = list(segments_culled_by_spacez)

            shadow_color = (0, 0, 0)  # (50, 50, 50)
            lighted_color = (255, 255, 255)
            # if True or featureswitches.normal_illumination:
            #     shadow_color = (0, 0, 0)  # (50, 50, 50)
            #     lighted_color = (255, 255, 255)
            # else:
            #     shadow_color = (255, 255, 255)
            #     lighted_color = (120, 120, 120)

            # logging.info("=========== 0")

            # logging.info("=========== 1")
            light_surf = screen.copy()
            light_surf.fill(shadow_color)

            # todo introduce a clip figure surface?

            if featureswitches.use_light_volume:
                try:
                    if featureswitches.use_light_volume_3_vs_4:
                        intersections: list[Intersection] = ViewPolygon.get_visible_volume3(view_segments,
                                                                                            self.player.light_position)
                    else:
                        intersections: list[Intersection] = ViewPolygon.get_visible_volume4(view_segments,
                                                                                            self.player.light_position)
                    # logger.debug("intersections: %s", [_p.point for _p in intersections])
                except Exception as ex:
                    logger.error(f"light pos: {self.player.light_position}, {ex}")
                    intersections = []
                    # raise

                # if self._player_view_radius_on:
                #     for i in intersections:
                #         # u = t * d.length()
                #         u = i.t1 * i.ray.direction.length()
                #         if u > self._player_view_radius:
                #             t = self._player_view_radius / i.ray.direction.length()
                #             i.t1 = t
                #             i.point = i.ray.p + i.ray.direction * t

                if featureswitches.use_expanded_poly:
                    poly_points = []
                    for s in intersections:
                        if featureswitches.use_expand_poly_exp:
                            world_pos = (s.point - self.player.position) * (1.0 + (math.e ** (
                                    -s.point.distance_to(self.player.position) * 0.02))) + self.player.position
                        else:
                            world_pos = (
                                                s.point - self.player.position) * settings.poly_expand_factor + self.player.position
                        poly_points.append(self._cam.to_screen(world_pos))
                else:
                    poly_points = [self._cam.to_screen(_i.point) for _i in intersections]

                if poly_points and len(poly_points) > 1:
                    pygame.draw.polygon(light_surf, lighted_color, poly_points)

            else:
                intersections = []
                wc = pygame.Vector2(view_bb.center)
                sc = self._cam.to_screen(wc)
                pygame.draw.rect(light_surf, lighted_color, view_bb.move(*round(sc - wc)))

            if featureswitches.soft_shadows_on:
                light_surf2 = screen.copy()
                light_surf2.fill(shadow_color)

                soft_shadow_color = (64,) * 3
                shadow_light_dist = 4  # make sure this is smaller than the player radius, peek behind 'walls' otherwise
                try:
                    pos = self.player.light_position + self.player.look_direction.rotate(90) * shadow_light_dist
                    if featureswitches.use_light_volume_3_vs_4:
                        shadow_intersections: list[Intersection] = ViewPolygon.get_visible_volume3(view_segments, pos)
                    else:
                        shadow_intersections: list[Intersection] = ViewPolygon.get_visible_volume4(view_segments, pos)
                    pygame.draw.polygon(light_surf2, soft_shadow_color,
                                        [self._cam.to_screen(_i.point) for _i in shadow_intersections])
                    light_surf.blit(light_surf2, (0, 0), None, pygame.BLEND_RGB_ADD)
                    light_surf2.fill(shadow_color)
                except Exception as ex:
                    logger.error(f"light pos: {self.player.light_position}, {ex}")
                    raise
                try:
                    pos = self.player.light_position + self.player.look_direction.rotate(90) * shadow_light_dist * 2
                    if featureswitches.use_light_volume_3_vs_4:
                        shadow_intersections: list[Intersection] = ViewPolygon.get_visible_volume3(view_segments, pos)
                    else:
                        shadow_intersections: list[Intersection] = ViewPolygon.get_visible_volume4(view_segments, pos)
                    pygame.draw.polygon(light_surf2, soft_shadow_color,
                                        [self._cam.to_screen(_i.point) for _i in shadow_intersections])
                    light_surf.blit(light_surf2, (0, 0), None, pygame.BLEND_RGB_ADD)
                    light_surf2.fill(shadow_color)
                except Exception as ex:
                    logger.error(f"light pos: {self.player.light_position}, {ex}")
                    raise
                try:
                    pos = self.player.light_position + self.player.look_direction.rotate(-90) * shadow_light_dist
                    if featureswitches.use_light_volume_3_vs_4:
                        shadow_intersections: list[Intersection] = ViewPolygon.get_visible_volume3(view_segments, pos)
                    else:
                        shadow_intersections: list[Intersection] = ViewPolygon.get_visible_volume4(view_segments, pos)

                    pygame.draw.polygon(light_surf2, soft_shadow_color,
                                        [self._cam.to_screen(_i.point) for _i in shadow_intersections])
                    light_surf.blit(light_surf2, (0, 0), None, pygame.BLEND_RGB_ADD)
                    light_surf2.fill(shadow_color)
                except Exception as ex:
                    logger.error(f"light pos: {self.player.light_position}, {ex}")
                    raise
                try:
                    pos = self.player.light_position + self.player.look_direction.rotate(-90) * shadow_light_dist * 2
                    if featureswitches.use_light_volume_3_vs_4:
                        shadow_intersections: list[Intersection] = ViewPolygon.get_visible_volume3(view_segments, pos)
                    else:
                        shadow_intersections: list[Intersection] = ViewPolygon.get_visible_volume4(view_segments, pos)
                    pygame.draw.polygon(light_surf2, soft_shadow_color,
                                        [self._cam.to_screen(_i.point) for _i in shadow_intersections])
                    light_surf.blit(light_surf2, (0, 0), None, pygame.BLEND_RGB_ADD)
                    light_surf2.fill(shadow_color)
                except Exception as ex:
                    logger.error(f"light pos: {self.player.light_position}, {ex}")
                    raise

            circle_surf = screen.copy()
            circle_surf.fill(shadow_color)

            view_bb.center = self._cam.to_screen(self.player.light_position)
            if featureswitches.player_view_radius_on:
                # pygame.draw.circle(circle_surf, lighted_color, self._cam.to_screen(self.player.position),
                #                    self._player_view_radius, 0)
                look_dir = self.player.look_direction
                look_angle = -math.atan2(look_dir.y, look_dir.x)
                half = math.pi / 8  # 45° / 2
                pygame.draw.arc(circle_surf, lighted_color, view_bb, look_angle - half, look_angle + half,
                                self._player_view_radius * size)
                light_surf.blit(circle_surf, (0, 0), None, pygame.BLEND_RGB_MULT)

            if featureswitches.normal_illumination:
                lr = self.light_cone_img.get_rect()
                lr_off = pygame.Vector2(lr.centerx - 53, 0)
                angle = self.player.look_direction.angle_to(pygame.Vector2(1, 0))
                rot_img = pygame.transform.rotozoom(self.light_cone_img, angle, 1.0)
                lr_off.rotate_ip(-angle)
                lr = rot_img.get_rect(center=self._cam.to_screen(self.player.light_position))
                lr.move_ip(lr_off.x, lr_off.y)
                circle_surf.blit(rot_img, lr)  # , None, pygame.BLEND_RGB_MULT)

                rot_img2 = rot_img.copy()
                rot_img2.fill((255, 255, 255, 64), None, pygame.BLEND_RGBA_MULT)
                screen.blit(rot_img2, lr, None, pygame.BLEND_RGBA_ADD)

                light_surf.blit(circle_surf, (0, 0), None, pygame.BLEND_RGB_MULT)

            if featureswitches.use_light_reflection:
                # int_segments = set([_i.segment for _i in intersections if _i.segment.normal.dot(self.player.look_direction) < 0])
                reflections = []
                for i in intersections:
                    if not i.segment.is_wall:
                        continue
                    # if i.segment.normal.dot(self.player.look_direction) >= 0:
                    #     continue

                    angle0 = self.player.look_direction.dot((i.segment.p0 - self.player.light_position).normalize())
                    angle1 = self.player.look_direction.dot((i.segment.p1 - self.player.light_position).normalize())
                    if angle0 < self.player.view_angle and angle1 < self.player.view_angle:
                        continue

                    # direction0 = self.player.look_direction
                    direction0 = ((i.segment.p0 + i.segment.p1) / 2 - self.player.light_position).normalize()
                    reflected0 = direction0.reflect(i.segment.normal)
                    reflections.append((i, reflected0, angle0 if angle0 > angle1 else angle1))

                    if featureswitches.debug_render:
                        pygame.draw.line(screen, (200, 100, 100), self._cam.to_screen(i.point),
                                         self._cam.to_screen(i.point + reflected0 * 30), 7)

                if len(reflections) > 1 and reflections[0][0].segment != reflections[1][0].segment:
                    reflections = reflections[1:] + reflections[0:1]
                ref_looped = reflections + reflections[:2]
                for idx in range(0, len(reflections), 2):
                    r1, r2 = ref_looped[idx:idx + 2]

                    light_factor = 1.0
                    # if featureswitches.debug_render:
                    angle_factor = r1[2] if r1[2] < r2[2] else r2[2]
                    angle_factor = (angle_factor - self.player.view_angle) / (1 - self.player.view_angle)
                    color = [int(c * angle_factor * light_factor) for c in lighted_color]

                    points = [
                        self._cam.to_screen(r1[0].point),
                        self._cam.to_screen(r2[0].point),
                        self._cam.to_screen(r2[0].point + r2[1] * 30),
                        self._cam.to_screen(r1[0].point + r1[1] * 30)
                    ]
                    # pygame.draw.polygon(light_surf, color, [self._cam.to_screen(_p) for _p in points])
                    pygame.draw.polygon(light_surf, color, points)

                    # light_surf.blit(circle_surf, (0, 0), None, pygame.BLEND_RGB_MULT)

                    # s = i.segment
                    # # direction0 = s.p0 - self.player.light_position
                    # direction0 = self.player.look_direction
                    # reflected0 = direction0.normalize().reflect(s.normal)
                    # # direction1 = s.p1 - self.player.light_position
                    # direction1 = self.player.look_direction
                    # reflected1 = direction1.normalize().reflect(s.normal)
                    # points = [self._cam.to_screen(s.p0), self._cam.to_screen(s.p0 + reflected0 * 30),
                    #           self._cam.to_screen(s.p1 + reflected1 * 30), self._cam.to_screen(s.p1)]
                    # if featureswitches.debug_render:
                    #     pygame.draw.polygon(screen, (255, 255, 255, 128), points, 0)
                    #     pygame.draw.line(screen, (200, 100, 100), self._cam.to_screen(s.p0), self._cam.to_screen(s.p0 + reflected0*30), 7)
                    #     pygame.draw.line(screen, (200, 100, 100), self._cam.to_screen(s.p1), self._cam.to_screen(s.p1 + reflected1*30), 7)

            # logging.info("=========== 2")
            if featureswitches.show_segments:
                for s in self._segments:
                    pygame.draw.line(screen, (0, 0, 0), self._cam.to_screen(s.p0), self._cam.to_screen(s.p1), 5)

            # logging.info("=========== 3")
            if featureswitches.debug_render:
                for i_idx, i in enumerate(intersections):
                    pygame.draw.circle(screen, (255, 0, 0), self._cam.to_screen(i.point), 5)
                    player_on_screen = self._cam.to_screen(self.player.position)
                    point_on_screen = self._cam.to_screen_translation + self._cam.ppu * (
                            self.player.position + (i.point - self.player.position))
                    color = (0, 255, 255) if i_idx == 0 else "greenyellow"
                    pygame.draw.line(screen, color, player_on_screen, point_on_screen, 5 if i_idx == 0 else 1)
                    label = self.font.render(str(i_idx) + "/" + str(i.ray.id), True, "yellow")
                    screen.blit(label, (point_on_screen + player_on_screen) / 2)
                    # logging.info(i.point, i)

                for idx, s in enumerate(self._segments):
                    # draw normals
                    start = (s.p0 + s.p1) * 0.5
                    pygame.draw.line(screen, (255, 0, 0), self._cam.to_screen(start),
                                     self._cam.to_screen(start + 20 * s.normal), 3)
                    # draw direction
                    pygame.draw.line(screen, (0, 255, 0), self._cam.to_screen(s.p0),
                                     self._cam.to_screen(start), 2)

                if featureswitches.use_space_z:
                    for s in segments_culled_by_spacez:
                        # draw normals
                        # start = (s.p0 + s.p1) * 0.5

                        # draw direction
                        pygame.draw.line(screen, (255, 255, 255), self._cam.to_screen(s.p0),
                                         self._cam.to_screen(s.p1), 4)

                for s in view_segments:
                    # draw normals
                    start = (s.p0 + s.p1) * 0.5
                    pygame.draw.line(screen, (255, 0, 0), self._cam.to_screen(start),
                                     self._cam.to_screen(start + 20 * s.normal), 3)
                    # draw direction
                    pygame.draw.line(screen, (0, 255, 0), self._cam.to_screen(s.p0),
                                     self._cam.to_screen(start), 2)

                # draw expanded poly
                if intersections and len(intersections) > 1:
                    pygame.draw.polygon(light_surf, (0, 255, 255),
                                        [self._cam.to_screen(
                                            (
                                                    _i.point - self.player.position) * settings.poly_expand_factor + self.player.position)
                                            for _i in
                                            intersections], 2)
                    pygame.draw.polygon(light_surf, (255, 255, 255),
                                        [self._cam.to_screen(_i.point) for _i in intersections], 2)

                debug_size = 50
                pygame.draw.line(screen, "yellow", self._cam.to_screen(self.player.position),
                                 self._cam.to_screen(self.player.position + self.player.axis * debug_size), 2)
                pygame.draw.line(screen, "green", self._cam.to_screen(self.player.position), self._cam.to_screen(
                    self.player.position + (self.player.position - self.player.prev_position) * debug_size), 2)

                pygame.draw.line(screen, "greenyellow", self._cam.to_screen(self.player.position),
                                 pygame.mouse.get_pos(), 2)

            if featureswitches.show_vertex_coord:
                for idx, s in enumerate(self._segments):
                    start = (s.p0 + s.p1) * 0.5
                    # draw index to identify the segment
                    label = self.font.render(str(idx), True, "white")
                    screen.blit(label, self._cam.to_screen(start))
                    label = self.font.render(str(s.p0), True, "white")
                    screen.blit(label, self._cam.to_screen(s.p0))
                    label = self.font.render(str(s.p1), True, "white")
                    screen.blit(label, self._cam.to_screen(s.p1))

            # logging.info("=========== 4")

            if featureswitches.show_entry_light:
                dist_to_start = settings.player_start.distance_to(self.player.position)
                visibility_dist = self._player_view_radius
                p_pos = self.player.position
                if dist_to_start <= visibility_dist and settings.cove_entry.x < p_pos.x \
                        and settings.cove_entry.y < p_pos.y < settings.cove_entry.y + 200:
                    with clip_resource(light_surf, view_bb):
                        light_surf.blit(self.entrylight_img,
                                        screen_cove_entry_position)  # , None, pygame.BLEND_RGB_ADD)
                        t = dist_to_start / visibility_dist
                        val = 255 * (1 - t)
                        light_surf.fill((val, val, val), self.entry_img.get_rect(topleft=screen_cove_entry_position),
                                        pygame.BLEND_RGB_MULT)

            if not featureswitches.normal_illumination:
                temp = screen.copy()
                temp.fill((255, 255, 255, 255))
                temp.blit(light_surf, (0, 0), None, pygame.BLEND_RGBA_SUB)
                temp.fill((90, 90, 90), None, pygame.BLEND_RGBA_ADD)
                light_surf = temp

            screen.blit(light_surf, (0, 0), None, pygame.BLEND_RGB_MULT)

            cr = self.cursor_img.get_rect(center=pygame.mouse.get_pos())
            screen.blit(self.cursor_img, cr)
            if featureswitches.show_fps:
                clock_fps = round(self._clock.get_fps(), 1)
                draw_fps = round(self._draw_clock.get_fps(), 1)
                update_fps = round(self._update_clock.get_fps(), 1)
                labels = [
                    self.font.render(f"pps: {clock_fps}", True, "white"),
                    self.font.render(f"fps: {draw_fps}", True, "white"),
                    self.font.render(f"ups: {update_fps}", True, "white"),
                    self.font.render(f"pos: {self.player.position}/{self._cam.to_screen(self.player.position)}", True,
                                     "white"),
                    self.font.render(f"11 debug_render: {featureswitches.debug_render}", True, "white"),
                    self.font.render(f"12 player_view_radius_on: {featureswitches.player_view_radius_on}", True,
                                     "white"),
                    self.font.render(f"13 normal_illumination: {featureswitches.normal_illumination}", True, "white"),
                    self.font.render(f"14 use_expanded_poly: {featureswitches.use_expanded_poly}", True, "white"),
                    self.font.render(f"15 show_segments: {featureswitches.show_segments}", True, "white"),
                    self.font.render(f"16 super_speed: {featureswitches.super_speed}", True, "white"),
                    self.font.render(f"17 show_checker_board: {featureswitches.show_checker_board}", True, "white"),
                    self.font.render(f"18 record_points: {featureswitches.record_points}", True, "white"),
                    self.font.render(f"19 lerp_cam_on: {featureswitches.lerp_cam_on}", True, "white"),
                    self.font.render(f"20 soft_shadows_on: {featureswitches.soft_shadows_on}", True, "white"),
                    self.font.render(f"21 collision_detection_on: {featureswitches.collision_detection_on}", True,
                                     "white"),
                    self.font.render(f"22 show_fps: {featureswitches.show_fps}", True, "white"),
                    self.font.render(f"23 use_light_volume: {featureswitches.use_light_volume}", True, "white"),
                    self.font.render(f"24 use_light_volume_3_vs_4: {featureswitches.use_light_volume_3_vs_4}", True,
                                     "white"),
                    self.font.render(f"25 preprocess_view_segments: {featureswitches.preprocess_view_segments}", True,
                                     "white"),
                    self.font.render(f"26 use_space_z: {featureswitches.use_space_z}", True, "white"),
                    self.font.render(f"27 use_expand_poly_exp: {featureswitches.use_expand_poly_exp}", True, "white"),
                    self.font.render(f"28 use_light_reflection: {featureswitches.use_light_reflection}", True, "white"),
                    self.font.render(f"29 show_entry_light: {featureswitches.show_entry_light}", True, "white"),
                    self.font.render(f"30 show_vertex_coord: {featureswitches.show_vertex_coord}", True, "white"),
                    self.font.render(
                        f"31 move_player_to_debug_positions: {self.debug_pos_idx}/{len(self.debug_positions)-1} ({str(self.debug_positions[self.debug_pos_idx])})",
                        True, "white"),
                    self.font.render(f"32 use_new_light_engine: {featureswitches.use_new_light_engine}", True, "white"),
                ]
                logger.info("draw time: %s : prev: %s : raw: %s : ticks: %s", self._accumulator.game_time_s,
                            self._clock.get_time(), self._clock.get_rawtime(), pygame.time.get_ticks() / 1000)
                r = labels[0].get_rect()
                for l in labels:
                    screen.blit(l, r)
                    r.move_ip(0, r.h)

            if do_flip:
                pygame.display.flip()

    def _handle_events(self, ):
        next_scene = None
        # for event in [self._event_provider.wait()]:
        for event in self._event_provider.get():
            if event.type == pygame.QUIT:
                next_scene = SceneManager.PopCmd(100)
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    next_scene = SceneManager.PopCmd(transition=FadeOutAndIn(pygame.display, pygame.time.Clock()))
                if event.scancode == 53:
                    self._key_seq.clear()
                if event.key in (pygame.K_0,
                                 pygame.K_1,
                                 pygame.K_2,
                                 pygame.K_3,
                                 pygame.K_4,
                                 pygame.K_5,
                                 pygame.K_6,
                                 pygame.K_7,
                                 pygame.K_8,
                                 pygame.K_9,
                                 ):
                    self._key_seq.append(event.key)
                    key = tuple(self._key_seq)
                    if key == (pygame.K_1, pygame.K_1,):
                        featureswitches.debug_render = not featureswitches.debug_render
                        logger.warning("featureswitches.debug_render: %s", featureswitches.debug_render)
                    elif key == (pygame.K_1, pygame.K_2,):
                        featureswitches.player_view_radius_on = not featureswitches.player_view_radius_on
                        logger.warning("featureswitches.player_view_radius_on: %s",
                                       featureswitches.player_view_radius_on)
                    elif key == (pygame.K_1, pygame.K_3,):
                        featureswitches.normal_illumination = not featureswitches.normal_illumination
                        logger.warning("featureswitches.normal_illumination: %s", featureswitches.normal_illumination)
                    elif key == (pygame.K_1, pygame.K_4,):
                        featureswitches.use_expanded_poly = not featureswitches.use_expanded_poly
                        logger.warning("featureswitches.use_expanded_poly: %s", featureswitches.use_expanded_poly)
                    elif key == (pygame.K_1, pygame.K_5,):
                        featureswitches.show_segments = not featureswitches.show_segments
                        logger.warning("featureswitches.show_segments: %s", featureswitches.show_segments)
                    elif key == (pygame.K_1, pygame.K_6,):
                        featureswitches.super_speed = not featureswitches.super_speed
                        logger.warning("featureswitches.super_speed: %s", featureswitches.super_speed)
                        if featureswitches.super_speed:
                            self.player.speed *= 20
                        else:
                            self.player.speed /= 20
                    elif key == (pygame.K_1, pygame.K_7,):
                        featureswitches.show_checker_board = not featureswitches.show_checker_board
                        logger.warning("featureswitches.show_checker_board: %s", featureswitches.show_checker_board)
                    elif key == (pygame.K_1, pygame.K_8,):
                        featureswitches.record_points = not featureswitches.record_points
                        logger.warning("featureswitches.record_points: %s", featureswitches.record_points)
                        logger.warning(f"record_points: {featureswitches.record_points}")
                        level = logging.WARNING if featureswitches.record_points else logging.DEBUG
                        logging.getLogger().setLevel(level)
                    elif key == (pygame.K_1, pygame.K_9,):
                        featureswitches.lerp_cam_on = not featureswitches.lerp_cam_on
                        logger.warning("featureswitches.lerp_cam_on: %s", featureswitches.lerp_cam_on)
                    elif key == (pygame.K_2, pygame.K_0,):
                        featureswitches.soft_shadows_on = not featureswitches.soft_shadows_on
                        logger.warning("featureswitches.soft_shadows_on: %s", featureswitches.soft_shadows_on)
                    elif key == (pygame.K_2, pygame.K_1,):
                        featureswitches.collision_detection_on = not featureswitches.collision_detection_on
                        logger.warning("featureswitches.collision_detection_on: %s",
                                       featureswitches.collision_detection_on)
                    elif key == (pygame.K_2, pygame.K_2,):
                        featureswitches.show_fps = not featureswitches.show_fps
                        logger.warning("featureswitches.show_fps: %s", featureswitches.show_fps)
                    elif key == (pygame.K_2, pygame.K_3,):
                        featureswitches.use_light_volume = not featureswitches.use_light_volume
                        logger.warning("featureswitches.use_light_volume: %s", featureswitches.use_light_volume)
                    elif key == (pygame.K_2, pygame.K_4,):
                        featureswitches.use_light_volume_3_vs_4 = not featureswitches.use_light_volume_3_vs_4
                        extra = "(using V3)" if featureswitches.use_light_volume_3_vs_4 else "(using V4)"
                        logger.warning("featureswitches.use_light_volume_3_vs_4: %s %s",
                                       featureswitches.use_light_volume_3_vs_4, extra)
                    elif key == (pygame.K_2, pygame.K_5,):
                        featureswitches.preprocess_view_segments = not featureswitches.preprocess_view_segments
                        logger.warning("featureswitches.preprocess_view_segments: %s",
                                       featureswitches.preprocess_view_segments)
                    elif key == (pygame.K_2, pygame.K_6,):
                        featureswitches.use_space_z = not featureswitches.use_space_z
                        logger.warning("featureswitches.use_space_z: %s", featureswitches.use_space_z)
                    elif key == (pygame.K_2, pygame.K_7,):
                        featureswitches.use_expand_poly_exp = not featureswitches.use_expand_poly_exp
                        logger.warning("featureswitches.use_expand_poly_exp: %s", featureswitches.use_expand_poly_exp)
                    elif key == (pygame.K_2, pygame.K_8,):
                        featureswitches.use_light_reflection = not featureswitches.use_light_reflection
                        logger.warning("featureswitches.use_light_reflection: %s", featureswitches.use_light_reflection)
                    elif key == (pygame.K_2, pygame.K_9,):
                        featureswitches.show_entry_light = not featureswitches.show_entry_light
                        logger.warning("featureswitches.show_entry_light: %s", featureswitches.show_entry_light)
                    elif key == (pygame.K_3, pygame.K_0,):
                        featureswitches.show_vertex_coord = not featureswitches.show_vertex_coord
                        logger.warning("featureswitches.show_vertex_coord: %s", featureswitches.show_vertex_coord)
                    elif key == (pygame.K_3, pygame.K_1,):
                        self.debug_pos_idx = (self.debug_pos_idx + 1) % len(self.debug_positions)
                        pos = self.debug_positions[self.debug_pos_idx]
                        self.player.set_to_position(pygame.Vector2(pos))
                        logger.warning("featureswitches.move_player_to_debug_positions: %s, %s", self.debug_pos_idx,
                                       pos)
                    elif key == (pygame.K_3, pygame.K_2,):
                        featureswitches.use_new_light_engine = not featureswitches.use_new_light_engine
                        logger.warning("featureswitches.use_new_light_engine: %s", featureswitches.use_new_light_engine)

                    if len(key) == 2:
                        self._key_seq.clear()

            elif event.type == pygame.MOUSEMOTION:
                screen_mouse_pos = pygame.Vector2(event.pos)
                self.player.loot_at.update(self._cam.to_world(screen_mouse_pos))
                # self.player.position.update(mx, my)
                # logger.debug("player look at %s", self.player.position)
            elif event.type == pygame.MOUSEWHEEL:
                self._player_view_radius += event.y
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if not featureswitches.record_points:
                    if event.button == 1:
                        screen_mouse_pos = pygame.Vector2(event.pos)
                        self._take_picture(screen_mouse_pos)
                else:
                    if event.button == 1:
                        screen_mouse_pos = pygame.Vector2(event.pos)
                        w_pos = self._cam.to_world(screen_mouse_pos)
                        self.recorded_points.append(w_pos)
                    elif event.button == 3:
                        for i, p in enumerate(self.recorded_points[:-1]):
                            pn = self.recorded_points[i + 1]
                            x1 = int(p.x)
                            y1 = int(p.y)
                            x2 = int(pn.x)
                            y2 = int(pn.y)
                            logger.warning(
                                "        self._segments.append(Segment(pygame.Vector2(%s, %s), pygame.Vector2(%s, %s)))",
                                x1, y1, x2, y2)
                            try:
                                self._segments.append(Segment(pygame.Vector2(x1, y1), pygame.Vector2(x2, y2)))
                            except ValueError as ve:
                                logger.error(ve)
                        p = self.recorded_points[-1]
                        pn = self.recorded_points[0]
                        x1 = int(p.x)
                        y1 = int(p.y)
                        x2 = int(pn.x)
                        y2 = int(pn.y)
                        logger.warning(
                            "        self._segments.append(Segment(pygame.Vector2(%s, %s), pygame.Vector2(%s, %s)))",
                            x1, y1, x2, y2)
                        self._segments.append(Segment(pygame.Vector2(x1, y1), pygame.Vector2(x2, y2)))

                        self.recorded_points = []
                        logger.warning("#" * 20)

        return next_scene

    def _take_picture(self, screen_pos):
        cr = self.cursor_img.get_rect(center=screen_pos)
        picture_img = pygame.Surface(cr.size)
        picture_img.blit(pygame.display.get_surface(), (0, 0), cr)
        picture_img = pygame.transform.scale2x(picture_img)

        world_coord = self._cam.to_world(screen_pos)
        meta = None  # todo find out if something is on it!?
        self.game_data.pictures.append(PictureInfo(picture_img, meta))


@contextmanager
def clip_resource(screen, clip_rect):
    orig_clip = screen.get_clip()
    screen.set_clip(clip_rect)
    try:
        yield clip_rect
    finally:
        screen.set_clip(orig_clip)

class Light:

    def __init__(self, position, surf):
        self.position:pygame.Vector2 = position
        self.surf:pygame.Surface = surf

    def update(self, dt_s):
        pass

class Player:

    def __init__(self, position, look_at, key_provider):
        self.position: pygame.Vector2 = position
        self.prev_position = self.position.copy()
        self.loot_at = look_at
        self._key_provider = key_provider
        self.speed = 65.0
        self.look_direction = (self.loot_at - self.position).normalize()
        self._head_distance = 5
        self.light_position = self.position + self.look_direction * self._head_distance
        self.radius = 2 * self._head_distance + 2
        self.collision_displacement = pygame.Vector2(0, 0)
        self.axis = pygame.Vector2(0, 0)
        self.view_angle = math.cos(math.radians(45))  # the angle left and right of the view direction

    def update(self, dt_s):
        pressed = self._key_provider.get_pressed()
        left = pressed[pygame.K_LEFT] or pressed[pygame.K_a]
        right = pressed[pygame.K_RIGHT] or pressed[pygame.K_d]
        up = pressed[pygame.K_UP] or pressed[pygame.K_w]
        down = pressed[pygame.K_DOWN] or pressed[pygame.K_s]
        direction = pygame.Vector2(right - left, down - up)
        dir_len = direction.length()
        if dir_len:
            direction /= dir_len
        delta_distance = self.speed * dt_s * direction
        self.prev_position.update(self.position)
        self.position += delta_distance
        # self.position = round(self.position)
        self.loot_at += delta_distance
        self.look_direction = (self.loot_at - self.position).normalize()
        self.light_position = self.position + self.look_direction * self._head_distance
        self.axis = pygame.Vector2(0, 0)

    def set_to_position(self, pos):
        dp = pos - self.position
        self.move(dp)

    def move(self, dp):
        self.prev_position.update(self.position)
        self.prev_position += dp
        self.position += dp
        self.loot_at += dp
        self.look_direction = (self.loot_at - self.position).normalize()
        self.light_position = self.position + self.look_direction * self._head_distance
