# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'main.py' is part of pw-35
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The main entry point of the game.

"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2023"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

from contextlib import contextmanager

import pygame

from gamelib import scenemanager, settings, sound, spacez
from gamelib.gamedata import GameData
from gamelib.textscene import TextScene
from gamelib.timing import Timer, Accumulator
from gamelib.transitions import FadeOutAndIn
from gamelib.view import Camera
from gamelib.world import World, PreWorld, Player

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


@contextmanager
def pygame_resource():
    import pygame.font
    pygame.font.init()
    pygame.init()
    try:
        yield pygame
    finally:
        pygame.quit()


class MainLoop:

    @staticmethod
    def run():
        with pygame_resource():
            pygame.display.set_caption(settings.app_name, settings.app_name)
            pygame.display.set_icon(pygame.image.load(settings.icon_path))
            # screen = pygame.display.set_mode(SCREENSIZE, pygame.RESIZABLE | pygame.SRCALPHA, 32)
            screen = pygame.display.set_mode(settings.SCREENSIZE)
            pygame.mixer.init(buffer=128)
            sound.load_sfx()
            sound.load_songs()

            clock = pygame.time.Clock()
            space = spacez.new(size=100)

            player = Player(pygame.Vector2(settings.player_start), pygame.Vector2(1, 0), pygame.key)
            sw, sh = settings.SCREENSIZE
            camera = Camera(player.position, pygame.Rect(0, 0, sw, sh))
            transition = FadeOutAndIn(pygame.display, clock)
            game_data = GameData()
            world_scene = World(clock, pygame.event, player, space, camera, game_data, Timer(settings.draw_dt_s),
                                Accumulator(), pygame.font, pygame.time.Clock(), pygame.time.Clock())
            pre_world = PreWorld(pygame.event, world_scene)
            font = pygame.font.Font(None, 30)
            credits_text = "Credits\n\nDR0ID@pyweek35.2023\n\n\n\n\n\nThanks for playing.\n\n\npress any key to quit"
            credits_scene = TextScene(pygame.event, pygame.display, credits_text, font, transition)
            intro_text = "The cove\n" \
                         "\n" \
                         "Use keys a,s,d,w to move around,\n" \
                         "point and click with the mouse \n" \
                         "to take a picture.\n" \
                         "\n" \
                         "Proof that there is a monster lurking in the shadow.\n" \
                         "\n" \
                         "Unfortunately this game is unfinished. There is no win nor\n" \
                         "a loose condition. All you can do is wander around and take\n " \
                         "pictures (see 'pictures' directory after play).\n" \
                         "\n" \
                         "There are some feature switches: \n" \
                         "11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21\n" \
                         "\n" \
                         "press any key to start..."
            intro_scene = TextScene(pygame.event, pygame.display, intro_text, font, transition)

            stack = scenemanager.SceneManager()
            stack.push(credits_scene)
            stack.push(world_scene)
            stack.push(intro_scene)

            scene = stack.top()

            accumulator = 0.0

            update_dt = settings.update_dt_s
            draw_dt = settings.draw_dt_s
            game_time = 0
            next_draw = 0

            while scene:
                cmd = scene.run()
                if cmd:
                    scene = stack.update(cmd)


def main():
    main_loop = MainLoop()
    main_loop.run()


logger.debug("imported")
