# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'timing.py' is part of pw-35
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Time related stuff.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2023"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["Timer", "Accumulator"]  # list of public visible parts of this module

from gamelib import settings

logger = logging.getLogger(__name__)
logger.debug("importing...")


class Timer:

    def __init__(self, interval_s):
        self.interval_s = interval_s
        self._next_draw = 0
        self.elapsed = None

    def update(self, game_time_s):
        if self._next_draw < game_time_s:
            self.elapsed()
            self._next_draw += self.interval_s


class Accumulator:

    def __init__(self):
        self._accu = 0
        self.game_time_s = 0
        self.update_dt_s = settings.update_dt_s
        self.max_raw_dt_s = settings.max_raw_dt_s
        self.call_back = None

    def update(self, raw_dt_s):
        if raw_dt_s > self.max_raw_dt_s:
            raw_dt_s = self.max_raw_dt_s
        self._accu += raw_dt_s
        while self._accu > self.update_dt_s:
            do_break_loop = self.call_back(self.update_dt_s, self.game_time_s)
            if do_break_loop:
                return do_break_loop
            self.game_time_s += self.update_dt_s
            self._accu -= self.update_dt_s

    @property
    def alpha(self):
        return self._accu / self.update_dt_s


logger.debug("imported")
