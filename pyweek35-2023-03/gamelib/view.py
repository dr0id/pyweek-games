# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'view.py' is part of pw-35
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Anything related to the view of the game.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2023"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["Camera"]  # list of public visible parts of this module

from contextlib import contextmanager

import pygame

logger = logging.getLogger(__name__)
logger.debug("importing...")


class Camera:

    def __init__(self, world_position, world_rect: pygame.Rect, ppu=1.0, screen_pos=(0, 0), speed=4):
        self._position = world_position
        self.ppu = ppu
        self.world_rect = world_rect
        self.screen_rect = pygame.Rect(0, 0, self.world_rect.w * self.ppu, self.world_rect.h * self.ppu)
        self.screen_rect.topleft = screen_pos
        self._screen_offset = pygame.Vector2(self.screen_rect.topleft) + 0.5 * pygame.Vector2(self.screen_rect.size)
        self.offset = pygame.Vector2(0, 0)
        self.to_screen_translation = pygame.Vector2(0, 0)
        self.to_world_translation = pygame.Vector2(0, 0)
        self.speed = speed  # how fast it should lerp to new position

        self.look_at(self._position)  # update values

    def look_at(self, world_position):
        self._position = world_position
        self.world_rect.center = world_position
        self.to_screen_translation = -((self._position + self.offset) * self.ppu) + self._screen_offset
        self.to_world_translation = - (self._screen_offset / self.ppu) + self._position - self.offset

    def to_screen(self, world_pos):
        return round(world_pos * self.ppu + self.to_screen_translation)

    def to_world(self, screen_pos):
        return screen_pos / self.ppu + self.to_world_translation

    def lerp(self, point, dt_s):
        """
        Linear interpolation to the given point.
        :param point: The point where the camera should be when t=1.0
        :param dt_s: interpolation factor, t=0 -> self.position, t=1 -> point
        """
        # see for accurate lerp: https://en.wikipedia.org/wiki/Linear_interpolation
        # float lerp(float v0, float v1, float t) {
        #   return (1 - t) * v0 + t * v1;
        # }
        t = self.speed * dt_s
        t = 1 if t > 1 else (0 if t < 0 else t)
        new_position = (1 - t) * self._position + t * point
        self.look_at(new_position)

    @contextmanager
    def clip(self, screen: pygame.Surface):
        orig_clip = screen.get_clip()
        screen.set_clip(self.screen_rect)
        try:
            yield screen
        finally:
            screen.set_clip(orig_clip)


logger.debug("imported")
