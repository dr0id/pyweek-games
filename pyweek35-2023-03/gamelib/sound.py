# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'world.py' is part of pw-35
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
sound.py - Crude sfx manager and song carousel

To play sfx:
1. Init pygame mixer.
2. load_sfx()
3. Pick sfx by dict keyword. They are pygame.mixer.Sound objects.

To play songs:
1. Init pygame mixer.
2. load_songs()
3. In the game update loop, call roll_songs(). It's okay to spam, it has very light overhead.

To configure sfx:
1. Set sfx_path = 'some_path'. This can be absolute or relative.
2. Add sfx filenames to the dict _sfx_filename_map.

To configure songs:
1. Set songs_path = 'some_path'. This can be absolute or relative.
2. Add song filenames to the dict _songs_filename_map.
3. Arrange the order of the songs by adding the dict keywords to the list songs_queue.
4. Note: as songs play, the songs_queue list will be rotated front to back. If you want to restore or otherwise use
   the original order, it is saved in original_songs_queue. This module does not rely on original_songs_queue; it is
   for your convenience. NB: if you manually change songs_queue, you may want to also change origional_songs_queue to
   keep their contents in sync.

"""
from __future__ import print_function, division

import logging

import pygame

logging.basicConfig()
logger = logging.getLogger(__name__)

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2023"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"


# __all__ = []  # list of public visible parts of this module


sfx_path = 'resources/sfx'
songs_path = 'resources/songs'

_sfx_file_map = dict(
    default = 'bump.ogg',
)
sfx = {}

_songs_file_map = dict(
    default = 'vs1-1ada.mid',
)
songs = {}
songs_queue = ['default']
original_songs_queue = list(songs_queue)


class Song(object):

    master_volume = 1.0

    def __init__(self, name, file_name, volume=1.0):
        self.name = name
        self.file_name = file_name
        self.volume = volume

    def play(self):
        pygame.mixer.music.load(self.file_name)
        self.set_volume(self.master_volume)
        pygame.mixer.music.play()

    def queue(self):
        pygame.mixer.music.queue(self.file_name)

    @staticmethod
    def fadeout(time=1000):
        pygame.mixer.music.fadeout(time)

    @staticmethod
    def stop():
        pygame.mixer.music.stop()

    @staticmethod
    def pause():
        pygame.mixer.music.pause()

    @staticmethod
    def unpause():
        pygame.mixer.music.unpause()

    @staticmethod
    def stop():
        pygame.mixer.music.stop()

    @staticmethod
    def get_volume():
        return pygame.mixer.music.get_volume()

    @staticmethod
    def set_volume(value):
        Song.master_volume = value
        pygame.mixer.music.set_volume(value)

    @staticmethod
    def get_busy():
        return pygame.mixer.music.get_busy()


def load_sfx():
    sfx.clear()
    for k, filename in _sfx_file_map.items():
        sfx[k] = pygame.mixer.Sound(f'{sfx_path}/{filename}')


def load_songs():
    songs.clear()
    for k, filename in _songs_file_map.items():
        songs[k] = Song(k, f'{songs_path}/{filename}')


def roll_songs(intro=False):
    if songs_queue:
        song = now_playing()
        if not song.get_busy():
            if not intro:
                k = songs_queue.pop(0)
                songs_queue.append(k)
                song = now_playing()
            song.play()


def now_playing():
    k = songs_queue[0]
    current_song = songs[k]
    return current_song


def remove_intro():
    global songs
    songs = [s for s in songs if s.name != 'Rise of an Empire']


if __name__ == '__main__':
    pygame.mixer.init(buffer=128)

    # fix paths from pwd
    sfx_path = '../resources/sfx'
    songs_path = '../resources/songs'

    load_songs()
    load_sfx()

    sfx['default'].play()
    pygame.time.wait(500)
    sfx['default'].play()
    pygame.time.wait(500)
    sfx['default'].play()
    pygame.time.wait(500)

    # song = songs['default']
    # song.play()
    # while song.get_busy():
    #     pygame.time.wait(1000)

    play_seconds = 5 * 1000
    roll_songs()
    pygame.time.wait(play_seconds)
    now_playing = songs_queue[0]
    now_playing.fadeout()
    while now_playing.get_busy():
        pygame.time.wait(1000)
