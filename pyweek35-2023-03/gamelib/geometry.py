# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'geometry.py' is part of pw-35
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This is a module to help with geometry.


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2023"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

import math
import sys

import pygame

logger = logging.getLogger(__name__)
logger.debug("importing...")


class Segment:

    def __init__(self, p0, p1, is_wall=True):
        self.p0 = p0
        self.p1 = p1
        self.direction: pygame.Vector2 = p1 - p0
        self.id = id(self)
        self.is_wall = is_wall
        self.normal = self.direction.rotate(-90).normalize()
        xmin = int(math.floor(p0.x if p0.x < p1.x else p1.x))
        xmax = int(math.floor(p0.x if p0.x >= p1.x else p1.x))
        ymin = int(math.ceil(p0.y if p0.y < p1.y else p1.y))
        ymax = int(math.ceil(p0.y if p0.y >= p1.y else p1.y))
        self.rect = pygame.Rect(xmin, ymin, xmax - xmin + 1, ymax - ymin + 1)  # add for vertical and horizontal lines!
        self.idx = -1


class Ray:
    def __init__(self, p, segment_point):
        self.p = p
        self.direction: pygame.Vector2 = (segment_point - p)
        self.a = math.atan2(self.direction.y, self.direction.x)


class Ray2:
    def __init__(self, p, segment_point, segment):
        self.p = p
        self.direction: pygame.Vector2 = (segment_point - p)
        self.a = math.atan2(self.direction.y, self.direction.x)
        self.segment = segment
        self.begin = None
        self.id = -1


class Intersection:

    def __init__(self, ray, segment, t1):
        self.point = ray.p + ray.direction * t1
        self.segment = segment
        self.ray: Ray2 = ray
        self.t1 = t1
        # self.t2 = t2


def get_nearest(ray, segments):
    open_segments = dict()
    new_nearest = None
    new_nearest_t1 = sys.maxsize
    behind = None
    behind_t1 = sys.maxsize
    r = ray
    for s in segments:
        if s.normal.dot(r.direction) > ViewPolygon.TOLERANCE:
            # normal pointing away from view direction
            continue
        if s == r.segment:
            t1 = 1.0
        else:
            # print("seg", s.p0, s.p1, s.id)
            # // Solve for T2! # T2 = (r_dx*(s_py-r_py) + r_dy*(r_px-s_px))/(s_dx*r_dy - s_dy*r_dx)
            s_p = s.p0
            s_d = s.p1 - s_p
            r_d = r.direction
            r_p = r.p
            det = s_d.x * r_d.y - s_d.y * r_d.x
            if abs(det) <= ViewPolygon.TOLERANCE:
                continue
            t2 = (r_d.x * (s_p.y - r_p.y) + r_d.y * (r_p.x - s_p.x)) / det
            # // Plug the value of T2 to get T1 # T1 = (s_px+s_dx*T2-r_px)/r_dx
            if r_d.x != 0:
                t1 = (s_p.x + s_d.x * t2 - r_p.x) / r_d.x
            elif r_d.y != 0:
                t1 = (s_p.y + s_d.y * t2 - r_p.y) / r_d.y
            else:
                raise Exception("0-Vector!")

            if t1 < 0:  # behind of origin point of ray
                continue
            # if t2 < 0:  # outside of segment (p1)
            #     continue
            # if t2 > 1:  # outside of segment (p2)
            #     continue
            # if t2 == 1:  # this is the end of the segment, don't add to open segments! (p2)
            #     continue
            if t2 < 0:  # outside of segment (p1)
                if -t2 - 0 > ViewPolygon.TOLERANCE:  # t2 < 0
                    if logger.level == logging.DEBUG:
                        logger.debug("t2 < 0")
                    continue
                else:
                    logger.debug("to remove 11")
            if t2 > 1:  # outside of segment (p2)
                if t2 - 1 > ViewPolygon.TOLERANCE:  # t2 > 1
                    if logger.level == logging.DEBUG:
                        logger.debug("t2 > 1")
                    continue
                else:
                    logger.debug("to remove 12")
            if abs(t2 - 1) < ViewPolygon.TOLERANCE:  # this is the end of the segment, don't add to open segments! (p2)
                continue

        # intersection!
        open_segments[s.id] = s

        if t1 <= new_nearest_t1 and t1 <= 1.0:  # keep only nearest to ray origin
            new_nearest_t1 = t1
            new_nearest = s
        elif t1 <= behind_t1:
            behind_t1 = t1
            behind = s

    return open_segments, new_nearest, new_nearest_t1, behind, behind_t1


class ViewPolygon:
    TOLERANCE = 10e-8

    def get_visible_volume(self, segments, position):
        intersections = []
        for s in segments:
            r0 = Ray2(position, s.p0, s)
            intersections.extend(self.intersect_ray_segments(r0, s, 0, segments))

            r1 = Ray2(position, s.p1, s)
            intersections.extend(self.intersect_ray_segments(r1, s, 1, segments))

        intersections.sort(key=lambda _i: _i.ray.a)  # (_i.ray.a, _i.t1))
        return intersections

    @staticmethod
    def intersect_ray_segments(ray: Ray2, segment: Segment, idx: int, segments: list[Segment]):
        nearest_t1 = sys.maxsize
        for s in segments:
            if s == segment or s.p0 == segment.p0 or s.p1 == segment.p1:
                continue
            # // Solve for T2! # T2 = (r_dx*(s_py-r_py) + r_dy*(r_px-s_px))/(s_dx*r_dy - s_dy*r_dx)
            s_p = s.p0
            s_d = s.p1 - s_p
            r_d = ray.direction
            r_p = ray.p
            det = s_d.x * r_d.y - s_d.y * r_d.x
            if abs(det) <= 1e-10:
                continue
            t2 = (r_d.x * (s_p.y - r_p.y) + r_d.y * (r_p.x - s_p.x)) / det
            # // Plug the value of T2 to get T1 # T1 = (s_px+s_dx*T2-r_px)/r_dx
            if r_d.x != 0:
                t1 = (s_p.x + s_d.x * t2 - r_p.x) / r_d.x
            elif r_d.y != 0:
                # t1 = (s_py + s_dy * T2 - r_py) / r_dy
                t1 = (s_p.y + s_d.y * t2 - r_p.y) / r_d.y
            else:
                raise Exception("0-Vector!")

            if t1 < 0:  # behind of origin point of ray
                continue
            if t2 < 0:  # outside of segment (p1)
                continue
            if t2 > 1:  # outside of segment (p2)
                continue

            if t1 < nearest_t1:  # keep only nearest to ray origin
                nearest_t1 = t1

        if nearest_t1 == sys.maxsize:
            # no other segment intersected, just add the point on the segment
            segment_intersection = Intersection(ray, segment, 1)
            return [segment_intersection]  # , Intersection(ray, segment, t1 * 2)]
        else:
            if nearest_t1 < 1:
                # a segment intersection has been found that is nearer to the ray origin as the current edge point
                # add only this intersection point
                return [Intersection(ray, segment, nearest_t1)]
            else:
                # a segment intersection behind the segment point has been found, add both
                # nudge the current segment point a bit away from the segment so is not exactly on the ray anymore
                # the nudge direction has been chosen such that the sort by angle later gives the right order of
                # points (e.g. if sweep from left to right:
                #          point behind (0), left segment point (1), right segment point (2), behind (3) )
                #
                #                                    move 1 -> 1' and construct ray r1'
                #                                    move 2 -> 2'
                #     0 x
                #        \             x 3                \             /
                #         \    s      /              \     \1        2 /     /
                #        1 x---------x 2           1'  x     x---------x   x  2'
                #           \       /                    \   \       /   /
                #            \     /                   r1' \  r1   r2  / r2'
                #          r1 \   / r2                       \ \   / /
                #              \ /                             \\ //
                #               O                                O
                #
                nudge_t2 = (-0.001 if idx == 0 else 1.001)
                ray1 = Ray2(ray.p, segment.p0 + nudge_t2 * segment.direction, segment)
                nudged_intersection = Intersection(ray1, segment, nearest_t1)
                segment_intersection = Intersection(ray, segment, 1)

                # nudge_t2 = (0.001 if idx == 0 else 1.0-0.001)
                # ray1 = Ray(ray.p, segment.p1 + nudge_t2 * segment.direction)
                # nudged_intersection = Intersection(ray, segment, nearest_t1)
                # segment_intersection = Intersection(ray1, segment, 1)
                return [segment_intersection, nudged_intersection]

    # def lineIntersection(self, A, B, C, D):
    #     Bx_Ax = B[0] - A[0]
    #     By_Ay = B[1] - A[1]
    #     Dx_Cx = D[0] - C[0]
    #     Dy_Cy = D[1] - C[1]
    #     determinant = (-Dx_Cx * By_Ay + Bx_Ax * Dy_Cy)
    #     if abs(determinant) < 1e-20:
    #         return None
    #     s = (-By_Ay * (A[0] - C[0]) + Bx_Ax * (A[1] - C[1])) / determinant
    #     t = (Dx_Cx * (A[1] - C[1]) - Dy_Cy * (A[0] - C[0])) / determinant
    #     if s >= 0 and s <= 1 and t >= 0 and t <= 1:
    #         return (A[0] + (t * Bx_Ax), A[1] + (t * By_Ay))
    #     return None
    @staticmethod
    def get_visible_volume2(segments, position_in):
        intersection_points = []
        # not sure why, but it seems that for now it only works best with integer positions
        position = pygame.Vector2(int(position_in.x), int(position_in.y))
        # logger.debug("volume position %s", position)

        rays: list[Ray2] = []
        for segment in segments:
            r0 = Ray2(position, segment.p0, segment)
            rays.append(r0)
            r1 = Ray2(position, segment.p1, segment)
            rays.append(r1)
            dangle = r1.a - r0.a
            if dangle <= math.pi:
                dangle += 2 * math.pi
            if dangle > math.pi:
                dangle -= 2 * math.pi
            r0.begin = dangle > 0.0
            r1.begin = not r0.begin
        rays.sort(key=lambda _r: (_r.a, not _r.begin))

        # setup initial condition by sweeping once, keeps the open segments at the end
        open_segments = dict()
        for r in rays:
            # update open segments
            if r.begin:
                open_segments[r.segment.id] = r.segment  # add new segment
            else:
                if r.segment.id in open_segments.keys():  # end segment
                    del open_segments[r.segment.id]

        # find the initial nearst segment
        r = rays[0]
        # find nearest segment
        new_nearest_t1 = sys.maxsize
        new_nearest = None
        for s in open_segments.values():
            if s == r.segment:
                t1 = 1.0
            else:
                # print("seg", s.p0, s.p1, s.id)
                # // Solve for T2! # T2 = (r_dx*(s_py-r_py) + r_dy*(r_px-s_px))/(s_dx*r_dy - s_dy*r_dx)
                s_p = s.p0
                s_d = s.p1 - s_p
                r_d = r.direction
                r_p = r.p
                det = s_d.x * r_d.y - s_d.y * r_d.x
                if abs(det) <= 1e-10:
                    continue
                t2 = (r_d.x * (s_p.y - r_p.y) + r_d.y * (r_p.x - s_p.x)) / det
                # // Plug the value of T2 to get T1 # T1 = (s_px+s_dx*T2-r_px)/r_dx
                if r_d.x != 0:
                    t1 = (s_p.x + s_d.x * t2 - r_p.x) / r_d.x
                elif r_d.y != 0:
                    t1 = (s_p.y + s_d.y * t2 - r_p.y) / r_d.y
                else:
                    raise Exception("0-Vector!")

                if t1 < 0:  # behind of origin point of ray
                    continue
                if t2 < 0:  # outside of segment (p1)
                    continue
                if t2 > 1:  # outside of segment (p2)
                    continue

            if t1 < new_nearest_t1:  # keep only nearest to ray origin
                new_nearest_t1 = t1
                new_nearest = s

        # sweep again building the points of the polygon
        nearest = new_nearest
        for i, r in enumerate(rays):
            # print(r, r.p, len(rays))

            # update open segments
            if r.begin:
                open_segments[r.segment.id] = r.segment  # add new segment
            else:
                if r.segment.id in open_segments.keys():  # end segment
                    del open_segments[r.segment.id]

            # find nearest segment
            new_nearest_t1 = sys.maxsize
            new_nearest = None
            nearest_t1 = sys.maxsize
            # print("ray", r.a, r.p + r.direction, r.segment.id)
            for s in open_segments.values():
                if s == r.segment:
                    t1 = 1.0
                else:
                    # print("seg", s.p0, s.p1, s.id)
                    # // Solve for T2! # T2 = (r_dx*(s_py-r_py) + r_dy*(r_px-s_px))/(s_dx*r_dy - s_dy*r_dx)
                    s_p = s.p0
                    s_d = s.p1 - s_p
                    r_d = r.direction
                    r_p = r.p
                    det = s_d.x * r_d.y - s_d.y * r_d.x
                    if abs(det) <= 1e-10:
                        # ray and segment are parallel
                        r_len_sq = r.direction.dot(r.direction)
                        if r_len_sq < 1e-10:
                            # sitting on top of a point!
                            t1 = 0
                        else:
                            # segment is projected onto the ray and the smaller distance is then used
                            rp = r.direction / r_len_sq
                            proj_p0 = r.direction.dot(s.p0 - r.p) * rp
                            proj_p1 = r.direction.dot(s.p1 - r.p) * rp
                            proj = proj_p0 if proj_p0.dot(proj_p0) < proj_p1.dot(proj_p1) else proj_p1
                            t1 = proj.length() / r.direction.length()
                            if t1 < 0:  # behind of origin point of ray
                                if logger.level == logging.DEBUG:
                                    logger.debug("det 0 t1 < 0")
                                continue
                    else:
                        # find intersection point
                        t2 = (r_d.x * (s_p.y - r_p.y) + r_d.y * (r_p.x - s_p.x)) / det

                        if t2 < 0:  # outside of segment (p1)
                            if logger.level == logging.DEBUG:
                                logger.debug("t2 < 0")
                            continue
                        if t2 > 1:  # outside of segment (p2)
                            if logger.level == logging.DEBUG:
                                logger.debug("t2 > 1")
                            continue

                        # // Plug the value of T2 to get T1 # T1 = (s_px+s_dx*T2-r_px)/r_dx
                        if r_d.x != 0:
                            t1 = (s_p.x + s_d.x * t2 - r_p.x) / r_d.x
                        elif r_d.y != 0:
                            t1 = (s_p.y + s_d.y * t2 - r_p.y) / r_d.y
                        else:
                            raise Exception("0-Vector!")

                        if t1 < 0:  # behind of origin point of ray
                            if logger.level == logging.DEBUG:
                                logger.debug("t1 < 0")
                            continue

                # keep track of nearest values
                if s == nearest:
                    nearest_t1 = t1  # t1 value for the currently nearest segment
                elif t1 < new_nearest_t1:  # keep only nearest to ray origin
                    new_nearest_t1 = t1  # t1 value for the potentially new nearest segment
                    new_nearest = s
                elif t1 == new_nearest_t1:  # special case where two segments have the same distance to ray origin
                    # find shared point (same distance on ray)
                    n0 = s.p1
                    if (s.p0.x == new_nearest.p0.x and s.p0.y == new_nearest.p0.y) or \
                            (nearest.p0.x == new_nearest.p1.x and s.p0.y == new_nearest.p1.y):
                        n0 = s.p0
                    # find direction vectors pointing away from n0
                    n1 = s.p0
                    if n1.x == n0.x and n1.y == n0.y:
                        n1 = s.p1
                    nc = (n1 - n0).normalize().dot(r.direction)  # calculate angle between ray and segment
                    nn1 = new_nearest.p0
                    if nn1.x == n0.x and nn1.y == n0.y:
                        nn1 = new_nearest.p1
                    nnc = (nn1 - n0).normalize().dot(r.direction)  # calculate angle between ray and segment
                    if nc < nnc:
                        new_nearest = s
                    new_nearest_t1 = t1

            # print("check")

            if nearest != new_nearest:
                if nearest_t1 < new_nearest_t1:  # the nearest is still the nearest, do nothing
                    continue

                assert new_nearest is not None

                if nearest_t1 == new_nearest_t1:  # find the nearest one
                    # find shared point (same distance on ray)
                    n0 = nearest.p1
                    if (nearest.p0.x == new_nearest.p0.x and nearest.p0.y == new_nearest.p0.y) or \
                            (nearest.p0.x == new_nearest.p1.x and nearest.p0.y == new_nearest.p1.y):
                        n0 = nearest.p0
                    # find direction vectors pointing away from n0
                    n1 = nearest.p0
                    if n1.x == n0.x and n1.y == n0.y:
                        n1 = nearest.p1
                    nc = (n1 - n0).normalize().dot(r.direction)  # calculate angle between ray and segment
                    nn1 = new_nearest.p0
                    if nn1.x == n0.x and nn1.y == n0.y:
                        nn1 = new_nearest.p1
                    nnc = (nn1 - n0).normalize().dot(r.direction)  # calculate angle between ray and segment
                    if nc < nnc:
                        continue

                if r.begin:
                    if nearest:
                        assert nearest is not None, "assert0.0"
                        if nearest_t1 != sys.maxsize:
                            assert nearest_t1 != sys.maxsize, "assert0"
                            p0 = Intersection(r, nearest, nearest_t1)  # add point on nearest first
                            intersection_points.append(p0)
                        else:
                            if logger.level == logging.DEBUG:
                                logger.debug("assert nearest_t1 != sys.maxsize, assert0")
                        assert new_nearest_t1 is not None, "assert1.0"
                        assert new_nearest_t1 != sys.maxsize, "assert1"
                        p1 = Intersection(r, new_nearest, new_nearest_t1)
                        intersection_points.append(p1)
                        nearest = new_nearest
                    else:
                        raise Exception("superfluous 1")
                else:

                    if not r.begin and r.segment == nearest:
                        p0 = Intersection(r, nearest, 1)
                        intersection_points.append(p0)

                    if new_nearest:
                        assert new_nearest is not None, "assert4.0"
                        assert new_nearest_t1 != sys.maxsize, "assert4"
                        p1 = Intersection(r, new_nearest, new_nearest_t1)
                        intersection_points.append(p1)
                        nearest = new_nearest

        # print("volume done", len(intersection_points))
        return intersection_points

    @staticmethod
    def get_visible_volume3(segments, position_in):
        intersection_points = []
        # not sure why, but it seems that for now it only works best with integer positions
        position = pygame.Vector2(int(position_in.x), int(position_in.y))
        # logger.debug("volume position %s", position)

        rays: list[Ray2] = []
        segments_to_use = []
        for segment in segments:
            if segment.normal.dot(segment.p0 - position) > 0:
                # cull segments pointing away
                continue
            r0 = Ray2(position, segment.p0, segment)
            rays.append(r0)
            r1 = Ray2(position, segment.p1, segment)
            rays.append(r1)
            segments_to_use.append(segment)  # just add them here once, will scrap in next step
            dangle = r1.a - r0.a
            if dangle <= math.pi:
                dangle += 2 * math.pi
            if dangle > math.pi:
                dangle -= 2 * math.pi
            r0.begin = dangle > 0.0
            r1.begin = not r0.begin

        rays.sort(key=lambda _r: (_r.a, not _r.begin))
        logger.debug("segments in: %s segments: %s rays: %s pos_in: %s  pos: %s", len(segments), len(segments_to_use),
                     len(rays), position_in, position)
        # find intersections and nearest with initial ray, might be more efficient
        r = rays[0]
        open_segments, new_nearest, new_nearest_t1, behind, behind_t1 = get_nearest(r, segments_to_use)

        if r.segment == new_nearest and behind is not None:
            if r.begin:
                # only add the behind point if the nearest segment is also the first ray segment at segment start
                p0 = Intersection(r, behind, behind_t1)
                intersection_points.append(p0)
                p0 = Intersection(r, new_nearest, new_nearest_t1)
                intersection_points.append(p0)
            else:
                # only add the behind point if the nearest segment is also the first ray segment at segment end
                p0 = Intersection(r, new_nearest, new_nearest_t1)
                intersection_points.append(p0)
                p0 = Intersection(r, behind, behind_t1)
                intersection_points.append(p0)
        else:
            p0 = Intersection(r, new_nearest, new_nearest_t1)
            intersection_points.append(p0)

        # sweep
        nearest = new_nearest
        for i, r in enumerate(rays):
            # print(r, r.p, len(rays))

            # update open segments
            if r.begin:
                open_segments[r.segment.id] = r.segment  # add new segment
                # when adding a segment check against nearest, if nearer than swap, else do nothing?
                # we know that t for r.segment is 1
                # what t does the nearest segment have?
                s_p = nearest.p0
                s_d = nearest.p1 - s_p
                r_d = r.direction
                r_p = r.p
                # det = s_d.x * r_d.y - s_d.y * r_d.x
                det = s_d.cross(r_d)
                if abs(det) <= 1e-10:
                    # ray and segment are parallel
                    r_len_sq = r.direction.dot(r.direction)
                    if r_len_sq < 1e-10:
                        # sitting on top of a point!
                        t1 = 0
                    else:
                        # segment is projected onto the ray and the smaller distance is then used
                        rp = r.direction / r_len_sq
                        proj_p0 = r.direction.dot(nearest.p0 - r.p) * rp
                        proj_p1 = r.direction.dot(nearest.p1 - r.p) * rp
                        proj = proj_p0 if proj_p0.dot(proj_p0) < proj_p1.dot(proj_p1) else proj_p1
                        t1 = proj.length() / r.direction.length()
                        if t1 < 0:  # behind of origin point of ray
                            if logger.level == logging.DEBUG:
                                logger.debug("det 0 t1 < 0")
                            # continue
                            raise Exception("continue 1")
                else:
                    # find intersection point
                    # t2 = (r_d.x * (s_p.y - r_p.y) + r_d.y * (r_p.x - s_p.x)) / det
                    t2 = r_d.cross(s_p - r_p) / det

                    if t2 < 0:  # outside of segment (p1)
                        if logger.level == logging.DEBUG:
                            logger.debug("t2 < 0")
                        # continue
                        raise Exception("continue 2")
                    if t2 > 1:  # outside of segment (p2)
                        if logger.level == logging.DEBUG:
                            logger.debug("t2 > 1")
                        # continue
                        raise Exception("continue 3")

                    # // Plug the value of T2 to get T1 # T1 = (s_px+s_dx*T2-r_px)/r_dx
                    if r_d.x != 0:
                        t1 = (s_p.x + s_d.x * t2 - r_p.x) / r_d.x
                    elif r_d.y != 0:
                        t1 = (s_p.y + s_d.y * t2 - r_p.y) / r_d.y
                    else:
                        raise Exception("0-Vector!")

                    if t1 < 0:  # behind of origin point of ray
                        if logger.level == logging.DEBUG:
                            logger.debug("t1 < 0")
                        # continue
                        raise Exception("continue 4")

                nearest_t1 = t1
                new_nearest_t1 = 1.0
                if new_nearest_t1 < nearest_t1:
                    # new segment is in front of nearest
                    new_nearest = r.segment
                elif nearest_t1 == new_nearest_t1:
                    # new segment might be in front check further
                    # find shared point (same distance on ray)
                    new_nearest = r.segment
                    n0 = nearest.p1
                    if (nearest.p0.x == new_nearest.p0.x and nearest.p0.y == new_nearest.p0.y) or \
                            (nearest.p0.x == new_nearest.p1.x and nearest.p0.y == new_nearest.p1.y):
                        n0 = nearest.p0
                    # find direction vectors pointing away from n0
                    n1 = nearest.p0
                    if n1.x == n0.x and n1.y == n0.y:
                        n1 = nearest.p1
                    nc = (n1 - n0).normalize().dot(r.direction)  # calculate angle between ray and segment
                    nn1 = new_nearest.p0
                    if nn1.x == n0.x and nn1.y == n0.y:
                        nn1 = new_nearest.p1
                    nnc = (nn1 - n0).normalize().dot(r.direction)  # calculate angle between ray and segment
                    if nc < nnc:
                        # nearest is still the nearest
                        continue
                    new_nearest_t1 = t1
                else:
                    # nothing to do, new segment is farther away as nearest
                    continue

            else:
                if r.segment.id in open_segments.keys():  # end segment
                    del open_segments[r.segment.id]
                else:
                    raise Exception("segment should have been in open_segments!!")
                # remove segment, if it is nearest then find next else do nothing?
                if nearest == r.segment:
                    # find the nearest one in remaining open_segments

                    p0 = Intersection(r, nearest, 1)
                    intersection_points.append(p0)

                    nearest = None
                    nearest_t1 = sys.maxsize

                    # find nearest segment
                    new_nearest_t1 = sys.maxsize
                    new_nearest = None
                    # print("ray", r.a, r.p + r.direction, r.segment.id)
                    for s in open_segments.values():

                        if s == r.segment:
                            raise Exception("nearest has already been removed from open_segments")
                            t1 = 1.0
                        else:
                            # print("seg", s.p0, s.p1, s.id)
                            # // Solve for T2! # T2 = (r_dx*(s_py-r_py) + r_dy*(r_px-s_px))/(s_dx*r_dy - s_dy*r_dx)
                            s_p = s.p0
                            s_d = s.p1 - s_p
                            r_d = r.direction
                            r_p = r.p
                            det = s_d.x * r_d.y - s_d.y * r_d.x
                            if abs(det) <= 1e-10:
                                # ray and segment are parallel
                                r_len_sq = r.direction.dot(r.direction)
                                if r_len_sq < 1e-10:
                                    # sitting on top of a point!
                                    t1 = 0
                                else:
                                    # segment is projected onto the ray and the smaller distance is then used
                                    rp = r.direction / r_len_sq
                                    proj_p0 = r.direction.dot(s.p0 - r.p) * rp
                                    proj_p1 = r.direction.dot(s.p1 - r.p) * rp
                                    proj = proj_p0 if proj_p0.dot(proj_p0) < proj_p1.dot(proj_p1) else proj_p1
                                    t1 = proj.length() / r.direction.length()
                                    if t1 < 0:  # behind of origin point of ray
                                        if logger.level == logging.DEBUG:
                                            logger.debug("det 0 t1 < 0")
                                        continue
                            else:
                                # find intersection point
                                t2 = (r_d.x * (s_p.y - r_p.y) + r_d.y * (r_p.x - s_p.x)) / det

                                if t2 < 0:  # outside of segment (p1)
                                    if logger.level == logging.DEBUG:
                                        logger.debug("t2 < 0")
                                    continue
                                if t2 > 1:  # outside of segment (p2)
                                    if logger.level == logging.DEBUG:
                                        logger.debug("t2 > 1")
                                    continue

                                # // Plug the value of T2 to get T1 # T1 = (s_px+s_dx*T2-r_px)/r_dx
                                if r_d.x != 0:
                                    t1 = (s_p.x + s_d.x * t2 - r_p.x) / r_d.x
                                elif r_d.y != 0:
                                    t1 = (s_p.y + s_d.y * t2 - r_p.y) / r_d.y
                                else:
                                    raise Exception("0-Vector!")

                                if t1 < 0:  # behind of origin point of ray
                                    if logger.level == logging.DEBUG:
                                        logger.debug("t1 < 0")
                                    continue

                        # keep track of nearest values
                        if s == nearest:
                            raise Exception("not possible, already removed")
                            nearest_t1 = t1  # t1 value for the currently nearest segment
                        elif t1 < new_nearest_t1:  # keep only nearest to ray origin
                            new_nearest_t1 = t1  # t1 value for the potentially new nearest segment
                            new_nearest = s
                        elif t1 == new_nearest_t1:  # special case where two segments have the same distance
                            # find shared point (same distance on ray)
                            n0 = s.p1
                            if (s.p0.x == new_nearest.p0.x and s.p0.y == new_nearest.p0.y) or \
                                    (new_nearest.p0.x == new_nearest.p1.x and s.p0.y == new_nearest.p1.y):
                                n0 = s.p0
                            # find direction vectors pointing away from n0
                            n1 = s.p0
                            if n1.x == n0.x and n1.y == n0.y:
                                n1 = s.p1
                            nc = (n1 - n0).normalize().dot(r.direction)  # calculate angle between ray and segment
                            nn1 = new_nearest.p0
                            if nn1.x == n0.x and nn1.y == n0.y:
                                nn1 = new_nearest.p1
                            nnc = (nn1 - n0).normalize().dot(r.direction)  # calculate angle between ray and segment
                            if nc < nnc:
                                new_nearest = s
                            new_nearest_t1 = t1

                else:
                    # behind nearest removed, do nothing
                    continue
            #
            # # print("check")

            if nearest != new_nearest:
                if nearest_t1 < new_nearest_t1:  # the nearest is still the nearest, do nothing
                    raise Exception("new_nearest should only be different if it is nearer!!!")

                assert new_nearest is not None

                # if nearest_t1 == new_nearest_t1:  # find the nearest one
                #     # find shared point (same distance on ray)
                #     # raise Exception("this is already known")
                #     n0 = nearest.p1
                #     if (nearest.p0.x == new_nearest.p0.x and nearest.p0.y == new_nearest.p0.y) or \
                #             (nearest.p0.x == new_nearest.p1.x and nearest.p0.y == new_nearest.p1.y):
                #         n0 = nearest.p0
                #     # find direction vectors pointing away from n0
                #     n1 = nearest.p0
                #     if n1.x == n0.x and n1.y == n0.y:
                #         n1 = nearest.p1
                #     nc = (n1 - n0).normalize().dot(r.direction)  # calculate angle between ray and segment
                #     nn1 = new_nearest.p0
                #     if nn1.x == n0.x and nn1.y == n0.y:
                #         nn1 = new_nearest.p1
                #     nnc = (nn1 - n0).normalize().dot(r.direction)  # calculate angle between ray and segment
                #     if nc < nnc:
                #         raise Exception("this is not true 2")

                if r.begin:
                    if nearest:
                        assert nearest is not None, "assert0.0"
                        if nearest_t1 != sys.maxsize:
                            assert nearest_t1 != sys.maxsize, "assert0"
                            p0 = Intersection(r, nearest, nearest_t1)  # add point on nearest first
                            intersection_points.append(p0)
                        else:
                            raise Exception("nearest_t1 should not be sys.maxsize")
                            if logger.level == logging.DEBUG:
                                logger.debug("assert nearest_t1 != sys.maxsize, assert0")
                        assert new_nearest_t1 is not None, "assert1.0"
                        assert new_nearest_t1 != sys.maxsize, "assert1"
                        p1 = Intersection(r, new_nearest, new_nearest_t1)
                        intersection_points.append(p1)
                        nearest = new_nearest
                    else:
                        raise Exception("superfluous 1")
                else:

                    if not r.begin and r.segment == nearest:
                        raise Exception("this should not happen!")
                        p0 = Intersection(r, nearest, 1)
                        intersection_points.append(p0)

                    if new_nearest:
                        assert new_nearest is not None, "assert4.0"
                        assert new_nearest_t1 != sys.maxsize, "assert4"
                        p1 = Intersection(r, new_nearest, new_nearest_t1)
                        intersection_points.append(p1)
                        nearest = new_nearest
                    else:
                        raise Exception("there is always a new nearest")

        # print("volume done", len(intersection_points))
        return intersection_points

    @staticmethod
    def get_visible_volume4(segments, position_in):
        intersection_points = []
        position = position_in
        # logger.debug("volume position %s", position)

        rays: list[Ray2] = []
        segments_to_use = []
        r_id = 0
        for segment in segments:
            if segment.normal.dot(position - segment.p0) < 0:
                # cull segments pointing away
                continue
            r0 = Ray2(position, segment.p0, segment)
            rays.append(r0)
            r0.id = r_id
            r_id += 1
            r1 = Ray2(position, segment.p1, segment)
            rays.append(r1)
            r1.id = r_id
            r_id += 1
            segments_to_use.append(segment)  # just add them here once, will scrap in next step
            dangle = r1.a - r0.a
            if dangle <= math.pi:
                dangle += 2 * math.pi
            if dangle > math.pi:
                dangle -= 2 * math.pi
            r0.begin = dangle > 0.0
            r1.begin = not r0.begin

        rays.sort(key=lambda _r: (_r.a, not _r.begin))
        logger.debug("segments in: %s segments: %s rays: %s pos_in: %s  pos: %s", len(segments), len(segments_to_use),
                     len(rays), position_in, position)
        # find intersections and nearest with initial ray, might be more efficient
        r = rays[0]
        open_segments, new_nearest, new_nearest_t1, behind, behind_t1 = get_nearest(r, segments_to_use)

        if r.segment == new_nearest and behind is not None:
            if r.begin:
                # only add the behind point if the nearest segment is also the first ray segment at segment start
                p0 = Intersection(r, behind, behind_t1)
                intersection_points.append(p0)
                p0 = Intersection(r, new_nearest, new_nearest_t1)
                intersection_points.append(p0)
            else:
                # only add the behind point if the nearest segment is also the first ray segment at segment end
                p0 = Intersection(r, new_nearest, new_nearest_t1)
                intersection_points.append(p0)
                p0 = Intersection(r, behind, behind_t1)
                intersection_points.append(p0)
        else:
            p0 = Intersection(r, new_nearest, new_nearest_t1)
            intersection_points.append(p0)

        # sweep
        nearest = new_nearest
        for i, r in enumerate(rays):
            # print(r, r.p, len(rays))

            # update open segments
            if r.begin:
                open_segments[r.segment.id] = r.segment  # add new segment
                # when adding a segment check against nearest, if nearer then swap, else do nothing?
                # we know that t for r.segment is 1
                # what t does the nearest segment have?
                if r.segment == nearest:
                    t1 = 1.0
                else:
                    s_p = nearest.p0
                    s_d = nearest.p1 - s_p
                    r_d = r.direction
                    r_p = r.p
                    # det = s_d.x * r_d.y - s_d.y * r_d.x
                    det = s_d.cross(r_d)
                    if abs(det) <= 1e-10:
                        # ray and segment are parallel
                        r_len_sq = r.direction.dot(r.direction)
                        if r_len_sq < 1e-10:
                            # sitting on top of a point!
                            t1 = 0
                        else:
                            # segment is projected onto the ray and the smaller distance is then used
                            rp = r.direction / r_len_sq
                            proj_p0 = r.direction.dot(nearest.p0 - r.p) * rp
                            proj_p1 = r.direction.dot(nearest.p1 - r.p) * rp
                            proj = proj_p0 if proj_p0.dot(proj_p0) < proj_p1.dot(proj_p1) else proj_p1
                            t1 = proj.length() / r.direction.length()
                            if t1 < 0:  # behind of origin point of ray
                                if logger.level == logging.DEBUG:
                                    logger.debug("det 0 t1 < 0")
                                # continue
                                raise Exception("continue 1")
                    else:
                        # find intersection point
                        # t2 = (r_d.x * (s_p.y - r_p.y) + r_d.y * (r_p.x - s_p.x)) / det
                        t2 = r_d.cross(s_p - r_p) / det

                        if t2 < 0:  # outside of segment (p1)
                            if -t2 - 0 > ViewPolygon.TOLERANCE:  # t2 < 0
                                if logger.level == logging.DEBUG:
                                    logger.debug("t2 < 0")
                                # continue
                                raise Exception("continue 2")
                            else:
                                logger.debug("to remove 5")
                        if t2 > 1:  # outside of segment (p2)
                            if t2 - 1 > ViewPolygon.TOLERANCE:  # t2 > 1
                                if logger.level == logging.DEBUG:
                                    logger.debug("t2 > 1")
                                # continue
                                raise Exception("continue 3")
                            else:
                                logger.debug("to remove 4")

                        # // Plug the value of T2 to get T1 # T1 = (s_px+s_dx*T2-r_px)/r_dx
                        if r_d.x != 0:
                            t1 = (s_p.x + s_d.x * t2 - r_p.x) / r_d.x
                        elif r_d.y != 0:
                            t1 = (s_p.y + s_d.y * t2 - r_p.y) / r_d.y
                        else:
                            raise Exception("0-Vector!")

                        if t1 < 0:  # behind of origin point of ray
                            if -t1 - 0 > ViewPolygon.TOLERANCE:  # t1 < 0
                                if logger.level == logging.DEBUG:
                                    logger.debug("t1 < 0")
                                # continue
                                raise Exception("continue 4")
                            else:
                                logger.debug("ro remove 6")

                nearest_t1 = t1 if abs(t1 - 1.0) > ViewPolygon.TOLERANCE else 1.0
                new_nearest_t1 = 1.0
                if new_nearest_t1 < nearest_t1:
                    # new segment is in front of nearest
                    new_nearest = r.segment
                elif nearest_t1 == new_nearest_t1:
                    # new segment might be in front check further
                    # find shared point (same distance on ray)
                    new_nearest = r.segment
                    n0 = nearest.p1
                    if (nearest.p0.x == new_nearest.p0.x and nearest.p0.y == new_nearest.p0.y) or \
                            (nearest.p0.x == new_nearest.p1.x and nearest.p0.y == new_nearest.p1.y):
                        n0 = nearest.p0
                    # find direction vectors pointing away from n0
                    n1 = nearest.p0
                    if n1.x == n0.x and n1.y == n0.y:
                        n1 = nearest.p1
                    nc = (n1 - n0).normalize().dot(r.direction)  # calculate angle between ray and segment
                    nn1 = new_nearest.p0
                    if nn1.x == n0.x and nn1.y == n0.y:
                        nn1 = new_nearest.p1
                    nnc = (nn1 - n0).normalize().dot(r.direction)  # calculate angle between ray and segment
                    if nc < nnc:
                        # nearest is still the nearest
                        continue
                    new_nearest_t1 = t1
                else:
                    # nothing to do, new segment is farther away as nearest
                    continue

            else:
                # r.end
                if r.segment.id in open_segments.keys():  # end segment
                    del open_segments[r.segment.id]
                else:
                    raise Exception("segment should have been in open_segments!!")
                # remove segment, if it is nearest then find next else do nothing?
                if nearest == r.segment:
                    # find the nearest one in remaining open_segments

                    p0 = Intersection(r, nearest, 1)
                    intersection_points.append(p0)

                    nearest = None
                    nearest_t1 = sys.maxsize

                    # find nearest segment
                    new_nearest_t1 = sys.maxsize
                    new_nearest = None
                    # print("ray", r.a, r.p + r.direction, r.segment.id)
                    for s in open_segments.values():

                        if s == r.segment:
                            raise Exception("nearest has already been removed from open_segments")
                            t1 = 1.0
                        else:
                            # print("seg", s.p0, s.p1, s.id)
                            # // Solve for T2! # T2 = (r_dx*(s_py-r_py) + r_dy*(r_px-s_px))/(s_dx*r_dy - s_dy*r_dx)
                            s_p = s.p0
                            s_d = s.p1 - s_p
                            r_d = r.direction
                            r_p = r.p
                            det = s_d.x * r_d.y - s_d.y * r_d.x
                            if abs(det) <= ViewPolygon.TOLERANCE:
                                # ray and segment are parallel
                                r_len_sq = r.direction.dot(r.direction)
                                if r_len_sq < ViewPolygon.TOLERANCE:
                                    # sitting on top of a point!
                                    t1 = 0
                                else:
                                    # segment is projected onto the ray and the smaller distance is then used
                                    rp = r.direction / r_len_sq
                                    proj_p0 = r.direction.dot(s.p0 - r.p) * rp
                                    proj_p1 = r.direction.dot(s.p1 - r.p) * rp
                                    proj = proj_p0 if proj_p0.dot(proj_p0) < proj_p1.dot(proj_p1) else proj_p1
                                    t1 = proj.length() / r.direction.length()
                                    if t1 < 0:  # behind of origin point of ray
                                        if logger.level == logging.DEBUG:
                                            logger.debug("det 0 t1 < 0")
                                        continue
                            else:
                                # find intersection point
                                t2 = (r_d.x * (s_p.y - r_p.y) + r_d.y * (r_p.x - s_p.x)) / det

                                if t2 < 0:  # outside of segment (p1)
                                    if -t2 - 0 > ViewPolygon.TOLERANCE:  # t2 < 0
                                        if logger.level == logging.DEBUG:
                                            logger.debug("t2 < 0")
                                        continue
                                    else:
                                        logger.debug("to remove 1")
                                if t2 > 1:  # outside of segment (p2)
                                    if t2 - 1 > ViewPolygon.TOLERANCE:  # t2 > 1
                                        if logger.level == logging.DEBUG:
                                            logger.debug("t2 > 1")
                                        continue
                                    else:
                                        logger.debug("to remove 2")

                                # // Plug the value of T2 to get T1 # T1 = (s_px+s_dx*T2-r_px)/r_dx
                                if r_d.x != 0:
                                    t1 = (s_p.x + s_d.x * t2 - r_p.x) / r_d.x
                                elif r_d.y != 0:
                                    t1 = (s_p.y + s_d.y * t2 - r_p.y) / r_d.y
                                else:
                                    raise Exception("0-Vector!")

                                if t1 < 0:  # behind of origin point of ray
                                    if -t1 - 0 > ViewPolygon.TOLERANCE:  # t1 < 0
                                        if logger.level == logging.DEBUG:
                                            logger.debug("t1 < 0")
                                        continue
                                    else:
                                        logger.debug("to remove 3")

                        # keep track of nearest values
                        if s == nearest:
                            raise Exception("not possible, already removed")
                            nearest_t1 = t1  # t1 value for the currently nearest segment
                        elif t1 < new_nearest_t1:  # keep only nearest to ray origin
                            new_nearest_t1 = t1  # t1 value for the potentially new nearest segment
                            new_nearest = s
                        elif t1 == new_nearest_t1:  # special case where two segments have the same distance
                            # find shared point (same distance on ray)
                            n0 = s.p1
                            if (s.p0.x == new_nearest.p0.x and s.p0.y == new_nearest.p0.y) or \
                                    (new_nearest.p0.x == new_nearest.p1.x and s.p0.y == new_nearest.p1.y):
                                n0 = s.p0
                            # find direction vectors pointing away from n0
                            n1 = s.p0
                            if n1.x == n0.x and n1.y == n0.y:
                                n1 = s.p1
                            nc = (n1 - n0).normalize().dot(r.direction)  # calculate angle between ray and segment
                            nn1 = new_nearest.p0
                            if nn1.x == n0.x and nn1.y == n0.y:
                                nn1 = new_nearest.p1
                            nnc = (nn1 - n0).normalize().dot(r.direction)  # calculate angle between ray and segment
                            if nc < nnc:
                                new_nearest = s
                            new_nearest_t1 = t1

                    # if nearest is None: # in case there is no suitable in open_segments... keep the one
                    #     nearest = r.segment
                else:
                    # behind nearest removed, do nothing
                    continue
            #
            # # print("check")

            if nearest != new_nearest:
                # if nearest_t1 < new_nearest_t1:  # the nearest is still the nearest, do nothing
                if new_nearest_t1 - nearest_t1 > ViewPolygon.TOLERANCE:  # the nearest is still the nearest, do nothing
                    raise Exception("new_nearest should only be different if it is nearer!!!")

                assert new_nearest is not None

                # if nearest_t1 == new_nearest_t1:  # find the nearest one
                #     # find shared point (same distance on ray)
                #     # raise Exception("this is already known")
                #     n0 = nearest.p1
                #     if (nearest.p0.x == new_nearest.p0.x and nearest.p0.y == new_nearest.p0.y) or \
                #             (nearest.p0.x == new_nearest.p1.x and nearest.p0.y == new_nearest.p1.y):
                #         n0 = nearest.p0
                #     # find direction vectors pointing away from n0
                #     n1 = nearest.p0
                #     if n1.x == n0.x and n1.y == n0.y:
                #         n1 = nearest.p1
                #     nc = (n1 - n0).normalize().dot(r.direction)  # calculate angle between ray and segment
                #     nn1 = new_nearest.p0
                #     if nn1.x == n0.x and nn1.y == n0.y:
                #         nn1 = new_nearest.p1
                #     nnc = (nn1 - n0).normalize().dot(r.direction)  # calculate angle between ray and segment
                #     if nc < nnc:
                #         raise Exception("this is not true 2")

                if r.begin:
                    if nearest:
                        assert nearest is not None, "assert0.0"
                        if nearest_t1 != sys.maxsize:
                            assert nearest_t1 != sys.maxsize, "assert0"
                            p0 = Intersection(r, nearest, nearest_t1)  # add point on nearest first
                            intersection_points.append(p0)
                        else:
                            raise Exception("nearest_t1 should not be sys.maxsize")
                            if logger.level == logging.DEBUG:
                                logger.debug("assert nearest_t1 != sys.maxsize, assert0")
                        assert new_nearest_t1 is not None, "assert1.0"
                        assert new_nearest_t1 != sys.maxsize, "assert1"
                        p1 = Intersection(r, new_nearest, new_nearest_t1)
                        intersection_points.append(p1)
                        nearest = new_nearest
                    else:
                        raise Exception("superfluous 1")
                else:

                    if not r.begin and r.segment == nearest:
                        # raise Exception("this should not happen!")
                        p0 = Intersection(r, nearest, 1)
                        intersection_points.append(p0)

                    if new_nearest:
                        assert new_nearest is not None, "assert4.0"
                        assert new_nearest_t1 != sys.maxsize, "assert4"
                        p1 = Intersection(r, new_nearest, new_nearest_t1)
                        intersection_points.append(p1)
                        nearest = new_nearest
                    else:
                        raise Exception("there is always a new nearest")

        # print("volume done", len(intersection_points))
        return intersection_points


logger.debug("imported")
