# -*- coding: utf-8 -*-

"""
TODO: docstring
"""

__version__ = '$Id: __init__.py 27 2009-05-06 21:37:20Z dr0iddr0id $'

import random

import pygame

import pyknicpygame
from pyknicpygame.pyknic.mathematics import Point2
from pyknicpygame.pyknic.mathematics import Vec2

import settings

class Credits(pyknicpygame.pyknic.context.Context):
    """
    The context class.
    """
    def __init__(self):
        self.text = _("DR0ID (C) April 2013 | pyweek 16")
        self.renderer = None
        self.cam = None
        self.items = []
        self.rect = None

    def enter(self):
        """Called when this context is pushed onto the stack."""
        pygame.event.clear()

        sw, sh = settings.resolution
        
        # self.rect = pygame.Rect(0, 0, sw / 2, 3 * sh / 4)
        self.rect = pygame.Rect(0, 0, sw / 2, sw / 2)
        self.rect.center = ( sw / 2, sh / 2)

        pyknicpygame.spritesystem.TextSprite.font = pygame.font.Font(None, 30)

        self.renderer = pyknicpygame.spritesystem.DefaultRenderer()
        self.cam = pyknicpygame.spritesystem.Camera(pygame.Rect(0, 0, sw, sh), position=Point2(sw/2, sh/2))

        pos = Point2(sw / 2, sh / 2)
        sprite = pyknicpygame.spritesystem.TextSprite(self.text, pos, anchor="center")
        
        self.renderer.add_sprite(sprite)
        self.velocity = Vec2(random.randint(50, 150), random.randint(50, 150))
        self.zoom_factor = 1

        sprite.configure_image_cache(False, rotation_precision=2, zoom_precision=2, max_cache_entry_count=1000, single_sprite=True)
        sprite.rotation = random.randint(-45, 45)
        sprite.zoom = random.random() + 0.5


    def exit(self):
        """Called when this context is popped off the stack."""
        pass
    def suspend(self):
        """Called when another context is pushed on top of this one."""
        pass
    def resume(self):
        """Called when another context is popped off the top of this one."""
        pass
    def think(self, delta_time):
        """Called once per frame"""
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pyknicpygame.pyknic.context.pop()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                pyknicpygame.pyknic.context.pop()

        sprite = self.renderer.get_sprites()[0]

        if self.rect.collidepoint(sprite.position.as_tuple()):
            pass
        else:
            if sprite.position.y < self.rect.top and self.velocity.dot(Vec2(0, 1)) < 0.0:
                self.velocity.y *= -1
            elif sprite.position.y > self.rect.bottom and self.velocity.dot(Vec2(0, -1)) < 0.0:
                self.velocity.y *= -1
            if sprite.position.x < self.rect.left and self.velocity.dot(Vec2(1, 0)) < 0.0:
                self.velocity.x *= -1
            elif sprite.position.x > self.rect.right and self.velocity.dot(Vec2(-1, 0)) < 0.0:
                self.velocity.x *= -1
        
        sprite.position += self.velocity * delta_time
        
        sprite.rotation += 10.0 * delta_time

        if sprite.zoom > 2 and self.zoom_factor == 1:
            self.zoom_factor = -1
        elif sprite.zoom < 0.5 and self.zoom_factor == -1:
            self.zoom_factor = 1
        sprite.zoom += self.zoom_factor * 0.5 * delta_time
        
        
        
    def draw(self, screen):
        """Refresh the screen"""
        self.renderer.draw(screen, self.cam, fill_color=(0,0,0), do_flip=True)

