# -*- coding: utf-8 -*-

"""
TODO: docstring
"""

import random
import logging
import math

import pygame

import pyknicpygame
from pyknicpygame.pyknic.mathematics import Point2
from pyknicpygame.pyknic.mathematics import Vec2

import settings
import dialog

logger = logging.getLogger("intro")
logger.setLevel(pyknicpygame.pyknic.settings['log_level'])

class FocusTextSprite(pyknicpygame.spritesystem.TextSprite):

    def __init__(self, text, pos, font=None, color=None, anchor=None):
        pyknicpygame.spritesystem.TextSprite.__init__(self, text, pos, font=font, color=color, anchor=anchor)
        self.original_color = color

    def focus(self, focus=False):
        if focus:
            self.color = (255, 255, 0)
        else:
            self.color = self.original_color
        # trigger a rerender
        self.text = self.text


class Dialog(pyknicpygame.pyknic.context.Context):
    """
    The context class.
    """
    def __init__(self, dialogs, renderer=None, cam=None):
        self.dialogs = list(dialogs)
        self.current_dialog = None
        self.renderer = renderer
        self.cam = cam
        self.text_sprites = []
        self.guy_sprite = None
        self.talker_sprite = None
        self.answer_sprite = None
        self.cant_leave_sprite = None
        self.scheduler = pyknicpygame.pyknic.timing.Scheduler()
        self.scheduled_id = None
        self._time = 0


    def enter(self):
        """Called when this context is pushed onto the stack."""
        pygame.event.clear()
        self.done = False

        sw, sh = settings.resolution

        if self.renderer is None:
            self.renderer = pyknicpygame.spritesystem.DefaultRenderer()
        if self.cam is None:
            self.cam = pyknicpygame.spritesystem.Camera(pygame.Rect(0, 0, sw, sh), position=Point2(sw/2, sh/2))
            
        # TODO: load level dialogs
        self._load_dialog(self.dialogs.pop(0))
        
    def _load_dialog(self, dialog):
        # self.renderer.clear()
        for spr in self.text_sprites:
            self.renderer.remove_sprite(spr)
        self.renderer.remove_sprite(self.answer_sprite)
        self.text_sprites = []
        sw, sh = settings.resolution
        if self.current_dialog is None or self.current_dialog.guy_image_path != dialog.guy_image_path:
            if self.guy_sprite is not None:
                self.renderer.remove_sprite(self.guy_sprite)
            if dialog.guy_image_path is not None:
                guy_image = self._load_image(dialog.guy_image_path)
                guy_spr = pyknicpygame.spritesystem.Sprite(guy_image , Point2(sw, 0), anchor='topright')
                guy_spr.z_layer = 900
                self.guy_sprite = guy_spr
                self.add_sprite(guy_spr)

        if self.current_dialog is None or self.current_dialog.talker_image_path != dialog.talker_image_path:
            if self.talker_sprite is not None:
                self.renderer.remove_sprite(self.talker_sprite)
            if dialog.talker_image_path is not None:
                talker_image = self._load_image(dialog.talker_image_path)
                talker_spr = pyknicpygame.spritesystem.Sprite(talker_image , Point2(0, 0), anchor='topleft')
                talker_spr.z_layer = 900
                self.talker_sprite = talker_spr
                self.add_sprite(talker_spr)
        
        
        dialog_font = pygame.font.Font(dialog.font_name, 20)
        row = 0
        delay = 0
        for idx, textline in enumerate(dialog.initial):
            delay += len(textline.split()) * 0.15
            for text in textline.split('\n'):
                pos = Point2(20, sh - 150 + row * 30)
                spr = pyknicpygame.spritesystem.TextSprite(text, pos, font=dialog_font, color=dialog.talker_color , anchor="topleft")
                self.text_sprites.append(spr)
                row += 1

        pos = Point2(sw - 165, sh - 150)
        self.answer_sprite = FocusTextSprite(dialog.answer, pos, font=dialog_font, color=dialog.guy_color , anchor="midtop")
            
        # self.renderer.add_sprites(self.text_sprites)
        for sprite in self.text_sprites:
            self.add_sprite(sprite)
        # self.renderer.add_sprite(self.answer_sprite)
        
        if self.scheduled_id is not None:
            self.scheduler.remove(self.scheduled_id)
        
        def _add(spr, cont):
            cont.add_sprite(spr)
            return 0.0
        self.scheduled_id = self.scheduler.schedule(_add, delay, 0, self.answer_sprite, self)

        font = pygame.font.Font(dialog.font_name, 35)
        self.cant_leave_sprite = pyknicpygame.spritesystem.TextSprite(_("Sorry, you can't leave right now!"), Point2(sw//2, sh//2), font=font, color=(255, 0, 0) , anchor="center")
        
        self.current_dialog = dialog
        
    def _load_image(self, name):
        return pygame.image.load(name).convert_alpha()
        
    def add_sprite(self, sprite):
        sprite.alpha = 0
        def _update(self, dt):
            self.alpha += (255 - self.alpha) * 0.1
        sprite.update = _update
        self.renderer.add_sprite(sprite)
        
    def exit(self):
        """Called when this context is popped off the stack."""
        pass

    def suspend(self):
        """Called when another context is pushed on top of this one."""
        pass

    def resume(self):
        """Called when another context is popped off the top of this one."""
        pass

    def think(self, delta_time):
        """Called once per frame"""
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pyknicpygame.pyknic.context.pop()
                    pyknicpygame.pyknic.context.pop()
                elif event.key == pygame.K_SPACE:
                    self._change_to_next_dialog()
                if __debug__:
                    if event.key == pygame.K_F11:
                        self.done = True
            elif event.type == pygame.MOUSEBUTTONDOWN:
                hit_items = self.renderer.get_sprites_in_rect(pygame.Rect(event.pos, (1, 1)))
                if hit_items:
                    item = hit_items[0]
                    if item == self.answer_sprite:
                        self._change_to_next_dialog()
            elif event.type == pygame.MOUSEMOTION:
                self.answer_sprite.focus(False)
                hit_items = self.renderer.get_sprites_in_rect(pygame.Rect(event.pos, (1, 1)))
                if hit_items:
                    fitem = hit_items[0]
                    if fitem == self.answer_sprite:
                        fitem.focus(True)
            elif event.type == pygame.QUIT:
                if self.cant_leave_sprite:
                    self.add_sprite(self.cant_leave_sprite)
                    def _rm(spr, rnd):
                        rnd.remove_sprite(spr)
                        return 0.0
                    self.scheduler.schedule(_rm, 3, 0, self.cant_leave_sprite, self.renderer)
                
        self.scheduler.update(delta_time)
        
        self._time += delta_time
        # self.talker_sprite.position.y = 5 * math.sin(2 * math.pi * self._time / 4.0)
        
        for spr in self.renderer.get_sprites():
            spr.update(spr, delta_time)

        if self.done:
            pyknicpygame.pyknic.context.pop()

    def draw(self, screen):
        """Refresh the screen"""
        cont = pyknicpygame.pyknic.context.top(1)
        if cont:
            cont.draw(screen, False)
            screen.fill((128, 128, 128), None, pygame.BLEND_RGB_SUB)
        self.renderer.draw(screen, self.cam, do_flip=True)

    def _change_to_next_dialog(self):
        if self.dialogs:
            self._load_dialog(self.dialogs.pop(0))
        else:
            self.done = True
        
    def _get_next_level(self):
        cont_cls, cls, args = self.level.next
        level = None
        if cls:
            level = cls(*args)
        cont = cont_cls(level)
        return cont
            
