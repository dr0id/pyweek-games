# -*- coding: utf-8 -*-

"""
TODO: docstring
"""

import random

import pyknicpygame
from pyknicpygame.pyknic.mathematics import Point2
from pyknicpygame.pyknic.mathematics import Vec2

import settings
import core
import intro



class LevelBase(object):

    def __init__(self, num_cells_x, num_cells_y, next=None):
        self.num_cells_x = num_cells_x
        self.num_cells_y = num_cells_y
        self.source_tile_pos = (self.num_cells_x // 2, self.num_cells_y)
        self.target_tile_pos = (self.num_cells_x // 2, -1)
        self.player_speed = 0.5
        self.mutation_delay = 2 # insert a tile all 2 seconds
        self.tiles = []
        self.tile_image_name = "tile.png"
        self.tile_width = w = 66
        self.tile_height = h = 66
        x = 0
        y = 0
        self.tile_coords = (
                        (settings.type_tile_l_bl,  (x + w * 0, y, w, h), 0),
                        (settings.type_tile_l_tl,  (x + w * 0, y, w, h), 1),
                        (settings.type_tile_l_tr,  (x + w * 0, y, w, h), 2),
                        (settings.type_tile_l_br,  (x + w * 0, y, w, h), 3),
                        (settings.type_tile_bar_h, (x + w * 1, y, w, h), 0),
                        (settings.type_tile_bar_v, (x + w * 1, y, w, h), 1),
                        (settings.type_tile_cross, (x + w * 2, y, w, h), 0),
                        (settings.type_tile_t_u,   (x + w * 3, y, w, h), 0),
                        (settings.type_tile_t_r,   (x + w * 3, y, w, h), 1),
                        (settings.type_tile_t_b,   (x + w * 3, y, w, h), 2),
                        (settings.type_tile_t_l,   (x + w * 3, y, w, h), 3),
                        (settings.type_border_r,   (x + w * 4, y, w, h), 0),
                        (settings.type_border_b,   (x + w * 4, y, w, h), 1),
                        (settings.type_border_l,   (x + w * 4, y, w, h), 2),
                        (settings.type_border_t,   (x + w * 4, y, w, h), 3),
                        (settings.type_border_c_tl,   (x + w * 5, y, w, h), 0),
                        (settings.type_border_c_tr,   (x + w * 5, y, w, h), 1),
                        (settings.type_border_c_br,   (x + w * 5, y, w, h), 2),
                        (settings.type_border_c_bl,   (x + w * 5, y, w, h), 3),
                        )
        self.cursor_image_name = "cursor.png"
        self.cursor_coords = (
                        (settings.CURSOR, (x + w * 1, y, w, h), 0),
                        (settings.W,  (x + w * 0, y, w, h), 0),
                        (settings.N,  (x + w * 0, y, w, h), 1),
                        (settings.E,  (x + w * 0, y, w, h), 2),
                        (settings.S,  (x + w * 0, y, w, h), 3),
                        (settings.W_RED,  (x + w * 2, y, w, h), 0),
                        (settings.N_RED,  (x + w * 2, y, w, h), 1),
                        (settings.E_RED,  (x + w * 2, y, w, h), 2),
                        (settings.S_RED,  (x + w * 2, y, w, h), 3),
                        )
        self.create_random()
        self.next = next
        self.start_pos = Vec2(1.5*66, 1.5*66)
        ghost_color = (128, 255, 238)
        self.crush_dialog = [
                            DialogSnippet([_("Gotcha! You have nearly been \ncrushed by the wall! \nWatch your step!")], 
                                        _("Ohh, thanks!"), "guy.png", "ghost.png", (ghost_color)),
                            ]


    def get_tile_image_path(self):
        return core.get_path(self.tile_image_name)
        
    def create_random(self):
        tile_types = [
                    settings.type_tile_l_bl, 
                    settings.type_tile_l_tl, 
                    settings.type_tile_l_tr, 
                    settings.type_tile_l_br, 
                    settings.type_tile_l_bl, 
                    settings.type_tile_l_tl, 
                    settings.type_tile_l_tr, 
                    settings.type_tile_l_br, 
                    
                    settings.type_tile_bar_h, 
                    settings.type_tile_bar_v, 
                    settings.type_tile_bar_h, 
                    settings.type_tile_bar_v, 
                    
                    # settings.type_tile_cross, 
                    
                    settings.type_tile_t_u, 
                    settings.type_tile_t_r, 
                    settings.type_tile_t_b, 
                    settings.type_tile_t_l, 
                    settings.type_tile_t_u, 
                    settings.type_tile_t_r, 
                    settings.type_tile_t_b, 
                    settings.type_tile_t_l, 
                ]
        
        self.tiles = []
        
        for y in range(self.num_cells_y):
            self.tiles.append([])
            for x in range(self.num_cells_x):
                self.tiles[y].append((random.choice(tile_types), False))
    
class DialogSnippet(object):
    
    def __init__(self, initial, answer, guy_image_path, talker_image_path, talker_color, guy_color=(0, 127, 14), font_name="nvvzs.ttf"):
        self.initial = initial # [text, ]
        self.answer = answer # text
        self.guy_image_path = core.get_path(guy_image_path)
        self.talker_image_path = core.get_path(talker_image_path)
        self.guy_color = guy_color
        self.talker_color = talker_color
        self.font_name = font_name
        if font_name:
            self.font_name = core.get_path(font_name)
        
class Level0(object):
    
    def __init__(self):
        self.next = (core.Core, Level1, (3, 3))
        nemesis_color = (255, 160, 0)
        self.dialogs = [
                        DialogSnippet([_("")], 
                                        _("Ahh, you scared me!"), "guy.png", "nemesis.png", nemesis_color),
                        DialogSnippet([_("Do you know who I am?")], 
                                        _("No, who are you?"), "guy.png", "nemesis.png", nemesis_color),
                        DialogSnippet([_("I'm Nemesis, the goddess of revenge."), _("Do you know why I'm here?")], 
                                        _("I can only guess!?"), "guy.png", "nemesis.png", nemesis_color),
                        DialogSnippet([_("To punish you for pyweek!")], 
                                        _("What for?"), "guy.png", "nemesis.png", nemesis_color),
                        DialogSnippet([_("You are steeling time from \nothers, therefore I punish you by \nforcing you to play this game!")], 
                                        _("This isn't fair!"), "guy.png", "nemesis.png", nemesis_color),
                        DialogSnippet([_("Yes it is! \nYour punishment is to play this \ngame, find your way out!")], 
                                        _("Oh nooo..."), "guy.png", "nemesis.png", nemesis_color),
                        ]
        
class Level1(LevelBase):

    def __init__(self, num_cells_x, num_cells_y, next=None):
        LevelBase.__init__(self, num_cells_x, num_cells_y, next)
        self.tile_image_name = 'tiles.png'
        self.next = (core.Core, Level2, (5, 5, None))
        self.mutation_delay = 4 # insert a tile all x seconds

        ghost_color = (128, 255, 238)
        self.dialogs = [
                        DialogSnippet([_("Huuuuhuu!")], 
                                        _("Aaaaaahh! You scared me too!"), "guy.png", "ghost.png", (ghost_color)),
                        DialogSnippet([_("Hahahahaha!"), _("Who are you?")], 
                                        _("I have been sent here!"), "guy.png", "ghost.png", ghost_color),
                        DialogSnippet([_("Oh no, she did it again!?")], 
                                        _("You know Nemesis?"), "guy.png", "ghost.png", ghost_color),
                        DialogSnippet([_("Well, a little bit. \nYou are in trouble!")], 
                                        _("Why?"), "guy.png", "ghost.png", ghost_color),
                        DialogSnippet([_("You are in the labyrinth of death!\n No one has escaped alive ever!")], 
                                        _("This sounds encouraging!"), "guy.png", "ghost.png", ghost_color),
                        DialogSnippet([_("This labyrinth is always changing, \nwhatch out for traps!")], 
                                        _("I have no other choice :("), "guy.png", "ghost.png", ghost_color),
                        DialogSnippet([_("Well, here I have a little help, \nI have stolen this remote control from Nemesis!\nIt controls the grid. \nTake it!")], 
                                        _("Thanks!"), "guy.png", "ghost.png", ghost_color),
                        ]

class Level2(LevelBase):

    def __init__(self, num_cells_x, num_cells_y, next=None):
        LevelBase.__init__(self, num_cells_x, num_cells_y, next)
        self.tile_image_name = 'tiles.png'
        self.next = (core.Core, Level3, (9, 9, None))
        self.mutation_delay = 2 # insert a tile all x seconds

        ghost_color = (128, 255, 238)
        self.dialogs = []
        
                        
class Level3(LevelBase):

    def __init__(self, num_cells_x, num_cells_y, next=None):
        LevelBase.__init__(self, num_cells_x, num_cells_y, next)
        self.tile_image_name = 'tiles.png'
        self.next = (core.Core, Level35, (3, 3, None))
        self.mutation_delay = 1 # insert a tile all x seconds
        ghost_color = (128, 255, 238)
        nemesis_color = (255, 160, 0)
        self.dialogs = [
                        DialogSnippet([_("Whatch out!")], 
                                        _("Huh?"), "guy.png", "ghost.png", (ghost_color)),
                        DialogSnippet([_("WHERE DID YOU GET THIS?!")], 
                                        _("Errr..."), "guy.png", "nemesis.png", nemesis_color),
                        DialogSnippet([_("Oh I see, this sneaky Ghost! \nYou will still be in here forever!")], 
                                        _("No more please!"), "guy.png", "nemesis.png", nemesis_color),
                        ]

class Level35(LevelBase):

    def __init__(self, num_cells_x, num_cells_y, next=None):
        LevelBase.__init__(self, num_cells_x, num_cells_y, next)
        self.tile_image_name = 'tiles.png'
        self.next = (core.Core, Level4, (9, 9, None))
        self.mutation_delay = 0.5 # insert a tile all x seconds

        ghost_color = (128, 255, 238)
        self.dialogs = []
                        
class Level4(LevelBase):

    def __init__(self, num_cells_x, num_cells_y, next=None):
        LevelBase.__init__(self, num_cells_x, num_cells_y, next)
        self.tile_image_name = 'tiles.png'
        self.next = (intro.Intro, Level5, tuple())
        self.mutation_delay = 1 # insert a tile all x seconds

        ghost_color = (128, 255, 238)
        self.dialogs = []

class Level5(object):
    
    def __init__(self):
        self.next = None
        nemesis_color = (255, 160, 0)
        ghost_color = ghost_color = (128, 255, 238)
        self.mutation_delay = 1 # insert a tile all x seconds
        self.dialogs = [
                        DialogSnippet([_("Oooh noooo! You did it!")], 
                                        _("Yes!"), "guy.png", "nemesis.png", nemesis_color),
                        DialogSnippet([_("Well this was revenge enough.\nRemember: behave!")], 
                                        _("I will."), "guy.png", "nemesis.png", nemesis_color),
                                        
                        DialogSnippet([_("Good by!")], 
                                        _("Bye!"), "guy.png", "nemesis.png", nemesis_color),
                        
                        DialogSnippet([_("Glad you did it!")], 
                                        _("Yeah... thanks for your help."), "guy.png", "ghost.png", ghost_color),
                        DialogSnippet([_("Thanks for playing through!")], 
                                        _("Ok, I wasted enough time."), "guy.png", "ghost.png", ghost_color),
                        DialogSnippet([_("Oh yes, sorry!\n\n Good bye.")], 
                                        _("Bye!"), "guy.png", "ghost.png", ghost_color),
                        ]

                        
class Instructions(object):
    
    def __init__(self):
        self.next = None
        instr_color = (255, 160, 0)
        self.dialogs = [
                        DialogSnippet([_("To advance a dialog you have\n to click on the text on \nthe right side ->")], 
                                        _("Click here"), "glowarrow.png", None, instr_color),
                        DialogSnippet([_("Dialog text can be skipped by \npressing 'space'")], 
                                        _("Next..."), None, None, instr_color),
                        DialogSnippet([_("The game will look like this.\nThe goal is to get to the exit\n on the other side")], 
                                        _("Next..."), None, "game.png", instr_color),
                        DialogSnippet([_("The yellow lines indicate the available paths.\n Only if a path to the exit exists,\n our hero will start walking along it.\n Eventually arriving at the exit.")], 
                                        _("Next..."), None, "game.png", instr_color),
                        DialogSnippet([_("To reach the exit, the grid has to be modified,\nthe yellow cursor can be placed with the mouse\n to move that row or column.")], 
                                        _("Next..."), None, "game.png", instr_color),
                        DialogSnippet([_("The arrows in the cursor indicate\nif this row or column can\nbe moved.")], 
                                        _("Next..."), "game.png", "cursorinstructionalldirections.png", instr_color),
                        DialogSnippet([_("Either the column (left image)\nor the row (right image)\n can be blocked.")], 
                                        _("Next..."), "cursorinstructionrowblocked.png", "cursorinstructioncolblocked.png", instr_color),
                        DialogSnippet([_("The row can be moved by pressing\n   a, d   \nthe colum can be moved by pressing\n   s, w   \n on the keyboard.")], 
                                        _("Next..."), None, "game.png", instr_color),
                        DialogSnippet([_("Have Fun!")], 
                                        _("Done"), "game.png", None, instr_color),
                        ]