# -*- coding: utf-8 -*-

"""
TODO: docstring
"""

import random
import logging

import pygame

import pyknicpygame
from pyknicpygame.pyknic.mathematics import Point2
from pyknicpygame.pyknic.mathematics import Vec2

import settings
import dialog

logger = logging.getLogger("intro")
logger.setLevel(pyknicpygame.pyknic.settings['log_level'])



class Intro(pyknicpygame.pyknic.context.Context):
    """
    The context class.
    """
    def __init__(self, level, renderer=None, cam=None):
        self.level = level
        self.renderer = renderer
        self.cam = cam

    def enter(self):
        """Called when this context is pushed onto the stack."""
        pygame.event.clear()
        self.done = False

        sw, sh = settings.resolution

        if self.renderer is None:
            self.renderer = pyknicpygame.spritesystem.DefaultRenderer()
        if self.cam is None:
            self.cam = pyknicpygame.spritesystem.Camera(pygame.Rect(0, 0, sw, sh), position=Point2(sw/2, sh/2))
            
        # TODO: load level dialogs
        cont = dialog.Dialog(self.level.dialogs)
        pyknicpygame.pyknic.context.push(cont)
        
        
    def exit(self):
        """Called when this context is popped off the stack."""
        pass

    def suspend(self):
        """Called when another context is pushed on top of this one."""
        pass

    def resume(self):
        """Called when another context is popped off the top of this one."""
        self.done = True

    def think(self, delta_time):
        """Called once per frame"""
        for event in pygame.event.get():
            if __debug__:
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        pyknicpygame.pyknic.context.pop()
                        pyknicpygame.pyknic.context.pop()
                    elif event.key == pygame.K_F11:
                        self.done = True
            else:
                pass

        if self.done:
            pyknicpygame.pyknic.context.pop()
            if self.level.next:
                pyknicpygame.pyknic.context.push(self._get_next_level())
            else:
                pass

    def draw(self, screen, do_flip=True):
        """Refresh the screen"""
        self.renderer.draw(screen, self.cam, fill_color=(0,0,0), do_flip=do_flip)

    def _get_next_level(self):
        cont_cls, cls, args = self.level.next
        level = None
        if cls:
            level = cls(*args)
        cont = cont_cls(level)
        return cont
            
