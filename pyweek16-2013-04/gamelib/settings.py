# -*- coding: utf-8 -*-

"""
TODO: docstring
"""

__version__ = '$Id: __init__.py 27 2009-05-06 21:37:20Z dr0iddr0id $'

import pygame


resolution = (800, 660)
caption = "pyweek16 - Nemesis"  # TODO: game name!


sim_time_step = 0.025

# def _create_ascending_numbers(base):
    # i = 0
    # while True:
        # yield base + i
        # i += 1

# _ascending_numbers = _create_ascending_numbers(1000)

# 1<<30  == 1073741824  <- biggest non 'L' int
player_move_left        = 1000
player_move_right       = 1001
player_move_up          = 1002
player_move_down        = 1003
player_stop_left        = 1004
player_stop_right       = 1005
player_stop_up          = 1006
player_stop_down        = 1007
player_dir              = 1008
player_start_flooding   = 1009
player_stop_flooding    = 1010



key_map = {
            (pygame.KEYDOWN, pygame.K_a) : player_move_left, 
            (pygame.KEYDOWN, pygame.K_d) : player_move_right,
            (pygame.KEYDOWN, pygame.K_w) : player_move_up,
            (pygame.KEYDOWN, pygame.K_s) : player_move_down,
            (pygame.KEYUP, pygame.K_a) : player_stop_left, 
            (pygame.KEYUP, pygame.K_d) : player_stop_right,
            (pygame.KEYUP, pygame.K_w) : player_stop_up,
            (pygame.KEYUP, pygame.K_s) : player_stop_down,
            (pygame.KEYDOWN, pygame.K_LEFT) : player_move_left, 
            (pygame.KEYDOWN, pygame.K_RIGHT) : player_move_right,
            (pygame.KEYDOWN, pygame.K_UP) : player_move_up,
            (pygame.KEYDOWN, pygame.K_DOWN) : player_move_down,
            (pygame.KEYUP, pygame.K_LEFT) : player_stop_left, 
            (pygame.KEYUP, pygame.K_RIGHT) : player_stop_right,
            (pygame.KEYUP, pygame.K_UP) : player_stop_up,
            (pygame.KEYUP, pygame.K_DOWN) : player_stop_down,
            (pygame.MOUSEBUTTONDOWN, 1) : player_start_flooding,
            (pygame.MOUSEBUTTONUP, 1) : player_stop_flooding,
            (pygame.MOUSEMOTION, None) : player_dir,
            }


domain_name = "DR0IDs-pyweek16-nemesis-game" # used for translation files
            
# def _create_type_generator():
    # import sys
    # i = 0
    # while True:
        # val = 1 << i
        # # TODO: this allows for 31 types, is this enough?
        # if val > sys.maxsize:
            # raise OverflowError()
        # yield val
        # i += 1

# _type_generator = _create_type_generator()

# game types
type_none = 0

type_player     = 1 << 0

type_tile_l_bl  = 1 << 1
type_tile_l_tl  = 1 << 2
type_tile_l_tr  = 1 << 3
type_tile_l_br  = 1 << 4
type_tile_bar_h = 1 << 5
type_tile_bar_v = 1 << 6
type_tile_cross = 1 << 7
type_tile_t_u   = 1 << 8
type_tile_t_r   = 1 << 9
type_tile_t_b   = 1 << 10
type_tile_t_l   = 1 << 11

type_trigger    = 1 << 12

type_border_l   = 1 << 13
type_border_r   = 1 << 14
type_border_t   = 1 << 15
type_border_b   = 1 << 16
type_border_c_tl= 1 << 17
type_border_c_tr= 1 << 18
type_border_c_bl= 1 << 19
type_border_c_br= 1 << 20


type_all = ((~(1 << 31)) + 2) * -1 # == 2147483647 this is sys.maxsize (on 32 bit)

type_all_tiles = type_tile_l_bl | type_tile_l_tl | type_tile_l_tr | type_tile_l_br | type_tile_bar_h | \
                    type_tile_bar_v | type_tile_cross | type_tile_t_u | type_tile_t_r | type_tile_t_b | type_tile_t_l

type_all_border = type_border_l | type_border_r | type_border_t | type_border_b | type_border_c_tl | type_border_c_tr | type_border_c_bl | type_border_c_br

                    
assert type_all == (0xFFFFFFFF >> 1), "\n{0:b} != \n{1:b}".format(type_all, 0xFFFFFFFF >> 1)

N = 0
E = 1
S = 2
W = 3
N_RED = N + 10
E_RED = E + 10
S_RED = S + 10
W_RED = W + 10
CURSOR = 666

connections = {
            type_tile_l_bl : [N, E],
            type_tile_l_tl : [E, S],
            type_tile_l_tr : [S, W],
            type_tile_l_br : [N, W],
            type_tile_bar_h : [E, W],
            type_tile_bar_v : [N, S],
            type_tile_cross : [N, E, S, W],
            type_tile_t_u : [N, E, W],
            type_tile_t_r : [N, E, S],
            type_tile_t_b : [E, S, W],
            type_tile_t_l : [N, S, W],
                }

directions = [(E, 1, 0), (W, -1, 0), (S, 0, 1), (N, 0, -1)]



if __name__ == '__main__':
    # _type_generator = _create_type_generator()
    import sys
    # print('maxsize:', sys.maxsize)
    # try:
        # for i in range(100):
            # val = next(_type_generator)
            # print("{0:15}, {1:15}, {2:>50}".format(i, val, bin(val)))
            # if val > sys.maxsize:
                # print('!!! ' + str(i))
    # except:
        # pass
    print("{0:>32}, {1:>50}".format(type_all, bin(type_all)))
    print("{0:>32}, {1:>50}".format(sys.maxsize, bin(sys.maxsize)))
