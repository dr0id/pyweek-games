# -*- coding: utf-8 -*-

"""
TODO: docstring
"""

import os
import random
import logging
import heapq
from heapq import heappush, heappop, heapify


import pygame

import pyknicpygame
from pyknicpygame.pyknic.mathematics import Point2
from pyknicpygame.pyknic.mathematics import Vec2

import settings
import dialog

logger = logging.getLogger("core")
logger.setLevel(pyknicpygame.pyknic.settings['log_level'])


# ------------------------------------------------------------------------------

def get_path(name):
    if name:
        joined_path = os.path.join(pyknicpygame.pyknic.settings['appdir'], '..', 'data', name)
        return os.path.abspath(joined_path)
    return name
# ------------------------------------------------------------------------------

class Player(pyknicpygame.spritesystem.Sprite):

    def __init__(self, cont):
        img = pygame.image.load(get_path("player.png")).convert_alpha()
        pyknicpygame.spritesystem.Sprite.__init__(self, img, Point2(0.0, 0.0), anchor='midbottom')
        self.parent = None
        self.context = cont
        self.z_layer = 300
        self.type = settings.type_player
        # self.move(cont.level.start_pos)
        self.path = []
        self.current_node_idx = None
        self.current_node = None

    def update(self, dt):
        if self.path:
            if self.current_node_idx < len(self.path):
                self.current_node = self.path[self.current_node_idx]
                dist = self.current_node.position - self.position
                self.position += dist.normalized * self.context.level.player_speed
                if dist.length_sq < 0.1:
                    self.current_node_idx += 1

    def set_to(self, to):
        self.move(to - self.position)

    def move(self, delta):
        self.position += delta
        logger.debug("moving player by parent " + str(delta)) 
        new_parent = self._calculate_parent()
        if new_parent != self.parent:
            if self.parent is not None:
                self.parent.children.remove(self)
            if new_parent is not None:
                new_parent.children.append(self)
            logger.debug("setting new parent %s to player", new_parent)
            self.parent = new_parent
            self.context.search_graph()
            
    def _calculate_parent(self):
        idx = pygame.Rect(self.rect.midbottom, (1, 1)).collidelist(self.context.tiles)
        if idx == -1:
            logger.debug("no tile found for parent for position {0}  num tiles {1}".format(self.position, len(self.context.tiles)))
            x = self.position.x // self.context.level.tile_width
            y = self.position.y // self.context.level.tile_height
            logger.debug("calculated parent at %s, %s", x, y)
            sprites = [spr for spr in self.context.tiles if spr.row == y and spr.col == x]
            if sprites:
                return sprites[0]
            else:
                pass
            return None
            return None
        tile = self.context.tiles[idx]
        # logger.debug("tile found for parent for position {0}: {1}".format(self.position, tile))
        return tile
        
    def set_path(self, path):
        self.path = path
        if len(self.path) > 0:
            self.current_node_idx = 0
            if len(path) >= 2 and self.path[1] == self.current_node:
                self.current_node_idx = 1
        
            


# ------------------------------------------------------------------------------

class Tile(pyknicpygame.spritesystem.Sprite):

    def __init__(self, col, row, type, anchored=False):
        pyknicpygame.spritesystem.Sprite.__init__(self, None, Point2(0.0, 0.0), anchor='center', do_init=False)
        self.row = row
        self.col = col
        self.type = type
        self.target = Point2(0.0, 0.0)
        self.anchored = anchored
        self.done = False
        self.children = []

    def update(self, delta_time, w, h):
        self.target = Point2(w * self.col, h * self.row)
        delta = (self.target - self.position) * 0.1
        self.position += delta
        for child in self.children:
            child.move(delta)
            
    # def __eq__(self, other):
        # return self.col == other.col and self.row == other.row
            
    # def __le__(self, other):
        # return self.type <= other.type

    def __lt__(self, other):
        return self.type < other.type
        
    def __str__(self):
        return "<Tile({0}, {1}) at {2}>".format(self.col, self.row, id(self))
        
    __repr__ = __str__

# ------------------------------------------------------------------------------

class CursorTile(pyknicpygame.spritesystem.Sprite):

    def __init__(self, renderer, image):
        pyknicpygame.spritesystem.Sprite.__init__(self, image, Point2(0.0, 0.0), anchor='center', z_layer=400, do_init=True)
        self.renderer = renderer
        self.type = settings.type_none
        
        
    def set_visible(self, vis):
        if vis:
            self.renderer.add_sprite(self)
        else:
            self.renderer.remove_sprite(self)


# ------------------------------------------------------------------------------

class Core(pyknicpygame.pyknic.context.Context):
    """
    The context class.
    """
    def __init__(self, level, renderer=None, cam=None):
        self.level = level
        self.renderer = renderer
        self.cam = cam
        self.tile_images = {}
        self.scheduler = pyknicpygame.pyknic.timing.Scheduler()
        self.tiles = []
        self.source_tile = None
        self.target_tile = None
        self.nodes = {} # {(col, row) : tile}
        self.edges = {} # {tile : [tiles]}
        self.player = None
        # graph
        self.path = []
        self.path_tree = {}
        # cursor
        self._cursor_images = {}
        self._cursor_tiles = {}
        self.graph_sprite = None

    def enter(self):
        """Called when this context is pushed onto the stack."""
        pygame.event.clear()
        self.done = False

        sw, sh = settings.resolution

        if self.renderer is None:
            self.renderer = pyknicpygame.spritesystem.DefaultRenderer()
        if self.cam is None:
            self.cam = pyknicpygame.spritesystem.Camera(pygame.Rect(0, 0, sw, sh), position=Point2(sw/2, sh/2))
            
        image = pygame.Surface((sw, sh), pygame.SRCALPHA)
        image.fill((0, 0, 0, 0), None, pygame.BLEND_RGBA_MULT)
        # image.set_colorkey((255, 0, 255)) #, pygame.RLEACCEL)
        # image.fill((255, 0, 255))
        class GSprite(pyknicpygame.spritesystem.Sprite):
            def __init__(self, image):
                pyknicpygame.spritesystem.Sprite.__init__(self, image, Point2(0.0, 0.0), anchor='topleft', z_layer=250)
                self.draw_special = True
            def draw(self, surf, cam, interpolation_factor=1.0):
                surf.blit(self.image, (0, 0))
                return self.rect
        self.graph_sprite = GSprite(image)
            
        self.graph_sprite.type = settings.type_none

        # TODO: load level sprites
        self._load_tile_images()
        self._generate_tiles()
        self._generate_graph()
        self._reset_player()
        
        pygame.key.set_repeat(300, 50)
        self.scheduler.schedule(self._insert_random_tile, self.level.mutation_delay)
        # self._show_dialog(self.level.dialogs)
        self.scheduler.schedule(self._show_dialog, 2, 0, self.level.dialogs)
        pygame.mouse.set_visible(False)
        
        
    def _reset_player(self):
        self.renderer.remove_sprite(self.player)
        self.player = Player(self)
        logger.debug("setting player to tile {0}: {1}, {2}".format(self.source_tile, self.source_tile.col, self.source_tile.row))
        self.player.set_to(self.source_tile.position)
        self.renderer.add_sprite(self.player)
        self.search_graph()

    def exit(self):
        """Called when this context is popped off the stack."""
        pygame.key.set_repeat() # disable key repeat
        pygame.mouse.set_visible(True)

    def suspend(self):
        """Called when another context is pushed on top of this one."""
        pygame.mouse.set_visible(True)

    def resume(self):
        """Called when another context is popped off the top of this one."""
        pygame.mouse.set_visible(False)

    def think(self, delta_time):
        """Called once per frame"""
        self._handle_input(delta_time)
        # TODO:
        self.scheduler.update(delta_time)
        for tile in self.tiles:
            tile.update(delta_time, self.level.tile_width, self.level.tile_height)
            
        self.player.update(delta_time)
        
        self.check_player_crushed()
        
        self._check_level_done()

        if self.done:
            self._change_to_next_level()
        
    def _change_to_next_level(self):
            if self.level.next:
                pyknicpygame.pyknic.context.pop()
                pyknicpygame.pyknic.context.push(self._get_next_level())
            else:
                pyknicpygame.pyknic.context.pop()
                
    def check_player_crushed(self):
        hits = self.renderer.get_sprites_in_rect(pygame.Rect(self.player.rect.midbottom, (1, 1)))
        hits = [hit for hit in hits if hit.type != settings.type_none]
        if len(hits) > 0 and hits[0].type & settings.type_all_border:
            self._reset_player()
            dialog_cont = dialog.Dialog(self.level.crush_dialog)
            pyknicpygame.pyknic.context.push(dialog_cont)
            
                
    def _check_level_done(self):
        dist = self.player.position - self.target_tile.position
        if dist.length_sq < 0.5:
            self.done = True
        
    def _handle_input(self, delta_time):
        for event in pygame.event.get():
            if event.type == pygame.MOUSEMOTION:
                self._update_cursor_tile()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pyknicpygame.pyknic.context.pop()

                # elif event.key == pygame.K_RIGHT:
                    # self.cam.position.x += 100 * delta_time
                # elif event.key == pygame.K_LEFT:
                    # self.cam.position.x -= 100 * delta_time
                # elif event.key == pygame.K_DOWN:
                    # self.cam.position.y += 100 * delta_time
                # elif event.key == pygame.K_UP:
                    # self.cam.position.y -= 100 * delta_time

                    
                elif event.key == pygame.K_a:
                    self._move_tiles(self.level.num_cells_x, False)
                elif event.key == pygame.K_d:
                    self._move_tiles(2 * self.level.num_cells_x + self.level.num_cells_y, False)
                elif event.key == pygame.K_w:
                    self._move_tiles(self.level.num_cells_x + self.level.num_cells_y, True)
                elif event.key == pygame.K_s:
                    self._move_tiles(0, True)

                if __debug__:    
                    if event.key == pygame.K_SPACE:
                        self.level.create_random()
                        self._load_tile_images()
                        self._generate_tiles()
                        self._reset_player()
                        self._generate_graph()
                    elif event.key == pygame.K_F9:
                        self._change_to_next_level()
                    elif event.key == pygame.K_F10:
                        self._show_dialog(self.level.dialogs)
                    elif event.key == pygame.K_F11:
                        self.done = True
                    elif event.key == pygame.K_F12:
                        tiles = {}
                        for tile in self.tiles:
                            tiles[(tile.col, tile.row)] = tile
                        for y in range(self.level.num_cells_y):
                            s = ""
                            for x in range(self.level.num_cells_x):
                                s += "{0:>4}, ".format(tiles[(y,x)])
                            print(s)
                    elif event.key == pygame.K_l:
                        self.player.move(Vec2(200 * delta_time, 0))
                    elif event.key == pygame.K_j:
                        self.player.move(Vec2(-200 * delta_time, 0))
                    elif event.key == pygame.K_k:
                        self.player.move(Vec2(0, 200 * delta_time))
                    elif event.key == pygame.K_i:
                        self.player.move(Vec2(0, -200 * delta_time))
                        
                    elif event.key == pygame.K_RETURN:
                        self._insert_random_tile()
                    
    def _show_dialog(self, dialogs):
        if dialogs:
            dialog_cont = dialog.Dialog(dialogs)
            pyknicpygame.pyknic.context.push(dialog_cont)
        return 0.0
    
    def _insert_random_tile(self):
        i = random.randint(0, 2 * self.level.num_cells_x + 2 * self.level.num_cells_y - 1)
        self.insert_tile_at(i, 1 << random.randint(1, 11))
        return self.level.mutation_delay

    def _get_next_level(self):
        cont_cls, cls, args = self.level.next
        level = None
        if cls:
            level = cls(*args)
        cont = cont_cls(level)
        return cont

    def _load_tile_images(self):
        
        image_path = self.level.get_tile_image_path()

        # source_image = pygame.image.load(image_path).convert_alpha()
        source_image = pygame.image.load(image_path).convert()
        for type, coords, rot in self.level.tile_coords:
            img = pygame.Surface((self.level.tile_width, self.level.tile_height), source_image.get_flags(), source_image)
            img.blit(source_image, (0, 0), pygame.Rect(coords))
            if rot:
                img = pygame.transform.rotate(img, -90 * rot)
            self.tile_images[type] = img
            
        source_image = pygame.image.load(get_path(self.level.cursor_image_name)).convert_alpha()
        for type, coords, rot in self.level.cursor_coords:
            img = pygame.Surface((self.level.tile_width, self.level.tile_height), source_image.get_flags(), source_image)
            img.blit(source_image, (0, 0), pygame.Rect(coords))
            if rot:
                img = pygame.transform.rotate(img, -90 * rot)
            self._cursor_images[type] = img
            
            

    def _generate_tiles(self):
        self.renderer.clear()
        self.renderer.add_sprite(self.graph_sprite)

        for y in range(self.level.num_cells_y):
            for x in range(self.level.num_cells_x):
                type, anchored = self.level.tiles[y][x]
                tile = Tile(x, y, type, anchored)
                self.tiles.append(tile)
                self.tile_to_sprite(tile)
        self.renderer.add_sprites(self.tiles)
        # generate border
        for y in range(-1, self.level.num_cells_y + 1):
            for x in range(-1, self.level.num_cells_x + 1):
                if x == -1 or x == self.level.num_cells_x or\
                        y == -1 or y == self.level.num_cells_y:
                    if x == -1:
                        type = settings.type_border_l
                        if y == -1:
                            type = settings.type_border_c_tl
                        elif y == self.level.num_cells_x:
                            type = settings.type_border_c_bl
                    elif x == self.level.num_cells_x:
                        type = settings.type_border_r
                        if y == -1:
                            type = settings.type_border_c_tr
                        elif y == self.level.num_cells_x:
                            type = settings.type_border_c_br
                    else:
                        type = settings.type_border_b
                        if y == -1:
                            type = settings.type_border_t
                    tile = Tile(x, y, type, True)
                    self.tile_to_sprite(tile)
                    tile.z_layer = 100
                    self.renderer.add_sprite(tile)
                    
        # add source and target
        col, row = self.level.source_tile_pos
        self.source_tile = Tile(col, row, settings.type_tile_bar_v , True)
        self.source_tile.z_layer = 200
        self.tile_to_sprite(self.source_tile)
        self.tiles.append(self.source_tile)
        self.renderer.add_sprite(self.source_tile)
        
        col, row = self.level.target_tile_pos
        self.target_tile = Tile(col, row, settings.type_tile_bar_v , True)
        self.target_tile.z_layer = 200
        self.tile_to_sprite(self.target_tile)
        self.tiles.append(self.target_tile)
        self.renderer.add_sprite(self.target_tile)
        
        # center view
        x = self.level.num_cells_x // 2
        y = self.level.num_cells_y // 2
        tile = [spr for spr in self.tiles if spr.col == x and spr.row == y][0]
        self.cam.position.copy_values(tile.position)
        
        for type in [
                        settings.N,
                        settings.E,
                        settings.S,
                        settings.W,
                        settings.N_RED,
                        settings.E_RED,
                        settings.S_RED,
                        settings.W_RED,
                        settings.CURSOR,
                        ]:
            self._cursor_tiles[type] = CursorTile(self.renderer, self._cursor_images[type])
        self._cursor_tiles[settings.CURSOR].set_visible(True)
        pygame.mouse.set_pos(self.cam.rect.center)
        self._update_cursor_tile()
        
    def _update_cursor_tile(self):
        pygame.mouse.set_visible(True)
        mouse_pos = pygame.mouse.get_pos()
        hits = self.renderer.get_sprites_in_rect(pygame.Rect(mouse_pos, (1, 1)))
        hits = [spr for spr in hits if spr.type & settings.type_all_tiles]
        if hits:
            tile = hits[0]
            if tile.col >= 0 and tile.col < self.level.num_cells_x and tile.row >= 0 and tile.row < self.level.num_cells_y:
                pygame.mouse.set_visible(False)
                for cursor_tile in self._cursor_tiles.values():
                    cursor_tile.col = tile.col
                    cursor_tile.row = tile.row
                    cursor_tile.position.copy_values(tile.position)
                cursor = self._cursor_tiles[settings.CURSOR]
                if self._get_anchored_tiles_in_row(cursor.row):
                    self._cursor_tiles[settings.W].set_visible(True)
                    self._cursor_tiles[settings.W_RED].set_visible(False)
                    self._cursor_tiles[settings.E].set_visible(True)
                    self._cursor_tiles[settings.E_RED].set_visible(False)
                else:
                    self._cursor_tiles[settings.W].set_visible(False)
                    self._cursor_tiles[settings.W_RED].set_visible(True)
                    self._cursor_tiles[settings.E].set_visible(False)
                    self._cursor_tiles[settings.E_RED].set_visible(True)
                if self._get_anchored_tiles_in_col(cursor.col):
                    self._cursor_tiles[settings.N].set_visible(True)
                    self._cursor_tiles[settings.N_RED].set_visible(False)
                    self._cursor_tiles[settings.S].set_visible(True)
                    self._cursor_tiles[settings.S_RED].set_visible(False)
                else:
                    self._cursor_tiles[settings.N].set_visible(False)
                    self._cursor_tiles[settings.N_RED].set_visible(True)
                    self._cursor_tiles[settings.S].set_visible(False)
                    self._cursor_tiles[settings.S_RED].set_visible(True)
                

    def _move_tiles(self, insert_at, use_col):
        insert_at += self._cursor_tiles[settings.CURSOR].col if use_col else self._cursor_tiles[settings.CURSOR].row
        if insert_at >= 0 and insert_at < 2 * self.level.num_cells_x + 2 * self.level.num_cells_y:
            logger.debug("insterting tile at " + str(insert_at))
            # TODO: get random from level?
            self.insert_tile_at(insert_at, 1 << random.randint(1, 11))
        else:
            logger.info("insert at is out of bounds: " + str(insert_at))
    
    def insert_tile_at(self, position, tile_type):
        """
        for example for a 3x4 board (max position < 2 * 3 + 2 * 4 = 14) :

                         x
                +-------->
                |                top
                |             0   1   2
                |           +---+---+---+
              y v        13 |   |   |   | 3
                           +---+---+---+
                         12 |   |   |   | 4
                    left    +---+---+---+   right
                         11 |   |   |   | 5
                            +---+---+---+
                         10 |   |   |   | 6
                            +---+---+---+
                              9   8   7
                               bottom
        """

        assert position < 2 * self.level.num_cells_x + 2 * self.level.num_cells_y, "position '{0}' out of range '{1}'".format(position, 2 * self.level.num_cells_x + 2 * self.level.num_cells_y)
        direction = 0
        row = -1
        col = -1
        # top
        if position < self.level.num_cells_x:
            direction = 1
            col = position % self.level.num_cells_x
        # right
        elif position < self.level.num_cells_x + self.level.num_cells_y:
            direction = -1
            row = (position - self.level.num_cells_x) % self.level.num_cells_y
        # bottom
        elif position < 2 * self.level.num_cells_x + self.level.num_cells_y:
            direction = -1
            col = (position - self.level.num_cells_x - self.level.num_cells_y) % self.level.num_cells_x
        # left
        else:
            direction = 1
            row = (position - 2 * self.level.num_cells_x - self.level.num_cells_y) % self.level.num_cells_y

        remove_delay = 2
        if row > -1:
            tiles = [tile for tile in self.tiles if tile.row == row]

            # check if there is a anchored tile in this row
            if self._get_anchored_tiles_in_row(row):
                logger.debug("found anchored tile, not iserting")
                return False

            out_tile_row = tiles[0].row
            for tile in tiles:
                if tile == self.source_tile or tile == self.target_tile:
                    continue
                tile.col += direction
                if direction == 1:
                    if tile.col >= self.level.num_cells_y:
                        self.scheduler.schedule(self._remove_tile, remove_delay, 0, tile)
                else:
                    if tile.col < 0:
                        self.scheduler.schedule(self._remove_tile, remove_delay, 0, tile)

            if direction == -1:
                new_tile = Tile(self.level.num_cells_x, out_tile_row, tile_type)
                new_tile.old_position.x = self.level.num_cells_x -1
                new_tile.old_position.y = out_tile_row
            else:
                new_tile = Tile(-1, out_tile_row, tile_type)
                new_tile.old_position.x = 0
                new_tile.old_position.y = out_tile_row

            self.tiles.append(new_tile)

        else:
            tiles = [tile for tile in self.tiles if tile.col == col]

            # check if there is a anchored tile in this col/row
            if self._get_anchored_tiles_in_col(col):
                logger.debug("found anchored tile, not iserting")
                return False

            out_tile_col = tiles[0].col
            for tile in tiles:
                if tile == self.source_tile or tile == self.target_tile:
                    continue
                tile.row += direction
                if direction == 1:
                    if tile.row >= self.level.num_cells_x:
                        self.scheduler.schedule(self._remove_tile, remove_delay, 0, tile)
                else:
                    if tile.row < 0:
                        self.scheduler.schedule(self._remove_tile, remove_delay, 0, tile)

            if direction == -1:
                new_tile = Tile(out_tile_col, self.level.num_cells_y, tile_type)
                new_tile.old_position.x = out_tile_col
                new_tile.old_position.y = self.level.num_cells_y - 1
            else:
                new_tile = Tile(out_tile_col, -1, tile_type)
                new_tile.old_position.x = out_tile_col
                new_tile.old_position.y = 0

            self.tiles.append(new_tile)
            
        # remove tiles outside of walls
        for tile in [tile for tile in self.tiles if tile.row < -1 or tile.row > self.level.num_cells_y or tile.col < -1 or tile.col > self.level.num_cells_x]:
            self._remove_tile(tile)

        self.tile_to_sprite(new_tile)
        new_tile.col = new_tile.old_position.x
        new_tile.row = new_tile.old_position.y
        self.renderer.add_sprite(new_tile)
        self._generate_graph()
        self.search_graph()
        
    def _get_anchored_tiles_in_row(self, row):
        return [tile for tile in self.tiles if tile.row == row and tile.anchored]
        
    def _get_anchored_tiles_in_col(self, col):
        return [tile for tile in self.tiles if tile.col == col and tile.anchored]

    def search_graph(self):
        if self.player is not None and self.player.parent is not None:
            gsa = GraphSearchAStar(self.nodes, self.edges, self.player.parent, self.target_tile)
            self.path_tree = gsa.get_SPT()
            self.path = gsa.get_path_to_target()
            self.player.set_path(self.path)
        
    def _generate_graph(self):
        # generate nodes graph
        self.nodes = dict([[(t.col, t.row), t] for t in self.tiles])
        # logger.debug(self.nodes)
        
        # genrate edges of graph
        self.edges = {}
        for tile in self.tiles:
            self.edges[tile] = []
            cons = settings.connections[tile.type] # e.g. [N, E]
            # logger.debug("tile  coord: {2} type: {0} cons: {1}".format(tile.type, cons, (tile.col, tile.row)))
            # check for neighbours
            for direction, dx, dy in settings.directions:
                neigh = self.nodes.get((tile.col + dx, tile.row + dy), None)
                # check for border!
                if neigh:
                    # logger.debug("neigh coord: {2} type: {0} cons: {1}".format(neigh.type, settings.connections[neigh.type], (neigh.col, neigh.row)))
                    # for each connection check if the neighbour has a compatible connection
                    if direction in cons:
                        # find the opposit direction (look at the direciton constants in settings: N, E, S, W)
                        compatible_con = (direction + 2) % 4
                        if compatible_con in settings.connections[neigh.type]:
                            self.edges[tile].append(neigh)
        return True

    def _remove_tile(self, tile):
        try:
            self.tiles.remove(tile)
        except:
            pass
        try:
            self.renderer.remove_sprite(tile)
        except:
            pass
        self.player.move(Vec2(0, 0))
        self._generate_graph()
        return 0.0

    def tile_to_sprite(self, tile):
        image = self.tile_images[tile.type]
        tile.set_image(image)
        tile.position.x = self.level.tile_width * tile.col
        tile.position.y = self.level.tile_height * tile.row

    def draw(self, screen, do_flip=True):
        """Refresh the screen"""

                
        g_img = self.graph_sprite.image
        # g_img.fill((255, 0, 255))
        g_img.fill((0, 0, 0, 0), None, pygame.BLEND_RGBA_MULT)
        for node, parent in self.path_tree.items():
            if node is not None and parent is not None:
                if node.rect.center != (0, 0) and parent.rect.center != (0, 0):
                    # logger.debug("drawing line from {0} to {1}".format(node.rect.center, parent.rect.center))
                    pygame.draw.line(g_img, (255, 255, 0), node.rect.center, parent.rect.center, 5)
            
                
        if self.path and self.player.parent is not None:
            old = self.player.parent
            for nd in self.path:
                if nd.rect.center != (0, 0) and old.rect.center != (0, 0):
                    pygame.draw.line(g_img, (255, 0, 0), old.rect.center, nd.rect.center, 1)
                old = nd
        
        self.renderer.draw(screen, self.cam, fill_color=(0,0,0), do_flip=False)
        
        if __debug__ and False:
            for tile, edges in self.edges.items():
                for edge in edges:
                    pygame.draw.line(screen, (0, 0, 255), tile.rect.center, edge.rect.center, 3)
                    
        if do_flip:
            pygame.display.flip()

# ------------------------------------------------------------------------------

# adapted from the book 'Programming game ai by example'

class HeuristicEuclidian(object):
    
    @staticmethod
    def calculate(node1, node2):
        dx = node1.col - node2.col
        dy = node1.row - node2.row
        return (dx ** 2 + dy ** 2) ** 0.5

class GraphSearchAStar(object):

    _heuristic = HeuristicEuclidian()

    def __init__(self, nodes, edges, source, target):
        self._nodes = nodes
        self._edges = edges
        self._g_costs = {}
        self._f_costs = {} # {(x, y) : tile}
        self._shortest_path_tree = {} # {node : parent_node}
        self._search_frontier = {}
            
        self._source = source
        self._target = target
        
        self._search()
        
    def _search(self):
        
        pq = []
        heappush(pq, [0, self._source])
        
        while pq:
            new_cost, next_closest = heappop(pq)
            self._shortest_path_tree[next_closest] = self._search_frontier.get((next_closest.col, next_closest.row), None)

            if next_closest == self._target:
                return
            
            logger.debug("path tree: " + str(self._shortest_path_tree))
            logger.debug("searchfrontier: {0}".format(self._search_frontier))
            logger.debug("edges of {0}: {1}".format(next_closest, self._edges.get(next_closest, [])))
            for edge_to in self._edges.get(next_closest, []):
                h_cost = self._heuristic.calculate(next_closest, edge_to)
                g_cost = self._g_costs.get(next_closest, 0) + 1
                
                edge_to_id = (edge_to.col, edge_to.row)
                if edge_to_id not in self._search_frontier:
                    self._f_costs[edge_to_id] = g_cost + h_cost
                    self._g_costs[edge_to_id] = g_cost
                    
                    heappush(pq, [g_cost + h_cost, edge_to])
                    
                    self._search_frontier[edge_to_id] = next_closest
                
                elif (g_cost < self._g_costs[edge_to_id]) and \
                        self._shortest_path_tree.get(edge_to_id, None) is None:
                    
                    old_cost = self._f_costs[edge_to_id]
                    self._f_costs[edge_to_id] = g_cost + h_cost
                    self._g_costs[edge_to_id] = g_cost
                    idx = pq.index([old_cost, edge_to])
                    pq[idx][0] = self._f_costs[edge_to_id]
                    
                    heapify(pq)
                    
                    self._search_frontier[edge_to_id] = edge_to
            
    def get_path_to_target(self):
    
        logger.debug(self._shortest_path_tree)
    
        path = []
        
        
        
        nd = self._target
        if nd in self._shortest_path_tree:
            path.append(nd)
            while nd != self._source and self._shortest_path_tree.get(nd, None) is not None:
                nd = self._shortest_path_tree[nd]
                path.append(nd)
            
            path.reverse()
        logger.debug("!!! path: " + str(path))
        return path
        
    def get_SPT(self):
        return self._shortest_path_tree
            
        
        