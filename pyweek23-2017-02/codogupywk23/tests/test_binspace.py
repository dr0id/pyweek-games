# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'test_binspace.py' is part of codogupywk23
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]                    # For instance:
                   |                                  * 1.2.0.1 instead of 1.2-a
                   +-|* 0 for alpha (status)          * 1.2.1.2 instead of 1.2-b2 (beta with some bug fixes)
                     |* 1 for beta (status)           * 1.2.2.3 instead of 1.2-rc (release candidate)
                     |* 2 for release candidate       * 1.2.3.0 instead of 1.2-r (commercial distribution)
                     |* 3 for (public) release        * 1.2.3.5 instead of 1.2-r5 (commercial dist with many bug fixes)


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division
import unittest
import warnings

import codogupywk23.gamelib.binspace as mut  # module under test
from codogupywk23.gamelib.pyknic.mathematics import Point2, Vec2

warnings.simplefilter('error')  # make warnings visible for developers

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id [at] gmail [dot] com"
__copyright__ = "DR0ID @ 2017"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

if mut.__version_info__ != __version_info__:
    raise Exception("version numbers do not match!")


class _TestEnt(object):

    def __init__(self, x, y):
        self.position = Point2(x, y)


class BinSpaceTests(unittest.TestCase):

    def setUp(self):
        self.longMessage = True
        self.bucket_size = 20
        self.bin_space = mut.BinSpace(self.bucket_size, self.bucket_size)
        size = self.bucket_size
        offset = self.bucket_size // 2
        self.entities = []
        for y in range(20):
            for x in range(20):
                ent = _TestEnt(x * size + offset, y * size + offset)
                self.bin_space.add(ent)
                self.entities.append(ent)

    def test_get_from_rect_with_smaller_size_than_bucket_size(self):
        # arrange
        # act
        entities = self.bin_space.get_from_rect(2, 2, 10, 10)

        # verify
        self.assertEqual(1, len(entities))

    def test_get_from_rect_returns_only_entities_within_covered_buckets(self):
        # arrange

        # act
        x = self.bucket_size // 2
        y = self.bucket_size // 2
        w = self.bucket_size
        h = self.bucket_size
        actual_entities = self.bin_space.get_from_rect(x, y, w, h)

        # verify
        self.assertEqual(4, len(actual_entities), "number of actual_entities should be 4")
        for ent in actual_entities:
            self.assertGreaterEqual(ent.position.x, 0)
            self.assertLessEqual(ent.position.x, 40)
            self.assertGreaterEqual(ent.position.y, 0)
            self.assertLessEqual(ent.position.y, 40)

    def test_update_moves_entities_to_right_bucket(self):
        # arrange
        delta = Vec2(self.bucket_size // 2, self.bucket_size // 2)
        for ent in self.entities:
            ent.position += delta

        # act
        self.bin_space.update()
        actual_entities = self.bin_space.get_from_rect(10, 10, 20, 20)

        # verify
        self.assertEqual(1, len(actual_entities), "number of actual_entities should be 1")
        for ent in actual_entities:
            self.assertGreaterEqual(ent.position.x, 0)
            self.assertLessEqual(ent.position.x, 40)
            self.assertGreaterEqual(ent.position.y, 0)
            self.assertLessEqual(ent.position.y, 40)

    def test_update_in_rect_moves_some_entities_to_right_bucket(self):
        # arrange
        offset_size = self.bucket_size // 2
        delta = Vec2(offset_size, offset_size)
        for ent in self.entities:
            ent.position += delta

        # act
        rect = (self.bucket_size // 2, self.bucket_size // 2, self.bucket_size, self.bucket_size)
        self.bin_space.update_in_rect(*rect)
        actual_entities = self.bin_space.get_from_rect(*rect)

        # verify
        self.assertEqual(1, len(actual_entities), "number of actual_entities should be 1")
        for ent in actual_entities:
            self.assertGreaterEqual(ent.position.x, 0)
            self.assertLessEqual(ent.position.x, 40)
            self.assertGreaterEqual(ent.position.y, 0)
            self.assertLessEqual(ent.position.y, 40)

        # check that only the entities in the rect have been updated
        for by in range(20):
            for bx in range(20):
                b_ents = self.bin_space.get(bx, by)
                bucket_id = (bx, by)
                if bucket_id == (0, 0):
                    count = 0
                elif bucket_id in ((1, 0), (0, 1)):
                    count = 0
                elif bucket_id in ((2, 1), (1, 2), (2, 2)):
                    count = 2
                else:
                    count = 1

                _msg = "count entities for bucket {0} should be {1}"
                self.assertEqual(count, len(b_ents), _msg.format(bucket_id, count))

    def test_get_at_return_right_entities(self):
        # arrange

        # act
        actual = self.bin_space.get_at(23, 23)

        # verify
        expected = 9
        self.assertEqual(expected, len(actual))


if __name__ == '__main__':
    unittest.main()
