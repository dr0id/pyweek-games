# -*- coding: utf-8 -*-
from __future__ import print_function, division, absolute_import

import logging

import pygame

from .stats import Stats
from .context_loading import ContextLoading
from .eventmapper import get_actions
from . import pyknic

from . import settings
from .gameresources import GameResources

logger = logging.getLogger(__name__)

END_GAME = 'END GAME'

TEXT = [
    """Personal Log
    Mariners of olde have pursued it for ages. As legend has it, the elusive Lesser of Two Evils has never been found on or in the vasty deep of the Earth: therefore it must be in the stars.
    """,

    """Captain's Log
    We found the interstellar gate immediately, its beacon awakening to announce this gift from an unknown benefactor. Through it...a vast reach of unimaginable Evil. If anyone receives this...don't follow. Survival's a daily battle. Our stoic ship endured death by a thousand cuts and bludgeons. We were dead in space, on failing life support, but disabled two enemy ships. Crews boarded each to assess options. One blew up with five on board. Seven remain.
    """,

    (ContextLoading, ['Chapter 1 (Loading...)']),

    """Inner Dialogue
    As is the way of Evils, it is impossible to know the degree unless you've chosen. Even so, in the choosing is but a murky mystery, frothy and unknowable. Did you choose rightly? Should you have chosen the other?
    """,

    (ContextLoading, ['Chapter 2 (Loading...)']),

    """Inner Dialogue
    And the choice made is at once done. Consequences are like barnacles. I can't see outside the ship: blind in the dark of space. Dare we flee in the thick of a meteor field? Ha, better the blind man should fight to see tomorrow.
    """,

    """Captain's Log
    The last gate was barely functional. It took out our sensors. There are no spare parts. Every loose scrap is now plugging the holes in the hull.
    """,

    """On Screen
    [...]
    The sounds of weapons vibrate the ship.
    """,

    (ContextLoading, ['Chapter 3 (Loading...)']),

    """Inner Dialogue
    It seems we've found a lesser. But, as is the way of Evils, crush the head under the heel and two more rise up. Is there no rest!?
    """,


    (ContextLoading, ['Chapter 4 (Loading...)']),

    """Captain's Log
    We're on an approach...to an unknown. Sensors are repaired, but not by our hands. Something's not right. Investigating.
    """,

    """On Screen
    The ravages of war surround you. Ahead are two Evils. The host of their armies blocks the way. You've not come this far to turn back...
    """,

    (ContextLoading, ['Chapter 5 >>> BOSS <<< (Loading...']),

    (END_GAME, [GameResources.get(GameResources.IMG_BACKGROUND_1)]),
]


class ContextMenu(pyknic.context.Context):
    def __init__(self, stats=None):
        self.stats = stats
        self.text = None

        self.screen = pygame.display.get_surface()
        self.screen_rect = self.screen.get_rect()

        GR = GameResources
        # 40: ('raygun/RAYGUN.TTF', 25)
        # 79: ('stonehinge/StoneHinge.ttf', 24)
        # 81: ('vibrocentric/vibroceb.ttf', 24)
        # 82: ('vibrocentric/vibrocei.ttf', 28)
        self.font = GR.get(GR.LOG_FONT)
        if settings.ALlOW_CHEATS:
            self.read_prompt = self.font.render('[F10 (MORE), F9 (debug messages)]', True, pygame.Color('gold'))
            self.play_prompt = self.font.render('[F10 (PLAY), F9 (debug messages)]', True, pygame.Color('green3'))
            self.end_prompt = self.font.render('[Game Won: Escape or Quit]', True, pygame.Color('green'))
        else:
            self.read_prompt = self.font.render('[Press F10 for MORE...]', True, pygame.Color('gold'))
            self.play_prompt = self.font.render('[Press F10 to PLAY]', True, pygame.Color('green3'))
            self.end_prompt = self.font.render("[You Made It Home!! Press Escape or Quit]", True, pygame.Color('green'))
        self.displays = []

        self.bounds = None
        self.y = 0

    def enter(self):
        self.render_paragraph()

    def exit(self):
        pass

    def suspend(self):
        pass

    def resume(self):
        pass

    def render_paragraph(self):
        logger.debug('MENU start')
        GR = GameResources
        self.bounds = self.screen_rect.inflate(-200, -200)
        # self.bounds.x -= 20
        y = self.y if self.y > 0 else self.bounds.y
        section = TEXT.pop(0)
        if not isinstance(section, str):
            self.text, args = section
            img = args[0]
            rect = img.get_rect()
            self.displays = [(img, rect)]
            return
        section = section.lstrip().rstrip()
        logger.debug('section="{}"', section)
        for para in section.split('\n'):
            if 'Personal Log' in para or 'Inner Dialogue' in para:
                self.font = GR.get(GR.PERSON_FONT)
            elif "Captain's Log" in para:
                self.font = GR.get(GR.LOG_FONT)
            elif 'On Screen' in para:
                self.font = GR.get(GR.SCREEN_FONT)
            w, h = self.font.size('W')
            para = para.lstrip().rstrip()
            logger.debug('para="{}"', para)
            words = para.split()
            logger.debug('words={}', words)
            line = word = ''
            while words:
                word = words.pop(0)
                w, h = self.font.size(line + ' ' + word)
                if w >= self.bounds.w:
                    logger.debug('line="{}"', line)
                    img = self.font.render(line, True, pygame.Color('gold'))
                    rect = img.get_rect(topleft=(self.bounds.x, y))
                    logger.debug('line: rect={}', rect)
                    self.displays.append((img, rect))
                    y += h + 3
                    line = ''
                line = ' '.join((line, word))
                word = ''
            if line or word:
                line = ' '.join((line, word))
                img = self.font.render(line, True, pygame.Color('gold'))
                rect = img.get_rect(topleft=(self.bounds.x, y))
                self.displays.append((img, rect))
                y += h + 3
        self.y = y + h + 3

        for i, r in self.displays:
            logger.debug('{}, {}', i, r)
        logger.debug('MENU end')
        # quit()

    def update(self, delta_time, sim_time):
        for action, extra in get_actions(settings.event_map, pygame.event.get()):
            logger.debug("taking action {0}, {1}", action, extra)
            if action == settings.ACTION_QUIT:
                logging.debug('MENU: key ACTION_QUIT')
                self.pop(self.get_stack_length())  # quit application
            elif action == settings.ACTION_ESCAPE:
                # pause or just quit this context
                logging.debug('MENU: key ACTION_ESCAPE')
                self.pop()
            elif action == settings.ACTION_WIN_LEVEL and self.text != END_GAME:
                logging.debug('MENU: key ACTION_WIN_LEVEL (start game level)')
                # from .context_gameplay import ContextGamePlay
                # cgp = ContextGamePlay()
                # cgp.stats = Stats()  # create this in a correct way
                # cgp.load_map(settings.STARTING_MAP)
                # cgp.stats.current_level = settings.STARTING_LEVEL
                # self.exchange(cgp)
                if TEXT:
                    if isinstance(TEXT[0], str):
                        self.render_paragraph()
                    elif isinstance(TEXT[0], tuple):
                        C, args = TEXT.pop(0)
                        if C == END_GAME:
                            self.text = END_GAME
                            img = args[0]
                            rect = img.get_rect()
                            self.displays = [(img, rect)]
                            return
                        else:
                            self.text = args[0]
                            if self.stats is None:
                                self.stats = Stats()
                                self.stats.current_level = settings.STARTING_MAP
                            self.exchange(C(self.stats, self.text))
                            return  # prevent doing it twice if F10 gets hit multiple times
            elif settings.ALlOW_CHEATS and self.text != END_GAME:
                logger.debug('MENU: cheat key')
                if action == settings.ACTION_LOOSE_LEVEL:
                    logging.debug('MENU: cheat key ACTION_LOOSE_LEVEL')
                    if TEXT:
                        if isinstance(TEXT[0], str):
                            self.render_paragraph()
                        elif isinstance(TEXT[0], tuple):
                            C, args = TEXT.pop(0)
                            if C == END_GAME:
                                self.text = END_GAME
                                img = args[0]
                                rect = img.get_rect()
                                self.displays = [(img, rect)]
                                return
                            else:
                                self.exchange(ContextMenu(self.stats))
                                return

    def draw(self, screen, do_flip=True, interpolation_factor=1.0):
        screen.fill((0, 0, 0))

        if self.displays:
            x = y = h = 0
            for img, rect in self.displays:
                screen.blit(img, rect)
                x, y = rect.topleft
                h = rect.h
            y += h * 2
            if self.text == END_GAME:
                rect = self.end_prompt.get_rect(bottomright=self.screen_rect.bottomright)
                screen.blit(self.end_prompt, rect)
            elif isinstance(TEXT[0], tuple):
                screen.blit(self.play_prompt, (x, y))
            else:
                screen.blit(self.read_prompt, (x, y))

        # if self.bounds:
        #     pygame.draw.rect(screen, pygame.Color('darkgrey'), self.bounds, 1)

        if do_flip:
            pygame.display.flip()
