"""
TODO: docstring
"""

# TODO: Prior to release:
# todo: ROTATE_METEORS_STEP = 2
# todo: ALlOW_CHEATS = False
# todo: HUD_SHOW_DEBUGS = 'none'
# todo: MUSIC_PLAY = True
# TODO: set right level as STARTING_MAP!

import collections

import pygame


###############################################################################
#
# User Tuning
#
###############################################################################

# The player may want to customize these.
from .eventmapper import ANY_MOD
from .pyknic.generators import GlobalIdGenerator, FlagGenerator

SCREEN_WIDTH = 1024
SCREEN_HEIGHT = 768
BIT_DEPTH = 0

MUSIC_PLAY = True
MUSIC_VOLUME = 0.3

# Angle step for meteor rotation. This is a cosmetic setting. Any value above 2 does not look nice.
# 4 is a decent compromise for loading time and older CPUs. If your computer can't handle 2, it's
# recommended to set it to 0.
# Numbers must divide evenly into 360!
# 0 = no rotation, best performance
# 1 = 360 angles
# 2 = 180
# 3 = 120
# 4 = 90
# 5 = 72
ROTATE_METEORS_STEP = 3

# Scaling meteors below 1.0 will increase performance, but affect appearance.
# 1.0 is best appearance. 0.75 gives an acceptable balance.
SCALE_METEORS = 0.75

###############################################################################
#
#  End User Tuning
#
###############################################################################

# Dev / Debugs

ALlOW_CHEATS = False

PRINT_FPS = True
DEBUG_RENDER = False

HUD_SHOW_DEBUGS = 'none'      # 'all', 'some', or 'none'; see ACTION_HUD_SHOW_DEBUGS in ContextGamePlay
HUD_SHOW_FPS = True
HUD_SHOW_IMAGE_CACHE = True
HUD_SHOW_ENTITIES = False
HUD_SHOW_COLLISIONS = False
FPS = 0         # updated by _print_fps in main.py
GFX_FPS = 0     # updated by _print_fps in main.py
SIM_FPS = 0     # updated by _print_fps in main.py

INITIAL_ROTATE_METEORS_STEP = ROTATE_METEORS_STEP

# Time

SIM_TIME_STEP = 1.0 / 25.0

# Display

CAPTION = 'The Lesser of Two Evils'
SCREEN_SIZE = SCREEN_WIDTH, SCREEN_HEIGHT
FLAGS = pygame.DOUBLEBUF

# Graphics
_FontSetting = collections.namedtuple('_FontSetting', 'name size')
HUD_FONT = _FontSetting(name='data/font/FamilianSon/FAMI_S.TTF', size=27)
HUD_COLOR = pygame.Color('white')
PERSON_FONT = _FontSetting(name='data/font/vibrocentric/vibrocei.ttf', size=28)
LOG_FONT = _FontSetting(name='data/font/raygun/RAYGUN.TTF', size=26)
SCREEN_FONT = _FontSetting(name='data/font/youthanasia/YOUTHANI.ttf', size=27)

# Images

# Sounds

# Mixer

MIXER_FREQUENCY = 0
MIXER_BUFFER_SIZE = 128
MIXER_RESERVED_CHANNELS = 7
MIXER_CHANNEL_SHIP_BOOM = 0
MIXER_CHANNEL_BADDY_BOOM = 1
MIXER_CHANNEL_LESSER_EVIL = 2
MIXER_CHANNEL_BULLET_SOFT_WALL = 3
MIXER_CHANNEL_SHIP_FIRE_BULLET = 4
MIXER_CHANNEL_PICK_UP_DEBRIS = 5
MIXER_CHANNEL_BULLET_HIT_WALL = 6

# Advanced Tuning

# Game
STARTING_MAP = (
    "./data/test_map.json",
    "./data/gumm1_full_bg_image.json",
    "./data/gumm2_giant_map.json",
    "./data/3_giant_map.json",
    "./data/01_level.json",  # 4
    "./data/02_level.json",  # 5
    "./data/03_level.json",  # 6
    "./data/04_level.json",  # 7
    "./data/05_level.json",  # 8
)[4]
STARTING_LEVEL = "level00.json"

_action_generator = GlobalIdGenerator(0)
ACTION_QUIT = _action_generator.next()
ACTION_ESCAPE = _action_generator.next()
ACTION_WIN_LEVEL = _action_generator.next()
ACTION_LOOSE_LEVEL = _action_generator.next()
ACTION_FULL_STOP = _action_generator.next()

ACTION_MOVE_UP = _action_generator.next()
ACTION_MOVE_DOWN = _action_generator.next()
ACTION_MOVE_LEFT = _action_generator.next()
ACTION_MOVE_RIGHT = _action_generator.next()
ACTION_STOP_UP = _action_generator.next()
ACTION_STOP_DOWN = _action_generator.next()
ACTION_STOP_LEFT = _action_generator.next()
ACTION_STOP_RIGHT = _action_generator.next()
ACTION_FIRE = _action_generator.next()
ACTION_HOLD_FIRE = _action_generator.next()
ACTION_SECONDARY = _action_generator.next()
ACTION_TOGGLE_DEBUG_RENDER = _action_generator.next()
ACTION_HUD_SHOW_DEBUGS = _action_generator.next()
ACTION_ROTATE_METEORS = _action_generator.next()

event_map = {
    pygame.QUIT: {None: ACTION_QUIT},
    pygame.KEYDOWN: {
        (pygame.K_ESCAPE, ANY_MOD): ACTION_ESCAPE,
        (pygame.K_F4, pygame.KMOD_ALT): ACTION_QUIT,
        # cheat keys
        (pygame.K_F10, ANY_MOD): ACTION_WIN_LEVEL,
        (pygame.K_F9, ANY_MOD): ACTION_LOOSE_LEVEL,
        (pygame.K_F1, ANY_MOD): ACTION_TOGGLE_DEBUG_RENDER,
        (pygame.K_F2, ANY_MOD): ACTION_FULL_STOP,
        (pygame.K_F3, ANY_MOD): ACTION_HUD_SHOW_DEBUGS,
        (pygame.K_F5, ANY_MOD): ACTION_ROTATE_METEORS,
        # movement (arrows, joystick?, ...)
        (pygame.K_UP, ANY_MOD): ACTION_MOVE_UP,
        (pygame.K_DOWN, ANY_MOD): ACTION_MOVE_DOWN,
        (pygame.K_LEFT, ANY_MOD): ACTION_MOVE_LEFT,
        (pygame.K_RIGHT, ANY_MOD): ACTION_MOVE_RIGHT,
        (pygame.K_w, ANY_MOD): ACTION_MOVE_UP,
        (pygame.K_s, ANY_MOD): ACTION_MOVE_DOWN,
        (pygame.K_a, ANY_MOD): ACTION_MOVE_LEFT,
        (pygame.K_SPACE, ANY_MOD): ACTION_FIRE,
        (pygame.K_LCTRL, ANY_MOD): ACTION_FIRE,
        (pygame.K_m, ANY_MOD): ACTION_SECONDARY,
        (pygame.K_d, ANY_MOD): ACTION_MOVE_RIGHT,
    },
    pygame.KEYUP: {
        (pygame.K_SPACE, ANY_MOD): ACTION_HOLD_FIRE,
        (pygame.K_LCTRL, ANY_MOD): ACTION_HOLD_FIRE,
        (pygame.K_UP, ANY_MOD): ACTION_STOP_UP,
        (pygame.K_DOWN, ANY_MOD): ACTION_STOP_DOWN,
        (pygame.K_LEFT, ANY_MOD): ACTION_STOP_LEFT,
        (pygame.K_RIGHT, ANY_MOD): ACTION_STOP_RIGHT,
        (pygame.K_w, ANY_MOD): ACTION_STOP_UP,
        (pygame.K_s, ANY_MOD): ACTION_STOP_DOWN,
        (pygame.K_a, ANY_MOD): ACTION_STOP_LEFT,
        (pygame.K_d, ANY_MOD): ACTION_STOP_RIGHT,
    },
}


# kinds
_flag_gen = FlagGenerator()
KIND_SHIP_I010 = _flag_gen.next()
KIND_SHIELD_I011 = _flag_gen.next()
KIND_GUN_I020 = _flag_gen.next()
KIND_GUN_I021 = _flag_gen.next()
KIND_BULLET_I031 = _flag_gen.next()
KIND_BULLET_BADDY_I032 = _flag_gen.next()
KIND_MINE_I040 = _flag_gen.next()
KIND_SMART_BOMB_I041 = _flag_gen.next()
KIND_WALL_I050 = _flag_gen.next()
KIND_SOFT_WALL_I051 = _flag_gen.next()
KIND_WALL_EDGE_I052 = _flag_gen.next()
KIND_BADDY_I060 = _flag_gen.next()
KIND_BADDY2_I061 = _flag_gen.next()
KIND_POWER_UP_I070 = _flag_gen.next()
KIND_DEBRIS_I081 = _flag_gen.next()
KIND_ORANGE_I082 = _flag_gen.next()
KIND_METAL_I083 = _flag_gen.next()
KIND_CANS_I084 = _flag_gen.next()
KIND_GLOWING_ROCKS_I085 = _flag_gen.next()
KIND_DRONE_I091 = _flag_gen.next()
KIND_STATION_I092 = _flag_gen.next()
KIND_GATE_I101 = _flag_gen.next()
KIND_SPAWN_I111 = _flag_gen.next()
KIND_TRIGGER_I120 = _flag_gen.next()
KIND_BLACK_HOLE_I130 = _flag_gen.next()
KIND_GIANT_SUN_I140 = _flag_gen.next()
KIND_METEOR = _flag_gen.next()


BUCKETS_WIDTH = SCREEN_WIDTH // 3
BUCKETS_HEIGHT = SCREEN_HEIGHT // 3

SHOOT_INTERVAL = 0.3
BULLET_MAX_WALL_HIT_COUNT = 4
DEFAULT_SHIELD_LEVEL = 6
MAX_SHIELD_LEVEL = 7

BADDY_SHOOT_CHANCE = 0.6    # 0.0=never, 1.0=always
BADDY_ANGLE_PREC = 3

SHIP_MAX_SPEED = 100
BULLET_SPEED = 200

GATE_COL_DISTANCE_SQ = 20 * 20

try:
    from . import _gumm_custom_settings
    # STARTING_MAP = "./data/05_level.json"
    # MUSIC_PLAY = False
    # ROTATE_METEORS_STEP = 3
    ALlOW_CHEATS = True
    HUD_SHOW_DEBUGS = 'all'
except ImportError:
    pass
