# -*- coding: utf-8 -*-
from __future__ import print_function, division, absolute_import

import gc
import logging
import random
import sys

from . import settings
from . import collisiondetector
from . import pyknic
from .binspace import BinSpace
from .collisiondetector import check_collisions_between_all, check_collisions_between_nearest_all
from .context_menu import ContextMenu
from .entities import Ship, Baddy, Bullet, BaddyBullet, Wall, WallEdge, Gate, Meteor, Debris
from .eventmapper import get_actions
from .gameresources import GameResources
from .pyknic.mathematics import Point2, Vec2
from .pyknic.pygame_wrapper.spritesystem import Sprite, DefaultRenderer, Camera, TextSprite
from .pyknic.timing import Timer
from .pytmxloader import pytmxloader, TILE_LAYER_TYPE
from .settings import *

logger = logging.getLogger(__name__)


# noinspection PyMethodMayBeStatic
class ContextGamePlay(pyknic.context.Context):
    def __init__(self, stats):
        self.stats = stats
        self.renderer = DefaultRenderer()
        self.cam = Camera(pygame.Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT))
        self.ship = None  # temporary?
        self.space = BinSpace(BUCKETS_WIDTH, BUCKETS_HEIGHT)
        self.friendly_shoot_timer = Timer(
            SHOOT_INTERVAL, repeat=True, logger_to_use=logger, name='friendly shoot timer')
        self.friendly_shoot_timer.event_elapsed += self._create_friendly_bullets
        self.baddy_shoot_timer = Timer(1.0, repeat=True, logger_to_use=logger, name='baddy shoot timer')
        self.baddy_shoot_timer.event_elapsed += self._create_baddy_bullets
        self.baddies_on_screen = []
        self.bullets = []
        self.bullet_clean_timer = Timer(2.0, repeat=True, logger_to_use=logger, name="bullet clean timer")
        self.bullet_clean_timer.event_elapsed += self.clean_out_of_scope_bullets

        self.num_visible = 0
        self.num_updated = 0
        self.num_coll_checks = 0
        self.hud_player = None
        self.hud_shields = None
        self.hud_guns = None
        self.hud_fps = None
        self.hud_objects = None
        self.hud_coll_checks = None
        self.hud_image_cache = None
        self.hud_items = []
        self._shield_sprites = []
        self._shield_image = None
        self._shield_down_image = None
        self._background = None

    def _create_shield_sprite(self, pos):
        if self.ship.shield_level <= 0:
            if len(self._shield_sprites) > 1:
                return
            _img = self._shield_down_image
            spr = Sprite(_img, pos, z_layer=100, name="shield_down")
            Timer.scheduler.schedule(self._remove_shield_sprite, 0.1)
        else:
            if len(self._shield_sprites) > 4:
                return
            _img = self._shield_image
            spr = Sprite(_img, pos, z_layer=100, name="shield")
            Timer.scheduler.schedule(self._remove_shield_sprite, 0.2)

        self.renderer.insert_sprite(spr, -1)
        self._shield_sprites.append(spr)

    def _remove_shield_sprite(self, *args):
        if self._shield_sprites:
            spr = self._shield_sprites.pop()
            self.renderer.remove_sprite(spr)
        return 0

    # noinspection PyPep8Naming
    def _create_friendly_bullets(self, timer):
        self._create_bullets(self.ship, Bullet)
        self.ship.sfx_fire_bullet()

    # noinspection PyPep8Naming
    def _create_baddy_bullets(self, timer):
        logger.debug('BADDY BULLET')
        sfx = None
        for baddy in self.baddies_on_screen:
            if self.cam.rect.colliderect(baddy.sprite.rect):
                if random.random() < settings.BADDY_SHOOT_CHANCE:
                    baddy.direction = (self.ship.position - baddy.position).normalized
                    baddy.sprite.rotation = -baddy.direction.angle // BADDY_ANGLE_PREC * BADDY_ANGLE_PREC
                    self._create_bullets(baddy, BaddyBullet)
                    sfx = baddy
        if sfx is not None:
            sfx.sfx_fire_bullet()

    # noinspection PyUnusedLocal
    def _create_bullets(self, ship, bullet_class):
        logger.debug("create bullets ------------======= {0}", len(self.bullets))
        ship_dir = ship.direction
        sprites = []
        for d in ship.default_gun + ship.extra_guns:
            xr = Vec2(ship_dir.x, -ship_dir.y)
            yr = Vec2(ship_dir.y, ship_dir.x)
            dr = Vec2(xr.dot(d), yr.dot(d))
            pos = ship.position + dr * ship.radius
            bullet = bullet_class(pos, 20, 20, None)  # hack
            spr = Sprite(bullet_class.surf, pos, anchor='center', z_layer=ship.sprite.z_layer - 1,
                         name=str(bullet_class))
            bullet.sprite = spr  # hack
            bullet.vel = dr * BULLET_SPEED + ship.vel
            self.space.add(bullet)
            self.bullets.append(bullet)
            sprites.append(spr)
        self.renderer.add_sprites(sprites)

    # noinspection PyPep8Naming
    def _on_col_ship_gate(self, ships, gates):
        sfx = False
        for ship in ships:
            for gate in gates:
                assert ship.kind & KIND_SHIP_I010
                assert gate.kind & KIND_GATE_I101
                assert ship.kind < gate.kind

                self.num_coll_checks += 1

                if ship.position.get_distance_sq(gate.position) < GATE_COL_DISTANCE_SQ:
                    sfx = True
                    logger.debug("Gate collisions!")
                    # cgp = ContextGamePlay()
                    # cgp.stats = Stats()  # create this in a correct way
                    # cgp.load_map(gate.next_map)
                    # cgp.stats.current_level = gate.next_map
                    # self.exchange(cgp, FadeOutEffect(1.0))
                    if gate.next_map == "exit":
                        logger.info("gate has 'exit' on map {0}", self.stats.current_level)
                        # self.pop()
                        # TODO: untested because I cannot WIN!!! - Gumm
                        self.exchange(ContextMenu(self.stats))
                    else:
                        self._move_to_next_map(gate.next_map)
        if sfx:
            self.ship.sfx_gate()

    def _move_to_next_map(self, next_map):
        self.stats.extra_guns = list(self.ship.extra_guns)
        self.stats.guns = list(self.ship.guns)
        self.stats.shield_level = self.ship.shield_level
        self.stats.current_level = next_map
        self.exchange(ContextMenu(self.stats))

    # noinspection PyPep8Naming
    def _on_col_ship_wall(self, ship, wall):
        assert ship.kind & KIND_SHIP_I010
        assert wall.kind & KIND_WALL_I050
        assert ship.kind < wall.kind

        self.num_coll_checks += 1

        c_max = wall.position + wall.half_bounds
        c_min = wall.position - wall.half_bounds

        _axis, _da = collisiondetector.sphere_vs_aabb(ship.position, ship.radius, c_min, c_max, wall.position)

        if _axis is None:
            return

        _axis.length = _da

        ship.position += _axis
        if _axis.dot(ship.vel) != 0:
            ship.vel -= ship.vel.project_onto(_axis)

        # TODO: damage of ship?

    # noinspection PyPep8Naming
    def _on_col_ship_wall_edge(self, ships, wall_edges):
        sfx = False
        for ship in ships:
            for wall_edge in wall_edges:
                assert ship.kind & KIND_SHIP_I010
                assert wall_edge.kind & KIND_WALL_EDGE_I052
                assert ship.kind < wall_edge.kind

                self.num_coll_checks += 1

                ship_position = ship.position - ship.radius * wall_edge.normal
                p0 = wall_edge.p0
                to_ship = ship_position - p0
                if wall_edge.normal.dot(to_ship) > 0:
                    # in front of the right side of the wall
                    continue

                p1 = wall_edge.p1
                wall = p1 - p0
                if wall.dot(to_ship) < 0:
                    # not within segment, on the p0 side
                    continue

                if wall.dot(ship_position - p1) > 0:
                    # not within segment, on the p1 side
                    continue

                # distance to line
                depth_delta = wall.normalized.cross(to_ship).length

                if depth_delta > ship.radius:  # radius is enough, otherwise strange bouncing at acute angle will occur
                    # too far away, no collision and therefore no response
                    continue

                # collision response
                # depth_dir = wall_edge.normal.clone()
                # depth_dir.length = depth_delta

                ship.position += wall_edge.normal * depth_delta
                # GUMM, whack: ship.vel -= ship.vel.project_onto(wall_edge.normal)

                # TODO: clamp velocity; causing re-hits
                # GUMM, whack: if -1 < ship.vel.x < 1:
                # GUMM, whack:     ship.vel.x = 0.0
                # GUMM, whack: if -1 < ship.vel.y < 1:
                # GUMM, whack:     ship.vel.y = 0.0

                ship.hit_wall_edge(wall_edge)
                sfx = True
        if sfx:
            self.ship.sfx_boom()
            self._create_shield_sprite(self.ship.position)

    # noinspection PyPep8Naming
    def _on_col_ship_soft_wall(self, ships, walls):
        sfx = False
        for ship in ships:
            for wall in walls:
                assert ship.kind & KIND_SHIP_I010
                assert wall.kind & KIND_SOFT_WALL_I051
                assert ship.kind < wall.kind

                self.num_coll_checks += 1

                c_max = wall.position + wall.half_bounds
                c_min = wall.position - wall.half_bounds

                _axis, _da = collisiondetector.sphere_vs_aabb(ship.position, ship.radius, c_min, c_max, wall.position)

                if _axis is None:
                    continue

                logger.debug("RAMMED DESTRUCTIBLE WALL! @ " + str(wall.position))

                # self.space.remove(wall)
                # self.renderer.remove_sprite(wall.sprite)
                # TODO: damage of ship?
                self.ship.position += _axis
                ship.hit_soft_wall(wall)
                sfx = True
        if sfx:
            self.ship.sfx_boom()
            self._create_shield_sprite(self.ship.position)

    # noinspection PyPep8Naming
    def _on_col_bullet_wall_edge(self, bullets, wall_edges):
        sfx = None
        for bullet in bullets:
            assert bullet.kind & (KIND_BULLET_I031 | KIND_BULLET_BADDY_I032)

            kx = bullet.position.x
            ky = bullet.position.y
            o_ent = None
            dist = sys.maxsize

            for wall_edge in wall_edges:
                assert wall_edge.kind & KIND_WALL_EDGE_I052
                assert bullet.kind < wall_edge.kind

                self.num_coll_checks += 1

                dx = wall_edge.position.x - kx
                dy = wall_edge.position.y - ky
                # od = k_ent_position_get_distance_sq(other.position)
                od = dx * dx + dy * dy
                if od < dist:
                    dist = od
                    o_ent = wall_edge

            if o_ent:
                wall_edge = o_ent
                d1 = bullet.position - wall_edge.p0
                if wall_edge.normal.dot(d1) > 0:
                    continue  # in front

                wall = wall_edge.p1 - wall_edge.p0
                if wall.dot(d1) < 0:
                    continue  # not in segment on p0

                if wall.dot(bullet.position - wall_edge.p1) > 0:
                    continue  # not in segment on p1

                if wall_edge.normal.dot(bullet.vel) > 0:
                    # bullet moving away from wall
                    continue

                # distance to line
                depth_delta = wall.normalized.cross(d1).length + 1

                # response
                sfx = bullet
                bullet.position += wall_edge.normal * depth_delta
                bullet.vel = bullet.vel.reflect(wall_edge.normal)
                bullet.wall_hit_count += 1
                if bullet.wall_hit_count > BULLET_MAX_WALL_HIT_COUNT:
                    self.space.remove(bullet)
                    self.renderer.remove_sprite(bullet.sprite)
        if sfx is not None:
            sfx.sfx_hit_wall()

    # noinspection PyPep8Naming
    def _on_col_bullet_soft_wall(self, bullets, soft_walls):
        sfx = None
        for bullet in bullets:
            assert bullet.kind & (KIND_BULLET_I031 | KIND_BULLET_BADDY_I032)
            if bullet.kind & KIND_BULLET_BADDY_I032:
                return  # baddy bullet do not destroy them

            kx = bullet.position.x
            ky = bullet.position.y
            o_ent = None
            dist = sys.maxsize

            for soft_wall in soft_walls:
                assert soft_wall.kind & KIND_SOFT_WALL_I051
                assert bullet.kind < soft_wall.kind

                self.num_coll_checks += 1

                dx = soft_wall.position.x - kx
                dy = soft_wall.position.y - ky
                # od = k_ent_position_get_distance_sq(other.position)
                od = dx * dx + dy * dy
                if od < dist:
                    dist = od
                    o_ent = soft_wall

            if o_ent:
                soft_wall = o_ent
                if soft_wall.rect.collidepoint((bullet.position.x, bullet.position.y)):
                    sfx = bullet
                    self.space.remove(bullet)
                    self.renderer.remove_sprite(bullet.sprite)
                    o_ent.health -= 1
                    if o_ent.health <= 0:
                        self.space.remove(soft_wall)
                        self.renderer.remove_sprite(soft_wall.sprite)
        if sfx is not None:
            sfx.sfx_hit_soft_wall()

    # noinspection PyPep8Naming
    def _on_col_ship_baddy(self, ships, baddies):
        sfx = None
        for ship in ships:
            assert ship.kind & KIND_SHIP_I010
            ship_colliderect = ship.sprite.rect.colliderect
            for baddy in baddies:
                assert baddy.kind & KIND_BADDY_I060

                self.num_coll_checks += 1

                if ship_colliderect(baddy.sprite.rect):
                    sfx = True
                    self.space.remove(baddy)
                    self.renderer.remove_sprite(baddy.sprite)
                    ship.hit_baddy(baddy)
                    ship.hit_baddy(baddy)
                    ship.hit_baddy(baddy)
        if sfx:
            self.ship.sfx_boom()
            self._create_shield_sprite(self.ship.position)

    # noinspection PyPep8Naming
    def _on_col_ship_baddy_bullet(self, ships, baddy_bullets):
        sfx = None
        for ship in ships:
            assert ship.kind & KIND_SHIP_I010
            for baddy_bullet in baddy_bullets:
                assert baddy_bullet.kind & KIND_BULLET_BADDY_I032

                self.num_coll_checks += 1

                if ship.position.get_distance(baddy_bullet.position) < ship.radius + baddy_bullet.radius:
                    sfx = True
                    self.space.remove(baddy_bullet)
                    self.renderer.remove_sprite(baddy_bullet.sprite)
                    ship.hit_baddy_bullet(baddy_bullet)
        if sfx:
            self.ship.sfx_boom()
            self._create_shield_sprite(self.ship.position)

    # noinspection PyPep8Naming
    def _on_col_bullet_baddy(self, bullets, baddies):
        sfx = None
        cam_colliderect = self.cam._screen_rect.colliderect
        for baddy in baddies:
            assert baddy.kind & KIND_BADDY_I060
            r = baddy.sprite.rect
            baddy_collidepoint = r.collidepoint
            for bullet in bullets:
                assert bullet.kind & KIND_BULLET_I031

                self.num_coll_checks += 1

                if cam_colliderect(r) and baddy_collidepoint(bullet.sprite.rect.center):
                    sfx = baddy
                    self.space.remove(bullet)
                    self.renderer.remove_sprite(bullet.sprite)
                    baddy.health -= 1
                    if baddy.health <= 0:
                        self.space.remove(baddy)
                        self.renderer.remove_sprite(baddy.sprite)
        if sfx is not None:
            sfx.sfx_boom()

    def _on_col_ship_debris(self, ships, collection_of_debris):
        sfx = None
        n = 0
        for ship in ships:
            assert ship.kind & KIND_SHIP_I010
            for debris in collection_of_debris:
                assert debris.kind & KIND_METAL_I083

                self.num_coll_checks += 1

                if ship.position.get_distance(debris.position) < ship.radius + debris.radius:
                    sfx = True
                    # n += getattr(debris, 'bonus_value', 1)  # TODO: custom bonus if we want it
                    n += 1
                    self.space.remove(debris)
                    self.renderer.remove_sprite(debris.sprite)
        if sfx:
            self.ship.sfx_found_debris()
            self.ship.gain_shields(n)

    def enter(self):
        _ship_gate = (KIND_SHIP_I010, KIND_GATE_I101)
        collisiondetector.register_collision_func(_ship_gate, self._on_col_ship_gate)

        # _ship_wall = (KIND_SHIP_I010, KIND_WALL_I050)
        # collisiondetector.register_collision_func(_ship_wall, self._on_col_ship_wall)

        _ship_baddy = (KIND_SHIP_I010, KIND_BADDY_I060)
        collisiondetector.register_collision_func(_ship_baddy, self._on_col_ship_baddy)

        _ship_baddy_bullet = (KIND_SHIP_I010, KIND_BULLET_BADDY_I032)
        collisiondetector.register_collision_func(_ship_baddy_bullet, self._on_col_ship_baddy_bullet)

        _ship_soft_wall = (KIND_SHIP_I010, KIND_SOFT_WALL_I051)
        collisiondetector.register_collision_func(_ship_soft_wall, self._on_col_ship_soft_wall)

        _ship_wall_edge = (KIND_SHIP_I010, KIND_WALL_EDGE_I052)
        collisiondetector.register_collision_func(_ship_wall_edge, self._on_col_ship_wall_edge)

        _bullet_wall_edge = (KIND_BULLET_I031, KIND_WALL_EDGE_I052)
        collisiondetector.register_collision_func(_bullet_wall_edge, self._on_col_bullet_wall_edge)

        _bullet_soft_wall = (KIND_BULLET_I031, KIND_SOFT_WALL_I051)
        collisiondetector.register_collision_func(_bullet_soft_wall, self._on_col_bullet_soft_wall)

        _baddy_bullet_wall_edge = (KIND_BULLET_BADDY_I032, KIND_WALL_EDGE_I052)
        collisiondetector.register_collision_func(_baddy_bullet_wall_edge, self._on_col_bullet_wall_edge)

        _baddy_bullet_soft_wall = (KIND_BULLET_BADDY_I032, KIND_SOFT_WALL_I051)
        collisiondetector.register_collision_func(_baddy_bullet_soft_wall, self._on_col_bullet_soft_wall)

        _bullet_baddy = (KIND_BULLET_I031, KIND_BADDY_I060)
        collisiondetector.register_collision_func(_bullet_baddy, self._on_col_bullet_baddy)

        _ship_debris = (KIND_SHIP_I010, KIND_METAL_I083)
        collisiondetector.register_collision_func(_ship_debris, self._on_col_ship_debris)

        self.bullet_clean_timer.start()

        # item = [prev_stat, image, rect]
        self.hud_player = [None, None, None]
        self.hud_shields = [None, None, None]
        self.hud_guns = [None, None, None]
        self.hud_fps = [None, None, None]
        self.hud_objects = [None, None, None]
        self.hud_coll_checks = [None, None, None]
        self.hud_image_cache = [None, None, None]
        self._update_hud()  # make sure that in all cases the hud images exist

        self.wait_on_lazy_player()

    def exit(self):
        logger.debug('ContextGamePlay cleaning up...')
        for e in self.space.get_all_entities():
            self.space.remove(e)
        e = None
        for s in self.renderer.get_sprites():
            self.renderer.remove_sprite(s)
        s = None
        del self.renderer._sprite_stack[:]
        del self.bullets[:]
        del self._background
        Timer.scheduler.clear()
        Sprite._dirty_sprites.clear()
        Sprite._image_cache.clear()
        collisiondetector.collision_functions.clear()
        gc.collect()

        # stopping timers
        self.baddy_shoot_timer.stop()
        self.friendly_shoot_timer.stop()
        self.bullet_clean_timer.stop()

        try:
            logger.debug('Img Cache size={} bytes', Sprite._image_cache.size())
            logger.debug('GC isenabled={}', gc.isenabled())
            logger.debug('GC count={}', gc.get_count())
            logger.debug('GC stats={}', gc.get_stats())
        except:
            pass

        self.wait_on_lazy_player()

        logger.debug('ContextGamePlay exiting')

    def suspend(self):
        pass

    def resume(self):
        pass

    def wait_on_lazy_player(self):
        logger.debug('WAITING ON LAZY PLAYER...')
        key_down_actions = event_map[pygame.KEYDOWN]
        clock = pygame.time.Clock()
        waiting = 1
        while waiting:
            logger.debug('WAIT 250 ms')
            clock.tick(4)
            pygame.event.clear()
            keys_state = pygame.key.get_pressed()
            waiting = 0
            for keys, action in key_down_actions.items():
                k, mod = keys
                waiting += int(keys_state[k])
        logger.debug("WAITING NO LONGER - LET'S GO!")

    def update(self, delta_time, sim_time):
        self._handle_events()
        # update stuff
        Timer.scheduler.update(delta_time, sim_time)
        cam_rect = self.cam.world_rect
        entities = self.space.update_in_rect(*cam_rect)
        self.num_visible = len(entities)
        self.num_updated = 0
        self.baddies_on_screen = []
        for ent in entities:
            if ent.kind not in (KIND_WALL_EDGE_I052, KIND_WALL_I050):
                self.num_updated += 1
                ent.update(delta_time, sim_time)
            if ent.kind == KIND_BADDY_I060:
                self.baddies_on_screen.append(ent)
        if self.baddies_on_screen:
            if not self.baddy_shoot_timer._is_running:
                logger.debug('BADDY TIMER started')
                self.baddy_shoot_timer.start()
        elif self.baddy_shoot_timer._is_running:
            logger.debug('BADDY TIMER stopped')
            self.baddy_shoot_timer.stop()

        # self.renderer.clear()
        # self.renderer.add_sprites((_e.sprite for _e in entities if _e.sprite))

        self.num_coll_checks = 0
        # check_collisions_between(KIND_SHIP_I010, KIND_WALL_I050, self.cam.world_rect, self.space)
        check_collisions_between_all(KIND_SHIP_I010, KIND_SOFT_WALL_I051, self.space, entities)
        check_collisions_between_all(KIND_SHIP_I010, KIND_WALL_EDGE_I052, self.space, entities)
        check_collisions_between_all(KIND_SHIP_I010, KIND_GATE_I101, self.space, entities)
        check_collisions_between_all(KIND_SHIP_I010, KIND_BADDY_I060, self.space, entities)
        check_collisions_between_all(KIND_BADDY_I060, KIND_BULLET_I031, self.space, entities)
        check_collisions_between_all(KIND_SHIP_I010, KIND_BULLET_BADDY_I032, self.space, entities)
        check_collisions_between_all(KIND_SHIP_I010, KIND_METAL_I083, self.space, entities)
        check_collisions_between_nearest_all(KIND_BULLET_I031, KIND_WALL_EDGE_I052, self.space, entities)
        check_collisions_between_nearest_all(KIND_BULLET_I031, KIND_SOFT_WALL_I051, self.space, entities)
        check_collisions_between_nearest_all(KIND_BULLET_BADDY_I032, KIND_WALL_EDGE_I052, self.space, entities)
        check_collisions_between_nearest_all(KIND_BULLET_BADDY_I032, KIND_SOFT_WALL_I051, self.space, entities)

        # check win/loose conditions
        self._check_win()
        self._check_loose()

        self._update_hud()
        self._update_image_cache()

    def _update_hud(self):
        def update_debug(which):
            return (which and HUD_SHOW_DEBUGS == 'some') or HUD_SHOW_DEBUGS == 'all'

        def update_hud_item(item, label_fmt, cur, pos):
            prev, img, rect = item
            if prev != cur:
                text = label_fmt.format(*cur)
                img = GameResources.get(GameResources.HUD_FONT).render(text, True, HUD_COLOR)
                rect = img.get_rect(topleft=pos)
                item[:] = cur, img, rect
            pos.y += rect.height + 3
            self.hud_items.append(item)

        self.hud_items = []
        pos = Point2(x=10, y=10)
        update_hud_item(self.hud_player, '{}', ('PLAYER',), pos)
        update_hud_item(self.hud_shields, 'Shields {}', (self.ship.shield_level,), pos)
        update_hud_item(self.hud_guns, 'Guns {}', (len(self.ship.extra_guns),), pos)
        if update_debug(HUD_SHOW_FPS):
            update_hud_item(self.hud_fps, 'Clock {:.0f}/{:.0f}/{:.0f}',
                            (settings.FPS, settings.GFX_FPS, settings.SIM_FPS), pos)
        if update_debug(HUD_SHOW_ENTITIES):
            update_hud_item(self.hud_objects, 'Entities {}/{}/{}',
                            (self.num_updated, self.num_visible, len(self.space)), pos)
        if update_debug(HUD_SHOW_COLLISIONS):
            update_hud_item(self.hud_coll_checks, 'Collisions {}', (self.num_coll_checks,), pos)
        if update_debug(HUD_SHOW_IMAGE_CACHE):
            ic = Sprite._image_cache
            update_hud_item(self.hud_image_cache, 'Img Cache {} @ {} MB / {}%',
                            (ic.count(), ic.size() // 2**20, ic.hit_ratio()), pos)

    def _update_image_cache(self):
        # TODO: this is dirty
        Sprite._image_cache.update()

    def _handle_events(self):
        for action, extra in get_actions(event_map, pygame.event.get()):
            logger.debug("taking action {0}, {1}", action, extra)
            if action == ACTION_QUIT:
                self.pop(self.get_stack_length())  # quit application
            elif action == ACTION_ESCAPE:
                # pause or just quit this context
                self.pop()
            elif action == ACTION_MOVE_DOWN:
                self.ship.vel.y += SHIP_MAX_SPEED
            elif action == ACTION_MOVE_UP:
                self.ship.vel.y -= SHIP_MAX_SPEED
            elif action == ACTION_MOVE_LEFT:
                self.ship.vel.x -= SHIP_MAX_SPEED
            elif action == ACTION_MOVE_RIGHT:
                self.ship.vel.x += SHIP_MAX_SPEED
            elif action == ACTION_STOP_DOWN:
                self.ship.vel.y -= SHIP_MAX_SPEED
            elif action == ACTION_STOP_UP:
                self.ship.vel.y += SHIP_MAX_SPEED
            elif action == ACTION_STOP_LEFT:
                self.ship.vel.x += SHIP_MAX_SPEED
            elif action == ACTION_STOP_RIGHT:
                self.ship.vel.x -= SHIP_MAX_SPEED
            elif action == ACTION_FIRE:
                self.friendly_shoot_timer.start()
                self._create_friendly_bullets(None)
            elif action == ACTION_HOLD_FIRE:
                self.friendly_shoot_timer.stop()
            elif ALlOW_CHEATS:
                if action == ACTION_WIN_LEVEL:
                    logger.debug(">>>WIN<<<")
                    gates = [o for o in self.space.get_all_entities() if o.kind & settings.KIND_GATE_I101]
                    logger.debug(">>> GATES: num={}", len(gates))
                    for gate in gates:
                        logger.debug(">>> gate.next_map={}", getattr(gate, 'next_map', None))
                        if getattr(gate, 'next_map', None):
                            logger.debug('>>>GATE FOUND: gating on cheat key')
                            self.ship.position = gate.position
                            continue
                    # more = False
                    # for g in [o for o in self.space.get_all_entities() if hasattr(o, 'next_map')]:
                    #     logger.debug('CHEAT: next_map {}', g.next_map)
                    #     if g.next_map != 'exit':
                    #         logger.debug('CHEAT: loading {}', g.next_map)
                    #         self._move_to_next_map(g.next_map)
                    #         more = True
                    #         break
                    # if not more:
                    #     logger.debug('CHEAT: no next level found: popping')
                    #     # self.pop()
                    #     self.exchange(ContextMenu(self.stats))
                elif action == ACTION_LOOSE_LEVEL:
                    logger.debug("LOOSE")
                    g = ContextGamePlay(self.stats)
                    self.exchange(g)
                elif action == ACTION_TOGGLE_DEBUG_RENDER:
                    settings.DEBUG_RENDER = not settings.DEBUG_RENDER
                elif action == ACTION_FULL_STOP:
                    self.ship.vel.x = 0
                    self.ship.vel.y = 0
                elif action == ACTION_HUD_SHOW_DEBUGS:
                    next_value = dict(all='some', some='none', none='all')
                    settings.HUD_SHOW_DEBUGS = next_value[settings.HUD_SHOW_DEBUGS]
                elif action == ACTION_ROTATE_METEORS:
                    if settings.ROTATE_METEORS_STEP == settings.INITIAL_ROTATE_METEORS_STEP:
                        settings.ROTATE_METEORS_STEP = 0
                    else:
                        settings.ROTATE_METEORS_STEP = settings.INITIAL_ROTATE_METEORS_STEP

    def draw(self, screen, do_flip=True, interpolation_factor=1.0):
        # screen.fill((0, 0, 0))
        if self._background:
            screen.blit(self._background, (0, 0))
        self.renderer.draw(screen, self.cam, fill_color=None, do_flip=False, interpolation_factor=1.0)

        # screen.blit(self.hello, self.hello_rect)
        if settings.DEBUG_RENDER:
            self._draw_debug_visuals(screen)

        self._draw_hud(screen)

        if do_flip:
            pygame.display.flip()

    def _draw_debug_visuals(self, screen):
        if not settings.DEBUG_RENDER:
            return

        to_scr = self.cam.world_to_screen

        # draw bucket grid
        bw = BUCKETS_WIDTH
        bh = BUCKETS_HEIGHT
        col = (0, 0, 255)
        for i in range(100):
            s = to_scr(Point2(i * bw, -10000)).as_tuple()
            e = to_scr(Point2(i * bw, 10000)).as_tuple()
            pygame.draw.line(screen, col, s, e)
            s = to_scr(Point2(-10000, i * bh)).as_tuple()
            e = to_scr(Point2(10000, i * bh)).as_tuple()
            pygame.draw.line(screen, col, s, e)

        # draw walls and its normal
        for we in self.space.get_all_entities(KIND_WALL_EDGE_I052):
            pygame.draw.line(screen, (100, 100, 100), to_scr(we.p0).as_tuple(), to_scr(we.p1).as_tuple(), 3)
            mid = (we.p1 - we.p0) / 2 + we.p0
            pygame.draw.line(screen, (255, 0, 0), to_scr(mid).as_tuple(), to_scr(mid + we.normal * 20).as_tuple(), 3)

        # draw ships radius
        pygame.draw.circle(screen, (255, 0, 0), to_scr(self.ship.position).as_tuple(int), self.ship.radius, 2)

        # draw velocity vector of ship
        vel = self.cam.world_to_screen(self.ship.position + self.ship.vel)
        pygame.draw.line(screen, (0, 255, 0), to_scr(self.ship.position).as_tuple(), vel.as_tuple())

        # draw enemy ship hit boxes
        for e in self.space.get_from_rect(*self.cam.world_rect):
            if e.kind & KIND_BADDY_I060:
                pygame.draw.circle(screen, (255, 0, 0), to_scr(e.position).as_tuple(int), e.radius, 2)

        # draw player bullet hit boxes
        for e in self.space.get_from_rect(*self.cam.world_rect):
            if e.kind & (KIND_BULLET_I031 | KIND_BULLET_BADDY_I032):
                pygame.draw.circle(screen, (255, 0, 0), to_scr(e.position).as_tuple(int), e.radius, 1)

        # draw debris hit boxes
        for e in self.space.get_from_rect(*self.cam.world_rect):
            if e.kind & KIND_METAL_I083:
                pygame.draw.circle(screen, (255, 0, 0), to_scr(e.position).as_tuple(int), e.radius, 2)

    def _draw_hud(self, screen):
        for item in self.hud_items:
            val, img, rect = item
            screen.blit(img, rect)

    def _check_win(self):
        pass

    def _check_loose(self):
        if self.ship.is_dead():
            # reset level
            from .context_loading import ContextLoading
            self.exchange(ContextLoading(self.stats, "Restoring from gate's\ntransmission buffer..."))

    def load_map(self, name):
        img_cache = {}
        sprites = []
        has_background = False

        logger.info(">>> LOADING MAP {0}", name)
        # hack
        if '05_level' in name and settings.ACTION_ROTATE_METEORS in (1, 2):
            settings.ROTATE_METEORS_STEP = 3
        self._shield_image = pygame.image.load("./data/graphics/shield.png").convert_alpha()
        self._shield_down_image = pygame.image.load("./data/graphics/shield_down.png").convert_alpha()

        map_info = pytmxloader.load_map_from_file_path(name)
        # TODO: he is broke
        Sprite.configure_global_image_cache(True, max_cache_entry_count=100000)
        Sprite._image_cache.max_memory = 2 * 2 ** 30  # 1*2**30 = 1 GB
        for idx, layer in enumerate(map_info.layers):
            if not layer.visible:
                continue
            pxm = layer.properties.get("parallax_x_min", 1.0)
            pxa = layer.properties.get("parallax_x_max", 1.0)
            # pym = layer.properties.get("parallax_y_min", 1.0)
            # pya = layer.properties.get("parallax_y_max", 1.0)
            if layer.layer_type == TILE_LAYER_TYPE:
                logger.debug("layer {0}", layer.name)
                for tile_y in range(layer.height):
                    logger.debug("line {0}", tile_y)
                    for tile_x in range(layer.width):
                        pygame.event.pump()
                        tile = layer.data_yx[tile_y][tile_x]
                        if tile is None:
                            continue
                        pos = Point2(tile.tile_x + tile.offset_x, tile.tile_y + tile.offset_y)
                        ti = tile.tile_info
                        img = self._get_cached_image(img_cache, ti)

                        kind = ti.properties.get("kind", None)
                        if kind != "background":
                            pf = random.uniform(pxm, pxa)
                            parallax_factors = Vec2(pf, pf)
                            spr = Sprite(img, pos, anchor='topleft', z_layer=idx, parallax_factors=parallax_factors,
                                         name=(ti.tileset.image_path_rel_to_cwd, ti.spritesheet_x, ti.spritesheet_y))
                            spr.zoom = pf
                            sprites.append(spr)
                        if kind == "ship":
                            # sharing position vector with the sprite!
                            # pos.x += 1  # TODO: this works around the UP-sneaks-through-wall-edge bug;
                            # TODO: because both ship and wall edges snap to grid; hopefully nobody will
                            # TODO: ever discover this :)
                            pos += Vec2(ti.tileset.tile_width / 2, ti.tileset.tile_height / 2)
                            spr.anchor = 'center'
                            ent = Ship(pos, 64, 64, spr)
                            ent.extra_guns = list(self.stats.extra_guns)
                            ent.guns = list(self.stats.guns)
                            ent.shield_level = self.stats.shield_level
                            spr.old_position = ent.old_pos  # sharing this vector as well
                            self.space.add(ent)
                            self.cam.track = ent
                            assert self.ship is None, "there should be only one ship on the map!"
                            self.ship = ent
                            spr.z_layer += 1
                        elif kind == "wall":
                            spr.configure_image_cache(False)
                            wall = Wall(pos, KIND_WALL_I050, 64, 64, spr, 1)
                            self.space.add(wall)
                        elif kind == "destructible":
                            spr.position += Vec2(ti.tileset.tile_width / 2, ti.tileset.tile_height / 2)
                            spr.anchor = "center"
                            # TODO: flip and rotation seem not to work as expected ... :/
                            spr.flipped_x = ti.flip_x
                            spr.flipped_y = ti.flip_y
                            spr.rotation = ti.angle
                            spr.name = "meteor destruct" + str(id(spr))
                            spr.configure_image_cache(False)
                            health = int(ti.properties.get("health", 1))
                            wall = Wall(spr.position, KIND_SOFT_WALL_I051, 64, 64, spr, health)
                            self.space.add(wall)
                        elif kind == "meteor":
                            spr.position = pos + Vec2(ti.tileset.tile_width / 2, ti.tileset.tile_height / 2)
                            spr.anchor = "center"
                            spr.flipped_x = random.choice([True, False])
                            spr.flipped_y = random.choice([True, False])
                            spr.rotation = random.choice(range(0, 360, 5))
                            spr.zoom = SCALE_METEORS
                            pf = random.uniform(0.7, 0.99)
                            spr.position *= pf
                            spr.parallax_factors = Vec2(pf, pf)
                            rot = random.randrange(1, 6) * 0.2
                            spr.z_layer = idx + pf
                            if ROTATE_METEORS_STEP:
                                self.renderer.add_sprite(spr)
                                for a in range(0, 360, ROTATE_METEORS_STEP):
                                    spr.rotation = a
                                    self.renderer._update_dirty_sprites()
                            meteor = Meteor(spr.position, rot, spr)
                            self.space.add(meteor)
                        elif kind == "background":
                            if has_background is True:
                                sprites.remove(spr)
                                continue
                            has_background = True

                            self._background = img

                            # img = spr.image
                            # img_size = Vec2(0, 0)
                            # img_size.set_from_iterable(img.get_size())
                            # pf = 0.0
                            # map_size = Vec2(layer.width * layer.tile_w, layer.height * layer.tile_h)
                            # if map_size.y > map_size.x:  # keep aspect ratio
                            #     # new_size = f * img.size
                            #     f = map_size.y / img_size.y
                            # else:
                            #     f = map_size.x / img_size.x
                            #
                            # new_size = img_size * f
                            # # noinspection PyUnresolvedReferences
                            # img = pygame.transform.smoothscale(img, new_size.as_tuple(int))
                            # spr.set_image(img)
                            # spr.anchor = "center"
                            # spr.parallax_factors = Vec2(pf, pf)
                            # spr.position.x = SCREEN_WIDTH / 2  #map_size.x / 2
                            # spr.position.y = SCREEN_HEIGHT / 2  #map_size.y / 2
                            # # spr.position *= pf
                        elif kind == "enemy":
                            spr.position = pos + Vec2(ti.tileset.tile_width / 2, ti.tileset.tile_height / 2)
                            spr.anchor = "center"
                            health = int(ti.properties.get("health", 3))
                            guns_count = int(ti.properties.get("guns", 1))
                            baddy = Baddy(spr.position, 64, 64, spr, guns_count, health)
                            self.space.add(baddy)
                        elif kind == "debries":
                            spr.position = pos + Vec2(ti.tileset.tile_width / 2, ti.tileset.tile_height / 2)
                            spr.anchor = "center"
                            spr.flipped_x = random.choice([True, False])
                            spr.flipped_y = random.choice([True, False])
                            spr.rotation = random.choice(range(0, 360, 5))
                            debris = Debris(spr.position, 64, 64, spr)
                            self.space.add(debris)

                        text = ti.properties.get("text", None)
                        text_pos = pos + Vec2(ti.tileset.tile_width / 2, ti.tileset.tile_height + 5)
                        self._create_text_sprite(sprites, text, text_pos)

            elif layer.layer_type == pytmxloader.OBJECT_LAYER_TYPE:
                for ot in layer.tiles:
                    ti = ot.tile_info
                    pos = Point2(ot.x, ot.y) + Vec2(ti.tileset.tile_width / 2, -ti.tileset.tile_height / 2)
                    img = self._get_cached_image(img_cache, ti)
                    pf = random.uniform(pxm, pxa)
                    parallax_factors = Vec2(pf, pf)
                    spr = Sprite(img, pos, anchor="center", z_layer=idx, parallax_factors=parallax_factors,
                                 name=(ti.tileset.image_path_rel_to_cwd, ti.spritesheet_x, ti.spritesheet_y))
                    sprites.append(spr)
                    kind = ot.properties.get("kind", None)
                    if kind == "gate":
                        gate = Gate(ot.properties.get('next'), pos, 64, 64, spr)
                        self.space.add(gate)
                    elif kind == "enemy":
                        health = int(ot.properties.get("health", 3))
                        guns_count = ot.properties.get("guns", 1)
                        baddy = Baddy(pos, 64, 64, spr, guns_count, health)
                        self.space.add(baddy)
                    elif kind == "debries":
                        spr.flipped_x = random.choice([True, False])
                        spr.flipped_y = random.choice([True, False])
                        spr.rotation = random.choice(range(0, 360, 5))
                        debris = Debris(spr.position, 64, 64, spr)
                        self.space.add(debris)

                    text = ot.properties.get("text", None)
                    text_pos = pos + Vec2(0, ti.tileset.tile_height / 2 + 5)
                    self._create_text_sprite(sprites, text, text_pos)

                for ot in layer.poly_lines:
                    p_last = ot.pixel_points[0]
                    normal = ot.properties.get("normal", "left")
                    for p in ot.pixel_points[1:]:
                        wall_edge = WallEdge([p_last, p], normal)
                        self.space.add(wall_edge)
                        p_last = p

        self.renderer.add_sprites(sprites)  # this is many times faster than adding single sprites (only one z sort)!
        logger.info(">>> LOADING MAP {0} DONE", name)

        assert self.ship is not None, "a ship is missing on the map!"
        assert len(self.space.get_all_entities(KIND_GATE_I101)) > 0, "there should be at least one gate!"

    def _create_text_sprite(self, sprites, text, text_pos):
        if text is not None:
            ts = TextSprite(text, text_pos, z_layer=99, anchor='midtop')
            ts.name = str(id(ts))
            sprites.append(ts)

    def _get_cached_image(self, img_cache, ti):
        key = (ti.tileset.image_path_rel_to_cwd, ti.spritesheet_x, ti.spritesheet_y, ti.flip_x, ti.flip_y, ti.angle)
        img = img_cache.get(key, None)
        if img is None:
            spritesheet = img_cache.get(ti.tileset.image_path_rel_to_cwd, None)
            if spritesheet is None:
                # noinspection PyUnresolvedReferences
                spritesheet = pygame.image.load(ti.tileset.image_path_rel_to_cwd).convert_alpha()
                img_cache[ti.tileset.image_path_rel_to_cwd] = spritesheet
            area = pygame.Rect(ti.spritesheet_x, ti.spritesheet_y, ti.tileset.tile_width, ti.tileset.tile_height)
            img = pygame.Surface(area.size, spritesheet.get_flags(), spritesheet)
            img.blit(spritesheet, (0, 0), area)
            img = pygame.transform.flip(img, ti.flip_x, ti.flip_y)
            if ti.angle != 0:
                img = pygame.transform.rotate(img, ti.angle)
            ip = ti.tileset.image_path_rel_to_cwd
            if 'space1_0_4x_smooth.png' in ip:
                img = img.convert()
            img_cache[key] = img
        return img

    # noinspection PyUnusedLocal
    def clean_out_of_scope_bullets(self, *args):
        # bullets = self.space.get_all_entities(KIND_BULLET_I031)
        r = self.cam.world_rect
        r_cp = r.collidepoint
        to_remove = (_b for _b in self.bullets if not r_cp(_b.position.x, _b.position.y))
        for _rb in to_remove:
            self.space.remove(_rb)
            self.renderer.remove_sprite(_rb.sprite)
            self.bullets.remove(_rb)
