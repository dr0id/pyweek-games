# -*- coding: utf-8 -*-
"""
TODO: docstring
"""
from __future__ import division, print_function, absolute_import

import logging
import os
import sys

import pygame

from . import settings
from .gameresources import GameResources

logger = logging.getLogger(__name__)


def _log_pygame_keys():
    from pygame import locals
    keys = sorted(pygame.locals.__dict__.keys())

    logger.info("=" * 20)
    logging.info("pygame keys:")
    for key in keys:
        if key.startswith('--') or 'pyd' in key:
            continue
        logger.info('{}: {}'.format(key, pygame.locals.__dict__[key]))
    logger.info("=" * 20)


def main():
    from . import pyknic
    # from .pyknic import settings as pyknic_settings
    # pyknic_settings.appdir = sys.path[0]

    os.environ['SDL_VIDEO_CENTERED'] = '1'
    pygame.mixer.pre_init(frequency=settings.MIXER_FREQUENCY, buffer=settings.MIXER_BUFFER_SIZE)
    pygame.init()
    pygame.mixer.set_reserved(settings.MIXER_RESERVED_CHANNELS)
    pygame.display.set_caption(settings.CAPTION)
    _path_to_icon = "./data/icon.png"
    try:
        icon = pygame.image.load(_path_to_icon)
        pygame.display.set_icon(icon)
    except pygame.error as pger:
        logger.error("could not find icon.png in %s: error: %s", os.path.abspath(_path_to_icon), str(pger))

    screen = pygame.display.set_mode(settings.SCREEN_SIZE, settings.FLAGS, settings.BIT_DEPTH)

    _log_pygame_keys()

    clock = pygame.time.Clock()
    draw_clock = pygame.time.Clock()
    sim_clock = pygame.time.Clock()
    settings.CLOCK = clock
    settings.DRAW_CLOCK = draw_clock
    settings.SIM_CLOCK = sim_clock

    GameResources.load()

    from .context_menu import ContextMenu
    pyknic.context.push(ContextMenu())

    lock_stepper = pyknic.timing.LockStepper(timestep=settings.SIM_TIME_STEP, max_steps=8, max_dt=settings.SIM_TIME_STEP * 10)

    pyknic.context.set_deferred_mode(True)  # avoid routing problems, due stack operations only at update
    context_len = pyknic.context.length
    context_top = pyknic.context.top
    context_update = pyknic.context.update

    def _update_top_context(dt, simt, ls):
        top_context = context_top()
        if top_context:
            top_context.update(dt, simt)
        context_update()

    lock_stepper.event_integrate += lambda _dt, _simt, *args: sim_clock.tick()
    lock_stepper.event_integrate += _update_top_context

    # if __debug__:
    scheduler = pyknic.timing.Scheduler()
    scheduler.schedule(_print_fps, 2, 0, clock, draw_clock, sim_clock)

    def _do_draw(dt, simt, *args):
        top_context = context_top()
        if top_context:
            top_context.draw(screen, do_flip=True, interpolation_factor=lock_stepper.alpha)

    frame_cap = pyknic.timing.FrameCap()
    frame_cap.event_update += lambda _dt, _st, *_args: draw_clock.tick()
    frame_cap.event_update += _do_draw

    game_time = pyknic.timing.GameTime()
    game_time.event_update += frame_cap.update
    game_time.event_update += lock_stepper.update
    game_time.event_update += scheduler.update

    if settings.MUSIC_PLAY:
        pygame.mixer.music.load("data/music/DST-RailJet-LongSeamlessLoop.ogg")
        pygame.mixer.music.play(loops=-1)
        pygame.mixer.music.set_volume(settings.MUSIC_VOLUME)

    while context_len():
        dt = clock.tick() / 1000.0  # convert to seconds
        game_time.update(dt)
    # pygame.mixer.music.stop()

    try:
        pygame.mixer.music.fadeout(2000)
        while pygame.mixer.music.get_busy():
            pygame.time.wait(100)
    except IndexError:
        pass

    pygame.quit()


def _print_fps(clock, draw_clock, sim_clock):
    if settings.PRINT_FPS:
        settings.FPS = fps = clock.get_fps()
        settings.GFX_FPS = gfx_fps = draw_clock.get_fps()
        settings.SIM_FPS = sim_fps = sim_clock.get_fps()
        logger.debug('main loop fps: %s   draw fps: %s   sim fps: %s', str(fps), str(gfx_fps), str(sim_fps))
    return 1  # return next interval


# this is needed in order to work with py2exe
if __name__ == '__main__':
    main()
