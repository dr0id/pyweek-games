# -*- coding: utf-8 -*-

from .pyknic.pygame_wrapper.resource import resources, FONT, IMAGE, SOUND, SONG

from . import settings


class GameResources(object):
    HUD_FONT = -1
    PERSON_FONT = -1
    LOG_FONT = -1
    SCREEN_FONT = -1

    IMG_BACKGROUND_1 = -1

    SFX_SHIP_FIRES_BULLET = -1
    SFX_BADDY_FIRES_BULLET = -1
    SFX_BULLET_HITS_WALL = -1
    SFX_BULLET_HITS_SOFT_WALL = -1
    SFX_BULLET_HITS_SHIP = -1
    SFX_BULLET_HITS_BADDY = -1
    SFX_PICK_UP_DEBRIS = -1
    SFX_LESSER_EVIL_A = -1
    SFX_LESSER_EVIL_B = -1

    @staticmethod
    def load():
        GameResources.HUD_FONT = resources.load(FONT, settings.HUD_FONT.name, settings.HUD_FONT.size)
        GameResources.PERSON_FONT = resources.load(FONT, settings.PERSON_FONT.name, settings.PERSON_FONT.size)
        GameResources.LOG_FONT = resources.load(FONT, settings.LOG_FONT.name, settings.LOG_FONT.size)
        GameResources.SCREEN_FONT = resources.load(FONT, settings.SCREEN_FONT.name, settings.SCREEN_FONT.size)

        GameResources.IMG_BACKGROUND_1 = resources.load(IMAGE, 'data/Background-1.png', 1.0)

        GameResources.SFX_SHIP_FIRES_BULLET = resources.load(SOUND, 'data/sound/weaponfire6.ogg', 1.0)
        GameResources.SFX_BADDY_FIRES_BULLET = resources.load(SOUND, 'data/sound/weaponfire6.ogg', 0.5)
        GameResources.SFX_BULLET_HITS_WALL = resources.load(SOUND, 'data/sound/forcepulse.ogg', 0.8)
        GameResources.SFX_BULLET_HITS_SOFT_WALL = resources.load(SOUND, 'data/sound/antimaterhit.ogg', 0.8)
        GameResources.SFX_BULLET_HITS_SHIP = resources.load(SOUND, 'data/sound/menu_nclick.ogg', 0.8)
        GameResources.SFX_BULLET_HITS_BADDY = resources.load(SOUND, 'data/sound/weaponfire3.ogg', 0.4)
        GameResources.SFX_PICK_UP_DEBRIS = resources.load(SOUND, 'data/sound/Pickup_00.ogg', 0.8)
        GameResources.SFX_LESSER_EVIL_A = resources.load(SOUND, 'data/sound/weaponfire18.ogg', 0.4)
        GameResources.SFX_LESSER_EVIL_B = resources.load(SOUND, 'data/sound/weaponfire19.ogg', 0.4)

        # GameResources.X = resources.load(SONG, 'data/song/X', 1.0)

    @staticmethod
    def get(resourceid):
        return resources.get_resource(resourceid)
