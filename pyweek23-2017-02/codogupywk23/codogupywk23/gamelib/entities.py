# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'entities.py' is part of codogupywk23
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]                    # For instance:
                   |                                  * 1.2.0.1 instead of 1.2-a
                   +-|* 0 for alpha (status)          * 1.2.1.2 instead of 1.2-b2 (beta with some bug fixes)
                     |* 1 for beta (status)           * 1.2.2.3 instead of 1.2-rc (release candidate)
                     |* 2 for release candidate       * 1.2.3.0 instead of 1.2-r (commercial distribution)
                     |* 3 for (public) release        * 1.2.3.5 instead of 1.2-r5 (commercial dist with many bug fixes)


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division, absolute_import

import logging
import random

import pygame

from . import settings
from .pyknic.generators import GlobalIdGenerator
from .pyknic.mathematics import Vec2, Point2

from .gameresources import GameResources

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2017"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 

logger = logging.getLogger(__name__)
logger.debug("importing...")

_id_gen = GlobalIdGenerator()


class BaseEntity(object):

    def __init__(self, pos_point2, kind, width, height, spr, unique_id=None):
        self.sprite = spr
        self.id = unique_id
        self.kind = kind
        self.position = pos_point2
        if self.id is None:
            self.id = _id_gen.next()
        self.half_bounds = Vec2(width / 2, height / 2)

    def update(self, dt, simt):
        pass

    def hit_wall_edge(self, other):
        assert other.kind == settings.KIND_WALL_EDGE_I052

    def hit_soft_wall(self, other):
        assert other.kind == settings.KIND_SOFT_WALL_I051

    def hit_bullet(self, other):
        assert other.kind == settings.KIND_BULLET_I031

    def hit_baddy(self, other):
        assert other.kind == settings.KIND_BADDY_I060

    def hit_baddy_bullet(self, other):
        assert other.kind == settings.KIND_BULLET_BADDY_I032

    def hit_baddy2(self, other):
        assert other.kind == settings.KIND_BADDY2_I061

    def hit_station(self, other):
        assert other.kind == settings.KIND_STATION_I092

    def hit_power_up(self, other):
        assert other.kind == settings.KIND_POWER_UP_I070

    def hit_orange(self, other):
        assert other.kind == settings.KIND_ORANGE_I082

    def hit_metal(self, other):
        assert other.kind == settings.KIND_METAL_I083

    def hit_glowing_rocks(self, other):
        assert other.kind == settings.KIND_GLOWING_ROCKS_I085

    def hit_gate(self, other):
        assert other.kind == settings.KIND_GATE_I101

    def hit_black_hole(self, other):
        assert other.kind == settings.KIND_BLACK_HOLE_I130

    def hit_giant_sun(self, other):
        assert other.kind == settings.KIND_GIANT_SUN_I140


class Gate(BaseEntity):

    def __init__(self, next_map, pos_point2, width, height, spr, unique_id=None):
        BaseEntity.__init__(self, pos_point2, settings.KIND_GATE_I101, width, height, spr, unique_id)
        self.next_map = next_map


class Meteor(BaseEntity):

    def __init__(self, pos_point2, rot, spr, unique_id=None):
        BaseEntity.__init__(self, pos_point2, settings.KIND_METEOR, 0, 0, spr, unique_id)
        self.rot = rot
        self.angle = self.sprite.rotation

    def update(self, dt, simt):
        step = settings.ROTATE_METEORS_STEP
        if step and self.rot:
            self.angle += self.rot
            self.sprite.rotation = self.angle // step * step


class Wall(BaseEntity):

    def __init__(self, pos_point2, kind, width, height, spr, health=1, unique_id=None):
        pos_point2 = Point2(pos_point2.x, pos_point2.y)
        BaseEntity.__init__(self, pos_point2, kind, width, height, spr, unique_id)
        x, y = (self.position - self.half_bounds).as_tuple()
        w, h = (self.half_bounds * 2).as_tuple()
        self.rect = pygame.Rect(x, y, w, h)
        self.health = health


class WallEdge(BaseEntity):
    def __init__(self, pixel_points, normal, spr=None, unique_id=None):
        self.p0 = Point2(*pixel_points[0])
        self.p1 = Point2(*pixel_points[1])
        if normal == "left":
            self.normal = (self.p1 - self.p0).normal_right.normalized
        else:
            self.normal = (self.p1 - self.p0).normal_left.normalized
        xx = sorted(p[0] for p in pixel_points)
        yy = sorted(p[1] for p in pixel_points)
        width = xx[1] - xx[0]
        height = yy[1] - yy[0]
        pos_point2 = Point2(xx[0] + width / 2, yy[0] + height / 2)
        BaseEntity.__init__(self, pos_point2, settings.KIND_WALL_EDGE_I052, width, height, spr, unique_id)


class Ship(BaseEntity):

    def __init__(self, pos_point2, width, height, spr, unique_id=None):
        BaseEntity.__init__(self, pos_point2, settings.KIND_SHIP_I010, width, height, spr, unique_id)
        self.vel = Vec2(0.0, 0.0)
        self.acc = Vec2(0.0, 0.0)
        self.old_pos = Point2(pos_point2.x, pos_point2.y)
        self.active_collisions = set()
        self.direction = Vec2(1, 0)
        self.redundant_collisions = set()
        self.default_gun = []
        self.guns = [Vec2(1, 1).normalized, Vec2(0, 1), Vec2(-1, 1).normalized]
        self.guns.extend([-_x for _x in self.guns])
        self.guns.append(Vec2(-1, 0))
        self.extra_guns = []

        # values that are potential different for different ship types
        self.radius = 15
        self.max_speed = settings.SHIP_MAX_SPEED
        self.shield_level = settings.DEFAULT_SHIELD_LEVEL

    def update(self, dt, simt):
        self.old_pos.x = self.position.x
        self.old_pos.y = self.position.y

        speed = self.vel.length_sq
        if speed > 0.1:  # rounding tolerance
            self.direction = self.vel.normalized
        else:
            self.vel.x = 0
            self.vel.y = 0

        self.position += self.vel.scaled(self.max_speed) * dt

        self.sprite.rotation = -self.direction.angle

        # track previous collisions so self.hit* can detect a new hit vs. redundant hit
        inactive_collisions = self.redundant_collisions - self.active_collisions
        self.redundant_collisions -= inactive_collisions
        self.redundant_collisions |= self.active_collisions
        self.active_collisions.clear()

    def sfx_fire_bullet(self):
        sfx = GameResources.get(GameResources.SFX_SHIP_FIRES_BULLET)
        channel = pygame.mixer.Channel(settings.MIXER_CHANNEL_SHIP_FIRE_BULLET)
        channel.play(sfx)

    def sfx_boom(self):
        sfx = GameResources.get(GameResources.SFX_BULLET_HITS_SHIP)
        channel = pygame.mixer.Channel(settings.MIXER_CHANNEL_SHIP_BOOM)
        channel.play(sfx)

    def sfx_gate(self):
        a_or_b = random.choice((GameResources.SFX_LESSER_EVIL_A, GameResources.SFX_LESSER_EVIL_B))
        sfx = GameResources.get(a_or_b)
        channel = pygame.mixer.Channel(settings.MIXER_CHANNEL_LESSER_EVIL)
        channel.play(sfx)

    def sfx_found_debris(self):
        sfx = GameResources.get(GameResources.SFX_PICK_UP_DEBRIS)
        channel = pygame.mixer.Channel(settings.MIXER_CHANNEL_PICK_UP_DEBRIS)
        channel.play(sfx)

    def lose_a_shield(self):
        """penalty, lose a gun"""
        if self.shield_level > 0:
            logger.debug('Ship: lose a shield')
            self.shield_level -= 1
            return True
        logger.debug('Ship: no shield remaining')
        return False

    def lose_a_gun(self):
        """penalty, lose a gun"""
        if self.extra_guns:
            i = random.randrange(len(self.extra_guns))
            logger.debug('Ship: lose a gun {0}', i)
            self.guns.append(self.extra_guns[i])
            del self.extra_guns[i]
            return True
        logger.debug('Ship: no extra guns remaining')
        return False

    def gain_shields(self, n=1):
        """bonus, gain a shield"""
        logger.debug('Ship: gain {} shields', n)
        if self.shield_level < settings.MAX_SHIELD_LEVEL:
            self.shield_level += n
            if self.shield_level > settings.MAX_SHIELD_LEVEL:
                self.shield_level = settings.MAX_SHIELD_LEVEL
        else:
            if self.guns:
                i = random.randrange(len(self.guns))
                self.extra_guns.append(self.guns[i])
                del self.guns[i]

    def damage(self):
        self.lose_a_shield() or self.lose_a_gun()

    def is_dead(self):
        return self.shield_level <= 0 and len(self.extra_guns) <= 0

    def hit_baddy(self, other):
        self.damage()

    def hit_baddy_bullet(self, other):
        self.damage()

    def hit_wall_edge(self, other):
        BaseEntity.hit_wall_edge(self, other)
        self._track_wall_collisions(other)

    def _track_wall_collisions(self, other):
        self.active_collisions.add(other)
        if other in self.redundant_collisions:
            # redundant hit detected - no penalty
            pass
        else:
            # new hit - charge penalty
            self.damage()

    def hit_soft_wall(self, other):
        BaseEntity.hit_soft_wall(self, other)
        self._track_wall_collisions(other)


class Baddy(BaseEntity):
    fire_max_rate = 3

    def __init__(self, pos_point2, width, height, spr, guns_count, health, unique_id=None):
        BaseEntity.__init__(self, pos_point2, settings.KIND_BADDY_I060, width, height, spr, unique_id)
        self.direction = Vec2(-1, 0)
        spr.rotation = -self.direction.angle // settings.BADDY_ANGLE_PREC * settings.BADDY_ANGLE_PREC
        self.default_gun = []
        if guns_count == 2:
            self.extra_guns = [Vec2(1, 1).normalized, Vec2(1, -1).normalized]
        elif guns_count == 3:
            self.extra_guns = [Vec2(1, 1).normalized, Vec2(1, -1).normalized, Vec2(1, 0)]
        else:
            self.extra_guns = [Vec2(1, 0)]
        self.radius = 15
        self.vel = Vec2(0.0, 0.0)
        self.health = health

    def sfx_fire_bullet(self):
        sfx = GameResources.get(GameResources.SFX_SHIP_FIRES_BULLET)
        channel = pygame.mixer.Channel(settings.MIXER_CHANNEL_SHIP_FIRE_BULLET)
        channel.play(sfx)

    def sfx_boom(self):
        sfx = GameResources.get(GameResources.SFX_BULLET_HITS_BADDY)
        channel = pygame.mixer.Channel(settings.MIXER_CHANNEL_BADDY_BOOM)
        channel.play(sfx)


class Bullet(BaseEntity):
    _kind = settings.KIND_BULLET_I031
    COLOR = (0, 255, 0)
    surf = None

    def __init__(self, pos_point2, width, height, spr, unique_id=None):
        BaseEntity.__init__(self, pos_point2, self._kind, width, height, spr, unique_id)
        self.vel = Vec2(0.0, 0.0)
        self.acc = Vec2(0.0, 0.0)
        self.radius = 3
        self.wall_hit_count = 0
        if Bullet.surf is None:
            w = 7
            Bullet.surf = pygame.Surface((w, w))
            Bullet.surf.fill(Bullet.COLOR)
            pygame.draw.rect(Bullet.surf, (0, 0, 0), (0, 0, w, w), 1)
            # pygame.image.save(Bullet.surf, 'gumm.png')
            # quit()

        # self.old_pos = Point2(pos_point2.x, pos_point2.y)

    def sfx_hit_wall(self):
        sfx = GameResources.get(GameResources.SFX_BULLET_HITS_WALL)
        channel = pygame.mixer.Channel(settings.MIXER_CHANNEL_BULLET_HIT_WALL)
        channel.play(sfx)

    def sfx_hit_soft_wall(self):
        sfx = GameResources.get(GameResources.SFX_BULLET_HITS_SOFT_WALL)
        channel = pygame.mixer.Channel(settings.MIXER_CHANNEL_BULLET_SOFT_WALL)
        channel.play(sfx)

    def update(self, dt, simt):
        # self.old_pos.x = self.position.x
        # self.old_pos.y = self.position.y

        self.vel += self.acc * dt
        self.position += self.vel * dt


class BaddyBullet(Bullet):
    _kind = settings.KIND_BULLET_BADDY_I032
    COLOR = (255, 0, 0)
    surf = None

    def __init__(self, pos_point2, width, height, spr, unique_id=None):
        Bullet.__init__(self, pos_point2, width, height, spr, unique_id)
        if BaddyBullet.surf is None:
            w = 7
            BaddyBullet.surf = pygame.Surface((w, w))
            BaddyBullet.surf.fill(BaddyBullet.COLOR)
            pygame.draw.rect(BaddyBullet.surf, (0, 0, 0), (0, 0, w, w), 1)


class Debris(BaseEntity):
    def __init__(self, pos_point2, width, height, spr, unique_id=None):
        BaseEntity.__init__(self, pos_point2, settings.KIND_METAL_I083, width, height, spr, unique_id)
        self.radius = 15


class Powerup(BaseEntity):
    def __init__(self, pos_point2, width, height, spr, unique_id=None):
        BaseEntity.__init__(self, pos_point2, settings.KIND_BULLET_I031, width, height, spr, unique_id)

logger.debug("imported")
