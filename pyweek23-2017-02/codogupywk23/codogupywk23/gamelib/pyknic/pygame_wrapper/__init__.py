# -*- coding: utf-8 -*-
"""
.. todo:: docstring
.. todo:: make the relative import failure a warning?

TODO: move this to template! Or change to semvers.org schema!
::

    Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]                    # For instance:
                   |                                  * 1.2.0.1 instead of 1.2-a
                   +-|* 0 for alpha (status)          * 1.2.1.2 instead of 1.2-b2 (beta with some bug fixes)
                     |* 1 for beta (status)           * 1.2.2.3 instead of 1.2-rc (release candidate)
                     |* 2 for release candidate       * 1.2.3.0 instead of 1.2-r (commercial distribution)
                     |* 3 for (public) release        * 1.2.3.5 instead of 1.2-r5 (commercial dist with many bug fixes)



"""
from __future__ import division, print_function

import logging

import pygame


from .. import info
from . import context
from . import transform
from . import spritesystem
from . import surface
from . import resource
# noinspection PyUnresolvedReferences
from . import pygame as ppg

logger = logging.getLogger(__name__)


__version__ = "1.0.0.0"
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID (C) 2011"

__copyright__ = "Copyright 2011, pyknic.pygame_wrapper"
__credits__ = ["DR0ID"]
__license__ = "New BSD license"
__maintainer__ = "DR0ID"


# log info first before importing anything
def log_info(log_separator_width=50):
    """
    Logs the pygame specific information to the logger (using INFO level).

    :param log_separator_width: The width the separating lines should have.
    """
    logger.info("*" * log_separator_width)
    _format_string = "%-" + str(log_separator_width) + "s"
    logger.info(_format_string % ("pyknic.pygame_wrapper v" + str(__version__) + " (c) DR0ID 2011"))
    logger.info("using pygame version: " + str(pygame.version.ver) + " (SDL: " + str(pygame.get_sdl_version()) + ")")


log_info()

__all__ = [
    "audio",
    "context",
    "resource",
    "spritesystem",
    "surface",
    "transform",
]



# check pygame version
info.check_version("pygame", (1, 9, 0), pygame.version.vernum, (1, 9, 4))

# check pyknic version
info.check_version("pyknic", (4, 0, 0, 0), info.version_info, (5, 0, 0, 0))

logger.debug("imported " + "-" * 50)
