# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
Context management.

Limitations:
    pop can be called from anywhere, also from within a update method of an
    active (topmost) context. After poping it the active context is not the
    topmost context anymore, but can still modify the context stack (since
    it is still in the update method).

.. todo::
    new API, maybe better or not???::

        # should top() be in __all__ ??
        __all__ = ["update", "draw", "init", "length"]

        def init(initial_context):
            _push(initial_context)

        def update(dt):
            action = _CONTEXT_STACK[-1].update(dt) # -> None  : do nothing
                                                  #    False : pop top context
                                                  #NewContext: push new context
            if action:
                _push(action)
            elif action is False:
                _pop()

        def draw(screen):
            if _CONTEXT_STACK:
                _CONTEXT_STACK[-1].draw(screen)

.. TODO::
    pass around a 'info' object (through push/pop)?? <- could make scene management easier
"""
from __future__ import print_function, division

import logging
logger = logging.getLogger(__name__)
logger.debug("importing...")


__all__ = ["Context"]


class Context(object):
    """
    The context class.
    """

    def enter(self):
        """Called when this context is pushed onto the stack."""
        pass

    def exit(self):
        """Called when this context is popped off the stack."""
        pass

    def suspend(self):
        """Called when another context is pushed on top of this one."""
        pass

    def resume(self):
        """Called when another context is popped off the top of this one."""
        pass

    def update(self, delta_time, sim_time):
        """Called once per frame"""
        pass

    def draw(self, screen, do_flip=True, interpolation_factor=1.0):
        """
        Refresh the screen

        :param screen: the screen surface
        :param do_flip: if set to true, the it should flip the screen, otherwise not. Default: True
        :param interpolation_factor: interpolation factor for drawing between sim updates, default: 1.0
        """
        pass

    def process_stack_push(self, context_stack):
        """
        Processes the stack push for instances of this class.
        :param context_stack: the stack to push the context on (append it to the list).
        """
        if context_stack:
            context_stack[-1].suspend()
            if __debug__:
                logger.debug("CONTEXT: suspended " + str(context_stack[-1].__class__.__name__))
        context_stack.append(self)
        if __debug__:
            logger.debug("CONTEXT: pushed " + str(self.__class__.__name__))
        self.enter()
        if __debug__:
            logger.debug("CONTEXT: entered " + str(self.__class__.__name__))

    def process_stack_pop(self, context_stack, do_resume):
        """
        Processes the stack push for instances of this class.
        """
        self.exit()
        if __debug__:
            logger.debug("CONTEXT: exited " + str(self.__class__.__name__))
            logger.debug("CONTEXT: resume=" + str(do_resume))
        if context_stack and do_resume:
            # log first, because resume might pop a context from stack
            if __debug__:
                logger.debug("CONTEXT: resumed " + str(context_stack[-1].__class__.__name__))
            context_stack[-1].resume()

    # this method stub are here to fool a IDE, they are overwritten in context/__init__.py
    def push(self, cont, effect=None, update_old=False, update_new=False): pass  # see context.push(...)

    # this method stub are here to fool a IDE, they are overwritten in context/__init__.py
    def pop(self, count=1, do_resume=True, effect=None, update_old=False, update_new=False): pass  # see context.pop(..)

    # this method stub are here to fool a IDE, they are overwritten in context/__init__.py
    def top(self, idx=0): pass  # see context.top(...)

    # this method stub are here to fool a IDE, they are overwritten in context/__init__.py
    def exchange(self, cont, effect=None, update_old=False, update_new=False): pass  # see context.exchange(...)

    # this method stub are here to fool a IDE, they are overwritten in context/__init__.py
    def get_stack_length(self): pass  # see.context.length(...)

logger.debug("imported")
