# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek22-2016-09
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging
import pygame

logger = logging.getLogger(__name__)

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2016"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module


NO_MOD = pygame.KMOD_NONE
ANY_MOD = 0xFFFF


def _get_action_for_key(_actions, _event_key, _event_scancode, _event_mod, _extra):
    for key, _action in _actions.items():
        k, m = key
        # if (k == _event_key or k == _event_scancode) and (m & _event_mod or (m == pygame.KMOD_NONE and _event_mod == pygame.KMOD_NONE) or (m == ANY_MOD and _event_mod == 0)):
        if k == _event_key and (m & _event_mod or (m == pygame.KMOD_NONE and _event_mod == pygame.KMOD_NONE) or (m == ANY_MOD and _event_mod == 0)):
            return _action, _extra


# TODO: check event map for duplicates... may not be possible due its a dict?
# TODO: method that gives all keys from an action
# TODO: check scan code usage

def get_actions(event_map, events):
    """
    usage:

    for action, extra in  eventmapper.get_actions(event_map, pygame.event.get()):
        ...

    :param event_map: the event map: {event_type: {(a, b, c): action_x, (a, d, e): action_y}, ...}
    :param events: list of pygame events
    :return: list of actions
    """
    result = []
    for event in events:
        actions = event_map.get(event.type, None)
        if actions is None:
            if event.type != pygame.MOUSEMOTION:
                logger.debug("no actions found for event type: {0}", event)
            continue
        key = None
        extra = None

        # MOUSEMOTION      pos, rel, buttons
        if event.type == pygame.MOUSEMOTION:
            key = None
            extra = (event.pos, event.rel, event.buttons)
        # MOUSEBUTTONUP    pos, button
        elif event.type == pygame.MOUSEBUTTONDOWN:
            key = event.button
            extra = (event.pos, )
        # MOUSEBUTTONDOWN  pos, button
        elif event.type == pygame.MOUSEBUTTONUP:
            key = event.button
            extra = (event.pos, )
        # JOYAXISMOTION    joy, axis, value
        # JOYBALLMOTION    joy, ball, rel
        # JOYHATMOTION     joy, hat, value
        # JOYBUTTONUP      joy, button
        # JOYBUTTONDOWN    joy, button

        # QUIT             none
        elif event.type == pygame.QUIT:
            key = None
        # ACTIVEEVENT      gain, state
        elif event.type == pygame.ACTIVEEVENT:
            key = (event.state, event.gain)
        # KEYDOWN          unicode, key, mod, scancode
        elif event.type == pygame.KEYDOWN:
            a = _get_action_for_key(actions, event.key, event.scancode, event.mod, event.unicode)
            if a is None:
                a = _get_action_for_key(actions, None, event.scancode, event.mod, event.unicode)
                if a is None:
                    logger.debug("key down: no mapping found for key: {0}", event)
                    continue
            result.append(a)
            continue

        # KEYUP            key, mod, scancode
        elif event.type == pygame.KEYUP:
            a = _get_action_for_key(actions, event.key, event.scancode, event.mod, None)
            if a is None:
                # TODO: last argument as event.key isn't good (binds to pygame and makes it a dependency!)
                a = _get_action_for_key(actions, None, event.scancode, event.mod, event.key)
                if a is None:
                    logger.debug("key up: no mapping found for key: {0}", event)
                    continue
            result.append(a)
            continue
        # VIDEORESIZE      size, w, h
        # VIDEOEXPOSE      none
        # USEREVENT        code

        action = actions.get(key, None)
        if action is None:
            logger.debug("no mapping found for key: {0} of event {1}", key, event)
        else:
            result.append((action, extra))

    return result


if __name__ == '__main__':

    ACTION_QUIT = 1
    ACTION_02 = 2
    ACTION_03 = 3
    ACTION_04 = 4
    ACTION_05 = 5
    ACTION_06 = 6
    ACTION_07 = 7
    ACTION_08 = 8
    ACTION_09 = 9
    ACTION_10 = 10
    ACTION_11 = 11

    event_map = {
        pygame.QUIT: {None: ACTION_QUIT},
        pygame.KEYDOWN: {
            (pygame.K_ESCAPE, NO_MOD): ACTION_QUIT,
            (pygame.K_F4, pygame.KMOD_ALT): ACTION_QUIT,
            (pygame.K_s, NO_MOD): ACTION_02,
            (None, pygame.KMOD_SHIFT): ACTION_03,
            (None, ANY_MOD): ACTION_04,  # map any key down with any modifiers to action 04
        },
        pygame.KEYUP: {
            (pygame.K_d, NO_MOD): 99,
        },
        pygame.MOUSEMOTION: {None: ACTION_05},
        pygame.MOUSEBUTTONDOWN: {1: ACTION_06, 3: ACTION_07},
        pygame.MOUSEBUTTONUP: {1: ACTION_08, 3: ACTION_09},
        pygame.ACTIVEEVENT: {
            (1, 0): ACTION_10,  # mouse left win
            (1, 1): ACTION_11  # mouse entered win
        }
    }


    import pygame
    pygame.init()
    screen = pygame.display.set_mode((800, 600))

    is_key_up_correct = pygame.KEYUP == 3

    running = True
    while running:

        events = pygame.event.get()
        if events:
            print("events:", [_ev for _ev in events])
        for action, extra in get_actions(event_map, events):
            print("action:", action, extra)
            if action == ACTION_QUIT:
                running = False
