# -*- coding: utf-8 -*-
from __future__ import print_function, division, absolute_import

import logging
import time

import pygame

from .pyknic.pygame_wrapper.context.effects import FadeOutEffect
from .stats import Stats
from .eventmapper import get_actions
from . import pyknic

from . import settings

logger = logging.getLogger(__name__)


class ContextLoading(pyknic.context.Context):
    def __init__(self, stats, text="loading...", pos=None):
        self.stats = stats
        self._seen = False
        screen = pygame.display.get_surface()
        screen_rect = screen.get_rect()
        font = pygame.font.SysFont('courier', 40, True)
        # hello = font.render('loading...', True, pygame.Color('gold'))
        # hello_rect = hello.get_rect(center=screen_rect.center)
        # self.sprites = [(hello, hello_rect)]
        self.sprites = []
        other_rect = pygame.Rect(0, 0, 3, 1)
        if pos is None:
            pos = screen_rect.center
        other_rect.midbottom = pos
        other_rect.move_ip(0, -20)  # shift some space up
        if text is not None:
            for line in text.split('\n'):
                surf = font.render(line, True, pygame.Color('gold'))
                rect = surf.get_rect(midtop=(other_rect.midbottom))
                self.sprites.append((surf, rect))
                other_rect = rect
        self.time = time.time()
        self.rest = 0.0

    def enter(self):
        # TODO: remove this if we ever figure out the memory leak
        self.time = time.time()

    def exit(self):
        pass

    def suspend(self):
        pass

    def resume(self):
        pass

    def update(self, delta_time, sim_time):
        for action, extra in get_actions(settings.event_map, pygame.event.get()):
            logger.debug("taking action {0}, {1}", action, extra)
            # TODO: not sure if those actions are appropriate here
            if action == settings.ACTION_QUIT:
                self.pop(self.get_stack_length())  # quit application
            elif action == settings.ACTION_ESCAPE:
                # pause or just quit this context
                self.pop()

        if self._seen and time.time() >= self.time + self.rest:
            logger.debug('Loading screen ended')
            from .context_gameplay import ContextGamePlay
            cgp = ContextGamePlay(self.stats)
            cgp.load_map(self.stats.current_level)
            self.exchange(cgp)

    def draw(self, screen, do_flip=True, interpolation_factor=1.0):
        screen.fill((0, 0, 0))

        # screen.blit(self.hello, self.hello_rect)
        for surf, rect in self.sprites:
            screen.blit(surf, rect)

        if do_flip:
            pygame.display.flip()

        self._seen = True
