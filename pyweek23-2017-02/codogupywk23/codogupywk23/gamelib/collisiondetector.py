# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of pyweek22-2016-09
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |        |
                   |        +-|* x for x bugfixes
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import sys

from .pyknic.mathematics import Point2, Vec2

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2016"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module


collision_functions = {}  # {(type1, type2): func, ...}


def register_collision_func(kinds, func):
    assert kinds[0] < kinds[1], "kinds should be sorted!"
    collision_functions[kinds] = func


def _no_collision(kinds):

    def _no_col(*args):
        print("no col function registered for kinds: {0} and args: {1}".format(kinds, args))

    return _no_col


def get_col_func(kinds):
    assert kinds[0] < kinds[1], "kinds should be sorted!"
    return collision_functions.get(kinds, _no_collision(kinds))


def coarse_detection(bin_space, rect, kind_filter=None):
    entities = bin_space.get_from_rect(*rect)
    if kind_filter:
        return [_ent for _ent in entities if _ent.kind & kind_filter]
    return entities


def check_collisions_between(kind, kind_other, rect, bin_space):
    _all_entities = bin_space.get_from_rect(*rect)
    kind_entities = (_e for _e in _all_entities if _e.kind & kind)
    for k_ent in kind_entities:
        other_entities = bin_space.get_at(k_ent.position.x, k_ent.position.y)
        other_entities = (_e for _e in other_entities if _e.kind & kind_other)
        for o_ent in other_entities:
            o_kind = o_ent.kind
            k_kind = k_ent.kind
            if k_kind < o_kind:
                col_func = get_col_func((k_kind, o_kind))
                col_func(k_ent, o_ent)
            else:
                col_func = get_col_func((o_kind, k_kind))
                col_func(o_ent, k_ent)


def check_collisions_between_all(kind, kind_other, bin_space, all_entities):
    kind_entities = (_e for _e in all_entities if _e.kind & kind)
    other_entities = [_o for _o in all_entities if _o.kind & kind_other]
    o_kind = kind_other
    k_kind = kind
    if k_kind < o_kind:
        col_func = get_col_func((k_kind, o_kind))
        col_func(kind_entities, other_entities)
    else:
        col_func = get_col_func((o_kind, k_kind))
        col_func(other_entities, kind_entities)
    # for k_ent in kind_entities:
    #     # other_entities = bin_space.get_at(k_ent.position.x, k_ent.position.y)
    #     # other_entities = (_e for _e in other_entities if _e.kind & kind_other)
    #     for o_ent in other_entities:
    #         n += 1


def check_collisions_between_nearest(kind, kind_other, rect, bin_space):
    _all_entities = bin_space.get_from_rect(*rect)
    kind_entities = (_e for _e in _all_entities if _e.kind & kind)
    for k_ent in kind_entities:
        other_entities = bin_space.get_at(k_ent.position.x, k_ent.position.y)
        other_entities = (_e for _e in other_entities if _e.kind & kind_other)

        o_ent = None
        dist = sys.maxsize
        for other in other_entities:
            od = k_ent.position.get_distance_sq(other.position)
            if od < dist:
                dist = od
                o_ent = other

        if o_ent:
            o_kind = o_ent.kind
            k_kind = k_ent.kind
            if k_kind < o_kind:
                col_func = get_col_func((k_kind, o_kind))
                col_func(k_ent, o_ent)
            else:
                col_func = get_col_func((o_kind, k_kind))
                col_func(o_ent, k_ent)


def check_collisions_between_nearest_all(kind, kind_other, bin_space, all_entities):
    kind_entities = (_e for _e in all_entities if _e.kind & kind)
    other_entities = [_o for _o in all_entities if _o.kind & kind_other]
    o_kind = kind_other
    k_kind = kind
    if k_kind < o_kind:
        col_func = get_col_func((k_kind, o_kind))
        col_func(kind_entities, other_entities)
    else:
        col_func = get_col_func((o_kind, k_kind))
        col_func(other_entities, kind_entities)


def Xcheck_collisions_between_nearest_all(kind, kind_other, bin_space, all_entities):
    kind_entities = (_e for _e in all_entities if _e.kind & kind)
    other_entities = [_o for _o in all_entities if _o.kind & kind_other]
    for k_ent in kind_entities:
        # other_entities = bin_space.get_at(k_ent.position.x, k_ent.position.y)
        # other_entities = (_e for _e in other_entities if _e.kind & kind_other)
        # k_ent_position_get_distance_sq = k_ent.position.get_distance_sq
        kx = k_ent.position.x
        ky = k_ent.position.y
        o_ent = None
        dist = sys.maxsize
        for other in other_entities:
            dx = other.position.x - kx
            dy = other.position.y - ky
            # od = k_ent_position_get_distance_sq(other.position)
            od = dx * dx + dy * dy
            if od < dist:
                dist = od
                o_ent = other

        if o_ent:
            o_kind = o_ent.kind
            k_kind = k_ent.kind
            if k_kind < o_kind:
                col_func = get_col_func((k_kind, o_kind))
                col_func(k_ent, o_ent)
            else:
                col_func = get_col_func((o_kind, k_kind))
                col_func(o_ent, k_ent)


def sphere_vs_aabb(sphere_position, radius, c_min, c_max, aabb_position):
    assert c_min.x < c_max.x
    assert c_min.y < c_max.y
    assert radius > 0

    if sphere_position.x - radius > c_max.x or sphere_position.x + radius < c_min.x:
        # gap on x axis
        return None, None

    if sphere_position.y - radius > c_max.y or sphere_position.y + radius < c_min.y:
        # gap on y axis
        return None, None

    c3 = Point2(c_max.x, c_min.y)
    c4 = Point2(c_min.x, c_max.y)
    _da = sys.maxsize
    _axis = None

    if not ((c_max.y > sphere_position.y > c_min.y) or (c_max.x > sphere_position.x > c_min.x)):
        # consider the 3rd axis too
        # find the nearest corner
        if sphere_position.x > aabb_position.x:
            if sphere_position.y > aabb_position.y:
                _axis = sphere_position - c_max
            else:
                _axis = sphere_position - c3
        else:
            if sphere_position.y > aabb_position.y:
                _axis = sphere_position - c4
            else:
                _axis = sphere_position - c_min

        if _axis.length >= radius:
            # gap on this axis
            return None, None

        _da = radius - _axis.length

    if sphere_position.x < aabb_position.x:
        _dx = sphere_position.x + radius - c_min.x
        _xa = -Vec2.unit_x()
    else:
        _dx = c_max.x - (sphere_position.x - radius)
        _xa = Vec2.unit_x()

    if _dx < _da:
        _da = _dx
        _axis = _xa

    if sphere_position.y < aabb_position.y:
        _dy = sphere_position.y + radius - c_min.y
        _ya = -Vec2.unit_y()
    else:
        _dy = c_max.y - (sphere_position.y - radius)
        _ya = Vec2.unit_y()

    if _dy < _da:
        _da = _dy
        _axis = _ya

    _axis.length = _da

    return _axis, _da
