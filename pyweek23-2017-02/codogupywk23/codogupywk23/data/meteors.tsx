<?xml version="1.0" encoding="UTF-8"?>
<tileset name="meteors" tilewidth="120" tileheight="98" tilecount="25" columns="5">
 <image source="graphics/meteors.png" width="601" height="491"/>
 <tile id="0">
  <properties>
   <property name="kind" value="meteor"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="kind" value="meteor"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="kind" value="meteor"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="kind" value="meteor"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="kind" value="meteor"/>
  </properties>
 </tile>
 <tile id="6">
  <properties>
   <property name="kind" value="meteor"/>
  </properties>
 </tile>
 <tile id="7">
  <properties>
   <property name="kind" value="meteor"/>
  </properties>
 </tile>
 <tile id="8">
  <properties>
   <property name="kind" value="meteor"/>
  </properties>
 </tile>
 <tile id="10">
  <properties>
   <property name="kind" value="meteor"/>
  </properties>
 </tile>
 <tile id="11">
  <properties>
   <property name="kind" value="meteor"/>
  </properties>
 </tile>
 <tile id="12">
  <properties>
   <property name="kind" value="meteor"/>
  </properties>
 </tile>
 <tile id="13">
  <properties>
   <property name="kind" value="meteor"/>
  </properties>
 </tile>
 <tile id="15">
  <properties>
   <property name="kind" value="meteor"/>
  </properties>
 </tile>
 <tile id="16">
  <properties>
   <property name="kind" value="meteor"/>
  </properties>
 </tile>
 <tile id="17">
  <properties>
   <property name="kind" value="meteor"/>
  </properties>
 </tile>
 <tile id="18">
  <properties>
   <property name="kind" value="meteor"/>
  </properties>
 </tile>
</tileset>
