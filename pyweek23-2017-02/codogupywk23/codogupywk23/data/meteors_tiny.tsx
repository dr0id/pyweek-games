<?xml version="1.0" encoding="UTF-8"?>
<tileset name="meteors_tiny" tilewidth="18" tileheight="18" tilecount="2" columns="1">
 <image source="graphics/meteors_tiny.png" width="18" height="36"/>
 <tile id="0">
  <properties>
   <property name="kind" value="wall"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="kind" value="wall"/>
  </properties>
 </tile>
</tileset>
