<?xml version="1.0" encoding="UTF-8"?>
<tileset name="space1_0_4x" tilewidth="4096" tileheight="4096" tilecount="1" columns="0">
 <tile id="0">
  <properties>
   <property name="kind" value="background"/>
  </properties>
  <image width="4096" height="4096" source="space1_0_4x_smooth.png"/>
 </tile>
</tileset>
