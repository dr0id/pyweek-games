<?xml version="1.0" encoding="UTF-8"?>
<tileset name="basic" tilewidth="64" tileheight="64" tilecount="400" columns="20">
 <image source="basic64x64.png" width="1280" height="1280"/>
 <tile id="0">
  <properties>
   <property name="kind" value="ship"/>
  </properties>
 </tile>
 <tile id="20">
  <properties>
   <property name="kind" value="destructible"/>
  </properties>
 </tile>
 <tile id="21">
  <properties>
   <property name="health" type="int" value="2"/>
   <property name="kind" value="destructible"/>
  </properties>
 </tile>
 <tile id="22">
  <properties>
   <property name="health" type="int" value="3"/>
   <property name="kind" value="destructible"/>
  </properties>
 </tile>
 <tile id="23">
  <properties>
   <property name="health" type="int" value="4"/>
   <property name="kind" value="destructible"/>
  </properties>
 </tile>
 <tile id="24">
  <properties>
   <property name="health" type="int" value="6"/>
   <property name="kind" value="destructible"/>
  </properties>
 </tile>
 <tile id="40">
  <properties>
   <property name="kind" value="wall"/>
  </properties>
 </tile>
 <tile id="60">
  <properties>
   <property name="kind" value="gun"/>
  </properties>
 </tile>
 <tile id="80">
  <properties>
   <property name="kind" value="mine"/>
  </properties>
 </tile>
 <tile id="100">
  <properties>
   <property name="kind" value="smartbomb"/>
  </properties>
 </tile>
 <tile id="120">
  <properties>
   <property name="guns" type="int" value="1"/>
   <property name="health" value="2"/>
   <property name="kind" value="enemy"/>
  </properties>
 </tile>
 <tile id="121">
  <properties>
   <property name="guns" type="int" value="2"/>
   <property name="health" value="2"/>
   <property name="kind" value="enemy"/>
  </properties>
 </tile>
 <tile id="122">
  <properties>
   <property name="guns" type="int" value="2"/>
   <property name="health" value="3"/>
   <property name="kind" value="enemy"/>
  </properties>
 </tile>
 <tile id="123">
  <properties>
   <property name="guns" type="int" value="3"/>
   <property name="health" value="5"/>
   <property name="kind" value="enemy"/>
  </properties>
 </tile>
 <tile id="124">
  <properties>
   <property name="guns" type="int" value="3"/>
   <property name="health" value="8"/>
   <property name="kind" value="enemy"/>
  </properties>
 </tile>
 <tile id="140">
  <properties>
   <property name="kind" value="gate"/>
  </properties>
 </tile>
 <tile id="160">
  <properties>
   <property name="text" value="bla"/>
  </properties>
 </tile>
 <tile id="220">
  <properties>
   <property name="kind" value="debries"/>
  </properties>
 </tile>
</tileset>
