﻿The Lesser of Two Evils
=======================

CONTACT:

Homepage: https://pyweek.org/e/codogupywk23/
Name: The Lesser of Two Evils
Team: codogupywk23
Members: DR0ID, Axiom, Gummbum


DEPENDENCIES:

You might need to install some of these before running the game:

  Python:     http://www.python.org/
  PyGame:     http://www.pygame.org/


RUNNING THE GAME:

On Windows or Mac OS X, locate the "run.pyw" file and double-click it.

Othewise open a terminal / console and "cd" to the game directory and run:

  python run.py



HOW TO PLAY THE GAME:

Use WASD to steer the ship. Avoid meteors, walls, bullets, and baddies.

Use SPACE to shoot.

Fly over debris to pick it up and fortify your shields or guns.

When shields are depleted by enemies or blundering into a wall, your
guns will being to break off. If you lose all your guns, you'll have to
restart the level.

The boss level is extremely hard. You will need to use all your
accumulated skills to survive and vanquish the Evil.


CHEATING:

You may safely tweak any of the settings in gamelib/settings.py which are
in the User Tuning section. Note that changing the screen size is not
recommended as the game does not scale to fit the window.

You may want to allow cheat keys:
1. Set settings.ALlOW_CHEATS=True.
2. See the settings.event_map dict for action-key-mapping.
3. F1 is particularly helpful for seeing the wall boundaries.
4. F10 which "wins" the current level (so you can wimp out on the extreme
   level 5 and still see the ending :).


LICENSE:

This game skellington has same license as pyknic.
