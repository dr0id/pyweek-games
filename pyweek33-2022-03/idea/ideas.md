Idea 1:
-------
Goal: 

catch as manc objects as you can or fix them on the ground.

Gameplay: 

Player has to catch falling objects. If an object is to heavy, then the player gets knocked
out for short time. For each catched object points are added to the highscore. If an object 
makes it ot the ground it will lay there broken for some time before it disappears. During 
this timeperiod the player has a chance to fix it and collect more points.

Objects:

  - Player
  - Object
  - Highscore
  - Ground
  - Windows
  - evil twin
  - ...?

Collision:

 - player, falling object
 - player, broken object
 - falling object, ground
 - ...?

Controls:

 - left, right arrows, space (to fix)

Win/loose conditions: not sure about them,


Idea 2:
-------

![image](idea2.png)

Goal: 

send as many objects back to where they belong

Gameplay:

Evil twin throws objects out of the windows. They fall down to the ground. The player can now 
move to the object and using a drag'n'drop movement aim at the window. If the windows is not 
hit then it will fall back on the ground. If the correct window (the source where if was thrown
out) then points are added to the highscore. If two object collide in mid-air they will be 
deflected from their path. If the player is hit by an object the player is affected: small 
overlap -> push back, partial hit -> flat, half speed, full hit -> knocked out for some time.

Objects:

 - player
 - evil twin
 - object
 - windows
 - ground
 - highscore
 - house
 - timer

Collisions:

 - object, object
 - object, player
 - object, ground
 - object, window
 - mouse, object

Events: 

 - Mousemotion -> move player
 - timer -> twin generate object
 - timer 1' -> game ends
 - drag'n'drop on object -> shoot object

Controls: 

  - mouse

Win/loose conditions:

 - win: >8 objects back to right place
 - loose. <8 object back
