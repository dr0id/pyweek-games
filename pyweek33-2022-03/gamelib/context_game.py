# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'context_game.py' is part of pw-33
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2022"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["GameContext"]  # list of public visible parts of this module

import math

import random

import pygame.event
from pygame.math import Vector2 as Vec

from gamelib.shaker import Shaker
from pyknic import tweening
from pyknic.tweening import Tweener

from gamelib.sprites import ArrowSprite, AnimatedSprite, PygameTextSprite
from pyknic.pyknic_pygame.resource2 import resource_manager, SpriteSheetResource

from pyknic.pyknic_pygame.spritesystem import DefaultRenderer, Camera, Sprite

import pyknic.timing
from gamelib import levels, settings, sfx_handler, resources, game_message, spritefx
from gamelib.eventdispatcher import event_dispatcher
from gamelib.collider import Collider
from gamelib.entities import Player, CountdownTimer, Ground, House, Twin, HighScore, Window, Item, Mouse, Pointer, \
    z_gras1, z_gras2, z_aura, z_window_bg
from pyknic.pyknic_pygame.context import TimeSteppedContext

logger = logging.getLogger(__name__)
logger.debug("importing...")


class GameContext(TimeSteppedContext):

    def __init__(self, max_dt, step_dt, draw_fps, music_player):
        TimeSteppedContext.__init__(self, max_dt, step_dt, draw_fps)
        self.music_player = music_player
        self.max_dt = max_dt
        self.step_dt = step_dt
        self.draw_fps = draw_fps
        self.title = "Ooops- missing title in level!"
        self.appearances = []
        self.win_condition = 0
        self.scheduler = pyknic.timing.Scheduler()
        self.scheduler_slow_motion = pyknic.timing.Scheduler()
        self.player = Player(pyknic.timing.Timer(self.scheduler))
        self.countdown_timer = CountdownTimer()
        self.ground = Ground()
        self.house = House()
        self.twin = Twin()
        self.mouse = Mouse()
        self.windows = []
        self.items = []
        self.high_score = HighScore()
        self.collider = Collider()
        self.collider.register(Item.kind, Item.kind, self._midair_item_item_coll)
        self.collider.register(Player.kind, Item.kind, self._item_player_coll)
        self.collider.register(Item.kind, Ground.kind, self._item_ground_coll)
        self.collider.register(Window.kind, Item.kind, self._window_item_coll)
        self.collider.register(Twin.kind, Item.kind, self._twin_item_coll)
        # self.collider.register(Item.kind, Mouse.kind, self._item_mouse_coll)
        # music_player.fill_music_carousel([resources.resource_def[resources.resource_music_track_1]])
        # music_player.start_music_carousel()
        self.sfx_handler = sfx_handler.SfxHandler(self.music_player)
        self.sfx_handler.register_events(event_dispatcher)
        self.entities = []  # , self.mouse]
        # entity-to-sprite map
        self.e2s = {}
        # sprite-to-entity map
        self.s2e = {}
        self.level_timer = pyknic.timing.Timer(self.scheduler_slow_motion)
        self.end_timer = pyknic.timing.Timer(self.scheduler)
        self.level_timer.interval = 1.0  # seconds?
        self.level_timer.repeat = True
        self.level_timer.event_elapsed += self._update_level_timer
        self.twin_timer = pyknic.timing.Timer(self.scheduler_slow_motion)
        self.twin_timer.event_elapsed += self._hide_twin
        self.renderer = DefaultRenderer()
        self.cam = Camera(pygame.Rect(0, 0, settings.screen_width, settings.screen_height))
        self.cam_position = self.cam.position.clone()
        self.pointer = Pointer()
        self._sim_time = 0
        self._ups = pygame.time.Clock()
        self._fps = pygame.time.Clock()
        self.scheduler.schedule(self._print_updates, 1.0)
        self._mouse_rel_cache = [0] * 90
        self.slow_motion = False
        self.slow_motion_timer = pyknic.timing.Timer(self.scheduler)
        self.slow_motion_timer.event_elapsed += self._end_slow_motion
        self.slow_motion_timer.repeat = False
        self.slow_motion_timer.interval = settings.SLOW_MOTION_DURATION
        self.tweener = Tweener()
        self.is_level_running = True

        self.game_message_index = 0
        self.game_message = None
        self.game_message_interlude = settings.MESSAGE_INIT_INTERLUDE
        self.shaker = Shaker()
        self.leaving = False

    def _print_updates(self, *args, **kwargs):
        logger.info("fps: %s  ups: %s", self._fps.get_fps(), self._ups.get_fps())
        return 1.0

    def _update_level_timer(self, *args, **kwargs):
        if self.twin.state == Twin.STUNNED:
            return
        self.countdown_timer.time_left_in_sec -= 1
        logger.info("level time left: %s", self.countdown_timer.time_left_in_sec)
        self._check_end_condition()
        if self.countdown_timer.time_left_in_sec <= 0:
            self.level_timer.stop()

        if self.countdown_timer.time_left_in_sec % 10 == 0 or self.countdown_timer.time_left_in_sec < 10:
            spr = self.e2s[self.countdown_timer]
            self.tweener.create_tween_by_end(spr, "zoom", 1.1, 1.0, 0.3, tweening.ease_in_sine2)
            # todo add some peeping sound here
        if self.countdown_timer.time_left_in_sec == 10:
            event_dispatcher.fire(
                settings.EVT_SFX_CLOCK_ALARM, resources.resource_def[resources.resource_sfx_clock_alarm])
        elif self.countdown_timer.time_left_in_sec < 10 or self.countdown_timer.time_left_in_sec % 10 == 0:
            event_dispatcher.fire(
                settings.EVT_SFX_CLOCK_TICK, resources.resource_def[resources.resource_sfx_clock_tick])

    def change_level(self, *args, **kwargs):
        if self.leaving:
            return
        self.leaving = True
        if settings.current_level_idx < len(levels.levels):
            gc = GameContext(self.max_dt, self.step_dt, self.draw_fps, self.music_player)
            self.exchange(gc, effect=pyknic.pyknic_pygame.context.effects.Slide(0.5, settings.screen_height, (0, -1),
                                                                                move_old=True, move_new=False))
        else:
            self.pop(effect=pyknic.pyknic_pygame.context.effects.FadeOutEffect(0.5))

    def _check_end_condition(self):
        if self.is_level_running:
            count = len([i for i in self.items if i.state == Item.RETURNED])
            if self.countdown_timer.time_left_in_sec <= 0 or count >= self.win_condition:

                msg = ""
                if self.win_condition > count:
                    # loose!
                    logger.warning(">>>>> LOOSE <<<<<")
                    msg = settings.LOOSE_LEVEL_MESSAGE
                else:
                    logger.warning(">>>> WIN!!! <<<<<")
                    settings.current_level_idx += 1
                    msg = settings.WIN_LEVEL_MESSAGE
                    if settings.current_level_idx >= len(levels.levels):
                        # no more levels
                        msg = settings.GAME_END_MESSAGE

                self.end_timer.event_elapsed += self.change_level
                self.end_timer.start(5)
                msg_spr = PygameTextSprite(msg, Vec(settings.screen_width / 2, settings.screen_height / 2),
                                           settings.font_themes['title'])

                self.renderer.add_sprite(msg_spr)
                self.tweener.create_tween_by_end(msg_spr, 'alpha', 255, 0, 5, tweening.ease_in_sine)
                self.tweener.create_tween_by_end(msg_spr, 'zoom', 1.0, 2.0, 5, tweening.ease_in_sine)
                self.is_level_running = False

    def _twin_item_coll(self, twins, items):
        for twin in twins:
            if twin.state == Twin.NORMAL and twin.visible:
                items = list(items)
                idx = twin.rect.collidelist(items)
                if idx < 0:
                    return
                item = items[idx]
                if item.state == Item.THROWN:
                    event_dispatcher.fire(
                        settings.EVT_SFX_HIT_TWIN, resources.resource_def[resources.resource_sfx_hit_twin])
                    twin.state = Twin.STUNNED
                    self.slow_motion = True
                    self.slow_motion_timer.start()

    def _end_slow_motion(self, *args, **kwargs):
        self.twin.state = Twin.NORMAL
        # self.twin.visible = False # hide twin after stun
        self.slow_motion = False

    def _window_item_coll(self, windows, items):
        # check if catapulted item collides with destination window
        for item in items:
            if item.state == Item.THROWN:
                item.rect.centerx = item.position.x
                item.rect.centery = item.position.y
                windows = list(windows)
                idx = item.rect.collidelist(windows)
                if idx < 0:
                    continue
                w = windows[idx]
                if item.window == w:
                    # self.items.remove(item) # keep it in here because win/loose checks this list
                    item.state = Item.RETURNED
                    self.renderer.remove_sprite(self.e2s[item])
                    self.entities.remove(item)
                    self.high_score.add(1)
                    event_dispatcher.fire(
                        settings.EVT_SFX_ITEM_HIT_WINDOW,
                        resources.resource_def[resources.resource_sfx_item_returned])
                    self.tweener.create_tween_by_end(self.e2s[self.high_score], 'zoom', 1.1, 1.0, 0.3,
                                                     tweening.ease_in_sine)

    def _item_ground_coll(self, items, grounds):
        for ground in grounds:
            for item in items:
                if item.position.y >= ground.position.y:
                    if item.state in (Item.FALLING, Item.THROWN):
                        if item.resource in (resources.resource_item_cat, resources.resource_item_dog,
                                             resources.resource_item_budgie):
                            event_dispatcher.fire(
                                settings.EVT_SFX_SQUISH, resources.resource_def[resources.resource_sfx_squish])
                        else:
                            event_dispatcher.fire(
                                settings.EVT_SFX_CRASH, resources.resource_def[resources.resource_sfx_crash])
                        item.state = Item.GROUNDED
                        item.position.y = ground.position.y
                        item.vel.update(0, 0)  # reset velocity to 0
                        item.angular_vel = 0
                        if item.mass >= 2500:
                            self.shaker.add_trauma(item.mass / 10000)

    def _item_player_coll(self, players, items):
        # check if falling item hits the player
        for player in players:
            for item in items:
                if item.state == Item.FALLING or item.state == Item.THROWN:
                    if item.vel.y > 0 and item.rect.colliderect(player.rect):
                        event_dispatcher.fire(
                            settings.EVT_SFX_HIT_PLAYER, resources.resource_def[resources.resource_sfx_hit_player])
                        player.hit(item, self.mouse)

    def _midair_item_item_coll(self, items, others):
        # check if items are in air, check collision
        # if collide: deflect and rotate
        # attention: naive implementation is n^2
        items = list(items)
        hit_occurred = False
        for idx, item in enumerate(items):
            if item.state == Item.THROWN or item.state == Item.FALLING:
                for other in items[idx + 1:]:
                    if other.state == Item.THROWN or other.state == Item.FALLING:
                        if item.rect.colliderect(other.rect):
                            hit_occurred = True
                            # collision! deflect and rotate
                            item.angular_vel = 250
                            other.angular_vel = -300
                            normal = item.vel + other.vel
                            normal.normalize_ip()
                            normal.rotate_ip(90)
                            if normal.dot(item.vel) > 0:
                                normal *= -1
                            item.vel.reflect_ip(normal)
                            other.vel.reflect_ip(-normal)
        if hit_occurred:
            event_dispatcher.fire(
                settings.EVT_SFX_HIT_ITEMS_MIDAIR, resources.resource_def[resources.resource_sfx_hit_items_midair])

    def enter(self):
        if resource_manager.resources:
            self.resume()

    def exit(self):
        Sprite._dirty_sprites.clear()  # hack, make sure no sprite is in that list when a new level is loaded
        spritefx.tweener.clear()
        spritefx.text_sprites.clear()

    def resume(self):
        # load current level
        if settings.current_level_idx >= len(levels.levels):
            self.pop()
            return

        spritefx.tweener.clear()
        spritefx.text_sprites.clear()

        Sprite._dirty_sprites.clear()  # hack, make sure no sprite is in that list when a new level is loaded
        level = levels.levels[settings.current_level_idx]
        self.title = level.title
        self.win_condition = level.win_condition
        self.ground.set_y(level.ground_y)
        self.countdown_timer.time_left_in_sec = level.time_in_sec
        settings.ITEM_RANDOM_DIRECTION = level.ITEM_RANDOM_DIRECTION

        self.windows = [w.clone() for w in level.windows]
        self.appearances = [i.clone(level.time_in_sec) for i in level.appearances]
        self.appearances.sort(key=lambda a: -a.when_in_s)

        self.entities = []
        self.entities += self.windows

        # windows
        window_res = resource_manager.get_resource(resources.resource_window)
        quarter_open, quarter_open_bg, braun_open, braun_closed, open_bg, closed_bg, braun_wide_open, _, wide_open_bg = window_res.data
        types = [(open_bg, braun_open), (wide_open_bg, braun_wide_open), (quarter_open_bg, quarter_open)]
        for idx, w in enumerate(self.windows):
            bg, frame = (closed_bg, braun_closed)
            if w.is_open:
                bg, frame = random.choice(types)
            spr_bg = Sprite(bg, w.position, w.anchor, z_window_bg, name=f"win bg {idx}: {w.position}")
            self.renderer.add_sprite(spr_bg)
            spr_w = Sprite(frame, w.position, w.anchor, w.z_layer, name=f"win {idx}: {w.position}")
            self.renderer.add_sprite(spr_w)

        # house
        self._add_entity_sprite(self.house)

        # ground
        self.entities.append(self.ground)
        position = self.ground.position.copy()

        position.y -= 5
        spr = Sprite(resource_manager.get_resource(resources.resource_ground).image, position, self.ground.anchor,
                     self.ground.z_layer)
        self.renderer.add_sprite(spr)

        position.y -= 5
        spr = Sprite(resource_manager.get_resource(resources.resource_gras1).image, position, self.ground.anchor,
                     z_gras1)
        self.renderer.add_sprite(spr)

        position.y -= 5
        spr = Sprite(resource_manager.get_resource(resources.resource_gras2).image, position, self.ground.anchor,
                     z_gras2)
        self.renderer.add_sprite(spr)

        # highscore
        # self._add_entity_sprite(self.high_score)
        self.high_score.total = self.win_condition
        spr = PygameTextSprite(self.high_score.text,
                               self.high_score.position.copy(),
                               settings.font_themes['title'],
                               anchor=self.high_score.anchor,
                               z_layer=self.high_score.z_layer,
                               )
        self.e2s[self.high_score] = spr
        self.renderer.add_sprite(spr)

        # count down timer
        spr = PygameTextSprite(self.countdown_timer.time_left_in_sec,
                               self.countdown_timer.position.copy(),
                               settings.font_themes['title'],
                               anchor=self.countdown_timer.anchor,
                               z_layer=self.countdown_timer.z_layer,
                               )
        self.e2s[self.countdown_timer] = spr
        self.renderer.add_sprite(spr)

        # hand
        self._add_entity_sprite(self.mouse)
        self.e2s[self.mouse].alpha = 220
        self.e2s[resources.resource_hand_closed] = Sprite(
            resource_manager.get_resource(resources.resource_hand_closed).image, self.mouse.position,
            self.mouse.anchor, self.mouse.z_layer)
        self.e2s[resources.resource_hand_closed].visible = False

        arrow_sprite = ArrowSprite(self.pointer.position, resource_manager.get_resource(self.pointer.resource).image,
                                   self.pointer.z_layer)
        self.renderer.add_sprite(arrow_sprite)
        self.e2s[self.pointer] = arrow_sprite
        self.e2s[self.pointer].visible = False

        # player
        stun_anim: SpriteSheetResource = resource_manager.get_resource(resources.resource_stunned)
        anim = AnimatedSprite(stun_anim.data, stun_anim.resource_config.fps, self.scheduler, self.player.position,
                              self.player.z_layer + 1)
        anim.visible = False
        anim.start()
        self.renderer.add_sprite(anim)
        self.e2s['stun'] = anim

        leprechaun_sprite_sheet: SpriteSheetResource = resource_manager.get_resource(resources.resource_leprechaun)
        player_anim = AnimatedSprite(leprechaun_sprite_sheet.data, leprechaun_sprite_sheet.resource_config.fps,
                                     self.scheduler, self.player.position,
                                     self.player.z_layer + 1)
        player_anim.visible = False
        player_anim.start()
        self.renderer.add_sprite(player_anim)
        self.e2s[self.player] = player_anim
        self.entities.append(self.player)

        aura_anim = resource_manager.get_resource(resources.resource_aura)
        aura_spr = AnimatedSprite(list(reversed(aura_anim.data)), aura_anim.resource_config.fps,
                                  self.scheduler, self.player.position,
                                  z_aura)
        aura_spr.start()
        aura_spr.visible = True
        self.renderer.add_sprite(aura_spr)

        # twin
        # self._add_entity_sprite(self.twin, False)
        twin_sprite_sheet = resource_manager.get_resource(resources.resource_twin)
        twin_anim = AnimatedSprite(twin_sprite_sheet.data, twin_sprite_sheet.resource_config.fps, self.scheduler,
                                   self.twin.position,
                                   self.twin.z_layer)
        twin_anim.visible = False
        twin_anim.start()
        self.renderer.add_sprite(twin_anim)
        self.e2s[self.twin] = twin_anim
        self.entities.append(self.twin)

        stun_anim: SpriteSheetResource = resource_manager.get_resource(resources.resource_stunned_evil)
        anim = AnimatedSprite(stun_anim.data, stun_anim.resource_config.fps, self.scheduler, self.twin.position,
                              self.twin.z_layer + 1)
        anim.visible = False
        anim.start()
        self.renderer.add_sprite(anim)
        self.e2s['twin_stunned'] = anim

        aura_anim = resource_manager.get_resource(resources.resource_aura_evil)
        aura_spr = AnimatedSprite(list(reversed(aura_anim.data)), aura_anim.resource_config.fps,
                                  self.scheduler, self.twin.position,
                                  self.twin.z_layer - 1)
        aura_spr.start()
        aura_spr.visible = False
        self.e2s['evil_aura'] = aura_spr
        self.renderer.add_sprite(aura_spr)

        # level
        self.level_timer.start()
        pygame.mouse.set_visible(settings.MOUSE_VISIBLE)

        self._new_level()

    def _new_level(self):
        # todo: move level init / reinit code here
        def new_level_sfx(source_timer):
            event_dispatcher.fire(settings.EVT_SFX_NEW_LEVEL, resources.resource_def[resources.resource_sfx_new_level])

        # todo: Gumm is working on this sfx
        timer = pyknic.timing.Timer(self.scheduler, 0.5)
        timer.event_elapsed += new_level_sfx
        timer.start()

        self.game_message = None
        self.game_message_interlude = settings.MESSAGE_INIT_INTERLUDE
        self.game_message_index = 0

        title_spr = PygameTextSprite(self.title, Vec(settings.screen_width / 2, settings.screen_height / 3),
                                     settings.font_themes['title'])

        def remove_title(*args, **kwargs):
            self.renderer.remove_sprite(title_spr)

        self.renderer.add_sprite(title_spr)
        self.tweener.create_tween_by_end(title_spr, 'alpha', 255, 0, 5, tweening.ease_in_sine)
        self.tweener.create_tween_by_end(title_spr, 'zoom', 1.0, 0.8, 5, tweening.ease_in_sine, cb_end=remove_title)

        spritefx.tweener.clear()
        spritefx.text_sprites.clear()

    def _add_entity_sprite(self, ent, visible=True):
        img = resource_manager.get_resource(ent.resource).image
        spr = Sprite(img, ent.position, anchor=ent.anchor, z_layer=ent.z_layer)
        spr.visible = visible
        self.renderer.add_sprite(spr)
        self.e2s[ent] = spr
        self.s2e[spr] = ent

    def update_step(self, delta, sim_time, *args):
        self.tweener.update(delta)
        self._ups.tick()
        self.scheduler.update(delta, sim_time)  # this drives the unaffected things
        if self.slow_motion:
            delta *= settings.SLOW_MOTION_FACTOR
        self._sim_time += delta
        self.scheduler_slow_motion.update(delta, self._sim_time)  # this drives the affected things in slow motion
        self._update_twin()
        self._update_items(delta)
        self._update_entities()

        spritefx.update_tweens(delta)
        self._update_message(delta)

        self._handle_events()
        self.collider.check_all_collisions(self.entities)

    def _handle_events(self):
        mouse_rel_x = 0
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.pop()
            elif event.type == pygame.MOUSEMOTION:
                moved = self.player.move_to(event.pos[0], self.mouse.position.x)
                mouse_rel_x += event.rel[0]
                if moved:
                    self.mouse.update(event.pos)
                self.pointer.update(event.pos)
            elif event.type == pygame.MOUSEBUTTONDOWN:
                hits = self.renderer.get_sprites_at_tuple(event.pos)
                if settings.LIMIT_HAND_FREEDOM:
                    hits = self.renderer.get_sprites_at_tuple((event.pos[0], settings.GROUND_Y))
                for spr in hits:
                    item = self.s2e.get(spr, None)
                    if isinstance(item, Item):
                        if self.player.grab(item):
                            self.pointer.position.update(item.position)
                            break
            elif event.type == pygame.MOUSEBUTTONUP:
                self.player.throw(event.pos)
                pygame.mouse.set_pos(self.mouse.position)
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_r:
                    logger.warning("Resetting level by key!")
                    self.change_level()
                elif event.mod & pygame.KMOD_CTRL and event.key in (
                        pygame.K_0, pygame.K_1, pygame.K_2, pygame.K_3, pygame.K_4, pygame.K_5, pygame.K_6, pygame.K_7,
                        pygame.K_8, pygame.K_9):
                    settings.current_level_idx = event.key - pygame.K_0
                    logger.error("Setting next level to %s", settings.current_level_idx)

        self._mouse_rel_cache.append(math.fabs(mouse_rel_x))
        self._mouse_rel_cache.pop(0)

    def draw_step(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0, screen=None):
        dt = self._fps.tick()
        dt /= 1000.0  # convert to seconds

        # cam shake
        self.cam.position.x = self.cam_position.x
        self.cam.position.y = self.cam_position.y
        offset = self.shaker.update(dt)
        self.cam.position.x += offset.x
        self.cam.position.y += offset.y

        # twin
        twin_stunned = self.twin.state == Twin.STUNNED
        self.e2s[self.twin].visible = not twin_stunned and self.twin.visible
        self.e2s['twin_stunned'].visible = twin_stunned and self.twin.visible
        self.e2s['evil_aura'].visible = self.twin.visible

        # player/pointer/hand
        is_stunned = self.player.state == Player.STUNNED
        self.e2s['stun'].visible = is_stunned
        self.e2s[self.player].visible = not is_stunned

        self.e2s[self.player].flipped_x = self.player.face_left
        self.e2s[self.mouse].flipped_x = self.player.face_left
        if self.player.grabbed_item:
            self.e2s[self.pointer].visible = True
            self.pointer.position.update(self.player.grabbed_item.position)
            self.e2s[self.pointer].update_vector(self.pointer.pointer, self.pointer.scale)
            self.e2s[resources.resource_hand_closed].visible = True
            self.e2s[self.mouse].visible = False
        else:
            self.e2s[self.pointer].visible = False
            self.e2s[resources.resource_hand_closed].visible = False
            self.e2s[self.mouse].visible = True
            fps = sum(self._mouse_rel_cache) / len(self._mouse_rel_cache) * settings.PLAYER_TO_FPS_FACTOR
            if 0 < fps < settings.PLAYER_MIN_FPS:
                fps = settings.PLAYER_MIN_FPS
            self.e2s[self.player].fps = int((fps if fps < settings.PLAYER_MAX_FPS else settings.PLAYER_MAX_FPS) + 0.5)
        # logger.debug("player fps: %s   %s   %s", self.e2s[self.player].fps, self.e2s[self.player].is_running,
        #              self._mouse_rel_cache)

        # countdown
        self.e2s[self.countdown_timer].text = f"{self.countdown_timer.time_left_in_sec:^.0f}"

        # high score
        self.e2s[self.high_score].text = self.high_score.text

        # draw all the stuff
        screen = screen or pygame.display.get_surface()
        background_color = (193, 231, 255)
        self.renderer.draw(screen, self.cam, background_color, False)

        for spr in spritefx.text_sprites:
            screen.blit(spr.image, spr.rect)

        if settings.draw_debug:
            self._draw_debug(screen)

        if do_flip:
            pygame.display.flip()

    def _draw_debug(self, screen):
        if self.player.grabbed_item:
            mx, my = pygame.mouse.get_pos()
            pygame.draw.line(screen, 'red', self.player.grabbed_item.position, (mx, my))
        for ent in self.entities:
            pygame.draw.rect(screen, (0, 255, 200), ent.rect, 1)

        font = pygame.font.Font(None, 20)
        for spr in self.renderer.get_sprites():
            pygame.draw.rect(screen, 'red', spr.rect, 1)
            s = font.render(str(spr.z_layer), True, 'red')
            screen.blit(s, spr.rect.center)

        pygame.draw.line(screen, 'black', (0, self.ground.position.y), (settings.screen_width, self.ground.position.y),
                         2)

    def _update_twin(self):
        past = [a for a in self.appearances if a.when_in_s >= self.countdown_timer.time_left_in_sec]
        if len(past) > 1:
            logger.error(f"only one appearance at the time! Currently: {len(past)}")

        if not past:
            return

        if self.twin.state == Twin.STUNNED:
            return

        for p in past:
            self.appearances.remove(p)
        past = past[-1]

        past.duration  # todo show twin for n seconds

        self.twin_timer.stop()
        self.twin_timer.start(past.duration, False)
        self.twin.visible = True
        window = self.windows[past.window_id]
        self.twin.position.update(window.position)
        self.twin.position.x += random.randint(-settings.TWIN_WINDOW_OFFSET, settings.TWIN_WINDOW_OFFSET)
        self.twin.rect.center = window.position
        self.twin.position.y += (window.rect.height / 2 - self.twin.rect.height / 2)
        logger.info("Updated twin to position %s", self.twin.position)

        item = past.item
        if item:
            # drop item
            iw, ih = resource_manager.get_resource(item.resource).image.get_size()
            item.mass = iw * ih
            item.rect.width = iw
            item.rect.height = ih
            item.window = window
            item.position.update(self.twin.position)
            item.vel.update(0, 0)
            if settings.ITEM_RANDOM_DIRECTION:
                item.vel.update(settings.ITEM_MAX_SPEED, 0)
                item.vel.rotate_ip(-180 * random.random())
            item.spin()
            self._add_entity_sprite(item)
            self.entities.append(item)
            self.items.append(item)
            logger.info("Dropping item %s at %s", item, item.position)

    def _hide_twin(self, *args, **kwargs):
        self.twin.visible = False
        return 0

    def _update_items(self, dt):
        dt *= settings.DEBUG_TIME
        if self.slow_motion:
            dt *= settings.SLOW_MOTION_FACTOR
        for item in self.items:
            item.update(dt)
            self.e2s[item].rotation = item.angle.x

    def _update_entities(self):
        for ent in self.entities:
            ent.rect.centerx = ent.position.x
            ent.rect.centery = ent.position.y

    def _update_message(self, delta):
        if not self.is_level_running or settings.current_level_idx >= len(levels.levels):
            # game won, no more levels
            return
        level = levels.levels[settings.current_level_idx]
        self.game_message_interlude -= delta
        if self.game_message is not None:
            # maintain live game message
            if not self.game_message.alive:
                self.game_message = None
                self.game_message_interlude = settings.MESSAGE_INTERLUDE
        elif self._check_end_condition():
            # win-level message?
            pass
        elif self.game_message_interlude <= 0 and self.game_message_index < len(level.messages):
            # observe interlude, spawn new message
            message_text = level.messages[self.game_message_index]
            self.game_message_index += 1
            self.game_message = game_message.GameMessage(message_text)


logger.debug("imported")
