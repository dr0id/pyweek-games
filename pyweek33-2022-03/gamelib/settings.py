# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'settings.py' is part of pw-33
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The settings of the game.
"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2022"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module... not practical in settings.py!
import pygame
from gamelib import imagecache
from pyknic.generators import GlobalIdGenerator

from pyknic.pyknic_pygame import pygametext

logger = logging.getLogger(__name__)
logger.debug("importing...")

# --------------------------------------------------------------------------------
# IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT
# --------------------------------------------------------------------------------
#
# This file contains the distributed default settings.
#
# Don't override settings by editing this file, else they will end up in the repo.
# Instead, see the DEBUGS section at the end of this file for instructions on
# keeping your custom settings out of the repo.
#
# --------------------------------------------------------------------------------
# IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT
# --------------------------------------------------------------------------------


#
#
# # ------------------------------------------------------------------------------
# # Tunables
# # ------------------------------------------------------------------------------
#
# print_fps = False
max_fps = 60
master_volume = 0.5  # min: 0.0; max: 1.0
music_volume = 1.0  # resulting level = master_volume * music_volume
sfx_volume = 1.0  # resulting level = master_volume * sfx_volume
log_level = [logging.WARNING, logging.DEBUG, logging.INFO][1]

#
#
# # ------------------------------------------------------------------------------
# # Display
# # ------------------------------------------------------------------------------
#
# os.environ['SDL_VIDEO_CENTERED'] = '1'
#
screen_width = 1024
screen_height = 768
screen_size = screen_width, screen_height
screen_flags_presets = (
    0,
    pygame.FULLSCREEN | pygame.DOUBLEBUF,
)
screen_flags = screen_flags_presets[0]
default_fill_color = 100, 100, 100

title = "Evil twin KABOOOM!"
path_to_icon = "./data/icon.png"

# ------------------------------------------------------------------------------
# Game
# ------------------------------------------------------------------------------
current_level_idx = 0

ppu = 15
GROUND_Y = screen_height - 128
GROUND_WIDTH = screen_width
GROUND_HEIGHT = 128
HOUSE_WIDTH = 800
HOUSE_HEIGHT = 550
PLAYER_WIDTH = 20
PLAYER_HEIGHT = 48
PLAYER_MAX_FPS = 30
PLAYER_MIN_FPS = 5
TWIN_HEIGHT = 50
TWIN_WINDOW_OFFSET = 15  # how much random deviation from window center it can have
WINDOW_WIDTH = 60
WINDOW_HEIGHT = 80
ITEM_WIDTH = 20
ITEM_HEIGHT = 20
HIGH_SCORE_WIDTH = 100
HIGH_SCORE_HEIGHT = 40
TIMER_WIDTH = 100
TIMER_HEIGHT = 40

PLAYER_START_X = screen_width // 2
PLAYER_START_Y = GROUND_Y - PLAYER_HEIGHT // 2 + 4

ITEM_MAX_SPEED = 20
ITEM_SPIN_DEG_MAX = 400
ITEM_SPIN_DEG_MIN = 180

GRAVITY = 9.81
ITEM_PICKUP_DISTANCE = 10
HAND_DISTANCE = 15

ITEM_RANDOM_DIRECTION = True
ZAP_DISTANCE = 300
ZAP_DURATION = 0.5  # seconds
STUN_DURATION = 1.0  # seconds
PLAYER_TO_FPS_FACTOR = 15.0
# this evaluates the factor of f= abs(center1x - center2x) / (width1 + width2)
# f is a float [0, 1.0)  meaning 0 <= f < 1  (cannot be 1.0 because then there is no rect collision)
# order of evaluation: stun, zap
# therefore STUN_FACTOR and  STUN_FACTOR <= 1.0
STUN_FACTOR = 0.4

h = (GROUND_Y - ITEM_PICKUP_DISTANCE)
THROW_MAX_FORCE = (2 * GRAVITY * h) ** 0.5
POINTER_SCALING_FACTOR = 1.5

HAND_MAX_HIGH = GROUND_Y - PLAYER_HEIGHT
HAND_MIN_HIGH = GROUND_Y

MOUSE_VISIBLE = False
LIMIT_HAND_FREEDOM = False

SLOW_MOTION_DURATION = 10  # seconds
SLOW_MOTION_FACTOR = 0.2
GOTO_LEVELS = False
SIDE_DAMPENING_FACTOR = 0.1

MESSAGE_DURATION = 5  # seconds
MESSAGE_INIT_INTERLUDE = 1
MESSAGE_INTERLUDE = 2

LOOSE_LEVEL_MESSAGE = "Someone found the mess! Try again!"
WIN_LEVEL_MESSAGE = "All clear!"
GAME_END_MESSAGE = "The twins madness has gone now."

# ------------------------------------------------------------------------------
# Sound
# ------------------------------------------------------------------------------

# Mixer
MIXER_FREQUENCY = 0  # default:22050
MIXER_SIZE = -16  # default: -16
MIXER_CHANNELS = 2  # default: 2 stereo or 1 mono
MIXER_BUFFER_SIZE = 16  # default: 4096

MIXER_NUM_CHANNELS = 24
MIXER_RESERVED_CHANNELS = 6
(
    channel_evil_laugh,
    channel_squish,
    channel_item_returned,
    channel_crash,
    channel_hit_player,
    channel_hit_items_midair,
) = range(MIXER_RESERVED_CHANNELS)

music_ended_pygame_event = pygame.USEREVENT

global_id_names = {}
GLOBAL_ID_ACTIONS = 1000
global_id_generator = GlobalIdGenerator(GLOBAL_ID_ACTIONS)  # avoid collision with event ids

# ------------------------------------------------------------------------------
# Events
# ------------------------------------------------------------------------------
EVT_SFX_ITEM_HIT_WINDOW = global_id_generator.next('EVT_ITEM_HIT_WINDOW')
EVT_SFX_EVIL_LAUGH_A = global_id_generator.next('EVT_EVIL_LAUGH_A')
EVT_SFX_EVIL_LAUGH_B = global_id_generator.next()
EVT_SFX_EVIL_LAUGH_C = global_id_generator.next()
EVT_SFX_EVIL_LAUGH_D = global_id_generator.next()
EVT_SFX_EVIL_LAUGH_E = global_id_generator.next()
EVT_SFX_EVIL_LAUGH_F = global_id_generator.next()
EVT_SFX_EVIL_LAUGH_G = global_id_generator.next()
EVT_SFX_EVIL_LAUGH_H = global_id_generator.next()
EVT_SFX_NEW_LEVEL = global_id_generator.next('EVT_SFX_NEW_LEVEL')
EVT_SFX_SQUISH = global_id_generator.next('EVT_SFX_SQUISH')
EVT_SFX_CRASH = global_id_generator.next('EVT_SFX_CRASH')
EVT_SFX_HIT_TWIN = global_id_generator.next('EVT_SFX_HIT_TWIN')
EVT_SFX_HIT_PLAYER = global_id_generator.next('EVT_SFX_HIT_PLAYER')
EVT_SFX_HIT_ITEMS_MIDAIR = global_id_generator.next('EVT_SFX_HIT_ITEMS_MIDAIR')
EVT_SFX_CLOCK_TICK = global_id_generator.next('EVT_SFX_CLOCK_TICK')
EVT_SFX_CLOCK_ALARM = global_id_generator.next('EVT_SFX_CLOCK_ALARM')

# actions


# ------------------------------------------------------------------------------
# Stepper
# ------------------------------------------------------------------------------
STEP_DT = 1.0 / 120.0  # [1.0 / fps] = [s]
MAX_DT = 20 * STEP_DT  # [s]
DRAW_FPS = 60  # [fps]

# ------------------------------------------------------------------------------
# Graphics
# ------------------------------------------------------------------------------

draw_debug = False

# cached surfaces (required by spritefx)
# Note: pygametext does its own caching. No need to cache surfaces from pygametext.getsurf() unless you transform them.
# Note: these settings are not tunable on the fly. If you want to change the image cache settings during runtime, you'll
# have to call the image_cache instance's methods.
# Note: If you rely on aging or memory cap, remember to call image_cache.update(), or purge_memory() or purge_old().

image_cache_expire = 1 * 60  # expire unused textures after N seconds; recommend 1 min (1 * 60)
image_cache_memory = 5 * 2 ** 20  # force expiration if memory is exceeded; recommend 5 MB (5 * 2**20)
image_cache = imagecache.ImageCache(image_cache_expire, image_cache_memory)
image_cache.enable_age_cap(True)

pygametext.FONT_NAME_TEMPLATE = 'data/fonts/%s'
pygametext.MEMORY_LIMIT_MB = 32  # it takes about 5 min to hit the default 64 MB, so 32 is not too aggressive

# # cached surfaces (required by spritefx)
# # Note: pygametext does its own caching. No need to cache surfaces from pygametext.getsurf() unless you transform them.
# # Note: these settings are not tunable on the fly. If you want to change the image cache settings during runtime, you'll
# # have to call the image_cache instance's methods.
# # Note: If you rely on aging or memory cap, remember to call image_cache.update(), or purge_memory() or purge_old().
# image_cache_expire = 1 * 60  # expire unused textures after N seconds; recommend 1 min (1 * 60)
# image_cache_memory = 5 * 2 ** 20  # force expiration if memory is exceeded; recommend 5 MB (5 * 2**20)
# image_cache = imagecache.ImageCache(image_cache_expire, image_cache_memory)
# image_cache.enable_age_cap(True)

# font_themes is used with the functions in module pygametext
# Example call to render some text using a theme:
#   image = pygametext.getsurf('some text', **settings.font_themes['intro'])
# font_themes can be accessed by nested dict keys:
#   font_theme['intro']['fontname']
font_themes = dict(
    # Game title
    game_message=dict(
        fontname='Vera.ttf',
        fontsize=25,
        color='yellow',
        gcolor='orange',
        ocolor='black',
        # owidth=0.5,
        scolor='grey20',
        shadow=(1, 1),
    ),
    title=dict(
        fontname='Bubblegum_Sans.ttf',
        fontsize=50,
        color='red2',
        gcolor='orange1',
        ocolor='yellow',
        owidth=0.5,
        scolor=None,
        shadow=None,
    ),
    loose=dict(
        fontname='Bubblegum_Sans.ttf',
        fontsize=50,
        color='red2',
        gcolor='orange1',
        ocolor='yellow',
        owidth=0.5,
        scolor=None,
        shadow=None,
    ),
    win=dict(
        fontname='Bubblegum_Sans.ttf',
        fontsize=50,
        color='red2',
        gcolor='orange1',
        ocolor='yellow',
        owidth=0.5,
        scolor=None,
        shadow=None,
    ),
    intro2=dict(
        fontname='VeraBd.ttf',
        fontsize=25,
        color='white',
        gcolor='grey80',
        # ocolor=None,
        # owidth=0.5,
        scolor='grey20',
        shadow=(1, 1),
    ),
    speechbubble=dict(
        fontname='ComicNoteSmooth.ttf',
        fontsize=20,
        color='black',
        width=300,
        # gcolor='orange',
        # ocolor='black',
        # owidth=0.1,
        # scolor=None,
        # shadow=None,
        background=(255, 240, 142, 255)
    ),
    navigation=dict(
        fontname='ComicNoteSmooth.ttf',
        fontsize=20,
        color='black',
        width=300,
        # gcolor='orange',
        # ocolor='black',
        # owidth=0.1,
        # scolor=None,
        # shadow=None,
        background=(117, 255, 147, 200)
    ),
    gamehud=dict(
        fontname='digital-7.mono.ttf',  # fontname='digital-7.regular.ttf',
        fontsize=30,
        color='black',
        gcolor=None,  # 'orange2',
        ocolor=None,
        owidth=0.5,
        scolor='grey20',
        shadow=(1, 1),
    ),
    floatingtext=dict(
        fontname='Vera.ttf',
        fontsize=10,
        color='white',
        # gcolor='orange',
        ocolor='black',
        owidth=0.1,
        scolor=None,
        shadow=None,
        background=(0, 0, 0, 128)
    ),
    debug=dict(
        fontname='Vera.ttf',
        fontsize=16,
        color='yellow',
        # gcolor='orange',
        ocolor='black',
        owidth=0.5,
        scolor='black',
        shadow=(1, 1),
    ),
)

# DEV DEBUG CHEATS

# time step multiplier; <1.0 to slow down time; NOTE change in _custom.py, not here!
# to use it, place "dt *= settings.DEBUG_TIME" anywhere you want to modify the time step
DEBUG_TIME = 1.0

# noinspection PyBroadException
try:
    # define a _custom.py file to override some settings (useful while developing)
    # noinspection PyUnresolvedReferences,PyProtectedMember
    from gamelib._custom import *
except ImportError:
    pass

logger.debug("imported")
