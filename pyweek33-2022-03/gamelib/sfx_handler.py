# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'sfx_handler.py' is part of pw-33
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

import pygame.mixer

from gamelib import settings, resources
from gamelib import resources
from pyknic.pyknic_pygame import sfx

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module
from pyknic.pyknic_pygame.resource2 import resource_manager

logger = logging.getLogger(__name__)
logger.debug("importing...")


class SfxHandler(object):
    def __init__(self, music_player):
        self.music_player = music_player
        self.sfx_player = sfx.SoundPlayer(1.0)
        self.sfx_player.setup_channels(settings.MIXER_NUM_CHANNELS, settings.MIXER_RESERVED_CHANNELS)
        # todo this it not needed anymore, use resource_manager.resources[Id] instead!
        # self.sfx_player.load(resource_sfx.sfx_data)

        # todo: legacy; dunno if this is still useful
        # self.music_type = self.music_player.keep_last_musictype
        # self.last_position = self.music_player.keep_last_position
        # self.where = 'main'

    def register_events(self, event_dispatcher):
        # todo: register events and listeners
        # examples...
        # event_dispatcher.register_event_type(settings.EVT_GONG_SOUND)
        # event_dispatcher.register_event_type(settings.EVT_PLAY_SOUND_DATA)
        # event_dispatcher.register_event_type(settings.EVT_GUMM_SOUND)
        #
        # event_dispatcher.add_listener(settings.EVT_GONG_SOUND, self._on_gong)
        # event_dispatcher.add_listener(settings.EVT_PLAY_SOUND_DATA, self._on_play_sound_data)
        # event_dispatcher.add_listener(settings.EVT_GUMM_SOUND, self._on_play_gumm_sound)
        for evt in (
            settings.EVT_SFX_ITEM_HIT_WINDOW,
            settings.EVT_SFX_EVIL_LAUGH_A,
            settings.EVT_SFX_EVIL_LAUGH_B,
            settings.EVT_SFX_EVIL_LAUGH_C,
            settings.EVT_SFX_EVIL_LAUGH_D,
            settings.EVT_SFX_EVIL_LAUGH_E,
            settings.EVT_SFX_EVIL_LAUGH_F,
            settings.EVT_SFX_EVIL_LAUGH_G,
            settings.EVT_SFX_EVIL_LAUGH_H,
            settings.EVT_SFX_NEW_LEVEL,
            settings.EVT_SFX_SQUISH,
            settings.EVT_SFX_CRASH,
            settings.EVT_SFX_HIT_TWIN,
            settings.EVT_SFX_HIT_PLAYER,
            settings.EVT_SFX_HIT_ITEMS_MIDAIR,
            settings.EVT_SFX_CLOCK_TICK,
            settings.EVT_SFX_CLOCK_ALARM,
        ):
            event_dispatcher.register_event_type(evt)
            event_dispatcher.add_listener(evt, self._on_sfx)

        pass

    # todo: implement event handlers
    # def _on_jump(self, *args):
    #     self.sfx_player.play(resource_manager.resources[resources.resource_sound1_example])
    #
    # def _on_gong(self, *args):
    #     self.sfx_player.play(resource_manager.resources[resources.resource_gong_example])
    #
    # def _on_play_sound_data(self, sound_data):
    #     self.sfx_player.play(sound_data)
    #
    # def _on_play_gumm_sound(self, resource_id):
    #     self.sfx_player.play(resource_manager.resources[resource_id])

    def _on_sfx(self, sound_data):
        self.sfx_player.play(sound_data)

    # todo: music carousel, if gameplay is gonna have mood music
    # def _on_music_event(self, event_type, *args):
    #     self.music_player.fill_music_carousel(resources.songs)
    #     self.music_player.start_music_carousel()


logger.debug("imported")
