# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'sprites.py' is part of pw-33
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2022"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []    # list of public visible parts of this module

import pygame.math
import pygame.transform

from pyknic.pyknic_pygame import pygametext
from pyknic.pyknic_pygame.spritesystem import Sprite

from pyknic.animation import Animation

import pyknic.pyknic_pygame

logger = logging.getLogger(__name__)
logger.debug("importing...")

logger.debug("imported")


class ArrowSprite(pyknic.pyknic_pygame.spritesystem.Sprite):

    def __init__(self, position, img, z_layer):
        self.draw_special = True
        self.dirty_update = False
        pyknic.pyknic_pygame.spritesystem.Sprite.__init__(self, img, position, z_layer=z_layer)
        self._original_image = img.copy()
        self.vector = pygame.math.Vector2(0, 0)
        self.scale = 1.0

    def update_vector(self, v, scale):
        self.vector.update(v.x, v.y)
        self.scale = scale

    def draw(self, surf: pygame.Surface, cam: pyknic.pyknic_pygame.spritesystem.Camera, renderer,
             interpolation_factor=1.0):
        screen_pos = cam.world_to_screen(self.position)
        screen_pos = self.position
        screen_pos = pygame.math.Vector2(screen_pos.x, screen_pos.y)
        end_point = cam.world_to_screen(self.position + self.vector)
        end_point = self.position + self.vector
        end_point = pygame.math.Vector2(end_point.x, end_point.y)
        width = (end_point - screen_pos).length()
        size = (width, self._original_image.get_height() * self.scale)
        img = pygame.transform.smoothscale(self._original_image, size)
        angle = self.vector.angle_to(pygame.math.Vector2(1, 0))
        img2 = pygame.transform.rotate(img, angle)
        r_size = pygame.math.Vector2(img2.get_size())
        offset = pygame.math.Vector2(-width / 2, 0).rotate(-angle)

        dest = screen_pos - r_size / 2 - offset
        return surf.blit(img2, (int(dest.x + 0.5), int(dest.y + 0.5)))


class AnimatedSprite(Sprite, Animation):

    def __init__(self, frames, fps, scheduler, position, z_layer):
        Animation.__init__(self, 0, len(frames), fps, scheduler)
        Sprite.__init__(self, frames[0], position, z_layer=z_layer)
        self.event_index_changed += self._idx_changed
        self.frames = frames
        self.visible = False

    def _idx_changed(self, *args):
        self.set_image(self.frames[self.current_index])


pygametext.FONT_NAME_TEMPLATE = 'data/fonts/%s'


class PygameTextSprite(Sprite):
    """
    A sprite for displaying text. It inherits from sprite and the only difference is that
    a string is provided instead of an image.
    """

    def __init__(self, text, position, font_theme, \
                 anchor=None, z_layer=1000, parallax_factors=None):
        """

        :Parameters:
            text : string
                the text to display
            position : Vector
                where it should be, in world coordinates
            font_theme: see settings.py font_themes for example how to configure pygametext with keyword arguments
            anchor['center'] : rect attribute name or Vector
                anchor point, defaults to 'center' but can be one of following:
                    topleft, midtop, topright, midright, bottomright, midbottom, bottomleft, midleft, center
                    or a Vector (in sprite coordinates) defining the offset from the sprite center
            z_layer[1000] : float or int
                the layer this sprite is in, lower values is 'farther away'
            parallax_factors[Vector(1.0, 1.0)] : Vector
                a vector containin two floats, normally in the range [0.0, 1.0], if set to 0.0 then
                the sprite will not scroll (kinda attacked to the camera), this works good for single
                screen games using one camera (for split screen with multiple cameras this wont work because
                each screen should have its own HUD sprites, therefore the HUDRenderer should be used)

        """
        self._text = ""
        self.font_theme = font_theme
        Sprite.__init__(self, pygame.Surface((0, 0)), position, anchor=anchor, \
                        z_layer=z_layer, parallax_factors=parallax_factors)
        self.text = text

    def _get_text(self):
        """
        Get the text.
        """
        return self._text

    def _set_text(self, value):
        """
        Set the text.
        """
        t = str(value)
        if t != self._text:
            self._text = t
            self._orig_image = self.render()
            self.on_text_changed()

    text = property(_get_text, _set_text, doc="""Gets or sets the text.""")

    def on_text_changed(self):
        """
        This is called when the text changes.
        """
        # self._update()
        self._dirty_sprites.add(self)

    def render(self):
        """
        Renders the text using the attributes saved onto an image and returns it.

        :Returns: surface with the rendered text.
        """
        img = pygametext.getsurf(self._text, **self.font_theme)
        return img
