# -*- coding: utf-8 -*-
"""interpolation.py - a general purpose numeric interpolator for one or more values

r, g, b, a = pygame.Color('yellow')
interp_color = Interpolator(r, g, b)
r, g, b, a = pygame.Color('red')
interp_color.update(r, g, b)
if interp_color.differs():
    for i in range(51):     # <- free to use any linear or curve function
        r, g, b = interp_color.from_start(i / 50.0)
interp_color.sync_start()
"""


__version__ = '3.0.0'
__author__ = 'Gummbum, (c) 2017'
__vernum__ = tuple(int(s) for s in __version__.split('.'))
__all__ = ['Interpolator']


class Interpolator(object):
    """a general purpose numeric interpolator for one or more values
    """

    def __init__(self, *args):
        """create an Interpolator

        The args are an arbitrary number of numeric values. Initially the start and end values are set equal to the
        args.

        The same number of args supplied to the constructor must be passed to the update() and set() methods.

        :param args: varargs; initial numbers
        """
        self._len = len(args)
        self.start = list(args)
        self.end = list(args)
        self._range = range(self._len)

    def differs(self):
        """return the evaluation of start != end

        :return: bool; True if start and end differ, else False
        """
        return self.start != self.end

    def update(self, *args):
        """set start equal to end, and set end equal to args

        :param args: the new values
        :return: None
        """
        if len(args) != self._len:
            raise ValueError('length {} expected, got {}'.format(self._len, len(args)))
        self.start[:] = self.end
        self.end[:] = args

    def from_start(self, alpha, invert=False):
        """add interpolated offset to start

        The alpha must be a value from 0 to 1. It is not checked. You are on your honor. :)

        If invert is True, then alpha is inverted to 1.0 - alpha for the calculation.

        :param alpha: an interpolate (value from 0 to 1)
        :param invert: whether to invert the interpolation
        :return: interpolated offset from start
        """
        return [val + offset for val, offset in zip(self.start, self.raw_offset(alpha, invert))]

    def from_end(self, alpha, invert=False):
        """subtract interpolated offset from end

        The alpha must be a value from 0 to 1. It is not checked. You are on your honor. :)

        If invert is True, then alpha is inverted to 1.0 - alpha for the calculation.

        :param alpha: an interpolate (value from 0 to 1)
        :param invert: whether to invert the interpolation
        :return: interpolated offset from end
        """
        return [val - offset for val, offset in zip(self.end, self.raw_offset(alpha, invert))]

    def raw_offset(self, alpha, invert=False):
        """calculate the interpolated value between start and end as a raw offset

        The alpha must be a value from 0 to 1. It is not checked or clamped. This permits the use of functions that are
        not linear, and may yield values outside of 0 and 1.

        If invert is True, then alpha is inverted to 1.0 - alpha for the calculation.

        :param alpha: an interpolate (value from 0 to 1)
        :param invert: whether to invert the interpolation
        :return: interpolated difference
        """
        if invert:
            alpha = 1.0 - alpha
        # Hacked for speed
        #return [(end - start) * alpha for start, end in zip(self.start, self.end)]
        start = self.start
        end = self.end
        #return [(end[i] - start[i]) * alpha for i in self._range]
        return (end[0] - start[0]) * alpha, (end[1] - start[1]) * alpha

    def set(self, *args):
        """set start and end equal to args

        :param args: varargs; set start and end equal to args
        :return: None
        """
        if len(args) != self._len:
            raise ValueError('length {} expected, got {}'.format(self._len, len(args)))
        self.start[:] = self.end[:] = args

    def get_start(self):
        """return a copy of start

        :return: list
        """
        return self.start[:]

    def get_end(self):
        """return a copy of end

        :return: list
        """
        return self.end[:]

    def set_start(self, *args):
        """set start equal to args

        :param args: varargs; set start equal to args
        :return: None
        """
        if len(args) != self._len:
            raise ValueError('length {} expected, got {}'.format(self._len, len(args)))
        self.start[:] = args

    def set_end(self, *args):
        """set end equal to args

        :param args: varargs; set end equal to args
        :return: None
        """
        if len(args) != self._len:
            raise ValueError('length {} expected, got {}'.format(self._len, len(args)))
        self.end[:] = args

    def sync_start(self):
        """set start equal to end

        :return: None
        """
        self.start[:] = self.end

    def sync_end(self):
        """set end equal to start

        :return: None
        """
        self.end[:] = self.start


if __name__ == '__main__':

    # A little demo to interpolate mouse drags and clicks.

    import random
    import re
    import pygame
    from pygame.colordict import THECOLORS

    def draw_segment():
        if interp_line.differs():
            # interpolate the interp_line: points a to b
            # interpolate the color: previous color to random new color
            interp_color.update(*pygame.Color(random.choice(the_colors))[:3])
            x2, y2 = interp_line.start
            pygame.draw.line(screen, pygame.Color('red'), interp_line.start, interp_line.end, 1)
            # plot the segments (x1, y1)-(x2, y2) while graduating the color
            for i in range(1, 26):
                alpha = i / 25.0
                x1, y1 = x2, y2
                x2, y2 = interp_line.from_start(alpha)
                r, g, b = interp_color.from_start(alpha)
                pygame.draw.line(screen, (r, g, b), (x1, y1), (x2, y2), 7)
            interp_line.sync_start()
            interp_color.sync_start()
            pygame.display.flip()

    pygame.init()
    screen = pygame.display.set_mode((640, 480))
    screen_rect = screen.get_rect()
    clock = pygame.time.Clock()

    the_colors = [c for c in THECOLORS if not re.match(r'(^(black|gray|grey|white)|([34]$))', c)]
    interp_line = Interpolator(*screen_rect.center)
    interp_color = Interpolator(*pygame.Color('grey20')[:3])

    pygame.display.set_caption('Click, or Click-n-drag! | SPACE: Clear screen')

    mouse_action = ''
    mousex, mousey = screen_rect.center

    running = True
    while running:
        clock.tick(30)
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                quit()
            elif e.type == pygame.KEYDOWN:
                if e.key == pygame.K_ESCAPE:
                    quit()
                elif e.key == pygame.K_SPACE:
                    screen.fill((0, 0, 0))
                    pygame.display.flip()
            elif e.type == pygame.MOUSEBUTTONDOWN:
                mouse_action = 'click'
                mousex, mousey = e.pos
                interp_line.update(mousex, mousey)
                draw_segment()
            elif e.type == pygame.MOUSEBUTTONUP:
                mouse_action = ''
            elif e.type == pygame.MOUSEMOTION:
                if mouse_action == 'click':
                    mousex, mousey = e.pos
                    # restrict drag distance >= 35 pixels for prettier color grading
                    fx, fy = interp_line.end
                    if abs(fx - mousex) >= 35 or abs(fy - mousey) >= 35:
                        interp_line.update(mousex, mousey)
                        draw_segment()
