# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'resource_loader.py' is part of pw-33
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""


"""
from __future__ import print_function

import logging

import pygame

from pyknic.pyknic_pygame.sfx import SoundData, MusicData
from pyknic.resource2 import AbstractResourceLoader, AbstractResourceConfig, AbstractResource

logger = logging.getLogger(__name__)
logger.debug("importing...")


class SoundDataLoader(AbstractResourceLoader):

    def __init__(self):
        AbstractResourceLoader.__init__(self, SoundData)

    def load(self, sound_data_config, file_cache):
        key = (sound_data_config.filename, sound_data_config.volume, sound_data_config.reserved_channel_id)
        sound = file_cache.get(key, None)
        if sound is None:
            sound = self._load_sound(sound_data_config)
            file_cache[key] = sound
        return sound  # this is unusual, returns a SoundData instance with sound and channel attributes filled

    @staticmethod
    def _load_sound(data):
        if pygame.mixer.get_init() is None:
            raise Exception("pygame.mixer not initialized!")

        reserved_channel_ids = set()
        if isinstance(data, SoundData):
            logger.debug('loading sfx: {}', data.filename)
            try:
                data.sound = pygame.mixer.Sound(data.filename)
            except Exception as e:
                logger.error("error loading %s: %s", data.filename, e)
                raise e
            data.sound.set_volume(data.volume)
            if data.reserved_channel_id is not None:
                reserved_channel_ids.add(data.reserved_channel_id)
                # todo after pw: not sure how to handle this
                # if len(reserved_channel_ids) > self._num_reserved_channels:
                #     raise OverflowError("loading more reserved channels as configured reserved channels!")
                data.channel = pygame.mixer.Channel(data.reserved_channel_id)
        else:
            raise TypeError("cannot load other than SoundData instances, but it is " + type(data))
        return data

    def unload(self, res):
        pass


class MusicLoader(AbstractResourceLoader):

    def __init__(self):
        AbstractResourceLoader.__init__(self, MusicData)

    def load(self, music_config, file_cache):
        key = (music_config.filename, music_config.volume)
        music = file_cache.get(key, None)
        if music is None:
            music = music_config
            file_cache[key] = music
        return music  # this is unusual, returns a MusicData instance without modifications

    def unload(self, res):
        pass


class FontConfig(AbstractResourceConfig):

    def __init__(self, path_to_font, size):
        transformation = None
        AbstractResourceConfig.__init__(self, transformation)
        self.filename = path_to_font
        self.size = size


class FontResource(AbstractResource):

    def __init__(self, font, resource_config):
        AbstractResource.__init__(self, resource_config)
        self.font = font


class FontLoader(AbstractResourceLoader):

    def __init__(self):
        AbstractResourceLoader.__init__(self, FontConfig)

    def load(self, font_config, file_cache):
        key = (font_config.filename, font_config.size)
        font_res = file_cache.get(key, None)
        if font_res is None:
            font = pygame.font.Font(font_config.filename, font_config.size)
            font_res = FontResource(font, font_config)
            file_cache[key] = font_res
        return font_res

    def unload(self, res):
        pass


logger.debug("imported")
