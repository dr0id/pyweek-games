# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'context_intro.py' is part of pw-33
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This is the intro context, executed once.

"""
from __future__ import print_function, division

import logging

import pygame.display

import pyknic.pyknic_pygame.context.effects
from gamelib import settings, resources
from gamelib.eventdispatcher import event_dispatcher
from pyknic.pyknic_pygame import pygametext
from pyknic.pyknic_pygame.context import TimeSteppedContext

logger = logging.getLogger(__name__)
logger.debug("importing...")


class IntroContext(TimeSteppedContext):
    done = False

    def __init__(self, music_player):
        TimeSteppedContext.__init__(self, settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS)
        self.music_player = music_player
        self.screen = None
        self.center = None
        self.max_speed = 100
        self.min_speed = 1000
        self.max_duration_in_seconds = 2

        img = pygametext.getsurf('INTRO', sysfontname='sans', fontsize=50, bold=True)
        spr = pygame.sprite.Sprite()
        img = pygame.image.load('data/image/intro.png').convert()
        spr.image = img
        spr.rect = img.get_rect(center=(settings.screen_width // 2, settings.screen_height // 2))
        self.sprite = spr
        self.kaboom = pygame.sprite.Sprite()
        self.kaboom.image = pygame.image.load('data/image/kaboom.png').convert_alpha()
        self.kaboom.rect = self.kaboom.image.get_rect(midbottom=(settings.screen_width // 2, 0))
        self.a = 9.81
        self.v = 0

    def enter(self):
        self.screen = pygame.display.get_surface()  # assign the screen surface to draw on.
        # an example how to get an resource, see
        # img = resource_manager.resources[settings.resource_hero]  # settings.resource_hero is just an id, you have to know what kind of resource it is

    def exit(self):
        pass

    def suspend(self):
        pass

    def resume(self):
        self.music_player.start_music_carousel()

    def update_step(self, delta, sim_time, *args):
        for event in pygame.event.get():
            if event.type == pygame.QUIT or event.type == pygame.KEYDOWN or event.type == pygame.MOUSEBUTTONDOWN:
                event_dispatcher.fire(settings.EVT_SFX_EVIL_LAUGH_H,
                                      resources.resource_def[resources.resource_sfx_evil_laugh_h])
                self.music_player.fadeout(500)
                if not self.done:
                    self.pop(effect=pyknic.pyknic_pygame.context.effects.ExplosionEffect(
                        (settings.screen_width, settings.screen_height)))
                    self.done = True

        self.v += self.a * delta * settings.ppu
        self.kaboom.rect.y += self.v * delta * settings.ppu

        if self.kaboom.rect.centery >= settings.screen_height // 2:
            self.exchange(IntroContext(self.music_player), effect=pyknic.pyknic_pygame.context.effects.ExplosionEffect(
                (settings.screen_width, settings.screen_height)))

    def draw_step(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0, screen=None):
        # draw to the screen... and flip it.
        screen = self.screen if screen is None else screen
        screen.fill((0, 0, 0))
        screen.blit(self.sprite.image, self.sprite.rect)
        screen.blit(self.kaboom.image, self.kaboom.rect)
        if do_flip:
            pygame.display.flip()


logger.debug("imported")
