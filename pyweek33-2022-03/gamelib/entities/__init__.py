# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file '__init__.py.py' is part of pw-33
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The entities package.
"""
from __future__ import print_function

import logging
import math
import random

import pygame
from pygame import Rect
from pygame.math import Vector2 as Vec

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2022"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["Player", "CountdownTimer", "Ground", "House", "Twin", "HighScore", "Window",
           "Item"]  # list of public visible parts of this module

import pyknic
from gamelib import settings, resources
from gamelib.settings import HAND_DISTANCE, ZAP_DISTANCE, ZAP_DURATION, STUN_DURATION, STUN_FACTOR, \
    HAND_MAX_HIGH, HAND_MIN_HIGH, SIDE_DAMPENING_FACTOR

logger = logging.getLogger(__name__)
logger.debug("importing...")

_kind0 = settings.global_id_generator.next("_kind0 Player")
_kind1 = settings.global_id_generator.next('_kind1 Window')
_kind2 = settings.global_id_generator.next('_kind2 Twin')
_kind3 = settings.global_id_generator.next('_kind3 Item')
_kind4 = settings.global_id_generator.next('_kind4 Ground')
_kind5 = settings.global_id_generator.next('_kind5 House')
_kind6 = settings.global_id_generator.next('_kind6 HighScore')
_kind7 = settings.global_id_generator.next('_kind7 CountdownTimer')
_kind8 = settings.global_id_generator.next('_kind8 Mouse')
_kind9 = settings.global_id_generator.next('_kind9 Pointer')

z_default = -1
z_house = 20
z_window_bg = 25
z_twin = 30
z_window = 35

z_aura = 38
z_ground = 40
z_item = 41
z_player = 42
z_gras1 = 43
z_item_front = 44
z_gras2 = 45

z_high_score = 60
z_timer = 70
z_pointer = 1000
z_hand = 10000


class _Entity:
    kind = None
    resource = None
    anchor = 'center'
    rect = None  # see _Entity._init_pos()
    _position = None  # see _Entity._init_pos()
    z_layer = z_default

    def __init__(self):
        self._position = Vec(0, 0)

    def _init_pos(self, x, y):
        self._position.update(x, y)
        self.rect = Rect(0, 0, 0, 0)
        setattr(self.rect, self.anchor, self.position)

    def _getpos(self):
        return self._position

    def _setpos(self, vec):
        self._position.update(vec[0], vec[1])

    position = property(_getpos, _setpos)


class Player(_Entity):
    # todo states: normal, dizzy, flat, pointing
    NORMAL = 0
    POINTING = 1
    STUNNED = 2
    FLAT = 3
    ZAPPED = 4
    kind = _kind0
    resource = resources.resource_player
    z_layer = z_player

    def __init__(self, timer: pyknic.timing.Timer):
        _Entity.__init__(self)
        self.face_left = False
        self.grabbed_item = None
        self._init_pos(settings.PLAYER_START_X, settings.PLAYER_START_Y)
        self.rect.width = settings.PLAYER_WIDTH
        self.rect.height = settings.PLAYER_HEIGHT
        self.state = Player.NORMAL
        self.timer = timer
        self.timer.event_elapsed += self._timer_fired

    def _timer_fired(self, *args, **kwargs):
        if self.state == Player.ZAPPED:
            self.state = Player.NORMAL
        elif self.state == Player.STUNNED:
            self.state = Player.NORMAL

    def grab(self, item):
        if self.state == Player.NORMAL and item.state == Item.GROUNDED:
            self.state = Player.POINTING
            self.grabbed_item = item
            item.position.y -= settings.ITEM_PICKUP_DISTANCE
            return True
        return False

    def throw(self, event_pos):
        if self.grabbed_item:
            self.state = Player.NORMAL
            mx, my = event_pos
            p = Vec(mx - self.grabbed_item.position.x, my - self.grabbed_item.position.y)
            # p *= settings.ppu

            max_y = settings.THROW_MAX_FORCE
            if p.length() > max_y:
                p *= max_y / p.length()

            logger.info("p: %s m: %s i: %s", p, event_pos, self.grabbed_item.position)
            self.grabbed_item.vel.update(p)
            self.grabbed_item.spin()
            # self.grabbed_item.acc += p
            self.grabbed_item.state = Item.THROWN

            self.grabbed_item = None

    def hit(self, item, mouse):
        f = math.fabs(item.rect.centerx - self.rect.centerx) * 2.0 / (item.rect.width + self.rect.width)
        logger.debug("FACTOR: %s  %s/%s  %s, %s", f, math.fabs(item.rect.centerx - self.rect.centerx),
                     (item.rect.width + self.rect.width), item.rect, self.rect)
        if f < STUN_FACTOR:  # stun
            self.stun()
        else:  # zap
            self.zap(item, mouse)

    def zap(self, item, mouse):
        if self.state == Player.POINTING:
            self.grabbed_item.state = Item.FALLING
            self.grabbed_item = None

        # update self.state *after* pointing check
        if self.state == Player.NORMAL or self.state == Player.POINTING:
            dist = ZAP_DISTANCE
            if item.rect.centerx > self.rect.centerx:
                dist *= -1
            self.position.x += dist
            if self.position.x < 0:
                self.position.x = 0
            if self.position.x > settings.screen_width:
                self.position.x = settings.screen_width
            mouse.update_hardware_pos(self.position)
            self.state = Player.ZAPPED
            self.timer.start(ZAP_DURATION, False)

    def stun(self):
        if self.state == Player.POINTING:
            self.grabbed_item.state = Item.FALLING
            self.grabbed_item = None

        # update self.state *after* pointing check
        if self.state == Player.NORMAL or self.state == Player.POINTING:
            self.state = Player.STUNNED
            self.timer.start(STUN_DURATION, False)

    def move_to(self, pos_x, mouse_position_x):
        if self.state == Player.NORMAL:
            self.face_left = pos_x < mouse_position_x
            self.position.x = pos_x + (HAND_DISTANCE if self.face_left else -1 * HAND_DISTANCE)
            return True
        return False


class Window(_Entity):
    kind = _kind1
    resource = resources.resource_window
    z_layer = z_window

    def __init__(self, x, y, is_open=True):
        _Entity.__init__(self)
        self._init_pos(x, y)
        self.rect.w = settings.WINDOW_WIDTH
        self.rect.h = settings.WINDOW_HEIGHT
        self.is_open = is_open

    def clone(self):
        w = Window(self.position.x, self.position.y)
        w.is_open = self.is_open
        return w


class Twin(_Entity):
    kind = _kind2
    resource = resources.resource_twin
    z_layer = z_twin
    NORMAL = 0
    STUNNED = 1

    def __init__(self):
        _Entity.__init__(self)
        self.state = Twin.NORMAL
        self._init_pos(0, 0)
        self.rect = pygame.Rect(0, 0, settings.PLAYER_WIDTH, settings.TWIN_HEIGHT)
        self.delay_in_sec = 5
        self.visible = False


class Item(_Entity):
    # todo states: falling, grounded, broken, returned ?
    kind = _kind3
    resource = None
    z_layer = z_item

    FALLING = 0
    GROUNDED = 1
    RETURNED = 2
    THROWN = 3

    def __init__(self, resource_id=None):
        self.resource = resource_id
        if resource_id is None:
            self.resource = random.choice(resources.resource_items)
        self.z_layer = random.choice((z_item, z_item_front))
        _Entity.__init__(self)
        self.rect = pygame.Rect(0, 0, 0, 0)
        self.vel = Vec(0, 0)
        self.acc = Vec(0, 0)
        self.angular_vel = 0.0  # spin it after midair collision
        self.angle = Vec(0, 0)
        self.state = Item.FALLING
        self.window = None
        self.rect.w = settings.ITEM_WIDTH
        self.rect.h = settings.ITEM_HEIGHT

    def clone(self):
        item = Item(self.resource)
        return item

    def spin(self):
        self.angular_vel = random.randint(settings.ITEM_SPIN_DEG_MIN, settings.ITEM_SPIN_DEG_MAX) * random.choice(
            (-1, 1))

    def update(self, dt):
        if self.state == Item.FALLING or self.state == Item.THROWN:
            scale = settings.ppu
            self.acc.y += settings.GRAVITY
            # logger.debug("item acc: %s, vel: %s, pos: %s", self.acc, self.vel, self.position)
            self.vel += self.acc * dt * scale
            self.position += self.vel * dt * scale
            self.angle.x += self.angular_vel * dt
            # logger.debug("item acc: %s, vel: %s, pos: %s", self.acc, self.vel, self.position)
            if self.position.x < 0:
                self.position.x = 0 + 10  # todo replace with radius?
                self.vel.x = -self.vel.x * SIDE_DAMPENING_FACTOR
            if self.position.x > settings.screen_width:
                self.position.x = settings.screen_width - 10  # todo replace with radius?
                self.vel.x = -self.vel.x * SIDE_DAMPENING_FACTOR
        self.acc.update(0, 0)


class Ground(_Entity):
    kind = _kind4
    resource = resources.resource_ground
    anchor = 'midtop'
    z_layer = z_ground

    def __init__(self):
        _Entity.__init__(self)

    def set_y(self, y):
        self._init_pos(settings.screen_width / 2, y)  # only y-coordinate is relevant


class House(_Entity):
    kind = _kind5
    resource = resources.resource_house
    anchor = 'midbottom'
    z_layer = z_house

    def __init__(self):
        _Entity.__init__(self)
        self._init_pos(settings.screen_width // 2, settings.GROUND_Y)


class HighScore(_Entity):
    kind = _kind6
    resource = resources.resource_high_score
    anchor = 'topright'
    z_layer = z_high_score

    def __init__(self):
        _Entity.__init__(self)
        self._init_pos(settings.screen_width - 10, 10)
        self.score = 0
        self.total = 0

    @property
    def text(self):
        return f"{self.score:.0f}/{self.total:.0f}"

    def add(self, amount):
        self.score += amount
        logger.info("high score updated to %s by adding %s", self.score, amount)


class CountdownTimer(_Entity):
    kind = _kind7
    resource = resources.resource_timer
    anchor = 'center'
    z_layer = z_timer

    def __init__(self):
        _Entity.__init__(self)
        self._init_pos(10 + 60 / 2, 10 + 40 / 2)
        self.time_left_in_sec = 60.0


class Mouse(_Entity):
    kind = _kind8
    resource = resources.resource_hand
    z_layer = z_hand

    def __init__(self):
        _Entity.__init__(self)
        self._init_pos(0, 0)

    def update_hardware_pos(self, pos):
        self.update(pos)
        pygame.mouse.set_pos(pos)

    def update(self, pos):
        self.position.update(pos)
        # try to restrict y movement of the hand... not so good because actual mouse position is different -> can't grab
        if settings.LIMIT_HAND_FREEDOM:
            if self.position.y < HAND_MAX_HIGH:
                self.position.y = HAND_MAX_HIGH
            elif self.position.y > HAND_MIN_HIGH:
                self.position.y = HAND_MIN_HIGH


class Pointer(_Entity):
    kind = _kind9
    resource = resources.resource_pointer
    z_layer = z_pointer
    anchor = "midleft"

    def __init__(self):
        _Entity.__init__(self)
        self._pointer = Vec(1, 0)

    @property
    def pointer(self):
        if self._pointer.length_squared() > settings.THROW_MAX_FORCE ** 2:
            self._pointer.scale_to_length(settings.THROW_MAX_FORCE)
        return self._pointer * settings.POINTER_SCALING_FACTOR

    @property
    def scale(self):
        return 1.0 + self._pointer.length() / settings.THROW_MAX_FORCE * settings.POINTER_SCALING_FACTOR

    def update(self, pos):
        self._pointer.update(pos[0] - self.position.x, pos[1] - self.position.y)


logger.debug("imported")
