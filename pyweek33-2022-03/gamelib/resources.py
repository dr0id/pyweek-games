# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'resources.py' is part of pw-33
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Just holds the list of resources to load.

"""
from __future__ import print_function, division

import logging

import pygame.transform

from gamelib import settings
from gamelib.resource_loaders import FontConfig
from pyknic.pyknic_pygame.resource2 import ResizeTransform, ColorKeyTransform, AlphaTransform
from pyknic.pyknic_pygame.sfx import SoundData, MusicData
from pyknic.resource2 import FakeImage, Image, SpriteSheetConfig, BaseTransformation

logger = logging.getLogger(__name__)
logger.debug("importing...")

_global_id_generator = settings.global_id_generator


def get_generator_name(gen):
    return _global_id_generator.names[gen]


def sound_data(filename, volume=1.0):
    """return a SoundData object from data/sounds/ for use in a resource definition
    """
    return SoundData(f'data/sounds/{filename}', volume, reserved_channel_id=settings.channel_gong)


# ------------------------------------------------------------------------------
# ResourcesIds
# ------------------------------------------------------------------------------
# resource_item1 = global_id_generator.next("resource_item1")

# ------------------------------------------------------------------------------
# Resources
# ------------------------------------------------------------------------------

resource_player = _global_id_generator.next("resource_player")
resource_window = _global_id_generator.next()
resource_twin = _global_id_generator.next()
resource_ground = _global_id_generator.next()
resource_gras1 = _global_id_generator.next()
resource_gras2 = _global_id_generator.next()
resource_ground_night = _global_id_generator.next()
resource_gras1_night = _global_id_generator.next()
resource_gras2_night = _global_id_generator.next()
resource_house = _global_id_generator.next()
resource_high_score = _global_id_generator.next()
resource_timer = _global_id_generator.next()
resource_hand = _global_id_generator.next()
resource_hand_closed = _global_id_generator.next()
resource_pointer = _global_id_generator.next()
resource_stunned = _global_id_generator.next()
resource_stunned_evil = _global_id_generator.next()
resource_leprechaun = _global_id_generator.next()
resource_twin = _global_id_generator.next()
resource_aura = _global_id_generator.next()
resource_aura_evil = _global_id_generator.next()
resource_countdown_font = _global_id_generator.next()

# item resources
resource_item_tv = _global_id_generator.next('resource_item_tv')
resource_item_speaker = _global_id_generator.next('resource_item_speaker')
resource_item_cookie_jar = _global_id_generator.next('resource_item_cookie_jar')
resource_item_toaster = _global_id_generator.next('resource_item_toaster')
resource_item_cat = _global_id_generator.next('resource_item_cat')
resource_item_dog = _global_id_generator.next('resource_item_dog')
resource_item_budgie = _global_id_generator.next('resource_item_budgie')
resource_item_piano = _global_id_generator.next('resource_item_piano')
resource_item_gift2 = _global_id_generator.next('resource_item_gift2')
resource_item_crate = _global_id_generator.next('resource_item_crate')
resource_item_box = _global_id_generator.next('resource_item_box :')
resource_item_chest = _global_id_generator.next('resource_item_chest')
resource_item_gift3 = _global_id_generator.next('resource_item_gift3')
resource_item_gift1 = _global_id_generator.next('resource_item_gift1')

# items for sequential or random picking
resource_items = (
    resource_item_tv,
    resource_item_speaker,
    resource_item_cookie_jar,
    resource_item_toaster,
    resource_item_cat,
    resource_item_dog,
    resource_item_budgie,
    resource_item_piano,
    resource_item_gift2,
    resource_item_crate,
    resource_item_box,
    resource_item_chest,
    resource_item_gift3,
    resource_item_gift1,
)

# sfx resources
resource_sfx_item_returned = _global_id_generator.next('resource_sfx_item_returned')
resource_sfx_evil_laugh_a = _global_id_generator.next('resource_sfx_evil_laugh_a')
resource_sfx_evil_laugh_b = _global_id_generator.next('resource_sfx_evil_laugh_b')
resource_sfx_evil_laugh_c = _global_id_generator.next('resource_sfx_evil_laugh_c')
resource_sfx_evil_laugh_d = _global_id_generator.next('resource_sfx_evil_laugh_d')
resource_sfx_evil_laugh_e = _global_id_generator.next('resource_sfx_evil_laugh_e')
resource_sfx_evil_laugh_f = _global_id_generator.next('resource_sfx_evil_laugh_f')
resource_sfx_evil_laugh_g = _global_id_generator.next('resource_sfx_evil_laugh_g')
resource_sfx_evil_laugh_h = _global_id_generator.next('resource_sfx_evil_laugh_h')
resource_sfx_new_level = _global_id_generator.next('resource_sfx_new_level')
resource_sfx_squish = _global_id_generator.next('resource_sfx_squish')
resource_sfx_crash = _global_id_generator.next('resource_sfx_crash')
resource_sfx_hit_twin = _global_id_generator.next('resource_sfx_hit_twin')
resource_sfx_hit_player = _global_id_generator.next('resource_sfx_hit_player')
resource_sfx_hit_items_midair = _global_id_generator.next('resource_sfx_hit_items_midair')
resource_sfx_clock_tick = _global_id_generator.next('resource_sfx_clock_tick')
resource_sfx_clock_alarm = _global_id_generator.next('resource_sfx_clock_alarm')

resource_music_1 = _global_id_generator.next('resource_music_1')

alpha = AlphaTransform()


class Size2xTransform(BaseTransformation):

    def __init__(self):
        BaseTransformation.__init__(self)

    def transform(self, in_obj):
        transformed_obj = pygame.transform.scale2x(in_obj)
        return BaseTransformation.transform(self, transformed_obj)

    def __eq__(self, other):
        if isinstance(other, Size2xTransform):
            return True
        return NotImplemented

    def __hash__(self):
        return hash(Size2xTransform.__class__.__name__)


class TintTransform(BaseTransformation):

    def __init__(self, color):
        BaseTransformation.__init__(self)
        self.color = color

    def transform(self, in_obj):
        c: pygame.Surface = in_obj.copy()
        c.fill(self.color, None, pygame.BLEND_RGB_MULT)
        transformed_obj = c
        return BaseTransformation.transform(self, transformed_obj)

    def __eq__(self, other):
        if isinstance(other, Size2xTransform):
            return True
        return NotImplemented

    def __hash__(self):
        return hash(Size2xTransform.__class__.__name__)


alphaAnd2x = AlphaTransform().then(Size2xTransform())
resource_def = {
    resource_window: SpriteSheetConfig('data/image/free-vector-window-set-modified.png', (60, 80), 1, 9, 0,
                                       transformation=alpha),

    resource_item_gift2: Image("data/image/gift_02.png", alpha),
    resource_item_crate: Image("data/image/wooden_crate_04.png", alpha),
    resource_item_box: Image("data/image/box.png", alpha),
    resource_item_chest: Image("data/image/chest.png", alpha),
    resource_item_gift3: Image("data/image/gift_03.png", alpha),
    resource_item_gift1: Image("data/image/gift_01.png", alpha),

    resource_ground: Image('data/image/PisoDay.png', alpha),
    resource_gras1: Image('data/image/PisoDayLayer1.png', alpha),
    resource_gras2: Image('data/image/PisoDayLayer2.png', alpha),
    # resource_house: FakeImage(settings.HOUSE_WIDTH, settings.HOUSE_HEIGHT, 'grey', 'house'),
    resource_house: Image('data/image/house.png', alpha),

    resource_hand: Image('data/image/Hand-open.png', transformation=ResizeTransform(30, 30)),
    resource_hand_closed: Image('data/image/Hand-closed.png', transformation=ResizeTransform(30, 30)),
    resource_pointer: Image('data/image/pointer.png', transformation=alpha),  # FakeImage(40, 20, 'brown', 'pointer'),
    resource_stunned: SpriteSheetConfig('data/image/stunned.png', tile_size=(32, 48), row_count=1,
                                        column_count=4, fps=10, transformation=Size2xTransform().then(alpha)),
    resource_stunned_evil: SpriteSheetConfig('data/image/stunned.png', tile_size=(32, 48), row_count=1,
                                             column_count=4, fps=10, transformation=alpha),
    resource_leprechaun: SpriteSheetConfig('data/image/leprechaun.png', tile_size=(32, 48), row_count=4,
                                           column_count=4, fps=10, transformation=Size2xTransform().then(alpha),
                                           frames_slice=slice(8, 12)),
    resource_twin: SpriteSheetConfig('data/image/leprechaun.png', tile_size=(32, 48), row_count=4,
                                     column_count=4, fps=3, transformation=alpha, frames_slice=slice(0, 4)),
    resource_aura: SpriteSheetConfig('data/image/Smoke15Frames.png', tile_size=(256, 256), row_count=3,
                                     column_count=5, fps=7,
                                     transformation=ResizeTransform(150, 150).then(TintTransform((183, 239, 0))).then(
                                         alpha)),
    resource_aura_evil: SpriteSheetConfig('data/image/Smoke15Frames.png', tile_size=(256, 256), row_count=3,
                                          column_count=5, fps=7,
                                          transformation=ResizeTransform(80, 80).then(TintTransform((240, 0, 20))).then(
                                              alpha)),

    resource_item_tv: Image('data/image/obj_tv002.png', transformation=ResizeTransform(50, 50).then(alpha)),
    resource_item_speaker: Image('data/image/obj_speaker001.png', transformation=ResizeTransform(50, 50).then(alpha)),
    resource_item_cookie_jar: Image('data/image/obj_cookiejar001.png',
                                    transformation=ResizeTransform(40, 40).then(alpha)),
    resource_item_toaster: Image('data/image/obj_toaster001.png', transformation=ResizeTransform(40, 40).then(alpha)),
    resource_item_cat: Image('data/image/pixel-ani-cat.gif', transformation=alpha),
    resource_item_dog: Image('data/image/pixel-ani-dog.gif', transformation=alpha),
    resource_item_budgie: Image('data/image/pixel-ani-budgie.gif', transformation=alpha),
    resource_item_piano: Image('data/image/piano.png', transformation=ColorKeyTransform((238, 243, 250))),

    resource_countdown_font: FontConfig('data/fonts/Digital-7/digital-7.regular.ttf', 35),

    resource_sfx_item_returned: SoundData('data/sfx/58919__mattwasser__coin_up.ogg', 0.7,
                                          reserved_channel_id=settings.channel_item_returned),
    resource_sfx_evil_laugh_a: SoundData('data/sfx/118703__teqstudios__evil-laughs-demonic-echos_a.ogg', 0.5,
                                         reserved_channel_id=settings.channel_evil_laugh),
    resource_sfx_evil_laugh_b: SoundData('data/sfx/118703__teqstudios__evil-laughs-demonic-echos_b.ogg', 0.5,
                                         reserved_channel_id=settings.channel_evil_laugh),
    resource_sfx_evil_laugh_c: SoundData('data/sfx/118703__teqstudios__evil-laughs-demonic-echos_c.ogg', 0.5,
                                         reserved_channel_id=settings.channel_evil_laugh),
    resource_sfx_evil_laugh_d: SoundData('data/sfx/118703__teqstudios__evil-laughs-demonic-echos_d.ogg', 0.5,
                                         reserved_channel_id=settings.channel_evil_laugh),
    resource_sfx_evil_laugh_e: SoundData('data/sfx/118703__teqstudios__evil-laughs-demonic-echos_e.ogg', 0.5,
                                         reserved_channel_id=settings.channel_evil_laugh),
    resource_sfx_evil_laugh_f: SoundData('data/sfx/118703__teqstudios__evil-laughs-demonic-echos_f.ogg', 0.5,
                                         reserved_channel_id=settings.channel_evil_laugh),
    resource_sfx_evil_laugh_g: SoundData('data/sfx/118703__teqstudios__evil-laughs-demonic-echos_g.ogg', 0.5,
                                         reserved_channel_id=settings.channel_evil_laugh),
    resource_sfx_evil_laugh_h: SoundData('data/sfx/118703__teqstudios__evil-laughs-demonic-echos_h.ogg', 1.0,
                                         reserved_channel_id=settings.channel_evil_laugh),
    resource_sfx_new_level: SoundData('data/sfx/A-6734-Mach_New-7460_hifi.ogg', 0.5),
    resource_sfx_squish: SoundData('data/sfx/88511__gagaman__squish_a.ogg', 0.25,
                                   reserved_channel_id=settings.channel_squish),
    resource_sfx_crash: SoundData('data/sfx/115917__issalcake__weak-table-crash-break.ogg', 0.2,
                                  reserved_channel_id=settings.channel_crash),
    resource_sfx_hit_twin: SoundData('data/sfx/zap5a-short.ogg', 0.7),
    resource_sfx_hit_player: SoundData('data/sfx/ouch3.ogg', 0.7, reserved_channel_id=settings.channel_hit_player),
    resource_sfx_hit_items_midair: SoundData('data/sfx/drag_error.ogg', 0.7,
                                             reserved_channel_id=settings.channel_hit_items_midair),
    resource_sfx_clock_tick: SoundData('data/sfx/31882__HardPCM__Chip045_tick.ogg', 0.25),
    resource_sfx_clock_alarm: SoundData('data/sfx/31882__HardPCM__Chip045.ogg', 0.25),

    resource_music_1: MusicData('data/music/Anamalie.ogg', 0.4),
}

logger.debug("imported")

if __name__ == '__main__':
    logger.setLevel(logging.INFO)
    # print all missing resources
    logger.error("")
    logger.error("Missing resources:")
    for res_id, res in resource_def.items():
        if isinstance(res, FakeImage):
            logger.error(f" FakeImage Id: {res_id}  name: {res.name}  size: {res.width}x{res.height} ")

    logger.error("")
    logger.error("Used image files:")
    images = []
    for res_id, res in resource_def.items():
        if isinstance(res, Image) or isinstance(res, SpriteSheetConfig):
            images.append((res_id, res))
    for res_id, res in sorted(images, key=lambda i: i[1].path_to_file.lower()):
        logger.error(f"  Image: id: {res_id} filename: {res.path_to_file}")

    logger.error("")
    logger.error("Used sound files:")
    for res_id, res in resource_def.items():
        if isinstance(res, SoundData):
            logger.error(f"  Sound: id {res_id} filename: {res.filename}")

    logger.error("")
    logger.error("Used music files:")
    for res_id, res in resource_def.items():
        if isinstance(res, MusicData):
            logger.error(f"  Music: id {res_id} filename: {res.filename}")
