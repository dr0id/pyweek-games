# -*- coding: utf-8 -*-
import logging

from gamelib import settings, spritefx
from pyknic import tweening

logger = logging.getLogger(__name__)
logger.debug("importing...")


class GameMessage(object):

    font_theme_name = 'game_message'
    start_x = settings.screen_width
    start_y = 150

    def __init__(self, text, duration=settings.MESSAGE_DURATION):
        self.duration = duration
        self.sprite = spritefx.TextSprite(
            text, (self.start_x, self.start_y), expire=duration, theme_name=self.font_theme_name)

        # tween: enter screen-right to screen-center
        self.sprite.anchor = 'center'
        self.sprite.driftx_effect(self.sprite.x, settings.screen_width // 2 + 50, 1.0,
                                  tween_function=tweening.ease_out_quad, cb_end=self._cb_pause)

    @property
    def alive(self):
        return self.sprite.alive

    def _cb_pause(self, sprite, attr_name, tween):
        # tween: pause at current location
        self.sprite.driftx_effect(self.sprite.x, self.sprite.x - 100, self.duration - 2.0, cb_end=self._cb_finish)

    def _cb_finish(self, sprite, attr_name, tween):
        # tween: exit screen-left
        self.sprite.anchor = 'topright'
        self.sprite.driftx_effect(self.sprite.x, -1, 1.0, tween_function=tweening.ease_in_quad)


logger.debug("imported")
