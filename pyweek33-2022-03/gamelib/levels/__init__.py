# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file '__init__.py.py' is part of pw-33
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2022"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module

from gamelib import settings, resources
from gamelib.entities import Item, Window

logger = logging.getLogger(__name__)
logger.debug("importing...")


class Appearance:

    def __init__(self, when_in_s, duration, window_id, item):
        self.when_in_s = when_in_s
        self.duration = duration
        self.window_id = window_id
        self.item = item

    def clone(self, time_left_in_s):
        return Appearance(time_left_in_s - self.when_in_s, self.duration, self.window_id,
                          self.item.clone() if self.item else None)


class LevelBase:
    def __init__(self):
        self.ground_y = settings.GROUND_Y
        self.ITEM_RANDOM_DIRECTION = True
        self.messages = []


class Level0(LevelBase):

    def __init__(self):
        """
        Trainer. No spoiler needed for this one.
        """
        LevelBase.__init__(self)
        self.title = "Level 0 - Put it back! (Impish)"  # one window, 2-3 items max, enough time
        self.time_in_sec = 35.0
        self.win_condition = 4

        self.windows = [
            Window(400, 200),  # 1-3
        ]  # {id:position}
        self.appearances = [
            Appearance(10, 3, 0, Item(resources.resource_item_cat)),
            Appearance(20, 3, 0, Item(resources.resource_item_toaster)),
            Appearance(25, 3, 0, Item(resources.resource_item_cookie_jar)),
            Appearance(30, 3, 0, Item(resources.resource_item_budgie)),
        ]
        self.messages = [
            'PRESS "R" TO RETRY THE LEVEL',
            'THROW OBJECTS BACK INTO THE WINDOW THEY CAME FROM',
            'USE THE GRABBY HAND, DRAG AND RELEASE IN ANY DIRECTION',
        ]


class Level1(LevelBase):

    def __init__(self):
        """
        Trainer. No spoiler needed for this one.
        """
        LevelBase.__init__(self)
        self.title = "Level 1 - Back to where it came from! (Impish)"  # 2-3 windows, 3-4 items, enough time
        self.time_in_sec = 25.0
        self.win_condition = 4

        self.windows = [
            Window(400, 200), Window(600, 200)  # 1-3
        ]  # {id:position}
        self.appearances = [
            Appearance(5, 3, 1, Item(resources.resource_item_cat)),
            Appearance(10, 3, 0, Item(resources.resource_item_toaster)),
            Appearance(15, 3, 0, Item(resources.resource_item_cookie_jar)),
            Appearance(20, 3, 1, Item(resources.resource_item_budgie)),
        ]
        self.messages = [
            'MIND THE TWO WINDOWS',
            'PRESS "R" TO RETRY THE LEVEL',
        ]


class Level2(LevelBase):
    def __init__(self):
        """
        Trainer. No spoiler needed for this one.
        """
        LevelBase.__init__(self)
        self.title = "Level 2 - Watch out for the knock out! (Impish)"  # 2 windows, 5-6 items thrown repeatedly in short intervals
        self.time_in_sec = 60.0
        self.win_condition = 10
        self.ITEM_RANDOM_DIRECTION = False

        self.windows = [
            Window(400, 200), Window(600, 200)  # 1-3
        ]  # {id:position}
        self.appearances = [
            Appearance(1, 1, 0, Item(resources.resource_item_budgie)),
            Appearance(2, 1, 0, Item(resources.resource_item_speaker)),
            Appearance(3, 1, 1, Item(resources.resource_item_dog)),
            Appearance(4, 1, 0, Item(resources.resource_item_cat)),
            Appearance(5, 1, 0, Item(resources.resource_item_cookie_jar)),
            Appearance(16, 1, 1, Item(resources.resource_item_cat)),
            Appearance(17, 1, 1, Item(resources.resource_item_tv)),
            Appearance(18, 1, 1, Item(resources.resource_item_speaker)),
            Appearance(20, 1, 1, Item(resources.resource_item_speaker)),
            Appearance(25, 1, 1, Item(resources.resource_item_cookie_jar)),
        ]
        self.messages = [
            'FALLING AND THROWN ITEMS HURT WHEN THEY HIT',
            "A STUNNED LEPRECHAUN IS A LOSER",
            'RETURN ALL ITEMS TO THEIR WINDOW OF ORIGIN',
        ]


class Level3(LevelBase):
    def __init__(self):
        """
        Trainer. No spoiler needed for this one.
        """
        LevelBase.__init__(self)
        self.title = "Level 3 - What's the time (Impish)!"  # 2 windows, 5-6 items, shorter time
        self.time_in_sec = 16.0  # 60.0
        self.win_condition = 2  # 9
        self.ITEM_RANDOM_DIRECTION = False

        self.windows = [
            Window(200, 200), Window(400, 200), Window(600, 200), Window(800, 200),  # 1-3
            Window(200, 400), Window(400, 400), Window(600, 400), Window(800, 400),  # 4-7
        ]  # {id:position}
        self.appearances = [
            # Appearance(59, 3, 0, Item(resources.resource_item_cat)),
            Appearance(5, 1, 3, Item(resources.resource_item_tv)),
            Appearance(8, 1, 1, Item(resources.resource_item_dog)),
            Appearance(12, 3, 7, Item(resources.resource_item_cookie_jar)),
        ]
        self.messages = [
            'RETURN ENOUGH ITEMS BEFORE TIME RUNS OUT',
            'PRESS "R" TO RETRY THE LEVEL',
        ]


class Level4(LevelBase):
    def __init__(self):
        # This one is evil. HAHAhA :D -- Gumm
        # There is very little time for error, but you can recover for a small error.
        """
        SPOILER:
        1. At 5 secs toss bird close to window 7 in a safe spot.
        2. At 10 secs ready to toss bird again for twin's appearance in window 7.
        3. At 15 secs knock out twin when he reappears.
        4. At 15 secs bullet time, throw cat back.
        5. At 16 secs bullet time, another cat drops, dodge it.
        6. At 16 secs bullet time ends.
        7. At 16 secs Grab bird and knock out twin again, while aiming for window 4, but not too high!
        8. At 17 secs throw cat back during bullet time.
        9. At 17 secs wait for bird to fly home during bullet time.
        10. At 19 secs if you missed, grab bird and send him home to window 4.
        """
        LevelBase.__init__(self)
        self.title = "Level 4 - Plenty of time during knock out! (Fiendish)"  # 3 windows, 5 items, long appearances
        self.time_in_sec = 21.0
        self.win_condition = 3
        self.ITEM_RANDOM_DIRECTION = False

        self.windows = [
            Window(200, 200), Window(400, 200), Window(600, 200), Window(800, 200),  # 1-3
            Window(200, 400), Window(400, 400), Window(600, 400), Window(800, 400),  # 4-7
        ]  # {id:position}
        self.appearances = [
            Appearance(5, 1, 4, Item(resources.resource_item_budgie)),
            # Appearance(11, 1, 0, Item(resources.resource_item_budgie)),
            Appearance(15, 3, 7, Item(resources.resource_item_cat)),
            Appearance(16, 3, 7, Item(resources.resource_item_cat)),
        ]
        self.messages = [
            'THROW AN ITEM AT YOUR EVIL TWIN AFTER HE REAPPEARS',
            'FOR A BURST OF BULLET TIME',
        ]


class Level5(LevelBase):
    def __init__(self):
        # This one is more evil. HAHAhA :D -- Gumm
        # There is NO ROOM for error in this one. If you absolutely cannot complete it, even with the spoiler,
        # try setting self.time_in_sec = 18.0.
        """
        SPOILER:
        0. Window layout:
           0   1   2   3
           4   5   6   7
        1. Window 4 drops bird, 5 drops cat, 6 drops dog.
        2. Toss bird two windows to the right (just left of 6). You'll need it later.
        3. Grab and ready dog, wait for twin to reappear in window 7.
        4. Window 7 drops TV. Knock out twin.
        5. BULLET TIME. Return TV to window 7. (Dog is airborne.)
        6. Grab and ready cat, wait for twin to reappear in window 6.
        7. Window 6 drops speaker; dog lands on ground. Knock out twin.
        8. BULLET TIME. Return speaker and dog to window 6. (Cat is airborne.)
        9. Grab and ready bird, wait for twin to reappear in window 5. (Aim at window 4 arcing through window 5.)
        10. Window 5 drops toaster; cat lands on ground. Knock out twin.
        10. BULLET TIME. Return cat and toaster, farthest first, to window 5. (Bird is airborne.)
        11. Wait impatiently for airborne bird, cat, and toaster to land in their windows. :D :D :D
        """
        LevelBase.__init__(self)
        self.title = "Level 5 - It needs to be done in time! (Devilish)"  # n windows, m items, enough time
        self.time_in_sec = 17.0
        self.win_condition = 6
        self.ITEM_RANDOM_DIRECTION = False

        self.windows = [
            Window(200, 200), Window(400, 200), Window(600, 200), Window(800, 200),  # 1-3
            Window(200, 400), Window(400, 400), Window(600, 400), Window(800, 400),  # 4-7
        ]  # {id:position}
        self.appearances = [
            Appearance(5, 1, 4, Item(resources.resource_item_budgie)),
            Appearance(6, 1, 5, Item(resources.resource_item_cat)),
            Appearance(7, 1, 6, Item(resources.resource_item_dog)),
            Appearance(10, 3, 7, Item(resources.resource_item_tv)),
            Appearance(13, 3, 6, Item(resources.resource_item_speaker)),
            Appearance(16, 3, 5, Item(resources.resource_item_toaster)),
        ]
        self.messages = [
            'BULLET TIME, BULLET TIME, BULLET TIME',
        ]


class Level6(LevelBase):
    def __init__(self):
        """
        Take a break. No spoiler needed for this one.
        """
        LevelBase.__init__(self)
        self.title = "Level 6 - Too much stuff! (Impish)"  # n windows, m items, shorter time
        self.time_in_sec = 45.0
        self.win_condition = 16
        self.ITEM_RANDOM_DIRECTION = False

        self.windows = [
            Window(200, 200), Window(400, 200), Window(600, 200), Window(800, 200),  # 1-3
            Window(200, 400), Window(400, 400), Window(600, 400), Window(800, 400),  # 4-7
        ]  # {id:position}
        self.appearances = [
            Appearance(1, 1, 0, Item(resources.resource_item_dog)),
            Appearance(2, 1, 1, Item(resources.resource_item_cat)),
            Appearance(3, 1, 2, Item(resources.resource_item_budgie)),
            Appearance(4, 1, 3, Item(resources.resource_item_toaster)),
            Appearance(5, 1, 4, Item(resources.resource_item_speaker)),
            Appearance(6, 1, 5, Item(resources.resource_item_piano)),
            Appearance(7, 1, 6, Item(resources.resource_item_tv)),
            Appearance(8, 1, 7, Item(resources.resource_item_cookie_jar)),

            Appearance(9, 3, 0, Item(resources.resource_item_dog)),
            Appearance(13, 3, 1, Item(resources.resource_item_cat)),
            Appearance(17, 3, 2, Item(resources.resource_item_budgie)),
            Appearance(21, 3, 3, Item(resources.resource_item_toaster)),
            Appearance(25, 3, 4, Item(resources.resource_item_speaker)),
            Appearance(29, 3, 5, Item(resources.resource_item_piano)),
            Appearance(33, 3, 6, Item(resources.resource_item_tv)),
            Appearance(37, 3, 7, Item(resources.resource_item_cookie_jar)),
        ]
        self.messages = [
            'HAVE A BREAK',
            'NOTHING TOO HARD, JUST A LOT OF IT',
        ]


class Level7(LevelBase):
    def __init__(self):
        # Random. Pretty sloppy level.
        """
        SPOILER:
        Use the pets to keep the twin stun locked. Return the big items. Finally get the pets home.
        """
        LevelBase.__init__(self)
        self.title = "Level 7 - Only still time will win! (Naughty)"  # n windows, m items, short time, longish appearances
        self.time_in_sec = 20.0
        self.win_condition = 6
        self.ITEM_RANDOM_DIRECTION = True

        self.windows = [
            Window(200, 200), Window(400, 200), Window(600, 200), Window(800, 200),  # 1-3
            Window(200, 400), Window(400, 400), Window(600, 400), Window(800, 400),  # 4-7
        ]  # {id:position}
        self.appearances = [
            Appearance(1, 1, 4, Item(resources.resource_item_budgie)),
            Appearance(2, 1, 5, Item(resources.resource_item_dog)),
            Appearance(3, 1, 6, Item(resources.resource_item_cat)),
            Appearance(8, 3, 3, Item(resources.resource_item_toaster)),
            Appearance(11, 3, 2, Item(resources.resource_item_tv)),
            Appearance(14, 3, 1, Item(resources.resource_item_piano)),
        ]
        self.messages = [
            'RANDOM! WHY DID THERE HAVE TO BE RANDOM?',
            'VETERAN, WHY ARE YOU STILL READING THESE? :)',
            'SPACE FOR RENT',
        ]


class Level8(LevelBase):
    def __init__(self):
        LevelBase.__init__(self)
        self.title = "Level 8 - Raining pianos and some junk! (Fiendish)"
        self.time_in_sec = 45.0
        self.win_condition = 12
        self.ITEM_RANDOM_DIRECTION = True

        self.windows = [
            Window(200, 200), Window(400, 200), Window(600, 200), Window(800, 200),  # 1-3
            Window(200, 400), Window(400, 400), Window(600, 400), Window(800, 400),  # 4-7
        ]  # {id:position}
        self.appearances = [
            Appearance(4, 1, 5, Item(resources.resource_item_piano)),
            Appearance(4.1, 1, 6, Item(resources.resource_item_piano)),
            Appearance(7, 3, 0, Item(resources.resource_item_chest)),
            Appearance(10, 3, 3, Item(resources.resource_item_crate)),
            Appearance(20, 1, 4, Item(resources.resource_item_piano)),
            Appearance(20.1, 1, 7, Item(resources.resource_item_piano)),
            Appearance(23, 3, 0, Item(resources.resource_item_cat)),
            Appearance(26, 3, 3, Item(resources.resource_item_cookie_jar)),
            Appearance(29, 3, 5, Item(resources.resource_item_chest)),
            Appearance(35.1, 1, 0, Item(resources.resource_item_piano)),
            Appearance(38.1, 1, 3, Item(resources.resource_item_piano)),
            Appearance(41.1, 1, 2, Item(resources.resource_item_piano)),
        ]
        self.messages = [
            'P IS FOR PIANO, PIANO, PIANO...',
            "DOES SASQUATCH LEAVE A CARBON FOOTPRINT?",
            'HONK IF YOU LIKE BACON',
        ]


class Level9(LevelBase):
    def __init__(self):
        LevelBase.__init__(self)
        self.title = "Level 8 - Raining budgies 'n cats! (Devilish)"
        self.time_in_sec = 60.0
        self.win_condition = 12
        self.ITEM_RANDOM_DIRECTION = True

        self.windows = [
            Window(200, 200), Window(400, 200), Window(600, 200), Window(800, 200),  # 1-3
            Window(200, 400), Window(400, 400), Window(600, 400), Window(800, 400),  # 4-7
        ]  # {id:position}
        self.appearances = [
            Appearance(4, 1, 0, Item(resources.resource_item_budgie)),
            Appearance(5, 1, 1, Item(resources.resource_item_budgie)),
            Appearance(6, 1, 2, Item(resources.resource_item_cat)),
            Appearance(7, 1, 3, Item(resources.resource_item_cat)),
            Appearance(10, 1, 5, Item(resources.resource_item_speaker)),
            Appearance(11, 20, 6, Item(resources.resource_item_speaker)),
            Appearance(31, 1, 4, Item(resources.resource_item_budgie)),
            Appearance(32, 1, 5, Item(resources.resource_item_budgie)),
            Appearance(33, 1, 6, Item(resources.resource_item_cat)),
            Appearance(34, 1, 7, Item(resources.resource_item_cat)),
            Appearance(39, 1, 1, Item(resources.resource_item_speaker)),
            Appearance(40, 20, 2, Item(resources.resource_item_speaker)),
        ]
        self.messages = [
            'WE HAVE TO TALK',
            'LET ME BEND YOUR EAR',
            'CHEW THE FAT',
            'SHOOT THE BREEZE',
            'YACKITY YACK',
            'TALK SOME SMACK',
            'WHILE AWAY THE HOURS',
        ]


# only if we have time left to implement fog!
class Level10(LevelBase):
    def __init__(self):
        LevelBase.__init__(self)
        self.title = "Level 8 - If only I could see!"  # n windows, m items, enough time, slow item pace
        self.time_in_sec = 60.0
        self.win_condition = 8
        self.ITEM_RANDOM_DIRECTION = False

        self.windows = [
            Window(200, 200), Window(400, 200), Window(600, 200), Window(800, 200),  # 1-3
            Window(200, 400), Window(400, 400), Window(600, 400), Window(800, 400),  # 4-7
        ]  # {id:position}
        self.appearances = [
            Appearance(59, 3, 0, Item(resources.resource_item_cat)),
        ]


class OriginalTestLevel(LevelBase):

    def __init__(self):
        LevelBase.__init__(self)
        self.time_in_sec = 60.0
        self.win_condition = 8
        self.title = "Level 0 - Put it back!"  # one window, 2-3 items max, enough time

        self.windows = [
            Window(200, 200), Window(400, 200), Window(600, 200), Window(800, 200),  # 1-3
            Window(200, 400), Window(400, 400), Window(600, 400), Window(800, 400, False),  # 4-7
        ]  # {id:position}
        self.appearances = [
            Appearance(60 - 59, 3, 0, Item(resources.resource_item_cat)),
            Appearance(60 - 58, 20, 1, Item(resources.resource_item_piano)),
            Appearance(60 - 57, 1.5, 4, Item(resources.resource_item_tv)),
            Appearance(60 - 56, 1.4, 6, Item(resources.resource_item_speaker)),
            Appearance(60 - 55, 3, 0, Item()),
            Appearance(60 - 54, 2, 1, Item(resources.resource_item_cat)),
            Appearance(60 - 53, 2, 2, Item(resources.resource_item_tv)),
            Appearance(60 - 52, 2, 3, Item(resources.resource_item_toaster)),
            Appearance(60 - 51, 2, 3, Item()),
            Appearance(60 - 50, 2, 3, Item(resources.resource_item_cat)),

            Appearance(60 - 49, 3, 0, Item()),
            Appearance(60 - 48, 2, 1, Item(resources.resource_item_cookie_jar)),
            Appearance(60 - 47, 2, 2, Item(resources.resource_item_dog)),
            Appearance(60 - 46, 2, 3, Item(resources.resource_item_tv)),
            Appearance(60 - 45, 3, 0, Item(resources.resource_item_speaker)),
            Appearance(60 - 44, 2, 1, Item()),
            Appearance(60 - 43, 2, 2, Item(resources.resource_item_budgie)),
            Appearance(60 - 42, 2, 3, Item(resources.resource_item_toaster)),
            Appearance(60 - 41, 2, 3, Item(resources.resource_item_speaker)),
            Appearance(60 - 40, 2, 3, Item()),

            Appearance(60 - 39, 3, 0, Item()),
            Appearance(60 - 38, 2, 1, Item(resources.resource_item_toaster)),
            Appearance(60 - 37, 2, 2, Item(resources.resource_item_dog)),
            Appearance(60 - 36, 2, 3, Item(resources.resource_item_piano)),
            Appearance(60 - 35, 3, 0, Item(resources.resource_item_speaker)),
            Appearance(60 - 34, 2, 1, Item(resources.resource_item_cat)),
            Appearance(60 - 33, 2, 2, Item(resources.resource_item_cookie_jar)),
            Appearance(60 - 32, 2, 3, Item()),
            Appearance(60 - 31, 2, 3, Item(resources.resource_item_budgie)),
            Appearance(60 - 30, 2, 3, Item()),

        ]


class LevelTestSlowMotion:

    def __init__(self):
        self.windows = [
            Window(200, 200), Window(400, 200), Window(600, 200), Window(800, 200),  # 1-3
            Window(200, 400), Window(400, 400), Window(600, 400), Window(800, 400),  # 4-7
        ]  # {id:position}
        self.appearances = [
            Appearance(59, 3, 0, Item(resources.resource_item_cat)),
            Appearance(50, 10, 1, Item(resources.resource_item_piano)),
            Appearance(20, 10, 3, Item(resources.resource_item_piano)),
        ]
        self.time_in_sec = 60.0
        self.win_condition = 8  # 8 or more items returned
        self.ground_y = settings.GROUND_Y
        self.messages = [
            'THROW OBJECTS BACK INTO THE WINDOW THEY CAME FROM',
            'USE THE GRABBY HAND, DRAG AND RELEASE IN ANY DIRECTION',
        ]


class OutroLevel(LevelBase):
    def __init__(self):
        LevelBase.__init__(self)
        self.title = "Outro"
        self.time_in_sec = 900.0
        self.win_condition = 42
        self.ITEM_RANDOM_DIRECTION = True

        self.windows = [
            Window(200, 200), Window(400, 200), Window(600, 200), Window(800, 200),  # 1-3
            Window(200, 400), Window(400, 400), Window(600, 400), Window(800, 400),  # 4-7
        ]  # {id:position}
        self.appearances = [
            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),

            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),

            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),

            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),

            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),

            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),

            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),

            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),

            Appearance(-4, 0, 0, Item()),
            Appearance(-4, 0, 0, Item()),

            Appearance(5, 5, 0, None),
            Appearance(10, 5, 1, None),
            Appearance(15, 5, 2, None),
            Appearance(20, 5, 3, None),
            Appearance(25, 5, 4, None),
            Appearance(30, 5, 5, None),
            Appearance(35, 5, 6, None),
            Appearance(40, 5, 0, None),
            Appearance(45, 5, 1, None),
            Appearance(50, 5, 2, None),
            Appearance(55, 5, 3, None),
            Appearance(60, 5, 4, None),
            Appearance(65, 5, 5, None),
            Appearance(70, 5, 6, None),

        ]
        self.messages = [
            'WE HAVE TO TALK',
            'LET ME BEND YOUR EAR',
            'CHEW THE FAT',
            'SHOOT THE BREEZE',
            'YACKITY YACK',
            'TALK SOME SMACK',
            'WHILE AWAY THE HOURS',
        ]

levels = [
    Level0(),
    Level1(),
    Level2(),
    Level3(),
    Level4(),
    Level5(),
    Level6(),
    Level7(),
    Level8(),
    Level9(),
    # LevelTestSlowMotion()
    # OriginalTestLevel(),
]

logger.debug("imported")
