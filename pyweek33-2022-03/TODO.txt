These are not in any particular priority yet.

X  replace all FakeImages
X  intro
2  outro
3  more levels? (At least one more Devilish after the Naughty level 7.)
4  Mood music for intro and outro
o  fog level?
o  Juice collisions and actions - Particles, shockwaves, dust cloud, color burst, camera bump,
   sprite text w/ tweener FX, Batman style >KERPOW< image
o  Juice pieces falling off TV, toaster, piano
X  Juice pointer - Colorize or fatten the longer you make it
o  Juice eyes
X  Juice good and evil aura
o  SFX dog growl-biting when he hits twin
o  SFX cat yowling-hissing when he hits twin
o  SFX bird pecking when he hits twin
X  a retry key so you don't have to wait on the timer when you mess up?

o README.md
o Project page


BUGS

o  Graphics artifacts upon level change or replay
o  Should level complete when number of items are returned, and not wait for the timer?
