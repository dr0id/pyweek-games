# Pyweek 33 #

See [pyweek 33](https://pyweek.org/33/) for more information. 

## Theme for Pyweek 33, March 2022 ##

*My evil twin*

## Project GD-33 ##

Game title: Evil twin KABOOOM!

GD-33 is a collaboration between DR0ID and Gummbum for pyweek 33.

### Setup ###

Download Python. Versions 3.9.4 and 3.9.7 were used for development and testing.

pip install pygame. Version 2.0.1 was used for development and testing. Open command line and execute (or similar):

    'python -m pip install pygame'
    win: 'py -m pip install pygame'

Run the game using 'python run_game.py' on the command line.

### Playing the game ###

Your evil twin got out...somehow. It's a mystery. And anyhow, you need to back burner that 'cause right now he is
on a mad spree to mess up your neighborhood in the spirit of messing up stuff! Lest anyone mistake you for him, and
come after you for his misdeeds, you'd better get a handle on this pronto.

#### Levels ####

The game has 10 levels. They start out easy as you get your bearing, but quickly ramp up in difficulty.

#### Use the power glove to hurl stuff ####

You will get a glove of power, which you'll use to handle the misdeeds of your evil twin. Place the glove over a
mess he's made, click the mouse to grab the mess, and drag the mouse in the direction you want to throw. That's the
basic, hero. Sounds simple enough.

But wait, there's more.

#### Time constraints ####

Your neighbors are alerted and on the prowl, looking for miscreant(s). So clean up the messes within time constraints
to avoid detection.

#### Strategy ####

Also, on the harder levels you must dodge the falling messes whilst throwing some of them at your nemesis. A mess so
flung and making contact results in a short knock out. When you knock out your twin you'll gain the advantage of a
burst of bullet time. Some levels cannot be won without invoking bullet time.

#### Tips ####

Read the on-screen hints, at least until you've become a veteran at handling your evil twin.

In general, for the harder levels you'll have to study the drop order and figure out how to tactially stun your evil
twin to let you accomplish multiple tasks while he is stunned. And, as well, an expert's aim and timing and fancy
footwork will take you far.

If you get stuck and want to win at any cost,

1. View the source in gamelib/levels/__init__.py. All the levels' classes are in there. They are numbered the same
   as they are in game, so if you want a SPOILER for level 4 look for Level4 in the code. There you will find a
   docstring with the precise steps to become a winner.
2. You can force the game to start at any level. Create a file gamelib/_custom.py, place `current_level_idx = N`
   on a line by itself (where N is the level number), and restart the game. You can skip entire levels and come out
   a winner!

### Tech support and bug reports ###

Please leave a comment on the [project page](https://pyweek.org/e/GD-33/) diary,
or create a post to our attention on the [Pyweek message board](https://pyweek.org/messages/).
Please be ready to provide the relevant log file from the game's root directory.

### Attributions ###

Various attributions are required by license. These can be found in attribution.txt files the data/ directory
alongside the asset files used in the game.

### Cheats and spoilers ###

