decoration.png https://opengameart.org/content/vintage-decor
FroFox         https://opengameart.org/users/frofox
CC-BY 4.0

FreeArt_householdblocks_pngs.zip
obj*.png     https://opengameart.org/content/householdblocks
SpriteAttack https://opengameart.org/users/spriteattack
CC0

pixel-ani-cat.gif
pixel-ani-dog.gif
pixel-ani-cockatiel.gif
pixel-ani-budgie.gif https://opengameart.org/content/animals-and-tea-by-strawheart
strawheart           http://mtsids.com/
CC-BY-SA 3.0

HandIcons.png https://opengameart.org/content/mouse-hand-icons
Cough-E       https://opengameart.org/users/cough-e
OGA-BY 3.0, CC0

